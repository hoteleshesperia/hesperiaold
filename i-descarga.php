<?php
/* -------------------------------------------------------
Script  bajo los t&eacute;rminos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor: Hector A. Mantellini (Xombra)
ViserProject - @viserproject
-------------------------------------------------------- */

function Descarga($imagen,$descarga=null) {
		$etag = md5_file($imagen);
		$descarga = $descarga !== null ? $descarga : basename($imagen);
		header('Content-Description: File Transfer');
		header("Etag: $etag"); 
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . $descarga);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header("Pragma: no-cache");
		header('Content-Length: ' . filesize($imagen));
		ob_clean();
		flush();
		readfile($imagen);
		exit;
return;
}

$imagen = 'img/prensa/'.trim($_GET["imagen"]);
if (file_exists($imagen)) {
	$tipo  = getimagesize($imagen);
	switch ($tipo[2]) {
		case 2:
			header('content-type image/jpeg');
		    break;
		case 3:
			header("Content-type: image/png");
		    break;
	  default:
	   die("Tipo de Imagen no valida");
	}
	$nuevo_nombre = date("d-m-Y",filemtime($imagen)).'_'.$imagen;
	Descarga($imagen, $nuevo_nombre);
} else { echo '<p>ERROR!, la imagen solicitada no existe!</p>'; }
?>
