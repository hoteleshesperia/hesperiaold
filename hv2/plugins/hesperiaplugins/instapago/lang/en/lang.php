<?php return [
    'plugin' => [
        'name' => 'Instapago',
        'description' => ''
    ],
    'instapago' => [
    	'success' => 'Transacción exitosa.',
    	'empty' => 'Error de comunicación con la entidad bancaria.',
    ]
];