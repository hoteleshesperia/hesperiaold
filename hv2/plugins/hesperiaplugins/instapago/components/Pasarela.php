<?php namespace HesperiaPlugins\Instapago\Components;

use Cms\Classes\ComponentBase;

use HesperiaPlugins\Instapago\Models\Transacciones as Transaccion;
use HesperiaPlugins\Instapago\Models\Afiliados as Afiliado;

use Redirect;
use Flash;
use Lang;

class Pasarela extends ComponentBase{
	public $afiliadoId;
    public $monto;
    public $referencia;
    public $afiliado;

	public function componentDetails(){
		return[
			'name' => 'Pasarela de Pago',
			'description' => 'Pasarela de pago de Instapago, etc etc etc.'
		];
	}

	public function defineProperties()
    {
        return [
            'afiliadoId' => [
                'title'       => 'Id afiliado',
                'description' => 'Id del afiliado a consultar'
            ],
            'monto' => [
                'title'       => 'Monto',
                'description' => 'Monto a procesar'
            ],
            'referencia' => [
                'title'       => 'Referencia',
                'description' => 'Referencia de factura o servicio a pagar'
            ]
        ]; 
    }


    public function onRun(){ /*Corre antes del render*/

    } 

    public function onRender(){ /*Aqui es donde se deben recuperar los parametros*/
       
    }

	protected function datosAfiliado($id){		

		$afiliado = Afiliado::where('id', $id)
               ->get();
        return $afiliado;

	}

    public function onPay(){
        /*
         * Validate input
         */   
        $this->afiliadoId = $this->property('afiliadoId');
        $this->monto = $this->property('monto');
        $this->referencia = $this->property('referencia');

        if($this->afiliadoId == NULL)
            $this->afiliadoId = $this->page['afiliadoId'];

        if($this->monto == NULL)
            $this->monto = $this->page['monto'];

        if($this->referencia == NULL)
            $this->referencia = $this->page['referencia'];

        $data = post();

        $this->afiliado = $this->datosAfiliado($this->afiliadoId);

        //var_dump(sizeof($this->afiliado));
        if($this->afiliado != NULL && sizeof($this->afiliado) > 0){
            //var_dump('No es nulo');
            $url = 'https://api.instapago.com/payment';

            $fields = array(
                "KeyID"             => $this->afiliado[0]->llave_privada, //requerido
                "PublicKeyId"       => $this->afiliado[0]->llave_publica, //requerido
                "Amount"            => $this->monto, //requerido
                "Description"       => "Prueba pasarela de Pago por Maria Cristovao - Marketing", //requerido
                "CardHolder"        => $data["nombre_tarjeta"], //requerido
                "CardHolderId"      => $data["identificacion"], //requerido
                "CardNumber"        => $data["numero_tarjeta"], //requerido
                "CVC"               => $data["cvc"], //requerido
                "ExpirationDate"    => $data["mes_expira"].'/'.$data["anio_expira"], //requerido
                "StatusId"          => "2", //requerido
                "IP"                => $_SERVER["REMOTE_ADDR"] //requerido
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec ($ch);
            curl_close ($ch);
            $obj = json_decode($server_output);
            if($obj == NULL){

                Flash::error('Error de comunicación con la entidad bancaria.');

            }else{
                $transaccion = new Transaccion();
                $transaccion->tarjetahabiente = $data["nombre_tarjeta"];
                $transaccion->identificacion = $data["identificacion"];
                $transaccion->monto = $this->monto;
                $transaccion->status = $obj->code;
                $transaccion->mensaje = $obj->message;
                $transaccion->voucher = $obj->voucher;
                $transaccion->afiliado_id = $this->afiliadoId;
                $transaccion->referencia = $this->referencia;
                $transaccion->save();
                $message = $obj->message;
                if($obj->code == 201){
                    Flash::success($message);
                }else{
                    Flash::error($message);
                }
            }
            
        }else{
            Flash::error('No existe afiliado');
        }

        //return Redirect::to('/contrato')->withInput($data);
    }

}