<?php namespace hesperiaplugins\Instapago\Models;

use Model;

/**
 * Model
 */
class Transacciones extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'hesperiaplugins_instapago_transacciones';

    //VARIABLE - RUTA DEL MODELO - KEY--> CLAVE-FORANEA EN MI TABLA
    public $belongsTo = [
        'usuario' => ['rainlab\user\models\user', 'key' => 'usuario_id' ]
    ];
}