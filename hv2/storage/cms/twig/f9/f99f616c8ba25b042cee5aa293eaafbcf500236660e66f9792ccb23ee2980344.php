<?php

/* /home/hesperiave/public_html/hv2/plugins/hesperiaplugins/instapago/components/pasarela/default.htm */
class __TwigTemplate_9b532ac73f70d432357e546a2b32958151f81245f203eb6300731fff0a710fdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h4>Instapago</h4>

\t
\t<!-- <form class=\"form-inline text-center\" data-request=\"onDisponibilidad\"> -->
\t<form class=\"form text-center\" data-request=\"onPay\" data-request-flash>
        
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">   
\t\t\t\t<div class=\"col-xs-6 col-md-6\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"NameCard\">Nombre del tarjeta habiente</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"nombre_tarjeta\" id=\"nombre_tarjeta\" placeholder=\"Nombre del tarjeta habiente\" required />
\t\t\t                <span class=\"input-group-addon\"><i class=\"fui-user\"> </i> </span>
\t\t\t            </div>
\t\t\t\t\t</div>
\t\t\t\t</div>     \t\t
\t\t\t\t<div class=\"col-xs-6 col-md-6\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"identificacion\">Cédula | RIF | ID</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"identificacion\" id=\"identificacion\" placeholder=\"Numero de cédula | RIF | ID\" required />
\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"fui-info\"></i></span>
\t\t\t\t\t\t</div>
\t                </div>
                </div>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-8 col-md-8\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"numero_tarjeta\">Número de Tarjeta</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"numero_tarjeta\" id=\"numero_tarjeta\" placeholder=\"Numero de tarjeta de crédito sin espacios ni separador\" required />
\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"fui-credit-card\"></i></span>
\t\t\t\t\t\t</div>
\t                </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 col-md-4\">
\t\t\t\t\t<img src=\"plugins/hesperiaplugins/instapago/assets/img/accepted.png\" class=\"img-responsive\" alt=\"Tarjetas Aceptadas\" />
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-4 col-md-4\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"anio_expira\">Año Expiracion</label>
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"anio_expira\" id=\"anio_expira\" placeholder=\"AAAA\" required maxlength=\"4\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 col-md-4\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"mes_expira\">Mes Expiracion</label>
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mes_expira\" id=\"mes_expira\" placeholder=\"MM\" required maxlength=\"2\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 col-md-4 pull-right\">
\t\t\t\t\t<div class=\"form-group\">
\t                \t<label for=\"cvc\">Código CV</label>
\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"cvc\" id=\"cvc\" placeholder=\"CVC\" required  maxlength=\"3\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Pagar</button>
\t\t\t</div>
\t\t</div>

    </form>
";
    }

    public function getTemplateName()
    {
        return "/home/hesperiave/public_html/hv2/plugins/hesperiaplugins/instapago/components/pasarela/default.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h4>Instapago</h4>

\t
\t<!-- <form class=\"form-inline text-center\" data-request=\"onDisponibilidad\"> -->
\t<form class=\"form text-center\" data-request=\"onPay\" data-request-flash>
        
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">   
\t\t\t\t<div class=\"col-xs-6 col-md-6\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"NameCard\">Nombre del tarjeta habiente</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"nombre_tarjeta\" id=\"nombre_tarjeta\" placeholder=\"Nombre del tarjeta habiente\" required />
\t\t\t                <span class=\"input-group-addon\"><i class=\"fui-user\"> </i> </span>
\t\t\t            </div>
\t\t\t\t\t</div>
\t\t\t\t</div>     \t\t
\t\t\t\t<div class=\"col-xs-6 col-md-6\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"identificacion\">Cédula | RIF | ID</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"identificacion\" id=\"identificacion\" placeholder=\"Numero de cédula | RIF | ID\" required />
\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"fui-info\"></i></span>
\t\t\t\t\t\t</div>
\t                </div>
                </div>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-8 col-md-8\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"numero_tarjeta\">Número de Tarjeta</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"numero_tarjeta\" id=\"numero_tarjeta\" placeholder=\"Numero de tarjeta de crédito sin espacios ni separador\" required />
\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"fui-credit-card\"></i></span>
\t\t\t\t\t\t</div>
\t                </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 col-md-4\">
\t\t\t\t\t<img src=\"plugins/hesperiaplugins/instapago/assets/img/accepted.png\" class=\"img-responsive\" alt=\"Tarjetas Aceptadas\" />
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-4 col-md-4\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"anio_expira\">Año Expiracion</label>
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"anio_expira\" id=\"anio_expira\" placeholder=\"AAAA\" required maxlength=\"4\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 col-md-4\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"mes_expira\">Mes Expiracion</label>
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"mes_expira\" id=\"mes_expira\" placeholder=\"MM\" required maxlength=\"2\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 col-md-4 pull-right\">
\t\t\t\t\t<div class=\"form-group\">
\t                \t<label for=\"cvc\">Código CV</label>
\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"cvc\" id=\"cvc\" placeholder=\"CVC\" required  maxlength=\"3\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<br>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Pagar</button>
\t\t\t</div>
\t\t</div>

    </form>
", "/home/hesperiave/public_html/hv2/plugins/hesperiaplugins/instapago/components/pasarela/default.htm", "");
    }
}
