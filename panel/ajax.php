<?php
session_start();
include 'include/databases.php';

# calendario

	$dbhost=$hostname;
	$dbname=$db_name;
	$dbuser=$user;
	$dbpass=$password;
$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

?>
		<div class="formulario">
		<?php
		function fecha($fecha,$forma)
		{	$trim=explode("-",$fecha);
			if (intval($trim[2])<10) $trim[2]="0".$trim[2];
			if ($forma=="es") return $trim[2]."-".$trim[1]."-".$trim[0];
			elseif ($forma=="en") return $trim[0]."-".$trim[1]."-".$trim[2];
		}
		switch ($_GET["accion"])
		{ case "insertar":
				echo '<h1>
							Colocar Nuevo Precio Hotel '.$_SESSION["nombreHotel"].' el '.fecha($_GET["dia"],"es").'
						</h1>';
				if (isset($_POST) && count($_POST)>0)
				{  $ahora = time();
					$hoy = date("d-m-Y",$ahora);
					$fecha = $_POST["fecha"];
					if (strlen ( $fecha ) >= 10) {
					$fecha_precio = NORMAL_MYSQL2($fecha); }
					else { $fecha_precio = NORMAL_MYSQL3($fecha); }

					$id_hab = $_POST["id_hab"];

					$sql = sprintf("SELECT nombre_habitacion
											FROM hesperia_habitaciones
											WHERE id_habitacion = '$id_hab'");
					$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					$rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
					$nombre_habitacion = $rows["nombre_habitacion"];

					$id_hotel = $_SESSION["ElHotel"];
					$nombre_hotel = $_SESSION["nombreHotel"];
					$nuevo_precio = $_POST["nuevo_precio"];
					$nuevo_precio_promo = $_POST["nuevo_precio_promo"];
					$disponibles = $_POST["disponibles"];

					$sql = sprintf("INSERT INTO hesperia_post_precio 
											(id, id_hotel, id_habitacion, precio, precio_promocion,
											fecha_precio, fecha, procesado, disponible_dia)
										VALUES ( NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
								mysqli_real_escape_string($mysqli,$id_hotel),
								mysqli_real_escape_string($mysqli,$id_hab),
								mysqli_real_escape_string($mysqli,$nuevo_precio),
								mysqli_real_escape_string($mysqli,$nuevo_precio_promo),
								mysqli_real_escape_string($mysqli,$fecha_precio),
								mysqli_real_escape_string($mysqli,$ahora),
								mysqli_real_escape_string($mysqli,0),
								mysqli_real_escape_string($mysqli,$disponibles));
					$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					$UltID = mysqli_insert_id($mysqli);

					$texto = "Nuevo Precio Hotel: [ $nombre_hotel ]<br/> Habitacion: [ $nombre_habitacion ] <br/>Precio Normal: [ $nuevo_precio ] <br/>Precio Promocion: [ $nuevo_precio_promo ] <br/>Habitaciones Disponibles: [ $disponibles ] <br/>por [ $_SESSION[nombre] ] <br/>para el/los dia(s): [ $fecha ] <br/>Realizado: [ $hoy ]";

					$query_eventos=$db->query("INSERT INTO tcalendario (fecha,evento,id_precio,id_hotel) 
							values ('$fecha','$texto','$UltID','$id_hotel')");
					graba_LOG("Nuevo Precio Post Hotel: [ $nombre_hotel ] -Habitacion: [ $nombre_habitacion ]",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
					if ($query_eventos) echo "<p class='ok'>Se ha almacenado el nuevo precio correctamente.</p>";
					else echo "<p class='ko'>Se ha producido un error guardando la acción.</p>";
				} if (isset($_GET["dia"]) && !isset($_POST["evento"]))
				{ ?>
					<form action="" method="post" class="datos">
						<fieldset>
							<div>
							<?php echo '<p>
							 <label for="tipo_habitacion">Seleccione Habitación</label>
												<select class="form-control" name="id_hab" id="id_hab">
												<option value="0" selected="selected">Seleccione</option>';
							 						$sql = sprintf("SELECT id_habitacion,id_hotel,nombre_habitacion,
																			precio,precio_promocion
															 FROM hesperia_habitaciones
															WHERE id_hotel = '$_SESSION[ElHotel]' 
															ORDER BY nombre_habitacion ASC");
													 $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
													 while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
														echo '<option value="'.$rows["id_habitacion"].'">
														'.$rows["nombre_habitacion"].' | Precio Actual: '.$rows["precio"].' | 
														Precio Promo: '.$rows["precio_promocion"].'</option>';
													 }
													 echo'
												</select></p>

								<p><label>Nuevo Precio:</label>
								<input type="num" name="nuevo_precio" value="0"/></p>
								<p><label>Nuevo Precio Promo:</label>
								<input type="num" name="nuevo_precio_promo" value="0"/></p>
								<p><label>Disponibles:</label>
								<input type="num" name="disponibles" value="0"/></p>';
							?>
							</div>

							<div>
								<input type="hidden" name="fecha" value="<?php echo fecha($_GET["dia"],"en"); ?>">
								<br/>
								<input type="submit" value=" Cambiar Precios ">
							</div>
						</fieldset>
					</form>
					<?php
				}
			break;

			case "edicion":
				echo '<h1>Editar Precio de Habitación<br/> Hotel: '.$_SESSION["nombreHotel"].'</h1>';
				if (isset($_POST) && count($_POST)>0)
				{ 	$id_hab = $_POST["id_hab"];

					$sql = sprintf("SELECT nombre_habitacion
											FROM hesperia_habitaciones
											WHERE id_habitacion = '$id_hab'");
					$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					$rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
					$nombre_habitacion = $rows["nombre_habitacion"];
					$fecha = $_POST["fecha"];
					$id_hotel = $_SESSION["ElHotel"];
					$nombre_hotel = $_SESSION["nombreHotel"];
					$nuevo_precio = $_POST["nuevo_precio"];
					$nuevo_precio_promo = $_POST["nuevo_precio_promo"];
					$disponibles = $_POST["disponibles"];
					$ahora = time();
					$hoy = date("d-m-Y",$ahora);
					$texto = "Nuevo Precio Hotel: $nombre_hotel<br/> Habitacion: [ $nombre_habitacion ] <br/>Precio Normal: [ $nuevo_precio ] <br/>Precio Promocion: [ $nuevo_precio_promo ] <br/>Habitaciones Disponibles: [ $disponibles ] <br/>por [ $_SESSION[nombre] ] <br/>para el/los dia(s): [ $fecha ] <br/>Realizado: [ $hoy ]";
					$id_precio = $_POST["id_precio"];
					$sql = sprintf("UPDATE hesperia_post_precio SET
											id_habitacion = '%s',
											precio = '%s',
											precio_promocion = '%s',
											fecha = '%s',
											procesado = '%s',
											disponible_dia = '%s'
											WHERE id = '%s'",
								mysqli_real_escape_string($mysqli,$id_hab),
								mysqli_real_escape_string($mysqli,$nuevo_precio),
								mysqli_real_escape_string($mysqli,$nuevo_precio_promo),
								mysqli_real_escape_string($mysqli,$ahora),
								mysqli_real_escape_string($mysqli,0),
								mysqli_real_escape_string($mysqli,$disponibles),
								mysqli_real_escape_string($mysqli,$id_precio));
					$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					graba_LOG("Actaulizado Precio Post Hotel: [ $nombre_hotel ] -Habitacion: [ $nombre_habitacion ]",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);

					$query_eventos=$db->query("update tcalendario set evento='$texto' where id='".intval($_POST["idevento"])."'");

					if ($query_eventos) echo "<p class='ok'>
						Se ha modificado el precio con los datos suministrados.</p>";
					else echo "<p class='ko'>ERROR!  No se ha producido el cambio solicitado.</p>";
				}
				if (isset($_GET["idevento"]) && !isset($_POST["idevento"]))
				{
					$query_eventos=$db->query("select * from tcalendario where 1 && 
					id='".intval($_GET["idevento"])."' limit 1");
					if ($evento=$query_eventos->fetch_array())
					{ $fecha = $evento["fecha"];
					  $id_precio = $evento["id_precio"];
					?>
					<form action="" method="post" class="datos">
						<fieldset>
							<div>
								<p>Fecha:<?php echo $fecha; ?></p>
								<p><label>
									Datos previos: <pre><?php echo $evento["evento"]; ?></pre>
								</label></p>

							<?php 
							$sql = sprintf("SELECT * FROM hesperia_post_precio
													WHERE id = '$id_precio'");
							$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
							$rowsP = mysqli_fetch_array($result,MYSQLI_ASSOC);

						echo '<hr/><p>
							 <label for="tipo_habitacion">Seleccione Habitación</label>
												<select class="form-control" name="id_hab" id="id_hab">
												<option value="0" selected="selected">Seleccione</option>';
							 						$sql = sprintf("SELECT id_habitacion,id_hotel,nombre_habitacion,
																			precio,precio_promocion
															 FROM hesperia_habitaciones
															WHERE id_hotel = '$_SESSION[ElHotel]' 
															ORDER BY nombre_habitacion ASC");
													 $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
													 while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
														if ($rows["id_habitacion"] == $rowsP["id_habitacion"]) {
														echo '<option value="'.$rows["id_habitacion"].'" selected>
														'.$rows["nombre_habitacion"].' | Precio Actual: '.$rows["precio"].' | 
														Precio Promo: '.$rows["precio_promocion"].'</option>';
														} else {
														echo '<option value="'.$rows["id_habitacion"].'">
														'.$rows["nombre_habitacion"].' | Precio Actual: '.$rows["precio"].' | 
														Precio Promo: '.$rows["precio_promocion"].'</option>'; }
													 }
													 echo'
												</select></p>

								<p><label>Nuevo Precio:</label>
								<input type="num" name="nuevo_precio" value="'. $rowsP["precio"].'"/></p>
								<p><label>Nuevo Precio Promo:</label>
								<input type="num" name="nuevo_precio_promo" value="'. $rowsP["precio_promocion"].'"/></p>
								<p><label>Disponibles:</label>
								<input type="num" name="disponibles" value="'. $rowsP["disponible_dia"].'"/></p>'; ?>

							</div>
							<div>
								<input type="hidden" name="idevento" value="<?php echo $evento["id"]; ?>">
								<input type="hidden" name="id_precio" value="<?php echo $id_precio; ?>">
								<input type="hidden" name="fecha" value="<?php echo $fecha ; ?>">
								<input type="submit" value=" Modificar ">
							</div>
						</fieldset>
					</form>
					<?php
					}
				}
			break;

			case "eliminar":
				echo '<h1>
							Eliminar Precio de Hotel</h1>';
				if (isset($_POST) && count($_POST)>0)
				{ 	$nombre_hotel = $_SESSION["nombreHotel"];
					$fecha = $_POST["fecha"];
					$ahora = time();
					graba_LOG("Eliminado Precio Post Hotel: [ $nombre_hotel ] - de Fecha: $fecha",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
					$id_precio = $_POST["id_precio"];
					$sql = sprintf("DELETE FROM hesperia_post_precio
								WHERE id= '%s'",
								mysqli_real_escape_string($mysqli,$id_precio));
					$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					$query_eventos=$db->query("delete from tcalendario where id='".intval($_POST["idevento"])."'");
					if ($query_eventos) echo "<p class='ok'>Se ha eliminado correctamente el precio post datado.</p>";
					else echo "<p class='ko'>Se ha producido un error eliminando el precio.</p>";
				}
				if (isset($_GET["idevento"]) && !isset($_POST["idevento"]))
				{
					$query_eventos=$db->query("select * from tcalendario where 1 and id='".intval($_GET["idevento"])."' limit 1");
					if ($evento=$query_eventos->fetch_array())
					{
					?>
					<form action="" method="post" class="datos">
						<fieldset>
							<p>Fecha: <?php echo $evento["fecha"]; ?></p>
							<div><label>Por favor, confirma que quieres eliminar este cambio<br/> <strong><?php echo $evento["evento"]; ?></strong></label></div>
							<div>
								<input type="hidden" name="id_precio" value="<?php echo $evento["id_precio"]; ?>">
								<input type="hidden" name="idevento" value="<?php echo $evento["id"]; ?>">
								<input type="hidden" name="fecha" value="<?php echo $evento["fecha"]; ?>">
								<p><input type="submit" value=" Eliminar Precio "></p>
							</div>
						</fieldset>
					</form>
					<?php
					}
				}
			break;
		}
		?>
		</div>
		<script>
			$(document).ready(function()
			{ $("form.datos").validate();
			});
		</script>
