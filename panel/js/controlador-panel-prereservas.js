$(document).ready(function(){

	//alert("controlador");

	$(".delete-prereserva").on("click", function(){
		//alert($(this).data("value"));
		var codigo = $(this).data("value");
		var tipo = $(this).data("tipo");

		var item_data= new FormData();
		item_data.append("codigo",codigo);
		item_data.append("tipo",tipo);
		item_data.append("accion", "delete");
		swal({
		  title: "¿Estas seguro que desea eliminar esta solicitud?",
		  text: "el proceso no se podrá deshacer",
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  showCancelButton: true,
		  closeOnConfirm: false,
		  showLoaderOnConfirm: true,
		},
		function(){
			$.ajax({
				type:"POST",
				url:"https://hoteleshesperia.com.ve/panel/acciones/acciones_prereservas.php",
				data:item_data,
				contentType:false,
					processData:false,
				cache:false,
				success: function(response){
					if(response=="ok"){
						swal("OK", "Procesado correctamente", "success");
						location.reload();
					}else{
						swal("Error", "Algo ha salido mal, intente más tarde"+response, "error");
						//location.reload();
					}
				},

				error: function(response){
					swal("Error", "Algo ha salido mal, intente más tarde"+response, "error");
					//location.reload();
				}
			});		
		});
	});

});