$(document).ready(function(){

	$(".summernote").summernote({
		lang: 'es-ES',
        toolbar: [
	        ['style', ['bold', 'italic', 'underline', 'clear', 'link', 'color']],
	        ['para', ['ul', 'ol', 'paragraph']],
	        
        ]
	});

	$('.table-reg').DataTable();

	$("#form-modal-brochure").on("click", "#add-mod-brochure-btn", function(e){
		e.preventDefault();

		if( $('#add-mod-brochure-form').smkValidate() ){


			if ($(this).data("accion")=="add") {
				var formData = new FormData($("#add-mod-brochure-form")[0]);
			   	formData.append("accion", "insert");
			   	if (validar_formato($("#archivo-brochure"))) {
			   		ajaxMediaCenter(formData, "brochure");	
			   	}else{
			   		alert("formato de archivo invalido");
			   		return false;
			   	}

			}else if($(this).data("accion")=="mod"){
				var formData = new FormData($("#add-mod-brochure-form")[0]);
				formData.append("accion", "update");

				if ($("#archivo-brochure").val()!="" && !validar_formato($("#archivo-brochure"))) {
					alert("formato de archivo invalido");
					return false;
				}else{
					$(this).prop("disabled","true");
					$(this).text("Cargando");
					ajaxMediaCenter(formData, "brochure");	
				}	
			}		  	
		}
		
	});

	$("#dataTables-brochures").on("click", ".editar-pdf", function(e){
		//e.preventDefault();

		var data = $(this).data("value");

		$("#titulo").val(data.titulo);
		$("#descripcion").val(data.descripcion);
		$("#anterior").val(data.nombre_archivo);
		$("#id_brochure").attr("value", data.id);
		$("#tipo_pdf").val(data.tipo);
		if (data.ind_activo=="S") {
			$("#activo").prop("checked", true);
		}else{
			$("#activo").prop("checked", false);
		}
		$("#idhotel").val(data.id_hotel);
		$("#add-mod-brochure-btn").attr("data-accion", "mod");
		$("#titulo-form-brochure").text("Modificar Brochure");


		$("#form-modal-brochure").modal(function(){
			show:true
		});
		
	});

	$("#add-brochure-btn").click(function(){
		$("#titulo-form-brochure").text("Nuevo Brochure");
		$('#add-mod-brochure-form').smkClear();
		$("#form-modal-brochure").modal(function(){
			show:true
		});
	});

	$("#dataTables-brochures").on("click", ".eliminar-pdf", function(e){
		//e.preventDefault();
		var data = $(this).data("value");
		$("#delete-item-btn").attr("data-value", JSON.stringify(data));
		$("#delete-item-btn").attr("data-tipo", "brochure");
		$("#delete-modal-item").modal(function(){
			show:true
		});
	});

	$("#delete-item-btn").click(function(){
		var data = $(this).data("value");
		var tipo = $(this).data("tipo");
		var formData = new FormData();
		formData.append("accion", "delete");
		if (tipo=="brochure") {
			formData.append("id", data.id);
			formData.append("anterior", data.nombre_archivo);
		}else if(tipo=="imagen"){
			formData.append("id", data.id);
			formData.append("anterior", data.nombre_imagen);
		}else if(tipo=="video"){
			formData.append("id", data.id);
		}else if(tipo=="noticia"){
			formData.append("id", data.id_noticia);
			formData.append("anterior", data.imagen);
		}else if(tipo=="galeria-noticia"){
			formData.append("id", data.id_imagen);
			formData.append("anterior", data.nombre_imagen);
			formData.append("accion", "delete-galeria");
		}
		
		ajaxMediaCenter(formData, tipo);

	});

	$("#nueva-img-btn").click(function(){
		$("#add-imagen-btn").attr("data-accion", "insert");
		$("#tipo-img-content").show();
		$("#modal-imagen-titulo").text("Subir Imagen");

		$("#form-modal-imagen").modal(function(){
			show:true
		});
	});

	$("#add-imagen-btn").click(function(){
		if( $('#add-imagen-form').smkValidate() ){
			if (validar_formato_img($('#imagen-mediacenter'))) {
				var formData = new FormData($("#add-imagen-form")[0]);
				formData.append("accion", $(this).data("accion"));
				formData.append("id_hotel", $("#id_hotel_fotos").val());
				
				ajaxMediaCenter(formData, "imagen");
			}else{
				alert("formato de archivo invalido");
				return false;
				
			}
			
		}
	});
	$("#nuevo-video-btn").click(function(){
		$("#form-modal-video").modal(function(){
			show:true
		})

	});

	$("#add-video-btn").click(function(){
		if( $('#add-video-form').smkValidate() ){
			var formData = new FormData($("#add-video-form")[0]);
			formData.append("accion", "insert");
			ajaxMediaCenter(formData, "video");
			
		}
	});

	$("#dataTables-imagenes, #dataTables-promos").on("click", ".eliminar-imagen", function(e){
		var data = $(this).data("value");
		$("#delete-item-btn").attr("data-value", JSON.stringify(data));
		$("#delete-item-btn").attr("data-tipo", "imagen");
		$("#delete-modal-item").modal(function(){
			show:true
		});
	});

	$("#dataTables-videos").on("click", ".eliminar-video", function(e){
		var data = $(this).data("value");
		$("#delete-item-btn").attr("data-value", JSON.stringify(data));
		$("#delete-item-btn").attr("data-tipo", "video");
		$("#delete-modal-item").modal(function(){
			show:true
		});
	});

	$("#nueva-promo-btn").click(function(){
		$("#add-imagen-btn").attr("data-accion", "insert-promo");
		$("#tipo-img-content").hide();
		$("#modal-imagen-titulo").text("Subir Promo");
		$("#form-modal-imagen").modal(function(){
			show:true
		});
	});
	
	$("#nueva-noticia-btn").click(function(){
		$("#titulo-form-noticia").text("Nueva noticia");
		$("#add-mod-noticia-btn").attr("data-accion","insert");
		$("#summernote1").code("");
		$('#add-mod-noticia-form').smkClear();
		$("#form-modal-noticia").modal(function(){
			show:true
		});
	});
	

	$("#add-mod-noticia-btn").click(function(){
		if($('#add-mod-noticia-form').smkValidate()){

			if ($(this).data("accion")=="insert") {

				if(!validar_formato_img($("#banner"))){
					alert("formato de imagen invalido");
					return false;
				}else if($("#summernote1").code()==""){
					alert("contenido de la noticia es requerido");
					return false;
				}
	
			}else if ($(this).data("accion")=="update") {

					if ($("#banner").val()!="" && !validar_formato_img($("#banner"))) {
						alert("formato de imagen invalido");
						return false;
					}else if($("#summernote1").code()==""){
						alert("contenido de la noticia es requerido");
						return false;
					}
				}

			var formData = new FormData($("#add-mod-noticia-form")[0]);
			formData.append("contenido",$("#summernote1").code());
			formData.append("accion", $(this).data("accion"));
			ajaxMediaCenter(formData, "noticia");
		}

	});

	$("#dataTables-noticias").on("click", ".lista-categoria-noticia", function(e){
		var data = $(this).data("value");
		console.log(data);
		var formData = new FormData();
		formData.append("accion", "buscar-categoria");
		formData.append("id_noticia", data.id_noticia);
		ajaxBuscarCategoria(formData);
		/*$("#delete-item-btn").attr("data-value", JSON.stringify(data));
		$("#delete-item-btn").attr("data-tipo", "video");
		$("#delete-modal-item").modal(function(){
			show:true
		});*/
	});
	$("#dataTables-noticias").on("click", ".eliminar-noticia", function(e){
		var data = $(this).data("value");
		$("#delete-item-btn").attr("data-value", JSON.stringify(data));
		$("#delete-item-btn").attr("data-tipo", "noticia");
		$("#delete-modal-item").modal(function(){
			show:true
		});
	});

	$("#nueva-img-noticia-btn").click(function(){
		if( $('#form-foto-noticia').smkValidate() ){
			if (validar_formato_img($('#foto'))) {
				var formData = new FormData($("#form-foto-noticia")[0]);
				formData.append("accion", "insert-foto-noticia");
				formData.append("anterior", "");
				ajaxMediaCenter(formData, "noticia");
			}else{
				alert("formato de archivo invalido")
				return false;
				;
			}
			
		}
	});

	$("#dataTables-imagenes-noticias").on("click", ".eliminar-imagen-noticia", function(e){
		var data = $(this).data("value");
		$("#delete-item-btn").attr("data-value", JSON.stringify(data));
		$("#delete-item-btn").attr("data-tipo", "galeria-noticia");
		//$("#delete-item-btn").attr("data-accion", "delete-galeria");
		$("#delete-modal-item").modal(function(){
			show:true
		});
	});

	$("#dataTables-noticias").on("click", ".modificar-noticia", function(e){
		
		var data = $(this).data("value");

		$("#titulo").val(data.titulo);
		$("#subtitulo").val(data.subtitulo);
		//$("#banner").val(data.imagen);
		$("#summernote1").code(data.contenido);
		$("#anterior").val(data.imagen);
		$("#id_noticia").attr("value", data.id_noticia); 
		if (data.ind_activo=="S") {
			$("#activo").prop("checked", true);
		}else{
			$("#activo").prop("checked", false);
		}
		$("#add-mod-noticia-btn").attr("data-accion", "update");
		$("#titulo-form-noticia").text("Modificar Noticia");
		$("#form-modal-noticia").modal(function(){
			show:true
		});
	});
	
});

function ajaxMediaCenter(formData, tipo){
	var url="";
	if (tipo=="brochure") {
		url="https://hoteleshesperia.com.ve/panel/acciones/acciones_mediacenter_brochure.php";
	}else if(tipo=="imagen" || tipo=="promos" || tipo=="galeria-noticia"){
		url="https://hoteleshesperia.com.ve/panel/acciones/acciones_mediacenter_imagenes.php";
	}else if(tipo=="video"){
		url="https://hoteleshesperia.com.ve/panel/acciones/acciones_mediacenter_videos.php";
	}else if(tipo="noticia"){
		url="https://hoteleshesperia.com.ve/panel/acciones/acciones_mediacenter_noticias.php";
	}

	$.ajax({
		type:"POST",
		url:url,
		data:formData,
		contentType:false,
		processData:false,
		cache:false,
		success: function(response){
			if(response=="ok"){
				alert("Procesado Correctamente");
				location.reload();	
			}else{
				alert(response);
			}
			//location.reload();
		},
		error: function(response){
			alert("Ha ocurrido un error, vuelva a intentarlo"+response);
			location.reload();
		}

	});
}

function ajaxBuscarCategoria(formData){

	url="https://hoteleshesperia.com.ve/panel/acciones/buscar_categorias_noticias.php";
	
	$.ajax({
		type:"POST",
		url:url,
		data:formData,
		contentType:false,
		processData:false,
		cache:false,
		success: function(response){
			if(response){
				alert(response);
				console.log(response);
			}else{
				alert("no response");
			}
		}
	});
}

function validar_formato(obj){

        var extension = obj.val().split(".").pop();
        //var arr = [ "jpg", "jpeg", "png"];
        if (extension.toLowerCase().trim()==="pdf") {
            console.log(extension.toLowerCase().trim());
            return true;
        
        }else{
            console.log(extension.toLowerCase().trim());
            return false;
            
        }
        //return jQuery.inArray(extension.strtolower, arr)+extension;      
}

function validar_formato_img(obj){

        var extension = obj.val().split(".").pop();
        //var arr = [ "jpg", "jpeg", "png"];
        if (extension.toLowerCase().trim()==="jpg" || extension.toLowerCase().trim()==="jpeg"
        || extension.toLowerCase().trim()==="png" ) {
            console.log(extension.toLowerCase().trim());
            return true;
        
        }else{
            console.log(extension.toLowerCase().trim());
            return false;
            
        }
        //return jQuery.inArray(extension.strtolower, arr)+extension;      
}

