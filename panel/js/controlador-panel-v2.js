$(document).ready(function(){


	 var maxHeight = 460;          
    $(".equalize").each(function(){
      if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });         
    $(".equalize").height(maxHeight);
    $(".bp-partner-button").hide();



	$("#hotel-container").hide();
	$("#tipo-item-modal").change(function(){
		if ($(this).val()=="habitacion") {
			$("#hotel-container").show();
		}else{
			$("#hotel-container").hide();
		}
	});

	$(".editar").click(function(){

		var texto = $.trim($(this).text());
		var tipo = $(this).data("tipo");
		var codigo = $(this).data("valuetipo");
		var idformulario =  "#form-"+tipo+"-"+codigo;
		$(idformulario+" .caption .oculto").toggleClass("hidden");
		$("#form-"+tipo+"-"+codigo+" .caption .visible-caption").toggle();

		if (texto=="Editar") {
			$(this).text("Cancelar");
		}else{
			$(this).text("Editar");
		}
		//alert($(idformulario).serialize());
	});

	$(".guardar").click(function(){

		var tipo = $(this).data("tipo");
		var codigo = $(this).data("valuetipo");
		var idformulario =  "#form-"+tipo+"-"+codigo;

		var url = $(idformulario+" "+"#url-"+codigo).val();
		var idHotel = $(idformulario+" "+"#idHotel-"+codigo).val();
		var foto = document.getElementById("foto-"+tipo+"-"+codigo);
		var temp_foto=foto.files;
		var item_data= new FormData();

		if(temp_foto[0]=="" || temp_foto[0]==null || temp_foto[0]=="undefined"){
			item_data.append('foto',"");
		}else{
			item_data.append('foto',temp_foto[0]);
		}

		if(url==null || url=="" || url=="undefined"){
			item_data.append('url','');
		}else{
			item_data.append('url', url);
		}

		if(idHotel==null || idHotel=="" || idHotel=="undefined"){
			item_data.append('idHotel','');
		}else{
			item_data.append('idHotel', idHotel);
		}

		if (tipo=="banner") {
			var posicion = $("#posicion-"+codigo).val();
			item_data.append('posicion', posicion);
		};
		var array = $(idformulario+" img").attr("src").split("/");

		item_data.append('anterior', array[5]);
		item_data.append('titulo', $(idformulario+" "+"#titulo-"+codigo).val());
		item_data.append('descripcion', $(idformulario+" "+"#descripcion-"+codigo).val());
		item_data.append('id', $(this).attr("value"));
		item_data.append('tipoItem', tipo);
		item_data.append('transaccion','update');

		if($(idformulario+" "+"#activo-"+codigo).is(':checked')){
			item_data.append('activo', 'S');
		}else{
			item_data.append('activo', 'N');
		}
		
		$.ajax({

			type:"POST",
			url:"https://hoteleshesperia.com.ve/panel/include/admin_acciones_index.php",
			data:item_data,
			contentType:false,
		 	processData:false,
			cache:false,
			success: function(response){
				if(response=="ok"){
					alert("Modificado Correctamente");
					location.reload();
				}else{
					alert("Ha ocurrido un error, vuelva a intentarlo 1"+response);
					location.reload();
				}
			},

			error: function(response){
				alert("Ha ocurrido un error, vuelva a intentarlo 2"+response);
			}

		});
	});


	$("#aceptar-nuevo-item").click(function(){
		var titulo = $("#titulo-modal").val();
		var descripcion = $.trim($("#descripcion-modal").val());
		var foto = document.getElementById("foto-item-modal");
		var temp_foto=foto.files;
		var tipoItem =$("#tipo-item-modal").val();
		var idHotel = $("#idHotel").val();
		var url = $("#url-modal").val();

		var errores="";
		var item_data= new FormData();

		if(tipoItem=="habitacion"){
			if (titulo==null || titulo=="") {
				errores+="Titulo es obligatorio \n ";

			}else if (descripcion==null || descripcion=="") {
				errores+="Descripcion es obligatorio \n ";

			}else if(url==null || url==""){
				errores+="url es obligatorio \n ";				
			}else if(temp_foto[0]==null || temp_foto[0]=="" || temp_foto[0]=="undefined"){
				errores+="foto es obligatoria \n";
			}

		}else if(tipoItem=="banner"){

			if(temp_foto[0]==null || temp_foto[0]=="" || temp_foto[0]=="undefined"){
				errores+="foto es obligatoria \n";
			}
		}

		if(errores==""){

				var item_data= new FormData();
				item_data.append("descripcion",descripcion);
				item_data.append("foto",temp_foto[0]);
				item_data.append("idHotel", idHotel);
				item_data.append("titulo",titulo);
				item_data.append('tipoItem',tipoItem);
				item_data.append('url',url);
				item_data.append('transaccion','insert');

				item_data.append('id','');
				item_data.append('anterior','');
				item_data.append('activo','');
				
				$.ajax({

					type:"POST",
					url:"https://hoteleshesperia.com.ve/panel/include/admin_acciones_index.php",
					data:item_data,
					contentType:false,
				 	processData:false,
					cache:false,
					success: function(response){
						if(response=="ok"){
							alert("Guardado Correctamente");
							location.reload();
						}else{
							alert("Ha ocurrido un error, vuelva a intentarlo"+response);
						}
					},

					error: function(response){
						alert("Ha ocurrido un error, vuelva a intentarlo"+response);
					}
				});	
		}else{
			alert(errores);
		}
	});

	$(".eliminar").click(function(){

		$("#aceptar-delete-item").attr("value", $(this).attr("value"));
		$("#aceptar-delete-item").attr("data-anterior", $(this).data("anterior"));
		$("#aceptar-delete-item").attr("data-tipo", $(this).data("tipo"));
	
	});

	$("#aceptar-delete-item").click(function(){
		var tipoItem = $(this).attr("data-tipo");
		var id = $(this).attr("value");
		var anterior = $(this).attr("data-anterior");

		//alert(anterior+"-"+tipoItem);

		var item_data= new FormData();
		item_data.append("titulo","");
		item_data.append("descripcion","");
		item_data.append("foto","");
		item_data.append("idHotel", "");
		item_data.append("posicion","");
		item_data.append('tipoItem',tipoItem);
		item_data.append('url',"");
		item_data.append('transaccion','delete');
		item_data.append('anterior',anterior);
		item_data.append('activo','');
		item_data.append("id",id);


		$.ajax({

			type:"POST",
			url:"https://hoteleshesperia.com.ve/panel/include/admin_acciones_index.php",
			data:item_data,
			contentType:false,
				processData:false,
			cache:false,
			success: function(response){
				if(response=="ok"){
					alert("Guardado Correctamente");
					location.reload();
				}else{
					alert("Ha ocurrido un error, vuelva a intentarlo"+response);
				}
			},

			error: function(response){
				alert("Ha ocurrido un error, vuelva a intentarlo"+response);
			}
		});	
	});
});