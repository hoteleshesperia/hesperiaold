function InstapagoSettings() {
    var str, ruta=document.InstapagoSetting, error="";
    if(ruta.keyid.value == '')
    { error += "Debe escribir Keyid \n";}
    if(ruta.publickeyid.value == '')
    { error += "Debe escribir Publickeyid  \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#InstapagoSetting").serialize(),
        url:'include/admin_instapago.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("admin_instapago").innerHTML=resultado;
}

function FormPorcentajes() {
    var str, ruta=document.Porcentajes, error="";
    if(ruta.adultos.value == '')
    { error += "Debe incluir el % para adultos \n";}
    if(ruta.adultos_2.value == '')
    { error += "Debe incluir el % para 2 adultos \n";}
    if(ruta.adultuos_3.value == '')
    { error += "Debe incluir el % para 3 adultos \n";}
    if(ruta.adultos_4.value == '')
    { error += "Debe incluir el % para 4 adultos \n";}
    if(ruta.nino1.value == '')
    { error += "Debe incluir el % para 1 niño \n";}
    if(ruta.nino2.value == '')
    { error += "Debe incluir el % para 2 niños \n";}
    if(ruta.nino3.value == '')
    { error += "Debe incluir el % para 3 niños \n";}
    if(ruta.minima_edad_nino.value == '')
    { error += "Debe incluir la edad minima del niño \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#Porcentajes").serialize(),
        url:'include/admin_porcentajes.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("PorcentajesRespuesta").innerHTML=resultado;
}
function Recuperar()
{ var str, ruta = document.FormRecupera, error="";
    if(ruta.email.value == "")
   { error += "Indicar su email de registro \n";}
  if(ruta.email.value != "") { str = ruta.email.value;
     if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))
        error +="Formato de dirección de e-mail inválida\n"; }
  if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
  var resultado = $.ajax({
  type: "POST",
  data: $("#FormRecupera").serialize(),
  url: 'include/recupera.php',
  dataType: 'text', async:false
 }).responseText;
 document.getElementById("RecuperaClave").innerHTML = resultado;
}
function RespContacto() {
    var str, ruta=document.FormRespuestaContacto, error="";
    if(ruta.mensajerespuesta.value == '')
    { error += "Debe escribir un mensaje de respuesta \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#FormRespuestaContacto").serialize(),
        url:'include/respuesta_mensaje.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("RespuestaMensajeEnvio").innerHTML=resultado;
}
function PrecioHabPost() {
    var  ruta=document.CamHabPost, error="";
    if(ruta.precio.value == "" || ruta.precio.value <= 0)
    { error += "Debe indicar un precio a la Habitacion\n";}
    if(ruta.precio_promocion.value == "" || ruta.precio_promocion.value <= 0)
    { error += "Debe indicar un precio a la Habitacion\n";}
    if(ruta.fecha_precio.value == "")
    { error += "Debe indicar la fecha\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#CamHabPost").serialize(),
        url:'include/admin_cambio_precio_post.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("CambioPrecioHabPost").innerHTML=resultado;
}

function FormNosotrosV(){
    var ruta=document.FormNosotros, error="";
   if(ruta.texto_nosotros.value == "" || ruta.texto_nosotros.value == "")
    { error += "Debe ingresar un texto \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function ValidarNosotros(){
    var ruta=document.NosotrosSetting, error="";
    alert(ruta.quienes.value );
   if(ruta.quienes.value == "")
    { error += "Debe ingresar un texto \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function InfoGralHotelSelectValida(){
    var ruta=document.FormInfoGralHotelSelectValida, error="";
   if(ruta.id_hotel.value == "0")
    { error += "Debe seleccionar un hotel... \n";}
    if(ruta.tipo_paquete.value == "0")
    { error += "Debe seleccionar tipo paquete... \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function InfoValidaNoticias(){
    var ruta=document.InfoValidaNoticia, error="";
   if(ruta.noticia_id.value == "0")
    { error += "Debe seleccionar una nota de prensa... \n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function Ingreso() {
    var str, ruta=document.FormIngreso, error="";
    if(ruta.username.value == "")
    { error += "Debe ingresar su email \n";}
    if(ruta.clave.value == "")
    { error += "Debe ingresar clave de acceso \n";}
    if(ruta.username.value != ""){ str=ruta.username.value;
         if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i)){
         error += "Formato email invalido\n"; }}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#FormIngreso").serialize(),
        url:'include/acceso.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("InformacionAcceso").innerHTML=resultado;
}
function AgregarPublicaciones() {
    var ruta=document.Publicaciones, error="";
   if(ruta.titulo.value == "")
    { error += "Debe agregar un titulo\n";}
    if(ruta.intro.value == "")
   { error += "Debe agregar una introduccion al texto\n";}
    if(ruta.contenido.value == "")
  { error += "Debe agregar texto de contenido\n";}
 if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarEvento() {
    var str, ruta=document.FormAgregarEvento, error="";
    if(ruta.id_hotel.value == "")
    { error += "Debe seleccionar el hotel\n";}
    if(ruta.id_salon.value == "")
    { error += "Debe seleccionar un salon\n";}
    if(ruta.nombre_evento.value == "")
    { error += "Debe indicar el nombre del evento\n";}
    if(ruta.fecha_evento.value == "")
    { error += "Debe indicar la fecha del evento\n";}
    if(ruta.hora_evento.value == "")
    { error += "Debe indicar la hora del evento\n";}
    if(ruta.precio_evento.value == "")
    { error += "Debe indicar el precio del evento\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarContenidoEvento() {
    var str, ruta=document.FormAgregarContEvento, error="";
    if(ruta.titulo_detalle.value == "")
    { error += "Debe indicar el Titulo del Item\n";}
    if(ruta.descripcion_detalle.value == "")
    { error += "Debe indicar la Descripcion del Item\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function EditarEvento() {
    var str, ruta=document.FormEditarEvento, error="";
    if(ruta.id_hotel.value == "")
    { error += "Debe seleccionar el hotel\n";}
    if(ruta.id_salon.value == "")
    { error += "Debe seleccionar un salon\n";}
    if(ruta.nombre_evento.value == "")
    { error += "Debe indicar el nombre del evento\n";}
    if(ruta.fecha_evento.value == "")
    { error += "Debe indicar la fecha del evento\n";}
    if(ruta.hora_evento.value == "")
    { error += "Debe indicar la hora del evento\n";}
    if(ruta.precio_evento.value == "")
    { error += "Debe indicar el precio del evento\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function EditarContenidoEvento() {
    var str, ruta=document.FormEditarConteEvento, error="";
    if(ruta.nombre_evento.value == "")
    { error += "Debe indicar el Titulo del Item\n";}
    if(ruta.texto_evento.value == "")
    { error += "Debe indicar la Descripcion del Item\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function PrecioHab() {
    var  ruta=document.CamHab, error="";
    if(ruta.precio.value == "" || ruta.precio.value <= 0)
    { error += "Debe indicar un precio a la Habitacion\n";}
    if(ruta.precio_promocion.value == "" || ruta.precio_promocion.value <= 0)
    { error += "Debe indicar un precio a la Habitacion\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#CamHab").serialize(),
        url:'include/admin_cambio_precio.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("CambioPrecioHab").innerHTML=resultado;
}
function AgregarSlider(){
    var str, ruta=document.sube, error="";
//    if(ruta.id_hotel.value == "")
//    { error += "Debe seleccionar el hotel\n";}
//    if(ruta.ubicacion.value == "")
//    { error += "Debe seleccionar la ubicacion\n";}
    if(ruta.texto.value == "")
    { error += "Debe escribir un texto\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function EliminarSlider(enlace)
{ var resultado = $.ajax({
  type: "POST",
  data: 'id='+enlace,
  url: 'include/elimina_imagen_slider.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaSlider").innerHTML = resultado;
}
function CambiarUbicacionSlider(enlace)
{ var resultado = $.ajax({
  type: "POST",
  data: 'id='+enlace,
  url: 'include/cambiar_ubicacion_imagen_slider.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaSlider").innerHTML = resultado;
}
function CambiarUbicacion()
{ var resultado = $.ajax({
  type: "POST",
  data:$("#cambiar_ubicacion_slider").serialize(),
  url: 'include/cambiando_ubicacion_imagen_slider.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaSlider").innerHTML = resultado;
}
function EliminarRestaurant(enlace)
{ var resultado = $.ajax({
  type: "POST",
  data: 'id='+enlace,
  url: 'include/elimina_restaurant.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespEliminaRestaurant").innerHTML = resultado;
}
function VerificaFechas() {
    var ruta=document.FormFechaPrecio, error="";
    var params = "";
    var i = 0;
    $("input:checkbox").each(function(){
    //cada elemento seleccionado  
        params = params + "&param" + i + "=";  
        if($(this).is(":checked")){
           params = params + $(this).val();

        }
        i++;
    });
    //alert(params);

    if(ruta.id_hab.value == "" || ruta.id_hab.value == 0)
    { error += "Debe Indicar una habitacion\n";}
    if(ruta.fecha_inicio.value > ruta.fecha_final.value )
    { error += "La fecha de inicio no puede ser mayor que la fecha final\n";}
    if(ruta.fecha_inicio.value < ruta.eshoy.value )
    { error += "La fecha de inicio no puede ser anterior a Hoy\n";}
    if(ruta.nombre_hotel.value == "")
    { error += "Debe Indicar un Hotel\n";}
    if(ruta.nuevo_precio.value == 0)
    { error += "El nuevo precio no puede ser 0 (cero)\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    if(ruta.disponibles.value < 0)
    { error += "Debe indicar cuantas habitaciones disponibles\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }

var resultado = $.ajax({
  type: "POST",
   data:$("#FormFechaPrecio").serialize()+params,
  url: 'include/admin_nueva_post_precio.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("PreciosPorFecha").innerHTML = resultado;
}
function EditarRestaurant() {
    var ruta=document.FormEditarRestaurant, error="";
    if(ruta.nombre_restaurant.value == "")
    { error += "Debe Indicar el nombre del restaurant\n";}
    if(ruta.tipo_comida.value == "")
    { error += "Debe Indicar el tipo de comida\n";}
    if( ruta.caracteristicas.value == '' ||
         ruta.caracteristicas.value == '<p><br></p>')
    { error += "Debe Indicar las caracteristicas del restaurant\n";}
    if(ruta.hora_apertura.value == "")
    { error += "Debe Indicar hora de apertura\n";}
    if(ruta.hora_cierre.value == "")
    { error += "Debe Indicar hora de cierre\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarRestaurant() {
    var ruta=document.FormAgregarRestaurant, error="";
    if(ruta.id_hotel.value == "0")
    { error += "Indicar hotel\n";}
    if(ruta.nombre_restaurant.value == "")
    { error += "Debe Indicar el nombre del restaurant\n";}
    if(ruta.tipo_comida.value == "")
    { error += "Debe Indicar el tipo de comida\n";}
    if( ruta.caracteristicas.value == '' ||
         ruta.caracteristicas.value == '<p><br></p>')
    { error += "Debe Indicar las caracteristicas del restaurant\n";}
    if(ruta.hora_apertura.value == "")
    { error += "Debe Indicar hora de apertura\n";}
    if(ruta.hora_cierre.value == "")
    { error += "Debe Indicar hora de cierre\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function ContenidoValidar() {
    var ruta=document.FormEditarContenido, error="";
    if(ruta.id_hotel.value == 0)
    { error += "Indicar hotel\n";}
    if(ruta.categoria.value == 0)
    { error += "Debe Indicar la categoria\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarExperiencias() {
    var  ruta=document.FormAgregarExperiencias, error="";
    if(ruta.id_hotel.value == "0")
    { error += "Debe Seleccionar el hotel\n";}
    if(ruta.nombre_expe.value == "")
    { error += "Debe Indicar el nombre de la experiencia\n";}
    if(ruta.experiencia.value == "")
    { error += "Debe Indicar la descripcion de la experiencia\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarPaquetes() {
    var str, ruta=document.FormAgregarPaquetes, error="";
    if(ruta.nombre_paquete.value == "")
    { error += "Debe Indicar el nombre del paquete\n";}
    if(ruta.paquete.value == "")
    { error += "Debe Indicar la descripcion del paquete\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarApertura() {
    var str, ruta=document.FormAgregarApertura, error="";
    if(ruta.titulo_apertura.value == "")
    { error += "Debe Indicar el Titulo de la Apertura\n";}
    if(ruta.texto_apertura.value == "")
    { error += "Debe Indicar el Texto descriptivo de la Apertura\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarSalones() {
    var str, ruta=document.FormAgregarSalones, error="";
    if(ruta.id_hotel.value == "0")
    { error += "Debe seleccionar el hotel\n";}
    if(ruta.nombre_Salon.value == "")
    { error += "Debe Indicar el nombre del Salón\n";}
    if(ruta.caracteristicas.value == "")
    { error += "Debe Indicar la descripción del Salón\n";}
    if(ruta.imagen.value == "")
    { error += "Debe subir una imagen\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregandoPortada() {
    var str, ruta=document.FormAgregarPortada, error="";
    if(ruta.titulo_principal.value == "")
    { error += "Debe Indicar el titulo principal\n";}
    if(ruta.titulo_UNO.value == "")
    { error += "Debe Indicar un texto en titulo destacado 1\n";}
    if(ruta.titulo_DOS.value == "")
    { error += "Debe Indicar un texto en titulo destacado 2\n";}
    if(ruta.titulo_TRES.value == "")
    { error += "Debe Indicar un contenido en titulo destacado 2\n";}
    if(ruta.texto_UNO.value == "")
    { error += "Debe Indicar una descripcion en destacado 1\n";}
    if(ruta.texto_DOS.value == "")
    { error += "Debe Indicar una descripcion en destacado 2\n";}
    if(ruta.texto_TRES.value == "")
    { error += "Debe Indicar una descripcion en destacado 2\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregandoPortadaEventos() {
    var str, ruta=document.FormAgregarPortadaEventos, error="";
    if(ruta.titulo_principal.value == "")
    { error += "Debe Indicar el titulo principal\n";}
    if(ruta.titulo_UNO.value == "")
    { error += "Debe Indicar un texto en titulo destacado 1\n";}
    if(ruta.titulo_DOS.value == "")
    { error += "Debe Indicar un texto en titulo destacado 2\n";}
    if(ruta.titulo_TRES.value == "")
    { error += "Debe Indicar un contenido en titulo destacado 2\n";}
    if(ruta.texto_UNO.value == "")
    { error += "Debe Indicar una descripcion en destacado 1\n";}
    if(ruta.texto_DOS.value == "")
    { error += "Debe Indicar una descripcion en destacado 2\n";}
    if(ruta.texto_TRES.value == "")
    { error += "Debe Indicar una descripcion en destacado 2\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function AgregarDistancias() {
    var str, ruta=document.FormAgregarDistancia, error="";
    if(ruta.nombre_aeropuerto.value == "")
    { error += "Debe indicar el Nombre del Aeropuerto\n";}
    if(ruta.nombre_aeropuerto.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Aeropuerto\n";}
    if(ruta.nombra_parque.value == "")
    { error += "Debe indicar Nombre del Parque\n";}
    if(ruta.parques_nacionales.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Parque nacional\n";}
    if(ruta.centro_ciudad.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Centro de la Ciudad\n";}
    if(ruta.museo.value == "")
    { error += "Debe indicar Nombre del Museo\n";}
    if(ruta.nombre_museo.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Museo\n";}
    if(ruta.metro.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Metro\n";}
    if(ruta.nombre_estacion.value == "")
    { error += "Debe indicar Nombre de la estación del metro\n";}
    if(ruta.playas.value == "")
    { error += "Distancia desde el Hotel hasta las Playas más cercanas\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#FormAgregarDistancia").serialize(),
        url:'include/admin_distancias.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("InformacionAgregarDistancia").innerHTML=resultado;
}
function ActualizaDistancias() {
    var str, ruta=document.FormAgregarDistancia, error="";
    if(ruta.nombre_aeropuerto.value == "")
    { error += "Debe indicar el Nombre del Aeropuerto\n";}
    if(ruta.nombre_aeropuerto.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Aeropuerto\n";}
    if(ruta.nombra_parque.value == "")
    { error += "Debe indicar Nombre del Parque\n";}
    if(ruta.parques_nacionales.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Parque nacional\n";}
    if(ruta.centro_ciudad.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Centro de la Ciudad\n";}
    if(ruta.museo.value == "")
    { error += "Debe indicar Nombre del Museo\n";}
    if(ruta.nombre_museo.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Museo\n";}
    if(ruta.metro.value == "")
    { error += "Debe indicar Distancia desde el Hotel hasta el Metro\n";}
    if(ruta.nombre_estacion.value == "")
    { error += "Debe indicar Nombre de la estación del metro\n";}
    if(ruta.playas.value == "")
    { error += "Distancia desde el Hotel hasta las Playas más cercanas\n";}
    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
    var resultado=$.ajax({
        type:"POST",
        data:$("#FormAgregarDistancia").serialize(),
        url:'include/admin_distancias.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("InformacionAgregarDistancia").innerHTML=resultado;
}
function AgregaHabitacion() {
    var str, ruta=document.RegistroA, error="";
    if(ruta.nombre_habitacion.value == "")
    { error += "Ingresar Nombre de la habitacion \n";}
    if(ruta.precio.value == "")
    { error += "Ingresar Precio de la habitacion \n";}
    if(ruta.precio_promocion.value == "")
    { error += "Ingresar Precio promocional de la habitacion \n";}
    if(ruta.cantidad_hab.value == "")
    { error += "Ingresar la cantidad de habitaciones \n";}
    if(ruta.disponibles.value == "")
    { error += "Ingresar la cantidad de habitaciones disponibles \n";}
    if(ruta.tamano_hab.value == "")
    { error += "Ingresar tamaño de la habitacion \n";}
    if(ruta.tipo_habitacion.value == "")
    { error += "Indique si la habitacion es Matrimonial, sencilla o doble \n";}
    if(ruta.tipo_cama.value == "")
    { error += "Indique el tipo de cama \n";}
    if(ruta.cama_adicional.value == "")
    { error += "Indique si se permite cama adicional \n";}
    if(ruta.capacidad_personas.value == "")
    { error += "Indique la capacidad de personas \n";}
    if(ruta.mini_bar.value == "")
    { error += "Indique si tiene mini bar \n";}
    if(ruta.albornoz.value == "")
    { error += "Indique si tiene albornoz \n";}
    if(ruta.telefono.value == "")
    { error += "Indique si tiene telefono \n";}
    if(ruta.control_remoto_tv.value == "")
    { error += "Indique si tiene control remoto tv \n";}
    if(ruta.tv.value == "")
    { error += "Indique si tiene tv \n";}
    if(ruta.aire_acondicionado.value == "")
    { error += "Indique si tiene aire acondicionado \n";}
    if(ruta.servicio_habitacion.value == "")
    { error += "Indique si tiene servicio a la habitacion \n";}
    if(ruta.desayuno.value == "")
    { error += "Indique si tiene desayuno \n";}
    if(ruta.ambiente_musical.value == "")
    { error += "Indique si tiene ambiente musical \n";}
    if(ruta.secador_pelo.value == "")
    { error += "Indique si tiene secador de cabello \n";}
    if(ruta.lavanderia.value == "")
    { error += "Indique si tiene lavanderia \n";}
    if(ruta.caja_fuerte.value == "")
    { error += "Indique si tiene caja fuerte \n";}
    if(ruta.tv_cable.value == "")
    { error += "Indique si tiene tv por cable \n";}
    if(ruta.jacuzzi.value == "")
    { error += "Indique si tiene Jacuzzi \n";}
    if(ruta.area_estar.value == "")
    { error += "Indique si tiene area de estar \n";}
    if(ruta.jarra_agua_vasos.value == "")
    { error += "Indique si tiene Jarra de agua y vasos \n";}
    if(ruta.cafetera.value == "")
    { error += "Indique si tiene cafetera \n";}
    if(ruta.servicio_medico.value == "")
    { error += "Indique si tiene medico \n";}
    if(ruta.balcon.value == "")
    { error += "Indique si tiene balcon \n";}
    if(ruta.banera.value == "")
    { error += "Indique si tiene bañera \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
}
function RevGaleria(enlace,id_hotel)
{ var resultado = $.ajax({
  type: "POST",
  data: 'enlace='+enlace+'='+id_hotel,
  url: 'include/elimina_imagen_galeria.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaGaleria").innerHTML = resultado;
}
function DesVideo(enlace)
{ var resultado = $.ajax({
  type: "POST",
  data: 'id='+enlace,
  url: 'include/deshabilitar_video.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaVideo").innerHTML = resultado;
}
function HabVideo(enlace)
{ var resultado = $.ajax({
  type: "POST",
  data: 'id='+enlace,
  url: 'include/habilitar_video.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaVideo").innerHTML = resultado;
}
function EliminarUsuario()
{ var resultado = $.ajax({
  type: "POST",
  data:$("#FormEUsuario").serialize(),
  url: 'include/elimina_usuario.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("CambiodatosUsuario").innerHTML = resultado;
}
function AgregarVideo()
{  var ruta=document.FormVideo, error="";
    if(ruta.video_promocion.value == "")
    { error += "Debe indicar el enlace del video\n";}
    if(ruta.id_hotel.value == "")
    { error += "Debe Seleccionar a cual Hotel le pertenece el video\n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
  var resultado = $.ajax({
  type: "POST",
  data:$("#FormVideo").serialize(),
  url: 'include/Admin_agregar_video.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaVideoAgregar").innerHTML = resultado;
}
function EliminarLogs()
{  var ruta=document.FormLogs, error="";
    if(ruta.limpiar.value == "0")
    { error += "Seleccione su opción \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
  var resultado = $.ajax({
  type: "POST",
  data:$("#FormLogs").serialize(),
  url: 'include/elimina_logs.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestaLimpLogs").innerHTML = resultado;
}
function ActualizaPreciosHabitacion()
{  var ruta=document.CambioPrecioHabitacion, error="";
    if(ruta.precio.value == "")
    { error += "Debe indicar el precio de la habitacion \n";}
    if(ruta.precio_promocion.value == "")
    { error += "Debe indicar el precio promocional de la habitacion \n";}
    if(ruta.dp1.value == "")
    { error += "Debe indicar la fecha  \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
  var resultado = $.ajax({
  type: "POST",
  data:$("#CambioPrecioHabitacion").serialize(),
  url: 'include/admin_actualiza_precios_h.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("CambiarPrecioHabitacion").innerHTML = resultado;
}
function EliminarDescargas()
{   var str, ruta=document.DescargasE, error="";
    if(ruta.id.value == "0")
    { error += "Seleccione su opción \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
    var resultado = $.ajax({
  type: "POST",
  data:$("#DescargasE").serialize(),
  url: 'include/elimina_archivos_media.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("EliminacionArchivosMedia").innerHTML = resultado;
}
function FormSettings() {
    var ruta=document.hesperia_settings, error="";
    if(ruta.razon_social.value == "")
    { error += "Debe indicar Razon Social \n";}
    if(ruta.nombre_sitio.value == "")
    { error += "Debe indicar nombre del sitio \n";}
    if(ruta.rif.value == "")
    { error += "Indique el RIF \n";}
    if(ruta.dominio.value == "")
    { error += "Indique Dominio \n";}
    if(ruta.direccion.value == "")
    { error += "Debe indicar la dirección principal \n";}
    if(ruta.descripcion.value == "")
    { error += "Indique una descripcion del Hotel \n";}
    if(ruta.ciudad.value == "")
    { error += "Indique ciudad de ubicacion \n";}
    if(ruta.estado.value == "")
    { error += "Indique estado de ubicacion \n";}
    if(ruta.pais.value == "")
    { error += "Indique Pais \n";}
    if(ruta.latitud.value == "")
    { error += "Indique las coordenadas de latitud \n";}
    if(ruta.longitud.value == "")
    { error += "Indique coordenadas de longitud \n";}
    if(ruta.postal.value == "")
    { error += "Indique zona postal \n";}
    if(ruta.telefonos.value == "")
    { error += "Indique telefono(s)  \n";}
    if(ruta.telf_reserva.value == "")
    { error += "Indique telefono(s) para reservar \n";}
    if(ruta.check_in.value == "")
    { error += "Indique hora de check in \n";}
    if(ruta.check_out.value == "")
    { error += "Indique hora de check out \n";}
    if(ruta.correo_envio.value == "")
    { error += "Debe indicar el correo de envio \n";}
    if(ruta.correo_contacto.value == "")
    { error += "Indique correo de contacto \n";}
    if(ruta.cant_habitaciones.value == "" || ruta.cant_habitaciones.value == "0")
    { error += "Debe indicar la cantidad de habitaciones \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
    var resultado=$.ajax({
        type:"POST",
        data:$("#hesperia_settings").serialize(),
        url:'include/update_settings.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("RespSetting").innerHTML=resultado;
}
function FormAgregarSettings() {
    var ruta=document.hesperia_nuevo_settings, error="";
    if(ruta.razon_social.value == "")
    { error += "Debe indicar Razon Social \n";}
    if(ruta.nombre_sitio.value == "")
    { error += "Debe indicar nombre del sitio \n";}
    if(ruta.rif.value == "")
    { error += "Indique el RIF \n";}
    if(ruta.dominio.value == "")
    { error += "Indique Dominio \n";}
    if(ruta.direccion.value == "")
    { error += "Debe indicar la dirección principal \n";}
    if(ruta.descripcion.value == "")
    { error += "Indique una descripcion del Hotel \n";}
    if(ruta.ciudad.value == "")
    { error += "Indique ciudad de ubicacion \n";}
    if(ruta.estado.value == "")
    { error += "Indique estado de ubicacion \n";}
    if(ruta.pais.value == "")
    { error += "Indique Pais \n";}
    if(ruta.latitud.value == "")
    { error += "Indique las coordenadas de latitud \n";}
    if(ruta.longitud.value == "")
    { error += "Indique coordenadas de longitud \n";}
    if(ruta.postal.value == "")
    { error += "Indique zona postal \n";}
    if(ruta.telefonos.value == "")
    { error += "Indique telefono(s)  \n";}
    if(ruta.telf_reserva.value == "")
    { error += "Indique telefono(s) para reservar \n";}
    if(ruta.check_in.value == "")
    { error += "Indique hora de check in \n";}
    if(ruta.check_out.value == "")
    { error += "Indique hora de check out \n";}
    if(ruta.correo_envio.value == "")
    { error += "Debe indicar el correo de envio \n";}
    if(ruta.correo_contacto.value == "")
    { error += "Indique correo de contacto \n";}
    if(ruta.cant_habitaciones.value == "" || ruta.cant_habitaciones.value == "0")
    { error += "Debe indicar la cantidad de habitaciones \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
    var resultado=$.ajax({
        type:"POST",
        data:$("#hesperia_nuevo_settings").serialize(),
        url:'include/admin_agrega_hotel.php',
        dataType:'text',
        async:false
    }).responseText;
    document.getElementById("RespAgregaSetting").innerHTML=resultado;
}
function AgregarDescargas()
{  var ruta=document.Descargas, error="";
    if(ruta.tipo.value == "")
    { error += "Debe el tipo de descarga\n";}
    if(ruta.titulo.value == "")
    { error += "Debe Indicar titulo de la descarga\n";}
    if(ruta.contenidovalue == "")
    { error += "Debe Indicar la descripcion\n";}
    if(ruta.tipo.value == "0" && ruta.enlace.value == "")
    { error += "Debe Indicar el enlace\n";}
    if(ruta.tipo.value != "0" && ruta.enlace.value != "")
    { error += "El enlace es solo para video\n";}

    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }
}
function CambioUsuario() {
    var str, ruta=document.FormUsuario, error="";
    if(ruta.email.value == "")
    { error += "Ingresar email \n";}
    if(ruta.apellidos.value == "")
    { error += "Indicar Apellido \n";}
    if(ruta.nombres.value == "")
    { error += "Indicar Nombre \n";}
    if(ruta.telefono.value == "")
    { error += "Indicar su telefono\n";}
    if(ruta.email.value != ""){
        str=ruta.email.value;
        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))
            error +="Formato de dirección de e-mail inválida\n"; }
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
    var resultado=$.ajax({
        type:"POST",
        data:$("#FormUsuario").serialize(),
        url:'include/Admin_cambio_usuario.php',
        dataType:'text',async:false
    }).responseText;
    document.getElementById("CambiodatosUsuario").innerHTML=resultado;
}
function ContenidoValidarTexto1()
{   var  ruta=document.FormEXContenido, error="";
    if(ruta.texto_UNO.value == 0)
    { error += "No puede estar vacio el contenido \n";}
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
}
function ValidarServicios()
{   var  ruta=document.FormActualizaServicios, error="";
    if( ruta.texto.value == '' ||
         ruta.texto.value == '<p><br></p>')
    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}
    var resultado = $.ajax({
  type: "POST",
  data:$("#FormActualizaServicios").serialize(),
  url: 'include/admin_agrega_servicios.php',
  dataType: 'text',
  async:false
 }).responseText;
 document.getElementById("RespuestContenido").innerHTML = resultado;
}

function confirmarReserva(id, monto){
   
    formData = new FormData();
    formData.append('id', id);
    //formData.append('enviar', "0");

    setTimeout(function(){
                 var resultado=$.ajax({
                    type:"POST",
                    data:formData,
                    url:'https://hoteleshesperia.com.ve/panel/include/enviarReservaVal.php',
                    contentType:false,
                    processData:false,
                    cache:false,
                    async:false
                }).responseText;

                alert(resultado);
                if(resultado=="validacion exitosa, Enviado al departamento de reservas para su registro"){
                    //alert("fino");
                    ga('send', 'event', 'tangible-trf', 'aprob-reserva-hab', id, monto);
                }else{
                    //alert("fallo");
                }

            }, 2000);
}

function confirmarReservaBitPagos(id, monto){
   
    formData = new FormData();
    formData.append('id', id);
    //formData.append('enviar', "0");

    setTimeout(function(){
                 var resultado=$.ajax({
                    type:"POST",
                    data:formData,
                    url:'https://hoteleshesperia.com.ve/panel/include/enviarReservaValBitPagos.php',
                    contentType:false,
                    processData:false,
                    cache:false,
                    async:false
                }).responseText;

                alert(resultado);
                if(resultado=="validacion exitosa, Enviado al departamento de reservas para su registro"){
                    //alert("fino");
                    ga('send', 'event', 'tangible-trf', 'aprob-reserva-hab', id, monto);
                }else{
                    //alert("fallo");
                }

            }, 2000);
}

function confirmarReservaPaqBitPagos(id, monto){
   
    formData = new FormData();
    formData.append('id', id);
    //formData.append('enviar', "0");

    setTimeout(function(){
                 var resultado=$.ajax({
                    type:"POST",
                    data:formData,
                    url:'https://hoteleshesperia.com.ve/panel/include/enviarReservaPaqValBitPagos.php',
                    contentType:false,
                    processData:false,
                    cache:false,
                    async:false
                }).responseText;

                alert(resultado);
                if(resultado=="validacion exitosa, Enviado al departamento de reservas para su registro"){
                    //alert("fino");
                    ga('send', 'event', 'tangible-trf', 'aprob-reserva-hab', id, monto);
                }else{
                    //alert("fallo");
                }

            }, 2000);
}

function confirmarReservaPaq(id,monto){
    formData = new FormData();
    formData.append('id', id);
    //formData.append('enviar', "0");
    setTimeout(function(){
                 var resultado=$.ajax({
                    type:"POST",
                    data:formData,
                    url:'https://hoteleshesperia.com.ve/panel/include/enviarReservaPaqVal.php',
                    contentType:false,
                    processData:false,
                    cache:false,
                    async:false
                }).responseText;
                 alert(resultado);
                 if(resultado=="validacion exitosa, Enviado al departamento de reservas para su registro"){
                    //alert("fino");
                    ga('send', 'event', 'tangible-trf', 'aprob-reserva-pag', id, monto);
                }else{
                    //alert("fallo");
                }
                
            }, 2000);
}
