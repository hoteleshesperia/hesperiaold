<?php
/*
Script  bajo los términos y Licencia
GNU GENERAL PUBLIC LICENSE
Ver Terminos en:
http://www.gnu.org/copyleft/gpl.html
Traducción al español:
http://gugs.sindominio.net/gnu-gpl/gples.html
Autor: Hector A. Mantellini (Xombra)
*/
$imagen=$_GET["imagen"];
$valor=explode("-",strip_tags(trim($imagen)));

if (count($valor) == 2) {
$imagen = $valor[0];
$opcion = $valor[1]; }
else
{$imagen = $valor[0].'-'.$valor[1];
$opcion = $valor[2];  }


# Imagen Youtube de Video
if ($opcion ==16)
{	$directorio = 'https://img.youtube.com/vi';
    $nombre = $directorio.'/'.$imagen.'/0.jpg';
    $anchura  = 250;
    $maxima_y = 250;
}

if (!isset($imagen) )
    { die("Error no ha declarado una imagen");}

$datos = getimagesize($nombre);
switch ($datos[2]) {
    case 1:
        $img = imagecreatefromgif($nombre);
        break;
    case 2:
        $img = imagecreatefromjpeg($nombre);
        break;
    case 3:
        $img = imagecreatefrompng($nombre);
        break;
  default:
   die("Tipo de Imagen no valida");
}
$ratio  = $datos[0] / $anchura;
$altura = $datos[1] / $ratio;
if($altura >= $maxima_y)
   { $anchura2 = $maxima_y * $anchura / $altura;
     $altura   = $maxima_y;
     $anchura  = $anchura2;
   }
$thumb = imagecreatetruecolor($anchura,$altura);
$negro = imagecolorallocate($thumb, 0, 0, 0);
imagecolortransparent($thumb, $negro);
imagecolorallocatealpha($thumb, 255, 255, 255, 127);
imagecopyresampled($thumb, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);
switch ($datos[2]) {
    case 1:
        header("Content-type: image/gif");
    imagegif($thumb);
        break;
    case 2:
        header("Content-type: image/jpeg");
        imagejpeg($thumb,NULL,72);
        break;
    case 3:
        header("Content-type: image/png");
        imagepng($thumb,NULL,9);
        break;
  default:
   die("Tipo de Imagen no valida");
}
imagedestroy($img);
imagedestroy($thumb);
?>
