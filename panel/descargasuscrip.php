<?php
session_start();
if (empty($_SESSION["referencia"])) {
    echo '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado.<br/>Su sesion ha expirado</div>';
    die(); }
include 'include/databases.php';
	$sql = "SELECT * FROM hesperia_suscripciones ORDER BY id DESC ";
	$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
	$hoy = date("d-m-Y",time());
	$archivo = 'suscripciones_hesperia'.$hoy.'.xlsx';
	$registros = mysqli_num_rows($result);

if ($registros > 0) {
	require_once 'Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();

	//Informacion del excel
	$objPHPExcel->
	getProperties()
		->setCreator("Hoteles Hesperia")
		 ->setLastModifiedBy("hoteleshesperia.com.ve")
		->setTitle("Lista de Suscriptores")
		->setSubject("Office 2007 XLSX Suscriptores")
		->setDescription("Lista de suscriptores Generado via web")
		->setKeywords("hoteleshesperia.com.ve Suscriptores")
		->setCategory("Suscripciones");

# Cabecera
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', "Lista de Suscriptores por correo Hoteles Esperia");

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'eMail');
		$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Fecha Suscripcion');

		$i = 5;

	while ($registro = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		$email = $registro["email"];
		$fecha = date("d-m-Y",$registro["fecha"]);

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue("A$i",$email);
		$objPHPExcel->getActiveSheet()->SetCellValue("D$i",$fecha);

		$i++;
	} 

	header("Content-Type: application/vnd.ms-excel");
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header("Content-Disposition: attachment;filename=$archivo");
	header("Cache-Control: max-age=0");
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
}
else
{ echo '<style>.alert-danger{color:#A94442;background-color:#F2DEDE;border-color:#EBCCD1}</style>
			<div class="alert-danger">No se encontr&oacute; ning&uacute;n datos que mostrar<br/> Ser&aacute; redirigido en 5 segundos</div>
		<meta http-equiv="refresh" content="3; url=home.php?tok='.md5(time()).'"/>';}
	mysqli_close($mysqli);
	exit;
?>
