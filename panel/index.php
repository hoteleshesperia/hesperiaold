<?php
include 'include/core.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico"/>
        <link rel="icon" type="image/x-icon" href="../favicon.ico"/>
        <link rel="shortcut icon" href="../favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="chrome=1,IE=edge"/>
        <meta http-equiv="pragma" content="cache"/>
        <meta http-equiv="cache-control" content="cache"/>
        <meta http-equiv="vary" content="content-language"/>
        <meta name="owner" content="'.$enlace.' - ViSerProject"/>
        <meta name="resource-type" content="document"/>
        <meta name="robots" content="NOINDEX,NOFOLLOW,NOARCHIVE,NOODP,NOSNIPPET"/>
        <meta name="author" content="ViSerProject | Programacion por ViSerProject"/>
        <meta name="copyright" content="CopyLeft (c) <?php echo date("Y",time()); ?> by ViSerProject"/>
        <meta name="revisit-after" content="365 days"/>
        <meta name="revisit" content="365"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <meta name="no-email-collection" content="http://www.unspam.com/noemailcollection/"/>
        <title>PANEL DE CONTROL</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="https://hoteleshesperia.com.ve/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="https://hoteleshesperia.com.ve/panel/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://hoteleshesperia.com.ve/panel/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#0"><strong>Hoteles</strong> Hesperia</a>
      </div>
      <div class="login-box-body">
        <p class="login-box-msg">Ingrese sus datos para acceder</p>
            <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return Ingreso(); return document.MM_returnValue" name="FormIngreso" id="FormIngreso">
          <div class="form-group">
            <input type="email" class="form-control" name="username" id="username" placeholder="Email" value=""/>
          </div>
          <div class="form-group">
            <input type="password" name="clave" id="clave" class="form-control" placeholder="Clave de Acceso" value=""/>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button><br/>
            </div>
          </div>
        </form>
        <a href="#" data-toggle="modal" data-target="#myModal">Olvidó contrase&ntilde;a?</a>
      </div>
        <div id="InformacionAcceso"></div>
    </div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Recuerpar contrase&ntilde;a</h4>
      </div>
      <div class="modal-body">
            <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return Recuperar(); return document.MM_returnValue" name="FormRecupera" id="FormRecupera">
          <div class="form-group">
            <label>Ingrese su correo</label>
            <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="" />
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">
                  Recuperar
                </button>
            </div>
            <div id="RecuperaClave"></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
    <script src="https://hoteleshesperia.com.ve/js/jquery.js" type="text/javascript"></script>
    <script src="https://hoteleshesperia.com.ve/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://hoteleshesperia.com.ve/panel/js/validaciones.js" type="text/javascript"></script>
  </body>
</html>
