<?php 
include 'include/databases.php';
# calendario
	$dbhost=$hostname;
	$dbname=$db_name;
	$dbuser=$user;
	$dbpass=$password;
$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);
?>
		<div class="calendar" data-color="normal">
			<?php
			$query_eventos=$db->query("select id,fecha,evento from tcalendario where 1 &&  id_hotel='$_SESSION[ElHotel]'  order by fecha asc");
			if ($eventos=$query_eventos->fetch_array())
			{ do
				{
				?>
				<div data-role="day" data-day="<?php $fecha=explode("-",$eventos["fecha"]); echo $fecha[0].intval($fecha[1]).intval($fecha[2]); ?>">
					<div data-role="event" data-idevento="<?php echo $eventos["id"]; ?>" data-name="<?php echo utf8_encode($eventos["evento"]); ?>"></div>
				</div>
				<?php
				}
				while($eventos=$query_eventos->fetch_array());
			}
			?>
		</div>
	<script>
	var yy;
	var calendarArray =[];
	var monthOffset = [6,7,8,9,10,11,0,1,2,3,4,5];
	var monthArray = [["ENE","Enero"],["FEB","Febrero"],["MAR","Marzo"],["ABR","Abril"],["MAY","Mayo"],["JUN","Junio"],["JUL","Julio"],["AGO","Agosto"],["SEP","Septiembre"],["OCT","Octubre"],["NOV","Noviembre"],["DIC","Diciembre"]];
	var letrasArray = ["L","M","X","J","V","S","D"];
	var dayArray = ["7","1","2","3","4","5","6"];
	$(document).ready(function() 
	{	$(document).on('click','.calendar-day.have-events',activateDay);
		$(document).on('click','.specific-day',activatecalendar);
		$(document).on('click','.calendar-month-view-arrow',offsetcalendar);
		$(window).resize(calendarScale);
		$(".calendar").calendar({
			"2015827": {
				"": {
					idevento: ""
				}
			}
		});
		calendarSet();
		calendarScale();
		$('body').on('click', '.colorbox', function(e) 
		{ e.preventDefault();
			$.colorbox({href:$(this).attr('href'), open:true,iframe:true,innerWidth:"610px",innerHeight:"370px",overlayClose:false,onClosed:function() { location.reload(true); } });
			return false;
		});
	});
	</script>
