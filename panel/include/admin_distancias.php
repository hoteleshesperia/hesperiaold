<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">Ha ocurrido un error inesperado.<br/>Su sesion ha expirado</div>';
//    die(); }

$antesdecore = 1;
include 'databases.php';
$id_hotel           =   $_POST["id_hotel"];
$nombre_aeropuerto  =   nl2br(trim($_POST["nombre_aeropuerto"]));
$aeropuerto         =   $_POST["aeropuerto"];
$nombra_parque      =   nl2br(trim($_POST["nombra_parque"]));
$parques_nacionales =   $_POST["parques_nacionales"];
$centro_ciudad      =   $_POST["centro_ciudad"];
$museo              =   $_POST["museo"];
$nombre_museo       =   nl2br(trim($_POST["nombre_museo"]));
$metro              =   $_POST["metro"];
$nombre_estacion    =   nl2br(trim($_POST["nombre_estacion"]));
$playas             =   $_POST["playas"];
$centro_comercial   =   $_POST["centro_comercial"];
$banco              =   $_POST["banco"];
$op                 =   $_POST["opcion"];
$ahora              =   time();
switch($op){
    case 1:
        $sql = sprintf("INSERT INTO hesperia_distancia (id, id_hotel, nombre_aeropuerto, aeropuerto, nombra_parque, parques_nacionales, centro_ciudad, museo, nombre_museo, metro, nombre_estacion, playas, centro_comercial, banco)
     VALUES ( NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
            mysqli_real_escape_string($mysqli,$id_hotel),
            mysqli_real_escape_string($mysqli,$nombre_aeropuerto),
            mysqli_real_escape_string($mysqli,$aeropuerto),
            mysqli_real_escape_string($mysqli,$nombra_parque),
            mysqli_real_escape_string($mysqli,$parques_nacionales),
            mysqli_real_escape_string($mysqli,$centro_ciudad),
            mysqli_real_escape_string($mysqli,$museo),
            mysqli_real_escape_string($mysqli,$nombre_museo),
            mysqli_real_escape_string($mysqli,$metro),
            mysqli_real_escape_string($mysqli,$nombre_estacion),
            mysqli_real_escape_string($mysqli,$playas),
            mysqli_real_escape_string($mysqli,$centro_comercial),
            mysqli_real_escape_string($mysqli,$banco));
        $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli)){
            echo '
                <div class="callout callout-success text-center">
                    <h4>
                        Se ha guardado la información de forma correcta
                    </h4>
                </div>';
        }else{
            echo '
                <div class="callout callout-danger text-center">
                    <h4>
                        Disculpe
                    </h4>
                    <p>
                        Hay un problema por lo que no se realizó el registro. Intente nuevamente
                    </p>
                </div>';
        }
    break;
    case 2:
        $id  =   $_POST["id"];
        $sql = sprintf("UPDATE hesperia_distancia SET
                        id_hotel = '%s', nombre_aeropuerto = '%s', aeropuerto = '%s',
                        nombra_parque = '%s', parques_nacionales = '%s',  centro_ciudad = '%s',
                        museo = '%s', nombre_museo = '%s', metro = '%s', nombre_estacion = '%s', playas = '%s',
                        centro_comercial = '%s', banco = '%s'
                        WHERE id = '%s'",
            mysqli_real_escape_string($mysqli,$id_hotel),
            mysqli_real_escape_string($mysqli,$nombre_aeropuerto),
            mysqli_real_escape_string($mysqli,$aeropuerto),
            mysqli_real_escape_string($mysqli,$nombra_parque),
            mysqli_real_escape_string($mysqli,$parques_nacionales),
            mysqli_real_escape_string($mysqli,$centro_ciudad),
            mysqli_real_escape_string($mysqli,$museo),
            mysqli_real_escape_string($mysqli,$nombre_museo),
            mysqli_real_escape_string($mysqli,$metro),
            mysqli_real_escape_string($mysqli,$nombre_estacion),
            mysqli_real_escape_string($mysqli,$playas),
            mysqli_real_escape_string($mysqli,$centro_comercial),
            mysqli_real_escape_string($mysqli,$banco),
            mysqli_real_escape_string($mysqli,$id));
//    die($sql);
            $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
            if (mysqli_affected_rows($mysqli)){
                echo '
                    <div class="callout callout-success text-center">
                        <h4>
                            La información se ha actualizado de forma correcta
                        </h4>
                    </div>';
graba_LOG("Modificada distancia",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
            }else{
                echo '
                    <div class="callout callout-danger text-center">
                        <h4>
                            Disculpe
                        </h4>
                        <p>
                            Hay un problema por lo que no se realizó la actualización de la información. Intente nuevamente
                        </p>
                    </div>';
            }
    break;
}
?>
