<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$_SESSION["mensaje"]  = '';
function revisaloImagen()
{ $allowed = array('png', 'jpg');
 if(isset($_FILES["upl"]) && $_FILES["upl"]["error"] == 0){
     $extension = pathinfo($_FILES["upl"]["name"], PATHINFO_EXTENSION);
     if(!in_array(strtolower($extension), $allowed)){
         return 0; }
 }
 return 1;
}

function subeloimagen()
{ if(move_uploaded_file($_FILES["upl"]["tmp_name"], '../../img/salones/'.$_FILES["upl"]["name"])){
    $ahora = time();
    $nombre = $_FILES["upl"]["name"];
    $nuevo_nombre = substr(time(),-4).'_'.$_FILES["upl"]["name"];
    $search = array('%27',' ','-');
    $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
    rename ("../../img/salones/$nombre", "../../img/salones/$nuevo_nombre");
    chmod("../../img/salones/$nuevo_nombre", 0777);
    $_SESSION["newname"] = $nuevo_nombre;
    return 1;
}
 else { return 0;}
 return 1;
}
$imagen = $_POST["imagenactual"];
if (!empty($_FILES["imagen"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen"]["name"].' ha sido subida al servidor correctamente</p>
       </div></div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body"> <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen  indicada '.$_FILES["imagen"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
$antesdecore2 = 1;
include 'databases.php';
$ahora = time();
$id_hotel           =   $_POST["id_hotel"];
$nombre_Salon       =   nl2br(trim($_POST["nombre_Salon"]));
$caracteristicas    =   nl2br(trim($_POST["caracteristicas"]));
$id_salon           =   $_POST["id_salon"];
$sql = sprintf("UPDATE hesperia_salones
                SET id_hotel = '%s', nombre_Salon = '%s',
                caracteristicas = '%s', imagen = '%s'
                WHERE id_salon = '%s'",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$nombre_Salon),
                mysqli_real_escape_string($mysqli,$caracteristicas),
                mysqli_real_escape_string($mysqli,$imagen),
                mysqli_real_escape_string($mysqli,$id_salon));
//die($sql);
$result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli)){
    $_SESSION["mensaje"] .= '<div class="box-body">
                <div class="callout callout-success text-center">
                    <h4>
                        Se ha guardado la información de forma correcta
                    </h4>
                </div></div>';
    graba_LOG("Nuevo Salon : $nombre_Salon",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
}else{
    $_SESSION["mensaje"] .= '<div class="box-body">
                <div class="callout callout-danger text-center">
                    <h4>
                        Disculpe
                    </h4>
                    <p>
                        Hay un problema por lo que no se realizó el registro. Intente nuevamente
                    </p>
                </div></div>';
}
$ahora = time()*3;
echo '<meta http-equiv="refresh" content="5; url=../home.php?go=ActualizarSalones&tok='.md5($ahora).'&id_salon='.$id_salon.'&id_hotel='.$id_hotel.'"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link href="../css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
<meta charset="UTF-8">
<div class="row">
  <div class="col-md-6" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
    <div class="callout callout-success text-center">
        <h4 style="font-size: 12em;"><i class="fa fa-spinner fa-pulse"></i></h4>
        <p>Procesando, luego se redireccionará... aguarde...<br/>Redireccionando...</p>
    </div>
  </div>
</div>';

?>
