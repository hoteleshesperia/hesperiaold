<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$_SESSION["mensaje"]  = '';

function revisaloImagen()
{ $allowed = array('png', 'jpg');
 if(isset($_FILES["upl"]) && $_FILES["upl"]["error"] == 0){
     $extension = pathinfo($_FILES["upl"]["name"], PATHINFO_EXTENSION);
     if(!in_array(strtolower($extension), $allowed)){
         return 0; }
 }
 return 1;
}

function subeloimagen()
{
    if(move_uploaded_file($_FILES["upl"]["tmp_name"], '../../img/habitaciones/'.$_FILES["upl"]["name"])){
        $ahora = time();
        $nombre = $_FILES["upl"]["name"];
        $nuevo_nombre = substr(time(),-4).'_'.$_FILES["upl"]["name"];
        $search = array('%27',' ','-');
        $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
        rename ("../../img/habitaciones/$nombre", "../../img/habitaciones/$nuevo_nombre");
        chmod("../../img/habitaciones/$nuevo_nombre", 0777);
        $_SESSION["newname"] = $nuevo_nombre;
        return 1;
    }
    else { return 0;}
    return 1;
}

$imagen1    =   $_POST["imagenactual1"];
$imagen2    =   $_POST["imagenactual2"];
$imagen3    =   $_POST["imagenactual3"];
$imagen4    =   $_POST["imagenactual4"];
$imagen5    =   $_POST["imagenactual5"];
$imagen6    =   $_POST["imagenactual6"];
$imagen7    =   $_POST["imagenactual7"];
$imagen8    =   $_POST["imagenactual8"];
$imagen9    =   $_POST["imagenactual9"];
$imagen10   =   $_POST["imagenactual10"];
$imagen11   =   $_POST["imagenactual11"];
$imagen12   =   $_POST["imagenactual12"];
$imagen13   =   $_POST["imagenactual13"];
$imagen14   =   $_POST["imagenactual14"];
$imagen15   =   $_POST["imagenactual15"];
$imagen16   =   $_POST["imagenactual16"];
$imagen17   =   $_POST["imagenactual17"];
$imagen18   =   $_POST["imagenactual18"];

if (!empty($_FILES["imagen1"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen1"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen1 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen1"]["name"].' ha sido subida al servidor correctamente</p>
       </div></div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen1"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen1"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen2"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen2"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen2 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen2"]["name"].' ha sido subida al servidor correctamente</p>
       </div></div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body"><div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen2"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen2"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}

if (!empty($_FILES["imagen3"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen3"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen3 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen3"]["name"].' ha sido subida al servidor correctamente</p>
       </div></div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen3"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen3"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}

if (!empty($_FILES["imagen4"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen4"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen4 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen4"]["name"].' ha sido subida al servidor correctamente</p>
       </div></div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen4"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen4"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen5"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen5"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen5 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen5"]["name"].' ha sido subida al servidor correctamente</p>
       </div></div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen5"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen5"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen6"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen6"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen6"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen6"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen6"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen7"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen7"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen7"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen7"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen7"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen8"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen8"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen8"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen8"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen8"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen9"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen9"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen9"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen9"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen9"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen10"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen10"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen10"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen10"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen10"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen11"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen11"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen11"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen11"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen11"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen12"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen12"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen12"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen12"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen12"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen13"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen13"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen13"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen13"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen13"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen14"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen14"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen14"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen14"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen14"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen15"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen15"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen15"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen15"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen15"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen16"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen16"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen16"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen16"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen16"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}
if (!empty($_FILES["imagen17"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen17"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen17"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen17"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen17"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}

if (!empty($_FILES["imagen18"]["name"]))
{ 	$_FILES["upl"] = $_FILES["imagen18"];
 if  (revisaloImagen())
 { 	if (subeloimagen())
 {	$imagen6 = $_SESSION["newname"];
  $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen18"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
  else
  { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen18"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
 }
 else
 { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen18"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
 }
}

$antesdecore2 = 1;
include 'databases.php';
$id_hotel               = $_POST["id_hotel"];
$nombre_habitacion      = trim($_POST["nombre_habitacion"]);
$precio                 = $_POST["precio"];
$precio_promocion       = $_POST["precio_promocion"];
$cantidad_hab           = $_POST["cantidad_hab"];
$disponibles            = $_POST["disponibles"];
$descripcion           = nl2br(trim($_POST["descripcion"]));
$tamano_hab             = $_POST["tamano_hab"];
$tipo_habitacion        = $_POST["tipo_habitacion"];
$tipo_cama              = $_POST["tipo_cama"];
$cama_adicional         = $_POST["cama_adicional"];
$capacidad_personas     = $_POST["capacidad_personas"];
$mini_bar               = $_POST["mini_bar"];
$albornoz               = $_POST["albornoz"];
$telefono               = $_POST["telefono"];
$control_remoto_tv      = $_POST["control_remoto_tv"];
$tv                     = $_POST["tv"];
$aire_acondicionado     = $_POST["aire_acondicionado"];
$servicio_habitacion    = $_POST["servicio_habitacion"];
$desayuno               = $_POST["desayuno"];
$ambiente_musical       = $_POST["ambiente_musical"];
$secador_pelo           = $_POST["secador_pelo"];
$lavanderia             = $_POST["lavanderia"];
$caja_fuerte            = $_POST["caja_fuerte"];
$tv_cable               = $_POST["tv_cable"];
$jacuzzi                = $_POST["jacuzzi"];
$area_estar             = $_POST["area_estar"];
$jarra_agua_vasos       = $_POST["jarra_agua_vasos"];
$cafetera               = $_POST["cafetera"];
$servicio_medico        = $_POST["servicio_medico"];
$balcon                 = $_POST["balcon"];
$banera                 = $_POST["banera"];
$id_hab                 = $_POST["id_habitacion"];
$ahora = time();
graba_LOG("Actualizando habitacion:  $nombre_habitacion",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
$sql = sprintf("INSERT INTO hesperia_servicios (id_servicios, id_hotel, texto_mascotas, imagen_mascotas, texto_wifi, imagen_wifi, texto_estacionamiento, imagen_estacionamiento, texto_gimnasio, imagen_gimnasio, texto_spa, imagen_spa, texto_helipuerto, imagen_helipuerto, texto_accesibilidad, imagen_accesibilidad, texto_nana, imagen_nana, texto_transporte, imagen_transporte, texto_vigilancia, imagen_vigilancia, texto_cafetin, imagen_cafetin, texto_restaurant, imagen_restaurant, texto_bar, imagen_bar, texto_golf, imagen_golf, texto_tenis, imagen_tenis, texto_basket, imagen_basket, texto_paquetes, imagen_paquetes, texto_peluqueria, imagen_peluqueria)
     VALUES ( NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
mysqli_real_escape_string($mysqli,$id_hotel),
mysqli_real_escape_string($mysqli,$texto_mascotas),
mysqli_real_escape_string($mysqli,$imagen_mascotas),
mysqli_real_escape_string($mysqli,$texto_wifi),
mysqli_real_escape_string($mysqli,$imagen_wifi),
mysqli_real_escape_string($mysqli,$texto_estacionamiento),
mysqli_real_escape_string($mysqli,$imagen_estacionamiento),
mysqli_real_escape_string($mysqli,$texto_gimnasio),
mysqli_real_escape_string($mysqli,$imagen_gimnasio),
mysqli_real_escape_string($mysqli,$texto_spa),
mysqli_real_escape_string($mysqli,$imagen_spa),
mysqli_real_escape_string($mysqli,$texto_helipuerto),
mysqli_real_escape_string($mysqli,$imagen_helipuerto),
mysqli_real_escape_string($mysqli,$texto_accesibilidad),
mysqli_real_escape_string($mysqli,$imagen_accesibilidad),
mysqli_real_escape_string($mysqli,$texto_nana),
mysqli_real_escape_string($mysqli,$imagen_nana),
mysqli_real_escape_string($mysqli,$texto_transporte),
mysqli_real_escape_string($mysqli,$imagen_transporte),
mysqli_real_escape_string($mysqli,$texto_vigilancia),
mysqli_real_escape_string($mysqli,$imagen_vigilancia),
mysqli_real_escape_string($mysqli,$texto_cafetin),
mysqli_real_escape_string($mysqli,$imagen_cafetin),
mysqli_real_escape_string($mysqli,$texto_restaurant),
mysqli_real_escape_string($mysqli,$imagen_restaurant),
mysqli_real_escape_string($mysqli,$texto_bar),
mysqli_real_escape_string($mysqli,$imagen_bar),
mysqli_real_escape_string($mysqli,$texto_golf),
mysqli_real_escape_string($mysqli,$imagen_golf),
mysqli_real_escape_string($mysqli,$texto_tenis),
mysqli_real_escape_string($mysqli,$imagen_tenis),
mysqli_real_escape_string($mysqli,$texto_basket),
mysqli_real_escape_string($mysqli,$imagen_basket),
mysqli_real_escape_string($mysqli,$texto_paquetes),
mysqli_real_escape_string($mysqli,$imagen_paquetes),
mysqli_real_escape_string($mysqli,$texto_peluqueria),
mysqli_real_escape_string($mysqli,$imagen_peluqueria));

$sql = sprintf("UPDATE hesperia_habitaciones SET
                        id_hotel = '%s', cantidad_hab = '%s', disponibles = '%s',
                        nombre_habitacion = '%s', tamano_hab = '%s', tipo_habitacion = '%s', tipo_cama = '%s',
                        cama_adicional = '%s', descripcion = '%s', precio = '%s', precio_promocion = '%s',
                        capacidad_personas = '%s', mini_bar = '%s', albornoz = '%s', telefono = '%s',
                        control_remoto_tv = '%s', tv = '%s', aire_acondicionado = '%s', servicio_habitacion = '%s',
                        desayuno = '%s', ambiente_musical = '%s', secador_pelo = '%s', lavanderia = '%s',
                        caja_fuerte = '%s', tv_cable = '%s', jacuzzi = '%s', area_estar = '%s', jarra_agua_vasos = '%s',
                        cafetera = '%s', servicio_medico = '%s', balcon = '%s', banera = '%s', imagen1 = '%s',
                        imagen2 = '%s', imagen3 = '%s', imagen4 = '%s', imagen5 = '%s', imagen6 = '%s'
                        WHERE id_habitacion = '%s'",
               mysqli_real_escape_string($mysqli,$id_hotel),
               mysqli_real_escape_string($mysqli,$cantidad_hab),
               mysqli_real_escape_string($mysqli,$disponibles),
               mysqli_real_escape_string($mysqli,$nombre_habitacion),
               mysqli_real_escape_string($mysqli,$tamano_hab),
               mysqli_real_escape_string($mysqli,$tipo_habitacion),
               mysqli_real_escape_string($mysqli,$tipo_cama),
               mysqli_real_escape_string($mysqli,$cama_adicional),
               mysqli_real_escape_string($mysqli,$descripcion),
               mysqli_real_escape_string($mysqli,$precio),
               mysqli_real_escape_string($mysqli,$precio_promocion),
               mysqli_real_escape_string($mysqli,$capacidad_personas),
               mysqli_real_escape_string($mysqli,$mini_bar),
               mysqli_real_escape_string($mysqli,$albornoz),
               mysqli_real_escape_string($mysqli,$telefono),
               mysqli_real_escape_string($mysqli,$control_remoto_tv),
               mysqli_real_escape_string($mysqli,$tv),
               mysqli_real_escape_string($mysqli,$aire_acondicionado),
               mysqli_real_escape_string($mysqli,$servicio_habitacion),
               mysqli_real_escape_string($mysqli,$desayuno),
               mysqli_real_escape_string($mysqli,$ambiente_musical),
               mysqli_real_escape_string($mysqli,$secador_pelo),
               mysqli_real_escape_string($mysqli,$lavanderia),
               mysqli_real_escape_string($mysqli,$caja_fuerte),
               mysqli_real_escape_string($mysqli,$tv_cable),
               mysqli_real_escape_string($mysqli,$jacuzzi),
               mysqli_real_escape_string($mysqli,$area_estar),
               mysqli_real_escape_string($mysqli,$jarra_agua_vasos),
               mysqli_real_escape_string($mysqli,$cafetera),
               mysqli_real_escape_string($mysqli,$servicio_medico),
               mysqli_real_escape_string($mysqli,$balcon),
               mysqli_real_escape_string($mysqli,$banera),
               mysqli_real_escape_string($mysqli,$imagen1),
               mysqli_real_escape_string($mysqli,$imagen2),
               mysqli_real_escape_string($mysqli,$imagen3),
               mysqli_real_escape_string($mysqli,$imagen4),
               mysqli_real_escape_string($mysqli,$imagen5),
               mysqli_real_escape_string($mysqli,$imagen6),
               mysqli_real_escape_string($mysqli,$id_hab));
$result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli)){
    $_SESSION["mensaje"] .= '<div class="box-body">
        <div class="callout callout-success text-center">
            <h4>
                Se ha actualizado la habitación <strong>'.$nombre_habitacion.'</strong>  de forma correcta
            </h4>
        </div></div>';
                graba_LOG("Actualizando Habitacion: $nombre_habitacion",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
}else{ $_SESSION["mensaje"] .= '<div class="box-body">
    <div class="callout callout-danger text-center">
        <h4>
            Disculpe
        </h4>
        <p>
            Hay un problema por lo que no se realizó el registro. Intente nuevamente
        </p>
    </div></div>';
     }
$ahora = time()*5;
echo '<meta http-equiv="refresh" content="5; url=../home.php?go=AgregarHabitacion&tok='.md5($ahora).'"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link href="../css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
<meta charset="UTF-8">
<div class="row">
  <div class="col-md-6" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
    <div class="callout callout-success text-center">
        <h4 style="font-size: 12em;"><i class="fa fa-spinner fa-pulse"></i></h4>
        <p>Procesando, luego se redireccionará... aguarde...<br/>Redireccionando...</p>
    </div>
  </div>
</div>';
unset($_SESSION["newname"]);
?>
