<?php

$antesdecore2 = 1;
include 'databases.php';

$id_hotel = $_POST["id_hotel"];
$fechaDesde = $_POST["fechaDesdeReport"];
$fechaHasta = $_POST["fechaHastaReport"];

if($id_hotel > 0){
	$sql = "SELECT codigo_reserva, CONCAT(d.Nombres, ' ', d.Apellidos) as huesped, IF(tipo_pago=0,'TC','TR') as tipo_pago,  ('Habitación') as tipo_reserva, (CASE a.id_hotel WHEN 1 THEN 'Hesperia WTC Valencia' WHEN 2 THEN 'Hesperia Isla Margarita' WHEN 3 THEN 'Hesperia Playa El Agua' ELSE 'Hesperia Edén Club' END) as hotel, (desde) as check_in, (hasta) as check_out, cant_hab as no_habitaciones, REPLACE(habitaciones, '_', ', ') as habitaciones, REPLACE(adultos, '_', ', ') as adultos, replace(ninos, '_', ', ') as ninos, REPLACE(subtotal, '_', ', ') as subtotal_habitaciones, REPLACE(desc_serv, '_', ', ') as servicios, REPLACE(sub_serv, '_', ', ') as subtotal_servicios, (select datediff(b.hasta, b.desde)*b.cant_hab from hesperia_v2_reservaciones b where b.id = a.id)  as noches, a.total as total FROM `hesperia_v2_reservaciones` a, hesperia_usuario d WHERE a.id_hotel = '$id_hotel' and (desde between '$fechaDesde' and '$fechaHasta' or hasta between '$fechaDesde' and '$fechaHasta') and status <> 'N' and status_transac = 0 and fraude = 'N' and d.email = a.email order by codigo_reserva";
}else{
	$sql = "SELECT codigo_reserva, CONCAT(d.Nombres, ' ', d.Apellidos) as huesped, IF(tipo_pago=0,'TC','TR') as tipo_pago,  ('Habitación') as tipo_reserva, (CASE a.id_hotel WHEN 1 THEN 'Hesperia WTC Valencia' WHEN 2 THEN 'Hesperia Isla Margarita' WHEN 3 THEN 'Hesperia Playa El Agua' ELSE 'Hesperia Edén Club' END) as hotel, (desde) as check_in, (hasta) as check_out, cant_hab as no_habitaciones, REPLACE(habitaciones, '_', ', ') as habitaciones, REPLACE(adultos, '_', ', ') as adultos, replace(ninos, '_', ', ') as ninos, REPLACE(subtotal, '_', ', ') as subtotal_habitaciones, REPLACE(desc_serv, '_', ', ') as servicios, REPLACE(sub_serv, '_', ', ') as subtotal_servicios, (select datediff(b.hasta, b.desde)*b.cant_hab from hesperia_v2_reservaciones b where b.id = a.id)  as noches, a.total as total FROM `hesperia_v2_reservaciones` a, hesperia_usuario d WHERE (desde between '$fechaDesde' and '$fechaHasta' or hasta between '$fechaDesde' and '$fechaHasta') and status <> 'N' and status_transac = 0 and fraude = 'N' and d.email = a.email order by codigo_reserva";
}


$result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
$numRows= mysqli_num_rows($result);

    if ($numRows!=null && $numRows>0) {

    	date_default_timezone_set('America/Mexico_City');
    	if (PHP_SAPI == 'cli')
    		die('Este archivo solo se puede ver desde un navegador web');

		 /** Se agrega la libreria PHPExcel */
		 require_once '../Classes/PHPExcel.php';
		 
		// Se crea el objeto PHPExcel
		 $objPHPExcel = new PHPExcel();

		 // Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("Maria") // Nombre del autor
	    ->setLastModifiedBy("Maria") //Ultimo usuario que lo modificó
	    ->setTitle("Reporte Excel con PHP y MySQL") // Titulo
	    ->setSubject("Reporte Excel con PHP y MySQL") //Asunto
	    ->setDescription("Reporte de reservas") //Descripción
	    ->setKeywords("reporte de reservas") //Etiquetas
	    ->setCategory("Reporte excel"); //Categorias

	    $tituloReporte = "Reporte de Reserva de Habitaciones";
		$titulosColumnas = array('Codigo Reserva', 'Huesped', 'Tipo Pago', 'Tipo Reserva', 'Hotel', 'Check in', 'Check out',
			'N° habitaciones', 'Habitaciones', 'Adultos', 'Niños', 'Subtotal Hab.', 'Servicios', 'Subtotal Serv.', 'Noches', 'Total');
		// Se combinan las celdas A1 hasta D1, para colocar ahí el titulo del reporte
		$objPHPExcel->setActiveSheetIndex(0)
		    ->mergeCells('A1:P1');
		// Se agregan los titulos del reporte
	$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1',$tituloReporte) // Titulo del reporte
    ->setCellValue('A3',  $titulosColumnas[0])  //Titulo de las columnas
    ->setCellValue('B3',  $titulosColumnas[1])
    ->setCellValue('C3',  $titulosColumnas[2])
    ->setCellValue('D3',  $titulosColumnas[3])  //Titulo de las columnas
    ->setCellValue('E3',  $titulosColumnas[4])
    ->setCellValue('F3',  $titulosColumnas[5])
    ->setCellValue('G3',  $titulosColumnas[6])  //Titulo de las columnas
    ->setCellValue('H3',  $titulosColumnas[7])
    ->setCellValue('I3',  $titulosColumnas[8])
    ->setCellValue('J3',  $titulosColumnas[9])  //Titulo de las columnas
    ->setCellValue('K3',  $titulosColumnas[10])
    ->setCellValue('L3',  $titulosColumnas[11])
    ->setCellValue('M3',  $titulosColumnas[12])  //Titulo de las columnas
    ->setCellValue('N3',  $titulosColumnas[13])
    ->setCellValue('O3',  $titulosColumnas[14])
    ->setCellValue('P3',  $titulosColumnas[15]);

    $i = 4;
    while ($fila = mysqli_fetch_assoc($result)){
    	 $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A'.$i, $fila['codigo_reserva'])
         ->setCellValue('B'.$i, $fila['huesped'])
         ->setCellValue('C'.$i, $fila['tipo_pago'])
         ->setCellValue('D'.$i, $fila['tipo_reserva'])
         ->setCellValue('E'.$i, $fila['hotel'])
         ->setCellValue('F'.$i, $fila['check_in'])
         ->setCellValue('G'.$i, $fila['check_out'])
         ->setCellValue('H'.$i, $fila['no_habitaciones'])
         ->setCellValue('I'.$i, $fila['habitaciones'])
         ->setCellValue('J'.$i, $fila['adultos'])
         ->setCellValue('K'.$i, $fila['ninos'])
         ->setCellValue('L'.$i, $fila['subtotal_habitaciones'])
         ->setCellValue('M'.$i, $fila['servicios'])
         ->setCellValue('N'.$i, $fila['subtotal_servicios'])
         ->setCellValue('O'.$i, $fila['noches'])
         ->setCellValue('P'.$i, $fila['total']);
     	$i++;
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="ReporteReservasHab.xlsx"');
	header('Cache-Control: max-age=0');
	 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
	}
	else{
	    print_r('No hay resultados para mostrar');
	}

?>