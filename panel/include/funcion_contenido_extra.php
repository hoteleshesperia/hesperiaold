<?php
function EDITAR_CONTENIDO_EXTRA($mysqli,$data,$hostname,$user,$password,$db_name){
    $categoria  =   $_GET["categoria"];
    $sql        =  sprintf("SELECT * FROM hesperia_categoria WHERE id = '%s'",
                           mysqli_real_escape_string($mysqli,$categoria));
    $result     = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $resultCategoria = mysqli_fetch_array($result,MYSQLI_ASSOC);

    $sql        =  sprintf("SELECT * FROM hesperia_contenido WHERE id_categoria = '%s'",
                           mysqli_real_escape_string($mysqli,$categoria));
    $result     = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $resultContenido = mysqli_fetch_array($result,MYSQLI_ASSOC);
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Se encuentra editando '.utf8_encode($resultCategoria["nombre_categoria"]).'</h3>
                        </div>
                        <div class="box-body">
<form enctype="multipart/form-data"  action="include/Admin_cambio_contenido.php" role="form" method="post" onsubmit="return ContenidoValidarTexto1(); return document.MM_returnValue" name="FormEXContenido" id="FormEXContenido">';
    if(isset($_SESSION["mensaje"])){
        echo'<div id="InformacionAgregarPortada">'.$_SESSION["mensaje"].'</div>';
        unset($_SESSION["mensaje"]);
    }
    echo'
                            <div class="form-group">
                                <label for="inputtexto_UNO">Contenido</label>
<textarea name="texto_UNO" id="summernote" class="form-control">'.$resultContenido["texto"].'</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFileUNO">Subir Imagen </label>
                                <input type="file" id="imagen1" name="imagen1" />
            <input type ="hidden"  name="imagenactual1" id="imagenactual1" value="'.$resultContenido["imagen"].'"/>
                            </div>
                            <div class="box-footer">
                                <div class="col-lg-7">
                                    <p>
                                        Si ya realizo todo los cambios que deseaba hacer por favor,
                                        pulse el siguiente boton:
                                    </p>
                                </div>
                                <div class="col-lg-5">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Cambiar Datos
                                    </button>
                                    <input type="hidden" id="idcontenido" name="idcontenido" value="'.$resultContenido["id"].'">
                                </div>
                            </div>
</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



';




    return;
}
?>
