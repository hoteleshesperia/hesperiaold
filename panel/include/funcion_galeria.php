<?php
function SELECT_HOTEL_GALERIA($mysqli,$data,$hostname,$user,$password,$db_name){
    # solo lo vera el admin principal
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar la Galería</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=VerGaleriaHotel" role="form" method="post"  enctype="application/x-www-form-urlencoded" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    echo '<option value="0">Elija Hotel</option>';
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Galeria
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function GALERIA_IMG_HOTELES($mysqli,$data,$hostname,$user,$password,$db_name){
    if (isset($_REQUEST["id_hotel"])){
        $id = $_REQUEST["id_hotel"];
    }else{ $id = $_SESSION["id_hotel"]; }
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowsH = mysqli_fetch_array($result,MYSQLI_ASSOC);
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Subir imagenes a la galeria del Hotel <strong>'.$rowsH["nombre_sitio"].'</strong>
                    </h3>
                </div>';
    if (!empty($_SESSION["mensaje"]))
    { echo '<div id="Respuestagaleria">'.$_SESSION["mensaje"].'</div>'; }
    echo '
                <form id="sube" name="sube" method="post" action="upload.php" enctype="multipart/form-data">
                    <input type="hidden" name="id_hotel" id="id_hotel" value="'.$id.'"/>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputFile">Seleccione la imagen</label>
                            <input type="file" id="upl" name="upl"/><br/>
                        </div>
                        <div class="form-group">
                            <p class="help-block">Las imagenes deben ser igual o mayor 1024px x 800px resolución de 90dpi como mínimo, para un buen resultado</p>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Subir Imagen</button>
                        </div>
                    </div>
                </form>
                <!-- galeria -->
                <div class="center-block box-body"><hr/>
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Estas son las imagenes que se ven actualmente en la galería
                        </h3>
                        <p class="text-danger help-block"><span class="ion-android-warning"></span> <strong>ALERTA</strong>:Tenga en cuenta que al eliminar una imagen esta se elimina sin confirmación alguna de la acción, por tanto, debe estar seguro de lo que esta haciendo.</p>
                    </div>
                    <div id="RespuestaGaleria"></div>
                    <ul class="hide-bullets">';
    $_SESSION["mensaje"] = array();
    $cant = 0;
    $sql = sprintf("SELECT * FROM hesperia_galeria WHERE id_hotel  = '%s' and ind_mediacenter = 'N' and ind_promo='N'  ORDER by id DESC",
                   mysqli_real_escape_string($mysqli,$id));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rowImg=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $cant = 1;
        echo '<li class="col-sm-4">
                        <img src="../img/galeria/'.$rowImg["nombre_imagen"].'" class="img-responsive img-rounded slider-p">
                            <ul class="nav nav-pills center-block">
                                <li role="presentation">
                                    <a id="galeria" href="javascript:void(0)" onclick="javascript:RevGaleria('.$rowImg["id"].','.$id.');">
                                        Eliminar
                                    </a>
                                </li>
                            </ul>
                        </li>';
    }if ($cant == 0){
        echo '
                            <div class="callout callout-danger text-center">
                                <h3>
                                    Disculpe
                                </h3>
                                <p>
                                    Actualmente no hay imágenes para el hotel <strong>'.$rowsH["nombre_sitio"].'</strong>. Debe usar el formulario de arriba para agregar las imágenes.
                                </p>
                                <p>Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                            </div>';
    }echo '
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
