<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$_SESSION["mensaje"]  = '';

function revisaloImagen()
{ $allowed = array('png', 'jpg');
    if(isset($_FILES["upl"]) && $_FILES["upl"]["error"] == 0){
        $extension = pathinfo($_FILES["upl"]["name"], PATHINFO_EXTENSION);
               if(!in_array(strtolower($extension), $allowed)){
                return 0; }
    }
return 1;
}

function subeloimagen()
{ if(move_uploaded_file($_FILES["upl"]["tmp_name"], '../../img/paquetes/'.$_FILES["upl"]["name"])){
        $ahora = time();
        $nombre = $_FILES["upl"]["name"];
        $nuevo_nombre = substr(time(),-4).'_'.$_FILES["upl"]["name"];
        $search = array('%27',' ','-');
        $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
        rename ("../../img/paquetes/$nombre", "../../img/paquetes/$nuevo_nombre");
        chmod("../../img/paquetes/$nuevo_nombre", 0777);
        $_SESSION["newname"] = $nuevo_nombre;
        return 1;
    }
    else { return 0;}
return 1;
}
$imagen = $_POST["imagenactual"];
if (!empty($_FILES["imagen"]["name"]))
    { 	$_FILES["upl"] = $_FILES["imagen"];
        if  (revisaloImagen())
        { 	if (subeloimagen())
                {	$imagen = $_SESSION["newname"];
            $_SESSION["mensaje"] .=  '<div class="box-body"><div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
            else
            { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen  indicada '.$_FILES["imagen"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div></div>'; }
        }
        else
        { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen indicada '.$_FILES["imagen"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div></div>';
        }
}
$antesdecore2 = 1;
include 'databases.php';
$ahora = time();
$nombre_paquete =  trim($_POST["nombre_paquete"]);
$descripcion_p =   nl2br(trim($_POST["paquete"]));
$precio              =  $_POST["precio_paquete"];
$alojamiento         =  $_POST["upselling_paquete"];
$tiempo              =  $_POST["noches_paquete"];
$desde              =  $_POST["desde_paquete"];
$hasta              =  $_POST["hasta_paquete"];
$precio_nino         =  $_POST["precio_nino_paquete"];
$porcentaje         = $_POST["porcentaje_paquete"];
$id_tipo_hab        = $_POST["id_hab"];
$partai             = $_POST["partai_paquete"];
$status             = $_POST["status"];
$vip                = $_POST["vip"];
$precio_pension = $_POST["precio_paquete_pension"];
$precio_todo_inc = $_POST["precio_paquete_ti"];
$price = $_POST["price"];
$price_child = $_POST["price_child"];
$full_day = $_POST["full_day"];
$dominical = $_POST["dominical"];
$ninos_gratis = $_POST["ninos_gratis"];
$noches_gratis = $_POST["noches_gratis"];
$configurable = $_POST["noches_fijas"];
$id_tipo_paquete = $_POST["id_tipo_paquete"];
$vuelo = $_POST["vuelo"];
$noches_hasta = $_POST["noches_paquete_hasta"];
$status_dolar = $_POST["status_dolar"];
$fecha =   $ahora;
graba_LOG("Actualizando paquete:  $nombre_paquete",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
$id     =   $_POST["id"];
$sql    =   sprintf("UPDATE hesperia_paquetes SET
            nombre_paquete = '%s', img_paquete = '%s', descripcion_paquete = '%s', fecha = '%s', 
            precio = '%s', alojamiento = '%s', tiempo = '%s', desde = '%s', hasta = '%s', 
            precio_nino = '%s', porcentaje = '%s', id_tipo_hab = '%s', partai = '%s', status = '%s', 
            vip = '%s', precio_pension = '%s', precio_todo_inc = '%s', price = '%s', price_child = '%s',
            full_day = '%s', dominical = '%s', ninos_gratis = '%s', noches_gratis = '%s',
            configurable = '%s', vuelo = '%s', noches_hasta = '%s', status_dolar = '%s' WHERE id = '%s'",
            mysqli_real_escape_string($mysqli,$nombre_paquete),
            mysqli_real_escape_string($mysqli,$imagen),
            mysqli_real_escape_string($mysqli,$descripcion_p),
            mysqli_real_escape_string($mysqli,$fecha),            
            mysqli_real_escape_string($mysqli,$precio),
            mysqli_real_escape_string($mysqli,$alojamiento),
            mysqli_real_escape_string($mysqli,$tiempo),
            mysqli_real_escape_string($mysqli,$desde),
            mysqli_real_escape_string($mysqli,$hasta),
            mysqli_real_escape_string($mysqli,$precio_nino),
            mysqli_real_escape_string($mysqli,$porcentaje),
            mysqli_real_escape_string($mysqli,$id_tipo_hab),
            mysqli_real_escape_string($mysqli,$partai),
            mysqli_real_escape_string($mysqli,$status),
            mysqli_real_escape_string($mysqli,$vip),            
            mysqli_real_escape_string($mysqli,$precio_pension),
            mysqli_real_escape_string($mysqli,$precio_todo_inc),
            mysqli_real_escape_string($mysqli,$price),
            mysqli_real_escape_string($mysqli,$price_child),
            mysqli_real_escape_string($mysqli,$full_day),
            mysqli_real_escape_string($mysqli,$dominical),
            mysqli_real_escape_string($mysqli,$ninos_gratis),
            mysqli_real_escape_string($mysqli,$noches_gratis),
            mysqli_real_escape_string($mysqli,$configurable),
            mysqli_real_escape_string($mysqli,$vuelo),
            mysqli_real_escape_string($mysqli,$noches_hasta),
            mysqli_real_escape_string($mysqli,$status_dolar),             
            mysqli_real_escape_string($mysqli,$id));
        $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli)){
            $_SESSION["mensaje"] .= '<div class="box-body">
                <div class="callout callout-success text-center">
                    <h4>
                        Se ha guardado la información de forma correcta
                    </h4>
                </div></div>';
                graba_LOG("Actualizando paquete: $nombre_paquete",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
        }else{
            $_SESSION["mensaje"] .= '<div class="box-body">
                <div class="callout callout-danger text-center">
                    <h4>
                        Disculpe
                    </h4>
                    <p>
                        Hay un problema por lo que no se realizó el registro. Intente nuevamente
                    </p>
                </div></div>';
        }
$ahora = time()*3;
echo '<meta http-equiv="refresh" content="5; url=../home.php?go=ActualizarPaquetes&tok='.md5($ahora).'&id='.$id.'&tipo_paquete='.$id_tipo_paquete.'"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link href="../css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
<meta charset="UTF-8">
<div class="row">
  <div class="col-md-6" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
    <div class="callout callout-success text-center">
        <h4 style="font-size: 12em;"><i class="fa fa-spinner fa-pulse"></i></h4>
        <p>Procesando, luego se redireccionará... aguarde...<br/>Redireccionando...</p>
    </div>
  </div>
</div>';
?>
