<?php

$antesdecore2 = 1;
include 'databases.php';

$id_hotel = $_POST["id_hotel"];
$fechaDesde = $_POST["fechaDesdeReport"];
$fechaHasta = $_POST["fechaHastaReport"];

if($id_hotel > 0){
    $sql = "SELECT codigo_reserva,  CONCAT( b.Nombres,  ' ', b.Apellidos ) AS huesped, 
    IF( tipo_pago =0,  'TC',  'TR' ) AS tipo_pago, ('Paquete') AS tipo_reserva, 
    (CASE a.id_hotel WHEN 1 THEN  'Hesperia WTC Valencia' WHEN 2 THEN  'Hesperia Isla Margarita' WHEN 3 
        THEN  'Hesperia Playa El Agua' ELSE  'Hesperia Edén Club' END ) AS hotel, nombre_paq, 
    a.total, (a.fecha_inicio_promo) as check_in, (a.fecha_fin_promo) as check_out, 
    ( datediff(a.fecha_fin_promo, a.fecha_inicio_promo))  as noches 
    FROM `hesperia_v2_reservas_paq` a, hesperia_usuario b WHERE a.email = b.email 
    and a.id_hotel = '$id_hotel' and a.status <> 'N' and status_transac = 0 
    and fraude = 'N' and (fecha_inicio_promo between '$fechaDesde' and '$fechaHasta' 
        or fecha_fin_promo between '$fechaDesde' and '$fechaHasta')  
    order by codigo_reserva";
}else{
    $sql = "SELECT codigo_reserva,  CONCAT( b.Nombres,  ' ', b.Apellidos ) AS huesped, 
    IF( tipo_pago =0,  'TC',  'TR' ) AS tipo_pago, ('Paquete') AS tipo_reserva, 
    (CASE a.id_hotel WHEN 1 THEN  'Hesperia WTC Valencia' WHEN 2 THEN  'Hesperia Isla Margarita' WHEN 3 
        THEN  'Hesperia Playa El Agua' ELSE  'Hesperia Edén Club' END ) AS hotel, nombre_paq, 
    a.total, (a.fecha_inicio_promo) as check_in, (a.fecha_fin_promo) as check_out, 
    ( datediff(a.fecha_fin_promo, a.fecha_inicio_promo))  as noches 
    FROM `hesperia_v2_reservas_paq` a, hesperia_usuario b WHERE a.email = b.email and a.status <> 'N' 
    and status_transac = 0 and fraude = 'N' 
    and (fecha_inicio_promo between '$fechaDesde' and '$fechaHasta' 
        or fecha_fin_promo between '$fechaDesde' and '$fechaHasta')  order by codigo_reserva";
}


$result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
$numRows= mysqli_num_rows($result);

    if ($numRows!=null && $numRows>0) {

    	date_default_timezone_set('America/Mexico_City');
    	if (PHP_SAPI == 'cli')
    		die('Este archivo solo se puede ver desde un navegador web');

		 /** Se agrega la libreria PHPExcel */
		 require_once '../Classes/PHPExcel.php';
		 
		// Se crea el objeto PHPExcel
		 $objPHPExcel = new PHPExcel();

		 // Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("Hesperia") // Nombre del autor
	    ->setLastModifiedBy("Hesperia") //Ultimo usuario que lo modificó
	    ->setTitle("Reporte Excel con PHP y MySQL") // Titulo
	    ->setSubject("Reporte Excel con PHP y MySQL") //Asunto
	    ->setDescription("Reporte de reservas de Paquetes") //Descripción
	    ->setKeywords("Reporte de reservas de Paquetes") //Etiquetas
	    ->setCategory("Reporte excel"); //Categorias

	    $tituloReporte = "Reporte de Reserva de Paquetes";
		$titulosColumnas = array('Codigo Reserva', 'Huesped', 'Tipo Pago', 'Tipo Reserva', 'Hotel', 'Nombre Paquete', 'Total', 'Check in', 'Check out', 'Noches');		// Se combinan las celdas A1 hasta D1, para colocar ahí el titulo del reporte
		$objPHPExcel->setActiveSheetIndex(0)
		    ->mergeCells('A1:J1');
		// Se agregan los titulos del reporte
	$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1',$tituloReporte) // Titulo del reporte
    ->setCellValue('A3',  $titulosColumnas[0])  //Titulo de las columnas
    ->setCellValue('B3',  $titulosColumnas[1])
    ->setCellValue('C3',  $titulosColumnas[2])
    ->setCellValue('D3',  $titulosColumnas[3])  //Titulo de las columnas
    ->setCellValue('E3',  $titulosColumnas[4])
    ->setCellValue('F3',  $titulosColumnas[5])
    ->setCellValue('G3',  $titulosColumnas[6])
    ->setCellValue('H3',  $titulosColumnas[7])
    ->setCellValue('I3',  $titulosColumnas[8])
    ->setCellValue('J3',  $titulosColumnas[9]);

    $i = 4;
    while ($fila = mysqli_fetch_assoc($result)){
    	 $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A'.$i, $fila['codigo_reserva'])
         ->setCellValue('B'.$i, $fila['huesped'])
         ->setCellValue('C'.$i, $fila['tipo_pago'])
         ->setCellValue('D'.$i, $fila['tipo_reserva'])
         ->setCellValue('E'.$i, $fila['hotel'])
         ->setCellValue('F'.$i, $fila['nombre_paq'])
         ->setCellValue('G'.$i, $fila['total'])
         ->setCellValue('H'.$i, $fila['check_in'])
         ->setCellValue('I'.$i, $fila['check_out'])
         ->setCellValue('J'.$i, $fila['noches']);
     	$i++;
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="ReporteReservasPaq.xlsx"');
	header('Cache-Control: max-age=0');
	 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
	}
	else{
	    print_r('No hay resultados para mostrar');
	}

?>