<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Viserproject.com
--------------------------------------------------------*/

session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$antesdecore = 1;
include 'databases.php';

$PublicKeyId    = trim($_POST["publickeyid"]);
$KeyID          = trim($_POST["keyid"]);
$id_hotel       = $_POST["id_hotel"];
$op             = $_POST["op"];
$ahora          = time();

switch ($op){
    case 1:

        $sql = sprintf("INSERT INTO hesperia_instapago
                      (id, id_hotel, KeyID, PublicKeyId)
                       VALUES ( NULL, '%s', '%s', '%s')",
                       mysqli_real_escape_string($mysqli,$id_hotel),
                       mysqli_real_escape_string($mysqli,$KeyID),
                       mysqli_real_escape_string($mysqli,$PublicKeyId));
        $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli)){
            echo '
                    <div class="callout callout-success text-center">
                        <h4>
                            Se agregarón las llaves de forma correcta
                        </h4>
                    </div>';
            graba_LOG("Agregando Llaves de Instapago",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
        }else{
            echo '
                    <div class="callout callout-danger text-center">
                        <h4>
                            Disculpe
                        </h4>
                        <p>
                            Hay un problema por lo que no se agregarón las llaves.
                        </p>
                    </div>';
        }
        break;
    case 2:
        $sql    = sprintf("UPDATE hesperia_instapago
                    SET KeyID = '%s',
                    PublicKeyId = '%s'
                    WHERE id_hotel = '%s'",
                          mysqli_real_escape_string($mysqli,$KeyID),
                          mysqli_real_escape_string($mysqli,$PublicKeyId),
                          mysqli_real_escape_string($mysqli,$id_hotel));
        $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli)){
            echo '
                    <div class="callout callout-success text-center">
                        <h4>
                            Llaves Actualizadas
                        </h4>
                    </div>';
            graba_LOG("Actualizando Llaves de Instapago",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
        }else{
            echo '
                    <div class="callout callout-danger text-center">
                        <h4>
                            Disculpe
                        </h4>
                        <p>
                            Hay un problema por lo que no se realizó la actualización de llaves.
                        </p>
                    </div>';
        }
}
