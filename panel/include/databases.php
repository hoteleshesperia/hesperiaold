<?php
/* -------------------------------------------------------
Script  bajo los t&eacute;rminos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor: Hector A. Mantellini (Xombra)
-------------------------------------------------------- */
if(isset($antesdecore2)){
  include '../../config/data.ini.php'; }
else { if(!isset($antesdecore)){
    include '../config/data.ini.php'; }
else{ include '../../config/data.ini.php'; }
}

$hostname = $db["hostname"];
$user = $db["user"];
$password = $db["password"];
$db_name = $db["db_name"];

# calendario

	$dbhost=$hostname;
	$dbname=$db_name;
	$dbuser=$user;
	$dbpass=$password;
$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

# solo para la parte de comentarios
define("DB_SERVER", $hostname); //Servidor
define("DB_USER", $user); //Nombre del usuario
define("DB_PASS", $password); //Contraseña
define("DB_NAME", $db_name); //Nombre de la base de datos

$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);

function CONECTAR_BD($hostname,$user,$password,$db_name)
{ function_exists('mysqli_connect')
    or die ('<div class="alert alert-danger" role="alert"><p>ERROR:No se tiene soporte actualmente para la base de datos MySQL</p></div>');
    global $mysqli;
    $mysqli=mysqli_connect($hostname,$user,$password,$db_name)
    or die ('<div class="alert alert-danger" role="alert"><p>Error inesperado, No se pudo ejecutar la instruccion. Contacte al administrador</p></div>'.' '.mysqli_error($mysqli));
return $mysqli;
}

function QUERYBD($sql,$hostname,$user,$password,$db_name)
{ $mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
 if ($resulta=@mysqli_query($mysqli,$sql))
 { return $resulta; }
  else
  { die ('<div class="alert alert-danger" role="alert"><p>Error inesperado, no se pudo ejecutar la instrucción. Contacte al administrador!</p></div>'); }
return;
}

function graba_LOG($log,$quien,$ip,$momento,$hostname,$user,$password,$db_name){
 $momento=time();
 $sql=sprintf("INSERT INTO hesperia_logs VALUES (NULL,'$log','$quien','$ip','$momento')");
 $result= QUERYBD($sql,$hostname,$user,$password,$db_name);
return;
}

function NORMAL_MYSQL($fecha)
{ $dia  = substr($fecha,0,2);
  $mes  = substr($fecha,3,2);
  $year = substr($fecha,6);
return mktime ( 10, 1,1, $mes, $dia, $year);
}

function NORMAL_MYSQL2($fecha)
{ 	$year = substr($fecha,0,4);
	$mes = substr($fecha,5,2);
	$dia = substr($fecha,8);
return mktime (8, 1,1, $mes, $dia, $year);
}

function NORMAL_MYSQL3($fecha)
{ 	$year = substr($fecha,0,4);
	$mes = substr($fecha,5,1);
	$dia = substr($fecha,7);
return mktime (8, 1,1, $mes, $dia, $year);
}

function FileUploadErrorMsg($error_code) {
    switch ($error_code) {
        case UPLOAD_ERR_INI_SIZE:
          $code = "El archivo es más grande que lo permitido por el Servidor.";
        case UPLOAD_ERR_FORM_SIZE:
           $code = "El archivo subido es demasiado grande.";
        case UPLOAD_ERR_PARTIAL:
           $code = "El archivo subido no se terminó de cargar (probablemente cancelado por el usuario).";
        case UPLOAD_ERR_NO_FILE:
            $code = "No se subió ningún archivo";
        case UPLOAD_ERR_NO_TMP_DIR:
            $code = "Error del servidor: Falta el directorio temporal.";
        case UPLOAD_ERR_CANT_WRITE:
           $code = "Error del servidor: Error de escritura en disco";
        case UPLOAD_ERR_EXTENSION:
            $code ="Error del servidor: Subida detenida por la extención";
      default:
           $code = "Error del servidor: ".$error_code;
    }
return $code;
}
?>
