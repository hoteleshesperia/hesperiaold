<?php 

function cargarListaPdfs($mysqli){

	$query = "SELECT a.*, b.razon_social, b.id as id_hotel FROM hesperia_descarga a inner join hesperia_settings b on
	(a.id_hotel = b.id)";

	$result = $mysqli->query($query);

	return $result;
}


function cargarHoteles($mysqli){

	$query = "SELECT a.razon_social, a.id from hesperia_settings a";

	$result = $mysqli->query($query);

	return $result;
}

function cargarFotosMediaCenter($mysqli){
	$query = "SELECT a.*, b.razon_social from hesperia_galeria a inner join 
	 hesperia_settings b on
	(a.id_hotel = b.id) where ind_mediacenter ='S'";

	$result = $mysqli->query($query);
	return $result;
}

function cargarVideosMediaCenter($mysqli){
	$query = "SELECT a.*, b.razon_social from hesperia_video a inner join 
	 hesperia_settings b on
	(a.id_hotel = b.id)";

	$result = $mysqli->query($query);
	return $result;
}

function cargarNoticias($mysqli){
	$query = "SELECT * FROM hesperia_v2_noticias order by id_noticia desc";
	$result = $mysqli->query($query);

	return $result;
}

function cargarNoticiaCategoria($mysqli, $id_noticia){
	$query = "SELECT a.id_categoria, a.nombre_categoria from hesperia_v2_categoria a inner join
	hesperia_v2_categoria_noticia b on (a.id_categoria = b.id_categoria) where b.id_noticia = $id_noticia";

	$result = $mysqli->query($query);
	return $result;
}

function cargarTodoCategorias($mysqli){
	$query = "SELECT * FROM hesperia_v2_categoria";
	$result = $mysqli->query($query);
	return $result;
}

function cargarNoticiaIndividual($mysqli, $id_noticia){
	$query = "SELECT * from hesperia_v2_noticias where id_noticia = $id_noticia";
	$result = $mysqli->query($query);
	return $result;
}

function cargarTodoFotosNoticias($mysqli){
	$query = "SELECT a.*, b.titulo FROM hesperia_v2_noticia_galeria a inner join
	hesperia_v2_noticias b on (a.id_noticia = b.id_noticia)";
	$result = $mysqli->query($query);
	return $result;
}

function cargarFotosNoticiaIndividual($mysqli, $id){
	$query="SELECT a.* FROM hesperia_v2_noticia_galeria a inner join 
	hesperia_v2_noticias b on (a.id_noticia=b.id_noticia) where a.id_noticia = $id";
	$result = $mysqli->query($query);
	return $result;
}

 ?>