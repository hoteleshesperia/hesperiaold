<?php

function FORM_SELECCIONA_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel Para Agregar los % de costos Para las habitaciones</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=AgregaPorcentHab" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio
                                            FROM hesperia_settings
                                            ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo'<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Seleccionar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function FORM_SELECCIONA_HOTEL_EDITAR($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel Para Editar los % de costos Para las habitaciones</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=EditarPorcentHab" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio
                                            FROM hesperia_settings
                                            ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo'<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Seleccionar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function FORM_AGREGA_PORCENTAJE_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
    $id_hotel = $_REQUEST["id_hotel"];
        $sql = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $resultSet = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultSet,MYSQLI_ASSOC);
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregando % Para el hotel <strong>'.$rows["nombre_sitio"].'</strong> de costos Para las habitaciones</h3>
                </div>
                <div class="box-body">
                    <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return FormPorcentajes(); return document.MM_returnValue" name="Porcentajes" id="Porcentajes">
<div id="PorcentajesRespuesta"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="adultos">% Para 1 Adulto</label>
                    <input type="text" class="form-control" name="adultos" id="adultos" placeholder="% Para 1 Adulto">
                </div>
                <div class="form-group">
                    <label for="adultos_2">% Para 2 Adultos</label>
                    <input type="text" class="form-control" name="adultos_2" id="adultos_2" placeholder="% Para 2 Adultos">
                </div>
                <div class="form-group">
                    <label for="adultuos_3">% Para 3 Adultos</label>
                    <input type="text" class="form-control" name="adultuos_3" id="adultuos_3" placeholder="% Para 3 Adultos">
                </div>
                <div class="form-group">
                    <label for="adultos_4">% Para 4 Adultos</label>
                    <input type="text" class="form-control" name="adultos_4" id="adultos_4" placeholder="% Para 4 Adultos">
                </div>
            </div>

            <div class="col-md-6">

                <div class="form-group">
                    <label for="nino1">% Para 1 Niño</label>
                    <input type="text" class="form-control" name="nino1" id="nino1" placeholder="% Para 1 Niño">
                </div>
                <div class="form-group">
                    <label for="nino2">% Para 2 Niños</label>
                    <input type="text" class="form-control" name="nino2" id="nino2" placeholder="% Para 2 Niños">
                </div>
                <div class="form-group">
                    <label for="nino3">% Para 3 Niños</label>
                    <input type="text" class="form-control" name="nino3" id="nino3" placeholder="% Para 3 Niños">
                </div>
                <div class="form-group">
                    <label for="minima_edad_nino">Edad Minima Para ser Considerado Niño</label>
                    <input type="text" class="form-control" name="minima_edad_nino" id="minima_edad_nino" placeholder="Edad Minima Para ser Considerado Niño">
                </div>
            </div>

                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Agregar % de Precios
                                    </button>
                                    <input type="hidden" name="id_hotel" id="id_hotel" value="'.$id_hotel.'">
                                    <input type="hidden" name="op" id="op" value="1">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function FORM_EDITAR_PORCENTAJE_HAB($mysqli,$data,$hostname,$user,$password,$db_name){
    $id_hotel = $_REQUEST["id_hotel"];
        $sql = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $resultSet = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultSet,MYSQLI_ASSOC);
        $sqlPorcenT = sprintf("SELECT * FROM hesperia_porcentage_hab WHERE id_hotel = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
//    die($sqlPorcenT);
    $resultPorcentHab = QUERYBD($sqlPorcenT,$hostname,$user,$password,$db_name);
    $cantidad = mysqli_num_rows($resultPorcentHab);
    $rowsPorcentHab = mysqli_fetch_array($resultPorcentHab,MYSQLI_ASSOC);
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregando % Para el hotel <strong>'.$rows["nombre_sitio"].'</strong> de costos Para las habitaciones</h3>
                </div>
                <div class="box-body">';
if ($cantidad == 0){
        echo '
                            <div class="callout callout-danger text-center">
                                <h3>Disculpe</h3>
                                <p>Actualmente no hay información para actualizar este hotel.</p>
                                <p>Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada. O clic <a href="home.php?go=AgregaPorcentHab&id_hotel='.$id_hotel.'" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Crear % de Precios para el hotel '.$rows["nombre_sitio"].'"><strong>aquī</strong></a> para Crear % de Precios para el hotel '.$rows["nombre_sitio"].'</p>
                            </div>';
    }else{

    echo '

                    <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return FormPorcentajes(); return document.MM_returnValue" name="Porcentajes" id="Porcentajes">
<div id="PorcentajesRespuesta"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="adultos">% Para 1 Adulto</label>
                    <input type="text" class="form-control" name="adultos" id="adultos" placeholder="% Para 1 Adulto" value="'.$rowsPorcentHab["adultos"].'">
                </div>
                <div class="form-group">
                    <label for="adultos_2">% Para 2 Adultos</label>
                    <input type="text" class="form-control" name="adultos_2" id="adultos_2" placeholder="% Para 2 Adultos" value="'.$rowsPorcentHab["adultos_2"].'">
                </div>
                <div class="form-group">
                    <label for="adultuos_3">% Para 3 Adultos</label>
                    <input type="text" class="form-control" name="adultuos_3" id="adultuos_3" placeholder="% Para 3 Adultos" value="'.$rowsPorcentHab["adultuos_3"].'">
                </div>
                <div class="form-group">
                    <label for="adultos_4">% Para 4 Adultos</label>
                    <input type="text" class="form-control" name="adultos_4" id="adultos_4" placeholder="% Para 4 Adultos" value="'.$rowsPorcentHab["adultos_4"].'">
                </div>
            </div>

            <div class="col-md-6">

                <div class="form-group">
                    <label for="nino1">% Para 1 Niño</label>
                    <input type="text" class="form-control" name="nino1" id="nino1" placeholder="% Para 1 Niño" value="'.$rowsPorcentHab["nino1"].'">
                </div>
                <div class="form-group">
                    <label for="nino2">% Para 2 Niños</label>
                    <input type="text" class="form-control" name="nino2" id="nino2" placeholder="% Para 2 Niños" value="'.$rowsPorcentHab["nino2"].'">
                </div>
                <div class="form-group">
                    <label for="nino3">% Para 3 Niños</label>
                    <input type="text" class="form-control" name="nino3" id="nino3" placeholder="% Para 3 Niños" value="'.$rowsPorcentHab["nino3"].'">
                </div>
                <div class="form-group">
                    <label for="minima_edad_nino">Edad Minima Para ser Considerado Niño</label>
                    <input type="text" class="form-control" name="minima_edad_nino" id="minima_edad_nino" placeholder="Edad Minima Para ser Considerado Niño" value="'.$rowsPorcentHab["minima_edad_nino"].'">
                </div>
            </div>

                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Agregar % de Precios
                                    </button>
                                    <input type="hidden" name="id_hotel" id="id_hotel" value="'.$id_hotel.'">
                                    <input type="hidden" name="op" id="op" value="2">
                                </div>
                            </div>
                        </div>
                    </form>';
}echo'
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
