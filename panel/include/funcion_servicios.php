<?php
function FORMULARIO_SERVICIOS($mysqli,$data,$hostname,$user,$password,$db_name){
?>
<script type="text/javascript">
function enviar_parametro(valor){
    location = location.pathname + '?go=CrearServicios&id_hotel=' + valor;
}
window.onload = function(){
    document.getElementById('id_hotel').onchange = function(){
        enviar_parametro(this.value);
    }
}
function enviar_servicio(valor,id_hotel){
    var id_hotel = id_hotel;
    alert(id_hotel);
    location = location.pathname + '?go=CrearServicios&id_hotel=' + id_hotel + '&campo=' +valor;
}
window.onload = function(){
    document.getElementById('campo').onchange = function(){
        enviar_servicio(this.value);
    }
}
</script>
<?php
    if (isset($_REQUEST["id_hotel"])){
        $id = $_REQUEST["id_hotel"];
    }else{ $id = $_SESSION["id_hotel"]; }
    $sql = sprintf("SELECT * FROM hesperia_settings WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultSettin = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowsSet = mysqli_fetch_array($resultSettin,MYSQLI_ASSOC);
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Servicios del hotel <strong>'.$rowsSet["nombre_sitio"].'</strong></h3>
                </div>
                <div class="box-body">
                <div id="RespuestContenido"></div>
<form application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return ValidarServicios(); return document.MM_returnValue" name="FormActualizaServicios" id="FormActualizaServicios">
<input type="hidden" class="form-control" name="id_hotel" id="id_hotel" value="'.$id.'">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="nombre_hotel">Seleccione el Hotel al cual le pertenece este evento</label>
            <select class="form-control" name="id_hotel" id="id_hotel" onchange="enviar_parametro(this.value);">';
        if (!isset($_GET["id_hotel"]))
        { echo '<option value="" selected="selected">Seleccione</option>'; }
        $id_hotel =  $_GET["id_hotel"];
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            if ($_GET["id_hotel"]==$rows["id"]) {
                echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
            } else {
                echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>'; }
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
//    $id_hotel = $_GET["id_hotel"];
    $sql = sprintf("SELECT salones_evento,escapadas,experiencias,mascotas,wifi,piscina,estacionamiento,botones,
                gimnasio,spa,helipuerto,accesibilidad,nana,transporte,vigilancia_privada,servicio_medico,
                chofer,alquiler_vehiculos,cafetin,restaurant,bar,discotheca,golf,tenis,basket,tienda,parque,terraza
                FROM hesperia_settings WHERE id  = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $query = QUERYBD($sql,$hostname,$user,$password,$db_name);
    if(!isset($_GET["campo"])){
        $_GET["campo"] = '';
    }
    echo '
<div class="form-group">
    <label for="nombre_hotel">Seleccione el Servicio a editar</label>
    <select class="form-control" name="campo" id="campo" onchange="enviar_servicio(this.value,'.$id_hotel.');">
        <option value="0">Seleccione el servicio a editar</option>';
    if($servicios = mysqli_fetch_array($query,MYSQLI_ASSOC)){
        foreach ($servicios as $key => $value) {
            if($value == 1){
                if($_GET["campo"] == $key){
                    echo '<option value="'.$key.'" selected>'.str_replace('_',' ',ucfirst($key)).'</option>';
                }else{
                    echo '<option value="'.$key.'">'.str_replace('_',' ',ucfirst($key)).'</option>';
                }
            }
        }
    }
    echo '
    </select>
</div>';
    if(isset($_GET["campo"]) && isset($_GET["id_hotel"])){
        $campo = 'texto_'.$_GET["campo"];
        $id_hotel = $_GET["id_hotel"];
        $sql = sprintf("SELECT * FROM hesperia_servicios WHERE id_hotel = '%s'",
                       mysqli_real_escape_string($mysqli,$campo),
                       mysqli_real_escape_string($mysqli,$id_hotel));
        $query = QUERYBD($sql,$hostname,$user,$password,$db_name);
        $datocampo = mysqli_fetch_array($query,MYSQLI_ASSOC);
        echo'
    <div class="form-group">
        <textarea class="form-control" name="texto" id="summernote4">'.$datocampo[$campo].'</textarea>
    </div>
    <div class="col-lg-12">
        <div class="box-footer">
            <div class="col-lg-7">
                <p>
            Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                </p>
            </div>
            <div class="col-lg-5">
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Actualizar Servicios
                </button>
            </div>
        </div>
    </div>
  ';
    }
    echo'

</form>
                </div>
            </div>
        </div>
    </div><!-- /.row -->
</section><!-- /.content -->
';
    return;
}


?>
