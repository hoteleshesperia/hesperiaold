<?php
function VER_LOGS($mysqli,$data,$hostname,$user,$password,$db_name)
{  echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>Logs</strong>: Acciones realizadas en el sitio.</h3>
                </div>
                <div class="box-body">
                <div id="RespuestaLimpLogs"></div>';
                echo'<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return EliminarLogs();" name="FormLogs" id="FormLogs">
                    <div class="form-group">
                        <label for="inputlogs">Limpiar Logs:</label>
                        <select name="limpiar" id="limpiar" class="form-control">
                                    <option value="0" selected>Seleccione</option>
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                        </select>
                    </div>
                    <div class="box-footer">
                        <div class="col-lg-8">
                            <p class="help-block">
                                <strong>Sugerimos</strong> <br/>No eliminar los logs del sistema, ya que no podrá conocer las acciones realizadas por sus usuarios.
                            </p>
                        </div>';
        if ($_SESSION["nivel"] == 0) {
        echo '
                        <div class="col-lg-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                    Eliminar Logs
                                </button>
                            </div>
                        </div>';
        echo '    </div>
        </form>';
     }
echo '
                    <p class="help-block"><strong>Nota</strong>: Solo se mostrarán las últimas 350 acciones</p>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                  <td>Acción</td>
                                  <td>Quien</td>
                                  <td>IP</td>
                                  <td>Fecha</td>
                            </tr>
                        <thead>';
                        $i = 0;
                        $sql = sprintf("SELECT * FROM hesperia_logs ORDER BY log_id DESC limit 350");
                        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                        while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                            if ($i%2) {
                                echo '<tr class="success">';
                            }else{
                                echo '<tr class="info">';
                            }
                            echo '
                                <td>'.utf8_decode($rows["log"]).'</td>
                                <td>'.$rows["quien"].'</td>
                                <td>'.$rows["ip"].'</td>
                                <td>'.date("d-m-Y H:i",$rows["momento"]).'</td>';
                            echo ' </tr>';
                            $i++;
                        }
 echo '
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>';
}
?>
