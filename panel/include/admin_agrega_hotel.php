<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }

$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
extract($_POST);
$razon_social = trim($razon_social);
$descripcion = nl2br(trim($descripcion));
$correo_envio=strip_tags(strtolower(trim($correo_envio)));
$correo_envio=filter_var($correo_envio,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$email_reserva=strip_tags(strtolower(trim($email_reserva)));
$email_reserva=filter_var($email_reserva,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$correo_contacto=strip_tags(strtolower(trim($correo_contacto)));
$correo_contacto=filter_var($correo_contacto,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$fecha_creacion = time();
$sql = sprintf("INSERT INTO hesperia_settings (id, razon_social, rif, nombre_sitio, descripcion, dominio, direccion, ciudad, estado, pais, latitud, longitud, telefonos, telf_reserva, check_in, check_out, postal, twitter, facebook, gplus, youtube, instagram, pinterest, linkedin, correo_envio, correo_contacto, keywords, description, salones, eventos, escapadas, experiencias, mascotas, wifi, piscina, estacionamiento, botones, gimnasio, spa, helipuerto, accesibilidad, nana, transporte, vigilancia_privada, servicio_medico, chofer, alquiler_vehiculos, cafetin, restaurant, bar, discotheca, golf, tenis, basket, tienda, cant_habitaciones, parque, terraza, tripadvisor, tripadvisor_premio, fecha_creacion,email_reserva)
     VALUES ( NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
               mysqli_real_escape_string($mysqli,$razon_social),
               mysqli_real_escape_string($mysqli,$rif),
               mysqli_real_escape_string($mysqli,$nombre_sitio),
               mysqli_real_escape_string($mysqli,$descripcion),
               mysqli_real_escape_string($mysqli,$dominio),
               mysqli_real_escape_string($mysqli,$direccion),
               mysqli_real_escape_string($mysqli,$ciudad),
               mysqli_real_escape_string($mysqli,$estado),
               mysqli_real_escape_string($mysqli,$pais),
               mysqli_real_escape_string($mysqli,$latitud),
               mysqli_real_escape_string($mysqli,$longitud),
               mysqli_real_escape_string($mysqli,$telefonos),
               mysqli_real_escape_string($mysqli,$telf_reserva),
               mysqli_real_escape_string($mysqli,$check_in),
               mysqli_real_escape_string($mysqli,$check_out),
               mysqli_real_escape_string($mysqli,$postal),
               mysqli_real_escape_string($mysqli,$twitter),
               mysqli_real_escape_string($mysqli,$facebook),
               mysqli_real_escape_string($mysqli,$gplus),
               mysqli_real_escape_string($mysqli,$youtube),
               mysqli_real_escape_string($mysqli,$instagram),
               mysqli_real_escape_string($mysqli,$pinterest),
               mysqli_real_escape_string($mysqli,$linkedin),
               mysqli_real_escape_string($mysqli,$correo_envio),
               mysqli_real_escape_string($mysqli,$correo_contacto),
               mysqli_real_escape_string($mysqli,$keywords),
               mysqli_real_escape_string($mysqli,$description),
               mysqli_real_escape_string($mysqli,$salones),
               mysqli_real_escape_string($mysqli,$eventos),
               mysqli_real_escape_string($mysqli,$escapadas),
               mysqli_real_escape_string($mysqli,$experiencias),
               mysqli_real_escape_string($mysqli,$mascotas),
               mysqli_real_escape_string($mysqli,$wifi),
               mysqli_real_escape_string($mysqli,$piscina),
               mysqli_real_escape_string($mysqli,$estacionamiento),
               mysqli_real_escape_string($mysqli,$botones),
               mysqli_real_escape_string($mysqli,$gimnasio),
               mysqli_real_escape_string($mysqli,$spa),
               mysqli_real_escape_string($mysqli,$helipuerto),
               mysqli_real_escape_string($mysqli,$accesibilidad),
               mysqli_real_escape_string($mysqli,$nana),
               mysqli_real_escape_string($mysqli,$transporte),
               mysqli_real_escape_string($mysqli,$vigilancia_privada),
               mysqli_real_escape_string($mysqli,$servicio_medico),
               mysqli_real_escape_string($mysqli,$chofer),
               mysqli_real_escape_string($mysqli,$alquiler_vehiculos),
               mysqli_real_escape_string($mysqli,$cafetin),
               mysqli_real_escape_string($mysqli,$restaurant),
               mysqli_real_escape_string($mysqli,$bar),
               mysqli_real_escape_string($mysqli,$discotheca),
               mysqli_real_escape_string($mysqli,$golf),
               mysqli_real_escape_string($mysqli,$tenis),
               mysqli_real_escape_string($mysqli,$basket),
               mysqli_real_escape_string($mysqli,$tienda),
               mysqli_real_escape_string($mysqli,$cant_habitaciones),
               mysqli_real_escape_string($mysqli,$parque),
               mysqli_real_escape_string($mysqli,$terraza),
               mysqli_real_escape_string($mysqli,$tripadvisor),
               mysqli_real_escape_string($mysqli,$tripadvisor_premio),
               mysqli_real_escape_string($mysqli,$fecha_creacion),
               mysqli_real_escape_string($mysqli,$email_reserva));
                $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli)){
    echo '<div class="callout callout-success text-center" role="alert">
                    Los datos han sido cambiados satisfactoriamente!</div>';
    $ahora=time();
    graba_LOG("Fue Agregado: $razon_social",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
}else{
    echo '<div class="callout callout-warning text-center" role="alert">
                Ha ocurrido un error inesperado.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio.</div>';
}
unset($result,$sql,$email,$headers);
$_POST = array();
