<?php

function LISTAR_PRECIOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar los precios de las habitaciones</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=PrecioHabitaciones" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Precios de las Habitaciones
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
';
    return;
}
function LISTAR_PRECIO_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
?>
<script type="text/javascript">

</script>
<?php
    if ($_SESSION["id_hotel"] == 0){
        $id = $_REQUEST["id_hotel"];
    }else{
        $id = $_SESSION["id_hotel"];
    }
    $fecha = time();
    $sql = sprintf("SELECT * FROM hesperia_post_precio WHERE id_hotel = '%s' && fecha_precio >= '%s' ORDER BY id_hotel, id_habitacion && procesado = 0 DESC",
                   mysqli_real_escape_string($mysqli,$id),
                   mysqli_real_escape_string($mysqli,$fecha));
//    die($sql);
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $cant = 0;
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de habitaciones</h3>
                        </div>
                        <div class="box-body">';
    while ($resultprecioh=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $sqlH = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_hotel = '%s' && id_habitacion = '%s'",
                        mysqli_real_escape_string($mysqli,$id),
                        mysqli_real_escape_string($mysqli,$resultprecioh["id_habitacion"]));
        //    die($sql);
        $resultH = QUERYBD($sqlH,$hostname,$user,$password,$db_name);
        $resultadoh=mysqli_fetch_array($resultH,MYSQLI_ASSOC);
        $cant = 1;
        echo '                      <div class="list-group">
                                <span class="list-group-item clearfix">
                                  <strong>Habitación:</strong> '.$resultadoh["nombre_habitacion"].' <strong>Precio:</strong> '.$resultprecioh["precio"].' <strong>Promoción:</strong> '.$resultprecioh["precio_promocion"].' <strong>Valido hasta:</strong> '.date("d-m-Y",$resultprecioh["fecha_precio"]).' <strong>Creado:</strong> '.date('d/m/y',$resultprecioh["fecha"]).'
                                    <span class="pull-right">
                                    <a href="home.php?go=ActualizarPreciosHabitacion&id='.$resultprecioh["id"].'"  btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                            Editar
                                        </a>
                                    </span>
                                </span>
                            </div>';
    }
    if ($cant == 0){
        echo '
                            <div class="callout callout-danger text-center">
                                <h4>
                                    Disculpe
                                </h4>
                                <p>
                                    Actualmente no hay habitaciones en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
                                </p>
                            </div>
                            ';
    }
    echo '
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
';
    return;
}
function ACTUALIZA_PRECIOS_FECHAS_FUTURAS($mysqli,$data,$hostname,$user,$password,$db_name){
    $id = $_GET["id"];
    $sql = sprintf("SELECT * FROM hesperia_post_precio WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
    $sql = sprintf("SELECT id_hotel,nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                   mysqli_real_escape_string($mysqli,$rows["id_habitacion"]));
    $resulth = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowshabitacion = mysqli_fetch_array($resulth,MYSQLI_ASSOC);
    $sql = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$rowshabitacion["id_hotel"]));
    $results = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowshotel = mysqli_fetch_array($results,MYSQLI_ASSOC);
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Cambiar precios y fecha de la habitacion: <strong>'.$rowshabitacion["nombre_habitacion"].'</strong> del hotel <strong>'.$rowshotel["nombre_sitio"].'</strong>
                    </h3>
                </div>
                <div class="box-body">
<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post"  name="CambioPrecioHabitacion" id="CambioPrecioHabitacion" onsubmit="return ActualizaPreciosHabitacion(); return document.MM_returnValue">
                                          <div id="CambiarPrecioHabitacion"></div><br/>
<p>En este apartado puede editar la información sobre el precio de la habitación, su precio promocional y la fecha en la que estara disponible estos precios.</p>
<p>Recuerde que los precios deben ser numeros enteros, sin usar comas o puntos para decimales.</p><br/><br/>
                                <div class="row">
                                    <div class="col-lg-4">
                                         <div class="form-group">
                                              <label for="precio">Precio Bs:</label>
                                            <input type="number" class="form-control" name="precio" id="precio" placeholder="Precio" value="'.$rows["precio"].'"/>
                                          </div>
                                    </div>
                                    <div class="col-lg-4">
                                         <div class="form-group">
                                              <label for="precio">Precio Promoción Bs:</label>
                                            <input type="number" class="form-control" name="precio_promocion" id="precio_promocion" placeholder="Precio Promocion" value="'.$rows["precio_promocion"].'">
                                          </div>
                                    </div>
                                    <div class="col-lg-4">
                                         <div class="form-group">
                                              <label for="precio">Precio Promoción Bs:</label>
                                            <input type="text" class="form-control" name="fecha_precio" data-date-format="dd/mm/yyyy" id="dp1" placeholder="Precio Promocion" value="'.date("d-m-Y",$rows["fecha_precio"]).'">
                                          </div>
                                    </div>
                                </div>
        <div class="box-footer">
            <div class="col-lg-6 pull-right">
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Cambiar informacion
                </button>
                <input type="hidden" value="'.$id.'" name="id" id="id">
            </div>
        </div>
                                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
';
    return;
}
?>
