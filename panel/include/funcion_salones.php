<?php

function CREAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agrega un nuevo Salon</h3>
                        </div>';
    if(isset($_SESSION["mensaje"])){
        echo $_SESSION["mensaje"];
        unset($_SESSION["mensaje"]);
    }echo'
                        <div class="box-body">
                        <form enctype="multipart/form-data" action="include/admin_agrega_salones.php" role="form" method="post" onsubmit="return AgregarSalones(); return document.MM_returnValue" name="FormAgregarSalones" id="FormAgregarSalones">
                                <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece este salón</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
              <option selected="selected" value="0">Seleccione</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    echo'
                                    <div class="form-group">
                                        <label for="nombre_paquete">Nombre del Salón</label>
    <input type="text" class="form-control" name="nombre_Salon" id="nombre_Salon" placeholder="Nombre del Salón">
                                    </div>
                                    <div class="form-group">
                                        <label for="descripcion_paquete">Descripción del Salón</label>
                                        <textarea class="form-control" name="caracteristicas" id="summernote"></textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Crear Salón
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function ACTUALIZAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Actualizar Salones</h3>
                        </div>
                        <div class="box-body">';
    if(isset($_SESSION["mensaje"])){
        echo $_SESSION["mensaje"];
        unset($_SESSION["mensaje"]);
    }echo'
                            <form enctype="multipart/form-data" action="include/admin_actualiza_salones.php" role="form" method="post" onsubmit="return AgregarPaquetes(); return document.MM_returnValue" name="FormAgregarPaquetes" id="FormAgregarPaquetes">
<div id="InformacionAgregarExperiencias"></div>';
    $id_salon =   $_GET["id_salon"];
    $id_hotel =   $_GET["id_hotel"];
    $sql = sprintf("SELECT * FROM hesperia_salones WHERE id_salon = '%s' && id_hotel ='%s'",
                   mysqli_real_escape_string($mysqli,$id_salon),
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $resultSalones = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowsalones = mysqli_fetch_array($resultSalones,MYSQLI_ASSOC);
    echo'
                                <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece este salón</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
            <option value="0">Elija Hotel</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            if($rows["id"] == $id_hotel){
                echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
            }else{
                echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
            }
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    echo'
                                    <div class="form-group">
                                        <label for="nombre_expe">Nombre de la experiencia</label>
<input type="text" class="form-control" name="nombre_Salon" id="nombre_Salon" placeholder="Nombre de la experiencia" value="'.$rowsalones["nombre_Salon"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_expe">Descripcion De la Experiencia</label>
            <textarea class="form-control" name="caracteristicas" id="summernote">'.$rowsalones["caracteristicas"].'</textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                            <input type="hidden" id="imagenactual" name="imagenactual" value="'.$rowsalones["imagen"].'">
                                        <p class="help-block">Para ver la imagen actual haga clic
                                            <a href="#0" title="Ver imagen" data-toggle="modal" data-target="#myModal">
                                                aquí
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Actualizar Salones
                                        </button>
                                <input type="hidden" value="'.$rowsalones["id_salon"].'" name="id_salon" id="id_salon">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger" title="Cerrar">×</span>
                </button>
                <h4 class="modal-title">Imagen actual</h4>
            </div>
            <div class="modal-body">';
    if(!empty($rowsalones["imagen"])){
        echo'<img src="../img/salones/'.$rowsalones["imagen"].'" class="img-responsive" style="margin:0 auto;">';
    }else{
        echo'
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay una imagen para mostrar
                </p>
            </div>';
    }
    echo'
            </div>
            <div class="modal-footer">
                <p class="help-block">
                    Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.
                </p>
            </div>
        </div>
    </div>
</div>';

    return;
}

function LISTAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de Salones</h3>
                        </div>
                        <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        $id_hotel = $_REQUEST["id_hotel"];
    }else{
        $id_hotel = $_SESSION["id_hotel"];
    }
    if (!empty($_GET["ok"])){
        unset($_GET["ok"]);
    }
    $cant = 0;
    $sql =  sprintf("SELECT id_salon,nombre_Salon
                    FROM hesperia_salones
                    WHERE id_hotel = '%s'",
                    mysqli_real_escape_string($mysqli,$id_hotel));
    $resultPaquetes = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultPaquetes,MYSQLI_ASSOC)){
        $cant = 1;
        echo'
            <div class="list-group">
                <span class="list-group-item clearfix">
                    '.$rows["nombre_Salon"].'
                    <span class="pull-right">
<a href="home.php?go=ActualizarSalones&id_salon='.$rows["id_salon"].'&id_hotel='.$id_hotel.'" class="btn btn-xs btn-info" title="Editar">
                            <i class="fa fa-pencil"></i>
                            Editar
                        </a>
<a href="home.php?go=VerSalones&id_salon='.$rows["id_salon"].'&id_hotel='.$id_hotel.'" class="btn btn-xs btn-danger" title="Eliminar">
                            <i class="fa fa-trash-o"></i>
                            Eliminar
                        </a>
                    </span>
                </span>
            </div>';
    }
    if ($cant == 0){
        echo '
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>Actualmente no hay Salones.
    <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">
                    <strong>Regresar al menu anterior</strong>
                    </a>
                </p>
            </div>';
    }else{
        if (isset($_REQUEST["id_salon"])) {
            $id = $_REQUEST["id_salon"];
            $sql = sprintf("SELECT imagen FROM hesperia_salones WHERE id_salon = '%s'",
                           mysqli_real_escape_string($mysqli,$id));
            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
            $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
            $imagen = "../img/salones/".$rows["imagen"];
            @unlink($imagen);
            $sql = sprintf("DELETE FROM hesperia_salones WHERE id_salon = '%s'",
                           mysqli_real_escape_string($mysqli,$id));
            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
            echo'<div class="alert alert-success" role="alert"><p>Procesado acción de eliminación</p>
        <meta http-equiv="refresh" content="3; url=home.php?go=VerSalones&id_hotel='.$id_hotel.'&tok='.time().'&ok=1"/>
            </div>';
        }
    }
    echo'<p class="help-block">Haga clic<a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"> <strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    $_SESSION["mensaje"] = '';
    unset($_SESSION["mensaje"]);
    return;
}
function FORM_SELECION_HOTEL_SALONES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar los Salones</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=VerSalones" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Salones
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
