<?php 

 function testing($hostname,$user,$password,$db_name){
  $sql="SELECT * FROM hesperia_v2_banner where destino ='home' order by posicion asc";
  $resultPortada=QUERYBD($sql,$hostname,$user,$password,$db_name);
  
  echo "
	<div class='container-fluid'>
	     <h2>Banner</h2>

	 	<div class='row'>
	 	<div class='col-md-12'>
	        <p><type='button' class='btn btn-primary btn-lg' data-toggle='modal' data-target='#nuevo-item-modal'>Nuevo</button></p>

	     </div>
	     <br>
  ";
  $i=0;
  while ($reg = mysqli_fetch_assoc($resultPortada)){
  	if($reg["ind_activo"]=="S"){
  	   $activo = "Si";
  	   $checked="checked";	
  	}else{
  		$activo="No";
  		$checked=" ";
  	}
  	 foreach ($reg as $key => $value) {

                $reg[$key] = utf8_encode($reg[$key]);
            }
  	echo "
  	<div class='col-md-3'>
	 	<div class='thumbnail equalize'>
	 		<form id='form-banner-$i'>
	 			<img class='img-responsive' src='https://hoteleshesperia.com.ve/img/".$reg["nombre_img"]."'>
		 		<div class='caption' style='min-height:200px;'>

		 			<p class='visible-caption'>$reg[titulo]</p>
		 			<p class='visible-caption'>$reg[descripcion]</p>
		 			<p class='visible-caption'><b>Activo:</b> $activo</p>
		 			<p class='visible-caption'><b>Posicion:</b> $reg[posicion]</p>
		 			<p class='oculto hidden'>
		 			   <input type='text' name='titulo' id='titulo-$i' class='form-control' placeholder='Titulo' value='".$reg["titulo"]."'>
		 			</p>
		 			<p class='oculto hidden'>
		 			    <textarea class='form-control' rows='3' id='descripcion-$i' name='descripcion' placeholder='Descripcion' >".trim($reg['descripcion'])."</textarea>

		 			</p>		
		 			<p><input class='oculto hidden' id='foto-banner-$i' name='foto-banner' type='file'></p>
		 			<p class='hidden oculto'><input type='text' class='form-control' id='url-$i' placeholder='Url' value='".$reg["url"]."'></p>		
		 			<p class='oculto hidden'>
		 				<label class='checkbox-inline'>
		 					<input type='checkbox' id='activo-$i' name='activo' value='S' $checked> Activo
		 				</label>
		 			</p>
		 			<p class='oculto hidden'>
		 			    <input type='number' name='posicion' id='posicion-$i' class='form-control' placeholder='Posicion' value='".$reg["posicion"]."'>
		 			</p>		
		 			
		 			<p>
		 				<button type='button' class='btn btn-default oculto hidden guardar'
		 				    data-tipo='banner' data-valuetipo='$i' value='$reg[id_banner]'>
		 				    Guardar
		 				</button>

		 			    <button type='button' class='btn btn-danger oculto hidden eliminar' data-tipo='banner' 
		 			    data-anterior='$reg[nombre_img]' value='$reg[id_banner]' data-toggle='modal' data-target='#delete-modal'>
		 			        Eliminar
		 			    </button>

		 			</p>
		 			<p class='text-right'>
		 			  <button type='button' class='btn btn-link editar'
		 			   data-tipo='banner' data-valuetipo='$i' value='$reg[id_banner]'>
		 			       Editar
		 			   </button>
		 			  
		 			</p>		 			
		 		</div>
	 		</form>
	 	</div>
	 </div>

  	";
 	$i++;
  }

  echo "
  		<hidden id='total-banner' value='$i'></hidden>
  	 	</div>
	</div>

  ";
}

function array_query($mysqli, $query, $tipo){
  
  $result = $mysqli->query($query);
  $numRows = $result->num_rows;

  $retorno;


  switch ($tipo) {
  	case 'single':
  		if ($numRows!=null && $numRows>0) 
  			$retorno = $result;
  		else
  			$retorno= 0;
  		break;

  	case 'array':

  		if ($numRows!=null && $numRows>0) {
      		for ($set = array (); $row = mysqli_fetch_assoc($result); $set[] = $row);
    		$retorno= $set;
    	}else{
    		$retorno= 0;
    	}
  		break;
  	
  	default:
  		$retorno -1;
  		break;
  }

  return $retorno;

}
function cargar_titulos_index($mysqli, $ambito){

  $query = "SELECT * FROM hesperia_v2_portada";
  
  switch ($ambito) {
  	case 'index':
  		//POR SI ACASO USAMOS LA FUNCION EN EL INDEX DE LA PAG
  		//while ($reg=$result->mysqli_fetch_assoc()) {
  		//	echo "al index";
  		//}
  		break;

  	case 'panel':
  		$result= array_query($mysqli, $query, "single");

  		echo "
  			<div class='container-fluid'>
  				 <h2>Titulos</h2>
  			    <div class='row'>
  		";
  		$i=0;
  		while ($reg=mysqli_fetch_assoc($result)) {
  			foreach ($reg as $key => $value) {
                $reg[$key] = utf8_encode($reg[$key]);
            }
  			echo "
			  	<div class='col-md-4'>
				 	<div class='thumbnail'>
				 		<form id='form-titulo-$i'>
				 			<img class='img-responsive' style='margin:0 auto' src='https://hoteleshesperia.com.ve/img/".$reg["url_imagen"]."'>
					 		<div class='caption'>

					 			<p class='visible-caption'>$reg[titulo_columna]</p>
					 			<p class='visible-caption'>$reg[descripcion_columna]</p>
					 			<p class='hidden oculto'><input type='text' class='form-control' id='titulo-$i' placeholder='Titulo' value='".$reg["titulo_columna"]."'></p>
					 			<p class='hidden oculto'>
					 			    <textarea class='form-control' rows='3' id='descripcion-$i' placeholder='Descripcion'>".trim($reg['descripcion_columna'])."</textarea>

					 			</p>
					 					
					 			<p><input class='oculto hidden' id='foto-titulo-$i' name='foto-titulo' type='file'></p>
					 			<p>
					 			  <button type='button' class='btn btn-default oculto hidden guardar'
					 				    data-tipo='titulo' data-valuetipo='$i' value='$reg[id_columna]'>
					 				    Guardar
					 			  </button>			 			  
					 			</p>
					 			   
					 			<p class='text-right'>
					 			   <button type='button' class='btn btn-link editar'  data-tipo='titulo' data-valuetipo='$i' value='$reg[id_columna]'>Editar</button>
					 			</p>
		 					 			
					 		</div>
				 		</form>
				 	</div>
				 </div>
			  	";
			 $i++;
  		}
  		echo "
  		 		</div>
  			</div>

  		";
  		break;
  	
  }

}

function cargar_items_index($mysqli){
	$query = "SELECT a.*, b.razon_social from hesperia_v2_item_portada a inner join hesperia_settings b
	 on (a.id_hotel=b.id)";
	$reg= array_query($mysqli, $query, "array");
	
	echo "
		<div class='container-fluid'>
			<h2>Paquetes</h2>
			<div class='row'>		
	";

	for ($i=0; $i <count($reg) ; $i++) { 
	
		if($reg[$i]["id_tipo_item"]==1){
			echo "
			  	<div class='col-md-4'>
				 	<div class='thumbnail'>
				 		<form id='form-paquete-$i'>
				 			<img class='img-responsive' style='margin:0 auto' src='https://hoteleshesperia.com.ve/img/".$reg[$i]["url_imagen"]."'>
					 		<div class='caption'>

					 			<p class='visible-caption'>".utf8_encode($reg[$i]["titulo_item"])."</p>
					 			<p class='visible-caption'>".utf8_encode($reg[$i]["descripcion_item"])."</p>
					 			<p class='visible-caption'>hoteleshesperia.com.ve/paquetes/".$reg[$i]["url"]."</p>
					 			<p class='visible-caption'><strong>".$reg[$i]["razon_social"]."</strong></p>
					 			<p class='hidden oculto'><input type='text' class='form-control' id='titulo-$i' placeholder='Titulo' value='".utf8_encode($reg[$i]["titulo_item"])."'></p>
					 			<p class='hidden oculto'>
					 			    <textarea class='form-control' rows='3' id='descripcion-$i' placeholder='Descripcion'>".trim(utf8_encode($reg[$i]['descripcion_item']))."</textarea>
					 			</p>";

					 			echo("<p class='hidden oculto'>".hotel_selected($reg[$i]["id_hotel"],$i)."</p>");

					 			echo"
					 			<p class='hidden oculto'><input type='text' class='form-control' id='url-$i' placeholder='Url' value='".utf8_encode($reg[$i]["url"])."'></p>	
					 			<p><input class='hidden oculto' id='foto-paquete-$i' name='foto-paquete' type='file'></p>
					 			<p>
					 			  <button type='button' class='btn btn-default oculto hidden guardar'
					 				    data-tipo='paquete' data-valuetipo='$i' value='".$reg[$i]['id_item']."'>
					 				    Guardar
					 			  </button>			 			  
					 			</p>
					 			<p class='text-right'>
					 			  <button type='button' class='btn btn-link editar'  data-tipo='paquete' data-valuetipo='$i' value='".$reg[$i]['id_item']."'>Editar</button>
					 			</p>		 			
					 		</div>
				 		</form>
				 	</div>
				 </div>
			  	";
		}
	}

	echo "
			</div>
		</div>

	";

	echo "
		<div class='container-fluid'>
			<h2>Habitaciones</h2>
			<div class='row'>		
	";

	for ($i=0; $i <count($reg) ; $i++) { 
		if($reg[$i]["id_tipo_item"]==2){
			echo " 
			  	<div class='col-md-3'>
				 	<div class='thumbnail'>
				 		<form id='form-habitacion-$i'>
				 			<img class='img-responsive' style='margin:0 auto' src='https://hoteleshesperia.com.ve/img/".$reg[$i]["url_imagen"]."'>
					 		<div class='caption'>

					 			<p class='visible-caption'>".utf8_encode($reg[$i]["titulo_item"])."</p>
					 			<p class='visible-caption'>".utf8_encode($reg[$i]["descripcion_item"])."</p>
					 			<p class='visible-caption'>https://hoteleshesperia.com.ve/".utf8_encode($reg[$i]["url"])."</p>
					 			<p class='visible-caption'><strong>".$reg[$i]["razon_social"]."</strong></p>
					 			<p class='hidden oculto'><input type='text' class='form-control' id='titulo-$i' placeholder='Titulo' value='".utf8_encode($reg[$i]["titulo_item"])."'></p>
					 			<p class='hidden oculto'>
					 			    <textarea class='form-control' rows='3' id='descripcion-$i' placeholder='Descripcion'>".trim(utf8_encode($reg[$i]['descripcion_item']))."</textarea>

					 			</p>";

					 			echo("<p class='hidden oculto'>".hotel_selected($reg[$i]["id_hotel"],$i)."</p>");

					 			echo"
					 			<p class='hidden oculto'><input type='text' class='form-control' id='url-$i' placeholder='Url' value='".utf8_encode($reg[$i]["url"])."'></p>	
					 			<p><input class='hidden oculto' id='foto-habitacion-$i' name='foto-habitacion' type='file'></p>
					 			<p>
					 			  <button type='button' class='btn btn-default oculto hidden guardar'
					 				    data-tipo='habitacion' data-valuetipo='$i' value='".$reg[$i]['id_item']."'>
					 				    Guardar
					 			  </button>			 			  
					 			</p>
					 			<p class='text-right'>
					 			  <button type='button' class='btn btn-link editar'  data-tipo='habitacion' data-valuetipo='$i' value='".$reg[$i]['id_item']."'>Editar</button>
					 			</p>		 			
					 		</div>
				 		</form>
				 	</div>
				 </div>
			  	";
		}
	}
	echo "
			</div>
		</div>

	";
}

function modales(){
	echo "
		<div class='modal fade' id='nuevo-item-modal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
		  <div class='modal-dialog' role='document'>
		    <div class='modal-content'>
		      <div class='modal-header'>
		        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
		            <span aria-hidden='true'>&times;</span>
		        </button>
		        <h4 class='modal-title' id='myModalLabel'>Nuevo Item</h4>
		      </div>
		      <div class='modal-body'>
		        <form>
		            <div class='form-group'>
			            <label for='recipient-name' class='control-label'>Titulo</label>
			            <input type='text' class='form-control' name='titulo-modal' id='titulo-modal'>
			        </div>
			        <div class='form-group'>
			            <label for='message-text' class='control-label'>Descripcion</label>
			            <textarea class='form-control' name='descripcion-modal' id='descripcion-modal'>

			            </textarea>
			        </div>

			        <div class='form-group'>
			        	<label for='message-text' class='control-label'>Foto</label>
			            <input class='form-control' id='foto-item-modal' name='foto-item-modal' type='file'>
			        </div>

			        <div class='form-group'>
			        	<label for='message-text' class='control-label'>Tipo de Item</label>
			            <select class='form-control' id='tipo-item-modal'>
						  <option value='banner'>Banner</option>
						  <option value='habitacion'>Habitacion</option>
						</select>
			        </div>

			        <div class='form-group' id='hotel-container'>
			        	<label for='message-text' class='control-label'>Hotel</label>
			            <select class='form-control' id='idHotel'>
						  <option value='1'>Hesperia WTC Valencia</option>
						  <option value='2'>Hesperia Isla Margarita</option>
						  <option value='3'>Hesperia Playa el Agua</option>
						  <option value='4'>Hesperia Eden Club</option>
						</select>
			        </div>

			        <div class='form-group' id='url-content'>
			            <label for='recipient-name' class='control-label'>Url</label>
			            <input type='text' class='form-control' name='url-modal' id='url-modal'>
			        </div>



		        </form>
		      </div>
		      <div class='modal-footer'>
		        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
		        <button type='button' class='btn btn-primary' id='aceptar-nuevo-item'>Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>


		<div class='modal fade' id='delete-modal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
		  <div class='modal-dialog' role='document'>
		    <div class='modal-content'>
		      <div class='modal-header'>
		        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
		            <span aria-hidden='true'>&times;</span>
		        </button>
		        <h4 class='modal-title' id='myModalLabel'>Advertencia</h4>
		      </div>
		      <div class='modal-body'>
		       <h4 class='text-center'>¿Estas seguro que deseas eleminar este elemento permanentemente?</h4>
		      </div>
		      <div class='modal-footer'>
		        <button type='button' class='btn btn-default' data-dismiss='modal'>Cancelar</button>
		        <button type='button' class='btn btn-danger' id='aceptar-delete-item'>Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>

	";
}

function hotel_selected($numero, $id){
	$array_hoteles= array("Hesperia WTC Valencia", "Hesperia Isla Margarita", "Hesperia Playa el Agua", "Hesperia Eden Club");

	$select="<select class='form-control' id='idHotel-$id'>\n";
	for ($i=0; $i <count($array_hoteles) ; $i++) {
		$indice = $i+1;
		if ($numero ==$indice) {
			$select.="<option selected value='$indice'>$array_hoteles[$i]</option>";
		}else{
			$select.="<option value='$indice'>$array_hoteles[$i]</option>";
		}
	}
	
	$select.="</select>";					 				
	
	return $select;								 
}
 ?>

