<?php

session_start();

if (empty($_SESSION["referencia"])) {
    echo '<div class="alert alert-danger" role="alert">Ha ocurrido un error inesperado.<br/>Su sesion ha expirado</div>';
    die(); }
$tiempo =   time()*2;
function verifica_Archivo() {
    $allowed = array('png', 'jpg');
    if(isset($_FILES['archivo']['name']) && $_FILES['archivo']['error'] == 0){
        $extension = pathinfo($_FILES['archivo']['name'], PATHINFO_EXTENSION);
        if(!in_array(strtolower($extension), $allowed)){
        $_SESSION["mensaje"] = '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado.<br/>Su sesion ha expirado, o la extension de las imagenes es invalido</div>';
            echo '<meta http-equiv="refresh" content="0; url=../home.php?go=VerInfoGeneral#tab_2&tok='.md5($tiempo).'&ok=0&id_hotel='.$id.'"/>';
    die();
    }
    }
return;
}

$antesdecore2 = 1;
include 'databases.php';
if (!isset($hostname) || !isset($mysqli))
{$_SESSION["mensaje"] = '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado. Revise los valores que esta enviando.<br/>Su sesion ha expirado</div>';
echo '<meta http-equiv="refresh" content="0; url=../home.php?go=VerInfoGeneral&tok='.md5($tiempo).'&id_hotel='.$id.'"/>';
die();
}
    if (!empty($_POST["quienes"])) {
            $quienes = nl2br(trim($_POST["quienes"]));
            if (!empty($_FILES['archivo']['name']))
                {	if ($_FILES['archivo']['name'] != $_POST["imagen_actual"])
                    { verifica_Archivo();
                    move_uploaded_file($_FILES['archivo']['tmp_name'], '../../img/logo/'.$_FILES['archivo']['name']);
                        $nombre = $_FILES['archivo']['name'];
                        $nuevo_nombre = substr(time(),-4).'_'.$_FILES['archivo']['name'];
                        $search = array('%27',' ','-');
                        $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
                        rename ("../../img/logo/$nombre", "../../img/logo/$nuevo_nombre");
                        chmod("../../img/logo/$nuevo_nombre", 0777);
                    }
                }
            else { $nuevo_nombre = $_POST["imagen_actual"]; }
            $id = $_POST["id_hotel"];
            $sql = sprintf("UPDATE hesperia_settings SET
                                        quienes = '%s',
                                        logo = '%s'
                                        WHERE id = '%s'",
                  mysqli_real_escape_string($mysqli,$quienes),
                  mysqli_real_escape_string($mysqli,$nuevo_nombre),
                  mysqli_real_escape_string($mysqli,$id));
            $ahora = time();
           $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
            $_SESSION["mensaje"] =  '<div class="callout callout-success text-center">
                        <p>Quienes modificado exitosamente!.</p></div>';
            graba_LOG("Modificado Quienes",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
            echo '<meta http-equiv="refresh" content="0; url=../home.php?go=VerInfoGeneral&tok='.md5($tiempo).'&ok=1&id_hotel='.$id.'"/>';
die();
    }
$_SESSION["mensaje"] = '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado.</div>';
echo '<meta http-equiv="refresh" content="0; url=../home.php?go=VerInfoGeneral&tok='.md5($tiempo).'&ok=0&id_hotel='.$id.'"/>';
?>
