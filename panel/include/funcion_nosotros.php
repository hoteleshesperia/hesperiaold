<?php
function SOBRE_EL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
$sql = sprintf("SELECT * FROM hesperia_nosotros");
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Informacion sobre "Nosotros Principal"</h3>
                </div>';
                         if(isset($_SESSION["mensaje"])){
                            echo '<div class="box-body">'.$_SESSION["mensaje"].'</div>';
                        }echo'
                <form role="form" enctype="multipart/form-data" action="include/admin_nosotros.php" role="form" method="post" onsubmit="return FormNosotrosV(); return document.MM_returnValue" name="FormNosotros" id="FormNosotros">
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" name="id" id="id" value="'.$row["id"].'">
                            <input type="hidden" name="imagen" id="id" value="'.$row["imagen"].'">
                            <textarea name="texto_nosotros" id="summernote" class="form-control">
                                '.trim($row["texto_nosotros"]).'
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Subir imagen</label>
                            <input type="file" id="file" name="file">
                            <p class="help-block">Para ver la imagen actual haga clic
                                <a href="#0" title="Ver imagen" data-toggle="modal" data-target="#myModal">
                                    aquí
                                </a>
                            </p>
                        </div>
                        <div class="box-footer">
                            <div id="InfoNosotros"></div>
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor,
                                    pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                    Cambiar Datos
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger" title="Cerrar">×</span>
                </button>
                <h4 class="modal-title">Imagen actual</h4>
            </div>
            <div class="modal-body">';
    if(!empty($row["imagen"])){
        echo'<img src="../img/logo/'.$row["imagen"].'" class="img-responsive" style="margin:0 auto;">';
    }else{
        echo'
            <div class="callout callout-danger text-center">
                <h4>Disculpe</h4><p>Actualmente no hay una imagen para mostrar</p>
            </div>';
    }
       echo'
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>';
    $_SESSION["mensaje"] = '';
    unset($_SESSION["mensaje"]);
    return;
}
?>
