<?php
function CALENDARIO($mysqli,$data,$hostname,$user,$password,$db_name)
{ ?>
<script type="text/javascript">
function mostrarCalen(elem, idhotel){
    
    if(elem.value == 0){
        $(".calendar").hide();
    }else{
        var selectId = '#includeCalendar';
        //alert("id_hotel: " + idhotel + " id_hab: " + elem.value);
        var parametros = {
                "idhotel" : idhotel,
                "idhab" : elem.value
        };
        
        $.ajax({
                data:  parametros,
                url:   'calendario.php',
                type:  'post',
                success:  function (response) {
                    $(selectId).html(response);
                }

        });
        //$(".calendar").show();
    }
}
</script>
<?php
    echo'
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-info">

				<div class="box-header with-border">
					<h3 class="box-title">Calendario precios Hotel</h3>
				</div>

				<div class="box-body">';
				if ($_SESSION["nivel"] == 0)
				{ echo '
								<form enctype="application/x-www-form-urlencoded" action="home.php?go=Calendario&tok='.time().'" role="form" method="post" name="FormElegir" id="FormElegir">
						            <div class="form-group">
						                <label for="nombre_hotel"></label>
						                 <select class="form-control" name="id" id="id" onchange="this.form.submit()">
						                    <option value="0">Elija Hotel</option>';
											$sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings 
																	ORDER BY razon_social ASC");
											$resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
											while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
											if (isset($_POST["id"]))
												{ if ($_POST["id"] ==  $rows["id"])
													{ $id = $_POST["id"];
														$_SESSION["nombreHotel"] =  $hotel = $rows["nombre_sitio"];
														
								echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
													}
												}

												echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
												}
								echo'</select>
						            </div>

								</form>';

				}
				else
				{	$id = $_SESSION["id_hotel"];
					$_SESSION["nombreHotel"] = $hotel = $_SESSION["nombre_sitio"];
				}

			if (isset($id))
				{ echo '<h3>Hotel: '.$hotel.'</h3>';
				  $inicio = time();
				   $fin = $inicio + 86400;
					$_SESSION["ElHotel"] = $id;

				echo '<hr/><div class="row">
					<div class="col-lg-6 col-xs-6">

		<div class="col-md-12">
			<div class="row">
				<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return VerificaFechas(); return document.MM_returnValue" name="FormFechaPrecio" id="FormFechaPrecio">
				<h4>Rango de Fechas</h4>

				<div class="col-md-6">
			<input name="id_hotel" type="hidden" value="'.$id.'">
			<input type="hidden" value="'.$hotel.'" name="nombre_hotel">
			<input type="hidden" value="'.date("Y-m-d",time()).'" name="eshoy">
		    <div class="form-group">
		        <label for="tipo_habitacion">Seleccione Habitación</label>
		            <select class="form-control" name="id_hab" id="id_hab" onchange="mostrarCalen(this, '.$id.')">
		            <option value="0" selected="selected">Seleccione</option>';
 						$sql = sprintf("SELECT id_habitacion,id_hotel,nombre_habitacion
								 FROM hesperia_habitaciones
								WHERE id_hotel = '$id' ORDER BY nombre_habitacion ASC");
						 $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
						 while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
							echo '<option value="'.$rows["id_habitacion"].'" class="form-control">'.$rows["nombre_habitacion"].'</option>';
						 }
						 echo'
		            </select>
		    </div>

            <div class="form-group">
                <label>Nuevo Precio:</label>
                <input type="num" name="nuevo_precio" value="0" class="form-control" />
            </div>

            <div class="form-group">
                <label>Excepto:</label><br>
                <input type="checkbox" name="lunes" value="1"/> <label>Lunes</label><br>
                <input type="checkbox" name="martes" value="2"/> <label>Martes</label><br>
                <input type="checkbox" name="miercoles" value="3"/> <label>Miercoles</label><br>
                <input type="checkbox" name="jueves" value="4"/> <label>Jueves</label><br>
                <input type="checkbox" name="viernes" value="5"/> <label>Viernes</label><br>
                <input type="checkbox" name="sabado" value="6"/> <label>Sabado</label><br>
                <input type="checkbox" name="domingo" value="7"/> <label>Domingo</label><br>
            </div>
				</div>

				<div class="col-md-6">

            <div class="form-group">
                <label>Fecha Inicio:</label>
                <input type="date" name="fecha_inicio" value="'.date("Y-m-d",$inicio).'" class="form-control" />
            </div>
            <div class="form-group">
                <label>Fecha Final</label>
                <input type="date" name="fecha_final" value="'.date("Y-m-d",$fin).'" class="form-control" />
            </div>

            <div class="form-group">
                <label>Disponibles:</label>
                <input type="num" name="disponibles" value="0" class="form-control" />
            </div>           

				</div>

            <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                       Cambiar Precios
                    </button>
			</div>

            <div class="form-group">
                <div id="PreciosPorFecha"></div>
            </div>

			</div>
		</div>

			</form>
		</div>
	</div>
 </div>
 <div id="includeCalendar">
 </div>
';
				//include 'calendario.php'; 
				}

echo '
				</div>
			</div>
		</div>
	</div>
</section>';

return;
}


function FORM_ACTUALIZAR_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar las habitaciones</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=VerTodasHabitaciones" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Habitaciones
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function LISTAR_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
    if ($_SESSION["id_hotel"] == 0){
        $id_hotel = $_REQUEST["id_hotel"];
    }else{
        $id_hotel = $_SESSION["id_hotel"];
    }
    $sql = sprintf("SELECT * FROM hesperia_habitaciones WHERE id_hotel = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $cant = 0;
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de habitaciones</h3>
                        </div>
                        <div class="box-body">';
    while ($rowHab=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $cant = 1;
        echo '                      <div class="list-group">
                                <span class="list-group-item clearfix">
                                  '.$rowHab["nombre_habitacion"].'
                                    <span class="pull-right">
                                        <a href="home.php?go=EditarHabitacion&id='.$rowHab["id_habitacion"].'&hotel='.$rowHab["id_hotel"].'" class="btn btn-xs btn-info" title="Editar">
                                            <i class="fa fa-pencil"></i>
                                            Editar
                                        </a>
                                        <a href="home.php?go=VerTodasHabitaciones&id_habitacion='.$rowHab["id_habitacion"].'&id_hotel='.$rowHab["id_hotel"].'" class="btn btn-xs btn-danger" title="Eliminar">
                                            <i class="fa fa-trash-o"></i>
                                            Eliminar
                                        </a>
                                    </span>
                                </span>
                            </div>';
    }
    if ($cant == 0){
        echo '
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay habitaciones en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
                </p>
            </div>';
    }else{
        if (isset($_REQUEST["id_habitacion"])) {
            $id = $_REQUEST["id_habitacion"];
            $sql = sprintf("SELECT imagen1,imagen2,imagen3,imagen4,imagen5,imagen6
                            FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                           mysqli_real_escape_string($mysqli,$id));
//            die($sql);
            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
            $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
            $imagen1 = "../img/habitaciones/".$rows["imagen1"];
            $imagen2 = "../img/habitaciones/".$rows["imagen2"];
            $imagen3 = "../img/habitaciones/".$rows["imagen3"];
            $imagen4 = "../img/habitaciones/".$rows["imagen4"];
            $imagen5 = "../img/habitaciones/".$rows["imagen5"];
            $imagen6 = "../img/habitaciones/".$rows["imagen6"];
            @unlink($imagen1);
            @unlink($imagen2);
            @unlink($imagen3);
            @unlink($imagen4);
            @unlink($imagen5);
            @unlink($imagen6);
            $sql = sprintf("DELETE FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                           mysqli_real_escape_string($mysqli,$id));
            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
            echo'<div class="alert alert-success" role="alert"><p>Procesado acción de eliminación</p>
        <meta http-equiv="refresh" content="3; url=home.php?go=VerTodasHabitaciones&id_hotel='.$id_hotel.'&tok='.time().'&ok=1"/>
            </div>';
        }
    }
    echo'<p class="help-block">Haga clic<a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"> <strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
';
    return;
}

function FORM_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agrega una nueva habitación</h3>
                        </div>
                        <div class="box-body">';
    if(isset($_SESSION["mensaje"])){
        echo '<div id="AccionMensajeRegistro">'.$_SESSION["mensaje"].'</div>';
    }
    echo'<form enctype="multipart/form-data" action="include/admin_agregar_habitacion.php" role="form" method="post" onsubmit="return AgregaHabitacion(); return document.MM_returnValue" name="RegistroA" id="RegistroA">
                                <div class="row">
                                    <div class="col-md-6">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual le pertenece esta habitación</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
              <option selected="selected">Seleccione</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    echo'
                                        <div class="form-group">
                                            <label for="nombre_habitacion">Nombre de la habitacion</label>
<input type="text" class="form-control" name="nombre_habitacion" id="nombre_habitacion" placeholder="Nombre de la habitacion">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cantidad_hab">Cantidad de habitaciones</label>
<input type="number" class="form-control" name="cantidad_hab" id="cantidad_hab" placeholder="Cantidad de habitaciones">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="descripcion">Descripcion de la habitacion</label>
<textarea id="summernote" name="descripcion" class="form-control" row="30" placeholder="Este campo no debe estar vacio"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                Caracteristicas de la habitacion
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="sizehabitaciones">Tama&ntilde;o de la habitacion</label>
                                            <input type="number" class="form-control" name="tamano_hab" id="tamano_hab" placeholder="Ingrese la cantidad de mts2">
                                        </div>
                                    <div class="form-group">
                                        <label for="tipo_habitacion">Tipo habitacion</label>
                                        <select class="form-control" name="tipo_habitacion" id="tipo_habitacion">
                                            <option value="">Seleccione</option>
                                            <option value="1">Matrimonial</option>
                                            <option value="0">Matrimonial doble</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo_cama">Tipo cama</label>
                                        <input type="text" class="form-control" name="tipo_cama" id="tipo_cama" placeholder="Tipo de cama">
                                    </div>
                                    <div class="form-group">
                                        <label for="cama_adicional">Cama adicional</label>
                                        <select class="form-control" name="cama_adicional" id="cama_adicional">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="capacidad_personas">Capacidad personas permitidas</label>
                                    <input type="number" class="form-control" name="capacidad_personas" id="capacidad_personas" placeholder="Capacidad personas permitidas">
                                    </div>
                                    <div class="form-group">
                                        <label for="mini_bar">Mini bar</label>
                                        <select class="form-control" name="mini_bar" id="mini_bar">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="albornoz">Albornoz</label>
                                        <select class="form-control" name="albornoz" id="albornoz">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="telefono">Telefono</label>
                                        <select class="form-control" name="telefono" id="telefono">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="control_remoto_tv">Control remoto tv</label>
                                        <select class="form-control" name="control_remoto_tv" id="control_remoto_tv">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tv">Tv</label>
                                        <select class="form-control" name="tv" id="tv">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="aire_acondicionado">Aire acondicionado</label>
                                        <select class="form-control" name="aire_acondicionado" id="aire_acondicionado">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="servicio_habitacion">Servicio habitacion</label>
                                        <select class="form-control" name="servicio_habitacion" id="servicio_habitacion">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="desayuno">Desayuno</label>
                                        <select class="form-control" name="desayuno" id="desayuno">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="ambiente_musical">Ambiente musical</label>
                                        <select class="form-control" name="ambiente_musical" id="ambiente_musical">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="secador_pelo">Secador pelo</label>
                                        <select class="form-control" name="secador_pelo" id="secador_pelo">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="lavanderia">Lavanderia</label>
                                        <select class="form-control" name="lavanderia" id="lavanderia">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="caja_fuerte">Caja fuerte</label>
                                        <select class="form-control" name="caja_fuerte" id="caja_fuerte">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tv_cable">Tv cable</label>
                                        <select class="form-control" name="tv_cable" id="tv_cable">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jacuzzi">Jacuzzi</label>
                                        <select class="form-control" name="jacuzzi" id="jacuzzi">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="area_estar">Area de estar</label>
                                        <select class="form-control" name="area_estar" id="area_estar">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jarra_agua_vasos">Jarra con agua y vasos</label>
                                        <select class="form-control" name="jarra_agua_vasos" id="jarra_agua_vasos">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cafetera">Cafetera</label>
                                        <select class="form-control" name="cafetera" id="cafetera">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                        <div class="form-group">
                                            <label for="servicio_medico">Servicio medico</label>
                                            <select class="form-control" name="servicio_medico" id="servicio_medico">
                                                <option value="">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="balcon">Balcon</label>
                                            <select class="form-control" name="balcon" id="balcon">
                                                <option value="">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="banera">Banera</label>
                                            <select class="form-control" name="banera" id="banera">
                                                <option value="">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Agregar imagenes de la habitación <small>Debe agregar por lo menos una imagen.</small></h3>
                                        </div>
                                    </div>
                                    <div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen1" name="imagen1" type="file" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen2" name="imagen2" type="file" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen3" name="imagen3" type="file" class="form-control">
                                        </div>
                                        <!-- div class="form-group">
                                            <input id="imagen4" name="imagen4" type="file" class="form-control">
                                        </div -->
                                    </div>
                                    <!-- div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen5" name="imagen5" type="file" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input id="imagen6" name="imagen6" type="file" class="form-control">
                                        </div>
                                    </div -->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-footer">
                                            <div class="col-lg-7">
                                                <p>
                                                    Si ya realizo todo los cambios que deseaba hacer por favor,
                                                    pulse el siguiente boton:
                                                </p>
                                            </div>
                                            <div class="col-lg-5">
                                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                                    Crear Habitación
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    unset($_SESSION["mensaje"]);
    return;
}

function FORM_EDITAR_HABITACION($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Editar habitaciones</h3>
                        </div>
                        <div class="box-body">
                            <div id="AccionMensajeRegistro"></div>
                            <form enctype="multipart/form-data" action="include/admin_actualizar_habitacion.php" role="form" method="post" onsubmit="return AgregaHabitacion(); return document.MM_returnValue" name="RegistroA" id="RegistroA">
                                <div class="row">
                                    <div class="col-md-6">';
    if ($_SESSION["id_hotel"] == 0){
        $hotel = $_GET["hotel"];
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual le pertenece esta habitación</label>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings WHERE id = '$hotel' ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
        echo '<input type="text" class="form-control" value="'.$rows["nombre_sitio"].'" readonly>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    $id = $_GET["id"];
    $sql = sprintf("SELECT * FROM hesperia_habitaciones WHERE id_habitacion = '$id'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultHab = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
    echo'
                                        <div class="form-group">
                                            <label for="nombre_habitacion">Nombre de la habitacion</label>
<input type="text" class="form-control" name="nombre_habitacion" id="nombre_habitacion" placeholder="Nombre de la habitacion" value="'.$rows["nombre_habitacion"].'">
                                        </div>
                                        <div class="row">                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cantidad_hab">Cantidad de habitaciones</label>
<input type="number" class="form-control" name="cantidad_hab" id="cantidad_hab" placeholder="Cantidad de habitaciones" value="'.$rows["cantidad_hab"].'">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="descripcion">Descripcion de la habitacion</label>
<textarea id="summernote" name="descripcion" class="form-control" row="30" placeholder="Este campo no debe estar vacio">'.$rows["descripcion"].'</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                Caracteristicas de la habitacion
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="sizehabitaciones">Tama&ntilde;o de la habitacion</label>
                                    <input type="number" class="form-control" name="tamano_hab" id="tamano_hab" placeholder="Ingrese la cantidad de mts2 " value="'.$rows["tamano_hab"].'">
                                        </div>
                                    <div class="form-group">
                                        <label for="tipo_habitacion">Tipo habitacion</label>
                                        <select class="form-control" name="tipo_habitacion" id="tipo_habitacion">
                                            <option value="1">Matrimonial</option>
                                            <option value="0">Matrimonial doble</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo_cama">Tipo cama</label>
                                        <input type="text" class="form-control" name="tipo_cama" id="tipo_cama" placeholder="Tipo de cama" value="'.$rows["tipo_cama"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="cama_adicional">Cama adicional</label>
                                        <select class="form-control" name="cama_adicional" id="cama_adicional">';
    if($rows["cama_adicional"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="capacidad_personas">Capacidad personas permitidas</label>
                                    <input type="number" class="form-control" name="capacidad_personas" id="capacidad_personas" placeholder="Capacidad personas permitidas" value="'.$rows["capacidad_personas"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="mini_bar">Mini bar</label>
                                        <select class="form-control" name="mini_bar" id="mini_bar">';
    if($rows["mini_bar"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="albornoz">Albornoz</label>
                                        <select class="form-control" name="albornoz" id="albornoz">';
    if($rows["albornoz"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="telefono">Telefono</label>
                                        <select class="form-control" name="telefono" id="telefono">';
    if($rows["telefono"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="control_remoto_tv">Control remoto tv</label>
                                        <select class="form-control" name="control_remoto_tv" id="control_remoto_tv">';
    if($rows["control_remoto_tv"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tv">Tv</label>
                                        <select class="form-control" name="tv" id="tv">';
    if($rows["tv"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="aire_acondicionado">Aire acondicionado</label>
                                        <select class="form-control" name="aire_acondicionado" id="aire_acondicionado">';
    if($rows["aire_acondicionado"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="servicio_habitacion">Servicio habitacion</label>
                                        <select class="form-control" name="servicio_habitacion" id="servicio_habitacion">';
    if($rows["servicio_habitacion"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="desayuno">Desayuno</label>
                                        <select class="form-control" name="desayuno" id="desayuno">';
    if($rows["desayuno"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="ambiente_musical">Ambiente musical</label>
                                        <select class="form-control" name="ambiente_musical" id="ambiente_musical">';
    if($rows["ambiente_musical"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="secador_pelo">Secador pelo</label>
                                        <select class="form-control" name="secador_pelo" id="secador_pelo">';
    if($rows["secador_pelo"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="lavanderia">Lavanderia</label>
                                        <select class="form-control" name="lavanderia" id="lavanderia">';
    if($rows["lavanderia"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="caja_fuerte">Caja fuerte</label>
                                        <select class="form-control" name="caja_fuerte" id="caja_fuerte">';
    if($rows["caja_fuerte"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tv_cable">Tv cable</label>
                                        <select class="form-control" name="tv_cable" id="tv_cable">';
    if($rows["tv_cable"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jacuzzi">Jacuzzi</label>
                                        <select class="form-control" name="jacuzzi" id="jacuzzi">';
    if($rows["jacuzzi"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="area_estar">Area de estar</label>
                                        <select class="form-control" name="area_estar" id="area_estar">';
    if($rows["area_estar"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jarra_agua_vasos">Jarra con agua y vasos</label>
                                        <select class="form-control" name="jarra_agua_vasos" id="jarra_agua_vasos">';
    if($rows["jarra_agua_vasos"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cafetera">Cafetera</label>
                                        <select class="form-control" name="cafetera" id="cafetera">';
    if($rows["cafetera"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                        </select>
                                    </div>
                                        <div class="form-group">
                                            <label for="servicio_medico">Servicio medico</label>
                                            <select class="form-control" name="servicio_medico" id="servicio_medico">';
    if($rows["servicio_medico"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="balcon">Balcon</label>
                                            <select class="form-control" name="balcon" id="balcon">';
    if($rows["balcon"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="banera">Banera</label>
                                            <select class="form-control" name="banera" id="banera">';
    if($rows["banera"]== 1){
        echo '<option value="1" selected>Si</option>
                                                  <option value="0">No</option>';
    }else{
        echo '<option value="0" selected>No</option>
                                                    <option value="1">Si</option>';
    }
    echo'
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                Agregar imagenes de la habitación
                                                <small>Debe agregar por lo menos una imagen.</small>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen1" name="imagen1" type="file" class="form-control">
                      <input id="imagenactual1" name="imagenactual1" type="hidden" value="'.$rows["imagen1"].'">
                                        </div>
                                    </div>
                                    <div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen2" name="imagen2" type="file" class="form-control">
                      <input id="imagenactual2" name="imagenactual2" type="hidden" value="'.$rows["imagen2"].'">
                                        </div>
                                    </div>
                                    <div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen3" name="imagen3" type="file" class="form-control">                         <input id="imagenactual3" name="imagenactual3" type="hidden" value="'.$rows["imagen3"].'">
                                        </div>
                                        <!-- div class="form-group">
                                            <input id="imagen4" name="imagen4" type="file" class="form-control">
                                        </div -->
                                    </div>
                                    <!-- div class="col-md-4"><br/>
                                        <div class="form-group">
                                            <input id="imagen5" name="imagen5" type="file" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input id="imagen6" name="imagen6" type="file" class="form-control">
                                        </div>
                                    </div -->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-footer">
                                            <div class="col-lg-7">
                                                <p>
                                                    Si ya realizo todo los cambios que deseaba hacer por favor,
                                                    pulse el siguiente boton:
                                                </p>
                                            </div>
                                            <div class="col-lg-5">
                                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                                    Actualizar Habitación
                                                </button>
                                <input id="id_hotel" name="id_hotel" type="hidden" value="'.$hotel.'">
                <input id="id_habitacion " name="id_habitacion" type="hidden" value="'.$rows["id_habitacion"].'">
                <input id="imagenactual6" name="imagenactual6" type="hidden" value="'.$rows["imagen6"].'">
                <input id="imagenactual5" name="imagenactual5" type="hidden" value="'.$rows["imagen5"].'">
                <input id="imagenactual4" name="imagenactual4" type="hidden" value="'.$rows["imagen4"].'">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
