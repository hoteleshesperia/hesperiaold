<?php
function LISTA_VIDEO($mysqli,$data,$hostname,$user,$password,$db_name)
{   echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Video promocionales - Canal Youtube</h3>
                    <div class="pull-right">
                        <a href="#0" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-file-video-o"></i>
                            Agregar Nuevo Video
                        </a>
                    </div>
                </div>
                <div class="box-body">';
 $sql = sprintf("SELECT * FROM hesperia_video ORDER BY id DESC");
 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
 $total_video=mysqli_num_rows($result);
 $totalV=@intval($total_video);
 $hay = mysqli_num_rows($result);
 if ($hay < 1)
 { echo '
<div class="text-center"><i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>No hay videos registrados.</p></div>';
 }
 else {
     echo '
                                <p>Total de videos promocionales registrados: <strong>'.$totalV.'</strong></p>
                                <div id="RespuestaVideo"</div>
<div class="row">';
     while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))
     { $id = ($rows["id"]);
      echo '

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rows["video_promocion"].'-16" alt="Video Promocional">
      <div class="caption">
        <p>';
      if ($rows["activo"] == 1) {
          echo '<a class="btn btn-success btn-xs btn-lg btn-block" href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Deshabilitar - No mostrar" role="button" onclick="javascript:DesVideo('.$id.');"><i class="fa fa-times"></i> Deshabilitar</a>'; }else{echo'
<a class="btn btn-success btn-xs btn-lg btn-block" href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Habilitar - Mostrar" role="button" onclick="javascript:HabVideo('.$id.');">
    <i class="fa fa-eye"></i> Habilitar</a>'; }echo'</p>
      </div>
    </div>
  </div>';
     }echo'

</div>';
 }

 echo '
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Nuevo video de promoción</h4>
      </div>
      <div class="modal-body">

        <div id="RespuestaVideoAgregar"</div>
<form enctype="multipart/form-data" action="javascript:void(0)" role="form" method="post" onsubmit="return AgregarVideo();" name="FormVideo" id="FormVideo">';
 if ($_SESSION["id_hotel"] == 0){
     echo '
                    <div class="form-group">
    <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece</label>
                            <select class="form-control" name="id_hotel" id="id_hotel">
                              <option selected="selected" value="0">Seleccione</option>';
                             $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
                             $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
                             while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
                                 echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
                             }
                             echo'</select>
                    </div>';
 }else{
     $id_hotel = $_SESSION["id_hotel"];
     echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
 }
              echo '<div class="form-group">
                        <label for="inputvideo">Video:</label>
                        <input type="text" class="form-control" id="video_promocion" name="video_promocion" value=""/>
                        <p class="help-block">Solo colocar el valor que esta en rojo. https://www.youtube.com/watch?v=<code>X2ABs1PIQMQ</code></p>
                    </div>
                        <div class="col-lg-5 pull-right"><br/>
                            <button type="submit" class="btn btn-primary btn-sm btn-block">
                                Agregar Video
                            </button>
                        </div>
                    </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>';
 echo '
                </div>
            </div>
        </div>
    </div>
</section>';
}
?>
