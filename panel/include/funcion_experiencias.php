<?php
function CREAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agrega una nueva Experiencia</h3>
                        </div>';
    if(isset($_SESSION["mensaje"])){
        echo $_SESSION["mensaje"];
        unset($_SESSION["mensaje"]);
    }echo'
                        <div class="box-body">
                        <form enctype="multipart/form-data" action="include/admin_agrega_experiencia.php" role="form" method="post" onsubmit="return AgregarExperiencias(); return document.MM_returnValue" name="FormAgregarExperiencias" id="FormAgregarExperiencias">
                                <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece esta Experiencia</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
              <option selected="selected" value="0">Seleccione</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    echo'
                                    <div class="form-group">
                                        <label for="nombre_expe">Nombre de la experiencia</label>
        <input type="text" class="form-control" name="nombre_expe" id="nombre_expe" placeholder="Nombre de la experiencia">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_expe">Descripcion De la Experiencia</label>
                                        <textarea class="form-control" name="experiencia" id="summernote"></textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Crear Experiencia
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function ACTUALIZAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Actualizar experiencia</h3>
                        </div>';
    if(isset($_SESSION["mensaje"])){
        echo '<div id="InformacionAgregarExperiencias">'.$_SESSION["mensaje"].'</div>';
        unset($_SESSION["mensaje"]);
    }echo'
                        <div class="box-body">
                            <form enctype="multipart/form-data" action="include/admin_actualiza_experiencia.php" role="form" method="post" onsubmit="return AgregarExperiencias(); return document.MM_returnValue" name="FormAgregarExperiencias" id="FormAgregarExperiencias">
<div id="InformacionAgregarExperiencias"></div>';
    $id =   $_GET["id"];
    $sql = sprintf("SELECT * FROM hesperia_experiencias WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultExp = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultExp,MYSQLI_ASSOC);
    echo'
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="nombre_expe">Nombre de la experiencia</label>
<input type="text" class="form-control" name="nombre_expe" id="nombre_expe" placeholder="Nombre de la experiencia" value="'.$rows["nombre_expe"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_expe">Descripcion De la Experiencia</label>
            <textarea class="form-control" name="experiencia" id="summernote">'.$rows["descripcion_expe"].'</textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                                <input type="hidden" id="imagenactual" name="imagenactual" value="'.$rows["img_expe"].'">
                                        <p class="help-block">Para ver la imagen actual haga clic
                                            <a href="#0" title="Ver imagen" data-toggle="modal" data-target="#myModal">
                                                aquí
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Actualizar Experiencias
                                        </button>
                                        <input type="hidden" value="'.$id.'" name="id" id="id">
                                <input type="hidden" value="'.$rows["id_hotel"].'" name="id_hotel" id="id_hotel">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger" title="Cerrar">×</span>
                </button>
                <h4 class="modal-title">Imagen actual</h4>
            </div>
            <div class="modal-body">';
    if(!empty($rows["img_expe"])){
        echo'<img src="../img/experiencias/'.$rows["img_expe"].'" class="img-responsive" style="margin:0 auto;">';
    }else{
        echo'
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay una imagen para mostrar
                </p>
            </div>';
    }
    echo'
            </div>
            <div class="modal-footer">
                <p class="help-block">
                    Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.
                </p>
            </div>
        </div>
    </div>
</div>';
    return;
}

function FORM_SELECION_HOTEL_EXPE($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar las Experiencias</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=VerExperiencias" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Lista de Experiencias
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function LISTAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de Experiencias</h3>
                        </div>
                        <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        $id_hotel = $_REQUEST["id_hotel"];
    }else{
        $id_hotel = $_SESSION["id_hotel"];
    }
    if (!empty($_GET["ok"])){
        unset($_GET["ok"]);
    }
    $cant = 0;
    $sql = sprintf("SELECT *
                    FROM hesperia_experiencias
                    WHERE id_hotel = '%s'",
                    mysqli_real_escape_string($mysqli,$id_hotel));
    $resultEventos = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultEventos,MYSQLI_ASSOC)){
        $cant = 1;
        echo'
            <div class="list-group">
                <span class="list-group-item">
                    '.$rows["nombre_expe"].'
                    <span class="pull-right">
<a href="home.php?go=ActualizarExperiencias&id='.$rows["id"].'" class="btn btn-xs btn-info" title="Editar">
                            <i class="fa fa-pencil"></i>
                            Editar
                        </a>
                        <a href="home.php?go=VerExperiencias&id='.$rows["id"].'&id_hotel='.$rows["id_hotel"].'" class="btn btn-xs btn-danger" title="Eliminar">
                            <i class="fa fa-trash-o"></i>
                            Eliminar
                        </a>
                    </span>
                </span>
            </div>';
    }
    if ($cant == 0){
        echo '
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay Experiencias. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior"><strong>Regresar al menu anterior</strong></a>
                </p>
            </div>';
    }else{
        if (isset($_REQUEST["id"])) {
            $id = $_REQUEST["id"];
            $sql = sprintf("SELECT id_hotel, img_expe FROM hesperia_experiencias WHERE id = '%s'",
                           mysqli_real_escape_string($mysqli,$id));
            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
            $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
            $imagen = "../img/experiencias/".$rows["img_expe"];
            @unlink($imagen);
            $sql = sprintf("DELETE FROM hesperia_experiencias WHERE id = '%s'",
                           mysqli_real_escape_string($mysqli,$id));
            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
            echo'<div class="alert alert-success" role="alert">
                    <p>Procesado acción de eliminación</p>
                    <meta http-equiv="refresh" content="6; url=home.php?go=VerExperiencias&tok='.time().'&ok=1&id_hotel='.$rows["id_hotel"].'"/>
            </div>';
        }
    }
    echo'<p class="help-block">Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
