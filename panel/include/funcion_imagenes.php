<?php

function SUBIR_IMAGEN($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Subir Imagenes | Sala de Prensa</h3>
                </div>
                <div class="box-body">';
              echo '<form class="form-horizontal" enctype="multipart/form-data" action="home.php?go=SubirImagen" role="form" method="post" onsubmit="return AgregarImagen();" name="ImagenForm" id="ImagenForm">
                        <div class="form-group">
                            <label for="inputtitulo" class="col-sm-2 control-label">
                                Titulo:
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="titulo" name="titulo"/>
								<input type="hidden" name="tipo" id="tipo" value="1"/>
                                <p class="help-block">Máximo 200 caracteres</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputdescripcion" class="col-sm-2 control-label">
                                Descripción:
                            </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="descripcion" id="summernote"></textarea>
                                <p class="help-block">Breve descripcion de la imagen</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputimagen" class="col-sm-2 control-label">
                                Archivo:
                            </label>
                            <div class="col-sm-10">
                                <input type="file" name="archivo" class="form-control" id="archivo"/>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                            Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">
                                    Subir Imagen
                                </button>
                            </div>
                        </div>
                    </form>';
if (!empty($_POST["tipo"]))
    { if (!empty($_FILES['archivo']['name'])) {
            if(move_uploaded_file($_FILES['archivo']['tmp_name'], '../img/prensa/'.$_FILES['archivo']['name'])){
                    $nombre = $_FILES['archivo']['name'];
                    $nuevo_nombre = substr(time(),-4).'_'.$_FILES['archivo']['name'];
                    $search = array('%27',' ','-');
                    $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
                    rename ("../img/prensa/$nombre", "../img/prensa/$nuevo_nombre");
                    chmod("../img/prensa/$nuevo_nombre", 0777);
            }
            else {$nuevo_nombre = 'sin-imagen.png'; }
    }
    $ahora = time();
    $titulo = trim($_POST["titulo"]);
    $descripcion = nl2br(trim($_POST["descripcion"]));
   $sql = sprintf("INSERT INTO hesperia_imagenes_prensa VALUES (NULL, '%s','%s', '%s', '%s')",
                    mysqli_real_escape_string($mysqli,$nuevo_nombre),
                    mysqli_real_escape_string($mysqli,$titulo),
                    mysqli_real_escape_string($mysqli,$descripcion),
                    mysqli_real_escape_string($mysqli,$ahora));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
     echo '<div class="box-footer">
                <div class="callout callout-success text-center">
                    <h4>Se ha guardado la imagen correctamente</h4>
                </div>
                </div>';
            graba_LOG("Agregada imagen de prensa $nuevo_nombre",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
}
echo '
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function ELIMINAR_IMAGEN($mysqli,$data,$hostname,$user,$password,$db_name){
echo '<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Eliminar Imagenes | Sala de Prensa</h3>
                </div>
                <div class="box-body">';
					$sql = sprintf("SELECT id,titulo FROM hesperia_imagenes_prensa ORDER BY id DESC");
					$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					$hay = mysqli_num_rows($result);
						if ($hay < 1){
							echo '
						<div class="text-center"><i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
								<h2>Disculpe</h2> <p>No hay Eliminar Archivos - Boletines - Video | Sala de Prensa.</p></div>';
						}else{
							echo'
					<div id="Eliminacionimagen"></div>
							 <form  class="form-horizontal" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return EliminarImagen();" name="ImagenE" id="ImagenE">
										<div class="form-group">
										    <label for="inputtitulo" class="col-sm-2 control-label">
										        Imagen:
										    </label>
										    <div class="col-sm-10">
										    <select name="id" id="id" class="form-control">
										    <option value="0">Seleccione</option>';
										        while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
										            echo '<option value="'.$rows["id"].'">'.$rows["titulo"].'</option>';
										        }
					echo '						</select>
										    </div>
										</div>
										    <div class="box-footer">
										        <div class="col-lg-7">
										            <p>Esta acción no posee confirmación</p>
										        </div>
										        <div class="col-lg-5">
										            <button type="submit" class="btn btn-primary btn-sm btn-block">
										                Eliminar Archivo
										            </button>
										        </div>
										    </div>
										</form>';
						}
echo'
                </div>
            </div>
        </div>
    </div>
</section>';
return;
}
?>
