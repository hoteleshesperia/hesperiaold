<?php
function FORM_ACTUALIZAR_INSTAPAGO($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar las llaves de Instapago</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=VerInstapago" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio
                                            FROM hesperia_settings
                                            ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo'<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Configuracion de Instapago
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function LISTAR_INSTAPAGO($mysqli,$data,$hostname,$user,$password,$db_name){
    if ($_SESSION["id_hotel"] == 0){
        $id_hotel = $_REQUEST["id_hotel"];
    }else{
        $id_hotel = $_SESSION["id_hotel"];
    }
    $sqlInstap = sprintf("SELECT * FROM hesperia_instapago WHERE id_hotel = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $resultInsta = QUERYBD($sqlInstap,$hostname,$user,$password,$db_name);

    $cant =mysqli_num_rows($resultInsta);
    $rowsInstapago = mysqli_fetch_array($resultInsta,MYSQLI_ASSOC);
    $sql = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $resultSet = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultSet,MYSQLI_ASSOC);
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Panel Instapago</h3>
                        </div>
                        <div class="box-body">';
    if ($cant == 0){
        echo '
        <div id="admin_instapago"></div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Se encuentra actualizando las llaves asignadas al hotel: <strong>'.$rows["nombre_sitio"].'</h3>
                        </div>
                        <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return InstapagoSettings(); return document.MM_returnValue" name="InstapagoSetting" id="InstapagoSetting">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="keyid">Keyid</label>
                                    <input type="text" class="form-control" name="keyid" id="keyid" placeholder="Keyid" value="'.$rowsInstapago["KeyID"].'">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="publickeyid">Publickeyid</label>
                                    <input type="text" class="form-control" name="publickeyid" id="publickeyid" placeholder="Publickeyid" value="'.$rowsInstapago["PublicKeyId"].'">
                                </div>
                            </div>
<br>
<br>
<br>
<br>
<br>
                            <div class="box-footer">
                                <div class="col-lg-6 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                       Actualizar Llaves
                                    </button>
                            <input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">
                                    <input type="hidden" value="1" name="op" id="op">
                                </div>
                            </div>
                        </form>';
    }else{
        echo'
        <div id="admin_instapago"></div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Se encuentra actualizando las llaves asignadas al hotel: <strong>'.$rows["nombre_sitio"].'</h3>
                        </div>
                        <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return InstapagoSettings(); return document.MM_returnValue" name="InstapagoSetting" id="InstapagoSetting">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="keyid">Keyid</label>
                                    <input type="text" class="form-control" name="keyid" id="keyid" placeholder="Keyid" value="'.$rowsInstapago["KeyID"].'">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="publickeyid">Publickeyid</label>
                                    <input type="text" class="form-control" name="publickeyid" id="publickeyid" placeholder="Publickeyid" value="'.$rowsInstapago["PublicKeyId"].'">
                                </div>
                            </div>
<br>
<br>
<br>
<br>
<br>
                            <div class="box-footer">
                                <div class="col-lg-6 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                       Actualizar Llaves
                                    </button>
                        <input type="hidden" value="'.$rowsInstapago["id_hotel"].'" name="id_hotel" id="id_hotel">
                                    <input type="hidden" value="2" name="op" id="op">
                                </div>
                            </div>
                        </form>';
    }
    echo'
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
