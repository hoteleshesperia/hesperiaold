<?php

function CREAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agrega un nuevo Paquete</h3>
                        </div>
                        <div class="box-body">';
                         if(isset($_SESSION["mensaje"])){
                            echo $_SESSION["mensaje"];
                            unset($_SESSION["mensaje"]);
                        }echo'
                        <form enctype="multipart/form-data" action="include/admin_agrega_paquetes.php" role="form" method="post" onsubmit="return AgregarPaquetes(); return document.MM_returnValue" name="FormAgregarPaquetes" id="FormAgregarPaquetes">
                                <div class="box-body">';
    if ($_POST["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece este Paquete</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
              <option selected="selected" value="0">Seleccione</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_POST["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
        $id_tipo_paquete = $_POST["tipo_paquete"];

        /*
    <option value="1">Full day</option>
                                    <option value="2">Dominical</option>
                                    <option value="3">Paquete c / alojamiento</option>
                                    <option value="4">Paquete c / alojamiento y vuelo</option>
                                    <option value="5">Upselling</option>';          
        */
        if($id_tipo_paquete == 1){
            $tipo_paquete = "Full day";
        }
        if($id_tipo_paquete == 2){
            $tipo_paquete = "Dominical";
        }
        if($id_tipo_paquete == 3){
            $tipo_paquete = "Paquete c / alojamiento";
        }
        if($id_tipo_paquete == 4){
            $tipo_paquete = "Paquete c / alojamiento y vuelo";
        }
        if($id_tipo_paquete == 5){
            $tipo_paquete = "Upselling";
        }
    }
    echo'
                                    <div class="form-group">
                                        <label for="nombre_paquete">Tipo de Paquete</label>
                                        <input type="text" class="form-control" value="'.$tipo_paquete.'" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_paquete">Nombre del Paquete</label>
        <input type="text" class="form-control" name="nombre_paquete" id="nombre_paquete" placeholder="Nombre del Paquete">
                                    </div>';
                                    if($id_tipo_paquete == 1 || $id_tipo_paquete == 2 || $id_tipo_paquete == 5){
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete" id="precio_paquete" placeholder="Precio del Paquete">
                                        </div>
                                        <input type="hidden" name="precio_paquete_pension" id="precio_paquete_pension" value="0"/>
                                        <input type="hidden" name="precio_paquete_ti" id="precio_paquete_ti" value="0"/>';
                                    }else{
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio Full por noche A+D del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete" id="precio_paquete" placeholder="Precio por noche alojamiento y desayuno">
                                        </div>';
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio Full por noche A+D+C del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete_pension" id="precio_paquete_pension" placeholder="Precio por noche media pension">
                                        </div>';
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio Full por noche A+TI del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete_ti" id="precio_paquete_ti" placeholder="Precio por noche todo incluido">
                                        </div>';
                                    }

                                    echo '
                                        <div class="form-group">
                                            <label for="price">Precio en dólares</label>
                                            <input type="text" class="form-control" name="price" id="price" placeholder="Precio en dólares (USD)">
                                        </div>';

                                    if($id_tipo_paquete == 1 || $id_tipo_paquete == 2 ){
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_nino_paquete">Precio por niño del Paquete</label>
                                            <input type="text" class="form-control" name="precio_nino_paquete" id="precio_nino_paquete" placeholder="Precio por niño del Paquete" >
                                        </div>
                                        <div class="form-group">
                                            <label for="price_child">Precio por niño en dólares</label>
                                            <input type="text" class="form-control" name="price_child" id="price_child" placeholder="Precio por niño en dólares (USD)">
                                        </div>';
                                    }else{
                                        echo '<input type="hidden" name="precio_nino_paquete" id="precio_nino_paquete" value="0"/>
                                        <input type="hidden" class="form-control" name="price_child" id="price_child" value="0">';
                                    }

                                    
                                    if($id_tipo_paquete == 1 || $id_tipo_paquete == 2){
                                        echo '<input type="hidden" name="full_day" id="full_day" value="1"/>';
                                    }else{
                                        echo '<input type="hidden" name="full_day" id="full_day" value="0"/>';
                                    }

                                    if($id_tipo_paquete == 2){
                                        echo '<input type="hidden" name="dominical" id="dominical" value="1"/>';
                                    }else{
                                        echo '<input type="hidden" name="dominical" id="dominical" value="0"/>';
                                    }

                                    if($id_tipo_paquete == 3 || $id_tipo_paquete == 4){
                                        echo '
                                        <div class="form-group">
                                            <label for="ninos_gratis">Cantidad de niños gratis</label>
                                            <input type="text" class="form-control" name="ninos_gratis" id="ninos_gratis" placeholder="Cantidad de niños gratis"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="noches_gratis">Cantidad de noches gratis</label>
                                            <input type="text" class="form-control" name="noches_gratis" id="noches_gratis" placeholder="Cantidad de noches gratis"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="noches_gratis">¿El número de noches es fijo o configurable por el usuario?</label>
                                            <select class="form-control" name="noches_fijas" id="noches_fijas">
                                                <option value="">Seleccione:</option>
                                                <option value="1">Fijo</option>
                                                <option value="0">Configurable</option>
                                            </select>
                                        </div>';
                                    }else{
                                        echo '<input type="hidden" class="form-control" name="ninos_gratis" id="ninos_gratis" value="0" />
                                        <input type="hidden" class="form-control" name="noches_gratis" id="noches_gratis" value="0" />
                                        <input type="hidden" class="form-control" name="noches_fijas" id="noches_fijas" value="0" />';
                                    }

                                    if($id_tipo_paquete == 4){
                                        echo '
                                        <div class="form-group">
                                            <label for="noches_gratis">Cantidad de pasajeros</label>
                                            <input type="text" class="form-control" name="vuelo" id="vuelo" placeholder="Cantidad de pasajeros"/>
                                        </div>
                                        ';
                                    }else{
                                        echo '<input type="hidden" name="vuelo" id="vuelo" value="0"/>';
                                    }

                                    
                                    if($id_tipo_paquete == 5){
                                        echo '<input type="hidden" name="upselling_paquete" id="upselling_paquete" value="1"/>';
                                    }else{
                                        echo '<input type="hidden" name="upselling_paquete" id="upselling_paquete" value="0"/>';
                                    }                                    

                                    if($id_tipo_paquete == 3 || $id_tipo_paquete == 4){
                                        echo '
                                        <div class="form-group">
                                            <label for="noches_paquete">Noches (Desde)</label>
                                            <input type="text" class="form-control" name="noches_paquete" id="noches_paquete" placeholder="Cantidad de noches mínimas o fijas">
                                        </div>
                                        <div class="form-group">
                                            <label for="noches_paquete">Noches (Hasta)</label>
                                            <input type="text" class="form-control" name="noches_paquete_hasta" id="noches_paquete_hasta" placeholder="Cantidad de noches máximas si el paq es configurable">
                                        </div>';
                                    }else{
                                        echo '<input type="hidden" name="noches_paquete" id="noches_paquete" value="0"/>
                                        <input type="hidden" name="noches_paquete_hasta" id="noches_paquete_hasta" value="0"/>';
                                    }

                                    if($id_tipo_paquete == 5){
                                        echo '<input type="hidden" name="desde_paquete" id="desde_paquete" value="0" />';
                                        echo '<input type="hidden" name="hasta_paquete" id="hasta_paquete" value="0" />';
                                        echo '<input type="hidden" name="porcentaje_paquete" id="porcentaje_paquete" value="0" />';
                                    }else{
                                        echo '
                                        <div class="form-group">
                                            <label for="desde_paquete">Desde - Formato yyyy-mm-dd (¡Indicar solo si no es upselling!)</label>
                                            <input type="text" class="form-control" name="desde_paquete" id="desde_paquete" placeholder="yyyy-mm-dd">
                                        </div>
                                        <div class="form-group">
                                            <label for="hasta_paquete">Hasta - Formato yyyy-mm-dd (¡Indicar solo si no es upselling!)</label>
                                            <input type="text" class="form-control" name="hasta_paquete" id="hasta_paquete" placeholder="yyyy-mm-dd">
                                        </div>
                                        <div class="form-group">
                                            <label for="porcentaje_paquete">Porcentaje Descuento - Formato Ej. 0.15 para 15% (¡Indicar solo si no es upselling y si aplica!)</label>
                                            <input type="text" class="form-control" name="porcentaje_paquete" id="porcentaje_paquete" value="0">
                                        </div>';
                                    }

                                    if($id_tipo_paquete == 5 || $id_tipo_paquete == 1 || $id_tipo_paquete == 2){
                                        echo '<input type="hidden" name="id_hab" id="id_hab" value="0" />';
                                    }else{
                                        echo '
                                            <div class="form-group">
                                                <label for="id_hab">Seleccione la habitación solo si aplica</label>
                                                    <select class="form-control" name="id_hab" id="id_hab">
                                                      <option selected="selected" value="">Seleccione</option>';
                                                $sql = sprintf("SELECT id_habitacion, nombre_habitacion FROM hesperia_habitaciones WHERE id_hotel = '%s' ORDER BY id_habitacion ASC",
                                                 mysqli_real_escape_string($mysqli,$id_hotel));
                                                $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
                                                while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
                                                    echo '<option value="'.$rows["id_habitacion"].'">'.$rows["nombre_habitacion"].'</option>';
                                                }
                                                echo'</select>
                                            </div>
                                        ';
                                    }
                                    echo '
                                    
                                    <div class="form-group">
                                        <label for="partai_paquete">Partaï?</label>
                                        <select class="form-control" name="partai_paquete" id="partai_paquete">
                                            <option value="">Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                            <label for="id_hab">VIP?</label>
                                                <select class="form-control" name="vip" id="vip">
                                                    <option selected="selected" value="">Seleccione</option>
                                                    <option selected="" value="1">Si</option>
                                                    <option selected="" value="0">No</option>                                                    
                                                </select>
                                        </div>
                                    <div class="form-group">
                                        <label for="descripcion_paquete">Descripcion del Paquete</label>
                                        <textarea class="form-control" name="paquete" id="summernote"></textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Crear Paquete
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function ACTUALIZAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Actualizar experiencia</h3>
                        </div>
                        <div class="box-body">';
                         if(isset($_SESSION["mensaje"])){
                            echo $_SESSION["mensaje"];
                             unset($_SESSION["mensaje"]);
                        }echo'
                            <form enctype="multipart/form-data" action="include/admin_actualiza_paquetes.php" role="form" method="post" onsubmit="return AgregarPaquetes(); return document.MM_returnValue" name="FormAgregarPaquetes" id="FormAgregarPaquetes">
<div id="InformacionAgregarExperiencias"></div>';
    $id =   $_GET["id"];

    $id_tipo_paquete = $_GET["tipo_paquete"];

    if($id_tipo_paquete == 1){
            $tipo_paquete = "Full day";
        }
        if($id_tipo_paquete == 2){
            $tipo_paquete = "Dominical";
        }
        if($id_tipo_paquete == 3){
            $tipo_paquete = "Paquete c / alojamiento";
        }
        if($id_tipo_paquete == 4){
            $tipo_paquete = "Paquete c / alojamiento y vuelo";
        }
        if($id_tipo_paquete == 5){
            $tipo_paquete = "Upselling";
        }

        echo '<input type="hidden" name="id_tipo_paquete" id="id_tipo_paquete" value="'.$id_tipo_paquete.'"/>';
    //$id_hotel = $_GET["id_hotel"];
    $sql = sprintf("SELECT * FROM hesperia_paquetes WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultExp = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultExp,MYSQLI_ASSOC);
    echo'
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="nombre_paquete">Tipo de Paquete</label>
                                        <input type="text" class="form-control" value="'.$tipo_paquete.'" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_expe">Nombre del Paquete</label>
<input type="text" class="form-control" name="nombre_paquete" id="nombre_paquete" placeholder="Nombre del Paquete" value="'.$rows["nombre_paquete"].'">
                                    </div>';
                                    if($id_tipo_paquete == 1 || $id_tipo_paquete == 2 || $id_tipo_paquete == 5){
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete" id="precio_paquete" placeholder="Precio del Paquete" value="'.$rows["precio"].'">
                                        </div>
                                        <input type="hidden" name="precio_paquete_pension" id="precio_paquete_pension" value="0"/>
                                        <input type="hidden" name="precio_paquete_ti" id="precio_paquete_ti" value="0"/>';
                                    }else{
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio Full por noche A+D del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete" id="precio_paquete" placeholder="Precio por noche alojamiento y desayuno" value="'.$rows["precio"].'">
                                        </div>';
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio Full por noche A+D+C del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete_pension" id="precio_paquete_pension" placeholder="Precio por noche media pension" value="'.$rows["precio_pension"].'">
                                        </div>';
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_paquete">Precio Full por noche A+TI del Paquete</label>
                                            <input type="text" class="form-control" name="precio_paquete_ti" id="precio_paquete_ti" placeholder="Precio por noche todo incluido" value="'.$rows["precio_todo_inc"].'">
                                        </div>';
                                    }
                                    echo '
                                        <div class="form-group">
                                            <label for="price">Precio en dólares</label>
                                            <input type="text" class="form-control" name="price" id="price" placeholder="Precio en dólares (USD)" value="'.$rows["price"].'">
                                        </div>';

                                    if($id_tipo_paquete == 1 || $id_tipo_paquete == 2 ){
                                        echo '
                                        <div class="form-group">
                                            <label for="precio_nino_paquete">Precio por niño del Paquete</label>
                                            <input type="text" class="form-control" name="precio_nino_paquete" id="precio_nino_paquete" placeholder="Precio por niño del Paquete" value="'.$rows["precio_nino"].'">
                                        </div>
                                        <div class="form-group">
                                            <label for="price_child">Precio por niño en dólares</label>
                                            <input type="text" class="form-control" name="price_child" id="price_child" placeholder="Precio por niño en dólares (USD)" value="'.$rows["price_child"].'">
                                        </div>';
                                    }else{
                                        echo '<input type="hidden" name="precio_nino_paquete" id="precio_nino_paquete" value="0"/>
                                         <input type="hidden" class="form-control" name="price_child" id="price_child" value="0" >';
                                    }

                                    
                                    
                                    if($id_tipo_paquete == 1 || $id_tipo_paquete == 2){
                                        echo '<input type="hidden" name="full_day" id="full_day" value="1"/>';
                                    }else{
                                        echo '<input type="hidden" name="full_day" id="full_day" value="0"/>';
                                    }

                                    if($id_tipo_paquete == 2){
                                        echo '<input type="hidden" name="dominical" id="dominical" value="1"/>';
                                    }else{
                                        echo '<input type="hidden" name="dominical" id="dominical" value="0"/>';
                                    }

                                    if($id_tipo_paquete == 3 || $id_tipo_paquete == 4){
                                        echo '
                                        <div class="form-group">
                                            <label for="ninos_gratis">Cantidad de niños gratis</label>
                                            <input type="text" class="form-control" name="ninos_gratis" id="ninos_gratis" placeholder="Cantidad de niños gratis" value="'.$rows["ninos_gratis"].'"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="noches_gratis">Cantidad de noches gratis</label>
                                            <input type="text" class="form-control" name="noches_gratis" id="noches_gratis" placeholder="Cantidad de noches gratis" value="'.$rows["noches_gratis"].'"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="noches_gratis">¿El número de noches es fijo o configurable por el usuario?</label>
                                            <select class="form-control" name="noches_fijas" id="noches_fijas">
                                                <option value="">Seleccione:</option>';
                                                if($rows["configurable"] == 1){
                                                    echo '
                                                    <option value="1" selected>Fijo</option>
                                                    <option value="0" >Configurable</option>';
                                                }else{
                                                    echo '                                                    
                                                    <option value="1" >Fijo</option>
                                                    <option value="0" selected>Configurable</option>';
                                                }
                                                echo '
                                                <option value="1">Fijo</option>
                                                <option value="0">Configurable</option>
                                            </select>
                                        </div>';
                                    }else{
                                        echo '<input type="hidden" class="form-control" name="ninos_gratis" id="ninos_gratis" value="0" />
                                        <input type="hidden" class="form-control" name="noches_gratis" id="noches_gratis" value="0" />
                                        <input type="hidden" class="form-control" name="noches_fijas" id="noches_fijas" value="0" />';
                                    }

                                    if($id_tipo_paquete == 4){
                                        echo '
                                        <div class="form-group">
                                            <label for="noches_gratis">Cantidad de pasajeros</label>
                                            <input type="text" class="form-control" name="vuelo" id="vuelo" placeholder="Cantidad de pasajeros" value="'.$rows["vuelo"].'"/>
                                        </div>
                                        ';
                                    }else{
                                        echo '<input type="hidden" name="vuelo" id="vuelo" value="0"/>';
                                    }
                                    if($id_tipo_paquete == 5){
                                         echo '<input type="hidden" name="upselling_paquete" id="upselling_paquete" value=1>';
                                    }else{
                                        echo '<input type="hidden" name="upselling_paquete" id="upselling_paquete" value=0>';
                                    }
                                    

                                        if($id_tipo_paquete == 3 || $id_tipo_paquete == 4){
                                        echo '
                                        <div class="form-group">
                                            <label for="noches_paquete">Noches (Desde)</label>
                                            <input type="text" class="form-control" name="noches_paquete" id="noches_paquete" placeholder="Cantidad de noches mínimas o fijas" value="'.$rows["tiempo"].'">
                                        </div>
                                        <div class="form-group">
                                            <label for="noches_paquete">Noches (Hasta)</label>
                                            <input type="text" class="form-control" name="noches_paquete_hasta" id="noches_paquete_hasta" placeholder="Cantidad de noches máximas si el paq es configurable" value="'.$rows["noches_hasta"].'">
                                        </div>';
                                    }else{
                                        echo '<input type="hidden" name="noches_paquete" id="noches_paquete" value="0"/>
                                        <input type="hidden" name="noches_paquete_hasta" id="noches_paquete_hasta" value="0"/>';
                                    }

                                    if($id_tipo_paquete == 5){
                                        echo '<input type="hidden" name="desde_paquete" id="desde_paquete" value="0" />';
                                        echo '<input type="hidden" name="hasta_paquete" id="hasta_paquete" value="0" />';
                                        echo '<input type="hidden" name="porcentaje_paquete" id="porcentaje_paquete" value="0" />';
                                    }else{
                                        echo '
                                        <div class="form-group">
                                            <label for="desde_paquete">Desde - Formato yyyy-mm-dd (¡Indicar solo si no es upselling!)</label>
                                            <input type="text" class="form-control" name="desde_paquete" id="desde_paquete" placeholder="yyyy-mm-dd" value="'.$rows["desde"].'">
                                        </div>
                                        <div class="form-group">
                                            <label for="hasta_paquete">Hasta - Formato yyyy-mm-dd (¡Indicar solo si no es upselling!)</label>
                                            <input type="text" class="form-control" name="hasta_paquete" id="hasta_paquete" placeholder="yyyy-mm-dd" value="'.$rows["hasta"].'">
                                        </div>
                                        <div class="form-group">
                                            <label for="porcentaje_paquete">Porcentaje Descuento - Formato Ej. 0.15 para 15% (¡Indicar solo si no es upselling y si aplica!)</label>
                                            <input type="text" class="form-control" name="porcentaje_paquete" id="porcentaje_paquete" value="'.$rows["porcentaje"].'">
                                        </div>';
                                    }

                                        echo '
                                        <div class="form-group">
                                        <label for="partai_paquete">Partaï?</label>
                                        <select class="form-control" name="partai_paquete" id="partai_paquete">';
                                        if($rows["partai"] == ''){
                                            echo '<option value="" selected>Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>';
                                        }
                                        if($rows["partai"] == '1'){
                                            echo '<option value="" selected>Seleccione</option>
                                            <option value="1" selected>Si</option>
                                            <option value="0">No</option>';
                                        }
                                        if($rows["partai"] == '0'){
                                            echo '<option value="" selected>Seleccione</option>
                                            <option value="1">Si</option>
                                            <option value="0" selected>No</option>';
                                        }
                                        echo'</select>
                                        </div>';

                                        if($id_tipo_paquete == 3 || $id_tipo_paquete == 4){
                                            echo '
                                        <div class="form-group">
                                            <label for="id_hab">Seleccione la habitación solo si no es Upselling y aplica</label>
                                                <select class="form-control" name="id_hab" id="id_hab">';
                                                if($rows["id_tipo_hab"] == ''){
                                                    echo '
                                                    <option selected="selected" value="">Seleccione</option>';
                                                    $sqlHab = sprintf("SELECT id_habitacion, nombre_habitacion FROM hesperia_habitaciones WHERE id_hotel = '%s' ORDER BY id_habitacion ASC",
                                                     mysqli_real_escape_string($mysqli,$rows["id_hotel"]));
                                                    $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
                                                    while ($rowsHab = mysqli_fetch_array($resultHab,MYSQLI_ASSOC)){
                                                        
                                                        echo '<option value="'.$rowsHab["id_habitacion"].'">'.$rowsHab["nombre_habitacion"].'</option>';
                                                        
                                                    }
                                                }else{
                                                    echo '
                                                    <option value="">Seleccione</option>';
                                                    $sqlHab = sprintf("SELECT id_habitacion, nombre_habitacion FROM hesperia_habitaciones WHERE id_hotel = '%s' ORDER BY id_habitacion ASC",
                                                     mysqli_real_escape_string($mysqli,$rows["id_hotel"]));
                                                    $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
                                                    while ($rowsHab = mysqli_fetch_array($resultHab,MYSQLI_ASSOC)){
                                                        if($rows["id_tipo_hab"] == $rowsHab["id_habitacion"]){
                                                            echo '<option value="'.$rowsHab["id_habitacion"].'" selected="selected">'.$rowsHab["nombre_habitacion"].'</option>';
                                                        }else{
                                                            echo '<option value="'.$rowsHab["id_habitacion"].'">'.$rowsHab["nombre_habitacion"].'</option>';
                                                        }
                                                    }
                                                }
                                            echo'</select>
                                        </div>';
                                        }else{
                                            echo '<input type=hidden name="id_hab" id="id_hab" value="0">';
                                        }
                                        echo '
                                        <div class="form-group">
                                            <label for="id_hab">VIP?</label>
                                                <select class="form-control" name="vip" id="vip">';
                                                if($rows["vip"] == ''){
                                                    echo '
                                                    <option selected="selected" value="">Seleccione</option>
                                                    <option selected="" value="1">Si</option>
                                                    <option selected="" value="0">No</option>';
                                                    
                                                }else{
                                                    echo '
                                                    <option value="">Seleccione</option>';
                                                    
                                                        if($rows["vip"] == '1'){
                                                            echo '<option value="1" selected="selected">Si</option>
                                                            <option value="0">No</option>';
                                                        }else{
                                                            echo '<option value="1">Si</option>
                                                            <option value="0" selected="selected">No</option>';
                                                        }
                                                    
                                                }
                                            echo'</select>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_hab">Status Venezuela</label>
                                                <select class="form-control" name="status" id="status">';
                                                if($rows["status"] == '' || $rows["status"] == 'S'){
                                                    echo '
                                                    <option selected="selected" value="S" >Visible</option>
                                                    <option value="N">Oculto</option>';
                                                    
                                                }else{
                                                    echo '
                                                    <option value="S">Visible</option>
                                                    <option value="N" selected="selected">Oculto</option>';
                                                }
                                            echo'</select>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_hab">Status Extranjero</label>
                                                <select class="form-control" name="status_dolar" id="status_dolar">';
                                                if($rows["status_dolar"] == '' || $rows["status_dolar"] == 'S'){
                                                    echo '
                                                    <option selected="selected" value="S" >Visible</option>
                                                    <option value="N">Oculto</option>';
                                                    
                                                }else{
                                                    echo '
                                                    <option value="S">Visible</option>
                                                    <option value="N" selected="selected">Oculto</option>';
                                                }
                                            echo'</select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_expe">Descripcion del Paquete</label>
            <textarea class="form-control" name="paquete" id="summernote">'.$rows["descripcion_paquete"].'</textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                            <input type="hidden" id="imagenactual" name="imagenactual" value="'.$rows["img_paquete"].'">
                                        <p class="help-block">Para ver la imagen actual haga clic
                                            <a href="#0" title="Ver imagen" data-toggle="modal" data-target="#myModal">
                                                aquí
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Actualizar Paquete
                                        </button>
                                        <input type="hidden" value="'.$id.'" name="id" id="id">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger" title="Cerrar">×</span>
                </button>
                <h4 class="modal-title">Imagen actual</h4>
            </div>
            <div class="modal-body">';
    if(!empty($rows["img_paquete"])){
        echo'<img src="../img/paquetes/'.$rows["img_paquete"].'" class="img-responsive" style="margin:0 auto;">';
    }else{
        echo'
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay una imagen para mostrar
                </p>
            </div>';
    }
    echo'
            </div>
            <div class="modal-footer">
                <p class="help-block">
                    Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.
                </p>
            </div>
        </div>
    </div>
</div>';

    return;
}


function FORM_SELECION_HOTEL_PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar los Paquetes</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=VerPaquetes" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                            <select class="form-control" name="tipo_paquete" id="tipo_paquete">
                                <option value="0">Elija tipo de paquete</option>
                                <option value="1">Full day</option>
                                <option value="2">Dominical</option>
                                <option value="3">Paquete c / alojamiento</option>
                                <option value="4">Paquete c / alojamiento y vuelo</option>
                                <option value="5">Upselling</option>
                            </select>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Lista de Experiencias
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function FORM_SELECION_HOTEL_PAQUETE_CREAR($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar los Paquetes</h3>
                </div>
                <div class="box-body">
                    <form action="home.php?go=NuevoPaquete" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
    }
    echo'</select>
                            </div>
                            <div class="form-group">
                                <label for="tipo_paquete"></label>
                                <select class="form-control" name="tipo_paquete" id="tipo_paquete">
                                    <option value="0">Elija tipo de paquete</option>
                                    <option value="1">Full day</option>
                                    <option value="2">Dominical</option>
                                    <option value="3">Paquete c / alojamiento</option>
                                    <option value="4">Paquete c / alojamiento y vuelo</option>
                                    <option value="5">Upselling</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Lista de Experiencias
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}


function LISTAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de Paquetes</h3>
                        </div>
                        <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        $id_hotel = $_REQUEST["id_hotel"];
    }else{
        $id_hotel = $_SESSION["id_hotel"];
    }
    if (!empty($_GET["ok"])){
        unset($_GET["ok"]);
    }
    $cant = 0;

    $id_tipo_paquete = $_POST["tipo_paquete"];

        /*
    <option value="1">Full day</option>
                                    <option value="2">Dominical</option>
                                    <option value="3">Paquete c / alojamiento</option>
                                    <option value="4">Paquete c / alojamiento y vuelo</option>
                                    <option value="5">Upselling</option>';          
        */
        if($id_tipo_paquete == 1){
            $tipo_paquete = "Full day";
             $sql = sprintf("SELECT *
                    FROM hesperia_paquetes
                    WHERE id_hotel = '%s'  and alojamiento = 0 and full_day = 1 and dominical = 0",
                    mysqli_real_escape_string($mysqli,$id_hotel));
        }
        if($id_tipo_paquete == 2){
            $tipo_paquete = "Dominical";
             $sql = sprintf("SELECT *
                    FROM hesperia_paquetes
                    WHERE id_hotel = '%s'  and alojamiento = 0 and full_day = 1 and dominical = 1",
                    mysqli_real_escape_string($mysqli,$id_hotel));
        }
        if($id_tipo_paquete == 3){
            $tipo_paquete = "Paquete c / alojamiento";
             $sql = sprintf("SELECT *
                    FROM hesperia_paquetes
                    WHERE id_hotel = '%s'  and alojamiento = 0 and full_day = 0 and dominical = 0 and tiempo > 0 and vuelo = 0",
                    mysqli_real_escape_string($mysqli,$id_hotel));
        }
        if($id_tipo_paquete == 4){
            $tipo_paquete = "Paquete c / alojamiento y vuelo";
             $sql = sprintf("SELECT *
                    FROM hesperia_paquetes
                    WHERE id_hotel = '%s' and vuelo > 0",
                    mysqli_real_escape_string($mysqli,$id_hotel));
        }
        if($id_tipo_paquete == 5){
            $tipo_paquete = "Upselling";
             $sql = sprintf("SELECT *
                    FROM hesperia_paquetes
                    WHERE id_hotel = '%s' and alojamiento = 1 and full_day = 0 and dominical = 0",
                    mysqli_real_escape_string($mysqli,$id_hotel));
        }

   
    $resultPaquetes = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultPaquetes,MYSQLI_ASSOC)){
        $cant = 1;
        echo'
<div class="list-group">
    <span class="list-group-item clearfix">
        '.$rows["nombre_paquete"].'
        <span class="pull-right">
            <a href="home.php?go=ActualizarPaquetes&id='.$rows["id"].'&id_hotel='.$rows["id_hotel"].'&tipo_paquete='.$id_tipo_paquete.'" class="btn btn-xs btn-info" title="Editar">
                <i class="fa fa-pencil"></i>
                Editar
            </a>
            <a href="home.php?go=VerPaquetes&id='.$rows["id"].'&id_hotel='.$rows["id_hotel"].'&tipo_paquete='.$id_tipo_paquete.'" class="btn btn-xs btn-danger" title="Eliminar">
                <i class="fa fa-trash-o"></i>
                Eliminar
            </a>
        </span>
    </span>
</div>';
    }
    if ($cant == 0){
        echo '
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>Actualmente no hay Paquetes.
    <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">
                    <strong>Regresar al menu anterior</strong>
                    </a>
                </p>
            </div>';
    }else{
        if (isset($_REQUEST["id"])) {
        $id = $_REQUEST["id"];
        $sql = sprintf("SELECT id_hotel,img_paquete FROM hesperia_paquetes WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $imagen = "../img/paquetes/".$rows["img_paquete"];
        @unlink($imagen);
        $sql = sprintf("DELETE FROM hesperia_paquetes WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        echo'<div class="alert alert-success" role="alert"><p>Procesado acción de eliminación</p>
                    <meta http-equiv="refresh" content="5; url=home.php?go=VerPaquetes&id_hotel='.$rows["id_hotel"].'&tok='.time().'&ok=1"/>
            </div>';
        }
    }
    echo'<p class="help-block">Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    $_SESSION["mensaje"] = '';
    unset($_SESSION["mensaje"]);
    return;
}
?>
