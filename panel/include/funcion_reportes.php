<?php
function REPORTE_VENTAS_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
	echo '
	<section class="content">
		<div class="row">
	        <div class="col-lg-12">
	            <div class="box box-info">
	                <div class="row">
	                	<form role="form" method="post" action="https://hoteleshesperia.com.ve/panel/include/reporteexcel.php" enctype="application/x-www-form-urlencoded" onsumbit="return generarReporte(); return document.MM_returnValue" name="FormGeneraReporte" id="FormGeneraReporte">
	                    <div class="col-lg-12">
	                        <div class="box-header with-border">
	                            <h3 class="box-title">Reporte de Reservas</h3>
	                        </div>
	                        <div class="box-body">	   
								<div class="row">
								    <div class="col-md-4">
								    	<div class="form-group">
								    		<label for="id_hotel">Hotel</label>
								            <select class="form-control" name="id_hotel" id="id_hotel">
								              <option selected="selected" value="0">Todos</option>';
										        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
										        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
										        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
										            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
										        }
								        echo'</select>
								        </div>
								    </div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha desde</label>
											<input class="form-control" name="fechaDesdeReport" id="fechaDesdeReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha Hasta</label>
											<input class="form-control" name="fechaHastaReport" id="fechaHastaReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
								</div>
	                        </div>
	                        <div class="box-footer">
 								<button type="submit" id="testBtn" class="btn btn-primary btn-block btn-flat">Generar Reporte</button>
	                        </div>
	                    </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>';
}

function REPORTE_VENTAS_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name){
	echo '
	<section class="content">
		<div class="row">
	        <div class="col-lg-12">
	            <div class="box box-info">
	                <div class="row">
	                	<form role="form" method="post" action="https://hoteleshesperia.com.ve/panel/include/reportepaqexcel.php" enctype="application/x-www-form-urlencoded" onsumbit="return generarReporte(); return document.MM_returnValue" name="FormGeneraReporte" id="FormGeneraReporte">
	                    <div class="col-lg-12">
	                        <div class="box-header with-border">
	                            <h3 class="box-title">Reporte de Reservas</h3>
	                        </div>
	                        <div class="box-body">	   
								<div class="row">
								    <div class="col-md-4">
								    	<div class="form-group">
								    		<label for="id_hotel">Hotel</label>
								            <select class="form-control" name="id_hotel" id="id_hotel">
								              <option selected="selected" value="0">Todos</option>';
										        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
										        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
										        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
										            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
										        }
								        echo'</select>
								        </div>
								    </div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha desde</label>
											<input class="form-control" name="fechaDesdeReport" id="fechaDesdeReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha Hasta</label>
											<input class="form-control" name="fechaHastaReport" id="fechaHastaReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
								</div>
	                        </div>
	                        <div class="box-footer">
 								<button type="submit" id="testBtn" class="btn btn-primary btn-block btn-flat">Generar Reporte</button>
	                        </div>
	                    </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>';
}

function REPORTE_POR_FECHA_COMPRA_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
	echo '
	<section class="content">
		<div class="row">
	        <div class="col-lg-12">
	            <div class="box box-info">
	                <div class="row">
	                	<form role="form" method="post" action="include/reporteporfechacompraexcel.php" enctype="application/x-www-form-urlencoded"  name="FormGeneraReporte" id="FormGeneraReporte">
	                    <div class="col-lg-12">
	                        <div class="box-header with-border">
	                            <h3 class="box-title">Reporte de Reservas</h3>
	                        </div>
	                        <div class="box-body">	   
								<div class="row">
								    <div class="col-md-4">
								    	<div class="form-group">
								    		<label for="id_hotel">Hotel</label>
								            <select class="form-control" name="id_hotel" id="id_hotel">
								              <option selected="selected" value="0">Todos</option>';
										        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
										        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
										        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
										            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
										        }
								        echo'</select>
								        </div>
								    </div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha desde</label>
											<input class="form-control" name="fechaDesdeReport" id="fechaDesdeReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha Hasta</label>
											<input class="form-control" name="fechaHastaReport" id="fechaHastaReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
								</div>
	                        </div>
	                        <div class="box-footer">
 								<button type="submit" id="testBtn" class="btn btn-primary btn-block btn-flat">Generar Reporte</button>
	                        </div>
	                    </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>';
}

function REPORTE_POR_FECHA_COMPRA_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name){
	echo '
	<section class="content">
		<div class="row">
	        <div class="col-lg-12">
	            <div class="box box-info">
	                <div class="row">
	                	<form role="form" method="post" action="https://hoteleshesperia.com.ve/panel/include/reporteporfechacomprapaqexcel.php" enctype="application/x-www-form-urlencoded" onsumbit="return generarReporte(); return document.MM_returnValue" name="FormGeneraReporte" id="FormGeneraReporte">
	                    <div class="col-lg-12">
	                        <div class="box-header with-border">
	                            <h3 class="box-title">Reporte de Reservas</h3>
	                        </div>
	                        <div class="box-body">	   
								<div class="row">
								    <div class="col-md-4">
								    	<div class="form-group">
								    		<label for="id_hotel">Hotel</label>
								            <select class="form-control" name="id_hotel" id="id_hotel">
								              <option selected="selected" value="0">Todos</option>';
										        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
										        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
										        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
										            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
										        }
								        echo'</select>
								        </div>
								    </div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha desde</label>
											<input class="form-control" name="fechaDesdeReport" id="fechaDesdeReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
								    		<label for="id_hotel">Fecha Hasta</label>
											<input class="form-control" name="fechaHastaReport" id="fechaHastaReport" value="" placeholder="aaaa-mm-dd"/>
										</div>
									</div>
								</div>
	                        </div>
	                        <div class="box-footer">
 								<button type="submit" id="testBtn" class="btn btn-primary btn-block btn-flat">Generar Reporte</button>
	                        </div>
	                    </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>';
}
?>