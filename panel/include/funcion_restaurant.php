<?php
function RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name)
{ ?>
<script type="text/javascript">
function enviar_valor(valor){
location = location.pathname + '?go=go=Restaurant&id_hotel=' + valor;
}
window.onload = function(){
document.getElementById('id_hotel').onchange = function(){
enviar_parametro(this.value);
}}
</script>
<?php echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Restaurant en Hoteles</h3>
                            <div class="pull-right">
                                <a href="home.php?go=AgregarRestaurant">Agregar nuevo restaurant</a>
                            </div>
                        </div>

                        <div class="box-body">';
                            $sql = sprintf("SELECT id_restaurant, id_hotel, nombre_restaurant
                                                    FROM hesperia_restaurant
                                                    ORDER BY nombre_restaurant ASC");
                            $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                            $cant = 0;
                            while ($rows=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $cant = 1;
                                $id_hotel = $rows["id_hotel"];
                                $sqlH = sprintf("SELECT razon_social FROM hesperia_settings WHERE id = '%s'",
                                        mysqli_real_escape_string($mysqli,$id_hotel));
                                $resultH = QUERYBD($sqlH,$hostname,$user,$password,$db_name);
                                $rowsH=mysqli_fetch_array($resultH,MYSQLI_ASSOC);
                                echo '
                                <div id="RespEliminaRestaurant">';
                                        if(isset($_SESSION["mensaje"])){
                                        echo $_SESSION["mensaje"];
                                        unset($_SESSION["mensaje"]);
                                    }
                                echo '</div>
<div class="list-group">
    <span class="list-group-item">
    '.$rows["nombre_restaurant"].' (<small>'.$rowsH["razon_social"].'</small>)
        <span class="pull-right">
            <a class="btn btn-success btn-xs" href="home.php?go=EditarRestaurant&id='.$rows["id_restaurant"].'" role="button"><i class="fa fa-pencil-square-o"></i> Editar </a>
            <a class="btn btn-danger btn-xs" href="javascript:void(0)" role="button" onclick="javascript:EliminarRestaurant('.$rows["id_restaurant"].');"><i class="fa fa-trash-o"></i> Eliminar </a>
        </span>
    </span>
</div>
                        ';
                                }
echo'</div>';
                        if ($cant == 0){
                    echo '<div class="box-body"><div class="callout callout-danger text-center">
                                <h4>Disculpe!	</h4>
                                <p>Actualmente no hay Restaurant registrados. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
                                </p>
                            </div></div>'; }
echo '
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
return;
}

function RESTAURANT_EDITAR($mysqli,$data,$hostname,$user,$password,$db_name)
{ echo '<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edición de Restaurant en Hoteles</h3>
                        </div>';
                        $id_restaurant = trim($_GET["id"]);
                        $sql = sprintf("SELECT * FROM hesperia_restaurant
                                                WHERE id_restaurant = '%s'",
                                    mysqli_real_escape_string($mysqli,$id_restaurant));
                        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                        $rows=mysqli_fetch_array($result,MYSQLI_ASSOC);
        echo '<form enctype="multipart/form-data" action="include/admin_editar_restaurant.php" role="form" method="post" onsubmit="return EditarRestaurant(); return document.MM_returnValue" name="FormEditarRestaurant" id="FormEditarRestaurant">
                      <div class="box-body">
                            <input type="hidden" name="id_restaurant" id="id_restaurant" value="'.$id_restaurant.'" />';
                            if(isset($_SESSION["mensaje"])){
                                echo'<div id="InformacionEditarRestaurant">'.$_SESSION["mensaje"].'</div>';
                                unset($_SESSION["mensaje"]);
                            }
        echo '
                       <div class="form-group">
                        <label for="nombre_evento">Nombre Restaurant:</label>
                        <input class="form-control" type="text" id="nombre_restaurant" name="nombre_restaurant" value="'.$rows["nombre_restaurant"].'">
                     </div>
                       <div class="form-group">
                        <label for="nombre_evento">Tipo Comida:</label>
                        <input class="form-control" type="text" id="tipo_comida" name="tipo_comida" value="'.$rows["tipo_comida"].'">
                     </div>
                      <div class="form-group">
                          <label for="texto_evento">Caracteristicas:</label>
                          <textarea class="form-control" name="caracteristicas" id="summernote">'.trim($rows["caracteristicas"]).'</textarea>
                      </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora apertura:</label>
                                <input type="text" name="hora_apertura" id="timepicker1" class="form-control" value="'.$rows["hora_apertura"].'">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora cierre:</label>
                                <input type="text" name="hora_cierre" id="timepicker2" class="form-control" value="'.$rows["hora_cierre"].'">
                            </div>
                          </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen:</label>
                                <input type="file" id="imagen"  name="imagen">
                                <input type="hidden" name="imagen_actual" id="imagen_actual" value="'.$rows["imagen"].'"/>
                            </div>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Editar Restaurant</button>
                            </div>
                        </div>
                    </form>';
echo '
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
return;
}
function AGREGAR_RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name)
{ echo '<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agregar Restaurant en Hoteles</h3>
                        </div>';
        echo '<form enctype="multipart/form-data" action="include/admin_agregar_restaurant.php" role="form" method="post" onsubmit="return AgregarRestaurant(); return document.MM_returnValue" name="FormAgregarRestaurant" id="FormAgregarRestaurant">
                      <div class="box-body">';
                            if(isset($_SESSION["mensaje"])){
                                echo'<div id="InformacionAgregarRestaurant">'.$_SESSION["mensaje"].'</div>';
                                unset($_SESSION["mensaje"]);
                            }
                    if ($_SESSION["id_hotel"] == 0){
                        echo '
                            <div class="form-group">
                                <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece este restaurant</label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0">Elija Hotel</option>';
                                $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
                                $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
                                while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
                                    if($rows["id"] == $id_hotel){
                                        echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
                                    }else{
                                        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
                                    }
                                }
                                echo'</select>
                            </div>';
                            }else{
                                $id_hotel = $_SESSION["id_hotel"];
                                echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
                            }
        echo '
                       <div class="form-group">
                        <label for="nombre_evento">Nombre Restaurant:</label>
                        <input class="form-control" type="text" id="nombre_restaurant" name="nombre_restaurant" value="'.$rows["nombre_restaurant"].'">
                     </div>
                       <div class="form-group">
                        <label for="nombre_evento">Tipo Comida:</label>
                        <input class="form-control" type="text" id="tipo_comida" name="tipo_comida" value="">
                     </div>
                      <div class="form-group">
                          <label for="texto_evento">Caracteristicas:</label>
                          <textarea class="form-control" name="caracteristicas" id="summernote"></textarea>
                      </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora apertura:</label>
                                <input type="text" name="hora_apertura" id="timepicker1" class="form-control" value="">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora cierre:</label>
                                <input type="text" name="hora_cierre" id="timepicker2" class="form-control" value="">
                            </div>
                          </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen:</label>
                                <input type="file" id="imagen"  name="imagen">
                            </div>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Agregar Restaurant</button>
                            </div>
                        </div>
                    </form>';
echo '
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
return;
}
?>
