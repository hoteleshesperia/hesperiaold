<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/
session_start();

$ahora = md5(time());
global $mysqli;

if(!isset($antesdecore)){
    include 'include/databases.php'; }
else{ include 'databases.php'; }
include "funciones.php";

# calendario

    $dbhost=$hostname;
    $dbname=$db_name;
    $dbuser=$user;
    $dbpass=$password;
    $db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);

//ENCABEZADO($lenguaje,$timezone_set,$charset);
//LIMPIAR_VALORES();
#BUFFER_INICIO("COMPRESS_PAGE");

if (empty($_GET['go'])){
    $_GET['go']=$go='index'; }
else
{  $go = $_GET['go']; }

if (!isset($sucursal))
{ $sucursal = 1; }

$sql = "SELECT * FROM hesperia_settings WHERE id = '$sucursal' limit 1";
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
$rows   = mysqli_fetch_array($result,MYSQLI_ASSOC);
$data["id"] = $_SESSION["empresa"] = $rows["id"];
$data["nombre_sitio"] = $_SESSION["nombre_sitio"] = utf8_encode($rows["nombre_sitio"]);
$data["descripcion"] = utf8_encode($rows["descripcion"]);
$data["dominio"] = $_SESSION["dominio"] = $rows["dominio"];
$data["correo_contacto"] = $_SESSION["correo_contacto"] = $rows["correo_contacto"];
$data["correo_envio"] = $rows["correo_envio"];
$data["razon_social"] = $rows["razon_social"];

# Eliminacion de Sessiones previas
$ahora = time();
$tok = md5($ahora);
$hora = $ahora - 28800;
$sql = sprintf("DELETE FROM hesperia_sesion WHERE fecha <='$hora'");
$result= QUERYBD($sql,$hostname,$user,$password,$db_name);

function BORRAR_VARIABLES()
{ $_GET=$_POST=array();
 unset($_POST,$_GET);
 return;
}

function BUFFER_INICIO($buffer)
{ if (!empty($buffer)) { $buffer=ob_start($buffer); }
 else { $buffer=ob_start(); }
 return $buffer;
}

function BUFFER_FIN($mysqli)
{ ob_flush();
 BORRAR_VARIABLES();
 mysqli_close($mysqli);
 return;
}

function COMPRESS_PAGE($buffer)
{ $search =$replace=array();
 $search =array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
 $replace=array(">","<",'\\1');
 return trim(preg_replace($search,$replace,$buffer));
}

function DECODE($origen) {
    $origen=html_entity_decode($origen,ENT_QUOTES,"UTF-8");
    $origen=htmlspecialchars($origen,ENT_QUOTES,"UTF-8");
    return $origen;
}

function ENCABEZADO($lenguaje,$timezone_set,$charset){
    global $ExpStr;
    setlocale(LC_TIME,$lenguaje);
    header('Accept-Ranges:bytes');
    $tiempo= $_SERVER['REQUEST_TIME'] + 28800;
    $ExpStr='Expires:'.gmdate("D,d M Y H:i:s",$tiempo)." GMT";
    session_cache_limiter('private_no_expire');
    session_cache_expire(28800);
    header($ExpStr);
    header("Cache-Control:maxage=$tiempo");
    header("Cache-Control:public,must-revalidate");
    header("Cache-Control:public");
    header("pragma:public");
    header("Content-Transfer-Encoding:gzip;q=1.0,identity;q=0.5,*;q=0");
    header("Cache-Control:cache");
    header("Pragma:cache");
    header("Content-Type:text/html; charset=$charset");
    $etag=md5($_SERVER['REQUEST_URI'].$ExpStr);
    header("Etag:$etag");
    return $ExpStr;
}

function ERROR($hostname,$user,$password,$db_name)
{
    echo '<div class="text-center err">
<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
<h2>Ha solicitado una acci&oacute;n no valida</h2> o su sesi&oacute;n expir&oacute;, por favor haga click, <a href="home.php?tok='.md5(time()).'" title="Regresar a la pagina principal">para regresar</a>.</div>';

    return;
}

function GENERAR_CODIGO()
{ $letra=array('A','B','C','D','E','F','G','H','I','J');
 $micro= microtime(true);
 $codigo='CC-'.rand (0,9).'-'.$letra[rand (0,9)].'-'.substr(str_replace('.','X',$micro),-4);
 return $codigo;
}

function LIMPIAR_VALORES()
{ $_SERVER['QUERY_STRING']=trim(strip_tags($_SERVER['QUERY_STRING']));
 URL();
 if (!empty($_GET)){
     foreach($_GET as $variable=>$valor){
         $_GET[$variable]=$_GET[$variable];
         $_GET[$variable]=str_replace("'","\'",$_GET[$variable]);
         $_GET[$variable]=DECODE($_GET[$variable]);
         $_GET[$variable]=filter_var(trim($_GET[$variable]),FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
     }}
 if (!empty($_POST) ){
     foreach($_POST as $variable=>$valor){
         $_POST[$variable]=$_POST[$variable];
         $_POST[$variable]=str_replace("'","\'",$_POST[$variable]);
         $_POST[$variable]=DECODE($_POST[$variable]);
     }}
 return;
}

function NADA($texto)
{ echo '<div class="text-center">
<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
<h2>Alerta</h2> No hay contenido para mostrar... Por favor haga click, <a href="index.php?tok='.md5(time()).'" title="Regresar a la pagina principal">para regresar</a>.</div>';
 return;
}

function SELECTOR($go,$hostname,$user,$password,$db_name,$data,$mysqli){
    switch($go){
        case 'index':
            INDEX($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ActualizarExperiencias':
            ACTUALIZAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ActualizarAperturas':
            ACTUALIZAR_APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregarAperturas':
            CREAR_APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AdministrarAperturas':
            LISTAR_APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ActualizarPaquetes':
            ACTUALIZAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ActualizarSalones':
            ACTUALIZAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregarHabitacion':
            FORM_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregarEventos':
            AGREGAR_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregarDistancias':
            if ($_SESSION["id_hotel"] == 0){
                FORM_SELCCIONA_DISTANCIAS_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                VER_DISTANCIA_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'AgregarDistanciaHotel':
            FORM_DISTANCIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregaHotelNuevo':
            AGREGAR_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregarRestaurant':
            AGREGAR_RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregaSalones':
            CREAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AgregaPorcentHab':
            FORM_AGREGA_PORCENTAJE_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'AllUsuario':
            TODOS_USUARIOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ContenidoEvento':
            CONTENIDO_PORTADA_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CrearContenidoEvento':
            CREAR_CONTENIDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'Calendario':
            CALENDARIO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CambioPrecio':
            CAMBIO_PRECIO_HAB($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CambioPrecioAdmin':
            ELECCION_CAMBIO_PRECIO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CambioPrecioFecha':
            CAMBIO_PRECIO_FECHA_HAB($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ContenidoPortada':
            //CONTENIDO_PORTADA($mysqli,$data,$hostname,$user,$password,$db_name);
            modales();
            testing($hostname,$user,$password,$db_name);
            cargar_titulos_index($mysqli, "panel");
            cargar_items_index($mysqli);
            break;
        case 'CrearExperiencias':
            CREAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CrearPorcHab':
            FORM_SELECCIONA_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ContenidoExtra':
            CONTENIDO_EXTRA($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CrearServicios':
            FORMULARIO_SERVICIOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'CrearPaquetes':
            FORM_SELECION_HOTEL_PAQUETE_CREAR($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'NuevoPaquete':
            CREAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'DescargaSuscripciones':
            DESCARGA_SUSCRIPCIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EditarContenidoExtra':
            EDITAR_CONTENIDO_EXTRA($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EditarContenidoEvento':
            EDITAR_CONTENIDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EditarPorcentHab':
            FORM_EDITAR_PORCENTAJE_HAB($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        
        case 'EditarEvento':
            FORM_ACTUALIZA_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EliminarImagen':
            ELIMINAR_IMAGEN($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EliminarPyE':
            ELIMINAR_PAQUETES_ESCAPADAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EditarHabitacion':
            FORM_EDITAR_HABITACION($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EditarRestaurant':
            RESTAURANT_EDITAR($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'EditarContenidoExtra':
            EDITAR_CONTENIDO_EXTRA($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'Instapago':
            FORM_ACTUALIZAR_INSTAPAGO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'InfoGeneral':
            if ($_SESSION["id_hotel"] == 0){
                FORM_SELECT_HOTEL_INFOGRAL($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                INFO_GENERAL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ListarContenidoExtra':
            CONTENIDO_EXTRA($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListarContenido':
            LISTAR_TODO_CONTENIDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListarHotelPorcent':
            FORM_SELECCIONA_HOTEL_EDITAR($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListarEventos':
            FORM_HOTEL_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListarDistancias':
            if ($_SESSION["id_hotel"] == 0){
                FORM_ACTUALIZAR_DISTANCIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                VER_DISTANCIA_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name); }
            break;
        case 'ListarExperiencias':
            if ($_SESSION["id_hotel"] == 0){
                FORM_SELECION_HOTEL_EXPE($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                LISTAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ListarPaquetes':
            if ($_SESSION["id_hotel"] == 0){
                FORM_SELECION_HOTEL_PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                LISTAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ListarSalones':
            if ($_SESSION["id_hotel"] == 0){
                FORM_SELECION_HOTEL_SALONES($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                LISTAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ListarHabitacion':
            if ($_SESSION["id_hotel"] == 0){
                FORM_ACTUALIZAR_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                LISTAR_PRECIO_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ListarPreciosHabitaciones':
            if ($_SESSION["id_hotel"] == 0){
                LISTAR_PRECIOS($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                LISTAR_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ListaReservas':
            LISTA_RESERVAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListaPrereservas':
            LISTA_PRERESERVAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListaPrereservasPaq':
            LISTA_PRERESERVAS_PAQ($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ListaReservasPaq':
            LISTA_RESERVAS_PAQ($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'PrecioHabitaciones':
            LISTAR_PRECIO_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ActualizarPreciosHabitacion':
            ACTUALIZA_PRECIOS_FECHAS_FUTURAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'Publicaciones':
            PUBLICACIONES_NOTICIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'PublicacionesD':
            ELIMINAR_NOTICIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'PublicacionesE':
            EDITAR_NOTICIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ProcesoEliminaPyE':
            PROCESADO_ELIMINAR_PAQUETE_ESCAPADAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'Restaurant':
            RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'SobreElHotel':
            SOBRE_EL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'SubirImagen':
            SUBIR_IMAGEN($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerInstapago':
            LISTAR_INSTAPAGO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerDistanciaHotel':
            VER_DISTANCIA_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerExperiencias':
            LISTAR_EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerPaquetes':
            LISTAR_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerDetalleReservacion':
            VER_DETALLE_RESERVACION($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerDetalleReservacionPaq':
            VER_DETALLE_RESERVACION_PAQ($mysqli,$data,$hostname,$user,$password,$db_name);
            break;            
        case 'VerGaleriaImagenes':
            if ($_SESSION["id_hotel"] == 0){
                SELECT_HOTEL_GALERIA($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                GALERIA_IMG_HOTELES($mysqli,$data,$hostname,$user,$password,$db_name); }
            break;
        case 'VerGaleriaHotel':
            GALERIA_IMG_HOTELES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerInfoGeneral':
            INFO_GENERAL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerSalones':
            LISTAR_SALONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerTodasHabitaciones':
            LISTAR_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerTodosEventos':
            LISTAR_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerUsuario':
            VER_USUARIO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerContacto':
            FORMULARIO_CONTACTO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VerContactoTrabajo':
            FORMULARIO_TRABAJO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'salir':
            if (!empty($_SESSION["admin"]))
            { SALIDA($hostname,$user,$password,$db_name); }
            break;
        case 'SliderPrincipal':
            SLIDER_HEADER_PRINCIPAL($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'todosArchivos':
            //SUBIR_ARCHIVOS_MEDIA($mysqli,$data,$hostname,$user,$password,$db_name);
            //echo "archivos";
            include_once("vistas/vista_panel_mediacenter.php");
            break;
        case 'verNoticias':
            //ELIMINAR_ARCHIVOS_MEDIA($mysqli,$data,$hostname,$user,$password,$db_name);
            //echo "noticias";
            include_once("vistas/vista_panel_noticias.php");
            break;

        case 'VerLogs':
            VER_LOGS($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'VideoPromocion':
            LISTA_VIDEO($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'GeneraId':
            for($i=0; $i < 50; $i++){
                $uniqid = uniqid();
                $sql = sprintf("INSERT INTO hesperia_v2_acceso_vip (id, codigo)
                                VALUES ( NULL, '%s')",
                                mysqli_real_escape_string($mysqli,$uniqid));
                $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
            }
            break;
        case 'GeneraCodigosPromo':
            for($i=0; $i < 50; $i++){
                $uniqid = uniqid();
                $sql = sprintf("INSERT INTO hesperia_v2_codigos_promocionales (id, codigo, descuento)
                                VALUES ( NULL, '%s', 0)",
                                mysqli_real_escape_string($mysqli,$uniqid));
                $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
            }
            break;
        case 'ReportesHabitaciones':
            // Generar reporte de venta
                REPORTE_VENTAS_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ReportesPaquetes':
            // Generar reporte de venta
                REPORTE_VENTAS_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ReportesFechaReservaHabitaciones':
            // Generar reporte de venta
                REPORTE_POR_FECHA_COMPRA_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        case 'ReportesFechaReservaPaquetes':
            // Generar reporte de venta
                REPORTE_POR_FECHA_COMPRA_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);
            break;
        default:
            ERROR($hostname,$user,$password,$db_name);
            break;
    }
    return;
}

function TITULOS($data,$title){
    if  ($title == 'INDEX') {$row = $data;}
    else { $row = $data.' - '.$title; }
    echo $row;
    return $row;
}

function unhtmlentities($cadena)
{   $search = array("&lt;","&gt;");
 $cambio= array('<','>');
 $trans_tbl = get_html_translation_table(HTML_ENTITIES);
 $trans_tbl = array_flip($trans_tbl);
 $cadena = strtr($cadena, $trans_tbl);
 $cadena = str_replace($search,$cambio,$cadena);
 return $cadena;
}

function URL() {
    if (empty($_SERVER["HTTP_REFERER"])) { $_SERVER["HTTP_REFERER"]='';}
    $valor=strip_tags($_SERVER["HTTP_REFERER"]);
    $replace="%20";
    $search=array(">","<","|",";","-","'",'"');
    $_SERVER["HTTP_REFERER"]=str_replace($search,$replace,$valor);
    return;
}

function VERIFICA_EMAIL($email){
    $email= strtolower(trim($email));
    $mail_correcto=0;
    if ((strlen($email)>=6) && (substr_count($email,"@")==1) &&
        (substr($email,0,1) !="@") && (substr($email,strlen($email)-1,1) !="@")){
        if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) &&
            (!strstr($email,"\$")) && (!strstr($email," "))) {
            if (substr_count($email,".")>=1){
                $term_dom=substr(strrchr ($email,'.'),1);
                if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
                    $antes_dom=substr($email,0,strlen($email) - strlen($term_dom) - 1);
                    $caracter_ult=substr($antes_dom,strlen($antes_dom)-1,1);
                    if ($caracter_ult !="@" && $caracter_ult !="."){
                        return 1;
                    }
                }
            }
        }
    }
    return 0;
}
?>
