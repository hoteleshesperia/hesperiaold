<?php
function FORM_DISTANCIAS($mysqli,$data,$hostname,$user,$password,$db_name){
    $id_hotel  = $_REQUEST["id_hotel"];
    $sql = sprintf("SELECT id FROM hesperia_distancia WHERE id_hotel = '%s'",
                       mysqli_real_escape_string($mysqli,$id_hotel));
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
if(!empty($rows["id"])){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="callout callout-danger text-center">
                        <h4>Disculpe! </h4>
                        <p>
                            El hotel seleccionado ya posee información relacionada a las distancias, si desea actualizar la información haga clic <a href="home.php?go=VerDistanciaHotel&id_hotel='.$id_hotel.'" data-toggle="tooltip" data-placement="bottom" title="Actualizar información del hotel">aquí</a>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
}else{
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Distancias relativas al hotel</h3>
                </div>
                <div class="box-body">
                    <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return AgregarDistancias(); return document.MM_returnValue" name="FormAgregarDistancia" id="FormAgregarDistancia">
                <div id="InformacionAgregarDistancia"></div>';
    if ($_SESSION["id_hotel"] == 0){
        $id_hotel  = $_REQUEST["id_hotel"];
        echo '
    <div class="col-md-12">
        <div class="form-group">
            <label for="nombre_hotel">Agregando distancias para el Hotel</label>';
        $sql = sprintf("SELECT id,nombre_sitio,latitud,longitud FROM hesperia_settings WHERE id = '%s'",
                       mysqli_real_escape_string($mysqli,$id_hotel));
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
        echo '<input class="form-control" type="text" value="'.$rows["nombre_sitio"].'" name="id_hotel" id="id_hotel" placeholder="'.$rows["nombre_sitio"].'" readonly>';
        echo'
        </div>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '
    <div class="col-md-12">
        <div class="form-group">
            <label for="nombre_hotel">Agregando distancias para el Hotel</label>';
        $sql = sprintf("SELECT id,nombre_sitio,latitud,longitud FROM hesperia_settings WHERE id = '%s'",
                       mysqli_real_escape_string($mysqli,$id_hotel));
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
        echo '<input class="form-control" type="text" value="'.$rows["nombre_sitio"].'" name="id_hotel" id="id_hotel" placeholder="'.$rows["nombre_sitio"].'" readonly>';
        echo'
        </div>
    </div>';
    }
    echo'
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nombre_aeropuerto">Nombre del Aeropuerto</label>
                    <input type="text" class="form-control" name="nombre_aeropuerto" id="nombre_aeropuerto" placeholder="Nombre del Aeropuerto">
                </div>
                <div class="form-group">
                    <label for="aeropuerto">Distancia desde el Hotel hasta el Aeropuerto</label>
                    <input type="text" class="form-control" name="aeropuerto" id="aeropuerto" placeholder="Distancia desde el Hotel hasta el Aeropuerto">
                </div>
                <div class="form-group">
                    <label for="nombra_parque">Nombra del Parque</label>
                    <input type="text" class="form-control" name="nombra_parque" id="nombra_parque" placeholder="Nombre del Parque">
                </div>
                <div class="form-group">
                    <label for="parques_nacionales">Distancia desde el Hotel hasta el Parque nacional</label>
                    <input type="text" class="form-control" name="parques_nacionales" id="parques_nacionales" placeholder="Distancia desde el Hotel hasta el Parque nacional">
                </div>
                <div class="form-group">
                    <label for="centro_ciudad">Distancia desde el Hotel hasta el Centro de la Ciudad</label>
                    <input type="text" class="form-control" name="centro_ciudad" id="centro_ciudad" placeholder="Distancia desde el Hotel hasta el Centro de la Ciudad">
                </div>
                <div class="form-group">
                    <label for="centro_ciudad">Distancia desde el Hotel hasta el Centro Comercial</label>
                    <input type="text" class="form-control" name="centro_comercial" id="centro_comercial" placeholder="Distancia desde el Hotel hasta el Centro Comercial">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="museo">Distancia desde el Hotel hasta el Museo</label>
                    <input type="text" class="form-control" name="museo" id="museo" placeholder="Nombre del Museo">
                </div>
                <div class="form-group">
                    <label for="nombre_museo">Nombre del Museo</label>
                    <input type="text" class="form-control" name="nombre_museo" id="nombre_museo" placeholder="Distancia desde el Hotel hasta el Museo">
                </div>
                <div class="form-group">
                    <label for="metro">Distancia desde el Hotel hasta el Metro</label>
                    <input type="text" class="form-control" name="metro" id="metro" placeholder="Distancia desde el Hotel hasta el Metro">
                </div>
                <div class="form-group">
                    <label for="nombre_estacion">Nombre de la estación del metro</label>
                    <input type="text" class="form-control" name="nombre_estacion" id="nombre_estacion" placeholder="Nombre de la estación del metro">
                </div>
                <div class="form-group">
                    <label for="playas">Distancia desde el Hotel hasta las Playas más cercanas</label>
                    <input type="text" class="form-control" name="playas" id="playas" placeholder="Distancia desde el Hotel hasta las Playas más cercanas">
                </div>
                <div class="form-group">
                    <label for="playas">Distancia desde el Hotel hasta el Banco más cercano</label>
                    <input type="text" class="form-control" name="banco" id="banco" placeholder="Distancia desde el Hotel hasta el Banco más cercano">
                </div>
            </div>
            <div class="col-md-12">
                <p class="help-block">Puede hacer clic
                <a href="https://www.google.co.ve/maps/@'.$rows["latitud"].','.$rows["longitud"].',19z" target="_blank">aquí</a> para ver las distancias. De no aplicar alguno de estos campos debe colocar cero. Las distancias son en kilometros y solo se aceptan valores <strong>numéricos</strong>.</p>
            </div>
            <div class="col-md-12">
                <div class="box-footer">
                    <div class="col-lg-7">
                        <p>
                            Si ya realizo todo los cambios que deseaba hacer por favor,
                            pulse el siguiente boton:
                        </p>
                    </div>
                    <div class="col-lg-5">
                        <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                            Crear Distancias
                        </button>
                        <input type="hidden" value="1" name="opcion" id="opcion">
                        <input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">
                    </div>
                </div>
            </div>

    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
}
    return;
}

function FORM_ACTUALIZAR_DISTANCIAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Distancias relativas al hotel</h3>
                </div><!-- /.box-header -->
                <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
                    <form action="home.php?go=VerDistanciaHotel" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel">Seleccione el Hotel para editar las distancias</label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                      <option value="0">Elija Hotel</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Distancias
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>';
    }echo'
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function FORM_SELCCIONA_DISTANCIAS_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
    // funciona solo para el admin
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Distancias relativas al hotel</h3>
                </div>
                <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
                    <form enctype="multipart/form-data" action="home.php?go=AgregarDistanciaHotel" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_hotel">Seleccione el Hotel para agregar las distancias</label>
                                    <select class="form-control" name="id_hotel" id="id_hotel">
                                      <option value="0">Elija Hotel</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-5 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Ver Distancias
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>';
    }echo'
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function VER_DISTANCIA_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
    if (isset($_REQUEST["id_hotel"])){
        $id = $_REQUEST["id_hotel"];
    }else{ $id = $_SESSION["id_hotel"]; }
    $sql = sprintf("SELECT id,nombre_sitio,latitud,longitud
                    FROM hesperia_settings
                    WHERE id = '%s'
                    ORDER BY razon_social ASC",
                   mysqli_real_escape_string($mysqli,$id));
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Distancias relativas al hotel <strong>'.$rows["nombre_sitio"].'</strong></h3>
                </div>
                <div class="box-body">';
    $sql = sprintf("SELECT * FROM hesperia_distancia  WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultD = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowsD = mysqli_fetch_array($resultD,MYSQLI_ASSOC);
    if(empty($rowsD["id"])){
        echo '
        <div class="callout callout-danger text-center">
            <h4>
                Disculpe
            </h4>
            <p>
                El hotel <strong>'.$rows["nombre_sitio"].'</strong> no tiene registros para actualizar, debe hacer clic <a href="home.php?go=AgregarDistancias"  data-toggle="tooltip" data-placement="bottom" title="Agregar registros para el hotel '.$rows["nombre_sitio"].'">aquí</a> para agregar la información que corresponda.
            </p>
        </div>';
    }else{
        echo  '
<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return ActualizaDistancias(); return document.MM_returnValue" name="FormAgregarDistancia" id="FormAgregarDistancia">
            <div class="form-group">
                <div id="InformacionAgregarDistancia"></div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nombre_aeropuerto">Nombre del Aeropuerto</label>
                    <input type="text" class="form-control" name="nombre_aeropuerto" id="nombre_aeropuerto" placeholder="Nombre del Aeropuerto" value="'.utf8_encode($rowsD["nombre_aeropuerto"]).'">
                </div>
                <div class="form-group">
                    <label for="aeropuerto">Distancia desde el Hotel hasta el Aeropuerto</label>
                    <input type="text" class="form-control" name="aeropuerto" id="aeropuerto" placeholder="Distancia desde el Hotel hasta el Aeropuerto" value="'.$rowsD["aeropuerto"].'">
                </div>
                <div class="form-group">
                    <label for="nombra_parque">Nombre del Parque</label>
                    <input type="text" class="form-control" name="nombra_parque" id="nombra_parque" placeholder="Nombre del Parque" value="'.utf8_encode($rowsD["nombra_parque"]).'">
                </div>
                <div class="form-group">
                    <label for="parques_nacionales">Distancia desde el Hotel hasta el Parque nacional</label>
                    <input type="text" class="form-control" name="parques_nacionales" id="parques_nacionales" placeholder="Distancia desde el Hotel hasta el Parque nacional" value="'.$rowsD["parques_nacionales"].'">
                </div>
                <div class="form-group">
                    <label for="centro_ciudad">Distancia desde el Hotel hasta el Centro de la Ciudad</label>
                    <input type="text" class="form-control" name="centro_ciudad" id="centro_ciudad" placeholder="Distancia desde el Hotel hasta el Centro de la Ciudad" value="'.$rowsD["centro_ciudad"].'">
                </div>
                <div class="form-group">
                    <label for="centro_ciudad">Distancia desde el Hotel hasta el Centro Comercial</label>
                    <input type="text" class="form-control" name="centro_comercial" id="centro_comercial" placeholder="Distancia desde el Hotel hasta el Centro Comercial" value="'.$rowsD["centro_comercial"].'">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="museo">Distancia desde el Hotel hasta el Museo</label>
                    <input type="text" class="form-control" name="museo" id="museo" placeholder="Nombre del Museo" value="'.$rowsD["museo"].'">
                </div>
                <div class="form-group">
                    <label for="nombre_museo">Nombre del Museo</label>
                    <input type="text" class="form-control" name="nombre_museo" id="nombre_museo" placeholder="Distancia desde el Hotel hasta el Museo" value="'.utf8_encode($rowsD["nombre_museo"]).'">
                </div>
                <div class="form-group">
                    <label for="metro">Distancia desde el Hotel hasta el Metro</label>
                    <input type="text" class="form-control" name="metro" id="metro" placeholder="Distancia desde el Hotel hasta el Metro" value="'.$rowsD["metro"].'">
                </div>
                <div class="form-group">
                    <label for="nombre_estacion">Nombre de la estación del metro</label>
                    <input type="text" class="form-control" name="nombre_estacion" id="nombre_estacion" placeholder="Nombre de la estación del metro" value="'.utf8_encode($rowsD["nombre_estacion"]).'">
                </div>
                <div class="form-group">
                    <label for="playas">Distancia desde el Hotel hasta las Playas más cercanas</label>
                    <input type="text" class="form-control" name="playas" id="playas" placeholder="Distancia desde el Hotel hasta las Playas más cercanas" value="'.$rowsD["playas"].'">
                </div>
                <div class="form-group">
                    <label for="playas">Distancia desde el Hotel hasta el Banco más cercano</label>
                    <input type="text" class="form-control" name="banco" id="banco" placeholder="Distancia desde el Hotel hasta el Banco más cercano" value="'.$rowsD["banco"].'">
                </div>
            </div>
            <div class="col-md-12">
                <p class="help-block">Puede hacer clic <a href="https://www.google.co.ve/maps/@'.$rows["latitud"].','.$rows["longitud"].',19z" target="_blank">aquí</a> para ver las distancias. De no aplicar alguno de estos campos para el hotel <strong>'.$rows["nombre_sitio"].'</strong> debe colocar cero. Las distancias son en kilometros</p>
            </div>
            <div class="col-md-12">
                <div class="box-footer">
                    <div class="col-lg-7">
                        <p>
                            Si ya realizo todo los cambios que deseaba hacer por favor,
                            pulse el siguiente boton:
                        </p>
                    </div>
                    <div class="col-lg-5">
                        <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                            Actualizar Distancias
                        </button>
                        <input type="hidden" value="2" name="opcion" id="opcion">
                        <input type="hidden" value="'.$rowsD["id"].'" name="id" id="id">
                        <input type="hidden" value="'.$id.'" name="id_hotel" id="id_hotel">
                    </div>
                </div>
            </div>
    </form>';
    }
    echo'
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
