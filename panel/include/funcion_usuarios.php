<?php
function VER_USUARIO($mysqli,$data,$hostname,$user,$password,$db_name)
{  echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edite los campos correspondientes, una vez realizado los cambios haga clic en el botón <strong>Cambiar Datos</strong>.</h3>
                </div>
                <div class="box-body">
                <div id="CambiodatosUsuario"></div>';
$id = $_GET["id"];
$sql = sprintf("SELECT * FROM hesperia_usuario WHERE id = '%s'",
            mysqli_real_escape_string($mysqli,$id));
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
 echo ' <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return CambioUsuario();" name="FormUsuario" id="FormUsuario">
            <div class="form-group">
                <label for="inputemail">Email de registro:</label>
                <input type="hidden" name="id" id=id" value="'.$id.'"/>
                <input type="email" name="email" id="email" value="'.$rows["email"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputnombres"> Nombres: </label>
        <input type="text" name="nombres" id="nombres" value="'.$rows["nombres"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputapellidos"> Apellidos: </label>
        <input type="text" name="apellidos" id="apellidos" value="'.$rows["apellidos"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputtelefono">Telefono(s):</label>
        <input type="text" name="telefono" id="telefono" value="'.$rows["telefono"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputEmpresa"> Empresa: </label>
            <input type="text" name="empresa" id="empresa" value="'.$rows["empresa"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputEmpresaNum"> Número Telefónico Empresa: </label>
<input type="text" name="numero_empresa" id="numero_empresa" value="'.$rows["numero_empresa"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputEmpresaemail"> eMail de Empresa: </label>
<input type="email" name="email_empresa" id="email_empresa" value="'.$rows["email_empresa"].'" class="form-control" />
            </div>
            <div class="form-group">
                <label for="inputnivel">Nivel:</label>';
                switch($rows["nivel"]){
                    case 0:
                    $texto = 'Administrador general';
                    break;
                    case 1:
                    $texto = 'Admin Hotel';
                    break;
                    case 2:
                    $texto = 'Editor Hote';
                    break;
                    case 3:
                    $texto = 'Usuario';
                    break;
                    case 4:
                    $texto = 'RRHH';
                    break;
                    case 5:
                    $texto = 'Reservaciones';
                    break;
                }
             echo'
                <select name="nivel" id="nivel" class="form-control">
                            <option value="'.$rows["nivel"].'" selected>'.$texto.'</option>
                            <option value="0">Administrador general</option>
                            <option value="1">Admin Hotel</option>
                            <option value="2">Editor Hotel</option>
                            <option value="3">Usuario</option>
                            <option value="4">RRHH</option>
                            <option value="5">Reservaciones</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputEmpresaemail">Validación:</label>
                <select name="valido" id="valido" class="form-control">';
                switch($rows["valido"]){
                     case 0:
                     $texto = 'No validado';
                     break;
                     case 1:
                     $texto = 'Validado';
                     break;
                }
             echo'
                    <option value="'.$rows["valido"].'" selected>'.$texto.'</option>
                    <option value="0">No validado</option>
                    <option value="1">Validado</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputregistro">Fecha Registro:</label>
                <input type="text" name="fecha_registro" id="fecha_registro" value="'.date("d-m-Y",$rows["fecha_registro"]).'" class="form-control" readonly />
            </div>
            <div class="form-group">
                <label for="inputregistro">Ultimo login:</label>
                <input type="text" name="ultimo_login" id="ultimo_login" value="'.date("d-m-Y",$rows["ultimo_login"]).'" class="form-control" readonly />
            </div>';

if ($_SESSION["nivel"] <= $rows["nivel"]) {
         echo '
            <div class="box-footer">
                <div class="col-lg-7">
                    <p>
                        Si ya realizó los cambios al usuario <strong>'.$rows["nombres"].' '.$rows["apellidos"].'</strong> por favor, pulse el siguiente boton:
                    </p>
                </div>
                <div class="col-lg-5">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                            Cambiar datos de Usuario
                        </button>
                    </div>
                </div>
            </div>';
}
echo '
        </form>';
     }
if ($_SESSION["nivel"] <= $rows["nivel"]) {
         echo '
<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return EliminarUsuario();" name="FormEUsuario" id="FormEUsuario">
    <input type="hidden" name="id" id=id" value="'.$id.'"/>
    <div class="box-footer">
        <div class="col-lg-7">
            <p>
                Si desea eliminar al usuario <strong>'.$rows["nombres"].' '.$rows["apellidos"].'</strong> tenga en cuenta que al eliminarlo no hay vuelta atras. Si esta seguro por favor,
                pulse el siguiente boton:
            </p>
            <div id="CambiodatosUsuario"></div>
        </div>
        <div class="col-lg-5">
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-sm btn-lg btn-block">
                    Eliminar Usuario
                </button>
            </div>
        </div>
    </div>
</form>';
     }
echo '
                </div>
            </div>
        </div>
    </div>
</section>';
}

function TODOS_USUARIOS($mysqli,$data,$hostname,$user,$password,$db_name)
{   echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Usuarios en el sistema</h3>
                </div>
                <div class="box-body">';
 $cantidad=100;
 if (isset($_GET["primera"]))
 { $primera=htmlentities($_GET["primera"]);
  $pg=htmlentities($_GET["pg"]);
  $temp=$pg;
  settype($primera,integer); }
 if (!isset($primera))
 {	$primera=$pg=$temp =1;
  $inicial=$paginacion=0; }
 else { $pg=$_GET["pg"]; }
 if (!isset($_GET["pg"])){ $pg = 1;}
 else {$pg = $_GET["pg"]; }
 if ($temp>=$pg && $primera !=1)
 { $inicial=$pg * $cantidad + 1;  }
 else {  $inicial=$pg * $cantidad - $cantidad; }
 $sql = sprintf("SELECT id,nombres,apellidos FROM hesperia_usuario");
 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
 $total_usuarios=mysqli_num_rows($result);
 $pages=@intval($total_usuarios / $cantidad) + 1;
 $hay = mysqli_num_rows($result);
 if ($hay < 1)
 { echo '<br/><div class="text-center">
                            <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                            <h2>Disculpe</h2> <p>No hay usuarios registrados.</p></div>';}
 else {
     echo '<p>Leyenda:</p>
<ul class="list-inline">
  <li><i class="ion-person"></i> Usuario Admin/Editor.</li>
  <li><i class="ion-checkmark-circled"></i> Usuario Activo.</li>
  <li><i class="ion-close-circled"></i> Usuario No Activo.</li>
</ul>';
     $sql = sprintf("SELECT id,nombres,apellidos,nivel,valido FROM hesperia_usuario
                                  ORDER BY id DESC LIMIT %s,%s",
                    mysqli_real_escape_string($mysqli,$inicial),
                    mysqli_real_escape_string($mysqli,$cantidad));
     $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
     echo '<p>Total de usuarios registrados: <strong>'.$total_usuarios.'</strong> | P&aacute;gina(s): <strong>'.$pg.'/'.$pages.'</strong></p><div class="list-group">';
     while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
         $id = ($rows["id"]);
         $usuario = $rows["apellidos"].' '.$rows["nombres"];
         if ($rows["nivel"] <= 2) {
             echo '
                                <span class="list-group-item clearfix">
                                    <i class="ion-person"></i> '. utf8_decode($usuario).'
                                    <span class="pull-right">
<a class="btn btn-success btn-xs" href="home.php?go=VerUsuario&id='.$id.'" data-toggle="tooltip" data-placement="left" title="Editar '. utf8_decode('información').'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Ver usuario
                                        </a>
                                    </span>
                                </span>';
         }else{
             echo '
                                <span class="list-group-item clearfix">
                            <i class="ion-checkmark-circled"></i> '.utf8_decode($usuario).'
                                    <span class="pull-right">
<a class="btn btn-success btn-xs" href="home.php?go=VerUsuario&id='.$id.'" data-toggle="tooltip" data-placement="left" title="Editar '. utf8_decode('información').'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Ver usuario
                                        </a>
                                    </span>
                                </span>';
         }
     }echo'
                            </div>';
     PAGINACION($mysqli,$data,$hostname,$user,$password,$db_name,$total_usuarios,$pages,$cantidad,$pg);
 }
 echo '          </div>
            </div>
        </div>
    </div>
</section>';
}
?>
