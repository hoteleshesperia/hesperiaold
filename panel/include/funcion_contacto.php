<?php

function FORMULARIO_CONTACTO($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Buzón de Mensajes</h3>
                </div>
                <div class="box-body">';
    $cantidad=100;
    if (isset($_GET["primera"]))
    { $primera=htmlentities($_GET["primera"]);
     $pg=htmlentities($_GET["pg"]);
     $temp=$pg;
     settype($primera,integer); }
    if (!isset($primera))
    {	$primera=$pg=$temp =1;
     $inicial=$paginacion=0; }
    else { $pg=$_GET["pg"]; }
    if (!isset($_GET["pg"])){ $pg = 1;}
    else {$pg = $_GET["pg"]; }
    if ($temp>=$pg && $primera !=1)
    { $inicial=$pg * $cantidad + 1;  }
    else {  $inicial=$pg * $cantidad - $cantidad; }
    $sql = sprintf("SELECT * FROM hesperia_contacto");
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $total_mensajes=mysqli_num_rows($result);
    $pages=@intval($total_mensajes / $cantidad) + 1;
    $hay = mysqli_num_rows($result);
    if ($hay < 1)
    { echo '<div class="text-center"><i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>No hay mensajes.</p></div>';}
    else {echo'
                    <div class="box-group panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
          $sql = sprintf("SELECT * FROM hesperia_contacto ORDER BY id_mensaje ASC");
          $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
          while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
              echo'
                        <div class="panel box box-primary">
                            <div class="panel-heading" role="tab" id="headingOne">
                              <h4 class="box-title">
                              <strong>'.$rows["nombre_apellido"].'</strong> ha usado el formulario de contacto. Para ver el mensaje haga clic
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#'.$rows["id_mensaje"].'" aria-expanded="false" aria-controls="collapseOne" data-hover="tooltip" data-placement="right" title="Haga clic para ver el mensaje">aquí</a>.
                              </h4>
                            </div>
                <div id="'.$rows["id_mensaje"].'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                <p>'.$rows["mensaje_contacto"].'<br/>
                                '.$rows["email_contacto"].'<br/>'.$rows["telefono_contacto"].'<p>
                              </div>
                            </div>
                        </div>';
          }
                echo'</div>
                    <div class="box-footer">
                        <p>Total de mensajes: <strong>'.$total_mensajes.'</strong> | P&aacute;gina(s): <strong>'.$pg.'/'.$pages.'</strong></p>';
    PAGINACION($mysqli,$data,$hostname,$user,$password,$db_name,$total_mensajes,$pages,$cantidad,$pg);
    echo'
                    </div>';
                         }
        echo'</div>
        </div>
    </div>
</section>
';
    return;
}
?>
