<?php
function CONTENIDO_PORTADA($mysqli,$data,$hostname,$user,$password,$db_name){
$sql="SELECT * FROM hesperia_portada";
$resultPortada=QUERYBD($sql,$hostname,$user,$password,$db_name);
$rows = mysqli_fetch_array($resultPortada,MYSQLI_ASSOC);
echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-body">
                    <form enctype="multipart/form-data" action="include/admin_portada.php" role="form" method="post" onsubmit="return AgregandoPortada(); return document.MM_returnValue" name="FormAgregarPortada" id="FormAgregarPortada">';
                        if(isset($_SESSION["mensaje"])){
                            echo'<div id="InformacionAgregarPortada">'.$_SESSION["mensaje"].'</div>';
                            unset($_SESSION["mensaje"]);
                        }
                        echo'<div class="row">
                            <div class="col-lg-12">
                                        <p class="help-block">Esta es la informacion que aparece en la pagina principal</p>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="titulo_principal">Encabezado Principal</label>
                                        <input type="text" class="form-control" name="titulo_principal" id="titulo_principal" placeholder="Titulo Principal" value="'.$rows["titulo_princial"].'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="box-header with-border">
                                <h3 class="box-title">Texto Destacado #1</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputtitulo_UNO">Titulo</label>
                                    <input type="text" class="form-control" name="titulo_UNO" id="titulo_UNO" placeholder="Titulo del Item destacado" value="'.$rows["titulo_UNO"].'">
                                </div>
                                <div class="form-group">
                                    <label for="inputtexto_UNO">Descripción</label>
                                    <textarea name="texto_UNO" id="summernote" class="form-control">'.utf8_decode($rows["texto_UNO"]).'</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFileUNO">Imagen </label>
                                    <input type="file" id="imagen1" name="imagen1" />
                                    <p class="help-block">La imagen debe ser de 320px de alto y 170px de ancho</p>
                                </div>
                                <div class="form-group center-block">
                                    <label for="imgactual">Imagen Actual</label>
                                    <input type ="hidden"  name="imagenactual1" id="imagenactual1" value="'.$rows["imagen_UNO"].'"/>
                                    <ul class="hide-bullets">
                                        <li>';
    $imagen = '../img/portada/'.$rows["imagen_UNO"];
 if (!file_exists($imagen)){
    echo'<img src="../img/portada/sin-imagen.png"  alt="'.$rows["titulo_UNO"].'" class="img-responsive img-rounded"/>';
                }else{

    echo'<img src="'.$imagen.'"  alt="Portada: '.$rows["titulo_UNO"].'" class="img-responsive img-rounded"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["titulo_UNO"].'"/>';
 }
echo'
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="box-header with-border">
                                <h3 class="box-title">Texto Destacado #2</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputtitulo_DOS">Titulo</label>
                                    <input type="text" class="form-control" name="titulo_DOS" id="titulo_DOS" placeholder="Titulo del Item destacado" value="'.$rows["titulo_DOS"].'">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputtexto_DOS">Descripcion</label>
                                    <textarea name="texto_DOS" id="summernote2" class="form-control">'.utf8_decode($rows["texto_DOS"]).'</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen </label>
                                    <input type="file" id="imagen2" name="imagen2"/>
                                    <p class="help-block">La imagen debe ser de 320px de alto y 170px de ancho</p>
                                </div>
                                <div class="form-group">
                                    <label for="imgactual">Imagen Actual</label>
                                    <input type ="hidden" name="imagenactual2" id="imagenactual1" value="'.$rows["imagen_DOS"].'"/>
                                    <ul class="hide-bullets">
                                        <li>';
    $imagen = '../img/portada/'.$rows["imagen_DOS"];
 if (!file_exists($imagen)){
    echo'<img src="../img/portada/sin-imagen.png"  alt="'.$rows["titulo_DOS"].'" class="img-responsive img-rounded"/>';
                }else{

    echo'<img src="'.$imagen.'"  alt="Portada: '.$rows["titulo_DOS"].'" class="img-responsive img-rounded"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["titulo_DOS"].'"/>';
 }
echo'                                   </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="box-header with-border">
                                <h3 class="box-title">Texto Destacado #3</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputtitulo_TRES">Titulo</label>
                                    <input type="text" class="form-control" name="titulo_TRES" id="titulo_TRES" placeholder="Titulo del Item destacado" value="'.$rows["titulo_TRES"].'">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputtexto_TRES">Descripcion</label>
                                    <textarea name="texto_TRES" id="summernote3" class="form-control">'.utf8_decode($rows["texto_TRES"]).'</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen </label>
                                    <input type="file" id="imagen3" name="imagen3"/>
                                    <p class="help-block">La imagen debe ser de 320px de alto y 170px de ancho</p>
                                </div>
                                <div class="form-group">
                                    <label for="imgactual">Imagen Actual</label>
                                    <input type ="hidden" name="imagenactual3" id="imagenactual1" value="'.$rows["imagen_TRES"].'"/>
                                    <ul class="hide-bullets">
                                        <li>';
    $imagen = '../img/portada/'.$rows["imagen_TRES"];
 if (!file_exists($imagen)){
    echo'<img src="../img/portada/sin-imagen.png"  alt="'.$rows["titulo_TRES"].'" class="img-responsive img-rounded"/>';
                }else{

    echo'<img src="'.$imagen.'"  alt="Portada: '.$rows["titulo_TRES"].'" class="img-responsive img-rounded"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["titulo_TRES"].'"/>';
 }
echo'
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor,
                                    pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                    Cambiar Datos
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
