<?php

function FORMULARIO_TRABAJO($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Buzón de Solicitudes de Trabajo</h3>
                </div>
                <div class="box-body">';
    $cantidad=100;
    if (isset($_GET["primera"]))
    { $primera=htmlentities($_GET["primera"]);
     $pg=htmlentities($_GET["pg"]);
     $temp=$pg;
     settype($primera,integer); }
    if (!isset($primera))
    {	$primera=$pg=$temp =1;
     $inicial=$paginacion=0; }
    else { $pg=$_GET["pg"]; }
    if (!isset($_GET["pg"])){ $pg = 1;}
    else {$pg = $_GET["pg"]; }
    if ($temp>=$pg && $primera !=1)
    { $inicial=$pg * $cantidad + 1;  }
    else {  $inicial=$pg * $cantidad - $cantidad; }
    $sql = sprintf("SELECT * FROM hesperia_trabajo");
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $total_mensajes=mysqli_num_rows($result);
    $pages=@intval($total_mensajes / $cantidad) + 1;
    $hay = mysqli_num_rows($result);
    if ($hay < 1)
    { echo '<div class="text-center"><i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>No hay mensajes.</p></div>';}
    else {echo'
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
<div class="table-responsive">    
<table id="table_id" class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Tel. Celular</th>
            <th>Tel. Habitacion</th>
            <th>Email</th>
            <th>Archivo</th>
            <th>Especialidad</th>
            <th>Cuidad</th>
            <th>Formacion</th>
            <th>Hotel</th>
        </tr>
    </thead>
    <tbody>';
          $sql = sprintf("SELECT *
          FROM hesperia_trabajo 
          ORDER BY id_curriculum");
          $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
          while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){

              echo'
        <tr>
            <td>'.$rows["id_curriculum"].'</td>
            <td>'.$rows["nombre"].'</td>
            <td>'.$rows["telefono_movil"].'</td>
            <td>'.$rows["telefono_habi"].'</td>
            <td>'.$rows["email"].'</td>
            <td><a href="../empleos/'.$rows["archivo"].'" target="framename">Clic</a></td>
            <td>'.$rows["especialidad"].'</td>
            <td>'.$rows["ciudad"].'</td>
            <td>'.$rows["formacion"].'</td>
            <td>'.$rows["hotel_preferido"].'</td>
        </tr>';
          }
          echo '
    </tbody>
</table>
</div>
                    <div class="box-footer">';
    echo '
                    </div>';
                         }
        echo'</div>
        </div>
    </div>
</section>
<script>

$(document).ready( function () {
    $(\'#table_id\').DataTable( {
     "order": [[ 0, "desc" ]],
     responsive: true
});
} );
</script>';
    return;
}
?>
