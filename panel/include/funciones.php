<?php
include "funcion_aperturas.php";
include "funcion_archivos_media.php";
include "funcion_contacto.php";
include "funcion_contenido_extra.php";
//include "funcion_contenido_portada.php";

include_once("funcion_contenido_portadaV2.php");

include "funcion_distancias.php";
include "funcion_eventos.php";
include "funcion_experiencias.php";
include "funcion_galeria.php";
include "funcion_habitaciones.php";
include "funcion_imagenes.php";
include "funcion_info_general.php";
include "funcion_instapago.php";
include "funcion_logs.php";
include "funcion_listar_precios.php";
include "funcion_nosotros.php";
include "funcion_noticias.php";
include "funcion_paquetes.php";
include "funcion_porcentajes_hab.php";
include "funcion_precios.php";
include "funcion_reservaciones.php";
include "funcion_restaurant.php";
include "funcion_salones.php";
include "funcion_slider_headers.php";
include "funcion_servicios.php";
include "funcion_trabajo.php";
include "funcion_usuarios.php";
include "funcion_videos.php";
include "funcion_reportes.php";


function INDEX($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
    <div class="row">';
    if ($_SESSION["nivel"] == 5) {
echo '
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservaciones where status <> 'N' and status_transac = 0");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaReservas" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservaciones where status <> 'N' and status_transac = 3");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones por confirmar</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaPrereservas" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservas_paq where status <> 'N' and status_transac = 0");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones de Paquetes y Eventos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaReservasPaq" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservas_paq where status <> 'N' and status_transac = 3");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones de Paquetes y Eventos por confirmar</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaPrereservasPaq" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Habitaciones</h3>
                    <p>Reporte por fecha de reservas</p>
                </div>
                <div class="icon">
                    <i class="ion-person-stalker"></i>
                </div>
                <a href="home.php?go=ReportesFechaReservaHabitaciones" class="small-box-footer">
                    Generar
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Paquetes</h3>
                    <p>Reporte por fecha de reservas</p>
                </div>
                <div class="icon">
                    <i class="ion-person-stalker"></i>
                </div>
                <a href="home.php?go=ReportesFechaReservaPaquetes" class="small-box-footer">
                    Generar
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>';        
    } else {
    
    if ($_SESSION["nivel"] != 4) {  
        echo '
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservaciones where status <> 'N' and status_transac = 0");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaReservas" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservaciones where status <> 'N' and status_transac = 3");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones por confirmar</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaPrereservas" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservas_paq where status <> 'N' and status_transac = 0");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones de Paquetes y Eventos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaReservasPaq" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_v2_reservas_paq where status <> 'N' and status_transac = 3");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Reservaciones de Paquetes y Eventos por confirmar</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="home.php?go=ListaPrereservasPaq" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>';
echo '
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_usuario
                        WHERE valido = '1' && nivel = '3'");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Usuarios Registrados</p>
                </div>
                <div class="icon">
                    <i class="ion-person-stalker"></i>
                </div>
                <a href="home.php?go=AllUsuario" class="small-box-footer">
                    Ver Todos
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">';
                    $sql = sprintf("SELECT id_mensaje FROM hesperia_contacto");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Mensajes</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-email-outline"></i>
                </div>
                <a href="home.php?go=VerContacto" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-teal">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_eventos");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Eventos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bowtie"></i>
                </div>
                <a href="home.php?go=ListarEventos" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-maroon">
                <div class="inner">';
                    $sql = sprintf("SELECT id FROM hesperia_suscripciones");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Suscripciones</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="descargasuscrip.php" target="_parent" class="small-box-footer">Descargar Suscripciones <i class="ion-ios-cloud-download"></i></a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Habitaciones</h3>
                    <p>Reporte por fecha de reservas</p>
                </div>
                <div class="icon">
                    <i class="ion-person-stalker"></i>
                </div>
                <a href="home.php?go=ReportesFechaReservaHabitaciones" class="small-box-footer">
                    Generar
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Paquetes</h3>
                    <p>Reporte por fecha de reservas</p>
                </div>
                <div class="icon">
                    <i class="ion-person-stalker"></i>
                </div>
                <a href="home.php?go=ReportesFechaReservaPaquetes" class="small-box-footer">
                    Generar
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-purple">
                <div class="inner">';
                    $sql = sprintf("SELECT id_curriculum FROM hesperia_trabajo");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Solicitudes de Trabajo</p>
                </div>
                <div class="icon">
                    <i class="ion ion-settings"></i>
                </div>
                <a href="home.php?go=VerContactoTrabajo" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>';
        
        } else {
            echo '
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-purple">
                <div class="inner">';
                    $sql = sprintf("SELECT id_curriculum FROM hesperia_trabajo");
                    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row_cnt = mysqli_num_rows($result);
echo '              <h3>'.$row_cnt.'</h3>
                    <p>Solicitudes de Trabajo</p>
                </div>
                <div class="icon">
                    <i class="ion ion-settings"></i>
                </div>
                <a href="home.php?go=VerContactoTrabajo" class="small-box-footer">Ver Más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>';
        }
    } // validacion para el personal de reservas 
echo '        
    </div>
</section>';
    return;
}

function MENU_LATERAL($mysqli,$data,$hostname,$user,$password,$db_name)
{ echo '
<section class="sidebar">
    <ul class="sidebar-menu">
        <li class="treeview">
            <a href="home.php">
                <i class="fa fa-link"></i>
                <span>Regresar al Inicio</span>
            </a>
        </li>
        <li class="treeview">
            <a href="home.php?go=ContenidoPortada">
                <i class="fa fa-link"></i>
                <span>Portada Principal</span>
            </a>
        </li>
        <li class="treeview">
            <a href="home.php?go=Restaurant">
                <i class="fa fa-link"></i>
                <span>Restaurant</span>
            </a>
        </li>
        <!--li class="treeview">
            <a href="home.php?go=CrearServicios">
                <i class="fa fa-link"></i>
                <span>Servicios del Hotel</span>
            </a>
        </li -->
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Experiencias</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=CrearExperiencias">Crear Experiencias</a></li>
                <li><a href="home.php?go=ListarExperiencias">Administrar Experiencias</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Paquetes</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=CrearPaquetes">Crear Paquetes</a></li>
                <li><a href="home.php?go=ListarPaquetes">Administrar Paquetes</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Aperturas</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=AgregarAperturas">Crear Aperturas</a></li>
                <li><a href="home.php?go=AdministrarAperturas">Administrar Aperturas</a></li>
            </ul>
        </li>'   ;
 if ($_SESSION["id_hotel"] == 0){
    echo'
        <li class="treeview">
            <a href="#">
                <i class="fa fa-link"></i>
                <span>Configuraciones</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu" style="display: none;">
                <li class="">
                    <a href="#">
                        <i class="fa fa-circle-o"></i>
                        Configuración General
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li>
                            <a href="home.php?go=InfoGeneral">
                            <i class="fa fa-circle-o"></i> Actualizar Información</a>
                        </li>
                        <li>
                            <a href="home.php?go=AgregaHotelNuevo">
                            <i class="fa fa-circle-o"></i> Agregar Hotel</a>
                        </li>
                        <li>
                            <a href="home.php?go=Instapago">
                            <i class="fa fa-circle-o"></i> Instapago</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#">
                        <i class="fa fa-circle-o"></i>
                        % de Habitaciones
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li>
                            <a href="home.php?go=CrearPorcHab">
                            <i class="fa fa-circle-o"></i> Agregar % por Hotel</a>
                        </li>
                        <li>
                            <a href="home.php?go=ListarHotelPorcent">
                            <i class="fa fa-circle-o"></i> Actualizar % por Hotel</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="home.php?go=SliderPrincipal">
                <i class="fa fa-link"></i>
                <span>Slider Principal</span>
            </a>
        </li>

        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Nosotros</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=SobreElHotel">Administrar Nosotros</a></li>
            </ul>
        </li>';
 }
 echo'
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Reservaciones</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=ListaReservas">Listado de Reservaciones</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Salones</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=AgregaSalones">Agregar Salones</a></li>
                <li><a href="home.php?go=ListarSalones">Listar Salones</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Distancias</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">';
 if ($_SESSION["id_hotel"] == 0){
    echo'
                <li><a href="home.php?go=AgregarDistancias">Crear Distancias</a></li>';
 } echo'
                <li><a href="home.php?go=ListarDistancias">Administrar Distancias</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="home.php?go=VideoPromocion">
                <i class="fa fa-link"></i>
                <span>Videos Promocionales</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Contenido Extra</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">';
 $sql = sprintf("SELECT * FROM hesperia_contenido ORDER BY id DESC");
 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
 while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
    $sql        =  sprintf("SELECT * FROM hesperia_categoria WHERE id = '%s' ORDER BY nombre_categoria DESC",
                    mysqli_real_escape_string($mysqli,$rows["id_categoria"]));
    $resultn     = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $resultCategoria = mysqli_fetch_array($resultn,MYSQLI_ASSOC);
echo'
<li>
    <a href="home.php?go=EditarContenidoExtra&categoria='.$rows["id_categoria"].'">
        '.utf8_encode($resultCategoria["nombre_categoria"]).'
    </a>
</li>';
 }
echo'
            </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-link"></i>
            <span>Media center</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li class="">
               <li>
                    <a href="home.php?go=verNoticias">
                    <i class="fa fa-circle-o"></i> Noticias</a></li>
                <li>

                <li>
                    <a href="home.php?go=todosArchivos">
                    <i class="fa fa-circle-o"></i> Todos los Archivos</a></li>
                <li>
            </li>
          </ul>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Eventos</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=AgregarEventos">Crear Eventos</a></li>
                <li><a href="home.php?go=ListarEventos">Administrar Eventos</a></li>
                <li><a href="home.php?go=ContenidoEvento">Página de Eventos</a></li>
                <li><a href="home.php?go=CrearContenidoEvento">Crear Contenido Página de Eventos</a></li>
                <li><a href="home.php?go=ListarContenido">Listar Contenido Eventos</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="home.php?go=VerGaleriaImagenes">
                <i class="fa fa-link"></i>
                <span>Galeria</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#0">
                <i class="fa fa-link"></i>
                <span>Habitaciones</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="home.php?go=AgregarHabitacion">Agregar Habitaciones</a></li>
                <li><a href="home.php?go=ListarHabitacion">Listar Habitaciones</a></li>';
 /*                if ($_SESSION["nivel"] == 0) {
                echo '
                <li><a href="home.php?go=CambioPrecioAdmin&op=2">Cambiar Precios Fecha</a></li>';
                }
                else { 	echo '
                <li><a href="home.php?go=CambioPrecioFecha">Cambiar Precios Fecha</a></li>';
} */

    echo '      <li><a href="home.php?go=Calendario">Cambiar Precios (Calendario)</a></li>
            </ul>
        </li>';
 if ($_SESSION["id_hotel"] == 0){
echo'
        <li class="treeview">
            <a href="home.php?go=VerLogs">
                <i class="fa fa-link"></i>
                <span>Logs</span>
            </a>
        </li>
';
 }
echo'
    </ul>
<div style=" height:70px"></div>
</section>';
return;
}

function PAGINACION($mysqli,$data,$hostname,$user,$password,$db_name,$total_noticias,$pages,$cantidad,$pg)
{	if (isset($_GET["primera"]))
        {	$primera=htmlentities($_GET["primera"]);
            $pg=htmlentities($_GET["pg"]);
            $temp=$pg;
            settype($primera,integer);
        }
    if (!isset($primera))
        {	$primera=$pg=$temp =1;
            $inicial=$paginacion=0; }
    else
        {	$pg=$_GET["pg"];
            $primera=htmlentities($_GET["primera"]); }
echo '
<nav>
  <ul class="pagination">';
         for ($i=1; $i<($pages + 1); $i++) {
            if ($i ==$pg) {
             echo '<li class="active"><a href="#0">'.$i.'</a></li>';
              }
              else {
            if ($paginacion ==40)
            { $paginacion=0; }
              $paginacion++;
               echo '<li><a href="home.php?go=AllUsuario&pg='.$i.'&primera=1&" title="Ir a p&aacute;gina:'.$i.'">'.$i.'</a></li>';
              }
          }
echo '
  </ul>
</nav>';
return;
}

function SALIDA($hostname,$user,$password,$db_name)
{ # Salida del sistem
    $sql=sprintf("DELETE FROM hesperia_sesion WHERE referencia='$_SESSION[referencia]'");
    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
    session_unset();
    session_destroy();
    unset($_SESSION["referencia"]);
    $_POST=$_GET=$_SESSION=array();
    $tok=md5(time());
    echo '<meta http-equiv="refresh" content="0; url=salir.php?q='.$tok.'"/>';
    die();
    return;
}
?>
