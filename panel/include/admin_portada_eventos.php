<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$_SESSION["mensaje"]  = '';

function revisaloImagen()
{ $allowed = array('png', 'jpg');
    if(isset($_FILES["upl"]) && $_FILES["upl"]["error"] == 0){
        $extension = pathinfo($_FILES["upl"]["name"], PATHINFO_EXTENSION);
               if(!in_array(strtolower($extension), $allowed)){
                return 0; }
    }
return 1;
}

function subeloimagen()
{ if(move_uploaded_file($_FILES["upl"]["tmp_name"], '../../img/eventos/'.$_FILES["upl"]["name"])){
        $ahora = time();
        $nombre = $_FILES["upl"]["name"];
        $nuevo_nombre = substr(time(),-4).'_'.$_FILES["upl"]["name"];
        $search = array('%27',' ','-');
        $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
        rename ("../../img/eventos/$nombre", "../../img/eventos/$nuevo_nombre");
        chmod("../../img/eventos/$nuevo_nombre", 0777);
        $_SESSION["newname"] = $nuevo_nombre;
        return 1;
    }
    else { return 0;}
return 1;
}

$titulo_principal =   trim(utf8_encode($_POST["titulo_principal"]));
$titulo_UNO =  trim(utf8_encode($_POST["titulo_UNO"]));
$texto_UNO =   nl2br(trim(utf8_encode($_POST["texto_UNO"])));
$titulo_DOS = trim(utf8_encode($_POST["titulo_DOS"]));
$texto_DOS = nl2br(trim(utf8_encode($_POST["texto_DOS"])));
$titulo_TRES = trim(utf8_encode($_POST["titulo_TRES"]));
$texto_TRES = nl2br(trim(utf8_encode($_POST["texto_TRES"])));
$imagen1 = $_POST["imagenactual1"];
$imagen2 = $_POST["imagenactual2"];
$imagen3 = $_POST["imagenactual3"];

if (!empty($_FILES["imagen1"]["name"]))
    { 	$_FILES["upl"] = $_FILES["imagen1"];
        if  (revisaloImagen())
        { 	if (subeloimagen())
                {	$imagen1 = $_SESSION["newname"];
            $_SESSION["mensaje"] .=  '<div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen1"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
            else
            { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen 1 indicada '.$_FILES["imagen1"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div>'; }
        }
        else
        { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen 1 indicada '.$_FILES["imagen1"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div>';
        }
}

if (!empty($_FILES["imagen2"]["name"]))
    { 	$_FILES["upl"] = $_FILES["imagen2"];
        if  (revisaloImagen())
        { 	if (subeloimagen())
                {	$imagen2 = $_SESSION["newname"];
            $_SESSION["mensaje"] .= '<div class="callout callout-success text-center">
            <p>Imagen 2 indicada '.$_FILES["imagen2"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
            else
            { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen 2 indicada '.$_FILES["imagen2"]["name"].' no pudo se subida al servidor. Intente nuevamente.</p>
            </div>'; }
        }
        else
        { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen 2  indicada '.$_FILES["imagen2"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div>';
        }
}

if (!empty($_FILES["imagen3"]["name"]))
    { 	$_FILES["upl"] = $_FILES["imagen3"];
        if  (revisaloImagen())
        { 	if (subeloimagen())
                {	$imagen3 = $_SESSION["newname"];
            $_SESSION["mensaje"] .=  '<div class="callout callout-success text-center">
            <p>Imagen 3  indicada '.$_FILES["imagen3"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
            else
            { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen 3 indicada '.$_FILES["imagen3"]["name"].' no pudo se subida al servidor. Intente nuevamente.</p>
            </div>'; }
        }
        else
        { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen3  indicada '.$_FILES["imagen3"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div>';
        }
}

$antesdecore2 = 1;
include 'databases.php';
$ahora = time();
$sql = sprintf("UPDATE hesperia_eventos_extra
                SET titulo_principal = '%s', titulo_uno = '%s', texto_uno = '%s',
                img_uno = '%s', titulo_dos = '%s', texto_dos = '%s', img_dos = '%s',
                titulo_tres = '%s', texto_tres = '%s', img_tres = '%s'
                WHERE id = '1'",
               mysqli_real_escape_string($mysqli,$titulo_principal),
               mysqli_real_escape_string($mysqli,$titulo_UNO),
               mysqli_real_escape_string($mysqli,$texto_UNO),
               mysqli_real_escape_string($mysqli,$imagen1),
               mysqli_real_escape_string($mysqli,$titulo_DOS),
               mysqli_real_escape_string($mysqli,$texto_DOS),
               mysqli_real_escape_string($mysqli,$imagen2),
               mysqli_real_escape_string($mysqli,$titulo_TRES),
               mysqli_real_escape_string($mysqli,$texto_TRES),
               mysqli_real_escape_string($mysqli,$imagen3));
$result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli)){
   $_SESSION["mensaje"] .= '
        <div class="callout callout-success text-center">
            <h4>Almacenada información en la base de datos!</h4>
        </div>';
graba_LOG("Nueva cambio en portada eventos",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
    }else{ $_SESSION["mensaje"] .= '
        <div class="callout callout-danger text-center">
            <h4>Disculpe! </h4>
            <p>Hay un problema por lo que no se realizó la actualización de la información. O solo se almacenó parte de ella. Intente nuevamente
            </p>
        </div>';
}
$ahora = time()*3;
echo '<meta http-equiv="refresh" content="5; url=../home.php?go=ContenidoEvento&tok='.md5($ahora).'"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link href="../css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
<meta charset="UTF-8">
<div class="row">
  <div class="col-md-6" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);">
    <div class="callout callout-success text-center">
        <h4 style="font-size: 12em;"><i class="fa fa-spinner fa-pulse"></i></h4>
        <p>Procesando, luego se redireccionará... aguarde...<br/>Redireccionando...</p>
    </div>
  </div>
</div>';
?>
