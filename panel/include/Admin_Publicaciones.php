<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$tiempo =   time()*6;
function verifica_Archivo() {
    $allowed = array('png', 'jpg');
    if(isset($_FILES['archivo']['name']) && $_FILES['archivo']['error'] == 0){
        $extension = pathinfo($_FILES['archivo']['name'], PATHINFO_EXTENSION);
        if(!in_array(strtolower($extension), $allowed)){
        $_SESSION["mensaje"] = '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado.<br/>Su sesion ha expirado, o la extension de las imagenes es invalido</div>';
            echo '<meta http-equiv="refresh" content="0; url=../home.php?go=Publicacioness&tok='.md5($tiempo).'&ok=0"/>';
    die();
    }
    }
return;
}

$antesdecore2 = 1;
include 'databases.php';
if (!isset($hostname) || !isset($mysqli))
{$_SESSION["mensaje"] = '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado. Revise los valores que esta enviando.<br/>Su sesion ha expirado</div>';
echo '<meta http-equiv="refresh" content="0; url=../home.php?go=Publicaciones&tok='.md5($tiempo).'&ok=0"/>';
die();
}

if ($_POST["op"] == '1') {
    $nuevo_nombre = 'sin-imagen.png';
    if (!empty($_POST["titulo"]) ||  !empty($_POST["intro"]) || !empty($_POST["contenido"])) {
        if(!empty($_FILES['archivo']['name'])){
            verifica_Archivo();
        if(move_uploaded_file($_FILES['archivo']['tmp_name'], '../../img/prensa/'.$_FILES['archivo']['name'])){
                $nombre = $_FILES['archivo']['name'];
                $nuevo_nombre = substr(time(),-4).'_'.$_FILES['archivo']['name'];
                $search = array('%27',' ','-');
                $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
                rename ("../../img/prensa/$nombre", "../../img/prensa/$nuevo_nombre");
                chmod("../../img/prensa/$nuevo_nombre", 0777);
            }
        }
        $ahora = time();
        $titulo = trim($_POST["titulo"]);
        $intro =nl2br(trim($_POST["intro"]));
        $contenido =nl2br(trim($_POST["contenido"]));
        $autor = $_SESSION["nombre"];
        $sql = sprintf("INSERT INTO hesperia_noticias (noticia_id, titulo, intro, contenido,
            autor, lecturas, imagen, fecha)
             VALUES ( NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
              mysqli_real_escape_string($mysqli,$titulo),
              mysqli_real_escape_string($mysqli,$intro),
              mysqli_real_escape_string($mysqli,$contenido),
              mysqli_real_escape_string($mysqli,$autor),
              mysqli_real_escape_string($mysqli,1),
              mysqli_real_escape_string($mysqli,$nuevo_nombre),
              mysqli_real_escape_string($mysqli,$ahora));
       $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
        $_SESSION["mensaje"] =  '<div class="callout callout-success text-center">
                    <p>Noticia | Publicación creada satisfactoriamente;.</p></div>';
            graba_LOG("Nueva publicacion $titulo",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
        echo '<meta http-equiv="refresh" content="0; url=../home.php?go=Publicaciones&tok='.md5($tiempo).'&ok=1"/>';
        die();
    }
}
if ($_POST["op"] == '2') {
    if (!empty($_POST["titulo"]) ||  !empty($_POST["intro"]) ||
         !empty($_POST["contenido"])) {
            $titulo = trim($_POST["titulo"]);
            $intro = nl2br(trim($_POST["intro"]));
            $contenido = nl2br(trim($_POST["contenido"]));
            if (!empty($_FILES['archivo']['name']))
                {	if ($_FILES['archivo']['name'] != $_POST["imagen_actual"])
                    { verifica_Archivo();
                    move_uploaded_file($_FILES['archivo']['tmp_name'], '../../img/prensa/'.$_FILES['archivo']['name']);
                        $nombre = $_FILES['archivo']['name'];
                        $nuevo_nombre = substr(time(),-4).'_'.$_FILES['archivo']['name'];
                        $search = array('%27',' ','-');
                        $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
                        rename ("../../img/prensa/$nombre", "../../img/prensa/$nuevo_nombre");
                        chmod("../../img/prensa/$nuevo_nombre", 0777);
                    }
                }
            else { $nuevo_nombre = $_POST["imagen_actual"]; }
            $id = $_POST["noticia_id"];
            $sql = sprintf("UPDATE hesperia_noticias SET titulo = '%s',
                                        intro = '%s',
                                        contenido = '%s',
                                        imagen = '%s'
                                        WHERE noticia_id = '%s'",
                  mysqli_real_escape_string($mysqli,$titulo),
                  mysqli_real_escape_string($mysqli,$intro),
                  mysqli_real_escape_string($mysqli,$contenido),
                  mysqli_real_escape_string($mysqli,$nuevo_nombre),
                  mysqli_real_escape_string($mysqli,$id));
           $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
            $_SESSION["mensaje"] =  '<div class="callout callout-success text-center">
                        <p>Noticia - Publicaciones modificada satisfactoriamente.</p></div>';
            graba_LOG("Modificada publicacion $titulo",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
            echo '<meta http-equiv="refresh" content="0; url=../home.php?go=PublicacionesE&tok='.md5($tiempo).'&ok=1&noticia_id='.$id.'"/>';
die();
    }
}
$_SESSION["mensaje"] = '<div class="callout callout-danger text-center">Ha ocurrido un error inesperado.</div>';
echo '<meta http-equiv="refresh" content="0; url=../home.php?go=Publicaciones&tok='.md5($tiempo).'&ok=0"/>';
?>

