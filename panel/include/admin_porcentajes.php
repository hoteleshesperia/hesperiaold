<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }

$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);

$id_hotel           =   $_POST["id_hotel"];
$sqlH               =   sprintf("SELECT nombre_sitio
                               FROM hesperia_settings
                               WHERE id = '%s'",
                                mysqli_real_escape_string($mysqli,$id_hotel));
$resultSet          =   QUERYBD($sqlH,$hostname,$user,$password,$db_name);
$rows               =   mysqli_fetch_array($resultSet,MYSQLI_ASSOC);
$razon_social       =   $rows["nombre_sitio"];
$adultos            =   trim($_POST["adultos"]);
$adultos_2          =   trim($_POST["adultos_2"]);
$adultuos_3         =   trim($_POST["adultuos_3"]);
$adultos_4          =   trim($_POST["adultos_4"]);
$nino1              =   trim($_POST["nino1"]);
$nino2              =   trim($_POST["nino2"]);
$nino3              =   trim($_POST["nino3"]);
$minima_edad_nino   =   trim($_POST["minima_edad_nino"]);
$fecha_modificacion =   time();
$op                 =   trim($_POST["op"]);
switch($op){
    case 1:
        $sql = sprintf("INSERT INTO hesperia_porcentage_hab
                        (id, id_hotel, adultos, adultos_2, adultuos_3,
                            adultos_4, nino1, nino2, nino3, minima_edad_nino, fecha_modificacion)
                           VALUES ( NULL, '%s', '%s', '%s', '%s', '%s', '%s',
                           '%s', '%s', '%s', '%s')",
                       mysqli_real_escape_string($mysqli,$id_hotel),
                       mysqli_real_escape_string($mysqli,$adultos),
                       mysqli_real_escape_string($mysqli,$adultos_2),
                       mysqli_real_escape_string($mysqli,$adultuos_3),
                       mysqli_real_escape_string($mysqli,$adultos_4),
                       mysqli_real_escape_string($mysqli,$nino1),
                       mysqli_real_escape_string($mysqli,$nino2),
                       mysqli_real_escape_string($mysqli,$nino3),
                       mysqli_real_escape_string($mysqli,$minima_edad_nino),
                       mysqli_real_escape_string($mysqli,$fecha_modificacion));
//        die($sql);
        $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli)){
            echo '<div class="callout callout-success text-center" role="alert">
                    Los datos han sido agregados satisfactoriamente!</div>';
            $ahora=time();
            graba_LOG("Fue Agregado los porcentajes de costos para el hotel: $razon_social",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
        }else{
            echo '<div class="callout callout-warning text-center" role="alert">
                Ha ocurrido un error inesperado.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio.</div>';
        }
        break;
    case 2:
        $sql = sprintf("UPDATE hesperia_porcentage_hab
                        SET adultos = '%s', adultos_2 = '%s', adultuos_3 = '%s',
                            adultos_4 = '%s', nino1 = '%s', nino2 = '%s', nino3 = '%s',
                            minima_edad_nino = '%s', fecha_modificacion = '%s'
                        WHERE id_hotel = '%s'",
                       mysqli_real_escape_string($mysqli,$adultos),
                       mysqli_real_escape_string($mysqli,$adultos_2),
                       mysqli_real_escape_string($mysqli,$adultuos_3),
                       mysqli_real_escape_string($mysqli,$adultos_4),
                       mysqli_real_escape_string($mysqli,$nino1),
                       mysqli_real_escape_string($mysqli,$nino2),
                       mysqli_real_escape_string($mysqli,$nino3),
                       mysqli_real_escape_string($mysqli,$minima_edad_nino),
                       mysqli_real_escape_string($mysqli,$fecha_modificacion),
                       mysqli_real_escape_string($mysqli,$id_hotel));
        //        die($sql);
        $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli)){
            echo '<div class="callout callout-success text-center" role="alert">
                    Los datos han sido cambiados satisfactoriamente!</div>';
            $ahora=time();
            graba_LOG("Fue actualizado los porcentajes de costos para el hotel: $razon_social",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
        }else{
            echo '<div class="callout callout-warning text-center" role="alert">
                Ha ocurrido un error inesperado.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio.</div>';
        }
        break;
}// switch
