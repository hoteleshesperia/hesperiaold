<?php

function SLIDER_HEADER_PRINCIPAL($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Subir imagenes a la Cabeceras Promocionales de Hotel(es)
                    </h3>
                </div>';
    echo '
                <form id="sube" name="sube" method="post" action="uploadHeader.php" enctype="multipart/form-data" onsubmit="return AgregarSlider(); return document.MM_returnValue">
                    <div class="box-body">';
    if ($_SESSION["nivel"]==0){
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings");
        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
        echo '<div class="form-group"><label for="texto">Hotel:</label>
                            <select class="form-control" name="id_hotel" id="id_hotel">
                            <option value="0" selected>Elija Hotel</option>';
        while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select></div>'; }
//    else {
//        echo '<input type="hidden" value="'.$_SESSION["id_hotel"].' name="id_hotel"  id=id_hotel"/>';
//    }
    echo '
                        <div class="form-group">
                            <label for="texto">Seleccione la ubicación de la Imagen</label>
                            <select class="form-control" name="ubicacion" id="ubicacion">
                                <option value="0" selected>Seleccione</option>
                                <option value="1">Portada</option>
                                <option value="2">Paquetes</option>
                                <option value="3">Experiencias</option>
                                <option value="4">Eventos</option>
                                <option value="5">Historia</option>
                                <option value="6">Gastronomía</option>
                                <option value="7">Responsabilidad Social</option>
                                <option value="8">Próximas aperturas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="texto">Texto de la Imagen</label>
                            <input type="text" id="texto" name="texto" value="" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Seleccione la imagen</label>
                            <input type="file" id="upl" name="upl" class="form-control"/><br/>
                        </div>
                        <div class="form-group">
                            <p class="help-block">Las imagenes deben ser igual o mayor 1040px x 392px resolución de 100dpi como mínimo, para un buen resultado</p>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Subir Imagen</button>
                        </div>
                    </div>
                </form>';
    if (!empty($_SESSION["mensaje"]))
    { echo '<div id="Respuestagaleria">'.$_SESSION["mensaje"].'</div>'; }
echo'
                <!-- galeria -->
                <div class="center-block box-body"><hr/>
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Estas son las imagenes que estan actualmente en la base de datos
                        </h3>
                        <p class="text-danger help-block"><span class="ion-android-warning"></span> <strong>ALERTA</strong>:Tenga en cuenta que al eliminar una imagen esta se elimina sin confirmación alguna de la acción, por tanto, debe estar seguro de lo que esta haciendo.</p>
                    </div>
                    <div id="RespuestaSlider"></div>
                    <ul class="hide-bullets">';
    $_SESSION["mensaje"] = array();
    $cant = 0;
    $sql = sprintf("SELECT * FROM hesperia_headers ORDER BY promo_id DESC");
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rowImg=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $cant = 1;
        $posicion = $rowImg["ubicacion"];
        switch ($posicion){
            case 4:
                $ubicacion = 'Eventos';
                break;
            case 3:
                $ubicacion = 'Experiencias';
                break;
            case 2:
                $ubicacion = 'Paquetes';
                break;
            case 1:
                $ubicacion = 'Portada';
                break;
            case 5:
                $ubicacion = 'Historia';
                break;
            case 6:
                $ubicacion = 'Gastronomía';
                break;
            case 7:
                $ubicacion = 'Responsabilidad Social';
                break;
            case 8:
                $ubicacion = 'Próximas aperturas';
                break;
        }
        echo '
<li class="col-sm-4 center-block">
    <div class="thumbnail">
    <img src="../img/slider/'.$rowImg["imagen"].'" title="'.$rowImg["texto"].' Se encuentra en: '.$ubicacion.'" data-toggle="tooltip" data-placement="bottom">
      <div class="caption">
        <p>
            <a id="galeria" class="btn btn-danger btn-lg btn-sm" role="button" href="javascript:void(0)" onclick="javascript:EliminarSlider('.$rowImg["promo_id"].');">
                Eliminar Imagen
            </a>
            <a id="galeria" class="btn btn-success btn-lg btn-sm pull-right" role="button" href="javascript:void(0)" onclick="javascript:CambiarUbicacionSlider('.$rowImg["promo_id"].');">
                Cambiar Ubicación
            </a>
        </p>
      </div>
    </div>
</li>';
    }if ($cant == 0){
        echo '
                            <div class="callout callout-danger text-center">
                                <h3>Disculpe</h3>
                                <p>Actualmente no hay imágenes.</p>
                                <p>Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                            </div>';
    }echo '
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>';?>
<script>
$('[data-tooltip="tooltip"]').tooltip();
</script>
<?php
    return;
}
?>
