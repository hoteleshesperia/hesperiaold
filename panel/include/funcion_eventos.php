<?php
function AGREGAR_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                <div class="box-header with-border">
                    <h3 class="box-title">Agrega un nuevo evento</h3>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="include/admin_agregar_evento.php" role="form" method="post" onsubmit="return AgregarEvento(); return document.MM_returnValue" name="FormAgregarEvento" id="FormAgregarEvento">
                      <div class="box-body">';
    if(isset($_SESSION["mensaje"])){
        echo'<div id="InformacionAgregarEvento">'.$_SESSION["mensaje"].'</div>';
        unset($_SESSION["mensaje"]);
    }
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="tipo_habitacion">Seleccione el Hotel al cual pertenece este Evento</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
              <option selected="selected" value="0">Seleccione</option>';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings  WHERE eventos ='1' ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    echo'
       <div class="form-group">
                          <label for="nombre_evento">Nombre del evento</label>
                          <input class="form-control" type="text" id="nombre_evento" name="nombre_evento">
                        </div>
                        <div class="form-group">
                          <label for="texto_evento">Descripcion del evento</label>
                          <textarea class="form-control" name="texto_evento" id="summernote"></textarea>
                          <p class="help-block">Este campo no debe estar vacio</p>
                        </div>
                          <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_evento">Fecha del evento</label>
                        <input type="text" name="fecha_evento" class="form-control" data-date-format="dd/mm/yyyy" id="dp1" placeholder="">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora del Evento</label>
                                <input type="text" name="hora_evento" id="timepicker1" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="precio_evento">Precio del evento</label>
                                <input type="number" name="precio_evento" id="precio_evento" class="form-control">
                            </div>
                          </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen del Evento</label>
                                <input type="file" id="imagen"  name="imagen">
                            </div>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Crear Evento</button>
                                <input type="hidden" value="1" name="opcion" id="opcion">
                            </div>
                        </div>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function FORM_HOTEL_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de Eventos</h3>
                        </div>
                        <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '   <form action="home.php?go=VerTodosEventos" role="form" method="post">
                            <div class="form-group">
                                <label for="nombre_hotel">Seleccione el hotel</label>
                                <select class="form-control" name="id_hotel" id="id_hotel">';
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings WHERE eventos ='1'
        ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'
                                </select>
                            </div>
                            <div class="box-footer">
                                <div class="col-lg-6 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                        Enviar
                                    </button>
                                    <input type="hidden" value="1" name="opcion" id="opcion">
                                </div>
                            </div>
                        </form>';
    }else{
        $cant = 0;
        $id_hotel = $_SESSION["id_hotel"];
        $sql = sprintf("SELECT id,nombre_evento,imagen_evento FROM hesperia_eventos WHERE id_hotel = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
        $resultEventos = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultEventos,MYSQLI_ASSOC)){
            $cant = 1;
            echo'
                            <div class="list-group">
                                <span class="list-group-item clearfix">
                                    '.$rows["nombre_evento"].'
                                    <span class="pull-right">
                                        <a class="btn btn-success btn-xs" href="home.php?go=EditarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Editar
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="home.php?go=EliminarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-trash-o"></i>
                                            Eliminar
                                        </a>
                                    </span>
                                </span>
                            </div>';
        }
        if ($cant == 0){
            echo '
                        <div class="callout callout-danger text-center">
                            <h4>
                                Disculpe
                            </h4>
                            <p>
                                Actualmente no hay eventos en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
                            </p>
                        </div>';
        }
    }
    echo'</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function LISTAR_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    if (isset($_REQUEST["id_hotel"])){
        $id_hotel = $_REQUEST["id_hotel"];
    }else{ $id_hotel = $_SESSION["id_hotel"]; }
    $sql = sprintf("SELECT * FROM hesperia_eventos WHERE id_hotel = '%s'",
                   mysqli_real_escape_string($mysqli,$id_hotel));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $cant = 0;
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eventos actuales en el hotel</h3>
                        </div>
                        <div class="box-body">';
    if (!empty($_GET["ok"])){
        unset($_GET["ok"]);
    }
    while ($rows=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $cant = 1;
        echo '
                            <div class="list-group">
                                <span class="list-group-item clearfix">
                                    '.$rows["nombre_evento"].'
                                    <span class="pull-right">
                                        <a class="btn btn-success btn-xs" href="home.php?go=EditarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Editar
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="home.php?go=VerTodosEventos&id='.$rows["id"].'&id_hotel='.$id_hotel.'" role="button">
                                            <i class="fa fa-trash-o"></i>
                                            Eliminar
                                        </a>
                                    </span>
                                </span>
                            </div>';
    }
    if ($cant == 0){
echo '<div class="callout callout-danger text-center">
        <h4>
            Disculpe
        </h4>
        <p>
            Actualmente no hay eventos en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
        </p>
    </div>';
    }else{
        if (isset($_REQUEST["id"])) {
        $id = $_REQUEST["id"];
        $sql = sprintf("SELECT imagen_evento FROM hesperia_eventos WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $imagen = "../img/eventos/".$rows["imagen_evento"];
        @unlink($imagen);
        $sql = sprintf("DELETE FROM hesperia_eventos WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        echo'<div class="alert alert-success" role="alert">
                    <p>Procesado acción de eliminación</p>
                    <meta http-equiv="refresh" content="3; url=home.php?go=VerTodosEventos&tok='.time().'&ok=1&id_hotel='.$id_hotel.'"/>
            </div>';
        }
    }
    echo '<p class="help-block">Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function FORM_ACTUALIZA_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
?>
<script type="text/javascript">
function enviar_parametro2(valor){
location = location.pathname + '?go=EditarEvento&id_hotel=' + valor;
}
window.onload = function(){
document.getElementById('id_hotel').onchange = function(){
enviar_parametro(this.value);
}
}
</script>
<?php
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Actualizar Evento</h3>
                </div>
                <div class="box-body">
<form enctype="multipart/form-data" action="include/admin_editar_evento.php" role="form" method="post" onsubmit="return EditarEvento(); return document.MM_returnValue" name="FormEditarEvento" id="FormEditarEvento">
                      <div class="box-body">';
    if(isset($_SESSION["mensaje"])){
        echo'<div id="InformacionAgregarEvento">'.$_SESSION["mensaje"].'</div>';
        unset($_SESSION["mensaje"]);
    }
    $id_evento = $_GET["id"];
    $sql = "SELECT * FROM hesperia_eventos WHERE id = '$id_evento'";
    $resultEventos = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowE = mysqli_fetch_array($resultEventos,MYSQLI_ASSOC);
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="nombre_hotel">Seleccione el Hotel al cual le pertenece este evento</label>
            <select class="form-control" name="id_hotel" id="id_hotel" onchange="enviar_parametro2(this.value);">
              <option>Seleccione</option>';
        $sql = "SELECT id,nombre_sitio FROM hesperia_settings  WHERE eventos = '1' ORDER BY razon_social ASC";
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            if($rows["id"] == $rowE["id_hotel"]){
                echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
            }else{
                echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
            }
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }

    echo '   <div class="form-group">
                          <label for="nombre_evento">Nombre del evento</label>
                          <input class="form-control" type="text" id="nombre_evento" name="nombre_evento" value="'.$rowE["nombre_evento"].'">
                        </div>
                        <div class="form-group">
                          <label for="texto_evento">Descripcion del evento</label>
                          <textarea class="form-control" name="texto_evento" id="summernote">'.$rowE["texto_evento"].'</textarea>
                          <p class="help-block">Este campo no debe estar vacio</p>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_evento">Fecha del evento</label>
<input type="text" name="fecha_evento" class="form-control" value="'.date("d-m-Y",$rowE["fecha_evento"]).'" data-date-format="dd-mm-yyyy" id="dp1" placeholder="'.date("d-m-Y",$rowE["fecha_evento"]).'">
                            </div>
                        </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora del Evento</label>
                                <input type="text" name="hora_evento" id="timepicker1" class="form-control" value="'.$rowE["hora_evento"].'">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="precio_evento">Precio del evento</label>
                                <input type="number" name="precio_evento" id="precio_evento" class="form-control" value="'.$rowE["precio_evento"].'">
                            </div>
                          </div>
                        <div class="form-group">
                          <label for="exampleInputFile">Imagen del Evento</label>
                           <input type="file" id="imagen"  name="imagen">
                           <input type="hidden" id="imagen"  name="imagenActual" value="'.$rowE["imagen_evento"].'">
                        </div>
                        <div class="form-group">
                          <p>
                            Haga clic
                            <a href="#0" data-toggle="modal" data-target="#myModal">aquí</a> para ver la imagen actual
                          </p>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Enviar</button>
                                <input type="hidden" value="'.$id_evento.'" name="id" id="id">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Imagen actual</h4>
      </div>
      <div class="modal-body text-center">
        <img src="../img/eventos/'.$rowE["imagen_evento"].'" data-toggle="tooltip" data-placement="bottom" title="'.$rows["nombre_evento"].'" class="img-responsive">
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>';
    return;
}




function CONTENIDO_PORTADA_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
$sql="SELECT * FROM hesperia_eventos_extra";
$resultPortada=QUERYBD($sql,$hostname,$user,$password,$db_name);
$rows = mysqli_fetch_array($resultPortada,MYSQLI_ASSOC);
echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-body">
                    <form enctype="multipart/form-data" action="include/admin_portada_eventos.php" role="form" method="post" onsubmit="return AgregandoPortadaEventos(); return document.MM_returnValue" name="FormAgregarPortadaEventos" id="FormAgregarPortadaEventos">';
                        if(isset($_SESSION["mensaje"])){
                            echo'<div id="InformacionAgregarPortada">'.$_SESSION["mensaje"].'</div>';
                            unset($_SESSION["mensaje"]);
                        }
                        echo'<div class="row">
                            <div class="col-lg-12">
                                        <p class="help-block">Esta es la informacion que aparece en la pagina principal de Eventos</p>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="titulo_principal">Encabezado Principal</label>
                                        <input type="text" class="form-control" name="titulo_principal" id="titulo_principal" placeholder="Titulo Principal" value="'.utf8_decode($rows["titulo_principal"]).'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="box-header with-border">
                                <h3 class="box-title">Texto Destacado #1</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputtitulo_UNO">Titulo</label>
                                    <input type="text" class="form-control" name="titulo_UNO" id="titulo_UNO" placeholder="Titulo del Item destacado" value="'.$rows["titulo_uno"].'">
                                </div>
                                <div class="form-group">
                                    <label for="inputtexto_UNO">Descripción</label>
                                    <textarea name="texto_UNO" id="summernote" class="form-control">'.utf8_decode($rows["texto_uno"]).'</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFileUNO">Imagen </label>
                                    <input type="file" id="imagen1" name="imagen1" />
                                    <p class="help-block">La imagen debe ser de 320px de alto y 170px de ancho</p>
                                </div>
                                <div class="form-group center-block">
                                    <label for="imgactual">Imagen Actual</label>
                                    <input type ="hidden"  name="imagenactual1" id="imagenactual1" value="'.$rows["img_uno"].'"/>
                                    <ul class="hide-bullets">
                                        <li>';
    $imagen = '../img/eventos/'.$rows["img_uno"];
 if (!file_exists($imagen)){
    echo'<img src="../img/eventos/sin-imagen.png"  alt="'.$rows["titulo_uno"].'" class="img-responsive img-rounded"/>';
                }else{

    echo'<img src="'.$imagen.'"  alt="Portada: '.$rows["titulo_uno"].'" class="img-responsive img-rounded"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["titulo_uno"].'"/>';
 }
echo'
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="box-header with-border">
                                <h3 class="box-title">Texto Destacado #2</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputtitulo_DOS">Titulo</label>
                                    <input type="text" class="form-control" name="titulo_DOS" id="titulo_DOS" placeholder="Titulo del Item destacado" value="'.$rows["titulo_dos"].'">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputtexto_DOS">Descripcion</label>
                                    <textarea name="texto_DOS" id="summernote2" class="form-control">'.utf8_decode($rows["texto_dos"]).'</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen </label>
                                    <input type="file" id="imagen2" name="imagen2"/>
                                    <p class="help-block">La imagen debe ser de 320px de alto y 170px de ancho</p>
                                </div>
                                <div class="form-group">
                                    <label for="imgactual">Imagen Actual</label>
                                    <input type ="hidden" name="imagenactual2" id="imagenactual1" value="'.$rows["img_dos"].'"/>
                                    <ul class="hide-bullets">
                                        <li>';
    $imagen = '../img/eventos/'.$rows["img_dos"];
 if (!file_exists($imagen)){
    echo'<img src="../img/eventos/sin-imagen.png"  alt="'.$rows["titulo_dos"].'" class="img-responsive img-rounded"/>';
                }else{

    echo'<img src="'.$imagen.'"  alt="Portada: '.$rows["titulo_dos"].'" class="img-responsive img-rounded"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["titulo_dos"].'"/>';
 }
echo'                                   </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="box-header with-border">
                                <h3 class="box-title">Texto Destacado #3</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputtitulo_TRES">Titulo</label>
                                    <input type="text" class="form-control" name="titulo_TRES" id="titulo_TRES" placeholder="Titulo del Item destacado" value="'.$rows["titulo_tres"].'">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputtexto_TRES">Descripcion</label>
                                    <textarea name="texto_TRES" id="summernote3" class="form-control">'.utf8_decode($rows["texto_tres"]).'</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen </label>
                                    <input type="file" id="imagen3" name="imagen3"/>
                                    <p class="help-block">La imagen debe ser de 320px de alto y 170px de ancho</p>
                                </div>
                                <div class="form-group">
                                    <label for="imgactual">Imagen Actual</label>
                                    <input type ="hidden" name="imagenactual3" id="imagenactual1" value="'.$rows["img_tres"].'"/>
                                    <ul class="hide-bullets">
                                        <li>';
    $imagen = '../img/eventos/'.$rows["img_tres"];
 if (!file_exists($imagen)){
    echo'<img src="../img/eventos/sin-imagen.png"  alt="'.$rows["titulo_tres"].'" class="img-responsive img-rounded"/>';
                }else{

    echo'<img src="'.$imagen.'"  alt="Portada: '.$rows["titulo_tres"].'" class="img-responsive img-rounded"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["titulo_tres"].'"/>';
 }
echo'
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor,
                                    pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                    Cambiar Datos
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function CREAR_CONTENIDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                <div class="box-header with-border">
                    <h3 class="box-title">Agrega un nuevo evento</h3>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="include/admin_agregar_contenido_evento.php" role="form" method="post" onsubmit="return FormAgregarContenidoEvento(); return document.MM_returnValue" name="FormAgregarContEvento" id="FormAgregarContEvento">
                      <div class="box-body">
       <div class="form-group">
                          <label for="nombre_evento">Titulo del Item</label>
                          <input class="form-control" type="text" id="titulo_detalle" name="titulo_detalle">
                        </div>
                        <div class="form-group">
                          <label for="texto_evento">Descripcion del Item</label>
                          <textarea class="form-control" name="descripcion_detalle" id="summernote"></textarea>
                          <p class="help-block">Este campo no debe estar vacio</p>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen del Item</label>
                                <input type="file" id="imagen"  name="imagen">
                            </div>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">
                                    Crear Contenido
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function LISTAR_TODO_CONTENIDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    $sql = sprintf("SELECT * FROM hesperia_detalles_eventos");
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $cant = 0;
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eventos actuales en el hotel</h3>
                        </div>
                        <div class="box-body">';
    if (!empty($_GET["ok"])){
        unset($_GET["ok"]);
    }
    while ($rows=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $cant = 1;
        echo '
                            <div class="list-group">
                                <span class="list-group-item clearfix">
                                    '.$rows["titulo_detalle"].'
                                    <span class="pull-right">
                                        <a class="btn btn-success btn-xs" href="home.php?go=EditarContenidoEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Editar
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="home.php?go=VerContenidoEventos&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-trash-o"></i>
                                            Eliminar
                                        </a>
                                    </span>
                                </span>
                            </div>';
    }
    if ($cant == 0){
echo '<div class="callout callout-danger text-center">
        <h4>
            Disculpe
        </h4>
        <p>
            Actualmente no hay eventos en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
        </p>
    </div>';
    }else{
        if (isset($_REQUEST["id"])) {
        $id = $_REQUEST["id"];
        $sql = sprintf("SELECT img_detalle FROM hesperia_detalles_eventos WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $imagen = "../img/eventos/".$rows["img_detalle"];
        @unlink($imagen);
        $sql = sprintf("DELETE FROM hesperia_detalles_eventos WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        echo'<div class="alert alert-success" role="alert">
                    <p>Procesado acción de eliminación</p>
                    <meta http-equiv="refresh" content="3; url=home.php?go=VerContenidoEventos&tok='.time().'&ok=1&id_hotel='.$id_hotel.'"/>
            </div>';
        }
    }
    echo '<p class="help-block">Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function EDITAR_CONTENIDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Actualizar Evento</h3>
                </div>
                <div class="box-body">
<form enctype="multipart/form-data" action="include/admin_editar_contenido_evento.php" role="form" method="post" onsubmit="return EditarContenidoEvento(); return document.MM_returnValue" name="FormEditarConteEvento" id="FormEditarConteEvento">
                      <div class="box-body">';
    if(isset($_SESSION["mensaje"])){
        echo'<div id="InformacionAgregarEvento">'.$_SESSION["mensaje"].'</div>';
        unset($_SESSION["mensaje"]);
    }
    $id = $_GET["id"];
        $sql = sprintf("SELECT * FROM hesperia_detalles_eventos WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultEventos = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowE = mysqli_fetch_array($resultEventos,MYSQLI_ASSOC);
    echo '   <div class="form-group">
                          <label for="nombre_evento">Nombre del evento</label>
                          <input class="form-control" type="text" id="nombre_evento" name="nombre_evento" value="'.$rowE["titulo_detalle"].'">
                        </div>
                        <div class="form-group">
                          <label for="texto_evento">Descripcion del evento</label>
                          <textarea class="form-control" name="texto_evento" id="summernote">'.$rowE["descripcion_detalle"].'</textarea>
                          <p class="help-block">Este campo no debe estar vacio</p>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputFile">Imagen del Evento</label>
                           <input type="file" id="imagen"  name="imagen">
                           <input type="hidden" id="imagen"  name="imagenActual" value="'.$rowE["img_detalle"].'">
                        </div>
                        <div class="form-group">
                          <p>
                            Haga clic
                            <a href="#0" data-toggle="modal" data-target="#myModal">aquí</a> para ver la imagen actual
                          </p>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Enviar</button>
                                <input type="hidden" value="'.$id.'" name="id" id="id">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Imagen actual</h4>
      </div>
      <div class="modal-body text-center">
        <img src="../img/eventos/'.$rowE["img_detalle"].'" data-toggle="tooltip" data-placement="bottom" title="'.$rowE["titulo_detalle"].'" class="img-responsive">
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>';
    return;
}
?>
