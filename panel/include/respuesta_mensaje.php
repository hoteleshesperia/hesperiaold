<?php
/* -------------------------------------------------------
Script  bajo los t&eacute;rminos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor: ViserProject.com
-------------------------------------------------------- */

session_start();
if (empty($_SESSION["referencia"])) {
    echo '<div class="alert alert-danger" role="alert">
                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
    die(); }
$antesdecore = 1;
$tiempo=time();
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
$mensaje=filter_var(trim($_POST["mensajerespuesta"]),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$id_mensaje = $_POST["id_mensaje"];
$emailContacto = $_POST["email"];
$nombresContacto = $_POST["nombres"];
$emailEnvio = $data["correo_envio"];
$dominio = $data["dominio"];
$razon_social = $data["razon_social"];
$asunto  = "Respuesta mensaje desde $dominio";
$mensaje = "Saludos Sr(a). $nombresContacto<br>\r\n
                    Respuesta a su mensaje:<br>\r\n
                    $mensaje<br>\r\n
                    Cualquier duda contactenos al siguiente email: $emailEnvio<br>\r\n
                    ----------------------------------------------------------------------------------------<br>\r\n
                    Este mensaje es privado y confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.\r\n<br>
                    ----------------------------------------------------------------------------------------<br>\r\n
                    $dominio<br><br>";
                    $headers = '';
                    $headers .= "MIME-Version:1.0\r\n";
                    $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                    $headers .= "Received:from $dominio\r\n";
                    $headers .= "X-Priority:3\r\n";
                    $headers .= "X-MSMail-Priority:Normal\r\n";
                    $headers .= "From: $razon_social <$emailEnvio>\r\n";
                    $headers .= "X-Mailer:$emailEnvio\r\n";
                    $headers .= "Return-path:$emailEnvio\r\n";
                    $headers .= "Reply-To:$emailEnvio\r\n";
                    $headers .= "X-Antiabuse:Enviar notificacion a $emailEnvio\r\n";
                    if (@mail($emailContacto,"RE:$asunto",$mensaje,$headers))
                    { echo '<div class="alert alert-success" role="alert">
                    <p>Se ha enviado el mensaje de respuesta a <strong>'.$nombresContacto.'</strong>.</p>
                    </div>';
                    $ahora = time();
                    $sql=sprintf("UPDATE mensajes SET
                                        fecha_visto = '%s',
                                        visto_por = '%s',
                                        respondido = '1'
                                        WHERE id_mensaje = '%s'",
                                mysqli_real_escape_string($mysqli,$ahora),
                                mysqli_real_escape_string($mysqli,$_SESSION["nombre"]),
                                mysqli_real_escape_string($mysqli,$id_mensaje));
                    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                    graba_LOG("Respuesta de mensaje a: $emailContacto",$_SESSION["nombre"],$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
                    } else
                    { echo '<div class="alert alert-danger" role="alert">
              <p>Ha ocurrido un error inesperado. No se pudo enviar el mensaje de respuesta. Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
            </div>'; }
unset($result,$sql,$email,$headers,$mensaje);
$_POST = array();
?>
