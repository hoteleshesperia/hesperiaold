<?php

function SUBIR_ARCHIVOS_MEDIA($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Subir Archivos PDF/DOC/XLS - Boletines - Video | Sala de Prensa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">';
              echo '<form  class="form-horizontal" enctype="multipart/form-data" action="home.php?go=SubirArchivosMedia" role="form" method="post" onsubmit="return AgregarDescargas();" name="Descargas" id="Descargas">
                        <div class="form-group">
                            <label for="inputtitulo" class="col-sm-2 control-label">
                                Tipo:
                            </label>
                            <div class="col-sm-10">
                            <select name="tipo" id="tipo" class="form-control">
                                <option value="">Seleccione el tipo de Descarga</option>
                                <option value="2">Boletin</option>
                                <option value="1">PDF|DOC|XLS</option>
                                <option value="0">Video</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputtitulo" class="col-sm-2 control-label">
                                Titulo:
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="titulo" name="titulo"/>
                                <p class="help-block">Máximo 200 caracteres</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputdescripcion" class="col-sm-2 control-label">
                                Descripción:
                            </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="descripcion" id="summernote"></textarea>
                                <p class="help-block">Breve descripcion del contenido</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputenlace" class="col-sm-2 control-label">
                                Enlace Video:
                            </label>
    <div class="col-sm-10"><p class="help-block">Solo para videos. Ejemplo: https://www.youtube.com/watch?v=X2ABs1PIQMQ Solo colocar el valor despues de v= Es decir: <strong><code>X2ABs1PIQMQ</code></strong></p>
                                <input type="text" class="form-control" id="enlace" name="enlace" value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputimagen" class="col-sm-2 control-label">
                                Archivo:
                            </label>
                            <div class="col-sm-10">
                                <input type="file" name="archivo" class="form-control" id="archivo"/>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                            Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">
                                    Subir Archivo
                                </button>
                            </div>
                        </div>
                    </form>';
if (isset($_POST["tipo"]))
    {
         if (!empty($_FILES['archivo']['name'])) {
            if(move_uploaded_file($_FILES['archivo']['tmp_name'], '../img/prensa/'.$_FILES['archivo']['name'])){
                    $nombre = $_FILES['archivo']['name'];
                    $nuevo_nombre = substr(time(),-4).'_'.$_FILES['archivo']['name'];
                    $search = array('%27',' ','-');
                    $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
                    rename ("../img/prensa/$nombre", "../img/prensa/$nuevo_nombre");
                    chmod("../img/prensa/$nuevo_nombre", 0777);
            }
            else {$nuevo_nombre = 'sin-imagen.png'; }
    }
    $ahora = time();
    $tipo = trim($_POST["tipo"]);
    $enlace = trim($_POST["enlace"]);
    if ($tipo == 0) { $nuevo_nombre =  $enlace; }
    $titulo = trim($_POST["titulo"]);
    $descripcion = nl2br(trim($_POST["descripcion"]));
   $sql = sprintf("INSERT INTO hesperia_descarga VALUES (NULL, '%s','%s', '%s', '%s','%s')",
                    mysqli_real_escape_string($mysqli,$titulo),
                    mysqli_real_escape_string($mysqli,$nuevo_nombre),
                    mysqli_real_escape_string($mysqli,$descripcion),
                    mysqli_real_escape_string($mysqli,$tipo),
                    mysqli_real_escape_string($mysqli,$ahora));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
     echo '<div class="box-footer">
                <div class="callout callout-success text-center">
                    <h4>Se ha guardado la información correctamente</h4>
                </div>
                </div>';
            graba_LOG("Agregado archivo $nuevo_nombre",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
}
echo '
                </div>
            </div>
        </div>
    </div>
</section>
';
    return;
}

function ELIMINAR_ARCHIVOS_MEDIA($mysqli,$data,$hostname,$user,$password,$db_name){
echo '<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Eliminar Archivos - Boletines - Video | Sala de Prensa</h3>
                </div>
                <div class="box-body">';
$sql = sprintf("SELECT id,titulo FROM hesperia_descarga ORDER BY id DESC");
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);

$hay = mysqli_num_rows($result);
    if ($hay < 1){
        echo '
    <div class="text-center"><i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>No hay Eliminar Archivos - Boletines - Video | Sala de Prensa.</p></div>';
    }else{
        echo'
<div id="EliminacionArchivosMedia"></div>
                     <form  class="form-horizontal" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return EliminarDescargas();" name="DescargasE" id="DescargasE">
                    <div class="form-group">
                        <label for="inputtitulo" class="col-sm-2 control-label">
                            Tipo:
                        </label>
                        <div class="col-sm-10">
                        <select name="id" id="id" class="form-control">
                        <option value="0">Seleccione</option>';

                            while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                                echo '<option value="'.$rows["id"].'">'.$rows["titulo"].'</option>';
                            }
echo '						</select>
                        </div>
                    </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                            Esta acción no posee confirmación
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">
                                    Eliminar Archivo
                                </button>
                            </div>
                        </div>
                    </form>
        ';
    }
echo'
                </div>
            </div>
        </div>
    </div>
</section>';
return;
}
?>
