<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/
//echo'<pre>';
//print_r($_POST);
//echo'</pre>';
//die();

session_start();

if (empty($_SESSION["referencia"])) {
    echo '<div class="alert alert-danger" role="alert">Ha ocurrido un error inesperado.<br/>Su sesion ha expirado</div>';
    die(); }

$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
extract($_POST);
$razon_social = trim($razon_social);
$descripcion = nl2br(trim($descripcion));
$correo_envio=strip_tags(strtolower(trim($correo_envio)));
$correo_envio=filter_var($correo_envio,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$email_reserva=strip_tags(strtolower(trim($email_reserva)));
$email_reserva=filter_var($email_reserva,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$correo_contacto=strip_tags(strtolower(trim($correo_contacto)));
$correo_contacto=filter_var($correo_contacto,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$sql = sprintf("UPDATE hesperia_settings SET
                        razon_social = '%s',
                        nombre_sitio = '%s',
                        rif = '%s',
                        descripcion = '%s',
                        dominio = '%s',
                        direccion = '%s',
                        ciudad = '%s',
                        estado = '%s',
                        pais = '%s',
                        latitud = '%s',
                        longitud = '%s',
                        telefonos = '%s',
                        telf_reserva = '%s',
                        check_in = '%s',
                        check_out = '%s',
                        postal = '%s',
                        twitter = '%s',
                        facebook = '%s',
                        gplus = '%s',
                        youtube = '%s',
                        instagram = '%s',
                        pinterest = '%s',
                        linkedin = '%s',
                        correo_envio = '%s',
                        correo_contacto = '%s',
                        keywords = '%s',
                        description = '%s',
                        salones = '%s',
                        eventos = '%s',
                        escapadas = '%s',
                        experiencias = '%s',
                        mascotas = '%s',
                        wifi = '%s',
                        piscina = '%s',
                        estacionamiento = '%s',
                        botones = '%s',
                        gimnasio = '%s',
                        spa = '%s',
                        helipuerto = '%s',
                        accesibilidad = '%s',
                        nana = '%s',
                        transporte = '%s',
                        vigilancia_privada = '%s',
                        servicio_medico = '%s',
                        chofer = '%s',
                        alquiler_vehiculos = '%s',
                        cafetin = '%s',
                        restaurant = '%s',
                        bar = '%s',
                        discotheca = '%s',
                        golf = '%s',
                        tenis = '%s',
                        basket = '%s',
                        tienda = '%s',
                        cant_habitaciones = '%s',
                        parque = '%s',
                        terraza = '%s',
                        tripadvisor = '%s',
                        tripadvisor_premio = '%s',
                        email_reserva = '%s'
                        WHERE id = '%s'",
               mysqli_real_escape_string($mysqli,$razon_social),
               mysqli_real_escape_string($mysqli,$nombre_sitio),
               mysqli_real_escape_string($mysqli,$rif),
               mysqli_real_escape_string($mysqli,$descripcion),
               mysqli_real_escape_string($mysqli,$dominio),
               mysqli_real_escape_string($mysqli,$direccion),
               mysqli_real_escape_string($mysqli,$ciudad),
               mysqli_real_escape_string($mysqli,$estado),
               mysqli_real_escape_string($mysqli,$pais),
               mysqli_real_escape_string($mysqli,$latitud),
               mysqli_real_escape_string($mysqli,$longitud),
               mysqli_real_escape_string($mysqli,$telefonos),
               mysqli_real_escape_string($mysqli,$telf_reserva),
               mysqli_real_escape_string($mysqli,$check_in),
               mysqli_real_escape_string($mysqli,$check_out),
               mysqli_real_escape_string($mysqli,$postal),
               mysqli_real_escape_string($mysqli,$twitter),
               mysqli_real_escape_string($mysqli,$facebook),
               mysqli_real_escape_string($mysqli,$gplus),
               mysqli_real_escape_string($mysqli,$youtube),
               mysqli_real_escape_string($mysqli,$instagram),
               mysqli_real_escape_string($mysqli,$pinterest),
               mysqli_real_escape_string($mysqli,$linkedin),
               mysqli_real_escape_string($mysqli,$correo_envio),
               mysqli_real_escape_string($mysqli,$correo_contacto),
               mysqli_real_escape_string($mysqli,$keywords),
               mysqli_real_escape_string($mysqli,$description),
               mysqli_real_escape_string($mysqli,$salones),
               mysqli_real_escape_string($mysqli,$eventos),
               mysqli_real_escape_string($mysqli,$escapadas),
               mysqli_real_escape_string($mysqli,$experiencias),
               mysqli_real_escape_string($mysqli,$mascotas),
               mysqli_real_escape_string($mysqli,$wifi),
               mysqli_real_escape_string($mysqli,$piscina),
               mysqli_real_escape_string($mysqli,$estacionamiento),
               mysqli_real_escape_string($mysqli,$botones),
               mysqli_real_escape_string($mysqli,$gimnasio),
               mysqli_real_escape_string($mysqli,$spa),
               mysqli_real_escape_string($mysqli,$helipuerto),
               mysqli_real_escape_string($mysqli,$accesibilidad),
               mysqli_real_escape_string($mysqli,$nana),
               mysqli_real_escape_string($mysqli,$transporte),
               mysqli_real_escape_string($mysqli,$vigilancia_privada),
               mysqli_real_escape_string($mysqli,$servicio_medico),
               mysqli_real_escape_string($mysqli,$chofer),
               mysqli_real_escape_string($mysqli,$alquiler_vehiculos),
               mysqli_real_escape_string($mysqli,$cafetin),
               mysqli_real_escape_string($mysqli,$restaurant),
               mysqli_real_escape_string($mysqli,$bar),
               mysqli_real_escape_string($mysqli,$discotheca),
               mysqli_real_escape_string($mysqli,$golf),
               mysqli_real_escape_string($mysqli,$tenis),
               mysqli_real_escape_string($mysqli,$basket),
               mysqli_real_escape_string($mysqli,$tienda),
               mysqli_real_escape_string($mysqli,$cant_habitaciones),
               mysqli_real_escape_string($mysqli,$parque),
               mysqli_real_escape_string($mysqli,$terraza),
               mysqli_real_escape_string($mysqli,$tripadvisor),
               mysqli_real_escape_string($mysqli,$tripadvisor_premio),
               mysqli_real_escape_string($mysqli,$email_reserva),
               mysqli_real_escape_string($mysqli,$id));
//die($sql);
                $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli)){
    echo '<div class="callout callout-success text-center" role="alert">
                    Los datos han sido cambiados satisfactoriamente!</div>';

//    Esto esta comentado debido que el sistema por los momentos no va a usar la tabla servicios
//    $id = mysqli_insert_id($mysqli);
//    $sqlServicios = sprintf("INSERT INTO hesperia_servicios
//VALUES (NULL,'%s', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '')",
//    mysqli_real_escape_string($mysqli,$id));
//    $resultadoServicios = QUERYBD($sqlServicios,$hostname,$user,$password,$db_name);
//    for($i =1; $i <=4; $i++){
//        $sql = sprintf("INSERT INTO hesperia_contenido id_categoria, texto, imagen
//        VALUES (NULL,'%s', '%s', 'sin-imagen.png')",
//        mysqli_real_escape_string($mysqli,$i),
//        mysqli_real_escape_string($mysqli,$id));
//        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
//    }
    $ahora=time();
    graba_LOG("Actualizado setting $razon_social",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
}else{
    echo '<div class="callout callout-warning text-center" role="alert">
                Ha ocurrido un error inesperado.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio.</div>';
}
unset($result,$sql,$email,$headers);
$_POST = array();
?>
