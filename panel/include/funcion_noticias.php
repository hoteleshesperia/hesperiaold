<?php
function PUBLICACIONES_NOTICIAS($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar Publicaciones de Prensa - Noticias</h3>
                </div>
                <div class="box-body">';
                if (!empty($_GET["ok"])){
                    echo $_SESSION["mensaje"];
                    $_SESSION["mensaje"] = '';
                    unset($_GET["ok"]);
                }
echo '<small>Si ha utilizado una nota, publicación, noticia de otro sitio, debe colocar los créditos correspondientes al autor original, por ejemplo: el enlace al sitio web de donde se extrajo</small>
                <form enctype="multipart/form-data" action="include/Admin_Publicaciones.php" role="form" method="post" onsubmit="return AgregarPublicaciones();" name="Publicaciones" id="Publicaciones">
                    <input  type="hidden" name="op" id="op" value="1"/>
                    <div class="form-group">
                        <label for="inputtitulo">
                            Titulo:
                        </label>
                        <input type="text" class="form-control" id="titulo" name="titulo" value=""/>
                        <p class="help-block">Máximo 250 caracteres</p>
                    </div>
                    <div class="form-group">
                        <label for="inputintro">
                            Introducción:
                        </label>
                        <textarea class="form-control" id="summernote2" name="intro"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputdescripcion">
                            Descripción:
                        </label>
                        <textarea class="form-control" name="contenido" id="summernote"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputimagen">
                            Imagen:
                        </label>
                        <input type="file" name="archivo" id="archivo" class="form-control">
                        <p class="help-block">
                        Se recomienda una imagen como minimo de 1024px de alto por 768px de ancho con 80dpi</p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-7">
                                    <p>
                                        Si ya realizo todo los cambios que deseaba hacer por favor,
                                        pulse el siguiente boton:
                                    </p>
                                </div>
                                <div class="col-lg-5">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                        Crear Nota / Publicación
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function EDITAR_NOTICIAS($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Editar Publicaciones de Prensa - Noticias</h3>
                </div>
                <div class="box-body">';
                if (!empty($_GET["ok"])){
                    echo $_SESSION["mensaje"];
                    $_SESSION["mensaje"] = '';
                    unset($_GET["ok"]);
                }
echo '<form role="form" method="post" action="home.php?go=PublicacionesE&tok='.time().'"  enctype="multipart/form-data"  onsubmit="return InfoValidaNoticias(); return document.MM_returnValue" role="form" method="post" name="InfoValidaNoticia" id="InfoValidaNoticia">
                    <div class="box-body">';
                $sql = sprintf("SELECT noticia_id,titulo FROM hesperia_noticias ORDER BY noticia_id ASC");
                $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
$hay = mysqli_num_rows($result);
if ($hay < 1){
    echo'<div class="callout callout-danger text-center">
            <h4>
                Disculpe
            </h4>
            <p>
                Actualmente no hay  Publicaciones de Prensa <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
            </p>
        </div>';
}else{
echo'<p>Seleccione la noticia o publicación que desea editar</p>
                        <div class="form-group">
                            <label for="inputpublicacion" class="sr-only sr-only-focusable">
                                Publicación:
                            </label>
                            <select class="form-control" name="noticia_id" id="noticia_id">
                                        <option value="0" selected>Seleccionar Publicación</option>';
                                        while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))
                                { echo '<option value="'.$rows["noticia_id"].'">'.$rows["titulo"].'</option>'; }
                            echo '
                            </select>
                        </div>
                        <div class="box-footer">
                            <div class="col-lg-5 pull-right">
                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                    Seleccionar Publicación
                                </button>
                            </div>
                        </div>';
    }
echo'
                    </div>
                </form>';
if (isset($_POST["noticia_id"])) {

                if (isset($_GET["ok"])){
                    echo $_SESSION["mensaje"];
                    $_SESSION["mensaje"] = '';
                }
echo '<form enctype="multipart/form-data" action="include/Admin_Publicaciones.php" role="form" method="post" onsubmit="return AgregarPublicaciones();" name="Publicaciones" id="Publicaciones">
                    <input type="hidden" id="op" name="op" value="2"/>
                    <input type="hidden" id="noticia_id" name="noticia_id" value="'.$_REQUEST["noticia_id"].'"/>';
                    $noticia_id = $_REQUEST["noticia_id"];
                    $sql = sprintf("SELECT * FROM hesperia_noticias WHERE  noticia_id ='%s'",
                            mysqli_real_escape_string($mysqli,$noticia_id));
                    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
echo '				<div class="form-group">
                        <label for="inputdescripcion">
                            Titulo:
                        </label>
                        <input type="text" class="form-control" id="titulo" name="titulo" value="'.$row["titulo"].'"/>
                    </div>
                    <div class="form-group">
                        <label for="inputdescripcion">
                            Introducción:
                        </label>
                        <textarea class="form-control"name="intro" id="summernote">'.$row["intro"].'</textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputdescripcion">
                            Descripción:
                        </label>
                            <textarea class="form-control"name="contenido" id="summernote">'.$row["contenido"].'</textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputdescripcion">
                            Seleccione imagen:
                        </label>
                        <input type="file" name="archivo" id="archivo" class="form-control">
                        <input type="hidden" name="imagen_actual" id="imagen_actual" value="'.$row["imagen"].'">
                        <p class="help-block">Para ver la imagen actual haga clic
                            <a href="#0" title="Ver imagen" data-toggle="modal" data-target="#myModal">
                                aquí
                            </a>
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-footer">
                                <div class="col-lg-7">
                                    <p>
                                        Si ya realizo todo los cambios que deseaba hacer por favor,
                                        pulse el siguiente boton:
                                    </p>
                                </div>
                                <div class="col-lg-5">
                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                       Actualizar Publicación
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>';
}
echo '
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger" title="Cerrar">×</span>
                </button>
                <h4 class="modal-title">Imagen actual</h4>
            </div>
            <div class="modal-body">';
    if(!empty($row["imagen"])){
        echo'<img src="../img/prensa/'.$row["imagen"].'" class="img-responsive" style="margin:0 auto;">';
    }else{
        echo'
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay una imagen para mostrar
                </p>
            </div>';
    }
    echo'
            </div>
            <div class="modal-footer">
                <p class="help-block">
                    Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.
                </p>
            </div>
        </div>
    </div>
</div>';
return;
}

function ELIMINAR_NOTICIAS($mysqli,$data,$hostname,$user,$password,$db_name){
echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Eliminar Publicaciones de Prensa - Noticias</h3>
                    <br/><small>Una vez haga click en el botón eliminar, no hay vuelta átras, será eliminada la noticia / publicación junto con imagen asociada</small>
                </div>
                <div class="box-body">';
                if (!empty($_GET["ok"])){
                    echo $_SESSION["mensaje"];
                    $_SESSION["mensaje"] = '';
                    unset($_GET["ok"]);
                }
echo '<form action="home.php?go=PublicacionesD&tok='.time().'"   enctype="multipart/form-data" onsubmit="return InfoValidaNoticiasDel(); return document.MM_returnValue" role="form" method="post" name="InfoValidaNoticiad" id="InfoValidaNoticiad">';
$sql = sprintf("SELECT noticia_id,titulo FROM hesperia_noticias ORDER BY noticia_id ASC");
$result=QUERYBD($sql,$hostname,$user,$password,$db_name);
$hay = mysqli_num_rows($result);
if ($hay < 1){
    echo'<div class="callout callout-danger text-center">
            <h4>
                Disculpe
            </h4>
            <p>
                Actualmente no hay  Publicaciones de Prensa <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
            </p>
        </div>';
}else{
echo'<p>Seleccione la noticia o publicación que desea editar</p>
<input type="hidden" id="op" name="op" value="3"/>
                    <div class="form-group">
                        <label for="inputpublicacion" class="sr-only sr-only-focusable">
                            Publicación:
                        </label>
                        <select class="form-control" name="eliminar_id" id="eliminar_id">
                                       <option value="0" selected>Seleccionar Publicación</option>';
                                        while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))
                                    { echo '<option value="'.$rows["noticia_id"].'">'.$rows["titulo"].'</option>'; }
                  echo '</select>
                    </div>
                        <div class="box-footer">
                            <div class="col-lg-5 pull-right">
                                <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
                                    Eliminar Nota / Publicación
                                </button>
                            </div>
                        </div>';
}
echo'</form>';
if (isset($_POST["eliminar_id"])) {

                    $noticia_id = $_POST["eliminar_id"];
                    $sql = sprintf("SELECT imagen FROM hesperia_noticias WHERE noticia_id = '%s'",
                            mysqli_real_escape_string($mysqli,$noticia_id));
                    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                    $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
                    $imagen = "../img/prensa/".$rows["imagen"];
                    @unlink($imagen);
                    $sql = sprintf("DELETE FROM hesperia_noticias WHERE noticia_id = '%s'",
                            mysqli_real_escape_string($mysqli,$noticia_id));
                    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
echo '			<div class="alert alert-success" role="alert">
                        <p>Procesado acción de eliminación</p>
                        <meta http-equiv="refresh" content="3; url=home.php?go=PublicacionesD&tok='.time().'&ok=1"/>
                </div>';
}
echo '
                </div>
            </div>
        </div>
    </div>
</section>';
return;
}
?>
