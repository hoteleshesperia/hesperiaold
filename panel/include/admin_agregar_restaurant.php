<?php
session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$_SESSION["mensaje"]  = '';

function revisaloImagen()
{ $allowed = array('png', 'jpg');
    if(isset($_FILES["upl"]) && $_FILES["upl"]["error"] == 0){
        $extension = pathinfo($_FILES["upl"]["name"], PATHINFO_EXTENSION);
               if(!in_array(strtolower($extension), $allowed)){
                return 0; }
    }
return 1;
}

function subeloimagen()
{ if(move_uploaded_file($_FILES["upl"]["tmp_name"], '../../img/restaurant/'.$_FILES["upl"]["name"])){
        $ahora = time();
        $nombre = $_FILES["upl"]["name"];
        $nuevo_nombre = substr(time(),-4).'_'.$_FILES["upl"]["name"];
        $search = array('%27',' ','-');
        $nuevo_nombre = str_replace($search,'_',$nuevo_nombre);
        rename ("../../img/restaurant/$nombre", "../../img/restaurant/$nuevo_nombre");
        chmod("../../img/restaurant/$nuevo_nombre", 0777);
        $_SESSION["newname"] = $nuevo_nombre;
        return 1;
    }
    else { return 0;}
return 1;
}

if (empty($_POST["caracteristicas"]) || $_POST["caracteristicas"] ==  '<p><br></p>')
{ $id = $_POST["id_restaurant"];
    $_SESSION["mensaje"] = '<div class="callout callout-danger text-center">
                <h4>ERROR</h4>
                <p>Debe indicar la descripción del restaurant</p>
            </div>';
echo '<meta http-equiv="refresh" content="0; url=../home.php?go=AgregarRestaurant&id='.$id.'&tok='.md5($ahora).'"/>';
    die();
}

$imagen = '';

if (!empty($_FILES["imagen"]["name"]))
    { 	$_FILES["upl"] = $_FILES["imagen"];
        if  (revisaloImagen())
        { 	if (subeloimagen())
                {	$imagen = $_SESSION["newname"];
            $_SESSION["mensaje"] .=  '<div class="callout callout-success text-center">
            <p>Imagen indicada '.$_FILES["imagen"]["name"].' ha sido subida al servidor correctamente</p>
       </div>'; }
            else
            { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen  indicada '.$_FILES["imagen"]["name"].' no pudo ser subida al servidor. Intente nuevamente.</p>
            </div>'; }
        }
        else
        { $_SESSION["mensaje"] .= '<div class="box-body">
            <div class="callout callout-danger text-center"><h4>Disculpe</h4><p>
                ERROR: La imagen  indicada '.$_FILES["imagen"]["name"].' está en un formato no válido.Intente nuevamente</p>
            </div>';
        }
}
$antesdecore2 = 1;
include 'databases.php';
$id_hotel = $_POST["id_hotel"];
$tipo_comida = trim($_POST["tipo_comida"]);
$caracteristicas = nl2br(trim($_POST["caracteristicas"]));
$hora_apertura = trim($_POST["hora_apertura"]);
$hora_cierre = trim($_POST["hora_cierre"]);
$nombre_restaurant = trim($_POST["nombre_restaurant"]);

$sql = sprintf("INSERT INTO hesperia_restaurant
                        VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
               mysqli_real_escape_string($mysqli,$id_hotel),
               mysqli_real_escape_string($mysqli,$nombre_restaurant),
               mysqli_real_escape_string($mysqli,$tipo_comida),
               mysqli_real_escape_string($mysqli,$hora_apertura),
               mysqli_real_escape_string($mysqli,$hora_cierre),
               mysqli_real_escape_string($mysqli,$caracteristicas ),
               mysqli_real_escape_string($mysqli,$imagen));
$result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
$ahora = time();
if (mysqli_affected_rows($mysqli)){
   $_SESSION["mensaje"] .= '
        <div class="callout callout-success text-center">
            <h4>Almacenada información de restaurant en la base de datos!</h4>
        </div>';
graba_LOG("Nuevo restaurant $nombre_restaurant",$_SESSION["nombre"],$_SERVER["REMOTE_ADDR"],$ahora,$hostname,$user,$password,$db_name);
    }else{ $_SESSION["mensaje"] .= '
        <div class="callout callout-danger text-center">
            <h4>Disculpe! </h4>
            <p>Hay un problema por lo que no se realizó la actualización de la información.</p>
        </div>';
}
echo '<meta http-equiv="refresh" content="0; url=../home.php?go=Restaurant&tok='.md5($ahora).'"/>';
?>
