<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/

session_start();
//if (empty($_SESSION["referencia"])) {
//    echo '<div class="alert alert-danger" role="alert">
//                    Ha ocurrido un error inesperado. Probablemente su sesion ha expirado y/o ha intentado ingresar directamente.</div>';
//    die(); }
$antesdecore2 = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
unset($sql);
$ahora = time();
$id = $_POST['id'];
$email = strip_tags(strtolower(trim($_POST['email'])));
$email = filter_var($email,FILTER_SANITIZE_EMAIL);
$apellidos = strip_tags(ucwords(trim($_POST['apellidos'])));
$nombres = strip_tags(ucwords(trim($_POST['nombres'])));
$telefono = strip_tags(trim($_POST['telefono']));
$numero_empresa  = trim($_POST['numero_empresa']);
$email_empresa = strip_tags(strtolower(trim($_POST['email_empresa'])));
$email_empresa= filter_var($email,FILTER_SANITIZE_EMAIL);
$numero_empresa  = trim($_POST['numero_empresa']);
$empresa = strip_tags(trim($_POST['empresa']));
$valido = $_POST['valido'];
$nivel = $_POST['nivel'];
graba_LOG("Cambio de datos usuario: $email",$_SESSION["nombre"],$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
$sql = sprintf("UPDATE hesperia_usuario SET
                    email = '%s', nombres = '%s',
                    apellidos = '%s', telefono = '%s',
                    empresa = '%s', numero_empresa = '%s',
                    email_empresa = '%s',
                    valido = '%s',
                    nivel = '%s'
                    WHERE id = '%s'",
                    mysqli_real_escape_string($mysqli,$email),
                    mysqli_real_escape_string($mysqli,$nombres),
                    mysqli_real_escape_string($mysqli,$apellidos),
                    mysqli_real_escape_string($mysqli,$telefono),
                    mysqli_real_escape_string($mysqli,$empresa),
                    mysqli_real_escape_string($mysqli,$numero_empresa),
                    mysqli_real_escape_string($mysqli,$email_empresa),
                    mysqli_real_escape_string($mysqli,$valido),
                    mysqli_real_escape_string($mysqli,$nivel),
                    mysqli_real_escape_string($mysqli,$id));
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli))
    { echo '<br/><div class="callout callout-success text-center">
        <p>Cambios realizados con exito en usuario seleccionado</p>
        </div>';
    echo '<meta http-equiv="refresh" content="2; url=home.php?go=VerUsuario&id='.$id.'&tok='.md5($ahora).'"/>';
 } else
 { echo '<div class="alert alert-danger" role="alert">
      <p>Ha ocurrido un error inesperado. No se cambio nada al usuario seleccionado.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
    </div>'; }
unset($result,$sql);
$_POST = array();
?>
