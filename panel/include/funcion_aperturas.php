<?php

function CREAR_APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Agregar contenido para el apartado <strong>Próximas Aperturas</strong></h3>
                        </div>
                        <div class="box-body">';
                         if(isset($_SESSION["mensaje"])){
                            echo $_SESSION["mensaje"];
                            unset($_SESSION["mensaje"]);
                        }echo'
                        <form enctype="multipart/form-data" action="include/admin_agregar_aperturas.php" role="form" method="post" onsubmit="return AgregarApertura(); return document.MM_returnValue" name="FormAgregarApertura" id="FormAgregarApertura">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="titulo_apertura">Titulo de la Apertura</label>
    <input type="text" class="form-control" name="titulo_apertura" id="titulo_apertura" placeholder="Titulo de la Apertura">
                                    </div>
                                    <div class="form-group">
                                        <label for="descripcion_paquete">Texto descriptivo de la Apertura</label>
                                        <textarea class="form-control" name="texto_apertura" id="summernote"></textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Agregar Contenido
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function ACTUALIZAR_APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Actualizar experiencia</h3>
                        </div>
                        <div class="box-body">';
                         if(isset($_SESSION["mensaje"])){
                            echo $_SESSION["mensaje"];
                             unset($_SESSION["mensaje"]);
                        }echo'
                            <form enctype="multipart/form-data" action="include/admin_actualiza_aperturas.php" role="form" method="post" onsubmit="return AgregarApertura(); return document.MM_returnValue" name="FormAgregarApertura" id="FormAgregarApertura">
<div id="InformacionAgregarExperiencias"></div>';
    $id =   $_GET["id"];
    $sql = sprintf("SELECT * FROM hesperia_aperturas WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $resultExp = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultExp,MYSQLI_ASSOC);
    echo'
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="nombre_expe">Titulo de la Apertura</label>
<input type="text" class="form-control" name="titulo_apertura" id="titulo_apertura" placeholder="Titulo de la Apertura" value="'.$rows["titulo_apertura"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_expe">Texto descriptivo de la Apertura</label>
            <textarea class="form-control" name="texto_apertura" id="summernote">'.$rows["texto_apertura"].'</textarea>
                                        <p class="help-block">Este campo no debe estar vacio</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagenexpe">Subir imagen</label>
                                        <input id="imagen" name="imagen" type="file" class="form-control">
                                        <p class="help-block">La imagen debe ser de 320px de alto por 170px de ancho.</p>
                            <input type="hidden" id="imagenactual" name="imagenactual" value="'.$rows["img_apertura"].'">
                                        <p class="help-block">Para ver la imagen actual haga clic
                                            <a href="#0" title="Ver imagen" data-toggle="modal" data-target="#myModal">
                                                aquí
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            Actualizar Paquete
                                        </button>
                                        <input type="hidden" value="'.$id.'" name="id" id="id">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-danger" title="Cerrar">×</span>
                </button>
                <h4 class="modal-title">Imagen actual</h4>
            </div>
            <div class="modal-body">';
    if(!empty($rows["img_apertura"])){
        echo'<img src="../img/aperturas/'.$rows["img_apertura"].'" class="img-responsive" style="margin:0 auto;">';
    }else{
        echo'
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>
                    Actualmente no hay una imagen para mostrar
                </p>
            </div>';
    }
    echo'
            </div>
            <div class="modal-footer">
                <p class="help-block">
                    Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.
                </p>
            </div>
        </div>
    </div>
</div>';

    return;
}


//function FORM_SELECION_HOTEL_PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name){
//    echo'
//<section class="content">
//    <div class="row">
//        <div class="col-lg-12">
//            <div class="box box-info">
//                <div class="box-header with-border">
//                    <h3 class="box-title">Seleccione el Hotel para editar las Experiencias</h3>
//                </div>
//                <div class="box-body">
//                    <form action="home.php?go=VerPaquetes" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
//                        <div class="col-md-12">
//                            <div class="form-group">
//                                <label for="nombre_hotel"></label>
//                                    <select class="form-control" name="id_hotel" id="id_hotel">
//                                    <option value="0">Elija Hotel</option>';
//    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
//    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
//    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
//        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
//    }
//    echo'</select>
//                            </div>
//                        </div>
//                        <div class="col-md-12">
//                            <div class="box-footer">
//                                <div class="col-lg-5 pull-right">
//                                    <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">
//                                        Ver Lista de Experiencias
//                                    </button>
//                                </div>
//                            </div>
//                        </div>
//                    </form>
//                </div>
//            </div>
//        </div>
//    </div>
//</section>';
//    return;
//}


function LISTAR_APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Contenido publicado en <strong>Próximas aperturas</strong></h3>
                        </div>
                        <div class="box-body">';
//    if ($_SESSION["id_hotel"] == 0){
//        $id_hotel = $_REQUEST["id_hotel"];
//    }else{
//        $id_hotel = $_SESSION["id_hotel"];
//    }
    if (!empty($_GET["ok"])){
        unset($_GET["ok"]);
    }
    $cant = 0;
    $sql = sprintf("SELECT *
                    FROM hesperia_aperturas");
    $resultPaquetes = QUERYBD($sql,$hostname,$user,$password,$db_name);
    while ($rows = mysqli_fetch_array($resultPaquetes,MYSQLI_ASSOC)){
        $cant = 1;
        echo'
<div class="list-group">
    <span class="list-group-item clearfix">
        '.$rows["titulo_apertura"].'
        <span class="pull-right">
            <a href="home.php?go=ActualizarAperturas&id='.$rows["id"].'" class="btn btn-xs btn-info" title="Editar">
                <i class="fa fa-pencil"></i>
                Editar
            </a>
            <a href="home.php?go=AdministrarAperturas&id='.$rows["id"].'" class="btn btn-xs btn-danger" title="Eliminar">
                <i class="fa fa-trash-o"></i>
                Eliminar
            </a>
        </span>
    </span>
</div>';
    }
    if ($cant == 0){
        echo '
            <div class="callout callout-danger text-center">
                <h4>
                    Disculpe
                </h4>
                <p>Actualmente no hay Contenido.
    <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">
                    <strong>Regresar al menu anterior</strong>
                    </a>
                </p>
            </div>';
    }else{
        if (isset($_REQUEST["id"])) {
        $id = $_REQUEST["id"];
        $sql = sprintf("SELECT id,img_apertura FROM hesperia_aperturas WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $imagen = "../img/aperturas/".$rows["img_paquete"];
        @unlink($imagen);
        $sql = sprintf("DELETE FROM hesperia_aperturas WHERE id = '%s'",
                mysqli_real_escape_string($mysqli,$id));
        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
        echo'<div class="alert alert-success" role="alert"><p>Procesado acción de eliminación</p>
                    <meta http-equiv="refresh" content="5; url=home.php?go=AdministrarAperturas&tok='.time().'&ok=1"/>
            </div>';
        }
    }
    echo'<p class="help-block">Haga clic <a href="javascript:window.location.reload();" data-toggle="tooltip" data-placement="bottom" title="Actualizar la página"><strong>aquī</strong></a> si la información no esta actualizada.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    $_SESSION["mensaje"] = '';
    unset($_SESSION["mensaje"]);
    return;
}
?>
