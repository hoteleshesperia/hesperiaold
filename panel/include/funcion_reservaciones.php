<?php



function FORM_SELECCIONA_HOTEL_RESERVA($mysqli,$data,$hostname,$user,$password,$db_name){

    echo'

<section class="content">

    <div class="row">

        <div class="col-lg-12">

            <div class="box box-info">

                <div class="box-header with-border">

                    <h3 class="box-title">Seleccione el Hotel para editar las habitaciones</h3>

                </div>

                <div class="box-body">

                    <form action="home.php?go=ListaReservas" role="form" method="post">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label for="nombre_hotel"></label>

                                    <select class="form-control" name="id_hotel" id="id_hotel">';

    $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';

    }

    echo'</select>

                            </div>

                        </div>

                        <div class="col-md-12">

                            <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">Submit</button>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</section>';

    return;

}



function LISTA_RESERVAS($mysqli,$data,$hostname,$user,$password,$db_name){

    if (isset($_REQUEST["id_hotel"])){

        $id_hotel = $_REQUEST["id_hotel"];

    }else{ $id_hotel = $_SESSION["id_hotel"]; }

    $sql = sprintf("SELECT id,id_hotel,email FROM hesperia_v2_reservaciones WHERE status <> 'N' and status_transac = 0 ORDER BY id DESC",

                   mysqli_real_escape_string($mysqli,$id_hotel));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rowsReser = mysqli_fetch_array($result,MYSQLI_ASSOC);

    echo'
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>

<section class="content">

    <div class="row">

        <div class="col-lg-12">

            <div class="box box-info">

                <div class="row">

                    <div class="col-lg-12">

                        <div class="box-header with-border">

                            <h3 class="box-title">

                                Lista de reservaciones

                            </h3>

                        </div>

                        <div class="box-body">
<table class="table" id="table_id">
    <thead>
        <tr>
            <th>#</th>
            <th>Hotel</th>
            <th>Cliente</th>
            <th>Fecha CheckIn</th>
            <th># de Reserva</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>


                        ';

    $sql = sprintf("SELECT id,id_hotel,email,codigo_reserva,desde

                                                FROM hesperia_v2_reservaciones

                                                WHERE status <> 'N'

                                                and status_transac = 0

                                                ORDER BY id DESC");

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        $sqlH = sprintf("SELECT nombre_sitio

                                                FROM hesperia_settings

                                                WHERE id = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["id_hotel"]));

        $resultR = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

        $rowsSet = mysqli_fetch_array($resultR,MYSQLI_ASSOC);

        $sqlU = sprintf("SELECT nombres,apellidos

                                                FROM hesperia_usuario

                                                WHERE email = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["email"]));

        $resultU = QUERYBD($sqlU,$hostname,$user,$password,$db_name);

        $rowsUser = mysqli_fetch_array($resultU,MYSQLI_ASSOC);

        echo'

        <tr>
            <th scope="row">'.$rows["id"].'</th>
            <td>'.$rowsSet["nombre_sitio"].'</td>
            <td>'.$rowsUser["nombres"].' '.$rowsUser["apellidos"].'</td>
            <td>'.$rows["desde"].' </td>
            <td>'.$rows["codigo_reserva"].' </td>
            <td><a href="home.php?go=VerDetalleReservacion&cod_reserva='.$rows["id"].'" class="btn btn-xs btn-info">Ver Reserva</a></td>
        </tr>';}

    echo'

    </tbody>
</table>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section><script>

$(document).ready( function () {
    $(\'#table_id\').DataTable( {
     "order": [[ 0, "desc" ]],
     responsive: true
});
} );
</script>';

    return;

}



function LISTA_PRERESERVAS($mysqli,$data,$hostname,$user,$password,$db_name){

    if (isset($_REQUEST["id_hotel"])){

        $id_hotel = $_REQUEST["id_hotel"];

    }else{ $id_hotel = $_SESSION["id_hotel"]; }

    $sql = sprintf("SELECT id,id_hotel,email FROM hesperia_v2_reservaciones WHERE status <> 'N' and status_transac = 3 ORDER BY id DESC",

                   mysqli_real_escape_string($mysqli,$id_hotel));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rowsReser = mysqli_fetch_array($result,MYSQLI_ASSOC);

    echo'

<section class="content">

    <div class="row">

        <div class="col-lg-12">

            <div class="box box-info">

                <div class="row">

                    <div class="col-lg-12">

                        <div class="box-header with-border">

                            <h3 class="box-title">

                                Lista de reservaciones

                            </h3>

                        </div>

                        <div class="box-body">';

    $sql = sprintf("SELECT id,id_hotel,email,codigo_reserva

                                                FROM hesperia_v2_reservaciones

                                                WHERE status <> 'N'

                                                and status_transac = 3

                                                ORDER BY id DESC");

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        $sqlH = sprintf("SELECT nombre_sitio

                                                FROM hesperia_settings

                                                WHERE id = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["id_hotel"]));

        $resultR = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

        $rowsSet = mysqli_fetch_array($resultR,MYSQLI_ASSOC);

        $sqlU = sprintf("SELECT nombres,apellidos

                                                FROM hesperia_usuario

                                                WHERE email = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["email"]));

        $resultU = QUERYBD($sqlU,$hostname,$user,$password,$db_name);

        $rowsUser = mysqli_fetch_array($resultU,MYSQLI_ASSOC);

        echo'

                            <div class="list-group">



                                <span class="list-group-item clearfix">



                            Reserva para el: <strong>'.$rowsSet["nombre_sitio"].'</strong>

                                    por: '.$rowsUser["nombres"].' '.$rowsUser["apellidos"].'

                                  <span class="pull-right" style="margin-left:2px;">

                                    <a href="#" data-value="'.$rows["id"].'" data-tipo="hab" class="btn btn-xs btn-danger delete-prereserva">Eliminar</a>

                                  </span>



                                  <span class="pull-right">

                                    <a href="home.php?go=VerDetalleReservacion&cod_reserva='.$rows["id"].'" class="btn btn-xs btn-info">Ver Reserva</a>

                                  </span>



                                </span>

                            </div>';}

    echo'

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>';

    return;

}



function LISTA_RESERVAS_PAQ($mysqli,$data,$hostname,$user,$password,$db_name){

    if (isset($_REQUEST["id_hotel"])){

        $id_hotel = $_REQUEST["id_hotel"];

    }else{ $id_hotel = $_SESSION["id_hotel"]; }

    $sql = sprintf("SELECT id,id_hotel,email FROM hesperia_v2_reservas_paq WHERE status <> 'N' and status_transac = 0 ORDER BY id DESC",

                   mysqli_real_escape_string($mysqli,$id_hotel));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rowsReser = mysqli_fetch_array($result,MYSQLI_ASSOC);

    echo'

<section class="content">

    <div class="row">

        <div class="col-lg-12">

            <div class="box box-info">

                <div class="row">

                    <div class="col-lg-12">

                        <div class="box-header with-border">

                            <h3 class="box-title">

                                Lista de reservaciones

                            </h3>

                        </div>

                        <div class="box-body">
<table class="table" id="table_id">
    <thead>
        <tr>
            <th>#</th>
            <th>Hotel</th>
            <th>Cliente</th>
            <th># de Reserva</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>


                        ';

    $sql = sprintf("SELECT id,id_hotel,email,codigo_reserva

                                                FROM hesperia_v2_reservas_paq

                                                WHERE status <> 'N'

                                                and status_transac = 0");

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        $sqlH = sprintf("SELECT nombre_sitio

                                                FROM hesperia_settings

                                                WHERE id = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["id_hotel"]));

        $resultR = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

        $rowsSet = mysqli_fetch_array($resultR,MYSQLI_ASSOC);

        $sqlU = sprintf("SELECT nombres,apellidos

                                                FROM hesperia_usuario

                                                WHERE email = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["email"]));

        $resultU = QUERYBD($sqlU,$hostname,$user,$password,$db_name);

        $rowsUser = mysqli_fetch_array($resultU,MYSQLI_ASSOC);

        echo'


      <tr>
            <th scope="row">'.$rows["id"].'</th>
            <td>'.$rowsSet["nombre_sitio"].'</td>
            <td>'.$rowsUser["nombres"].' '.$rowsUser["apellidos"].'</td>
            <td>'.$rows["codigo_reserva"].' </td>
            <td><a href="home.php?go=VerDetalleReservacionPaq&cod_reserva='.$rows["id"].'" class="btn btn-xs btn-info">Ver Reserva</a></td>
        </tr>';}                            

    echo'    </tbody>
</table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section><script>

$(document).ready( function () {
    $(\'#table_id\').DataTable( {
     "order": [[ 0, "desc" ]],
     responsive: true
});
} );
</script>';

    return;

}

function LISTA_PRERESERVAS_PAQ($mysqli,$data,$hostname,$user,$password,$db_name){

    if (isset($_REQUEST["id_hotel"])){

        $id_hotel = $_REQUEST["id_hotel"];

    }else{ $id_hotel = $_SESSION["id_hotel"]; }

    $sql = sprintf("SELECT id,id_hotel,email FROM hesperia_v2_reservas_paq WHERE status <> 'N' and status_transac = 3 ORDER BY id DESC",

                   mysqli_real_escape_string($mysqli,$id_hotel));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rowsReser = mysqli_fetch_array($result,MYSQLI_ASSOC);

    echo'

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
<section class="content">

    <div class="row">

        <div class="col-lg-12">

            <div class="box box-info">

                <div class="row">

                    <div class="col-lg-12">

                        <div class="box-header with-border">

                            <h3 class="box-title">

                                Lista de reservaciones

                            </h3>

                        </div>

                        <div class="box-body">';

    $sql = sprintf("SELECT id,id_hotel,email,codigo_reserva

                                                FROM hesperia_v2_reservas_paq

                                                WHERE status <> 'N'

                                                and status_transac = 3

                                                ORDER BY id DESC");

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        $sqlH = sprintf("SELECT nombre_sitio

                                                FROM hesperia_settings

                                                WHERE id = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["id_hotel"]));

        $resultR = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

        $rowsSet = mysqli_fetch_array($resultR,MYSQLI_ASSOC);

        $sqlU = sprintf("SELECT nombres,apellidos

                                                FROM hesperia_usuario

                                                WHERE email = '%s'",

                        mysqli_real_escape_string($mysqli,$rows["email"]));

        $resultU = QUERYBD($sqlU,$hostname,$user,$password,$db_name);

        $rowsUser = mysqli_fetch_array($resultU,MYSQLI_ASSOC);

        echo'

                            <div class="list-group">

                                <span class="list-group-item clearfix">

                            Reserva para el: <strong>'.$rowsSet["nombre_sitio"].'</strong>

                                    por: '.$rowsUser["nombres"].' '.$rowsUser["apellidos"].'

                                  <span class="pull-right" style="margin-left:2px;">

                                    <a href="#" data-value="'.$rows["id"].'" data-tipo="paq" class="btn btn-xs btn-danger delete-prereserva">Eliminar</a>

                                  </span>

                                  <span class="pull-right">

                                    <a href="home.php?go=VerDetalleReservacionPaq&cod_reserva='.$rows["id"].'" class="btn btn-xs btn-info">Ver Reserva</a>

                                  </span>

                                </span>

                            </div>';}

    echo'

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>';

    return;

}



function VER_DETALLE_RESERVACION($mysqli,$data,$hostname,$user,$password,$db_name){

    $codigo_reserva = $_GET["cod_reserva"];

    echo'

<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Detalle de Reserva</h3>
                </div>
                <div class="box-body">';
    $sql = sprintf("SELECT a.*, b.descuento FROM hesperia_v2_reservaciones a
                    LEFT JOIN hesperia_v2_codigos_promocionales b
                        on (a.codigo_promocional = b.codigo) WHERE a.id = '%s' ",
                   mysqli_real_escape_string($mysqli,$codigo_reserva));
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $x=0;

    echo'<div class="row invoice-info">
            <div class="col-sm-12 invoice-col">';
    while($rowsu = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        $cant_h         = $rowsu["cant_hab"];
        $desde          = $rowsu["desde"];
        $hasta          = $rowsu["hasta"];
        $habis          = split('_', $rowsu["habitaciones"]);
        $ninos          = split('_', $rowsu["ninos"]);
        $adultos        = split('_', $rowsu["adultos"]);
        $subs           = split('_', $rowsu["subtotal"]);
        $monto          = $rowsu["total"];
        $voucher        = $rowsu["voucher"];
        $status_transac = $rowsu["status_transac"];
        $total          = $rowsu["total"];
        $totalSinPromo  = $rowsu["total_sin_promo"];
        $id             = $rowsu["id"];
        $comprobante    = $rowsu["nombre_archivo"];

        $sqlU = sprintf("SELECT nombres,apellidos,telefono,email
                        FROM hesperia_usuario
                        WHERE email = '%s'",
                        mysqli_real_escape_string($mysqli,$rowsu["email"]));
        $resultU = QUERYBD($sqlU,$hostname,$user,$password,$db_name);
        $rowsUser = mysqli_fetch_array($resultU,MYSQLI_ASSOC);
            if($x==0){
                echo'
                    <div class="pull-right">';
                        if($status_transac == 0){
                            echo '<b>Reservación: '.$rowsu["codigo_reserva"].'</b>';
                        }else{
                            if($rowsu["tipo_pago"] == 4){
                                echo '<b>Plataforma BitPagos</b>
                                <br>
                                <button onclick="confirmarReservaBitPagos('.$id.','.$monto.')"class="btn btn-danger btn-lg btn-block">Confirmar Reserva</button>';
                            }else{
                                echo '<b>N° Identificación: '.$rowsu["referencia"].'</b>
                                <br>
                                <button id="confirmarReservaAdmin"
                                data-reserva-id="'. $id .'"
                                data-monto="'. $monto .'"" 
                                class="btn btn-danger btn-lg btn-block confrima-reserva">Confirmar Reserva</button>';
                            }
                        }
                        echo'                     
                        <br/>
                    </div>
                    <div>
                        Reservación Realizada a nombre de:
                        <address>
                            <strong>'.$rowsUser["nombres"].' '.$rowsUser["apellidos"].'</strong><br/>
                            Phone: '.$rowsUser["telefono"].'<br/>
                            Email: '.$rowsUser["email"].'
                        </address>
                    </div>
                    <h3 class="page-header">Datos de la Reserva:</h3>';

                $x++;
            }
            for ($i=0; $i < $cant_h; $i++) {
                    $j = $i+1;
                    echo 'Habitación '.$j.': <strong>'.$habis[$i].'</strong><br/>';
                    echo 'Adultos <strong>'.$adultos[$i].'</strong>, Niños <strong>'.$ninos[$i].'</strong><br/>';
                    echo 'Subtotal: <strong>'.$subs[$i].'</strong><br/>';
                }
                echo '<br/>';
                echo '<strong><span style="font-size:14px">Precio total ahora: '.number_format($total,2, '.', ',').'</span></strong><br>';
            if ($rowsu["early_booking"]!=0 && $rowsu["early_booking"]!="" ){
                $porcentaje = floatval($rowsu["early_booking"])*100;
                echo '<strong><span style="font-size:14px">Precio total antes: Bs <strike style="color:red;">'. number_format($totalSinPromo,2, '.', ',') .'</strike></span></strong><br>';
                echo 'Descuento: <strong>'.$porcentaje.'%</strong>';
            }else{
                echo 'Descuento: <strong>No Aplica</strong>';
            }
    }
    echo' </div>
<div class="col-sm-6">
    <h3 class="page-header">Observaciones:</h3>
    <textarea  class="form-control" readonly>'.$rowsu["observaciones"].'</textarea>
    Check In: '.$desde.'<br/>
    Check Out: '.$hasta.'<br/>
</div>
<div class="col-sm-6">';
if($status_transac == 0 && !empty($voucher)){
    echo '<p>Voucher de Pago</p></br>'.$voucher;
} else {
    echo '
    <h3 class="page-header">Comprobante:</h3><a  href="https://hoteleshesperia.com.ve/img/comprobantes/'. $comprobante .'" class="btn btn-info btn-lg btn-block" target="_blank">Bajar Comprobante</a>';
}
echo '
</div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
  $("button.confrima-reserva").click(function() {
    var reservaID       = $(this).attr("data-reserva-id");
    var reservaMonto    = $(this).attr("data-monto");
    // console.log("ID reserva " + reservaID );
    // console.log("Monto reserva  " + reservaMonto );
    confirmaReserva(reservaID, reservaMonto);
    console.log("Entro aqui");
  });
  function confirmaReserva(reservaID, reservaMonto) {

    formData = new FormData();
    formData.append("id", reservaID);       
    
    swal({
        title: "¿Está seguro?", 
        text: "Recuerda que previamente se debe haber comprobado que el dinero de esta reserva está en el banco", 
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Si, confirmar!",
        confirmButtonColor: "#3498db",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        allowEscapeKey: true
    },
    function(){
        setTimeout(function(){
            $.ajax({
                data:formData,
                url: "https://hoteleshesperia.com.ve/panel/include/enviarReservaVal.php",
                type: "POST",
                contentType:false,
                processData:false,
                cache:false,
                async:false
            })
            .done(function(data) {
                swal("Perfecto", "Se ha confirmado la reserva", "success");
                ga(\'send\', \'event\', \'tangible-trf\', \'aprob-reserva-hab\', reservaID, reservaMonto);
            })
            .error(function(data) {
                swal("Oops", "Hay un problema con el servidor", "error");
            });
        }, 2000);
    });

  }
 </script> 
';
    return;
}



function VER_DETALLE_RESERVACION_PAQ($mysqli,$data,$hostname,$user,$password,$db_name){
    $codigo_reserva = $_GET["cod_reserva"];
    echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar las habitaciones</h3>
                </div>
                <div class="box-body">';
    $sql = sprintf("SELECT * FROM hesperia_v2_reservas_paq
                    WHERE id = '%s'",
                   mysqli_real_escape_string($mysqli,$codigo_reserva));
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $x=0;
    echo'<div class="row invoice-info">
            <div class="col-sm-12 invoice-col">';
    while($rowsu = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        $monto              = $rowsu["total"];
        $voucher            = $rowsu["voucher"];
        $nombrePaq          = $rowsu["nombre_paq"];
        $fecha_reserva      = $rowsu["fecha_reserva"];
        $observaciones      = $rowsu["observaciones"];
        $status_transac     = $rowsu["status_transac"];
        $id                 = $rowsu["id"];
        $nombreArchivo      = $rowsu["nombre_archivo"];

        $sqlU = sprintf("SELECT nombres,apellidos,telefono,email
                        FROM hesperia_usuario
                        WHERE email = '%s'",
                        mysqli_real_escape_string($mysqli,$rowsu["email"]));
        $resultU = QUERYBD($sqlU,$hostname,$user,$password,$db_name);

        $rowsUser = mysqli_fetch_array($resultU,MYSQLI_ASSOC);
            if($x==0){
                echo'
                    <div class="pull-right">';
                        if($status_transac == 0){
                            echo '<b>Reservación: '.$rowsu["codigo_reserva"].'</b>';
                        }else{
                            if($rowsu["tipo_pago"] == 4){
                                echo '<b>Plataforma BitPagos</b>
                                <br>
                                <button onclick="confirmarReservaPaqBitPagos('.$id.','.$monto.')" class="btn btn-danger btn-lg btn-block">Confirmar Reserva</button>';
                            }else{
                                echo '<b>N° Identificación: '.$rowsu["referencia"].'</b>
                                <br>
                                <button id="confirmarReservaAdmin"
                                data-reserva-id="'. $id .'"
                                data-monto="'. $monto .'"" 
                                class="btn btn-danger btn-lg btn-block confrima-reserva">Confirmar Reserva</button>
                                <a href="https://hoteleshesperia.com.ve/img/comprobantes/'. $nombreArchivo .'" class="btn btn-info btn-lg btn-block" target="_blank">Bajar Comprobante</a>';
                            }
                        }
                        echo'
                        <br/>
                    </div>
                    <div>
                        Reservación Realizada a nombre de:

                        <address>

                            <strong>'.$rowsUser["nombres"].' '.$rowsUser["apellidos"].'</strong><br/>

                            Phone: '.$rowsUser["telefono"].'<br/>

                            Email: '.$rowsUser["email"].'

                        </address>

                    </div>

                    <h3 class="page-header">Datos de la Reserva:</h3>

                    <strong>Paquete o Evento</strong>: '.$nombrePaq.'<br/>

                    <strong>Fecha</strong>: '.$fecha_reserva.'<br/>
                    <strong>Total</strong>: '. number_format($rowsu["total"],2, '.', ',');
                    

                $x++;
            }



    }

    echo'

            </div>

<div class="col-sm-12">

    <h3 class="page-header">Observaciones:</h3>

    <textarea  class="form-control" readonly>'.$observaciones.'</textarea>

</div>

<div class="col-sm-12">';

if($status_transac == 0){

// echo'

//     Voucher de Pago

//     '.$voucher;

}

echo '

</div>

                </div>

            </div>

        </div>

    </div>

</section>
<script>
  $("button.confrima-reserva").click(function() {
    var reservaID       = $(this).attr("data-reserva-id");
    var reservaMonto    = $(this).attr("data-monto");
    // console.log("ID reserva " + reservaID );
    // console.log("Monto reserva  " + reservaMonto );
    confirmaReserva(reservaID, reservaMonto);
  });

  function confirmaReserva(reservaID, reservaMonto) {

    formData = new FormData();
    formData.append("id", reservaID);       
    
    swal({
        title: "¿Está seguro?", 
        text: "Recuerda que previamente se debe haber comprobado que el dinero de esta reserva está en el banco", 
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Si, confirmar!",
        confirmButtonColor: "#3498db",
        cancelButtonText: "Cancelar",
        showLoaderOnConfirm: true,
        allowEscapeKey: true
    },
    function(){
        setTimeout(function(){
            $.ajax({
                data:formData,
                url: "https://hoteleshesperia.com.ve/panel/include/enviarReservaPaqVal.php",
                type: "POST",
                contentType:false,
                processData:false,
                cache:false,
                async:false
            })
            .done(function(data) {
                swal("Perfecto", "Se ha confirmado la reserva", "success");
                ga(\'send\', \'event\', \'tangible-trf\', \'aprob-reserva-hab\', reservaID, reservaMonto);
            })
            .error(function(data) {
                swal("Oops", "Hay un problema con el servidor", "error");
            });
        }, 2000);
    });

  }
 </script>  
';

    return;

}

?>

