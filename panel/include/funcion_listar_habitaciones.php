<?php
function LISTAR_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name){
            $sql = sprintf("SELECT * FROM hesperia_habitaciones ORDER BY  id_habitacion DESC");
            $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de habitaciones</h3>
                        </div>
                        <div class="box-body">';
while ($rowHab=mysqli_fetch_array($result,MYSQLI_ASSOC)){
echo '                      <div class="list-group">
                                <span class="list-group-item clearfix">
                                  '.$rowHab["nombre_habitacion"].'
                                    <span class="pull-right">
                                        <a href="home.php?go=EditarHabitacion&id='.$rowHab["id_habitacion"].'" class="btn btn-xs btn-info" title="Editar">
                                            <i class="fa fa-pencil"></i>
                                            Editar
                                        </a>
                                    </span>
                                </span>
                            </div>';
}echo '
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
?>
