<?php
function ELECCION_CAMBIO_PRECIO($mysqli,$data,$hostname,$user,$password,$db_name)
{ echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Cambiar Precio de Habitación</h3>
                        </div>
                        <div class="box-body">';
 if (isset($_GET["op"])) {
     if ($_GET["op"] == 1) {
         echo '
<form enctype="application/x-www-form-urlencoded" action="home.php?go=CambioPrecio" role="form" method="post"  name="EleccionHotel" id="EleccionHotel">';
     }else{
         echo '<form enctype="application/x-www-form-urlencoded" action="home.php?go=CambioPrecioFecha" role="form" method="post"  name="EleccionHotel" id="EleccionHotel">';
     }
 }
 echo '
                                    <div class="form-group">
                                    <label for="tipo_habitacion">Seleccione el Hotel que será editado precio</label>
                                        <select class="form-control" name="id_hotel" id="id_hotel" onchange="this.form.submit()">
                                          <option value="0" selected="selected">Seleccione</option>';
 $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings
                                                            ORDER BY razon_social ASC");
 $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
 while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
     echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
 }
 echo'
                                            </select>
                                    </div>
                                </form>';
 echo '
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>';
 return;
}

function CAMBIO_PRECIO_HAB($mysqli,$data,$hostname,$user,$password,$db_name)
{ echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Cambiar Precio de Habitación</h3>
                        </div>
                        <div class="box-body">';

 echo '<form enctype="application/x-www-form-urlencoded" action="home.php?go=CambioPrecio" role="form" method="post"  name="SelHab" id="SelHab">
                                <div class="row">
                                    <div class="col-md-12">';
 if (!empty($_POST["id_hotel"])){
     $id_hotel = $_POST["id_hotel"];
     echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
 }else{
     $id_hotel = $_SESSION["id_hotel"];
     echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
 }
 $sql = sprintf("SELECT id_habitacion,id_hotel,nombre_habitacion
                FROM hesperia_habitaciones WHERE id_hotel = '$id_hotel'
                ORDER BY nombre_habitacion ASC");
 echo '
                                    <div class="form-group">
                                    <label for="tipo_habitacion">Seleccione Habitación</label>
                                        <select class="form-control" name="id_hab" id="id_hab">
                                          <option value="0" selected="selected">Seleccione</option>';
 $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
 while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
     echo '<option value="'.$rows["id_habitacion"].'">'.$rows["nombre_habitacion"].'</option>';
 }
 echo'
                                            </select>
                                    </div>';
 echo '
        <div class="box-footer">
            <div class="col-lg-6 pull-right">
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Elegir Habitación
                </button>
            </div>
        </div>
                                    </div>
                                </div>';
 echo '</form>';
 if (!empty($_POST["id_hab"])){
    $id_hab = trim($_POST["id_hab"]);
    $sql = sprintf("SELECT id_habitacion,precio,precio_promocion
                    FROM hesperia_habitaciones WHERE id_hotel = '$id_hotel' &&
                    id_habitacion = '$id_hab' ORDER BY nombre_habitacion ASC");
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
     echo '<hr/><form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post"  name="CamHab" id="CamHab" onsubmit="return PrecioHab(); return document.MM_returnValue">
                                <div class="row">
                                    <div class="col-md-6">
                                          <div id="CambioPrecioHab"></div>
                                            <input type="hidden" name="id_hab" id="id_hab" value="'.$id_hab.'"/>
                                         <div class="form-group">
                                              <label for="precio">Precio Bs:</label>
                                            <input type="number" class="form-control" name="precio" id="precio" placeholder="Precio" value="'.$rows["precio"].'"/>
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="form-group">
                                              <label for="precio">Precio Promoción Bs:</label>
                                            <input type="number" class="form-control" name="precio_promocion" id="precio_promocion" placeholder="Precio Promocion" value="'.$rows["precio_promocion"].'">
                                          </div>
                                    </div>
                                    </div>
        <div class="box-footer">
            <div class="col-lg-6 pull-right">
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Cambiar Precio
                </button>
            </div>
        </div>
                                    </form>';
 }


 echo '
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>';
 return;
}

function CAMBIO_PRECIO_FECHA_HAB($mysqli,$data,$hostname,$user,$password,$db_name)
{  echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Cambiar Precio de Habitación para una Fecha</h3>
                        </div>
                        <div class="box-body">';
echo '<form enctype="application/x-www-form-urlencoded" action="home.php?go=CambioPrecioFecha" role="form" method="post"  name="SelHab" id="SelHab">';
 if (!empty($_POST["id_hotel"])){
     $id_hotel = $_POST["id_hotel"];
     echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
 }else{
     $id_hotel = $_SESSION["id_hotel"];
     echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
 }
 $sql = sprintf("SELECT id_habitacion,id_hotel,nombre_habitacion FROM hesperia_habitaciones WHERE id_hotel = '$id_hotel'
                ORDER BY nombre_habitacion ASC");
 echo '
        <div class="form-group">
            <label for="tipo_habitacion">Seleccione Habitación</label>
                <select class="form-control" name="id_hab" id="id_hab">
                <option value="0" selected="selected">Seleccione</option>';
 $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
 while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
     if (!empty($_POST["id_hab"])){
         if ($_POST["id_hab"] == $rows["id_habitacion"]) {
             echo '<option value="'.$rows["id_habitacion"].'" selected>'.$rows["nombre_habitacion"].'</option>'; }
     }
     echo '<option value="'.$rows["id_habitacion"].'" class="form-control">'.$rows["nombre_habitacion"].'</option>';
 }
 echo'
                </select>
        </div>';
 echo '
        <div class="box-footer">
            <div class="col-lg-6 pull-right">
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Elegir Habitación
                </button>
            </div>
        </div>
        ';
 echo'</form>';
 if (!empty($_POST["id_hab"])){
     $id_hab = trim($_POST["id_hab"]);
     $sql = sprintf("SELECT id_habitacion,precio,precio_promocion
                                                            FROM hesperia_habitaciones WHERE id_hotel = '$id_hotel' &&
                                                            id_habitacion = '$id_hab'
                                                            ORDER BY nombre_habitacion ASC");
     $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
     $rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC);
     echo '
    <hr/>
    <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post"  name="CamHabPost" id="CamHabPost" onsubmit="return PrecioHabPost(); return document.MM_returnValue">
            <input type="hidden" name="id_hab" id="id_hab" value="'.$id_hab.'"/>
            <input type="hidden" name="id_hotel" id="id_hotel" value="'.$id_hotel.'"/>
<div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="precio">Precio Bs:</label><br>
                    <input type="number" class="form-control" name="precio" id="precio" placeholder="Precio" value="'.$rows["precio"].'"/>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="precio">Precio Promoción Bs:</label><br>
                    <input type="number" class="form-control" name="precio_promocion" id="precio_promocion" placeholder="Precio Promocion" value="'.$rows["precio_promocion"].'">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="precio">Fecha para el nuevo precio:</label><br>
                    <input type="text" name="fecha_precio" class="form-control" data-date-format="dd/mm/yyyy" id="dp1" placeholder="" >
                </div>
            </div>
</div>
        <div class="box-footer">
            <div class="col-lg-8">
                <p>Recuerde que los precios deben ser numeros enteros, sin puntos o comas.</p>
            </div>
            <div class="col-lg-4">
                <button type="submit" class="btn btn-primary btn-sm btn-block">
                    Agregar Precio para fecha estipulada
                </button>
            </div>
        </div>
            <div id="CambioPrecioHabPost"></div>
        </form>';
 }
 echo '

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
 return;
}
?>
