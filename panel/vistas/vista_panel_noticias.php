<?php 

include_once("include/funciones_mediacenter_panel.php");

$resultadoNoticias = cargarNoticias($mysqli);
$resultadoCategorias = cargarTodoCategorias($mysqli);
$resultadoFotos = cargarTodoFotosNoticias($mysqli);
$array_noticias = array();
 ?>
<div class="container">

    <div class="btn-group btn-group-justified" role="group" aria-label="...">
	  <div class="btn-group" role="group">
	    <a href='#noticias-content' class="btn btn-default" role='tab' data-toggle='tab'>Noticias</a>
	  </div>
	  <div class="btn-group" role="group">
	    <a href='#categorias-content' class="btn btn-default" role='tab' data-toggle='tab'>Categorias</a>
	  </div>
	  <div class="btn-group" role="group">
	    <a href='#galeria-content' class="btn btn-default" role='tab' data-toggle='tab'>Galeria de Noticias</a>
	  </div>
	</div>

    <div class="tab-content">

    	<div class='col-md-12 tab-pane active' role="tabpanel" id="noticias-content">
            <h2>Noticias</h2>
            <button type="button" class="btn btn-primary" data-toggle="modal" id="nueva-noticia-btn">Nueva</button>
           	<table class="table table-striped table-bordered table-hover table-reg" id="dataTables-noticias">
	                <thead>
	                    <tr>
	                        <th>Titulo</th>
	                        <th>Subtitulo</th>
	                        <th>Fecha</th>
	                        <th>Categorias</th>
	                        <th style='text-align:center'>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody id="bodyTable">
	                        <?php
	                        $index_noticia=0;
	                        while ($reg = mysqli_fetch_assoc($resultadoNoticias)) {

			    				$json = json_encode($reg, JSON_HEX_AMP);
			    				$array_noticias[$index_noticia]=$reg;
			    				$categorias="";
			    				$resultCategorias = cargarNoticiaCategoria($mysqli, $reg["id_noticia"]);

			    				while ($regCategorias =mysqli_fetch_assoc($resultCategorias)) {
			    					$categorias.=" $regCategorias[nombre_categoria]
			    					(<a href='#'>Eliminar</a>)<br>";
			    				}
			    				//NO BORRAR
			    				/*
			    				<a href='#' class='lista-categoria-noticia' data-value='$json'>
			                        		   Añadir Categoria</a>
			    				*/
			    				echo("<tr>
			    						<td>$reg[titulo]</td>
			                        	<td>$reg[subtitulo]</td>
			                        	<td>$reg[fecha]</td>
			                        	<td>".$categorias."</td>
			                        	<td style='text-align:center'>
			                        		<a href='#' class='modificar-noticia'
			                        		data-value='$json' >
			                        			Modificar
			                        		</a>
			                        		|
			                        		<a href='#' class='eliminar-noticia'
			                        		data-toggle='modal' data-value='$json'>
			                        			Eliminar
			                        		</a>
			                        	</td>
			                        </tr>");
			    				$index_noticia++;
		                    }
	                       
	                         ?>
	                </tbody>
	        </table>
        </div>

    	<div class='col-md-12 tab-pane ' role="tabpanel" id="categorias-content">
        	<h2>Categorias</h2>
        	<table class="table table-striped table-bordered table-hover table-reg" id="dataTables-promos">
	                <thead>
	                    <tr>
	                        <th>id</th>
	                        <th>categoria</th>
	                        <th style='text-align:center'>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody id="bodyTable">
	                        <?php
	                        while ($reg2 = mysqli_fetch_assoc($resultadoCategorias)) {
			    				$json = json_encode($reg2);
			    				echo("<tr>
			    						<td>$reg2[id_categoria]</td>
			                        	<td>$reg2[nombre_categoria]</td>
			                        	<td style='text-align:center'>
			                        		<a href='#'>Modificar</a> | 
			                        		<a href='#' class='eliminar-noticia'
			                        		data-toggle='modal' data-value='$json'>
			                        			Eliminar
			                        		</a>
			                        	</td>
			                        </tr>");
		                    }
	                       
	                         ?>
	                </tbody>
	        </table>
        </div>

        <div class='col-md-12 tab-pane' role="tabpanel" id="galeria-content">
            <h2>Galeria</h2>
            
            <form id="form-foto-noticia">
            	<div class="col-md-4">
            		<label class="control-label">Noticia:</label>
	            	<select id="select-noticia-img" name="id_noticia" class="form-control">
	                <?php 
	                for ($i=0; $i <count($array_noticias) ; $i++) { 
	                    echo("<option  value='".$array_noticias[$i]["id_noticia"]."'>
	                         ".$array_noticias[$i]["titulo"]."
	                          </option>");
	                    }
	                ?>
	                </select>          
	            </div>
	            <div class="col-md-4">
		            <div class="form-group">
						<label class="control-label">Foto:</label>
						<input type="file" name="foto" id="foto" required>
					</div>
				</div>

				<div class="col-md-12">
					<p class="text-center">
						<button type="button" class="btn btn-primary" data-toggle="modal" id="nueva-img-noticia-btn">Añadir Foto</button>
					</p>
	            	
	            </div>
            </form>                  
        	<br>
        	<br>	
        	<div class='col-md-12'>
        		<!--<label class="control-label">Hotel:</label>

					<select class="form-control" name="idhotel" id="idhotel_fotos" required>-->
					<!--</select>-->
        	</div>
            <br><br>
	            <table class="table table-striped table-bordered table-hover table-reg" id="dataTables-imagenes-noticias">
	                <thead>
	                    <tr>
	                        <th>Foto</th>
	                        <th>Noticia</th>
	                        <th>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody id="bodyTable">
	                        <?php 
	                        $i=0;
	                       while ($reg4 = mysqli_fetch_assoc($resultadoFotos)) {
	                        	$json = json_encode($reg4);
	                        	//$array_fotos_promos[$i] = $reg3;
	                        	echo("<tr>
	                        			<td>
	                        			<img src='https://hoteleshesperia.com.ve/img/media_center/$reg4[nombre_imagen]'
	                        				class='imagen-table img-responsive'>
	                        			</td>
	                        			<td>$reg4[titulo]</td>
	                        			<td>
	                        			 	<a href='#' class='eliminar-imagen-noticia'
	                        				data-toggle='modal' data-value='$json'>
	                        			 	Eliminar
	                        			 	</a>
	                        			 </td>
	                        		</tr>");
	                        	$i++;
	                        }

	                         ?>
	                </tbody>
	            </table>
    	
        </div>

        
    </div>

	<div class="modal fade" id="delete-modal-item" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Eliminar elemento</h4>
	      </div>
	      <div class="modal-body">
		    	<p>¿Estás seguro que deseas eliminar este elemento?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" data-tipo=""
	         id="delete-item-btn" data-accion="add">
	        	Aceptar
	        </button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<div class="modal fade" id="form-modal-noticia" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="titulo-form-noticia">Nueva Noticia</h4>
	      </div>
	      <div class="modal-body">
		    	<form id="add-mod-noticia-form">
		        	<div class="col-md-12">
			        	<div class="form-group col-md-6">
			        		<label for="recipient-name" class="control-label">Titulo:</label>
			        		<input type="text" class="form-control" name="titulo" id="titulo" required>
			        	</div>
			        	<div class="form-group col-md-6">
			        		<label for="message-text" class="control-label">Subtitulo:</label>
			        		<input  type="text" class="form-control" id="subtitulo" name="subtitulo" required>
			        	</div>
		        	</div>
		        	<div class="col-md-12">
			        	<div class="col-md-6 form-group">
				        	<div class="checkbox">
							  <label>
							    <input name="activo" id="activo" type="checkbox" value="1">
							    Activo
							  </label>
							</div>
						</div>

						<div class="col-md-6 form-group">
							<label class="control-label">Banner:</label>
							<input type="file" name="banner" id="banner">
						</div>
					</div>
					<div class="col-md-12">
						<textarea 
						 class="summernote" id="summernote1" name="contenido"></textarea>
						 <br>
					</div>
					
					<!--<div class="col-md-12">
                        <div class="alert alert-warning">
                            Recuerde que solo se admiten archivos con extensión: <a href="#" class="alert-link">PDF</a>.
                        </div>
                  	</div>-->
					<input type="hidden" name="id" id="id_noticia" value="-1">
					<input type="hidden" name="anterior" id="anterior" value="">
		        </form>
	      </div>

	      <div class="modal-footer">

	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" id="add-mod-noticia-btn" data-accion="add">
	        	Guardar
	        </button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>

