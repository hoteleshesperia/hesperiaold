<?php 

include_once("include/funciones_mediacenter_panel.php");
$resultado = cargarListaPdfs($mysqli);
$resultadoOptions = cargarHoteles($mysqli);
$resultadoImg= cargarFotosMediaCenter($mysqli);
$resultadoVid= cargarVideosMediaCenter($mysqli);

$array_select_hoteles = array();
$array_fotos_promos= array();


 ?>
<div class="container">

    <div class="btn-group btn-group-justified" role="group" aria-label="...">
	  <div class="btn-group" role="group">
	    <a href='#brochures-content' class="btn btn-default" role='tab' data-toggle='tab'>Brochures/Manuales</a>
	  </div>
	  <div class="btn-group" role="group">
	    <a href='#fotos-content' class="btn btn-default" role='tab' data-toggle='tab'>Fotos</a>
	  </div>
	  <div class="btn-group" role="group">
	    <a href='#videos-content' class="btn btn-default" role='tab' data-toggle='tab'>Videos</a>
	  </div>
	  <div class="btn-group" role="group">
	    <a href='#promos-content' class="btn btn-default" role='tab' data-toggle='tab'>Promos</a>
	  </div>
	</div>

    <div class="tab-content">
    	<div class='col-md-12 tab-pane active' role="tabpanel" id="brochures-content">
        	<h2>Brochures/Manuales</h2>
        	<button type="button" class="btn btn-primary" data-toggle="modal" id="add-brochure-btn">Nuevo</button>
        	<table class="table table-striped table-bordered table-hover table-reg" id="dataTables-brochures">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Descripcion</th>
                        <th>Hotel</th>
                        <th>¿Activo?</th>
                        <th>Tipo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="bodyTable">
                        <?php
                        $i=0;
                        while ($reg = mysqli_fetch_assoc($resultado)) {
                        	$json = json_encode($reg);
                        	$tipo="";
                        	if ($reg["tipo"]=="1") {
                        		$tipo="General";
                        	}else{
                        		$tipo="Marca";
                        	}
                        	echo("<tr>
                        			<td>$reg[titulo]</td>
                        			<td>$reg[descripcion]</td>
                        			<td>$reg[razon_social]</td>
                        			<td>$reg[ind_activo]</td>
                        			<td>$tipo</td>
                        			<td>
                        				<a href='#' class='editar-pdf'
                        				data-toggle='modal' data-value='$json'>
                        			 	Editar
                        			 	</a>
                        			 	|
                        			 	<a href='#' class='eliminar-pdf'
                        				data-toggle='modal' data-value='$json'>
                        			 	Eliminar
                        			 	</a>
                        			 </td>
                        		</tr>");
                        	$i++;
                        }

                         ?>
                </tbody>
            </table>
        </div>

        <div class='col-md-12 tab-pane' role="tabpanel" id="fotos-content">
            <h2>Fotos</h2>
            <button type="button" class="btn btn-primary" data-toggle="modal" id="nueva-img-btn">Nuevo</button>
        	<br>
        	<br>	
        	<div class='col-md-12'>
        		<!--<label class="control-label">Hotel:</label>

					<select class="form-control" name="idhotel" id="idhotel_fotos" required>-->
					<!--</select>-->

        	</div>
            <br><br>
	            <table class="table table-striped table-bordered table-hover table-reg" id="dataTables-imagenes">
	                <thead>
	                    <tr>
	                        <th>Foto</th>
	                        <th>Hotel</th>
	                        <th>Tipo</th>
	                        <th>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody id="bodyTable">
	                        <?php 
	                        $i=0;
	                       while ($reg3 = mysqli_fetch_assoc($resultadoImg)) {
	                        	$json = json_encode($reg3);
	                        	$array_fotos_promos[$i] = $reg3;
	                        	$tipo="";
	                        	if ($reg3["ind_marca"]=="S") {
	                        		$tipo="Marca";
	                        	}else{
	                        		$tipo="General";
	                        	}
	                        	if ($reg3["ind_mediacenter"]=="S" && $reg3["ind_promo"]=="N") {
	                        		echo("<tr>
	                        			<td>
	                        			<img src='https://hoteleshesperia.com.ve/img/media_center/$reg3[nombre_imagen]'
	                        				class='imagen-table img-responsive'>
	                        			</td>
	                        			<td>$reg3[razon_social]</td>
	                        			<td>$tipo</td>
	                        			<td>
	                        				<a href='#' class='ver-imagen'
	                        				data-toggle='modal' data-value='$json'>
	                        			 	Ver
	                        			 	</a>
	                        			 	|
	                        			 	<a href='#' class='eliminar-imagen'
	                        				data-toggle='modal' data-value='$json'>
	                        			 	Eliminar
	                        			 	</a>
	                        			 </td>
	                        		</tr>");
	                        	}
	                        	$i++;
	                        }

	                         ?>
	                </tbody>
	            </table>  	
        </div>

        <div class='col-md-12 tab-pane' role="tabpanel" id="videos-content">
            <h2>Videos</h2>
           	<button type="button" class="btn btn-primary" data-toggle="modal" id="nuevo-video-btn">Nuevo</button>
        	<br>
        	<br>
	        <table class="table table-striped table-bordered table-hover table-reg" id="dataTables-videos">
		    	<thead>
		    		<tr>
		    			<th>Video</th>
		    			<th>Hotel</th>
		    			<th>Acciones</th>
		    		</tr>
		    	</thead>
		    	<tbody id="bodyTable">
		    		<?php 
		    			while ($reg4 = mysqli_fetch_assoc($resultadoVid)) {
		    				$json = json_encode($reg4);
		    				echo("<tr>
		    						<td>
		                        		<div class='embed-responsive embed-responsive-16by9'>
		                        			<iframe class='embed-responsive-item'
		                        				src='//www.youtube.com/embed/$reg4[video_promocion]'>
		                        			</iframe>
		                        		</div>

		                        	</td>
		                        	<td>$reg4[razon_social]</td>
		                        	<td>
		                        		<a href='#' class='eliminar-video'
		                        		data-toggle='modal' data-value='$json'>
		                        			Eliminar
		                        		</a>
		                        	</td>
		                        </tr>");
		                    }

		                ?>
		        </tbody>
		    </table>

	    </div>
        

        <div class='col-md-12 tab-pane' role="tabpanel" id="promos-content">
            <h2>Promos</h2>
            <button type="button" class="btn btn-primary" data-toggle="modal" id="nueva-promo-btn">Nuevo</button>
            

            <table class="table table-striped table-bordered table-hover table-reg" id="dataTables-promos">
	                <thead>
	                    <tr>
	                        <th>Foto</th>
	                        <th>Hotel</th>
	                        <th>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody id="bodyTable">
	                        <?php
	                        for ($i=0; $i <count($array_fotos_promos) ; $i++) { 
	                         	if ($array_fotos_promos[$i]["ind_promo"]=="S") {
	                         		$json = json_encode($array_fotos_promos[$i]);
	                         		echo("<tr>
	                        			<td>
	                        			<img src='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."'
	                        				class='imagen-table img-responsive'>
	                        			</td>
	                        			<td>".$array_fotos_promos[$i]["razon_social"]."</td>
	                        			<td>
	                        				<a href='#' class='ver-imagen'
	                        				data-toggle='modal' data-value='$json'>
	                        			 	Ver
	                        			 	</a>
	                        			 	|
	                        			 	<a href='#' class='eliminar-imagen'
	                        				data-toggle='modal' data-value='$json'>
	                        			 	Eliminar
	                        			 	</a>
	                        			 </td>
	                        		</tr>");
	                         	}
	                         } 
	                       
	                         ?>
	                </tbody>
	            </table>	 
        </div>

        
    </div>

    <div class="modal fade" id="form-modal-brochure" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="titulo-form-brochure">Nuevo Brochure</h4>
	      </div>
	      <div class="modal-body">
		    	<form id="add-mod-brochure-form">
		        	<div class="form-group">
		        		<label for="recipient-name" class="control-label">Titulo:</label>
		        		<input type="text" class="form-control" name="titulo" id="titulo" required>
		        	</div>
		        	<div class="form-group">
		        		<label for="message-text" class="control-label">Descripción:</label>
		        		<textarea class="form-control" id="descripcion" name="descripcion" required></textarea>
		        	</div>
		        	<div class="form-group">
			        	<div class="checkbox">
						  <label>
						    <input name="activo" id="activo" type="checkbox" value="1">
						    Activo
						  </label>
						</div>
					</div>
					<div class="form-group">
						<label  class="control-label">Tipo:</label>
						<select class="form-control" name='tipo_pdf' id='tipo_pdf'>
							<option value='1'>General</option>
							<option value='2'>Marca</option>
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">Hotel:</label>
						<select class="form-control" name="idhotel" id="idhotel" required>
						  <?php 
						  	$i=0;
	                        while ($reg2 = mysqli_fetch_assoc($resultadoOptions)) {
	                        	$array_select_hoteles[$i]=$reg2;
	                        	$json = json_encode($reg2);
	                        	echo("<option  value='$reg2[id]'>
	                        			$reg2[razon_social]
	                        		 </option>");
	                        	$i++;
	                        }

	                         ?>
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">Archivo:</label>
						<input type="file" name="archivo-brochure" id="archivo-brochure">
					</div>
					<div class="col-md-12">
                        <div class="alert alert-warning">
                            Recuerde que solo se admiten archivos con extensión: <a href="#" class="alert-link">PDF</a>.
                        </div>
                  	</div>
					<input type="hidden" name="id" id="id_brochure" value="-1">
					<input type="hidden" name="anterior" id="anterior" value="">
		        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" id="add-mod-brochure-btn" data-accion="add">
	        	Guardar
	        </button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<div class="modal fade" id="form-modal-imagen" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="modal-imagen-titulo">Subir Imagen</h4>
	      </div>
	      <div class="modal-body">
	      	<form id="add-imagen-form">
	      		<div class="form-group">
					<label class="control-label">Imagen:</label>
					<input type="file" name="imagen-mediacenter" id="imagen-mediacenter">
				</div>

				<div class="form-group" id="tipo-img-content">
					<label  class="control-label">Tipo:</label>
						<select class="form-control" name='tipo_foto'>
							<option value='N'>General</option>
							<option value='S'>Marca</option>
						</select>
				</div>

				<div class="form-group">
					<label class="control-label">Hotel:</label>
					<select class="form-control" name="id_hotel" id="id_hotel_fotos" required>
					<?php 

					for ($i=0; $i <count($array_select_hoteles) ; $i++) { 
						echo("<option  value='".$array_select_hoteles[$i]["id"]."'>
		                        ".$array_select_hoteles[$i]["razon_social"]."
		                      </option>");
		                     
					}
						
					 ?>
					</select>
				 </div>
		    	<div class="col-md-12">
                	<div class="alert alert-warning">
                    	Recuerde que solo se admiten archivos con extensión: <a href="#" class="alert-link">PNG o JPG</a>.
                    </div>
                </div>
                <input type="hidden" value="" name="anterior" id="anterior">
		    </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" id="add-imagen-btn" data-accion="add">
	        	Aceptar
	        </button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="form-modal-video" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Subir Video</h4>
	      </div>
	      <div class="modal-body">
	      	<form id="add-video-form">
	      		<div class="form-group">
					<label class="control-label">Codigo del video:</label>
					<input type="text" class="form-control" name="url-video" id="url-video" required>
					<span id="helpBlock" class="help-block">Solo debe escribir el codigo del video ej:
						https://www.youtube.com/watch?v=<b>Pgum6OT_VH8</b> (lo que esta en negrita)
					</span>
				</div>

				<div class="form-group">
			        	<div class="checkbox">
						  <label>
						    <input name="activo-video" id="activo-video" type="checkbox" value="1">
						    Activo
						  </label>
						</div>
				</div>
				<div class="form-group">
					<label class="control-label">Hotel:</label>
					<select class="form-control" name="id_hotel" id="id_hotel_video" required>
					<?php 

					for ($i=0; $i <count($array_select_hoteles) ; $i++) { 
						echo("<option  value='".$array_select_hoteles[$i]["id"]."'>
		                        ".$array_select_hoteles[$i]["razon_social"]."
		                      </option>");
		                     
					}
						
					 ?>
					</select>
				 </div>
		    </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" id="add-video-btn" data-accion="add">
	        	Aceptar
	        </button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="delete-modal-item" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Eliminar elemento</h4>
	      </div>
	      <div class="modal-body">
		    	<p>¿Estás seguro que deseas eliminar este elemento?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary" data-tipo=""
	         id="delete-item-btn" data-accion="add">
	        	Aceptar
	        </button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>

