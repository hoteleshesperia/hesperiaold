<?php
include_once ("panel/include/funciones_mediacenter_panel.php");
     //   
$result_noticia = cargarNoticiaIndividual($mysqli, $_GET["id"]);
$result_fotos_noticia = cargarFotosNoticiaIndividual($mysqli, $_GET["id"]);
?>

<?php if ($reg = mysqli_fetch_assoc($result_noticia)){
$phpdate = strtotime( $reg["fecha"] );
$mysqldate = date( 'Y-m-d H:i:s', $phpdate)
 ?>
<div class='container'>
    <div class='row'>
        <div class='col-md-12'>
            <h2 class='page-header'>Noticias <small>Hesperia Hotels & Resorts</small></h2>
        </div>
    </div>
    <div class="row">
        <h2><?php echo($reg["titulo"]) ?></h2>
        <p><i class='fa fa-clock-o'></i> Fecha: <?php echo($mysqldate) ?></p>

        
        <img class='img-responsive col-md-12' style='padding-bottom:2%;' src=<?php echo("'https://hoteleshesperia.com.ve/img/media_center/$reg[imagen]'") ?>
        alt='$reg[titulo]'>
          
         <hr>
         <p class='lead'><?php echo($reg["subtitulo"]) ?></p>
       
        <!--<p class="text-justify">-->
           <?php echo($reg["contenido"]); ?>
       <!-- </p>-->

        <hr>
        <h3 class='text-primary'>Galeria</h3>
        <div class="col-md-12">
            <?php 
             while ($reg2 = mysqli_fetch_assoc($result_fotos_noticia)) {
                $json = json_encode($reg2);
                    echo("
                    <div class='col-md-3'>
                        <div class='thumbnail'>
                            <a href='#' class='show-modal' data-value='https://hoteleshesperia.com.ve/img/media_center/$reg2[nombre_imagen]'>
                            <img src='https://hoteleshesperia.com.ve/img/media_center/$reg2[nombre_imagen]'>
                            </a>
                        </div>
                    </div>
                    ");
            
                }
             ?>
        </div>
        <hr>
        <div class='col-md-12'>
            <h3 class='text-primary text-center'>Comparte</h3>
            <div class="text-center" id="share"></div>
        </div>
    </div>
    <div class='col-md-12'>
        <p class='text-center'>
            <a href="https://hoteleshesperia.com.ve/mediacenter" class="btn btn-primary">Volver al Mediacenter</a>
        </p>
    </div>
       <!-- <h3 class='text-primary'>Compartir</h3>-->
    </div>
</div>

<?php 
    }else{
        echo "<h2>La noticia ya no esta disponible</h2>";
    }

 ?>