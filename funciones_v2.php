<?php 

function banner_v2($mysqli, $sql,$hostname,$user,$password,$db_name,$destino){

    $sql = "SELECT * from hesperia_v2_banner where ind_activo ='S' and destino='".$destino."' order by posicion asc";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $numRows= mysqli_num_rows($result);

    if ($numRows!=null && $numRows>0) {
        echo " 
          <header>
            <div id='myCarousel' class='carousel slide' data-interval='4000' data-ride='carousel'>
                <ol class='carousel-indicators hidden-xs hidden-sm'>
        ";
        
        for ($i=0; $i <$numRows; $i++) { 
            
            $active="";
           
            if($i==0)
                $active="class='active'";

            echo "
                <li data-target='#myCarousel' data-slide-to='$i' $active> </li>
            ";
        }
        echo "
           </ol>
           <div class='carousel-inner'>
          
        ";

        //////////RECORRER LOS BANNERS///////////
        $i=0;
       while ($reg = mysqli_fetch_assoc($result)){

            foreach ($reg as $key => $value) {

                $reg[$key] = utf8_encode($reg[$key]);
            }

            if ($reg["url"]!=null && $reg["url"]!="") {
               $urlBanner = "<a class='btn btn-primary btn-small' href='https://hoteleshesperia.com.ve/".$reg["url"]."'>Ver Más</a>";
             }else{
              $urlBanner="";
             }
            
            $active="";
           
            if($i==0)
                $active="active";
            else  
                $active=" ";
            echo "
                  <div class='item $active'>

                    <div class='carousel-caption hidden-lg hidden-md'>
                        <h3 class='main-text text-center'>  $reg[titulo]<br>
                        $urlBanner
                        </h3>
                        <p class='visible-lg visible-md'>$reg[descripcion]</p>

                    </div>

                    <div class='pull-left carousel-caption hidden-xs hidden-sm'>
                        <h3 class='main-text'>$reg[titulo]</h3>
                        <p class='visible-lg visible-md'>$reg[descripcion]<br>
                        $urlBanner
                        </p>
                        
                     </div>

                    <img src='https://hoteleshesperia.com.ve/img/$reg[nombre_img]' alt='$reg[titulo]' class='image-responsive' style='width:100%''>

                </div>

            ";
            $i++;
        }

      
        echo "
          </div>
        </div>";

        echo "<div id='my-reserva' style='margin-top:-500px;' class='panel panel-reserva over-slider ancho-reserva pull-right visible-lg visible-md reset-reserva'>";
         
        CREAR_RESERVA($mysqli,$hostname,$user,$password,$db_name);
    echo'
             </div>
        </header>';
    }

}

function titulos_index($hostname,$user,$password,$db_name){
    $sql = "SELECT * from hesperia_v2_portada";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $numRows= mysqli_num_rows($result);
    echo "<div class='container-fluid'>

           <div class='row'>";
    if ($numRows!=null && $numRows>0) {
        $i=0;
        while ($reg = mysqli_fetch_assoc($result)){

            foreach ($reg as $key => $value) {

                $reg[$key] = utf8_encode($reg[$key]);
            }

            $pos = strpos($reg["descripcion_columna"], "|");

            if($pos===false){
              $mostrarDescripcion = $reg["descripcion_columna"];
            }else{
              $portada = explode("|", $reg["descripcion_columna"]);
              $mostrarDescripcion = "$portada[0]
                    <a  href='#' id='mas-info-$i' class='portada-ver-mas btn btn-link'>Mas Informacíon</a>
                     <span class='portada-span'>$portada[1]</span>";
            }            
            echo "
                <div class='col-lg-4'>
                    <h3 class='text-center text-primary imgPortadaV' ><img src='https://hoteleshesperia.com.ve/img/".$reg["url_imagen"]."'/></h3>
                    <h2 class='text-center text-primary'>$reg[titulo_columna]</h2>
                    <p class='text-justify'>
                        $mostrarDescripcion
                    </p>
                </div>

            ";
            $i++;
        }
    }
    echo "</div>
    </div>

    </div>";

}

function cargar_items_index($hostname,$user,$password,$db_name){
    $sql = "SELECT a.*, b.razon_social from hesperia_v2_item_portada a inner join hesperia_settings b on (a.id_hotel=b.id)";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $numRows= mysqli_num_rows($result);
    $arrayReg= array();
    if ($numRows!=null && $numRows>0) {
  
      for ($set = array (); $row = mysqli_fetch_assoc($result); $set[] = $row);
    
    }
  return $set;
}

function mostrar_paquetes($reg){

   echo "<div class='container-fluid' id='container-paquetes'>
          <div class='row'>
            <div class='col-lg-12'>
              <h2 class='page-header text-center text-primary'>Paquetes</h2>
            </div> 
        ";
   for ($i=0; $i <count($reg) ; $i++) { 
     if($reg[$i]["id_tipo_item"]=="1"){
      echo "
         <div class='col-md-4 col-xs-12 col-sm-6  no-space'>
            <div class='pdf-thumb-box' id='paquete-".$i."'>
                <a href='https://hoteleshesperia.com.ve/paquetes/".$reg[$i]["url"]."/".toAscii(utf8_encode($reg[$i]["titulo_item"]))."'>
                    <div class='pdf-thumb-box-overlay'>
                      <h2 class='text-center'>".utf8_encode($reg[$i]["titulo_item"])."</h2>
                      <p class='text-center'>".utf8_encode($reg[$i]["descripcion_item"])."</p>
                      <div class='btn-primary btn btn-sm'>
                        Reservar
                      </div>
                    </div>
                    <img class='img-responsive block-center' src='https://hoteleshesperia.com.ve/img/".$reg[$i]["url_imagen"]."' alt='".utf8_encode($reg[$i]["titulo_item"])."'>
                </a>

            </div>
            <div class='vertical-social-box'></div>
        </div>
        ";
     }
   }

   echo " 
    </div>";
}
function habitaciones_index($reg){
   $flag_primero = 0;
   $active=" ";

   echo "
   <div class='container-fluid' id='container-habitaciones'>
      <h2 class='page-header text-center text-primary'>Habitaciones</h2>
      

            <div id='carousel-example-generic' class='carousel slide carousel-hab' data-interval='false' data-ride='carousel'>
            
                <div class='carousel-inner'>
                
          ";
   for ($i=0; $i <count($reg) ; $i++) { 

     if($reg[$i]["id_tipo_item"]=="2"){
      
      if($flag_primero==0){
        $active="active";
        $flag_primero=1;
      }else{
        $active="";
      }
      echo "
         <div class='item $active' style='background:white;'>
            <a class='left carousel-control' href='#carousel-example-generic' style='z-index:9999;'' data-slide='prev'>
              <span class='glyphicon glyphicon-chevron-left'></span>
            </a>
            <div class='col-md-6 hidden-xs hidden-sm'>
                <h3 class='featurette-heading text-primary'>".utf8_encode($reg[$i]["titulo_item"])."</h3>
                <h3 class='text-muted'><small>".$reg[$i]["razon_social"]."</small></h3>
                <p class='lead'>".utf8_encode($reg[$i]["descripcion_item"])."
                </p>
                <p class='text-center'><a class='btn btn-sm btn-primary' href='".$reg[$i]["url"]."' role='button'>Visitar Hotel</a></p>
            </div>

            <div class='col-md-6'>
                <img class='featurette-image img-responsive center-block no-space' data-src='holder.js/500x500/auto'
                 src='img/".$reg[$i]["url_imagen"]."' data-holder-rendered='true'>
                           
                  <div class='carousel-caption hidden-lg hidden-md'>
                      <h3 class=''>".utf8_encode($reg[$i]["titulo_item"])."<br>
                          ".$reg[$i]["razon_social"]."
                      </h3>
                      <p class='text-center'><a class='btn btn-sm btn-primary' href='https://hoteleshesperia.com.ve/".$reg[$i]["url"]."' role='button'>Visitar Hotel</a></p>
                  </div>
            </div>
            <a class='right carousel-control' href='#carousel-example-generic' data-slide='next'>
                <span class='glyphicon glyphicon-chevron-right'></span>
            </a> 
        </div>
      ";
     }
   }

   echo "
        </div>
      </div>
   </div>
   ";
}

function redes_sociales(){
 /* echo '

  <div class="container-fluid">
     <h2 class="page-header text-center text-primary">Social Media</h2>
    <div class="row">

   <div class="col-md-4 col-sm-12" id="widget-facebook">
      <h3 class="text-center text-primary">Facebook</h3>
      <div class="fb-page" data-href="https://www.facebook.com/HesperiaVe" data-tabs="timeline" data-width="500px" data-height="600px" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/HesperiaVenezuela"><a href="https://www.facebook.com/HesperiaVenezuela">Hoteles Hesperia Venezuela</a></blockquote></div></div>
  </div>
   <div class="col-md-4 col-sm-12" id="widget-twitter">
      <h3 class="text-center text-primary">Twitter</h3>
      <a class="twitter-timeline" href="https://twitter.com/HesperiaVE" data-widget-id="703316194443583488">Tweets por el @HesperiaVE.</a>
   </div>

   <div class="col-md-4 col-sm-12" id="widget-instagram" style="height: 600px;">
      <h3 class="text-center text-primary">Instagram</h3>
     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BSHQ4eIgGpk/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Un domingo especial desde @olasbeachclubmgta del #HesperiaIslaMargarita en Playa Puerto Viejo  #IslaMargarita #sunday #felizDomingo #beach #beachclub #IslaDeMargarita</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una publicación compartida de Hoteles Hesperia Venezuela (@hesperiave) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-03-26T19:43:33+00:00">26 de Mar de 2017 a la(s) 12:43 PDT</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
  </div>

   </div>
  </div>
  ';*/
}

function restaurantes($hostname,$user,$password,$db_name){
    $sql = "SELECT a.*, b.razon_social FROM hesperia_restaurant a inner join hesperia_settings b
    on(a.id_hotel = b.id) where a.ind_portada ='S' order by orden asc";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $numRows= mysqli_num_rows($result);
    $arrayReg= array();
    if ($numRows!=null && $numRows>0) {
  
      for ($set = array (); $row = mysqli_fetch_assoc($result); $set[] = $row);
    
    }
    echo "
    <div class='container marketing'>
        <div class='row featurette'>
            <div class='col-lg-12'>
               <h2 class='featurette-heading text-center'>Restaurantes</h2>
               <p class='lead text-center'>
                  Tanto en nuestro Hotel de Valencia como nuestros Resorts en la Isla de Margatita
                  podrás disfrutar de excelentes experiencias culinarias con nuestros restaurantes
                  especializados.
         </div>
        
    </div> <!-- marketing -->

    <div class='col-md-12'>

        <h2 class='page-header text-uppercase'>Reserva con nosotros</h2>

        <form  role='form' id='form-reserva-restaurant'>

          <div class='col-md-5'>

            <div class='form-group'>

              <label for='hoteles'>Elija un restaurant</label>

              <select class='form-control' name='id_restaurant' id='id_restaurant'>";
                for ($i=0; $i <count($set) ; $i++) {

                  echo "<option value='".$set[$i]["id_restaurant"]."'>".$set[$i]["nombre_restaurant"]." (".$set[$i]["razon_social"].")

                  </option>";  
                }

                
              echo "
              </select>

            </div>

          </div>

          <div class='col-md-7'>

            <div class='form-group'>

          
              <div class='col-md-2'>
              
                <label>Asistentes</label><br>

                <input type='number' class='form-control' name='asistentes' min='1' id='asistentes'>

              </div>
          
              <div class='col-md-2'>
                <label>Mesas</label>
                <input type='number' class='form-control' name='mesas' id='mesas' min='1'>

              </div>

              <div class='col-md-4'>

                <label>Fecha</label>
                <input class='form-control'  name='fecha' id='fecha_restaurant'>          
                  
              </div>

              <div class='col-md-4'>

                <label>Hora</label>
                <div class='input-group bootstrap-timepicker timepicker'>
                    <input id='timepicker1' type='text' name='hora' class='form-control input-small'>
                    <span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>
                </div>

              </div> 
            </div> <!--FORM GROUP-->

          </div> <!--COL-MD-7 -->

      </div>

      <div class='col-md-12'>
        <div class='form-group'>

          <div class='col-md-2'>
              
            <label>Nombre</label><br>

            <input type='text' class='form-control' name='nombre' id='nombre'>

          </div>
          <div class='col-md-2'>
            <label>Telefono</label>
            <input type='text' class='form-control' name='telefono' id='telefono'>
          </div>

          <div class='col-md-4'>
            <label>Email</label>
            <input type='text' class='form-control' name='email' id='email-rest'>

          </div>
            <div class='col-md-4'>
                <label>Mensaje (opcional)</label>
                <input type='text' class='form-control' name='mensaje' id='mensaje'>
            </div>
        </div>
      </div>

      <div class='col-md-12'>
    
        <div class='col-md-4 col-md-offset-4'>
          <br>
          <button type='button' id='reservar-restaurant' class='btn btn-primary btn-sm btn-lg btn-block m-button'>Reservar</button>
        </div>
           
      </div>
  </form>

</div>
    ";
    /*
    
               <div class='col-lg-6'>
                
                 <div class='form-group'>
                     <label>Fecha</label>
                     <input type='number' class='form-control input-small' name='fecha'>
                 </div>

                 <div class='form-group'>
                    <div class='input-group bootstrap-timepicker timepicker'>
                      <input id='timepicker1' type='text' class='form-control input-small'>
                      <span class='input-group-addon'><i class='glyphicon glyphicon-time'></i></span>
                  </div>

                  <button type='reservar' class='btn btn-primary'>Reservar</button>
                </div>

    */
    $push ="col-sm-6 col-md-7";
    $pull ="col-sm-6 col-md-5";
    $flag=1;
    for ($i=0; $i <count($set) ; $i++) {     
       // if ($set[$i]["id_hotel"]==1) {
          echo "

          <div class='container marketing'>
            <hr class='featurette-divider'>
            <div class='row featurette'>
              <div class='$push'>
                  <h2 class='featurette-heading'>".$set[$i]["nombre_restaurant"]."</h2>
                  
                  <p class='hidden-xs lead'>".strip_tags($set[$i]["caracteristicas"], '<a>')."</p>
                  
                  <h3 class='text-primary'>
                    ".$set[$i]["razon_social"]."
                      <a class='hidden-xs hidden-sm btn btn-primary pull-right reserva-restaurant' 
                      data-value='".$set[$i]["id_restaurant"]."' href='#id_restaurant'>Reservar</a>
                  </h3>   
              </div>

              <div class='$pull'>
                  <img class='featurette-image img-responsive' src='https://hoteleshesperia.com.ve/img/restaurant/img-portada/".$set[$i]["img_portada"]."'>
                  <a class='visible-xs visible-sm btn btn-primary reserva-restaurant'
                   data-value='".$set[$i]["id_restaurant"]."' href='#id_restaurant'>Reservas</a>
              </div>
            </div>
          </div>
          ";
       // }
        if ($flag==1) {
                 
             $push ="col-sm-6 col-md-7 pull-right";
             $pull ="col-sm-6 col-md-5";
             $flag=0;
          }else{
             $push ="col-sm-6 col-md-7";
             $pull ="col-sm-6 col-md-5";
             $flag=1;
          }

    }  
}


function blog_home($hostname,$user,$password,$db_name){
    $sql = "SELECT a.*, b.nombre, b.apellido FROM hesperia_v2_post a 
    inner join hesperia_v2_blog_autores b on(a.id_autor = b.id_autor)
     ORDER BY a.fecha DESC limit 1";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $numRows= mysqli_num_rows($result);
    $arrayReg= array();
    if ($numRows!=null && $numRows>0) {
  
      for ($set = array (); $row = mysqli_fetch_assoc($result); $set[] = $row);
    
    }
    echo "

    <div class='container marketing'>
    <div class='col-lg-12'>
              <h2 class='page-header text-center text-primary'>Blog</h2>
      </div>
            <hr class='featurette-divider'>
            <div class='row featurette'>
              <div class='col-sm-6 col-md-7 pull-right'>
                  <h2 class='featurette-heading'>Visita Nuestro Blog</h2>
                  
                  <p class='hidden-xs lead'>".$set[0]['titulo']."</p>
                  
                  <h3 class='text-primary'>
                   <b>Autor:</b> ".$set[0]["nombre"]." ".$set[0]["apellido"]." 
                      <a class='hidden-xs hidden-sm btn btn-primary pull-right' href='https://hoteleshesperia.com.ve/blog'>Visitar Blog</a>
                  </h3>   
              </div>

              <div class='col-sm-6 col-md-5'>
                  <img class='featurette-image img-responsive' src='https://hoteleshesperia.com.ve/img/gonzalopena1.jpg' alt='blog de gonzalo peña'>
                  <a class='visible-xs visible-sm btn btn-primary' href='https://hoteleshesperia.com.ve/blog'>Visitar Blog</a>
              </div>
            </div>
          </div>
      </div>
    ";
   

}

function cargarPDF($nombre){
  $titulo ="";
  $url="";

  switch (strtolower($nombre)) {
    case 'resumenhim':
     $titulo = "Hesperia Isla Margarita";
     $url = "resumenHIM.pdf";
      break;

    case 'resumenhec':
     $titulo = "Hesperia Edén Club";
     $url = "resumenHEC.pdf";
      break;
    
    case 'resumenhpa':
     $titulo = "Hesperia Playa El Agua";
     $url = "resumenHPA.pdf";
      break;

      case 'resumenhwtc':
     $titulo = "Hesperia WTC Valencia";
     $url = "resumenHWTC.pdf";
      break;

      case 'carnavalhec':
     $titulo = "Carnaval en Hesperia Edén Club";
     $url = "carnavalHEC.pdf";
      break;

      case 'carnavalhpa':
     $titulo = "Carnaval en Hesperia Playa El Agua";
     $url = "carnavalHPA.pdf";
      break;

      case 'carnavalhim':
     $titulo = "Carnaval en Hesperia Isla Margarita";
     $url = "carnavalHIM.pdf";
      break;

       case 'partai2017':
     $titulo = "Programación Partai 2017";
     $url = "partai2017.pdf";
      break;

      case 'sanvalentin2017':
     $titulo = "San Valentín 2017 en Hesperia";
     $url = "sanvalentin2017.pdf";
      break;

      case 'hesperiabluehpa':
     $titulo = "Hesperia Blue - Hesperia Playa el Agua";
     $url = "hesperia_blueHPA.pdf";
      break;

      case 'hesperiabluehim':
     $titulo = "Hesperia Blue - Hesperia Isla Margarita";
     $url = "hesperia_blueHIM.pdf";
      break;

      case 'hesperiabluehec':
     $titulo = "Hesperia Blue - Hesperia Edén Club";
     $url = "hesperia_blueHEC.pdf";
      break;

      case 'cupidoweekend':
     $titulo = "Programación Cupido Weekend";
     $url = "cupidoweekend.pdf";
      break;      

  }
  echo "
  <div class='container marketing'>
  <br>
      <div class='col-lg-12'>
                 <h2 class='featurette-heading text-center'>$titulo</h2>
      </div>
    <div class='col-lg-12'>
      <iframe src='//docs.google.com/gview?url=https://hoteleshesperia.com.ve/img/prensa/$url&embedded=true'
       style='width:100%; height:800px;' frameborder='0'></iframe>

    </div>
  </div>";
}

function noticia_home($mysqli){
  $query = "SELECT * FROM `hesperia_v2_noticias` WHERE 1 order by id_noticia desc limit 1";

  $result = $mysqli->query($query);
  while ($reg = mysqli_fetch_assoc($result)) {
    $aux = strip_tags($reg["contenido"]);
    $resumen = substr($aux, 0, 100);
    echo"
    <h2 class='page-header text-center text-primary'>Noticias</h2>
    <div class='container-fluid'>
            <div class='col-md-6'>
                <a href='https://hoteleshesperia.com.ve/noticia/".$reg["id_noticia"]."/".toAscii($reg["titulo"])."'>
                    <img class='img-responsive img-hover' src='https://hoteleshesperia.com.ve/img/media_center/".$reg["imagen"]."' alt=''>
                </a>
            </div>
            <div class='col-md-6'>
                <h3>
                    <a href='https://hoteleshesperia.com.ve/noticia/".$reg["id_noticia"]."/".toAscii($reg["titulo"])."'>".$reg["titulo"]."</a>
                </h3>
                <blockquote>
                  <p>".$reg["subtitulo"]."</p>
                  <footer>$resumen... <a href='https://hoteleshesperia.com.ve/noticia/".$reg["id_noticia"]."/".toAscii($reg["titulo"])."'>Leer nota</a></footer>
                </blockquote>
                
                <a class ='btn btn-primary pull-right' href='https://hoteleshesperia.com.ve/mediacenter'>Más Noticias</a>
            </div>
        </div>
    ";
    
  }
  
}

function getHoteles($mysqli, $tipo){
  $resultado = null;

  switch ($tipo) {
    case 'vacacional':
      $query = "SELECT * from hesperia_settings where id in(2,3,4)";
      break;

    case 'urbano':
      $query = "SELECT * from hesperia_settings where id in(1, 5)";
      break;
    
    default:
      $query = null;
      break;
  }

  $resultado = $mysqli->query ($query);

  return $resultado;
}
 ?>