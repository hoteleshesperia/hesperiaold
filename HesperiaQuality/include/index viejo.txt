<div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Hesperia Quality
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Área</th>
                                            <th>Hesperia WTC Valencia</th>
                                            <th>Hesperia Isla Margarita</th>
                                            <th>Hesperia Playa El Agua</th>
                                            <th>Hesperia Edén Club</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($_SESSION["tipo_acceso"] == 1){
                                            $sql=sprintf("SELECT * FROM hesperia_v2_enc_tipo_encuenta WHERE codificacion like '%s%%' AND codificacion not in ('HUB', 'CON')",
                                                mysql_real_escape_string("HU"));
                                        }else{
                                            $sql=sprintf("SELECT * FROM hesperia_v2_enc_tipo_encuenta WHERE codificacion like '%s%%' AND codificacion not in ('HPB', 'CON')",
                                                mysql_real_escape_string("HP"));
                                        }                                        

                                        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

                                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                                            $sqlResp=sprintf("SELECT count(id_tipo_enc) as total_encuesta FROM hesperia_v2_enc_respuesta WHERE id_tipo_enc = '%s'",
                                                mysql_real_escape_string($row["id"]));

                                            $resultResp=QUERYBD($sqlResp,$hostname,$user,$password,$db_name);
                                            $rowResp=mysqli_fetch_array($resultResp,MYSQLI_ASSOC);

                                            echo'
                                                <tr class="gradeX">
                                                    <td><strong>'.utf8_encode($row["tipo_encuesta"]).'</strong></td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                </tr>
                                            ';
                                        }
                                        ?>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <fieldset>
                        <legend>Párametros de Reporting</legend>
                        <div class="form-group col-md-6">
                            <label>Desde</label>
                            <input class="form-control" placeholder="2016-04-27" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Hasta</label>
                            <input class="form-control" placeholder="2016-04-27" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Área</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="">Todas:</option>
                                <?php
                                $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                                while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                                    echo '<option value="">'.utf8_encode($row["tipo_encuesta"]).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Sexo</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="">Seleccione:</option>
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Edad</label>
                            <select class="form-control" name="edad" id="edad">
                                <option value="">Seleccione:</option>
                                <option value="18-24">18-24</option>
                                <option value="25-34">25-34</option>
                                <option value="35-44">35-44</option>
                                <option value="45-54">45-54</option>
                                <option value="55-64">55-64</option>
                                <option value="+65">+65</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>País</label>
                            <input class="form-control" placeholder="Indique país" name="pais" id="pais">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Ciudad</label>
                            <input class="form-control" placeholder="Indique ciudad" name="ciudad" id="ciudad">
                        </div>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-info btn-block">Reporting</button>
                        </div>
                        
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <fieldset>
                        <legend>Párametros de Reporting</legend>
                        <div class="form-group col-md-6">
                            <label>Desde</label>
                            <input class="form-control" placeholder="AAAA-MM-DD" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Hasta</label>
                            <input class="form-control" placeholder="AAAA-MM-DD" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Año</label>
                            <input class="form-control" placeholder="AAAA" name="fecha" id="fecha">
                        </div>
                        
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-info btn-block">Reporting</button>
                        </div>
                    </fieldset>

                </div>
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> General
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Acciones
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">General reporte en .PDF</a>
                                        </li>
                                        <li><a href="#">Enviar por correo</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">                            
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>