<?php

function Redirect($url, $permanent = false)
{
    if (headers_sent() === false)
    {
    	header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
    }

    exit();
}

function cargarSelectHoteles($hostname,$user,$password,$db_name, $todos){

	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
	$queryHoteles = "select a.razon_social, a.id from hesperia_settings a";
	$result = $mysqli->query($queryHoteles);

	if ($todos=="S") {
		echo "<option value='-1'>Todos</option>";
	}

	$clase="";
	$selected="";
	while ($reg = mysqli_fetch_assoc($result)) {
			if ($reg["id"]==1) {
				$clase="hu";
			}else{
				$clase="hp";
			}

			if ($_SESSION["tipo_acceso"]==$reg["id"]) {
				$selected="selected";
			}else{
				$selected="";
			}
			echo "<option id='option-$reg[id]' class='$clase'  value='$reg[id]' $selected> ".$reg["razon_social"]."</option>";
	}
	//$mysqli->close();
}

function listaEncuestasCalidad($tipo_acceso, $mysqli){

	//include_once('../include/databases.php');

	//$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);

	if ($tipo_acceso==1) {
		$tipo="HU";
	}else{
		$tipo="HP";
	}

	$where="";
	if ($_SESSION["tipo_acceso"]<5 && $_SESSION["tipo_acceso"]>0) {
		$where = " and id_hotel=".$_SESSION["tipo_acceso"];
	}

	$query="SELECT a.*, a.id as ident, (SELECT count(id_tipo_enc) FROM hesperia_v2_enc_respuesta WHERE id_tipo_enc = ident $where)
	as total_encuesta FROM hesperia_v2_enc_tipo_encuenta a WHERE codificacion like '%$tipo%'
	AND codificacion not in ('HUB', 'CON', 'HPB')";
	/*
	SELECT a.*, a.id as ident, (SELECT count(id_tipo_enc) FROM hesperia_v2_enc_respuesta WHERE id_tipo_enc = ident) as total_encuesta, (SELECT id from hesperia_v2_enc_respuesta where id_tipo_enc = ident)as id_encu, (SELECT sum(respuesta) from hesperia_v2_enc_res_detalle where id_encuesta = id_encu) as sumatoria
	 FROM hesperia_v2_enc_tipo_encuenta a WHERE codificacion like '%HP%'
	AND codificacion not in ('HUB', 'CON', 'HPB')

	*/

	$result=$mysqli->query($query);

	if($result->num_rows>0){

		while ($reg=mysqli_fetch_assoc($result)) {
			echo "
			<tr class='gradeX'>
				<td>$reg[tipo_encuesta]</td>
				<td>$reg[codificacion]</td>
				<td>$reg[total_encuesta]</td>
				<td class='text-center'>
				    <a href='http://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=nuevaencuesta&enc=$reg[codificacion]'>Nueva</a>
				</td>
			</tr>
			";
		}

	}

	$mysqli->close();
}


function listaEncuestasPorHotel($tipo_acceso){

	/*include_once('../include/databases.php');

	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);

	if ($tipo_acceso==1) {
		$tipo="HU";
	}else{
		$tipo="HP";
	}
	$query="SELECT a.*, (SELECT count(id_tipo_enc) FROM hesperia_v2_enc_tipo_encuenta WHERE id_tipo_enc = a.id)
	as total_encuesta FROM hesperia_v2_enc_tipo_encuenta a WHERE codificacion like '%$tipo%'
	AND codificacion not in ('HUB', 'CON')";

	$result=$mysqli->query($query);

	if($result->num_rows>0){

		while ($reg=mysqli_fetch_assoc($result)) {
			echo "
			<tr class='gradeX'>
				<td>$reg[tipo_encuesta]</td>
				<td>$reg[codificacion]</td>
				<td>$reg[total_encuesta]</td>
				<td class='text-center'>
				    <a href='nueva_encuesta.php?enc=$reg[codificacion]'>Nueva</a>
				</td>
			</tr>
			";
		}

	}

	$mysqli->close();*/
}

function verMenu($tipo){

	if ($tipo!=5) {

		switch ($tipo) {
			case '1':
				echo "
				     <li>
				     	<a href='http://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=listaencuestas'>
				     	   <span class='fa fa-pencil-square-o'></span>Cargar Encuesta (Hesperia WTC Valencia)
				     	</a>
				     </li>

				    ";
				break;
			case '2':
				echo "
				    <li>
						<a href='http://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=listaencuestas'>
							<span class='fa fa-pencil-square-o'></span>Cargar Encuesta (Hesperia Isla Margarita)
						</a>
					</li>
				";
				break;

			case '3':
				echo "
					<li>
						<a href='http://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=listaencuestas'>
							<span class='fa fa-pencil-square-o'></span> Cargar Encuesta <br>(Hesperia Playa El Agua)
						</a>
					</li>

				";
				break;

			case '4':
				echo "
					<li>
						<a href='http://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=listaencuestas'>
							<span class='fa fa-pencil-square-o'></span> Cargar Encuesta <br>(Hesperia Edén Club)
						</a>
					</li>

				";
				break;

			default:
				# code...
				break;

		}
	}

}

//RESPALDO DE LA FUNCION PARA FILTRAR
function verPromediosEncuestas($mysqli){

	$queryHoteles = "select a.razon_social, a.id from hesperia_settings a";
	$result = $mysqli->query($queryHoteles);
	$array_tipos_encuestas;

	while ($reg = mysqli_fetch_assoc($result)) {
		//	echo "$reg[razon_social]-$reg[id]<br>";
	}

	$query = "SELECT a.*, (SELECT count(id_tipo_enc) FROM hesperia_v2_enc_respuesta WHERE id_tipo_enc = a.id)
	 as total_encuesta FROM hesperia_v2_enc_tipo_encuenta a WHERE codificacion not in ('HUB', 'CON', 'HPB')
	  GROUP by tipo_encuesta";

	$result = $mysqli->query($query);
	$i=0;
	while ($reg = mysqli_fetch_assoc($result)) {
			$arrayAux["tipo_encuesta"]=$reg["tipo_encuesta"];
			$arrayAux["codificacion"] =substr($reg["codificacion"], 2);

 		  	$queryCount = "SELECT a.*, a.id as id_encuesta, count(a.codificacion) as cantidad from hesperia_v2_enc_tipo_encuenta a inner join hesperia_v2_enc_respuesta b on(a.id = b.id_tipo_enc)
			    where a.codificacion like '%".$arrayAux["codificacion"]."%' " ;
			 $result2 = $mysqli->query($queryCount);

			 while ($reg2 = mysqli_fetch_assoc($result2)) {
			 	$arrayAux["cantidad"]=$reg2["cantidad"];


			 	$queryIdEncuesta = "select a.*, b.codificacion from hesperia_v2_enc_respuesta a inner join
			 	hesperia_v2_enc_tipo_encuenta b on  (a.id_tipo_enc = b.id)  where b.codificacion LIKE '%".$arrayAux["codificacion"]."%'
			 	 group by a.id ";

			 	$resultIdEncuesta = $mysqli->query($queryIdEncuesta);
			 	$acumTotalesHP=0;
			 	$acumTotalesHU=0;
			 	while ($regIdEncuesta = mysqli_fetch_assoc($resultIdEncuesta)) {
			 		if ($regIdEncuesta["id"]!=null) {
			 		$id_aux = $regIdEncuesta["id"];
				 	}else{
				 		$id_aux = 0;
				 	}
				 	$flagTipoHotel =substr($regIdEncuesta["codificacion"], 0,2);

				 	$queryTotales = "SELECT IF (($id_aux>0), (SELECT sum(c.respuesta) as res from hesperia_v2_enc_res_detalle c where c.id_encuesta = $id_aux),0) as total";

				 	$result3 = $mysqli->query($queryTotales);
				 	$reg3 = mysqli_fetch_assoc($result3);
				 	//echo "<br><br>$queryTotales<br><br>";
				 	if ($flagTipoHotel=="HP") {
				 		$acumTotalesHP = $reg3["total"]+$acumTotalesHP;
				 	}else{
				 		$acumTotalesHU=$reg3["total"]+$acumTotalesHU;
				 	}

			 	}

			 	if ($arrayAux["cantidad"]>0) {
			 		//$promedio = $acumTotales/$arrayAux["cantidad"];
			 		$auxHU = promedioTipoPregunta($mysqli, "HU".$arrayAux["codificacion"]);
			 		$auxHP = promedioTipoPregunta($mysqli, "HP".$arrayAux["codificacion"]);

			 		$promHU = $acumTotalesHU/$arrayAux["cantidad"]/$auxHU;
			 		$promHP = $acumTotalesHP/$arrayAux["cantidad"]/$auxHP;
			 		//$prom1=$acumTotales/promedioTipoPregunta($mysqli, "HU".$arrayAux["codificacion"]);
			 		//$prom2 =$acumTotales/promedioTipoPregunta($mysqli, "HP".$arrayAux["codificacion"]);
			 	}else{
			 		$promedio=0;
			 		$promHU=0;
			 		$promHP=0;
			 	}
			 }
			// echo "$arrayAux[codificacion]-$arrayAux[tipo_encuesta]-$arrayAux[cantidad]-$acumTotales-$promedio<br>";
		//$prom1=promedioTipoPregunta($mysqli, "HU".$arrayAux["codificacion"]);
		//$prom2 = promedioTipoPregunta($mysqli, "HP".$arrayAux["codificacion"]);
		echo "
		<tr class='gradeX'>
			<td>$arrayAux[tipo_encuesta] - $arrayAux[codificacion]</td>
			<td>$arrayAux[cantidad]</td>
			<td>$acumTotalesHP - $acumTotalesHU </td>
			<td>$promHP - $promHU</td>
        </tr>

		";
		$i++;
	}
	$mysqli->close();
}

function promedioTipoPregunta($mysqli, $cod){

	$query="SELECT COUNT(*)as x from (SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
    hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HUB'
    and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
    (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c
    where c.codificacion = '".$cod."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc) x";

	$result = $mysqli->query($query);
	if ($reg = mysqli_fetch_assoc($result)) {
		return $reg["x"];
	}else{
		return $mysqli->error;
	}
}

function promedioPreguntaEspecifica($hostname,$user,$password,$db_name, $cod){
	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
	$query="SELECT DISTINCT(a.id) as id, pregunta, orden, (c.id) as id_enc, NULL from hesperia_v2_enc_preguntas a,
	 hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HPB'
	  and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT DISTINCT (a.id) as id, pregunta, orden, (c.id)
	  as id_enc, c.codificacion from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b,
	  hesperia_v2_enc_tipo_encuenta c where c.codificacion like '%$cod'
	  and b.id_encuesta = c.id and a.id = b.id_pregunta group by (id) order by orden asc";
	$result = $mysqli->query($query);

	while ($reg = mysqli_fetch_assoc($result)) {
		$query2 = "SELECT SUM(respuesta) as suma, count(respuesta) as
		 cant_pregunta, b.pregunta, a.id_pregunta from hesperia_v2_enc_res_detalle a
		 left join hesperia_v2_enc_preguntas b on (a.id_pregunta = b.id) where id_pregunta = $reg[id]";

		$result2 = $mysqli->query($query2);

		while ($reg2=mysqli_fetch_assoc($result2)) {

			if($reg2["cant_pregunta"]>0){
				$promedio = $reg2["suma"]/$reg2["cant_pregunta"];
			}else{
				$promedio=0;
			}

			echo("<br>".$reg2["suma"]."-".$reg2["id_pregunta"]."-".$reg2["cant_pregunta"]."-".$reg["pregunta"]."-".$promedio."<br>$query2");
		}
	}

	if ($error=$mysqli->error) {
		echo($error);
	}
	$mysqli->close();
}


function testing($hostname,$user,$password,$db_name, $cod){

	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
	$indice =0;
	$string_id_encuestas="";

	echo "<div class='dataTable_wrapper'>
    	<table class='table table-striped table-bordered table-hover' id='dataTables-example2'>
        	<thead>
            	<tr>
                	<th>Pregunta</th>
                    <th>Cantidad de Encuestas</th>
                    <th>Tipo</th>
                    <th>Promedio</th>
                </tr>
            </thead>;
            <tbody id='bodyTable-modal'>";

	while ($indice<2) {

		if ($indice==0) {
			//PREGUNTAS ESPECIFICAS

			$query="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc, c.codificacion
			from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta
			 c where c.codificacion like '%CKO' and b.id_encuesta = c.id and a.id = b.id_pregunta
			 group by (id) order by orden asc";
		}else{
			echo("PREGUNTAS GENERALES<BR>");
			//PREGUNTAS GENERALES
			$query = "SELECT DISTINCT(a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
			 hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HPB'
			  and b.id_encuesta = c.id and a.id = b.id_pregunta ";
		}

		$result = $mysqli->query($query);
		$array_id_encuestas = array();
		$auxIndice =0;
		//$string_id_encuestas=""; //REESTABLECEMOS

		while($reg = mysqli_fetch_assoc($result)){
			if ($indice==0) {
			$query2 = "SELECT id_encuesta FROM `hesperia_v2_enc_res_detalle` WHERE id_pregunta = $reg[id]";
			}else{
				$query2 = "SELECT id_encuesta FROM `hesperia_v2_enc_res_detalle` WHERE id_pregunta = $reg[id]
				and id_pregunta in (".rtrim($string_id_encuestas,",").") ";
			}

			$result2 = $mysqli->query($query2);
			if ($auxIndice==0) {
				while ($reg2 = mysqli_fetch_assoc($result2)) {

					if (!in_array($reg2["id_encuesta"] , $array_id_encuestas) && $reg2["id_encuesta"]>0) {
						$size = count($array_id_encuestas);
						$string_id_encuestas.= $reg2["id_encuesta"].",";
						if ($size>0) {
							$array_id_encuestas[$size+1] = $reg2["id_encuesta"];

						}else{
							$array_id_encuestas[$size] = $reg2["id_encuesta"];
						}

					}
				}
			}
			$auxIndice++;
			if ($string_id_encuestas!="") {
				sumatoria_encuestas($mysqli, $reg["id"], rtrim($string_id_encuestas, ","));
			}else{
				echo "<tr>
						<td>$reg[pregunta]</td>
						<td>0</td>
						<td></td>
					  </tr>";
			}

		}
	  $indice++;
	}
	echo "</tbody>
        </table>
    </div>";
	//echo("<br>".rtrim($string_id_encuestas,","));
}

function sumatoria_encuestas($mysqli, $id, $in){
	$query3 = "SELECT SUM(respuesta) as suma, count(respuesta) as cant_pregunta,
	b.pregunta, a.id_pregunta from hesperia_v2_enc_res_detalle a  inner join hesperia_v2_enc_preguntas b on
	(a.id_pregunta = b.id) where id_pregunta = ".$id." and a.id_encuesta in (".$in.")";

	$result3 = $mysqli->query($query3);

	while ($reg3 = mysqli_fetch_assoc($result3)) {
		if ($reg3["cant_pregunta"]>0) {
			 $promedio = $reg3["suma"]/$reg3["cant_pregunta"];
		}else{
			 $promedio=0;
		}
		echo "<tr>
				<td>$reg3[pregunta]</td>
				<td>$reg3[suma]</td>
				<td>$promedio</td>
			  </tr>
		";
		//echo($reg3["suma"]."-".$reg3["cant_pregunta"]."-".$reg3["pregunta"]."$promedio<br>");
	}
	//echo ($query3);
}

function lista_areas($mysqli, $tipo_acceso){
  $hotel="";
  if ($tipo_acceso>0 && $tipo_acceso<5) {
    $hotel= "and id_hotel = $tipo_acceso";
  }
	$query = "SELECT a.*, (SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id $hotel)
	 as total_encuesta FROM hesperia_v2_enc_tipo_encuenta a WHERE codificacion not in ('HUB', 'CON', 'HPB')
	 order by a.tipo_encuesta ";
	 // GROUP by tipo_encuesta para hacer distinct
	$result = $mysqli->query($query);
	while ($reg = mysqli_fetch_assoc($result)) {
		if (substr($reg["codificacion"], 0, 2)=="HU") {
			$tipocod = "(Urbano)";
		}else{
			$tipocod ="(Playa)";
		}
		echo "<tr><td>$reg[tipo_encuesta] - $tipocod</td><td>$reg[total_encuesta]</td></tr>";

	}

	$mysqli->close();
}

function mostrar_lista_enviados($tipo, $mysqli){

	if ($tipo!=1) {
		$ind_masivo="1";
	}else{
		$ind_masivo="0";
	}
	$where="";
	if ($_SESSION["tipo_acceso"]<5 && $_SESSION["tipo_acceso"]>0) {
		$where = " and id_hotel=".$_SESSION["tipo_acceso"];
	}
	$query ="SELECT a.*, DATE_FORMAT( a.fecha_env, '%d/%m/%Y') as x_fecha_env,
	DATE_FORMAT(fecha_respuesta , '%d/%m/%Y') as x_fecha_respuesta
	 FROM hesperia_v2_encuestas_mail a WHERE ind_masivo= '$ind_masivo' $where";
	$result = $mysqli->query($query);
	echo "<tbody>";
	while ($reg = mysqli_fetch_assoc($result)) {

		//if ($tipo ==1) {
			if ($reg["status"]==0) {
			$respondido ="No";
			}else{
				$respondido="Si";
			}
			echo "
					<tr>
						<td>$reg[email]</td>
						<td>$reg[x_fecha_env]</td>
						<td>$respondido</td>
						<td>$reg[x_fecha_respuesta]</td>
				  	</tr>
				 ";
		//}

	}
	echo " </tbody>";
	$mysqli->error;
}
function resumen_enviados_areas($mysqli){

	$where="";
	if ($_SESSION["tipo_acceso"]<5 && $_SESSION["tipo_acceso"]>0) {
		$where = " and id_hotel=".$_SESSION["tipo_acceso"]." ";
	}

	$query ="SELECT a.*, (SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id
		and ind_masivo=1 $where)
	 as total_encuesta, (SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id
	 	and ind_masivo=1 and id_respuesta !=-1 $where)as respondidas,
	 	(SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id
	 	and ind_masivo=1 and id_respuesta =-1 $where)as pendientes

	 	  FROM hesperia_v2_enc_tipo_encuenta a  inner join hesperia_v2_encuestas_mail b on(a.id=b.id_tipo_enc)
	 	   WHERE codificacion not in ('HUB', 'CON', 'HPB') and b.ind_masivo = 1 $where
	 order by a.tipo_encuesta";

	 $result = $mysqli->query($query);

	 //echo ($query);
	 echo "<tbody>";
	 while ($reg = mysqli_fetch_assoc($result)) {
	 	if ($reg["total_encuesta"]!=0) {
	 		$porcentaje = (100*$reg["respondidas"])/$reg["total_encuesta"];
	 	}else{
	 		$porcentaje=0;
	 	}

		if (substr($reg["codificacion"], 0, 2)=="HU") {
			$tipocod = "(Urbano)";
		}else{
			$tipocod ="(Playa)";
		}

		echo "
			<tr>
				<td>$reg[tipo_encuesta] $tipocod</td>
				<td>$reg[codificacion]</td>
				<td>$reg[total_encuesta]</td>
				<td>$reg[respondidas]</td>
				<td>$reg[pendientes]</td>
				<td>$porcentaje%</td>
			</tr>
			";
		//}

	}
}

function cargar_info_encuesta($tipo, $mysqli){
	$query="SELECT tipo_encuesta, id from hesperia_v2_enc_tipo_encuenta where codificacion = '".$tipo."' ";
	$result=$mysqli->query($query);
	if ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
	    $mysqli->close();
	    return $row;
	}else{
		$mysqli->close();
		return null;
	}
}

?>
