<?php

include_once('../include/databases.php');

$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);

session_start();
foreach ($_POST as $key => $value) {

	$data["$key"] = $mysqli->real_escape_string($value);
// echo ("$key-".$data['$key']);
}

if ($data["accion"]=="modal") {
	//testing($mysqli, $data);
	echo "modal";
}else{
	resumen_enviados_areas($mysqli, $data);
}

$mysqli->close();


function resumen_enviados_areas($mysqli, $data){

	$where="";

	if ($_SESSION["tipo_acceso"]<5 && $_SESSION["tipo_acceso"]>0) {
		$where = " and id_hotel=".$_SESSION["tipo_acceso"]." ";
	}else if($_SESSION["tipo_acceso"]==99){
		if ($data["id_hotel"]!=-1) {
		 $where = "and id_hotel=".$data["id_hotel"]." ";
	 }else{
		 $where="";
	 }
	}

	$fecha = filtro_fechas($data["fecha_desde"], $data["fecha_hasta"], "");

	$query ="SELECT DISTINCT a.*, (SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id
		and ind_masivo=1 $where $fecha)
	 as total_encuesta, (SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id
	 	and ind_masivo=1 and id_respuesta !=-1 $where $fecha)as respondidas,
	 	(SELECT count(id_tipo_enc) FROM hesperia_v2_encuestas_mail WHERE id_tipo_enc = a.id
	 	and ind_masivo=1 and id_respuesta =-1 $where $fecha)as pendientes

	 	  FROM hesperia_v2_enc_tipo_encuenta a  inner join hesperia_v2_encuestas_mail b on(a.id=b.id_tipo_enc)
	 	   WHERE codificacion not in ('HUB', 'CON', 'HPB') and b.ind_masivo = 1 $where $fecha
	 order by a.tipo_encuesta";

	 $result = $mysqli->query($query);

	 //echo ($query);
	 //echo "<tbody>";
	 while ($reg = mysqli_fetch_assoc($result)) {
	 	if ($reg["total_encuesta"]!=0) {
	 		$porcentaje = (100*$reg["respondidas"])/$reg["total_encuesta"];
	 	}else{
	 		$porcentaje=0;
	 	}

		if (substr($reg["codificacion"], 0, 2)=="HU") {
			$tipocod = "(Urbano)";
		}else{
			$tipocod ="(Playa)";
		}

		echo "
			<tr>
				<td>$reg[tipo_encuesta] $tipocod</td>
				<td>$reg[codificacion]</td>
				<td>$reg[total_encuesta]</td>
				<td>$reg[respondidas]</td>
				<td>$reg[pendientes]</td>
				<td>$porcentaje%</td>
			</tr>
			";
		//}

	}
	if (!$result) {
		echo "";
	}
}

function filtro_fechas($fecha_desde, $fecha_hasta, $prefijo_tabla){
	$fecha="";
	$fecha_desde_bd = date("Y-m-d", strtotime($fecha_desde));
	$fecha_hasta_bd = date("Y-m-d", strtotime($fecha_hasta));

	if ($fecha_desde!="" && $fecha_hasta!="") {
		$fecha = " AND date($prefijo_tabla"."fecha_env) between date('$fecha_desde_bd') and date('$fecha_hasta_bd')";
	}else{
		if ($fecha_desde !="") {
			$fecha = " AND date($prefijo_tabla"."fecha_env) >= date('$fecha_desde_bd')";
		}else if($fecha_hasta !=""){
			$fecha = " AND date($prefijo_tabla"."fecha_env) <= date('$fecha_hasta_bd')";
		}else{
			$fecha=" ";
		}
	}

	return $fecha;
}
 ?>
