<?php


$disabled = "";

if ($_SESSION["tipo_acceso"]!=5 && $_SESSION["tipo_acceso"]>0) {
    $disabled = " disabled= 'disabled'";
}
 ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Enviar encuestas por e-mail</h1>
    </div>

</div>

<div class="row">
	<form role="form">
		<div class="col-md-7">
			<div class="form-group">
	            <label>Destinatarios</label>
	            <textarea class="form-control" rows="3" name="direcciones" id="direcciones"></textarea>
	            <p class="help-block">Separe las direcciones de correo con punto y coma ";"</p>
	        </div>

	        <div class="form-group">
                <label>Elija un formato</label>
                <select class="form-control" name="formato" id="formato" <?php echo ($disabled); ?>>
                	<option value="mix">Ambos</option>
                    <option value="hu">Solo Hoteles Urbanos</option>
                    <option value="hp">Solo Hoteles de Playa</option>
                </select>
            </div>

            <div class="form-group">
                <label>Elija Hotel</label>
                <select class="form-control" name="id_hotel" id="id_hotel" <?php echo($disabled) ?>>
                   <?php
                   cargarSelectHoteles($hostname,$user,$password,$db_name, "S");
                    ?>
                </select>
            </div>

	        <button class="btn btn-primary" id="env-mail-masivo">Enviar</button>
        </div>
        <div class="col-md-5">
	        <div class="panel panel-default">
	        	<div class="panel-heading">Lista de Areas</div>
				<table class="table">
			    	<tr>
			    		<th>Area</th>
			    		<th>Emails enviados</th>
			    	</tr>

			    	<?php lista_areas($mysqli, $_SESSION["tipo_acceso"]); ?>
			    </table>
	        </div>
        </div>
	</form>
</div>
