<?php 

if ($_SESSION["tipo_acceso"]!=5 && $_SESSION["tipo_acceso"]>0) {
    $disabled = " disabled= 'disabled'";
}

 ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Enviar encuestas a e-mail especifico</h1>
    </div>

</div>
<div class="row">
	<form role="form" id="form-env-indv">
		<div class="col-md-12">
			<div class="form-group col-md-4 ">
	            <label>Email Destino</label>
	            <input type="email" class="form-control" id="email" name="email">        
	        </div>


            <div class="form-group col-md-4 ">
                <label>Elija Hotel</label>
                <select class="form-control" name="id_hotel" id="id_hotel" <?php echo ($disabled); ?>>
                   <?php 
                   cargarSelectHoteles($hostname,$user,$password,$db_name, "N");
                    ?>
                </select>
            </div>

            <div class="form-group col-md-4 ">
                <label>Elija Tipo de Encuesta</label>
                <select class="form-control" name="tipo_encuesta" id="tipo_encuesta">
                   
                </select>
            </div>
            <p class="text-center">
                <button class="btn btn-primary" id="env-mail-individual">Enviar</button>
            </p>
	        
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Registro de enviados</div>
                <table class="table" id="dataTableEspecificos">
                    <thead>
                        
                            <th>Email</th>
                            <th>Fecha</th>
                            <th>Respondido</th>
                            <th>Fecha-Respuesta</th>
                        
                    </thead>
                    
                    <?php mostrar_lista_enviados(1, $mysqli); ?>
                    
                </table>
            </div>
        </div>
	</form>
</div>