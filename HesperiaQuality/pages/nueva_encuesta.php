<?php
session_start();
include_once('../include/funciones_principales.php');
if (!isset($_SESSION["nombres"])) {
  Redirect('http://hoteleshesperia.com.ve/HesperiaQuality/pages/login.html', false);
  exit();
}
include_once('../include/databases.php'); 

$sql="SELECT tipo_encuesta, id from hesperia_v2_enc_tipo_encuenta where codificacion = '".$_GET["enc"]."' ";
$result=QUERYBD($sql,$hostname,$user,$password,$db_name);
if ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
    $tipo_encuesta = $row["tipo_encuesta"];
    $id_tipo_enc = $row["id"];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hesperia Quality</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hesperia Quality</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <div>
                                <p>
                                     <strong>Status General</strong>
                                </p>
                                    
                            </div>
                        </li>
                        <li class="divider"></li>
                        <?php
                            if($_SESSION["tipo_acceso"] == 1){
                                echo '
                                <li>
                                    <a href="#">
                                        <div>
                                            <p>
                                                <strong>Hesperia WTC Valencia</strong>
                                                <span class="pull-right text-muted">4 / 10</span>
                                            </p>
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="divider"></li>
                                ';
                            }
                            if($_SESSION["tipo_acceso"] == 2){
                                echo '
                                <li>
                                    <a href="#">
                                        <div>
                                            <p>
                                                <strong>Hesperia Isla Margarita</strong>
                                                <span class="pull-right text-muted">2 / 10</span>
                                            </p>
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                ';
                            }
                            if($_SESSION["tipo_acceso"] == 3){
                                echo '
                                <li>
                                    <a href="#">
                                        <div>
                                            <p>
                                                <strong>Hesperia Playa El Agua</strong>
                                                <span class="pull-right text-muted">6 / 10</span>
                                            </p>
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                ';
                            }
                            if($_SESSION["tipo_acceso"] == 4){
                                echo '
                                <li>
                                    <a href="#">
                                        <div>
                                            <p>
                                                <strong>Hesperia Edén Club</strong>
                                                <span class="pull-right text-muted">8 / 10</span>
                                            </p>
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                ';
                            }
                        ?>
                        
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Hoteles<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                                               
                                <?php
                                if($_SESSION["tipo_acceso"] == 1){
                                    echo '
                                    <li>
                                        <a href="#">Urbanos <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Hesperia WTC Valencia</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                    ';
                                }

                                if($_SESSION["tipo_acceso"] == 2){
                                    echo '
                                    <li>
                                        <a href="#">De Playa <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Hesperia Isla Margarita</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                    ';
                                }

                                if($_SESSION["tipo_acceso"] == 3){
                                    echo '
                                    <li>
                                        <a href="#">De Playa <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Hesperia Playa El Agua</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                    ';
                                }

                                if($_SESSION["tipo_acceso"] == 4){
                                    echo '
                                    <li>
                                        <a href="#">De Playa <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Hesperia Edén Club</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                    ';
                                }

                                if($_SESSION["tipo_acceso"] == 0){
                                    echo '
                                    <li>
                                        <a href="#">Urbanos <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Hesperia WTC Valencia</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                    <li>
                                        <a href="#">De Playa <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Hesperia Isla Margarita</a>
                                            </li>
                                            <li>
                                                <a href="#">Hesperia Playa El Agua</a>
                                            </li>
                                            <li>
                                                <a href="#">Hesperia Edén Club</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    </li>
                                    ';
                                }
                                ?>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Nueva encuesta</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" method="post" name="formNuevaEncuesta" id="formNuevaEncuesta" >
                    <fieldset>
                        <legend>Datos generales</legend>
                        <div class="form-group col-md-4">
                            <label>N°</label>
                            <input class="form-control" placeholder="N° Encuesta" name="no_enc" id="no_enc">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Email</label>
                            <input class="form-control" placeholder="Indique Email" name="email" id="email">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Fecha</label>
                           <input class="form-control" placeholder="DD-MM-AAAA" name="fecha" id="fecha">
                        </div>

                        <div class="form-group col-md-4">
                            <label>N° Habitación</label>
                            <input class="form-control" placeholder="Ejemplo. 23" name="no_hab" id="no_hab">
                        </div>

                        <div class="form-group col-md-4">
                            <label>Sexo</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="">Seleccione:</option>
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Edad</label>
                            <select class="form-control" name="edad" id="edad">
                                <option value="">Seleccione:</option>
                                <option value="18-24">18-24</option>
                                <option value="25-34">25-34</option>
                                <option value="35-44">35-44</option>
                                <option value="45-54">45-54</option>
                                <option value="55-64">55-64</option>
                                <option value="+65">+65</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>País residencia</label>
                            <input class="form-control" placeholder="Indique páis de residencia" name="pais" id="pais">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Ciudad de residencia</label>
                            <input class="form-control" placeholder="Indique ciudad de residencia" name="ciudad" id="ciudad">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Motivo de visita</label>
                            <select class="form-control" name="motivo" id="motivo">
                                <option value="">Seleccione:</option>
                                <option value="Trabajo">Trabajo</option>
                                <option value="Evento social">Evento social</option>
                                <option value="Evento corporativo">Evento corporativo</option>
                                <option value="Vacaciones">Vacaciones</option>
                                <option value="Paquete promocional">Paquete promocional</option>
                            </select>
                        </div>
                    </fieldset> 
                    
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php
                                    echo 'Hesperia Quality - '.utf8_encode($tipo_encuesta);
                                ?>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover">

                                            <?php
                                            echo ($_SESSION["tipo_acceso"]."-".$_GET["enc"]);
                                            echo "<input type='hidden' id='id_volver' value='".$_SESSION["tipo_acceso"]."'>";
                                            if($_SESSION["tipo_acceso"] == 1){
                                                $sql="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
                                                 hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HUB'
                                                  and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
                                                 (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c 
                                                 where c.codificacion = '".$_GET["enc"]."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc";
                                            }else{
                                                $sql="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
                                                 hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HPB'
                                                 and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
                                                  (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, 
                                                  hesperia_v2_enc_tipo_encuenta c where c.codificacion = '".$_GET["enc"]."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc";
                                            }
                                            
                                            echo '<input type="hidden" name="enc" id="enc" value="'.$_GET["enc"].'" />';

                                            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                                            $i = 1;
                                            while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                                                echo'<input type="hidden" name="id_enc'.$i.'" value="'.$row["id_enc"].'" />
                                                    <input type="hidden" name="preg'.$i.'" value="'.$row["id"].'" />
                                                    <tr>
                                                        <td>'.$i.'-&nbsp;'.$row["pregunta"].'</td>
                                                        <td>
                                                            <input type="hidden" name="evalua'.$i.'" value="" />
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="10" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-01.jpg" width="20" heigth="20" title="Excelente"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="8" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-02.jpg" width="20" heigth="20" title="Bueno"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="6" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-03.jpg" width="20" heigth="20" title="Regular"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="4" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-04.jpg" width="20" heigth="20" title="Malo"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="2" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-05.jpg" width="20" heigth="20" title="Muy Malo"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="0" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-06.jpg" width="20" heigth="20" title="No sabe / No aplica"/>
                                                        </td>

                                                    </tr>
                                                ';
                                                $i++;
                                            }
                                            $i--;
                                            echo '<input type="hidden" name="count_enc" value="'.$i.'" id="count_enc"/>
                                            <input type="hidden" name="id_hotel" value="'.$_SESSION["tipo_acceso"].'" id="id_hotel"/>
                                            <input type="hidden" name="id_tipo_enc" value="'.$id_tipo_enc.'" id="id_tipo_enc"/>
                                            ';
                                            ?>
                                    </table>
                                    <div class="row">
                                    <?php
                                    $sql=sprintf("SELECT (a.id) as id, pregunta, (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'CON' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc");
                                    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                                    $i = 0;
                                    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                        echo '
                                        <input type="hidden" name="id_enc_con'.$i.'" id="id_enc_con'.$i.'" value="'.$row["id_enc"].'"/>
                                        <input type="hidden" name="id_con'.$i.'" id="id_con'.$i.'" value="'.$row["id"].'"/>
                                        
                                        <div class="col-lg-8">'.utf8_encode($row["pregunta"]).'</div>
                                        <div class="col-lg-4">
                                            <select name="conclu'.$i.'" id="conclu'.$i.'" class="form-control">
                                                <option value="-1" >Seleccione:</option>
                                                <option value="4" >Definitivamente si</option>
                                                <option value="3" >Probablemente si</option>
                                                <option value="2" >No lo se</option>
                                                <option value="1" >No lo haría</option>
                                            </select>
                                        </div>';
                                        $i++;
                                    }

                                    echo '<input type="hidden" name="count_con" id="count_con" value="'.$i.'"/>';

                                    ?>
                                    </div>
                                </div>
                                <!-- /.table-responsive -->
                                
                            </div>

                            <div id="informacionEnc"></div>
                            <!-- /.panel-body -->
                            <button type="submit" class="btn btn-primary" id="submitEncuesta2">Registrar encuesta</button>
                        </div>
                        <!-- /.panel -->
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="http://hoteleshesperia.com.ve/HesperiaQuality/js/quality.js"></script>
    <script type="text/javascript" src="../js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-datepicker.css">
    <script type="text/javascript">
$(document).ready(function(){

    $('#fecha').datepicker({
    format: 'dd-mm-yyyy',
    startDate: '-1d',
    endDate: '+0d'
    });
});
</script>

</body>

</html>
