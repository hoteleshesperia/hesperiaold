<?php
session_start();
include_once('../include/funciones_principales.php');
if (!isset($_SESSION["nombres"])) {
  Redirect('https://hoteleshesperia.com.ve/HesperiaQuality/pages/login.php', false);
  exit();
}

include_once('../include/databases.php');

$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hesperia Quality</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hesperia Quality</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                 <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">

                        <li><a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?m=cerrarSesion"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>

                        <?php

                        //  verMenu($_SESSION["tipo_acceso"]);

                        ?>



                        <li>
                            <a href="#"><i class="fa fa-paper-plane"></i> Encuestas por email<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                              <?php
                              if ($_SESSION["tipo_acceso"]!="99") {
                               ?>
                              <li>
                                <a  href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=enviarmail">
                                  <i class="fa fa-users"></i> Enviar por lote
                                </a>
                              </li>
                              <li>
                                <a  href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=enviarmailindv">
                                  <i class="fa fa-envelope-o"></i> Enviar individualmente
                                </a>
                              </li>
                              <?php
                              }
                               ?>
                              <li>
                                <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php?&m=reportelote">
                                  <i class="fa fa-list-alt"></i> Reporte de envio por lote
                                </a>
                              </li>

                        </li>



                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <?php
           // verThumbsIndex($_SESSION["tipo_acceso"]);
             ?>

             <?php
             if (isset($_GET["m"])) {
                $modulo = $_GET["m"];
              }else{
                $modulo="";
              }

              switch ($modulo) {
                  case '':
                     // if ($_SESSION["tipo_acceso"]==5) {
                        include_once("tabla_reporte.php");
                     // }
                      break;

                  case 'enviarmail':
                     // if ($_SESSION["tipo_acceso"]==5) {
                        include_once("panel_enviar_mail.php");
                     // }
                      break;
                  case 'enviarmailindv':
                     // if ($_SESSION["tipo_acceso"]==5) {
                        include_once("panel_enviar_mail_especifico.php");
                     // }
                    break;

                  case 'reportelote':
                     // if ($_SESSION["tipo_acceso"]==5) {
                        include_once("panel_reporte_enviados.php");
                     // }
                    break;

                  case 'listaencuestas':
                      if ($_SESSION["tipo_acceso"]>0 && $_SESSION["tipo_acceso"]<=5) {
                          include_once("lista_encuestas.php");
                      }
                      break;

                  case 'nuevaencuesta':
                          if (isset($_GET["enc"])) {
                            include_once("form_encuesta.php");
                          }
                     // if ($_SESSION["tipo_acceso"]>0 && $_SESSION["tipo_acceso"]<=5 && isset($_GET["enc"])) {

                      //}
                      break;

                  case 'cerrarSesion':
                      session_destroy();
                      echo "<script>location.reload()</script>";
                      break;
                  default:
                      # code...
                      break;
              }

              ?>



        </div>
        <!-- /#page-wrapper -->
        <div class="modal fade " tabindex="-1" role="dialog" id="modal1">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Resumen</h4>
              </div>
              <div class="modal-body" id="modal-body">

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <!--DESCOMENTAR PARA USAR CHARTS-->
    <!--<script src="../bower_components/morrisjs/morris.min.js"></script>-->
    <!--<script src="../js/morris-data.js"></script>-->

    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../bower_components/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/Buttons-1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/Stuk-jszip-ec9505e/dist/jszip.min.js"></script>

    <!--C:\xampp\htdocs\hh3\HesperiaQuality\bower_components\Stuk-jszip-ec9505e\dist
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>-->
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/pdfmake-master/build/pdfmake.min.js"></script>

    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/Buttons-1.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/Buttons-1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/Buttons-1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/Buttons-1.2.2/js/buttons.print.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/pdfmake-master/build/vfs_fonts.js"></script>
    <!--
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <script type="text/javascript" src="../js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/envio_encuestas.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-datepicker.css">

    <script type="text/javascript">
    $(document).ready(function(){

        $('.fechas').datepicker({
        format: 'dd-mm-yyyy',
        endDate: 'today'
        });


        //TABLA GLOBAL PARA LOS RESULTADOS DEL FILTRO PRINCIPAL
       tablaglobal = $("#dataTables-example").DataTable( {
          dom: 'Bfrtip',
          buttons: [
                'excel', 'pdf', 'print'
              ]
          });

       tablamodalfiltros =  $("#dataTables-example2").DataTable( {
          dom: 'Bfrtip',
          buttons: [
                'excel', 'pdf', 'print'
              ]
          });

       tablaReportesLote = $("#dataTableEspecificos").DataTable( {
          dom: 'Bfrtip',
          buttons: [
                'excel', 'pdf', 'print'
              ]
          });

    });

    </script>
    <script type="text/javascript" src="../js/filtros.js"></script>
    <script type="text/javascript" src="../js/filtros-reporte-lote.js"></script>
</body>

</html>
