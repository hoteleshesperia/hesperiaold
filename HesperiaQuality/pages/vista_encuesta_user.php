<?php
include_once('../include/databases.php'); 
?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hesperia Quality</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/js/clipboard.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <div id="wrapper">

        <!-- Page Content -->
        <?php 
            if (isset($_GET["enc"]) && strlen($_GET["enc"])==23) {
                $enlace = $_GET["enc"];
                $query="SELECT b.codificacion, b.id, b.tipo_encuesta, a.id_hotel, a.email from hesperia_v2_encuestas_mail a 
                inner join hesperia_v2_enc_tipo_encuenta b on (a.id_tipo_enc = b.id) where a.enlace = '$enlace'
                and a.status=0 ";
                
                $result=$mysqli->query($query);

                if ($result->num_rows >0) {
                    $row = mysqli_fetch_assoc($result);
                    $tipo_encuesta = $row["tipo_encuesta"];
                    $id_tipo_enc = $row["id"];
                    $codificacion = $row["codificacion"];
                    $id_hotel = $row["id_hotel"];
                    $email=$row["email"];
                    $mysqli->close();
                }else{
                    echo("
                    <div class='container'>
                    <br><br>
                    <div class='alert alert-danger'>
                        Este enlace ha caducado o no existe. <a href='https://hoteleshesperia.com.ve'
                         class='alert-link'>ir a Hoteleshesperia.com.ve</a>.
                    </div>
                    </div>");
                    $mysqli->close();
                    exit(); 
                }
               
            }else{
                echo("
                    <div class='container'>
                    <br><br>
                    <div class='alert alert-danger'>
                        Este enlace ha caducado o no existe. <a href='https://hoteleshesperia.com.ve'
                         class='alert-link'>ir a Hoteleshesperia.com.ve</a>.
                    </div>
                    </div>");
                 $mysqli->close();
                exit();
            }
         ?>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">Hesperia Quality</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" method="post" name="formNuevaEncuesta" id="formNuevaEncuesta" >
                    <fieldset>
                        <legend>Datos generales</legend>
                        <div class="form-group col-md-4">
                            <label>Email</label>
                            <input class="form-control" value=<?php echo "'$email'"; ?> name="email" 
                            id="email" readonly="readonly">
                        </div>


                        <div class="form-group col-md-4">
                            <label>N° Habitación</label>
                            <input class="form-control" placeholder="Ejemplo. 23" name="no_hab" id="no_hab">
                        </div>

                        <div class="form-group col-md-4">
                            <label>Sexo</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Edad</label>
                            <select class="form-control" name="edad" id="edad">
                                <option value="18-24">18-24</option>
                                <option value="25-34">25-34</option>
                                <option value="35-44">35-44</option>
                                <option value="45-54">45-54</option>
                                <option value="55-64">55-64</option>
                                <option value="+65">+65</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>País residencia</label>
                            <input class="form-control" placeholder="Indique páis de residencia" name="pais" id="pais">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Ciudad de residencia</label>
                            <input class="form-control" placeholder="Indique ciudad de residencia" name="ciudad" id="ciudad">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Motivo de visita</label>
                            <select class="form-control" name="motivo" id="motivo">
                                <option value="Trabajo">Trabajo</option>
                                <option value="Evento social">Evento social</option>
                                <option value="Evento corporativo">Evento corporativo</option>
                                <option value="Vacaciones">Vacaciones</option>
                                <option value="Paquete promocional">Paquete promocional</option>
                            </select>
                        </div>
                    </fieldset> 
                    
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php
                                    echo 'Hesperia Quality - '.$tipo_encuesta;
                                ?>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover">

                                            <?php
                                           // echo ($_SESSION["tipo_acceso"]."-".substr($codificacion, 0, 2));
                                           // echo "<input type='hidden' id='id_volver' value='".$_SESSION["tipo_acceso"]."'>";
                                            if(substr($codificacion, 0, 2) == "HU"){
                                                $sql="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
                                                 hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HUB'
                                                  and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
                                                 (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c 
                                                 where c.codificacion = '".$codificacion."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc";
                                            }else{
                                                $sql="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
                                                 hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HPB'
                                                 and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
                                                  (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, 
                                                  hesperia_v2_enc_tipo_encuenta c where c.codificacion = '".$codificacion."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc";
                                            }
                                            
                                            echo '<input type="hidden" name="enc" id="enc" value="'.$codificacion.'" />';

                                            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                                            $i = 1;
                                            while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                                                echo'<input type="hidden" name="id_enc'.$i.'" value="'.$row["id_enc"].'" />
                                                    <input type="hidden" name="preg'.$i.'" value="'.$row["id"].'" />
                                                    <tr>
                                                        <td>'.$i.'-&nbsp;'.$row["pregunta"].'</td>
                                                        <td>
                                                            <input type="hidden" name="evalua'.$i.'" value="" />
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="10" /><img src="https://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-01.jpg" width="20" heigth="20" title="Excelente"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="8" /><img src="https://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-02.jpg" width="20" heigth="20" title="Bueno"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="6" /><img src="https://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-03.jpg" width="20" heigth="20" title="Regular"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="4" /><img src="https://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-04.jpg" width="20" heigth="20" title="Malo"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="2" /><img src="https://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-05.jpg" width="20" heigth="20" title="Muy Malo"/>
                                                            &nbsp;
                                                            <input type="radio" name="evalua'.$i.'" value="0" /><img src="https://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-06.jpg" width="20" heigth="20" title="No sabe / No aplica"/>
                                                        </td>

                                                    </tr>
                                                ';
                                                $i++;
                                            }
                                            $i--;
                                            echo '<input type="hidden" name="count_enc" value="'.$i.'" id="count_enc"/>
                                            <input type="hidden" name="id_hotel" value="'.$id_hotel.'" id="id_hotel"/>
                                            <input type="hidden" name="id_tipo_enc" value="'.$id_tipo_enc.'" id="id_tipo_enc"/>
                                            ';
                                            ?>
                                    </table>
                                    <div class="row">
                                    <?php
                                    $sql=sprintf("SELECT (a.id) as id, pregunta, (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'CON' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc");
                                    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                                    $i = 0;
                                    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                        echo '
                                        <input type="hidden" name="id_enc_con'.$i.'" id="id_enc_con'.$i.'" value="'.$row["id_enc"].'"/>
                                        <input type="hidden" name="id_con'.$i.'" id="id_con'.$i.'" value="'.$row["id"].'"/>
                                        
                                        <div class="col-lg-8">'.utf8_encode($row["pregunta"]).'</div>
                                        <div class="col-lg-4">
                                            <select name="conclu'.$i.'" id="conclu'.$i.'" class="form-control">
                                                <option value="-1" >Seleccione:</option>
                                                <option value="4" >Definitivamente si</option>
                                                <option value="3" >Probablemente si</option>
                                                <option value="2" >No lo se</option>
                                                <option value="1" >No lo haría</option>
                                            </select>
                                        </div>';
                                        $i++;
                                    }

                                    echo '<input type="hidden" name="count_con" id="count_con" value="'.$i.'"/>
                                          <input type="hidden" name="ind_mail" value="S">
                                          <input type="hidden" name="enlace" value="'.$enlace.'">
                                          ';

                                    ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comentarios</label>
                                        <textarea rows="3" id="comentarios" name="comentarios" class="form-control"></textarea>
                                        <button type="button" data-clipboard-target="#comentarios" id="clipboard" class="btn btn-info" >Copiar al portapapeles</button>
                                    </div>
                                </div>
                                <!-- /.table-responsive -->
                                
                            </div>

                            <div id="informacionEnc"></div>
                            <!-- /.panel-body -->
                            <p class="text-center"><button type="submit" class="btn btn-primary" data-clipboard-target="#comentarios" id="submitEncuesta">Registrar encuesta</button></p>
                        </div>
                        <!-- /.panel -->
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/js/quality.js"></script>
    <script type="text/javascript" src="../js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-datepicker.css">
    <script type="text/javascript">
    $(document).ready(function(){
        $('#fecha').datepicker({
        format: 'dd-mm-yyyy',
        startDate: '-1d',
        endDate: '+0d'
        });
    });
    </script>

</body>

</html>
