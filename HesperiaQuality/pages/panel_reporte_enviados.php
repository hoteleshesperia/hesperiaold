
<?php
if ($_SESSION["tipo_acceso"]!=99 && $_SESSION["tipo_acceso"]>0) {
    $disabled = " disabled= 'disabled'";
}

 ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Encuestas enviadas por lote</h1>
    </div>
    <div class="col-lg-12">
        <fieldset>
                <div class="form-group col-md-4 ">
                    <label>Desde</label>
                    <input class="form-control fechas input-sm" name="fecha" id="fecha_desde">
                </div>
                <div class="form-group col-md-4">
                    <label>Hasta</label>
                    <input class="form-control fechas input-sm"  name="fecha" id="fecha_hasta">
                </div>

                <div class="form-group col-md-4">
                    <label>Hotel</label>

                    <select class="form-control input-sm" name="hotel" id="hotel" <?php echo ($disabled); ?>>

                        <?php cargarSelectHoteles($hostname,$user,$password,$db_name, "S")?>
                    </select>

                </div>

                <div class="form-group col-md-12">
                    <p class="text-center">
                        <button type="button" class="btn btn-primary" id="btn-filtro-reporte-lote">Filtrar</button>
                    </p>
                </div>

        </fieldset>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Registro de enviados</div>
                <table class="table" id="dataTableEspecificos">
                    <thead>

                            <th>Tipo de Encuesta</th>
                            <th>Codificación</th>
                            <th>Total de Encuestas</th>
                            <th>Respondidas</th>
                            <th>Pendientes</th>
                            <th>% Respondidas</th>
                    </thead>
                    <tbody id="bodyTableLote">

                    </tbody>
                    <?php //resumen_enviados_areas($mysqli); ?>

                </table>
            </div>
        </div>
</div>
