<?php 

$row = cargar_info_encuesta($_GET["enc"], $mysqli);
if ($row!=null) {
	$tipo_encuesta = $row["tipo_encuesta"];
}
 ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Nueva encuesta</h1>
    </div>
                <!-- /.col-lg-12 -->
</div>
            <!-- /.row -->
<div class="row">
    <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" method="post" name="formNuevaEncuesta" id="formNuevaEncuesta" >
        <fieldset>
            <legend>Datos generales</legend>
            <div class="form-group col-md-4">
                <label>N°</label>
                <input class="form-control" placeholder="N° Encuesta" name="no_enc" id="no_enc">
            </div>
            <div class="form-group col-md-4">
                <label>Email</label>
                <input class="form-control" placeholder="Indique Email" name="email" id="email">
            </div>
            <div class="form-group col-md-4">
                <label>Fecha</label>
               <input class="form-control" placeholder="DD-MM-AAAA" name="fecha" id="fecha">
            </div>

                        <div class="form-group col-md-4">
                            <label>N° Habitación</label>
                            <input class="form-control" placeholder="Ejemplo. 23" name="no_hab" id="no_hab">
                        </div>

            <div class="form-group col-md-4">
                <label>Sexo</label>
                <select class="form-control" name="sexo" id="sexo">
                    <option value="">Seleccione:</option>
                    <option value="F">Femenino</option>
                    <option value="M">Masculino</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label>Edad</label>
                <select class="form-control" name="edad" id="edad">
                    <option value="">Seleccione:</option>
                    <option value="18-24">18-24</option>
                    <option value="25-34">25-34</option>
                    <option value="35-44">35-44</option>
                    <option value="45-54">45-54</option>
                    <option value="55-64">55-64</option>
                    <option value="+65">+65</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label>País residencia</label>
                <input class="form-control" placeholder="Indique páis de residencia" name="pais" id="pais">
            </div>
            <div class="form-group col-md-4">
                <label>Ciudad de residencia</label>
                <input class="form-control" placeholder="Indique ciudad de residencia" name="ciudad" id="ciudad">
            </div>
            <div class="form-group col-md-4">
                <label>Motivo de visita</label>
                <select class="form-control" name="motivo" id="motivo">
                    <option value="">Seleccione:</option>
                    <option value="Trabajo">Trabajo</option>
                    <option value="Evento social">Evento social</option>
                    <option value="Evento corporativo">Evento corporativo</option>
                    <option value="Vacaciones">Vacaciones</option>
                    <option value="Paquete promocional">Paquete promocional</option>
                </select>
            </div>
        </fieldset> 
                    
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php
                        echo 'Hesperia Quality - '.utf8_encode($tipo_encuesta);
                    ?>
                </div>
                            <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">

                        <?php
                        //echo ($_SESSION["tipo_acceso"]."-".$_GET["enc"]);
                        echo "<input type='hidden' id='id_volver' value='".$_SESSION["tipo_acceso"]."'>";
                        if($_SESSION["tipo_acceso"] == 1){
                            $sql="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
                             hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HUB'
                              and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
                             (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c 
                             where c.codificacion = '".$_GET["enc"]."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc";
                        }else{
                            $sql="SELECT (a.id) as id, pregunta, orden, (c.id) as id_enc from hesperia_v2_enc_preguntas a,
                             hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'HPB'
                             and b.id_encuesta = c.id and a.id = b.id_pregunta union SELECT (a.id) as id, pregunta, orden,
                              (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, 
                              hesperia_v2_enc_tipo_encuenta c where c.codificacion = '".$_GET["enc"]."' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc";
                        }
                                            
                        echo '<input type="hidden" name="enc" id="enc" value="'.$_GET["enc"].'" />';

                        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                        $i = 1;
                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                            echo'<input type="hidden" name="id_enc'.$i.'" value="'.$row["id_enc"].'" />
                                <input type="hidden" name="preg'.$i.'" value="'.$row["id"].'" />
                                <tr>
                                    <td>'.$i.'-&nbsp;'.$row["pregunta"].'</td>
                                    <td>
                                        <input type="hidden" name="evalua'.$i.'" value="" />
                                        &nbsp;
                                        <input type="radio" name="evalua'.$i.'" value="10" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-01.jpg" width="20" heigth="20" title="Excelente"/>
                                        &nbsp;
                                        <input type="radio" name="evalua'.$i.'" value="8" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-02.jpg" width="20" heigth="20" title="Bueno"/>
                                        &nbsp;
                                        <input type="radio" name="evalua'.$i.'" value="6" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-03.jpg" width="20" heigth="20" title="Regular"/>
                                        &nbsp;
                                        <input type="radio" name="evalua'.$i.'" value="4" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-04.jpg" width="20" heigth="20" title="Malo"/>
                                        &nbsp;
                                        <input type="radio" name="evalua'.$i.'" value="2" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-05.jpg" width="20" heigth="20" title="Muy Malo"/>
                                        &nbsp;
                                        <input type="radio" name="evalua'.$i.'" value="0" /><img src="http://hoteleshesperia.com.ve/HesperiaQuality/img/Cariras Px-06.jpg" width="20" heigth="20" title="No sabe / No aplica"/>
                                    </td>

                                </tr>
                            ';
                            $i++;
                        }
                        $i--;
                        echo '<input type="hidden" name="count_enc" value="'.$i.'" id="count_enc"/>
                        <input type="hidden" name="id_hotel" value="'.$_SESSION["tipo_acceso"].'" id="id_hotel"/>
                        <input type="hidden" name="id_tipo_enc" value="'.$row["id_tipo_encuesta"].'" id="id_tipo_enc"/>
                        ';
                        ?>
                        </table>
                        <div class="row">
                        <?php
                        $sql=sprintf("SELECT (a.id) as id, pregunta, (c.id) as id_enc from hesperia_v2_enc_preguntas a, hesperia_v2_enc_preg_encuesta b, hesperia_v2_enc_tipo_encuenta c where c.codificacion = 'CON' and b.id_encuesta = c.id and a.id = b.id_pregunta order by orden asc");
                        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                        $i = 0;
                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                        	echo '
                        	<input type="hidden" name="id_enc_con'.$i.'" id="id_enc_con'.$i.'" value="'.$row["id_enc"].'"/>
                        	<input type="hidden" name="id_con'.$i.'" id="id_con'.$i.'" value="'.$row["id"].'"/>
                                        
                        	<div class="col-lg-8">'.utf8_encode($row["pregunta"]).'</div>
                        	<div class="col-lg-4">
                            	<select name="conclu'.$i.'" id="conclu'.$i.'" class="form-control">
                                	<option value="" >Seleccione:</option>
                                	<option value="4" >Definitivamente si</option>
                                	<option value="3" >Probablemente si</option>
                                	<option value="2" >No lo se</option>
                                	<option value="1" >No lo haría</option>
                            	</select>
                        	</div>';
                        	$i++;
                    	}

                        echo '<input type="hidden" name="count_con" id="count_con" value="'.$i.'"/>';

                        ?>
                        </div>
                    </div>
                                <!-- /.table-responsive -->
                                
                </div>

                <div id="informacionEnc"></div>
                <!-- /.panel-body -->
                <button type="submit" class="btn btn-primary" id="submitEncuesta">Registrar encuesta</button>
            </div>
                     
        </div>
    </form>
                
</div>
           