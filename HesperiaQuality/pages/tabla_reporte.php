<?php
if ($_SESSION["tipo_acceso"]!=99 && $_SESSION["tipo_acceso"]>0) {
    $disabled = " disabled= 'disabled'";
}
 ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Inicio</h1>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <fieldset>
                <div class="form-group col-md-4 ">
                    <label>Desde</label>
                    <input class="form-control fechas input-sm" name="fecha" id="fecha_desde">
                </div>
                <div class="form-group col-md-4">
                    <label>Hasta</label>
                    <input class="form-control fechas input-sm"  name="fecha" id="fecha_hasta">
                </div>

                <div class="form-group col-md-4 ">
                    <label>Sexo</label>
                    <select class="form-control input-sm" name="sexo" id="sexo">
                        <option value="-1">Todos</option>
                        <option value="F">Femenino</option>
                        <option value="M">Masculino</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Edad</label>
                    <select class="form-control input-sm" name="edad" id="edad">
                        <option value="">Todos</option>
                        <option value="18-24">18-24</option>
                        <option value="25-34">25-34</option>
                        <option value="35-44">35-44</option>
                        <option value="45-54">45-54</option>
                        <option value="55-64">55-64</option>
                        <option value="+65">+65</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label>Hotel</label>

                    <select class="form-control input-sm" name="hotel" id="hotel" <?php echo ($disabled); ?>>

                        <?php cargarSelectHoteles($hostname,$user,$password,$db_name, "S")?>
                    </select>

                </div>

                <div class="form-group col-md-4">
                    <label>Tipo de encuesta</label>

                    <select class="form-control input-sm" name="forma_encuesta" id="forma_encuesta">
                        <option value="-1">Todo</option>
                        <option value="N">En papel</option>
                        <option value="S">Via Email</option>
                    </select>

                </div>


                       <p class="text-center">
                          <button type="button" class="btn btn-primary" id="btn-filtro">Buscar</button>
                       </p>


            </fieldset>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Hesperia Quality
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Área</th>
                                <th>Cant. Respondidas</th>
                                <th>Sumatoria</th>
                                <th>Promedio</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTable">
                        <?php //verPromediosEncuestas($mysqli); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

            </div>
            <div class="panel-footer">
                <?php //promedioPreguntaEspecifica($hostname,$user,$password,$db_name,"CTL")
                //testing($hostname,$user,$password,$db_name, "CTL");
                ?>
            </div>

        </div>
                    <!-- /.panel -->
    </div>
                <!-- /.col-lg-8 -->

</div>
