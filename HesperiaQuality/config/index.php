<?php
session_start();

include_once('../include/databases.php'); 

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hesperia Quality</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Hesperia Quality</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <div>
                                <p>
                                     <strong>Status General</strong>
                                </p>
                                    
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Hesperia WTC Valencia</strong>
                                        <span class="pull-right text-muted">4 / 10</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Hesperia Isla Margarita</strong>
                                        <span class="pull-right text-muted">2 / 10</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Hesperia Playa El Agua</strong>
                                        <span class="pull-right text-muted">6 / 10</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Hesperia Edén Club</strong>
                                        <span class="pull-right text-muted">8 / 10</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                        </li> -->
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Hoteles<span class="fa arrow"></span></a>
                            <!--<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul> -->
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Urbanos <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=1">Hesperia WTC Valencia</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#">De Playa <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=2">Hesperia Isla Margarita</a>
                                        </li>
                                        <li>
                                            <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=3">Hesperia Playa El Agua</a>
                                        </li>
                                        <li>
                                            <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=4">Hesperia Edén Club</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Inicio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Hesperia WTC Valencia</div>
                                </div>
                            </div>
                        </div>
                        <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=1">
                            <div class="panel-footer">
                                <span class="pull-left">Nueva Encuesta</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Hesperia Isla Margarita</div>
                                </div>
                            </div>
                        </div>
                        <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=2">
                            <div class="panel-footer">
                                <span class="pull-left">Nueva Encuesta</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Hesperia Playa El Agua</div>
                                </div>
                            </div>
                        </div>
                        <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=3">
                            <div class="panel-footer">
                                <span class="pull-left">Nueva Encuesta</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>Hesperia Edén Club</div>
                                </div>
                            </div>
                        </div>
                        <a href="https://hoteleshesperia.com.ve/HesperiaQuality/pages/index_encuesta.php?id=4">
                            <div class="panel-footer">
                                <span class="pull-left">Nueva Encuesta</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Hesperia Quality
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Área</th>
                                            <th>Hesperia WTC Valencia</th>
                                            <th>Hesperia Isla Margarita</th>
                                            <th>Hesperia Playa El Agua</th>
                                            <th>Hesperia Edén Club</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($_SESSION["tipo_acceso"] == 1){
                                            $sql=sprintf("SELECT * FROM hesperia_v2_enc_tipo_encuenta WHERE codificacion like '%s%%' AND codificacion not in ('HUB', 'CON')",
                                                mysql_real_escape_string("HU"));
                                        }else{
                                            $sql=sprintf("SELECT * FROM hesperia_v2_enc_tipo_encuenta WHERE codificacion like '%s%%' AND codificacion not in ('HPB', 'CON')",
                                                mysql_real_escape_string("HP"));
                                        }                                        

                                        $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

                                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                                            $sqlResp=sprintf("SELECT count(id_tipo_enc) as total_encuesta FROM hesperia_v2_enc_respuesta WHERE id_tipo_enc = '%s'",
                                                mysql_real_escape_string($row["id"]));

                                            $resultResp=QUERYBD($sqlResp,$hostname,$user,$password,$db_name);
                                            $rowResp=mysqli_fetch_array($resultResp,MYSQLI_ASSOC);

                                            echo'
                                                <tr class="gradeX">
                                                    <td><strong>'.utf8_encode($row["tipo_encuesta"]).'</strong></td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                    <td>'.$rowResp["total_encuesta"].'</td>
                                                </tr>
                                            ';
                                        }
                                        ?>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <fieldset>
                        <legend>Párametros de Reporting</legend>
                        <div class="form-group col-md-6">
                            <label>Desde</label>
                            <input class="form-control" placeholder="2016-04-27" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Hasta</label>
                            <input class="form-control" placeholder="2016-04-27" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Área</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="">Todas:</option>
                                <?php
                                $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
                                while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
                                    echo '<option value="">'.utf8_encode($row["tipo_encuesta"]).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Sexo</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="">Seleccione:</option>
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Edad</label>
                            <select class="form-control" name="edad" id="edad">
                                <option value="">Seleccione:</option>
                                <option value="18-24">18-24</option>
                                <option value="25-34">25-34</option>
                                <option value="35-44">35-44</option>
                                <option value="45-54">45-54</option>
                                <option value="55-64">55-64</option>
                                <option value="+65">+65</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>País</label>
                            <input class="form-control" placeholder="Indique país" name="pais" id="pais">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Ciudad</label>
                            <input class="form-control" placeholder="Indique ciudad" name="ciudad" id="ciudad">
                        </div>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-info btn-block">Reporting</button>
                        </div>
                        
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <fieldset>
                        <legend>Párametros de Reporting</legend>
                        <div class="form-group col-md-6">
                            <label>Desde</label>
                            <input class="form-control" placeholder="AAAA-MM-DD" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Hasta</label>
                            <input class="form-control" placeholder="AAAA-MM-DD" name="fecha" id="fecha">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Año</label>
                            <input class="form-control" placeholder="AAAA" name="fecha" id="fecha">
                        </div>
                        
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-info btn-block">Reporting</button>
                        </div>
                    </fieldset>

                </div>
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> General
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Acciones
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">General reporte en .PDF</a>
                                        </li>
                                        <li><a href="#">Enviar por correo</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">                            
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-12">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
           
            
                        

            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
