$(document).ready(function(){

	var item_data= new FormData();
	item_data.append("accion", "normal");
	item_data.append("fecha_desde", $("#fecha_desde").val());
	item_data.append("fecha_hasta", $("#fecha_hasta").val());
	item_data.append("id_hotel", $("#hotel").val());
	buscar_reporte_lote(item_data);

	$("#btn-filtro-reporte-lote").click(function(){
		var item_data= new FormData();
		item_data.append("accion", "normal");
		item_data.append("fecha_desde", $("#fecha_desde").val());
		item_data.append("fecha_hasta", $("#fecha_hasta").val());
		item_data.append("id_hotel", $("#hotel").val());
		buscar_reporte_lote(item_data);

	});



});


function buscar_reporte_lote(item_data){
		//item_data.append("cod", "CKO");
		//alert(item_data.get("id_hotel"));

		var headerPDF = getHeader("pdf", item_data);
		var headerPrint = getHeader("print", item_data);

		$.ajax({

		type:"POST",
		url:"https://hoteleshesperia.com.ve/HesperiaQuality/include/resultado_filtro_reporte_envio.php",
		data:item_data,
		contentType:false,
		processData:false,
		cache:false,
		success: function(response){
			if(response){
					tablaReportesLote.destroy();
					$("#bodyTableLote").html(response);
					tablaReportesLote = $("#dataTableEspecificos").DataTable( {
			          dom: 'Bfrtip',
			          buttons: [
			                'excel',
			                {
			                	extend: 'pdfHtml5',
			                	message: ''+headerPDF
			                },
			                {
				                extend: 'print',
				                message: ''+headerPrint
				            }
			              ]
			          });


				//location.reload();
			}else{
				$("#bodyTableLote").html("");
				//location.reload();
			}
		},
		error: function(response){
			alert("Ha ocurrido un error, vuelva a intentarlo2");
			//location.reload();
			},


		});
}
