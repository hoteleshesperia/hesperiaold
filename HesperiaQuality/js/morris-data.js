$(function() {

    

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: 'Enero',
            a: 100,
            b: 90,
            c: 85,
            d: 49
        }, {
            y: 'Febrero',
            a: 75,
            b: 65,
            c: 85,
            d: 49
        }, {
            y: 'Marzo',
            a: 50,
            b: 40,
            c: 85,
            d: 49
        }, {
            y: 'Abril',
            a: 75,
            b: 65,
            c: 85,
            d: 49
        }],
        xkey: 'y',
        ykeys: ['a', 'b', 'c', 'd'],
        labels: ['Hesperia WTC Valencia', 'Hesperia Isla Margarita', 'Hesperia Playa El Agua', 'Hesperia Edén Club'],
        hideHover: 'auto',
        resize: true
    });

});
