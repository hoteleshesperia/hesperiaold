$(document).ready(function(){

    var clipboard = new Clipboard('#clipboard, #submitEncuesta');

	$("#btnAccess").click(function(){

		if(validarFormAcceso()){
			var resultado=$.ajax({
		        type:"POST",
		        data:$("#FormIngresoQuality").serialize(),
		        url:'https://hoteleshesperia.com.ve/HesperiaQuality/include/ajax_acceso.php',
		        dataType:'text',
		        async:false
		    }).responseText;

		    if(resultado == '1'){
		    	document.location.href='https://hoteleshesperia.com.ve/HesperiaQuality/pages/index.php';
		    }else{
		    	$("#InformacionAcceso").html('<div class="alert alert-danger">Usted no tiene acceso a Hesperia Quality'+resultado+'</div>');
		    }

		}

    });

    $("#submitEncuesta").click(function(){
         var form_data = new FormData($("#formNuevaEncuesta")[0]);
         var keys = form_data.keys();
         var data = form_data.entries();
         var reg = "^evalua";
         var cant_preguntas = 0;
         var acumulador=0;
         var promedio=0;
         for (var pair of form_data.entries()) {

            if (pair[0].match(reg) && pair[1]!="") {
                console.log(pair[0]+ ', ' + pair[1]);
                cant_preguntas++;  
                acumulador = parseInt(acumulador+parseInt((pair[1])));
            };
             
        }
        promedio = parseInt(acumulador/cant_preguntas);

        console.log(parseInt(acumulador/cant_preguntas)+"-"+form_data.get("id_hotel"));
        //alert("promedio:"+acumulador/cant_preguntas);
        if(validarDatosGenerales()){
            var tripadvisor="";
            if (form_data.get("id_hotel")==4) {
                tripadvisor = "https://www.tripadvisor.com.ve/UserReviewEdit-g316061-d7730156-Hesperia_Eden_Club-Margarita_Island_Coastal_Islands_Insular_Region.html";
            }else if(form_data.get("id_hotel")==3){
                tripadvisor ="https://www.tripadvisor.com.ve/UserReviewEdit-g1075652-d316888-Hesperia_Playa_El_Agua-Playa_el_Agua_Margarita_Island_Coastal_Islands_Insular_Region.html";
            }else if(form_data.get("id_hotel")==2){
                tripadvisor ="https://www.tripadvisor.com.ve/UserReviewEdit-g316061-d316990-Hesperia_Isla_Margarita-Margarita_Island_Coastal_Islands_Insular_Region.html";
            }else if(form_data.get("id_hotel")==1){
                tripadvisor ="https://www.tripadvisor.com.ve/UserReviewEdit-g316069-d2065193-Hesperia_WTC_Valencia-Valencia_Central_Region.html";
            }

            $.ajax({
	        type: "POST",
	        url: "https://hoteleshesperia.com.ve/HesperiaQuality/include/ajax_registrar_encuesta.php",
	        data: form_data,
	        contentType:false,
	        processData:false,
	        cache:false,
	        success: function(response){
	        if(response=="2" &&  promedio>=7){
	          alert("Gracias por participar en nuestra encuesta, le invitamos a compartir su opinión con los futuros"+
                " visitantes en la reconocida web de reputación hotelera Tripadvisor."+
                " Para su comodidad el comentario que ha escrito en esta encuesta se ha copiado al portapapeles");
	          window.location.href=tripadvisor;
	        }else if(response=="2"){
                alert("Gracias por participar en nuestra encuesta");
                window.location.href="https://hoteleshesperia.com.ve";
            }else if(response=="1"){
                alert("Proceso Completado con exito");
               // location.reload();
            }else{
	         alert("Ha ocurrido un error, intente nuevamente"+response);
	          }
	        }
	    });
        }
    });

    $("#submitEncuesta2").click(function(){

         var form_data = new FormData($("#formNuevaEncuesta")[0]);

        if(validarDatos()){

            $.ajax({
            type: "POST",
            url: "https://hoteleshesperia.com.ve/HesperiaQuality/include/ajax_registrar_encuesta.php",
            data: form_data,
            contentType:false,
            processData:false,
            cache:false,
            success: function(response){
            if(response=="2"){
              alert("Gracias por participar en nuestra encuesta");
              window.locationf="https://hoteleshesperia.com.ve";

            }else if(response=="1"){
                alert("Proceso Completado con exito");
                location.reload();
            }else{
             alert("Ha ocurrido un error, intente nuevamente"+response);
              }
            }
        });
        }
    });



});

function validarFormAcceso(){
	$("#InformacionAcceso").html('');

        form = document.FormIngresoQuality;
        error = '';

        if(form.email.value == '' && form.password.value == ''){
        	error = 'Indique email y clave de acceso';
        }else{
        	if(form.email.value == '' && form.password.value != ''){
        		error = 'Indique email de acceso';
        	}

        	if(form.email.value != '' && form.password.value == ''){
        		error = 'Indique clave de acceso';
        	}

        }

        if(error != ''){
        	$("#InformacionAcceso").html('<div class="alert alert-danger">'+error+'</div>');
        	return false;
        }else{
        	return true;
        }
}

function validarDatosGenerales(){

    var nohab= $("#no_hab").val();
    var conclu0 = $("#conclu0").val();
    var conclu1 = $("#conclu1").val();
    var conclu2 = $("#conclu2").val();
    var error="";
    if (!$.isNumeric(nohab)) {
        error+="<br>Escriba un N° de habitación correcto";
    };

    if (conclu0=="-1") {
        error+="<br>Por favor seleccione una respuesta para: <strong>¿Si usted regresara a este destino, volvería a hospedarse con nosotros?</strong>";

    };

    if (conclu1=="-1") {
        error+="<br>Por favor seleccione una respuesta para: <strong>¿Recomendaría usted este hotel a familiares y amigos?</strong>";

    };

    if (conclu2=="-1") {
        error+="<br>Por favor seleccione una respuesta para: <strong>¿Si usted visitara otro destino donde exista otro hotel de nuestra cadena, se hospedaría con nosotros?</strong>";

    };

    if (error!="") {
        $("#informacionEnc").html('<div class="alert alert-danger">'+error+'</div>');
        return false;
    }else{
        return true;
    }
}

function validarDatos(){
    var nohab= $("#no_hab").val();
    var noenc= $("#no_enc").val();
    var conclu0 = $("#conclu0").val();
    var conclu1 = $("#conclu1").val();
    var conclu2 = $("#conclu2").val();
    var error="";
    var fecha = $("#fecha").val();
    if (!$.isNumeric(nohab)) {
        error+="<br>Escriba un N° de habitación correcto";
    };

    if (fecha=="") {
        error+="<br>Seleccione una fecha correcta";
    };

     if (!$.isNumeric(noenc)) {
        error+="<br>Escriba un N° de encuesta correcto";
    };

    if (conclu0=="-1") {
        error+="<br>Por favor seleccione una respuesta para: <strong>¿Si usted regresara a este destino, volvería a hospedarse con nosotros?</strong>";

    };

    if (conclu1=="-1") {
        error+="<br>Por favor seleccione una respuesta para: <strong>¿Recomendaría usted este hotel a familiares y amigos?</strong>";

    };

    if (conclu2=="-1") {
        error+="<br>Por favor seleccione una respuesta para: <strong>¿Si usted visitara otro destino donde exista otro hotel de nuestra cadena, se hospedaría con nosotros?</strong>";

    };

    if (error!="") {
        $("#informacionEnc").html('<div class="alert alert-danger">'+error+'</div>');
        return false;
    }else{
        return true;
    }
}
