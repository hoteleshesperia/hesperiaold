$(document).ready(function(){
		var item_data= new FormData();
		item_data.append("id_hotel", $("#hotel").val());
		item_data.append("sexo", $("#sexo").val());
		item_data.append("edad", $("#edad").val());
		item_data.append("accion", "xyz");
		item_data.append("fecha_desde", $("#fecha_desde").val());
		item_data.append("fecha_hasta", $("#fecha_hasta").val());
		item_data.append("forma_encuesta", $("#forma_encuesta").val());
		buscar(item_data,"standard");


	$("#btn-filtro").click(function(){

		var item_data= new FormData();
		item_data.append("id_hotel", $("#hotel").val());
		item_data.append("accion", "xyz");
		item_data.append("sexo", $("#sexo").val());
		item_data.append("edad", $("#edad").val());
		item_data.append("fecha_desde", $("#fecha_desde").val());
		item_data.append("fecha_hasta", $("#fecha_hasta").val());
		item_data.append("forma_encuesta", $("#forma_encuesta").val());
		///alert($("#fecha_desde").val()+"-"+$("#fecha_hasta").val());
		buscar(item_data,"standard");

	});
	 $('#bodyTable').on("click", ".clasificaciones-item", function(e){
      	e.preventDefault();
		var item_data= new FormData();
		var cod = $(this).data("value");
		item_data.append("id_hotel", $("#hotel").val());
		item_data.append("accion", "modal");
		item_data.append("cod", cod);
		item_data.append("sexo", $("#sexo").val());
		item_data.append("edad", $("#edad").val());
		item_data.append("fecha_desde", $("#fecha_desde").val());
		item_data.append("fecha_hasta", $("#fecha_hasta").val());
		item_data.append("forma_encuesta", $("#forma_encuesta").val());
		buscar(item_data,"modal");

	});
});

function getHeader(tipo, item_data){
  if (tipo=="pdf") {
  	var header = "Filtro utilizado\n";
  	if (item_data.get("id_hotel")=="-1") {
			header+="Hotel: Todos\n";
		}else{
			header+="Hotel: "+$("#hotel :selected").text()+"\n";
		}

		if (item_data.get("sexo")=="-1") {
			header+="Sexo: Todos\n";
		}else{
			header+="Sexo: "+$("#sexo :selected").text()+"\n";
		}
		if (item_data.get("edad")=="-1") {
			header+="Edad: Todos\n";
		}else{
			header+="Edad: "+$("#edad :selected").text()+"\n";
		}

		if (item_data.get("edad")=="-1") {
			header+="Forma de encuesta: Todos\n";
		}else{
			header+="Forma de Encuesta: "+$("#forma_encuesta :selected").text();+"\n";
		}

		header+="\nPeriodo: desde( "+item_data.get("fecha_desde")+" ) - hasta( "+item_data.get("fecha_hasta")+") )\n";

	}else if("print"){
		var header = "<strong>Filtro utilizado</strong><br>";

		if (item_data.get("id_hotel")=="-1") {
			header+="<strong>Hotel: </strong>Todos<br>";
		}else{
			header+="<strong>Hotel: </strong>"+$("#hotel :selected").text()+"<br>";
		}

		if (item_data.get("sexo")=="-1") {
			header+="<strong>Sexo: </strong>Todos<br>";
		}else{
			header+="<strong>Sexo: </strong>"+$("#sexo :selected").text()+"<br>";
		}
		if (item_data.get("edad")=="-1") {
			header+="<strong>Edad: </strong>Todos<br>";
		}else{
			header+="<strong>Edad: </strong>"+$("#edad :selected").text()+"<br>";
		}

		if (item_data.get("edad")=="-1") {
			header+="<strong>Forma de encuesta: </strong>Todos<br>";
		}else{
			header+="<strong>Forma de Encuesta: </strong>"+$("#forma_encuesta :selected").text();+" <br>";
		}

		header+="<br><strong>Periodo: </strong> desde("+item_data.get("fecha_desde")+") - hasta("+item_data.get("fecha_hasta")+"))<br>";

	}

	return header;
}

function buscar(item_data, tipo){
		//item_data.append("cod", "CKO");
		//alert(item_data.get("id_hotel"));

		var headerPDF = getHeader("pdf", item_data);
		var headerPrint = getHeader("print", item_data);

		$.ajax({

		type:"POST",
		url:"https://hoteleshesperia.com.ve/HesperiaQuality/include/resultado_filtro_encuestas.php",
		data:item_data,
		contentType:false,
		processData:false,
		cache:false,
		success: function(response){
			if(response){
				if(tipo=="modal"){
					tablamodalfiltros.destroy();
					$("#modal-body").html(response);
					tablamodalfiltros = $("#dataTables-example2").DataTable( {
			          dom: 'Bfrtip',
			          buttons: [
			                'excel',
			                {
			                	extend: 'pdfHtml5',
			                	message: ''+headerPDF
			                },
			                {
				                extend: 'print',
				                message: ''+headerPrint
				            }
			              ]
			          });
					$("#modal1").modal(function(){
						show:true
					});
				}else{

					tablaglobal.destroy();
					$("#bodyTable").html(response);
					tablaglobal = $("#dataTables-example").DataTable( {
			          dom: 'Bfrtip',
			          buttons: [
			                'excel',
			                {
			                	extend: 'pdfHtml5',
			                	message: ''+headerPDF
			                },
			                {
				                extend: 'print',
				                message: ''+headerPrint
				            }
			              ]
			          });
				}

				//location.reload();
			}else{
				alert("Ha ocurrido un error, vuelva a intentarlo");
				//location.reload();
			}
		},
		error: function(response){
			alert("Ha ocurrido un error, vuelva a intentarlo");
			//location.reload();
			},


		});
}
