-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `hesperia_categoria`;
CREATE TABLE `hesperia_categoria` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_categoria` (`id`, `nombre_categoria`) VALUES
(1,	'Historia'),
(2,	'Próximas Aperturas'),
(3,	'Gastronomía'),
(4,	'Responsabilidad Social');

DROP TABLE IF EXISTS `hesperia_contacto`;
CREATE TABLE `hesperia_contacto` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `nombre` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` text COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `pais` tinytext COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_contenido`;
CREATE TABLE `hesperia_contenido` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(12) NOT NULL,
  `texto` longtext COLLATE latin1_spanish_ci NOT NULL,
  `imagen` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_contenido` (`id`, `id_categoria`, `texto`, `imagen`) VALUES
(1,	1,	'<p>Abr4xum xdisenadurums Mauris quisque at tellus consequatur. Et integer quia interdum eleifend ultrices iaculis. Vestibulum proin eget mauris, semper venenatis justo facilisis est, elit rhoncus egestas maecenas sem euismod vestibulum, et at consequat sagittis error amet. Metus eu ut aliquet odio eget sagittis, tempor vestibulum elit amet cras dapibus, eaque nibh arcu augue nascetur in. Parturient vestibulum, blandit in ipsum urna vivamus massa amet, sed in interdum metus. Sit tristique dignissim erat</p>',	'sin-imagen.png'),
(2,	2,	'<p>A elit rhoncus egestas maecenas sem euismod vestibulum, et at consequat sagittis error amet. Metus eu ut aliquet odio eget sagittis, tempor vestibulum elit amet cras dapibus, eaque nibh arcu augue nascetur in. Parturient vestibulum, blandit in ipsum urna vivamus massa amet, sed in interdum metus. Sit tristique dignissim erat.</p>',	'sin-imagen.png'),
(3,	3,	'<p>Abr4xum tempor vestibulum elit amet cras dapibus, eaque nibh arcu augue nascetur in. Parturient vestibulum, blandit in ipsum urna vivamus massa amet, sed in interdum metus. Sit tristique dignissim erat.</p>',	'sin-imagen.png'),
(4,	4,	'<p>Abr4xum tempor vestibulum elit amet cras dapibus, eaque nibh arcu augue nascetur in. Parturient vestibulum, blandit in ipsum urna vivamus massa amet, sed in interdum metus. Sit tristique dignissim erat.&nbsp;<span style=\"line-height: 1.42857143;\">Abr4xum tempor vestibulum elit amet cras dapibus, eaque nibh arcu augue nascetur in. Parturient vestibulum, blandit in ipsum urna vivamus massa amet, sed in interdum metus. Sit tristique dignissim erat.</span><br></p>',	'sin-imagen.png');

DROP TABLE IF EXISTS `hesperia_descarga`;
CREATE TABLE `hesperia_descarga` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `archivo` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `tipo` int(1) NOT NULL,
  `fecha` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_distancia`;
CREATE TABLE `hesperia_distancia` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `nombre_aeropuerto` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `aeropuerto` decimal(10,3) DEFAULT NULL,
  `nombra_parque` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parques_nacionales` decimal(10,3) DEFAULT NULL,
  `centro_ciudad` decimal(10,3) DEFAULT NULL,
  `museo` decimal(10,3) DEFAULT NULL,
  `nombre_museo` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `metro` decimal(10,3) NOT NULL,
  `nombre_estacion` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `playas` decimal(10,3) DEFAULT NULL,
  `centro_comercial` decimal(10,3) DEFAULT NULL,
  `banco` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_distancia` (`id`, `id_hotel`, `nombre_aeropuerto`, `aeropuerto`, `nombra_parque`, `parques_nacionales`, `centro_ciudad`, `museo`, `nombre_museo`, `metro`, `nombre_estacion`, `playas`, `centro_comercial`, `banco`) VALUES
(1,	1,	'Aeropuerto Internacional Arturo Michelena',	22.100,	'Jardín Botánico Naguanagua',	0.650,	10.080,	5.900,	'Ateneo de Valencia',	0.000,	'metro',	54.500,	54.500,	0.300);

DROP TABLE IF EXISTS `hesperia_eventos`;
CREATE TABLE `hesperia_eventos` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `id_salon` int(12) NOT NULL,
  `nombre_evento` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `fecha_evento` int(12) NOT NULL,
  `hora_evento` varchar(5) COLLATE latin1_spanish_ci NOT NULL,
  `texto_evento` text COLLATE latin1_spanish_ci NOT NULL,
  `precio_evento` int(12) NOT NULL,
  `imagen_evento` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_salon` (`id_salon`),
  CONSTRAINT `hesperia_eventos_ibfk_1` FOREIGN KEY (`id_salon`) REFERENCES `hesperia_salones` (`id_salon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_experiencias`;
CREATE TABLE `hesperia_experiencias` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `nombre_expe` tinytext NOT NULL,
  `img_expe` varchar(200) NOT NULL,
  `descripcion_expe` longtext NOT NULL,
  `fecha` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `hesperia_galeria`;
CREATE TABLE `hesperia_galeria` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(11) NOT NULL,
  `nombre_imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_imagen` (`nombre_imagen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_habitaciones`;
CREATE TABLE `hesperia_habitaciones` (
  `id_habitacion` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `cantidad_hab` int(12) NOT NULL,
  `disponibles` int(12) NOT NULL,
  `nombre_habitacion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `tamano_hab` int(3) NOT NULL,
  `tipo_habitacion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `tipo_cama` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `cama_adicional` int(1) NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `precio` int(10) NOT NULL,
  `precio_promocion` int(10) NOT NULL,
  `capacidad_personas` int(3) NOT NULL,
  `mini_bar` int(1) NOT NULL,
  `albornoz` int(1) NOT NULL,
  `telefono` int(1) NOT NULL,
  `control_remoto_tv` int(1) NOT NULL,
  `tv` int(1) NOT NULL,
  `aire_acondicionado` int(1) NOT NULL,
  `servicio_habitacion` int(1) NOT NULL,
  `desayuno` int(1) NOT NULL,
  `ambiente_musical` int(1) NOT NULL,
  `secador_pelo` int(1) NOT NULL,
  `lavanderia` int(1) NOT NULL,
  `caja_fuerte` int(1) NOT NULL,
  `tv_cable` int(1) NOT NULL,
  `jacuzzi` int(1) NOT NULL,
  `area_estar` int(1) NOT NULL,
  `jarra_agua_vasos` int(1) NOT NULL,
  `cafetera` int(1) NOT NULL,
  `servicio_medico` int(1) NOT NULL,
  `balcon` int(1) NOT NULL,
  `banera` int(1) NOT NULL,
  `imagen1` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `imagen2` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `imagen3` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `imagen4` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `imagen5` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `imagen6` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_habitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_headers`;
CREATE TABLE `hesperia_headers` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `imagen` varchar(200) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `estatus` tinyint(1) NOT NULL DEFAULT '0',
  `texto` varchar(30) CHARACTER SET latin1 NOT NULL,
  `fecha` int(11) NOT NULL DEFAULT '0',
  `idhotel` int(11) NOT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_headers` (`promo_id`, `nombre`, `imagen`, `estatus`, `texto`, `fecha`, `idhotel`) VALUES
(1,	'heden',	'heden.jpg',	1,	'Hesperia Ed&eacute;n Club',	1433884591,	4),
(2,	'hwtc',	'hwtc.jpg',	1,	'Hesperia WTC Valencia',	1433884591,	1),
(3,	'him',	'him.jpg',	1,	'Hesperia Isla Margarita',	1429845212,	2),
(4,	'hpa',	'hpa.jpg',	1,	'Hesperia Playa El Agua',	1431185384,	3);

DROP TABLE IF EXISTS `hesperia_imagenes_prensa`;
CREATE TABLE `hesperia_imagenes_prensa` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `titulo` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `fecha` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_logs`;
CREATE TABLE `hesperia_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log` mediumtext COLLATE latin1_spanish_ci NOT NULL,
  `quien` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
  `ip` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `momento` int(16) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `quien` (`quien`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_logs` (`log_id`, `log`, `quien`, `ip`, `momento`) VALUES
(404,	'Inicio de sesion',	'Admin Principal',	'192.168.30.1',	1440456602),
(405,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440463864),
(406,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440464085),
(407,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440464850),
(408,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440466124),
(409,	'Nueva experiencia : dsfsdf',	'Admin Principal',	'192.168.30.1',	1440466299),
(410,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440466323),
(411,	'Inicio de sesion',	'Admin Principal',	'192.168.30.1',	1440467459),
(412,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440467515),
(413,	'Nueva cambio en portada',	'Admin Principal',	'192.168.30.1',	1440467563),
(414,	'Inicio de sesion',	'Admin Principal',	'192.168.30.1',	1440469211),
(415,	'Inicio de sesion',	'Admin Principal',	'192.168.30.1',	1440602430),
(416,	'Nueva experiencia : dsfsdf',	'Admin Principal',	'192.168.30.1',	1440604102),
(417,	'Inicio de sesion',	'Hector  A. Mantellini',	'192.168.30.1',	1440606227),
(418,	'Nueva experiencia : Otra gran experiencia',	'Hector  A. Mantellini',	'192.168.30.1',	1440606350),
(419,	'Inicio de sesion',	'Admin Principal',	'192.168.30.1',	1440607204),
(420,	'Actualizando experiencia: Otra gran experiencia',	'Admin Principal',	'192.168.30.1',	1440609150),
(421,	'Nuevo Paquetes : Un super paquete',	'Admin Principal',	'192.168.30.1',	1440610177),
(422,	'Actualizando paquete:  Un super paquete',	'Admin Principal',	'192.168.30.1',	1440610308),
(423,	'Actualizando paquete: Un super paquete',	'Admin Principal',	'192.168.30.1',	1440610308),
(424,	'Nuevo Paquetes : Un super paquete 2',	'Admin Principal',	'192.168.30.1',	1440610827);

DROP TABLE IF EXISTS `hesperia_nosotros`;
CREATE TABLE `hesperia_nosotros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) COLLATE latin1_spanish_ci NOT NULL,
  `texto_nosotros` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha` int(12) NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_nosotros` (`id`, `titulo`, `texto_nosotros`, `fecha`, `imagen`) VALUES
(2,	'Nosotros',	'cssdf',	123123123,	'4169_nosotros.png');

DROP TABLE IF EXISTS `hesperia_noticias`;
CREATE TABLE `hesperia_noticias` (
  `noticia_id` int(12) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) CHARACTER SET utf8 NOT NULL,
  `intro` tinytext CHARACTER SET utf8 NOT NULL,
  `contenido` text CHARACTER SET utf8 NOT NULL,
  `imagen` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `autor` varchar(30) CHARACTER SET utf8 NOT NULL,
  `fecha` int(14) NOT NULL DEFAULT '0',
  `lecturas` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noticia_id`),
  UNIQUE KEY `titulo` (`titulo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_paquetes`;
CREATE TABLE `hesperia_paquetes` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `nombre_paquete` tinytext NOT NULL,
  `img_paquete` varchar(200) NOT NULL,
  `descripcion_paquete` longtext NOT NULL,
  `fecha` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `hesperia_portada`;
CREATE TABLE `hesperia_portada` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `titulo_princial` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `titulo_UNO` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_UNO` text COLLATE latin1_spanish_ci NOT NULL,
  `imagen_UNO` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `titulo_DOS` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_DOS` text COLLATE latin1_spanish_ci NOT NULL,
  `imagen_DOS` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `titulo_TRES` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_TRES` text COLLATE latin1_spanish_ci NOT NULL,
  `imagen_TRES` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_portada` (`id`, `titulo_princial`, `titulo_UNO`, `texto_UNO`, `imagen_UNO`, `titulo_DOS`, `texto_DOS`, `imagen_DOS`, `titulo_TRES`, `texto_TRES`, `imagen_TRES`) VALUES
(1,	'VIVE UNA EXPERIENCIA EN GRANDE EN HOTELES HESPERIA VENEZUELA',	'RESTAURANTES GOURMET',	'<p>En nuestros restaurantes podrÃƒÂ¡s disfrutar de experiencias ÃƒÂºnicas de alta gastronomÃƒÂ­a para deleitar tu paladar.<br></p>',	'1982_11755207_10207030319138953_6499899398210474140_n.jpg',	'PAQUETES MAGICOS',	'<p>OlvÃƒÂ­date de la rutina diaria y disfrute de unas breves vacaciones en la perla del Caribe, con alojamiento \"Todo Incluido\"<br></p>',	'escapadasGEN.png',	'EVENTOS INOLVIDABLES',	'<p>Te ayudamos a planear con ÃƒÂ©xito tu Evento Social o Corporativo en espacios perfectos para cada requerimiento.<br></p>',	'eventos.jpg');

DROP TABLE IF EXISTS `hesperia_post_precio`;
CREATE TABLE `hesperia_post_precio` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `id_habitacion` int(12) NOT NULL,
  `precio` int(12) NOT NULL,
  `precio_promocion` int(12) NOT NULL,
  `fecha_precio` int(12) NOT NULL,
  `fecha` int(12) NOT NULL,
  `procesado` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_reservaciones`;
CREATE TABLE `hesperia_reservaciones` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `id_habitacion` int(12) NOT NULL,
  `id_usuario` int(12) NOT NULL,
  `desde` int(12) NOT NULL,
  `hasta` int(12) NOT NULL,
  `adultos` int(4) NOT NULL,
  `menores` int(4) NOT NULL,
  `aprobada` int(1) NOT NULL,
  `pago` int(1) NOT NULL,
  `monto` int(12) NOT NULL,
  `liberada` int(12) NOT NULL DEFAULT '0',
  `fecha_reserva` int(12) NOT NULL,
  `fecha_liberacion` int(12) DEFAULT NULL,
  `observaciones` text COLLATE latin1_spanish_ci,
  `codigo_reservacion` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_reservaciones` (`id`, `id_hotel`, `id_habitacion`, `id_usuario`, `desde`, `hasta`, `adultos`, `menores`, `aprobada`, `pago`, `monto`, `liberada`, `fecha_reserva`, `fecha_liberacion`, `observaciones`, `codigo_reservacion`) VALUES
(1,	1,	1,	1,	1437964506,	1437994506,	1,	0,	1,	12000,	12000,	1,	1437964506,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `hesperia_restaurant`;
CREATE TABLE `hesperia_restaurant` (
  `id_restaurant` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `nombre_restaurant` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `tipo_comida` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `hora_apertura` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `hora_cierre` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `caracteristicas` text COLLATE latin1_spanish_ci NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_restaurant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_salones`;
CREATE TABLE `hesperia_salones` (
  `id_salon` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `nombre_Salon` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `caracteristicas` text COLLATE latin1_spanish_ci NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_salon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_salones` (`id_salon`, `id_hotel`, `nombre_Salon`, `caracteristicas`, `imagen`) VALUES
(1,	1,	'sdasd',	'sdasdasd',	'sin-imagen.png');

DROP TABLE IF EXISTS `hesperia_servicios`;
CREATE TABLE `hesperia_servicios` (
  `id_servicios` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `texto_mascotas` text COLLATE latin1_spanish_ci,
  `imagen_mascotas` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_wifi` text COLLATE latin1_spanish_ci,
  `imagen_wifi` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_estacionamiento` text COLLATE latin1_spanish_ci,
  `imagen_estacionamiento` tinytext COLLATE latin1_spanish_ci,
  `texto_gimnasio` text COLLATE latin1_spanish_ci,
  `imagen_gimnasio` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_spa` text COLLATE latin1_spanish_ci,
  `imagen_spa` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_helipuerto` text COLLATE latin1_spanish_ci,
  `imagen_helipuerto` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_accesibilidad` text COLLATE latin1_spanish_ci,
  `imagen_accesibilidad` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_nana` text COLLATE latin1_spanish_ci,
  `imagen_nana` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_transporte` text COLLATE latin1_spanish_ci,
  `imagen_transporte` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_vigilancia` text COLLATE latin1_spanish_ci,
  `imagen_vigilancia` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `texto_cafetin` text COLLATE latin1_spanish_ci,
  `imagen_cafetin` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_restaurant` text COLLATE latin1_spanish_ci,
  `imagen_restaurant` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_bar` text COLLATE latin1_spanish_ci,
  `imagen_bar` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_golf` text COLLATE latin1_spanish_ci,
  `imagen_golf` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_tenis` text COLLATE latin1_spanish_ci,
  `imagen_tenis` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_basket` text COLLATE latin1_spanish_ci,
  `imagen_basket` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_paquetes` text COLLATE latin1_spanish_ci,
  `imagen_paquetes` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto_peluqueria` text COLLATE latin1_spanish_ci,
  `imagen_peluqueria` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_servicios`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_servicios` (`id_servicios`, `id_hotel`, `texto_mascotas`, `imagen_mascotas`, `texto_wifi`, `imagen_wifi`, `texto_estacionamiento`, `imagen_estacionamiento`, `texto_gimnasio`, `imagen_gimnasio`, `texto_spa`, `imagen_spa`, `texto_helipuerto`, `imagen_helipuerto`, `texto_accesibilidad`, `imagen_accesibilidad`, `texto_nana`, `imagen_nana`, `texto_transporte`, `imagen_transporte`, `texto_vigilancia`, `imagen_vigilancia`, `texto_cafetin`, `imagen_cafetin`, `texto_restaurant`, `imagen_restaurant`, `texto_bar`, `imagen_bar`, `texto_golf`, `imagen_golf`, `texto_tenis`, `imagen_tenis`, `texto_basket`, `imagen_basket`, `texto_paquetes`, `imagen_paquetes`, `texto_peluqueria`, `imagen_peluqueria`) VALUES
(1,	1,	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	NULL,	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\n<ul>\n<li>Desayuno buffet</li> \n<li>Restaurante Atmósfera</li>\n<li>Restaurante Orión</li>\n</ul>',	'sin-imagen.png',	NULL,	'sin-imagen.png',	'<p>Flojildus postmorteum etc...Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n\r\n<ul>\r\n<li>Desayuno buffet</li> \r\n<li>Restaurante Atmósfera</li>\r\n<li>Restaurante Orión</li>\r\n</ul>',	'sin-imagen.png',	NULL,	'sin-imagen.png',	NULL,	'sin-imagen.png',	'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>',	'sin-imagen.png',	NULL,	'sin-imagen.png');

DROP TABLE IF EXISTS `hesperia_sesion`;
CREATE TABLE `hesperia_sesion` (
  `id_session` int(12) NOT NULL AUTO_INCREMENT,
  `sesion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` int(12) NOT NULL,
  `referencia` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_session`),
  UNIQUE KEY `referencia` (`referencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_sesion` (`id_session`, `sesion`, `fecha`, `referencia`) VALUES
(6,	'ic6ra848a7qt1j94hfao3pv7i7',	1440606227,	'1'),
(7,	'ic6ra848a7qt1j94hfao3pv7i7',	1440607204,	'4');

DROP TABLE IF EXISTS `hesperia_settings`;
CREATE TABLE `hesperia_settings` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(250) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  `rif` varchar(250) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  `nombre_sitio` varchar(200) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `logo` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `dominio` varchar(200) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'hotelesperia.com.ve',
  `direccion` text COLLATE latin1_spanish_ci NOT NULL,
  `ciudad` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `estado` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `pais` varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'Venezuela',
  `latitud` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `longitud` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `telefonos` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `telf_reserva` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `check_in` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `check_out` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `postal` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `quienes` text COLLATE latin1_spanish_ci NOT NULL,
  `twitter` varchar(100) COLLATE latin1_spanish_ci DEFAULT 'hesperiave',
  `facebook` varchar(100) COLLATE latin1_spanish_ci DEFAULT 'HesperiaVenezuela',
  `gplus` varchar(100) COLLATE latin1_spanish_ci DEFAULT '112610000929588119367',
  `youtube` varchar(100) COLLATE latin1_spanish_ci DEFAULT 'UCW6Y9Fk_Pw2gSFIY456WmJw',
  `instagram` varchar(100) COLLATE latin1_spanish_ci DEFAULT 'hesperiave',
  `pinterest` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `linkedin` varchar(100) COLLATE latin1_spanish_ci DEFAULT 'hoteles-hesperia-venezuela',
  `correo_envio` varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'info@hotelesperia.com.ve',
  `correo_contacto` varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'reservas@hoteleshesperia.com.ve',
  `keywords` text COLLATE latin1_spanish_ci NOT NULL,
  `description` text COLLATE latin1_spanish_ci NOT NULL,
  `salones_evento` int(1) NOT NULL,
  `escapadas` int(1) NOT NULL,
  `experiencias` int(1) NOT NULL,
  `mascotas` int(1) NOT NULL,
  `wifi` int(1) NOT NULL,
  `piscina` int(1) NOT NULL,
  `estacionamiento` int(1) NOT NULL,
  `botones` int(1) NOT NULL,
  `gimnasio` int(1) NOT NULL,
  `spa` int(1) NOT NULL,
  `helipuerto` int(1) NOT NULL,
  `accesibilidad` int(1) NOT NULL,
  `nana` int(1) NOT NULL,
  `transporte` int(1) NOT NULL,
  `vigilancia_privada` int(1) NOT NULL,
  `servicio_medico` int(1) NOT NULL,
  `chofer` int(1) NOT NULL,
  `alquiler_vehiculos` int(1) NOT NULL,
  `cafetin` int(1) NOT NULL,
  `restaurant` int(1) NOT NULL,
  `bar` int(1) NOT NULL,
  `discotheca` int(1) NOT NULL,
  `golf` int(1) NOT NULL,
  `tenis` int(1) NOT NULL,
  `basket` int(1) NOT NULL,
  `tienda` int(1) NOT NULL,
  `cant_habitaciones` int(4) NOT NULL,
  `parque` int(1) NOT NULL,
  `terraza` int(1) NOT NULL,
  `tripadvisor` text COLLATE latin1_spanish_ci NOT NULL,
  `tripadvisor_premio` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `razon_social` (`razon_social`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_settings` (`id`, `razon_social`, `rif`, `nombre_sitio`, `descripcion`, `logo`, `dominio`, `direccion`, `ciudad`, `estado`, `pais`, `latitud`, `longitud`, `telefonos`, `telf_reserva`, `check_in`, `check_out`, `postal`, `quienes`, `twitter`, `facebook`, `gplus`, `youtube`, `instagram`, `pinterest`, `linkedin`, `correo_envio`, `correo_contacto`, `keywords`, `description`, `salones_evento`, `escapadas`, `experiencias`, `mascotas`, `wifi`, `piscina`, `estacionamiento`, `botones`, `gimnasio`, `spa`, `helipuerto`, `accesibilidad`, `nana`, `transporte`, `vigilancia_privada`, `servicio_medico`, `chofer`, `alquiler_vehiculos`, `cafetin`, `restaurant`, `bar`, `discotheca`, `golf`, `tenis`, `basket`, `tienda`, `cant_habitaciones`, `parque`, `terraza`, `tripadvisor`, `tripadvisor_premio`, `fecha_creacion`) VALUES
(1,	'Hesperia WTC Valencia',	' J-07532170-7',	'Hesperia WTC Valencia',	'<p>Hesperia WTC Valencia - Hotel and Convention Center, de categoría superior, pertenece a la reconocida cadena Hotelera Hesperia Hotels & Resorts, con presencia en 27 países; es una de las  más grandes en España y la quinta de Europa en la categoría de negocios.</p>\r\n<p>\r\nEl hotel de 15 pisos cuenta con 325 lujosas habitaciones, todas con aislamiento acústico que las mantiene en un ambiente de silencio y tranquilidad absoluto, único en su estilo y dimensiones estratégicamente ubicado en la Av. Salvador Feo La Cruz, a tan solo 15 minutos del Aeropuerto Internacional Arturo Michelena y a 30 minutos de la ciudad de Puerto Cabello, idóneo para el descanso o una reunión privada de negocios en la sala ejecutiva (habitaciones Suites). El equipamiento estándar de las habitaciones incluye: lujosa ropa de cama, una amplia variedad de comodidades, como Wi-Fi, televisión LCD y Minibar.</p>',	'wtc-valencia.png',	'hotelesperia.com.ve',	'Av. Salvador Feo la Cruz ',	'Naguanagua',	'Carabobo',	'Venezuela',	'10.2342178',	'-68.0056661',	' (+58) 0241-5153000',	' (+58) 0241-.5153005',	'',	'',	'2005',	'<p>Lorem ipsum dolor sit amet, curae consequat leo sit. Lobortis leo dui hac nibh duis, cras eu fringilla. Wisi pellentesque vitae est per ultricies, ultricies orci, neque vitae a volutpat mauris ligula. Dolorum lobortis, ac vivamus suscipit ipsum, molestie sodales sem. Eu at tempus ipsum aliquam, parturient massa aenean lacus, sed in urna lorem sed. Eu aliquam ut cursus, praesent sed dui leo tempus vitae, ornare urna suscipit dis facilisis aenean curae. Maecenas mi, justo quisque mauris pellentesque, est in expedita id suspendisse hendrerit, orci dui mi. Tempus sed nunc duis amet bibendum ut. Luctus maecenas dapibus, imperdiet ac nam praesent faucibus. Vel in ante risus risus, sit vulputate feugiat ut neque varius sollicitudin,</p><p><br></p><p>vitae felis consectetuer eget velit in a. Pharetra dictum aenean rutrum felis, viverra ut suspendisse ultrices sodales neque at, mauris vel quis dictum ipsum, nullam non dolor dui ullamcorper litora id. Sapien metus vitae pellentesque at suspendisse, pellentesque in a velit cras praesent, in nec varius. Ut a facere enim. A duis orci id libero. Wisi suspendisse a enim, congue aliquet justo adipiscing praesent justo. Eros pede vestibulum sed vestibulum, et vestibulum magna, nunc pede, blandit sit earum diam sem, quis ante vitae mattis non eget. Ipsum tellus mauris mollis ligula tellus, mattis nostra laoreet metus auctor et, pellentesque illum, et quisque. Aliquet libero nisl. Nulla etiam pretium potenti tempor, augue lacinia maecenas aenean vestibulum lectus, odio at at, sit lacus, tempus orci mauris est integer. Interdum eu, sapien molestie ridiculus fames feugiat. Nunc sit ligula, sodales nunc turpis lorem pretium sed, nunc ornare et malesuada adipiscing. Pharetra vivamus, malesuada non, neque feugiat posuere quam, mollis nulla nam alias sodales erat fermentum. Sed semper suspendisse.</p>',	'hesperiave',	'HesperiaVenezuela',	'112610000929588119367',	'UCW6Y9Fk_Pw2gSFIY456WmJw',	'hesperiave',	'',	'hoteles-hesperia-venezuela',	'reservas@hesperia-wtc.com',	'reservas@hesperia-wtc.com',	'Venezuela,Contador,Contador Público,Contador Publico,Contaduria,Contabilidad,YoSoyContadorPublicoColegiado,libros,contabilidad,auditoria,trabajo,correr,carro,negocio,venta,compra,eventos,congresos,carabobo,valencia,venezuela',	'hotelesperia del Estado Carabobo - Venezuela',	1,	1,	0,	0,	1,	0,	0,	1,	1,	0,	0,	1,	0,	1,	0,	0,	0,	0,	0,	1,	1,	0,	0,	0,	0,	1,	325,	0,	0,	'',	'                        <div id=\"TA_tchotel771\" class=\"TA_tchotel\">\r\n                            <ul id=\"XQn7oHAfW\" class=\"TA_links FcGj4bjRx5\">\r\n                                <li id=\"yabT4EFRxY\" class=\"Gqc3vh000\">\r\n                                    <a target=\"_blank\" href=\"http://www.tripadvisor.com.ve/Hotel_Review-g316069-d2065193-Reviews-Hesperia_WTC_Valencia-Valencia_Central_Region.html\">\r\n                                        <img src=\"img/social/tchotel_2015_LL_TM-11655-2.jpg\" class=\"img-responsive img-hover\" alt=\"TripAdvisor\"/>\r\n                                    </a>\r\n                                </li>\r\n                            </ul>\r\n                        </div>\r\n<script src=\"http://www.jscache.com/wejs?wtype=tchotel&uniq=771&locationId=2065193&lang=es_VE&year=2015&display_version=2\"></script>',	1413841297),
(2,	'Hesperia Isla Margarita',	'',	'Hesperia Isla Margarita',	'<p>Lorem ipsum dolor sit amet, odio a ac in, nec rerum eget. Gravida pede nullam aliquam, in molestie odio, tempus diam wisi vitae montes maecenas congue, ut vestibulum habitasse commodo nascetur laoreet diam. Lectus massa purus ridiculus in vehicula, blandit pharetra porttitor nunc wisi molestie, pede aliquam quam tellus malesuada sed arcu, nunc venenatis, sapien ut felis. Quam vivamus libero, sit tempus.</p>',	'wtc-margarita.png',	'hotelesperia.com.ve',	'Entre playas Puerto viejo y Puerto Cruz',	'Margarita',	'Nueva Esparta',	'Venezuela',	'11.128587',	'-63.918131',	'+58.295.4008111',	'+58.295.4007100',	'',	'',	'',	'<p>Lorem ipsum dolor sit amet, curae consequat leo sit. Lobortis leo dui hac nibh duis, cras eu fringilla. Wisi pellentesque vitae est per ultricies, ultricies orci, neque vitae a volutpat mauris ligula. Dolorum lobortis, ac vivamus suscipit ipsum, molestie sodales sem. Eu at tempus ipsum aliquam, parturient massa aenean lacus, sed in urna lorem sed. Eu aliquam ut cursus, praesent sed dui leo tempus vitae, ornare urna suscipit dis facilisis aenean curae. Maecenas mi, justo quisque mauris pellentesque, est in expedita id suspendisse hendrerit, orci dui mi. Tempus sed nunc duis amet bibendum ut. Luctus maecenas dapibus, imperdiet ac nam praesent faucibus. Vel in ante risus risus, sit vulputate feugiat ut neque varius sollicitudin,</p>\r\n<p> vitae felis consectetuer eget velit in a. Pharetra dictum aenean rutrum felis, viverra ut suspendisse ultrices sodales neque at, mauris vel quis dictum ipsum, nullam non dolor dui ullamcorper litora id.\r\nSapien metus vitae pellentesque at suspendisse, pellentesque in a velit cras praesent, in nec varius. Ut a facere enim. A duis orci id libero. Wisi suspendisse a enim, congue aliquet justo adipiscing praesent justo. Eros pede vestibulum sed vestibulum, et vestibulum magna, nunc pede, blandit sit earum diam sem, quis ante vitae mattis non eget. Ipsum tellus mauris mollis ligula tellus, mattis nostra laoreet metus auctor et, pellentesque illum, et quisque. Aliquet libero nisl. Nulla etiam pretium potenti tempor, augue lacinia maecenas aenean vestibulum lectus, odio at at, sit lacus, tempus orci mauris est integer. Interdum eu, sapien molestie ridiculus fames feugiat. Nunc sit ligula, sodales nunc turpis lorem pretium sed, nunc ornare et malesuada adipiscing. Pharetra vivamus, malesuada non, neque feugiat posuere quam, mollis nulla nam alias sodales erat fermentum. Sed semper suspendisse.</p>',	'',	'',	'',	'',	'',	'',	NULL,	'reservas@hesperia.com',	'reservas@hesperia.com',	'',	'',	1,	1,	1,	0,	1,	1,	1,	0,	1,	1,	0,	1,	1,	0,	0,	0,	0,	0,	0,	1,	1,	0,	1,	1,	0,	0,	50,	0,	0,	'',	'',	1413841297),
(3,	'Hesperia Playa El AguaX',	'J-000',	'Hesperia Playa El AguaX',	'<p>Il Abraxum trabajarum Muchum Lorem ipsum dolor sit amet, odio a ac in, nec rerum eget. Gravida pede nullam aliquam, in molestie odio, tempus diam wisi vitae montes maecenas congue, ut vestibulum habitasse commodo nascetur laoreet diam. Lectus massa purus ridiculus in vehicula, blandit pharetra porttitor nunc wisi molestie, pede aliquam quam tellus malesuada sed arcu, nunc venenatis, sapien ut felis. Quam vivamus libero, sit tempus.</p>\r\n<p>Lorem ipsum dolor sit amet, odio a ac in, nec rerum eget. Gravida pede nullam aliquam, in molestie odio, tempus diam wisi vitae montes maecenas congue, ut vestibulum habitasse commodo nascetur laoreet diam. Lectus massa purus ridiculus in vehicula, blandit pharetra porttitor nunc wisi molestie, pede aliquam quam tellus malesuada sed arcu, nunc venenatis, sapien ut felis. Quam vivamus libero, sit tempus.</p>',	'wtc-playa-el-agua.png',	'hhotelesperia.com.ve',	'Av. 31 de Julio, La Mira a',	'Maragaritaa',	'Nueva Espartaa',	'Venezuelaa',	'11.1442432',	'-63.8666662',	'+58.295.40081112',	'+58.295.40071002',	'1',	'2',	'222',	'<p>Il Lorem ipsum dolor sit amet, curae consequat leo sit. Lobortis leo dui hac nibh duis, cras eu fringilla. Wisi pellentesque vitae est per ultricies, ultricies orci, neque vitae a volutpat mauris ligula. Dolorum lobortis, ac vivamus suscipit ipsum, molestie sodales sem. Eu at tempus ipsum aliquam, parturient massa aenean lacus, sed in urna lorem sed. Eu aliquam ut cursus, praesent sed dui leo tempus vitae, ornare urna suscipit dis facilisis aenean curae. Maecenas mi, justo quisque mauris pellentesque, est in expedita id suspendisse hendrerit, orci dui mi. Tempus sed nunc duis amet bibendum ut. Luctus maecenas dapibus, imperdiet ac nam praesent faucibus. Vel in ante risus risus, sit vulputate feugiat ut neque varius sollicitudin,</p>\r\n<p> vitae felis consectetuer eget velit in a. Pharetra dictum aenean rutrum felis, viverra ut suspendisse ultrices sodales neque at, mauris vel quis dictum ipsum, nullam non dolor dui ullamcorper litora id.\r\nSapien metus vitae pellentesque at suspendisse, pellentesque in a velit cras praesent, in nec varius. Ut a facere enim. A duis orci id libero. Wisi suspendisse a enim, congue aliquet justo adipiscing praesent justo. Eros pede vestibulum sed vestibulum, et vestibulum magna, nunc pede, blandit sit earum diam sem, quis ante vitae mattis non eget. Ipsum tellus mauris mollis ligula tellus, mattis nostra laoreet metus auctor et, pellentesque illum, et quisque. Aliquet libero nisl. Nulla etiam pretium potenti tempor, augue lacinia maecenas aenean vestibulum lectus, odio at at, sit lacus, tempus orci mauris est integer. Interdum eu, sapien molestie ridiculus fames feugiat. Nunc sit ligula, sodales nunc turpis lorem pretium sed, nunc ornare et malesuada adipiscing. Pharetra vivamus, malesuada non, neque feugiat posuere quam, mollis nulla nam alias sodales erat fermentum. Sed semper suspendisse.</p>',	'tw',	'fb',	'gp',	'yt',	'in',	'pi',	'li',	'co',	'cc',	'key',	'desc',	1,	1,	1,	1,	0,	0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	1,	1,	1,	0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	'xxx',	'xxxxxxx',	1413841297),
(4,	'Hesperia EdÃ©n Club',	'J-111dddd',	'Hesperia EdÃ©n Club',	'<p>Edenus Hotelkerum Lorem ipsum dolor sit amet, odio a ac in, nec rerum eget. Gravida pede nullam aliquam, in molestie odio, tempus diam wisi vitae montes maecenas congue, ut vestibulum habitasse commodo nascetur laoreet diam. Lectus massa purus ridiculus in vehicula, blandit pharetra porttitor nunc wisi molestie, pede aliquam quam tellus malesuada sed arcu, nunc venenatis, sapien ut felis. Quam vivamus libero, sit tempus. d sd sd s s sd&nbsp;</p>',	'wtc-eden-club.png',	'hotelesperia.com.ve',	'Av. 31 de Julio, La Mira',	'Isla Margarita',	'Nueva Esparta ',	'Venezuela',	'11.144243',	'-63.866666',	'+58.295.4008111',	' +58.295.4007100',	'1',	'2',	'2002',	'<p>Lorem ipsum dolor sit amet, curae consequat leo sit. Lobortis leo dui hac nibh duis, cras eu fringilla. Wisi pellentesque vitae est per ultricies, ultricies orci, neque vitae a volutpat mauris ligula. Dolorum lobortis, ac vivamus suscipit ipsum, molestie sodales sem. Eu at tempus ipsum aliquam, parturient massa aenean lacus, sed in urna lorem sed. Eu aliquam ut cursus, praesent sed dui leo tempus vitae, ornare urna suscipit dis facilisis aenean curae. Maecenas mi, justo quisque mauris pellentesque, est in expedita id suspendisse hendrerit, orci dui mi. Tempus sed nunc duis amet bibendum ut. Luctus maecenas dapibus, imperdiet ac nam praesent faucibus. Vel in ante risus risus, sit vulputate feugiat ut neque varius sollicitudin,</p>\r\n<p> vitae felis consectetuer eget velit in a. Pharetra dictum aenean rutrum felis, viverra ut suspendisse ultrices sodales neque at, mauris vel quis dictum ipsum, nullam non dolor dui ullamcorper litora id.\r\nSapien metus vitae pellentesque at suspendisse, pellentesque in a velit cras praesent, in nec varius. Ut a facere enim. A duis orci id libero. Wisi suspendisse a enim, congue aliquet justo adipiscing praesent justo. Eros pede vestibulum sed vestibulum, et vestibulum magna, nunc pede, blandit sit earum diam sem, quis ante vitae mattis non eget. Ipsum tellus mauris mollis ligula tellus, mattis nostra laoreet metus auctor et, pellentesque illum, et quisque. Aliquet libero nisl. Nulla etiam pretium potenti tempor, augue lacinia maecenas aenean vestibulum lectus, odio at at, sit lacus, tempus orci mauris est integer. Interdum eu, sapien molestie ridiculus fames feugiat. Nunc sit ligula, sodales nunc turpis lorem pretium sed, nunc ornare et malesuada adipiscing. Pharetra vivamus, malesuada non, neque feugiat posuere quam, mollis nulla nam alias sodales erat fermentum. Sed semper suspendisse.</p>',	'',	'',	'',	'',	'',	'',	'',	'reservas@hesperia.com',	'reservas@hesperia.com',	'',	'',	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	12,	0,	0,	'asdasda',	'sadasddd',	1413841297);

DROP TABLE IF EXISTS `hesperia_suscripciones`;
CREATE TABLE `hesperia_suscripciones` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_trabajo`;
CREATE TABLE `hesperia_trabajo` (
  `id_curriculum` int(12) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) COLLATE latin1_spanish_ci NOT NULL,
  `telefono_movil` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `telefono_habi` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `direccion` text COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(220) COLLATE latin1_spanish_ci NOT NULL,
  `archivo` varchar(225) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` int(12) NOT NULL,
  PRIMARY KEY (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `hesperia_usuario`;
CREATE TABLE `hesperia_usuario` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `clave_acceso` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `nombres` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `empresa` text COLLATE latin1_spanish_ci NOT NULL,
  `numero_empresa` varchar(12) COLLATE latin1_spanish_ci NOT NULL,
  `email_empresa` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_registro` int(15) NOT NULL,
  `ultimo_login` int(15) NOT NULL,
  `nivel` int(1) NOT NULL DEFAULT '3',
  `valido` int(1) NOT NULL DEFAULT '0',
  `id_hotel` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `hesperia_usuario` (`id`, `email`, `clave_acceso`, `nombres`, `apellidos`, `telefono`, `empresa`, `numero_empresa`, `email_empresa`, `fecha_registro`, `ultimo_login`, `nivel`, `valido`, `id_hotel`) VALUES
(1,	'xombra.com@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'Hector',	' A. Mantellini',	'04128561612',	'',	'',	'',	1435715482,	1440606227,	0,	1,	1),
(4,	'info@viserproject.com',	'e10adc3949ba59abbe56e057f20f883e',	'Admin',	'Principal',	'0412',	'ViserProject',	'',	'info@viserproject.com',	1437324903,	1440607204,	0,	1,	0);

DROP TABLE IF EXISTS `hesperia_video`;
CREATE TABLE `hesperia_video` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `id_hotel` int(12) NOT NULL,
  `video_promocion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `activo` int(1) NOT NULL,
  `fecha_registro` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `video_promocion` (`video_promocion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


-- 2015-08-26 17:48:39
