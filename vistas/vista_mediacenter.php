<?php 
include_once ("include/funciones_mediacenter.php");
include_once ("panel/include/funciones_mediacenter_panel.php");

$resultadoOptions = cargarHoteles($mysqli);
$resultado = cargarListaPdfs($mysqli);

$resultadoImg= cargarFotosMediaCenter($mysqli);
$resultadoVid= cargarVideosMediaCenter($mysqli);
$resultadoNoticias = cargarNoticias($mysqli);

$array_select_hoteles = array();
$array_pdf = array();
$i=0;
while ($reg = mysqli_fetch_assoc($resultadoOptions)) {
  $array_select_hoteles[$i]=$reg;
  $i++;
}

 ?>
<div class='container-fluid'>

	<div class='row'>
		<div class='col-md-12'>
			<h2 class='page-header'>Media Center <small>encuentra todos nuestros contenidos</small></h2>
		</div>
	</div>

	<div class='row'>
		
		<div class='col-md-3' ><!--MENU MEDIA CENTER-->
            <?php 
            $menu = menuMediaCenter();
            echo($menu);
             ?>    
      </div><!--FIN MENU MEDIA CENTER-->
        <div class="tab-content">
            <div class='col-md-9 tab-pane' role="tabpanel" id="brochures-pdf-content">
            	<h2>Brochures - PDF</h2>
            	 <p>Todos nuestros archivos en formato PDF para tu disposición
               </p>
                 <div class="col-md-12">
                    <label class="control-label">Elija Hotel</label> 
                    <select id="select-hotel-brochure" class="form-control dinamic-select">
                      <?php 
                      for ($i=0; $i <count($array_select_hoteles) ; $i++) { 
                        echo("<option value='".$array_select_hoteles[$i]["id"]."'>
                          ".$array_select_hoteles[$i]["razon_social"]."
                           </option>");
                         }

                       ?>
                    </select>
                     <hr>
                 </div>
                
                 <div class="col-md-12"><!--CONTENEDOR PDF MEDIA CENTER-->
                 	 <?php 
                        $i=0;
                        while ($reg = mysqli_fetch_assoc($resultado)) {
                          $array_pdf[$i]=$reg;
                          $json = json_encode($reg);

                          echo("
                            <div class='row brochure-item hh-".$reg["id_hotel"]."'>
                              <div class='col-md-2'>
                                  <img class='img-responsive' src='https://hoteleshesperia.com.ve/img/pdf.png' alt=''>
                              </div>
                              <div class='col-md-10'>
                                  <h3>$reg[titulo]</h3>
                                  <p>$reg[descripcion]</p>

                                  <a class='btn btn-primary' 
                                     href='https://hoteleshesperia.com.ve/img/media_center/$reg[nombre_archivo]' target='_blank'>
                                     Ver
                                  </a>

                                  <a class='btn btn-primary' 
                                     href='https://hoteleshesperia.com.ve/img/media_center/$reg[nombre_archivo]' download>
                                     Descargar
                                  </a>
                              </div>
                              <hr>
                            </div>
                            ");
                          $i++;
                        }
                        
                        ?>
                 </div><!--FIN CONTENEDOR PDF MEDIA CENTER-->
           	</div>

           	<div class='col-md-9 tab-pane' role="tabpanel" id="fotos-content">
            	<h2>Fotos</h2>
            	 <p></p>
                 <div class="col-md-12">
                    <label class="control-label">Elija Hotel</label> 
                    <select id="select-hotel-img" class="form-control dinamic-select">
                     <?php 
                       for ($i=0; $i <count($array_select_hoteles) ; $i++) { 
                          echo("<option  value='".$array_select_hoteles[$i]["id"]."'>
                            ".$array_select_hoteles[$i]["razon_social"]."
                             </option>");
                           }
                        ?>
                    </select>
                     <hr>
                 </div>
                 <div class="col-md-12"><!--CONTENEDOR FOTOS MEDIA CENTER-->
                 	<?php //cargar_imagenes_mediacenter();

                        $i=0;
                         while ($reg2 = mysqli_fetch_assoc($resultadoImg)) {
                            $json = json_encode($reg2);
                            $array_fotos_promos[$i] = $reg2;
                            if ($reg2["ind_mediacenter"]=="S" && $reg2["ind_promo"]=="N"
                              &&  $reg2["ind_marca"] =="N") {
                             echo("
                              <div class='col-md-3 foto-item hh-".$reg2["id_hotel"]."'>
                                <div class='thumbnail'>
                                  <img src='https://hoteleshesperia.com.ve/img/media_center/$reg2[nombre_imagen]' >
                                  <div class='caption'>
                                    <a href='#' class='show-modal' data-value='https://hoteleshesperia.com.ve/img/media_center/$reg2[nombre_imagen]'>Ver</a>
                                     - 
                                     <a href='https://hoteleshesperia.com.ve/img/media_center/$reg2[nombre_imagen]' download>Descargar</a> 
                                  </div>
                                </div>
                              </div>
                              ");
                            }
                           
                            $i++;
                          }
                   ?>
                 </div><!--FIN CONTENEDOR FOTOS MEDIA CENTER-->
           	</div>


           	<div class='col-md-9 tab-pane' role="tabpanel" id="videos-content">
            	<h2>Videos</h2>
            	 <p></p>
                 <div class="col-md-12"><!--CONTENEDOR VIDEOS MEDIA CENTER-->
                 	<?php
                  while ($reg3 = mysqli_fetch_assoc($resultadoVid)) {
                    $json = json_encode($reg3);
                    echo("
                      <div class='col-sm-4' style='padding-bottom:2%;'>
                        <div class='embed-responsive embed-responsive-16by9' >
                          <iframe class='embed-responsive-item'  src='//www.youtube.com/embed/$reg3[video_promocion]'></iframe>
                        </div>
                      </div>
                      ");
                    }
                  ?>
                 </div><!--FIN CONTENEDOR VIDEOS MEDIA CENTER-->
           	</div>

            <div class='col-md-9 tab-pane' role="tabpanel" id="promos-content">
                <h2>Promociones</h2>
                 <p></p>
                 <div class="col-md-12">
                    <label class="control-label">Elija Hotel</label> 
                    <select id="select-hotel-promos" class="form-control dinamic-select">
                     <?php  
                       for ($i=0; $i <count($array_select_hoteles) ; $i++) { 
                          echo("<option value='".$array_select_hoteles[$i]["id"]."'>
                            ".$array_select_hoteles[$i]["razon_social"]."
                             </option>");
                           }
                      ?>
                    </select>
                     <hr>
                 </div>
                 <div class="col-md-12"><!--CONTENEDOR FOTOS MEDIA CENTER-->
                    <?php
                      for ($i=0; $i <count($array_fotos_promos) ; $i++) { 
                        if ($array_fotos_promos[$i]["ind_promo"]=="S") {
                         // $json = json_encode($array_fotos_promos[$i]);
                           echo("
                              <div class='col-md-3 promos-item hh-".$array_fotos_promos[$i]["id_hotel"]."'>
                                <div class='thumbnail'>
                                  <img src='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."'>
                                  <div class='caption'>
                                    <a href='#' class='show-modal' data-value='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."' >Ver</a>
                                     - 
                                    <a href='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."' download>Descargar</a> 
                                  </div>
                                </div>
                              </div>
                              ");
                            }
                           } 
                    ?>
                 </div><!--FIN CONTENEDOR FOTOS MEDIA CENTER-->
            </div>

           	<div class='col-md-9 tab-pane active' role="tabpanel" id="noticias-content">
            	<h2>Noticias</h2>
            	 <p>Entérate de todo lo nuevo que te trae Hesperia de Venezuela
                 </p>
                 <hr>
                 <div class="col-md-12"><!--CONTENEDOR NOTICIAS MEDIA CENTER-->
                    <table class="table table-striped table-bordered table-hover table-reg" id="dataTables-noticia">
                      <thead>
                          <tr>
                              <th> </th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        while ($reg4 = mysqli_fetch_assoc($resultadoNoticias)) {
                          echo("
                          <tr>
                            <td> 
                              <div class='col-md-6'>
                                  <a href='https://hoteleshesperia.com.ve/noticia/$reg4[id_noticia]/".toAscii($reg4["titulo"])."'>
                                          <img class='img-responsive img-hover'
                                          src='https://hoteleshesperia.com.ve/img/media_center/$reg4[imagen]' alt='".toAscii($reg4["titulo"])."'>
                                  </a>
                              </div>
                              <div class='col-md-6'>
                                  <h3>
                                      <a href='https://hoteleshesperia.com.ve/noticia/$reg4[id_noticia]/".toAscii($reg4["titulo"])."'>$reg4[titulo]</a>
                                   </h3>                 
                                   <p>Publicado: $reg4[fecha]</p>
                                   <p>$reg4[subtitulo]</p>
                                    <a class='btn btn-primary' href='https://hoteleshesperia.com.ve/noticia/$reg4[id_noticia]/".toAscii($reg4["titulo"])."'>Leer más <i class='fa fa-angle-right'></i></a>
                                </div>
                              </td>
                            </tr>
                          ");
                        } 
                        
                         ?>
                      </tbody>
                    </table>

                 </div><!--FIN CONTENEDOR NOTICIAS MEDIA CENTER-->
                 
           	</div>

            <div class='col-md-9 tab-pane' role="tabpanel" id="marca-content">
              <h2>Manual de Marca</h2>
               <p></p>
                 <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active">
                        <a href="#logos" aria-controls="logos" role="tab" data-toggle="tab">Logos</a>
                      </li>
                      <li role="presentation">
                        <a href="#manuales" aria-controls="manuales" role="tab" data-toggle="tab">Manuales</a>
                      </li>
                    </ul>

                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="logos">
                        <div class="col-md-12"><!--CONTENEDOR FOTOS MEDIA CENTER-->
                          <?php
                            for ($i=0; $i <count($array_fotos_promos) ; $i++) { 
                              if ($array_fotos_promos[$i]["ind_marca"]=="S") {
                                //clases: promos-item
                               // $json = json_encode($array_fotos_promos[$i]);
                                 echo("
                                    <div class='col-md-3 hh-".$array_fotos_promos[$i]["id_hotel"]."' style='padding-top:2%;'>
                                      <div class='thumbnail'>
                                        <img src='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."'>
                                        <div class='caption'>
                                          <a href='#' class='show-modal' data-value='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."' >Ver</a>
                                           - 
                                          <a href='https://hoteleshesperia.com.ve/img/media_center/".$array_fotos_promos[$i]["nombre_imagen"]."' download>Descargar</a> 
                                        </div>
                                      </div>
                                    </div>
                                    ");
                                  }
                                 }
                                 //var_dump($array_fotos_promos);
                          ?>
                       </div>

                      </div>
                      <div role="tabpanel" class="tab-pane" id="manuales">
                        <?php 
                        for ($i=0; $i <count($array_pdf); $i++) { 
                          //clase brochure-item
                          //$array_pdf[$i];
                          if ($array_pdf[$i]["tipo"]==2) {
                           echo("
                            <br>
                              <div class='row hh-".$array_pdf[$i]["id_hotel"]."'>
                                <div class='col-md-2'>
                                    <img class='img-responsive' src='https://hoteleshesperia.com.ve/img/pdf.png' alt=''>
                                </div>
                                <div class='col-md-10'>
                                    <h3>".$array_pdf[$i]['titulo']."</h3>
                                    <p>".$array_pdf[$i]['descripcion']."</p>

                                    <a class='btn btn-primary' 
                                       href='https://hoteleshesperia.com.ve/img/media_center/".$array_pdf[$i]['nombre_archivo']."' target='_blank'>
                                       Ver
                                    </a>

                                    <a class='btn btn-primary' 
                                       href='https://hoteleshesperia.com.ve/img/media_center/".$array_pdf[$i]['nombre_archivo']."' download>
                                       Descargar
                                    </a>
                                </div>
                              </div>
                              <hr>");
                          }
                          
                        }
                      //$array_pdf[$i]

                       ?>

                      </div>
                      
                      
                    </div>
                  <?php
                  /*while ($reg3 = mysqli_fetch_assoc($resultadoVid)) {
                    $json = json_encode($reg3);
                    echo("
                      <div class='col-sm-4' style='padding-bottom:2%;'>
                        <div class='embed-responsive embed-responsive-16by9' >
                          <iframe class='embed-responsive-item'  src='//www.youtube.com/embed/$reg3[video_promocion]'></iframe>
                        </div>
                      </div>
                      ");
                    }*/
                  ?>
                 </div><!--FIN CONTENEDOR VIDEOS MEDIA CENTER-->
            </div>
        </div>

	</div>

  
</div>

