<?php 
include_once("funciones_v2.php");
$resultado = getHoteles($mysqli, "vacacional");

$fotos = ["4" => "8358_Fotos_HEC_0013_Lounge_Interior.jpg",
 "3" => "3580_Fotos_HPA_0009_Foto_piscina_3.jpg",
 "2" => "0251_Fotos_HIM_0007_Brightness_Contrast_2.jpg"];
 ?>

<section style="background: url('https://hoteleshesperia.com.ve/img/banner-hoteles-en-margarita.png') no-repeat center center;
     background-size:cover;" class='bg-1 text-center' >
</section>

 <div class="container">
        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Hesperia Hotels & Resorts
                </h1>
            </div>
            <p class="lead">
                Encuentre en cada uno de nuestros Hoteles en la isla de Margarita una experiencia sin igual en todo sentido    
            </p>

        </div>
        <!-- /.row -->

        <!-- Features Section -->
        <div class="row">
            <?php
     
            while ($reg = mysqli_fetch_assoc($resultado)) {
                //echo "$reg[id] <br>";
                $id = $reg["id"];
                echo "
                <div class='col-lg-12'>
                    <h2 class='page-header'>$reg[razon_social]</h2>
                </div>

                <div class='col-md-6'>
                    $reg[descripcion]
                </div>

                <div class='col-md-6'>
                    <img class='img-responsive' src='https://hoteleshesperia.com.ve/img/galeria/$fotos[$id]'>
                    <br>
                    <p class='text-center'>
                        <a href='https://hoteleshesperia.com.ve/".toAscii(utf8_encode($reg["razon_social"]))."' class='btn btn-primary'>Visitar</a>
                    </p>
                    
                </div>
                ";
            }

             ?>
        </div>
        <!-- /.row -->
</div>