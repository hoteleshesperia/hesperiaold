<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Hesperia Partners</title>
    <meta name="description" content=""/>

    <meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">

    <!-- Loading Bootstrap -->
    <!--<link href="dist/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Loading Flat UI -->
    <!-- <link href="dist/css/flat-ui.css" rel="stylesheet"> -->
    <!-- <link href="docs/assets/css/demo.css" rel="stylesheet"> -->

    <link rel="shortcut icon" href="img/favicon.png">

    <link href="https://hoteleshesperia.com.ve/hesperiapartners/assets/css/theme.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="dist/js/vendor/html5shiv.js"></script>
      <script src="dist/js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  if(isset($_GET["go"])){
    $go = $_GET["go"];
    if($go == "login" || $go=="contacto"){
  ?>
  <nav id="layout-nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="http://localhost/260417/hesperiapartners">Hesperia Partners</a>
      </div>
    
  </nav>    
  <?php 
    }else{
  ?> 
    <div class="row demo-row">
        <div class="col-xs-12">
          <nav id="layout-nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
              
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=inicio">Hesperia Partners</a>
              </div>
              <div class="collapse navbar-collapse navbar-main-collapse">
                  <ul class="nav navbar-nav navbar-right">
                      <li class=" ">
                          <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=inicio" class="">
                              Inicio
                          </a>
                      </li>
                      <li class=" ">
                          <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=contratos" class="">
                              Contratos
                          </a>
                      </li>
                      <li class=" ">
                          <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=historico" class="">
                              Historico
                          </a>
                      </li>
                      <li class=" ">
                          <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=hesperios" class="">
                              Hesperios
                          </a>
                      </li>
                      <li class=" ">
                          <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=canjes" class="">
                              Canjes
                          </a>
                      </li>
                      <li class=" dropdown" style="padding-right: 10px;">
                          <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                              Mi cuenta
                              <span class="caret"></span>
                          </a>
                          <span class="dropdown-arrow"></span>
                          <ul class="dropdown-menu">
                              <li class=" ">
                                  <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=perfil" class="">
                                      Mis datos
                                  </a>
                              </li>
                              <li class=" ">
                                  <a href="http://hoteleshesperia.com.ve/hesperiapartners" class="">
                                      Salir
                                  </a>
                              </li>
                               
                          </ul>
                      </li>     
                      
                  </ul>
              </div><!-- /.navbar-collapse -->
            
          </nav><!-- /navbar -->
        </div>
      </div> <!-- /row -->
    <?php 
    }
    ?>
    

      <?php
      switch ($go) {
          case "inicio":
              include_once('pages/inicio.php');
              break;
          case "contratos":
            echo '<div class="container">';
              include_once('pages/contratos.php');
              break;              
            echo '</div>';
          case "historico":
            echo '<div class="container">';
              include_once('pages/historico.php');
              break;
            echo '</div>';
          case "hesperios":
            echo '<div class="container">';
              include_once('pages/hesperios.php');
              break;
            echo '</div>';
          case "paquetes":
            echo '<div class="container">';
              include_once('pages/paquetes.php');
              break;
            echo '</div>';
          case "reservar":
            echo '<div class="container">';
              include_once('pages/reservar.php');
              break;
            echo '</div>';
          case "canjes":
            echo '<div class="container">';
              include_once('pages/canjes.php');
              break;
            echo '</div>';
          case "canjear":
            echo '<div class="container">';
              include_once('pages/canjear.php');
              break;
            echo '</div>';
          case "contacto":
            echo '<div class="container">';
              include_once('pages/contactanos.php');
              break;
            echo '</div>';
          case "perfil":
            echo '<div class="container">';
              include_once('pages/perfil.php');
              break;
            echo '</div>';
      }
      ?>

    

    <script src="https://hoteleshesperia.com.ve/hesperiapartners/dist/js/vendor/jquery.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/hesperiapartners/dist/js/vendor/video.js"></script>
    <script src="https://hoteleshesperia.com.ve/hesperiapartners/dist/js/flat-ui.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/hesperiapartners/docs/assets/js/application.js"></script>

    <script src="https://hoteleshesperia.com.ve/hesperiapartners/assets/javascript/contratos.js"></script>
    <script src="https://hoteleshesperia.com.ve/hesperiapartners/assets/vendor/isotope/jquery.isotope.js"></script>
    <script src="https://hoteleshesperia.com.ve/hesperiapartners/assets/javascript/pages/portfolio.js"></script>

    <!-- <script>
      videojs.options.flash.swf = "dist/js/vendors/video-js.swf"
    </script> -->
    <?php
    }
    ?>
   
  </body>
</html>