<?php
echo '
	<div class="container">
    <div class="row">
        <div class="col-sm-7">
            <h4 class="headline"><span>Histórico de Reservas <span class="fui-calendar" /></span> </h4>
            <p class="text-justify">En esta sección usted podrá visualizar las reservas realizadas y los Hesperios obtenido en relación al importe de las mismas.</p>
            <!-- <p><a href="#" class="btn btn-lg btn-primary">Contact Us</a></p> -->
        </div>
        <div class="col-sm-5">
            <div class="contact-banner">
                <h3 class="banner-title">¿Cuánto equivale un Hesperio?</h3>
                <ul>
                    <li><span class="fui-check-inverted"></span> Un 5% de las ventas netas se convierten en Hesperios, siendo 1 Hesperio es igual a Bs. 1 </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="col-md-6">
                <label for="desde">Desde</label>
                <input
                    name="login"
                    type="date"
                    class="form-control signin-field"
                    placeholder="Desde"
                    id="desde" data-toggle="tooltip" data-placement="bottom" title="Indique la fecha desde donde desea consultar"/>
            </div>
            <div class="col-md-6">
                <label for="hasta">Hasta</label>
                <input
                    name="login"
                    type="date"
                    class="form-control signin-field"
                    placeholder="Hasta"
                    id="hasta" data-toggle="tooltip" data-placement="bottom" title="Indique la fecha hasta donde desea consultar"/>
                
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="row our-team">
        <div class="col-sm-12">
            <div class="row">
                <table id="example" class="table table-striped table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Localizador</th>
                        <th>Agente</th>
                        <th>Importe neto</th>
                        <th>Hesperios</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Localizador</th>
                        <th>Agente</th>
                        <th>Importe neto</th>
                        <th>Hesperios</th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>01/04/2017</td>
                        <td>1-0000001</td>
                        <td>John Pérez</td>
                        <td>Bs. 150.000,00</td>
                        <td>7.500</td>
                        <td><a class="fui-search" data-toggle="modal" data-target=".bs-example-modal-sm-1" /></td>
                    </tr>
                    <tr>
                        <td>01/04/2017</td>
                        <td>1-0000002</td>
                        <td>Aurora Sierra</td>
                        <td>Bs. 183.000,00</td>
                        <td>9.150</td>
                        <td><a class="fui-search" data-toggle="modal" data-target=".bs-example-modal-sm-2" /></td>
                    </tr>
                    <tr>
                        <td>04/04/2017</td>
                        <td>1-0000003</td>
                        <td>John Pérez</td>
                        <td>Bs. 295.000,00</td>
                        <td>14.750</td>
                        <td><a class="fui-search" data-toggle="modal" data-target=".bs-example-modal-sm-3" /></td>
                    </tr>
                    <tr>
                        <td>05/04/2017</td>
                        <td>1-0000004</td>
                        <td>John Pérez</td>
                        <td>Bs. 200.000,00</td>
                        <td>10.000</td>
                        <td><a class="fui-search" data-toggle="modal" data-target=".bs-example-modal-sm-4" /></td>
                    </tr>
                </tbody>
            </table>
                
            </div>
        </div>
    </div>

</div>

<div class="modal fade bs-example-modal-sm-1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="contact-banner">
        <!-- <button type="button" class="close fui-cross" data-dismiss="modal" aria-hidden="true"></button> -->
                    <h3 class="banner-title">Detalle de Reserva <br> 1-0000001</h3>
                    <ul>
                        <li>Hotel: Hesperia WTC Valencia</li>
                        <li>Tipo: Habitación</li>
                        <li>Huésped: María López</li>
                        <li>Cédula: V-1.000.000</li>
                        <li>Teléfono: (+58) 424 400 00 00</li>                        
                        <li>Concepto: Alojamiento</li>
                        <li>Régimen: Solo Desayuno</li>
                        <li>Check in: 14-04-2017</li>
                        <li>Check out: 17-04-2017</li>
                        <li>Room Night: 3</li>
                    </ul>
                </div>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-sm-2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="contact-banner">
        <!-- <button type="button" class="close fui-cross" data-dismiss="modal" aria-hidden="true"></button> -->
                    <h3 class="banner-title">Detalle de Reserva <br> 1-0000002</h3>
                    <ul>
                        <li>Hotel: Hesperia Playa El Agua</li>
                        <li>Tipo: Habitación</li>
                        <li>Huésped: Gabriel Noguera</li>
                        <li>Cédula: V-19.000.000</li>
                        <li>Teléfono: (+58) 424 400 00 00</li>                        
                        <li>Concepto: Alojamiento</li>
                        <li>Check in: 20-05-2017</li>
                        <li>Check out: 22-05-2017</li>
                        <li>Régimen: Pensión completa</li>
                        <li>Room Night: 2</li>
                    </ul>
                </div>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-sm-3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="contact-banner">
        <!-- <button type="button" class="close fui-cross" data-dismiss="modal" aria-hidden="true"></button> -->
                    <h3 class="banner-title">Detalle de Reserva <br> 1-0000003</h3>
                    <ul>
                        <li>Hotel: Hesperia WTC Valencia</li>
                        <li>Tipo: Habitación</li>
                        <li>Huésped: Jesús Oropeza</li>
                        <li>Cédula: V-20.000.010</li>
                        <li>Teléfono: (+58) 424 400 00 00</li>                        
                        <li>Concepto: Alojamiento</li>
                        <li>Check in: 20-05-2017</li>
                        <li>Check out: 22-05-2017</li>
                        <li>Régimen: Solo Desayuno</li>
                        <li>Room Night: 2</li>
                    </ul>
                </div>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-sm-4" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="contact-banner">
        <!-- <button type="button" class="close fui-cross" data-dismiss="modal" aria-hidden="true"></button> -->
                    <h3 class="banner-title">Detalle de Reserva <br> 1-0000004</h3>
                    <ul>
                        <li>Hotel: Hesperia WTC Valencia</li>
                        <li>Tipo: Habitación</li>
                        <li>Huésped: Rafaél Vivas</li>
                        <li>Cédula: V-18.000.000</li>
                        <li>Teléfono: (+58) 424 400 00 00</li>                        
                        <li>Concepto: Alojamiento</li>
                        <li>Régimen: Solo Desayuno</li>
                        <li>Check in: 19-04-2017</li>
                        <li>Check out: 24-04-2017</li>
                        <li>Room Night: 5</li>
                    </ul>
                </div>
    </div>
  </div>
</div>
';
?>