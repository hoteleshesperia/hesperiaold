<?php

echo '
<section id="layout-title">
    <div class="container">
        <h4>Fin de Semana Relax</h4>
        <h6>Hesperia WTC Valencia</h6>
    </div>
</section>

<div class="container shop-item">
    <div class="row">
        <div class="col-sm-12">

            <div class="row">
                <div class="col-sm-6">

                    <div id="shopImages" class="shop-images">
                        <div>
                            <a href="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/pack-1.jpg">
                                <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/pack-1.jpg" class="img-responsive main" alt="">
                            </a>
                        </div>
                        <div>
                            <a href="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/pack-2.jpg">
                                <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/pack-2.jpg" class="img-responsive" alt="">
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">

                    <p class="text-muted">Especial para relajarse en pareja de una manera 5 estrellas como solo <strong>Hesperia WTC Valencia</strong> lo puede ofrecer.</p>
                    <span class="text-muted reviews">(12 Reservas hasta el momento)</span>
                    <br>
                    <ul class="rating list-inline">
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                    </ul>
                    
                    <div>
                        <div class="price-block">
                            <span class="price">VEF 85.000,00</span>
                        </div>
                    </div>
                    <a class="btn btn-info" href="#">Reservar</a>
                </div>

            </div>

            <!-- Item Description -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="headline"><span>¿Quiéres saber más?</span></h4>
                    <p><i>Una escapada en pareja es siempre bienvenida y Hesperia WTC Valencia complace tus ideas de una forma muy especial y relajante para tí y tu acompañante.</i></p>
                    <div class="col-sm-1">
                        <p>Incluye: </p>
                    </div>
                    <div class="col-sm-11">
                        <p><span class="fui-check-inverted"></span> Alojamiento para dos(2) adultos en Habitación Deluxe.</p>
                        <p><span class="fui-check-inverted"></span> Desayuno tipo buffet para dos(2) adultos en Restaurante Atmósfera.</p>
                        <p><span class="fui-check-inverted"></span> Circuito de relajación para dos(2) adultos en nuestro maravilloso Spa.</p>
                        <p><span class="fui-check-inverted"></span> Acceso al área de piscina.</p>
                        <p><span class="fui-check-inverted"></span> Acceso al gimnasio.</p>
                        <p><span class="fui-check-inverted"></span> Late check-out: 4 p.m.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="headline"><span>Escapadas similares</span></h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="shop-product  featured">
                                <a href="#"><img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/spa.png" class="img-responsive" alt=""></a>
                                <a href="#"><h6>Relajación Deluxe</h6></a>
                                <p class="price">
                                    <span class="old">VEF 100.000,00</span>
                                    <span class="new">VEF 80.000,00</span>
                                </p>
                                <a href="#" class="btn btn-sm btn-info">Más información</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="shop-product">
                                <a href="#"><img style="border-radius: 100%; border: 4px solid #BDBDBD;" 
                        src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/luna_miel_esp.png" alt="Chat" class="tile-image"></a>
                                <a href="#"><h6>Escapada Romántica</h6></a>
                                <p>
                                    VEF 90.000,00
                                </p>
                                <a href="#" class="btn btn-sm btn-info">Más información</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
';

?>