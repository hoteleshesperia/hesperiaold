<?php
echo'
<div id="layout-content" >
            <div class="signin-container" >
                <div class="container">

                    <div class="row">
                            <div class="signin">
                                <div class="signin-screen">
                                    <div class="container-fuild" id="caja-reserva">
  <h4 class="text-center">Reserve con Hesperia Partners</h4>
  <!--<p class="text-center">-->
    <form class="form-inline text-center" >
    <!--  <div class="col-md-6">-->
        <div class="form-group" >
          <select class="form-control" name="idhotel">
           <option value="0">Seleccionar Hotel</option>
           <option value="1">Hesperia WTC Valencia</option>
           <option value="2">Hesperia Edén Club</option>
           <option value="3">Hesperia Playa El Agua</option>
           <option value="5">Hesperia Isla Margarita</option>
         </select>
       </div>
       <div class="form-group">
         <input name="checkin" type="date" class="form-control" id="exampleInputName2" placeholder="Check In">
       </div>
       <div class="form-group">
         <input name="checkout" type="date" class="form-control" id="exampleInputEmail2" placeholder="Check Out">
       </div>
       <button type="submit" class="btn btn-primary">Reservar</button>
    <!--</div>-->
   </form>
 <!--</p>-->
</div>
                                </div>
                            </div>

                    </div>

                </div>
            </div>
        </div>
<div class="container" >
  <section id="layout-title" style="background-color:white;">
    
        <div class="row">
            <div class="col-sm-7">
                <h4 class="headline"><span>Nuestros paquetes</span></h4>
            </div>
            <div class="btn-toolbar pull-right col-sm-5">
                <div class="btn-group" id="filters">
                    <a class="btn btn-primary active" href="#filter" class="selected" data-filter="*"><span class="fui-list"></span> Todos</a>
                    <a class="btn btn-primary" href="#filter" data-filter=".nature">Spa</a>
                    <a class="btn btn-primary" href="#filter" data-filter=".people">Románticos</a>
                    <a class="btn btn-primary" href="#filter" data-filter=".scenery">Navidad</a>
                </div>
            </div>
        </div>
    <p>Para los de corazón viajero, esos que escuchan una vocecita que impulsa a visitar destinos impresionantes.</p>
</section>

    

    <!-- start: Portfolio -->
    <div id="portfolioItems" class="row">

        <div class="col-md-3 portfolio-item nature people">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/hwtc.jpg" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=reservar" class="btn btn-info btn-block">Relajante <br><small>Hesperia WTC Valencia</small></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 portfolio-item world scenery">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/navidad-hap.png" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="#" class="btn btn-info btn-block">Navidad Mágica <br><small>Hesperia Playa El Agua</small></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 portfolio-item scenery people">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/navidad-him.png" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="#" class="btn btn-info btn-block">Escapada Romántica<br><small>Hesperia Isla Margarita</small></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 portfolio-item people">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/romantico.jpg" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="#" class="btn btn-info btn-block">Alojamiento Deluxe<br><small>Hesperia Edén Club</small></a>
                </div>
            </div>
        </div>

    </div>

</div>
';
?>