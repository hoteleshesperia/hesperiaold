<?php
echo '
<div class="row">
        <div class="col-sm-12">
            <h4 class="headline"><span>Perfil <span class="fui-user" /></span> </h4>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            
            <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/logo_scape.png" width="260" height="200" alt="" />
        </div>

        <div class="col-md-9">
            <form role="form">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" value="Scape Travel, C.A.">
                </div>
                <div class="form-group">
                    <label for="name">Identificación fiscal</label>
                    <input type="text" class="form-control" id="name" value="J-0000000-1">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Escribe tu email">
                </div>
                <div class="form-group">
                    <label for="message">Dirección</label>
                    <textarea class="form-control" rows="7" id="message" value="Escribe la dirección de tu Agencia" style="resize: none"></textarea>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-lg btn-info">Actualizar Datos</button>
                    </div>
                </div>
            </form>
            <br /><br />

        </div>

        

    </div>
</div>
';
?>