<?php
echo'
<section id="layout-title">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <h3>Nuestros Paquetes</h3>
            </div>
            <div class="btn-toolbar pull-right col-sm-5">
                <div class="btn-group" id="filters">
                    <a class="btn btn-primary active" href="#filter" class="selected" data-filter="*"><span class="fui-list"></span> Todos</a>
                    <a class="btn btn-primary" href="#filter" data-filter=".nature">Spa</a>
                    <a class="btn btn-primary" href="#filter" data-filter=".people">Románticos</a>
                    <a class="btn btn-primary" href="#filter" data-filter=".scenery">Navidad</a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <h5 class="text-justify">Para los de corazón viajero, esos que escuchan una vocecita que impulsa a visitar destinos impresionantes.</h5>

    <!-- start: Portfolio -->
    <div id="portfolioItems" class="row">

        <div class="col-md-3 portfolio-item nature people">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/hwtc.jpg" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=reservar" class="btn btn-info btn-block">Relajante <br><small>Hesperia WTC Valencia</small></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 portfolio-item world scenery">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/navidad-hap.png" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="#" class="btn btn-info btn-block">Navidad Mágica <br><small>Hesperia Playa El Agua</small></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 portfolio-item scenery people">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/navidad-him.png" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="#" class="btn btn-info btn-block">Escapada Romántica<br><small>Hesperia Isla Margarita</small></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 portfolio-item people">
            <div class="picture">
                <a class="image" href="#" title="Title">
                    <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/romantico.jpg" width="260" height="200" alt="" />
                </a>
                <div class="description">
                    <a href="#" class="btn btn-info btn-block">Alojamiento Deluxe<br><small>Hesperia Edén Club</small></a>
                </div>
            </div>
        </div>

    </div>
</div>
';
?>