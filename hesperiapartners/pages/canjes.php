<?php
echo '
<div class="container">

    <h3 class="headline"><span>Sección de canjes <span class="fui-credit-card"/></span></h3>
    <p class="text-justify">No importa cuantos Hesperios tengas en tu cuenta: podrás utilizarlos desde el momento en que los obtienes y canjearlos por noches de alojamiento. ¿Cómo canjear los Hesperios? Podrás redimir tus puntos en el hotel donde han sido generado. </p>
    <div class="row pricing-tiles">

        <div class="col-md-3">
            <div class="tile">
                <img style="border-radius: 100%; border: 4px solid #BDBDBD;" 
                        src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/buffet.png" alt="Infinity-Loop" class="tile-image">
                <h3 class="tile-title">Desayuno Buffet</h3>
                <p>Hesperia WTC Valencia</p>
                <p>Canjea por tan solo <strong>30.000 Hesperios</strong></p>
                <a class="btn btn-info btn-large btn-block" href="https://hoteleshesperia.com.ve/hesperiapartners/index.php?go=canjear">Canjear</a>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="tile">
                <img style="border-radius: 100%; border: 4px solid #BDBDBD;" 
                        src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/navidad-him.png" alt="Infinity-Loop" class="tile-image">
                <h3 class="tile-title">Hospedaje Deluxe</h3>
                <p>Hesperia Isla Margarita</p>
                <p>Canjea por tan solo <strong>150.000 Hesperios</strong></p>
                <a class="btn btn-info btn-large btn-block" data-toggle="modal" data-target=".bs-example-modal-sm">Canjear</a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="tile">
                <img style="border-radius: 100%; border: 4px solid #BDBDBD;" 
                        src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/navidad-hap.png" alt="Pensils" class="tile-image">
                <h3 class="tile-title">Navidad en Grande</h3>
                <p>Hesperia Playa El Agua</p>
                <p>Canjea por tan solo <strong>250.000 Hesperios</strong></p>
                <a class="btn btn-info btn-large btn-block" data-toggle="modal" data-target=".bs-example-modal-sm">Canjear</a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="tile tile-hot">
                <img style="border-radius: 100%; border: 4px solid #BDBDBD;" 
                        src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/luna_miel_esp.png" alt="Chat" class="tile-image">
                <h3 class="tile-title">Noche Romántica</h3>
                <p>Hesperia Edén Club</p>
                <p>Canjea por tan solo <strong>175.000 Hesperios</strong></p>
                <a class="btn btn-info btn-large btn-block" data-toggle="modal" data-target=".bs-example-modal-sm">Canjear</a>
            </div>

        </div>
    </div>

</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="contact-banner">
        <!-- <button type="button" class="close fui-cross" data-dismiss="modal" aria-hidden="true"></button> -->
                    <h3 class="banner-title">¡Sigue acumulando Hesperios!</h3>
                    <ul>
                        <li>Aún no posees los suficientes Hesperios para canjear este paquete</li>
                    </ul>
                </div>
    </div>
  </div>
</div>
';
?>