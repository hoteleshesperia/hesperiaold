<?php
echo '
	<div class="container">
    <div class="row">
        <div class="col-sm-7">
            <h4 class="headline"><span>Hesperios <span class="fui-calendar" /></span> </h4>
            <p class="text-justify">En esta sección usted podrá visualizar el estado de cuenta de los Hesperios generados por Agente.</p>
            <!-- <p><a href="#" class="btn btn-lg btn-primary">Contact Us</a></p> -->
        </div>
        <div class="col-sm-5">
            <div class="contact-banner">
                <h3 class="banner-title">¿Cuánto equivale un Hesperio?</h3>
                <ul>
                    <li><span class="fui-check-inverted"></span> Un 5% de las ventas netas se convierten en Hesperios, siendo 1 Hesperio es igual a Bs. 1 </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <form role="form" >
            <div class="col-md-7">
                <div class="form-group">
                    
                    <select class="form-control testAgente" >
                        <option>Seleccione agente:</option>
                            <option value="1">
                                John Pérez      
                            </option>
                            <option value="2">
                                Aurora Sierra       
                            </option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    <br>
    <br>
    <div class="row our-team">
        <div class="col-sm-12">
            <div class="row">
                <table id="example" class="table table-striped table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Saldo inicial</th>
                        <th>Hesperios generados</th>
                        <th>Hesperios redimidos</th>
                        <th>Saldo final</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Fecha</th>
                        <th>Saldo inicial</th>
                        <th>Hesperios generados</th>
                        <th>Hesperios redimidos</th>
                        <th>Saldo final</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr class="agente agente1">
                        <td>01/04/2017</td>
                        <td>0</td>
                        <td>7.500</td>
                        <td>0</td>
                        <td>7.500</td>
                    </tr>                    
                    <tr class="agente agente1">
                        <td>04/04/2017</td>
                        <td>7.500</td>
                        <td>14.750</td>
                        <td>0</td>
                        <td>22.250</td>
                    </tr>
                    <tr class="agente agente1">
                        <td>05/04/2017</td>
                        <td>22.250</td>
                        <td>10.000</td>
                        <td>0</td>
                        <td>32.250</td>
                    </tr>
                    <tr class="agente agente2">
                        <td>01/04/2017</td>
                        <td>0</td>
                        <td>9.150</td>
                        <td>0</td>
                        <td>9.150</td>
                    </tr>
                </tbody>
            </table>
                
            </div>
        </div>
    </div>

</div>

';
?>