<?php

echo '
<section id="layout-title">
    <div class="container">
        <h4>Desayuno Buffet</h4>
        <h6>Hesperia WTC Valencia</h6>
    </div>
</section>

<div class="container shop-item">
    <div class="row">
        <div class="col-sm-12">

            <div class="row">
                <div class="col-sm-6">

                    <div id="shopImages" class="shop-images">
                        <div>
                            <a href="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/pack-1.jpg">
                                <img src="https://hoteleshesperia.com.ve/hesperiapartners/assets/images/shop/buffet2.jpg" class="img-responsive main" alt="">
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">

                    <p class="text-muted">Al mejor estilo de un desayuno 5 estrellas como solo <strong>Hesperia WTC Valencia</strong> lo puede ofrecer.</p>
                    
                    <ul class="rating list-inline">
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                        <li><i class="icon-star"></i></li>
                    </ul>
                    
                    <div>
                        <div class="price-block">
                            <span class="price">Hesperios 30.000,00</span>
                        </div>
                    </div>
                    <a class="btn btn-info" href="#">Canjear</a>
                </div>

            </div>

            <!-- Item Description -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="headline"><span>¿Quiéres saber más?</span></h4>
                    <p><i>Disfruta de nuestra exquisita gastronomía y desayuna en los espacios relajantes que te ofrece nuestro Restaurante Atmósfera.</i></p>
                    <div class="col-sm-1">
                        <p>Incluye: </p>
                    </div>
                    <br>
                    <div class="col-sm-11">
                        <p><span class="fui-check-inverted"></span> Desayuno tipo buffet para dos(2) adultos en Restaurante Atmósfera.</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
';

?>