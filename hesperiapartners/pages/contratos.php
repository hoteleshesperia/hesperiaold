<?php 
echo '
    <h3 class="headline"><span>Contratos <span class="fui-new"/></span></h3>
    <div class="row">          
            <p>¡Selecciona el Hotel para el que deseas ver el resumen del contrato!</p>
            <form role="form" >
            <div class="col-md-7">
                <div class="form-group">
                    
                    <select class="form-control testContrato" >
                        <option>Seleccione:</option>
                            <option value="0">
                                Hesperia WTC Valencia      
                            </option>
                            <option value="1">
                                Hesperia Playa El Agua       
                            </option>
                            <option value="2">
                                Hesperia Edén Club       
                            </option>
                            <option value="3">
                                Hesperia Isla Margarita       
                            </option>
                    </select>
                </div>
            </div>
            </form>
            <br /><br />
    </div>
    <div class="row">
        <div class="col-md-12 contrato" id="contrato0">
            <div class="contact-banner">
                <h3 class="banner-title"></h3>
                <ul>
                    <li>Tipo de tarifa: Agencia</li>
                    <li>Compañía: Scape Travel</li>
                    <li>Hotel: Hesperia WTC Valencia</li>
                    <li>Teléfono: +58 241 515 30 00</li>
                    <li>Email: <a href="#">partners@hoteleshesperia.com.ve</a></li>
                    <li>Moneda: Bs.</li>
                    <li>Cupo mensual: 30</li>
                    <li>Cupo objetivo (R.N.): 100</li>
                    <li>Descuento sobre Rack: -30%</li>
                    <li>Over: 5%</li>
                    <li>Forma de pago: Crédito</li>
                    <li>Válido hasta el: 31-12-2017</li>
                </ul>
            </div>
        </div>
        <div class="col-md-12 contrato" id="contrato1">
            <div class="contact-banner">
                <h3 class="banner-title"></h3>
                <ul>
                    <li>Tipo de tarifa: Agencia</li>
                    <li>Compañía: Scape Travel</li>
                    <li>Hotel: Hesperia Playa El Agua</li>
                    <li>Teléfono: +58 295 400 71 00</li>
                    <li>Email: <a href="#">partners@hoteleshesperia.com.ve</a></li>
                    <li>Moneda: Bs.</li>
                    <li>Cupo mensual: 30</li>
                    <li>Cupo objetivo (R.N.): 100</li>
                    <li>Descuento sobre Rack: -30%</li>
                    <li>Over: 5%</li>
                    <li>Forma de pago: Crédito</li>
                    <li>Válido hasta el: 31-12-2017</li>
                </ul>
            </div>
        </div>
        <div class="col-md-12 contrato" id="contrato2">
            <div class="contact-banner">
                <h3 class="banner-title"></h3>
                <ul>
                    <li>Tipo de tarifa: Agencia</li>
                    <li>Compañía: Scape Travel</li>
                    <li>Hotel: Hesperia Edén Club</li>
                    <li>Teléfono: +58 295 400 71 00</li>
                    <li>Email: <a href="#">partners@hoteleshesperia.com.ve</a></li>
                    <li>Moneda: Bs.</li>
                    <li>Cupo mensual: 30</li>
                    <li>Cupo objetivo (R.N.): 100</li>
                    <li>Descuento sobre Rack: -35%</li>
                    <li>Over: 5%</li>
                    <li>Forma de pago: Crédito</li>
                    <li>Válido hasta el: 31-12-2017</li>
                </ul>
            </div>
        </div>
        <div class="col-md-12 contrato" id="contrato3">
            <div class="contact-banner">
                <h3 class="banner-title"></h3>
                <ul>
                    <li>Tipo de tarifa: Agencia</li>
                    <li>Compañía: Scape Travel</li>
                    <li>Hotel: Hesperia Isla Margarita</li>
                    <li>Teléfono: +58 295 400 71 11</li>
                    <li>Email: <a href="#">partners@hoteleshesperia.com.ve</a></li>
                    <li>Moneda: Bs.</li>
                    <li>Cupo mensual: 30</li>
                    <li>Cupo objetivo (R.N.): 100</li>
                    <li>Descuento sobre Rack: -20%</li>
                    <li>Over: 5%</li>
                    <li>Forma de pago: Crédito</li>
                    <li>Válido hasta el: 31-12-2017</li>
                </ul>
            </div>
        </div>
    </div>
';
?>