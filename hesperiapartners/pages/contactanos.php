<?php
echo '
<section id="layout-title">
    <div class="container">
        <h3>Contáctanos</h3>
    </div>
</section>

<div class="container">
    <div class="row">

        <div class="col-md-7">
            <p>Forma parte de Hesperia Partners y podrás realizar reservas en tiempo real con una tarifa especial, bajo confirmación inmediata!</p>
            <form role="form">
                <div class="form-group">
                    <label for="email">Tu email</label>
                    <input type="email" class="form-control" id="email" placeholder="Escribe tu email">
                </div>
                <div class="form-group">
                    <label for="name">Tu nombre</label>
                    <input type="text" class="form-control" id="name" placeholder="Escribe tu nombre">
                </div>
                <div class="form-group">
                    <label for="message">Mensaje</label>
                    <textarea class="form-control" rows="7" id="message" placeholder="Escribe un mensaje" style="resize: none"></textarea>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-lg btn-info">Enviar mensaje</button>
                    </div>
                </div>
            </form>
            <br /><br />

        </div>

        <div class="col-md-5">
            <div class="contact-banner">
                <h3 class="banner-title">Nuestra ubicación</h3>
                <ul>
                    <li>Av. Salvador Feo La Cruz, Naguanagua.</li>
                    <li>Carabobo, Venezuela.</li>
                    <li>Teléfono: +58 241 515 30 00</li>
                    <li>Email: <a href="#">info.partners@hoteleshesperia.com.ve</a></li>
                </ul>
            </div>

            <div class="google-maps">
              <iframe
                width="500"
                height="450"
                frameborder="0"
                scrolling="no"
                marginheight="0"
                marginwidth="0"
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3971.9814406146297!2d-68.0035459283617!3d10.232544918656282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6fc8245dcdcb3025!2sHesperia+WTC+Valencia!5e0!3m2!1ses-419!2sve!4v1495637423262"></iframe>
            </div>

        </div>

    </div>
</div>
';
?>