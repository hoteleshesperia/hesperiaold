<?php

/* -------------------------------------------------------

Script  bajo los t&eacute;rminos y Licencia

Apache License

Version 2.0, January 2004

https://www.apache.org/licenses/LICENSE-2.0

Autor: Hector A. Mantellini (@Xombra)

Diseño: Angel Cruz (@abr4xas)

ViserProject - @viserproject

-------------------------------------------------------- */

session_start();

//require( 'php_error.php' );

//    \php_error\reportErrors();

include 'include/core.php';

?>

<!DOCTYPE html>

<html lang="<?php echo $lenguaje; ?>">

<head>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link href="css/modern-business.css" rel="stylesheet">

    <link href="css/datepicker.css" rel="stylesheet">

<!--    <link href="css/slider-hotel.css" rel="stylesheet">-->

    <link href="css/lightbox.css" rel="stylesheet" >

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/2016h01e29/js/jquery.js"></script>

    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=es" async defer></script>



    <script>

      var recaptcha1;

      var recaptcha2;

      var myCallBack = function() {

        //Render the recaptcha1 on the element with ID "recaptcha1"

        recaptcha1 = grecaptcha.render('recaptcha1', {

          'sitekey' : '6LdKuBYTAAAAADLj3h3iK8hhJassh_GLOM6MFVk1', //Replace this with your Site key

          'theme' : 'light'

        });

        

        //Render the recaptcha2 on the element with ID "recaptcha2"

        recaptcha2 = grecaptcha.render('recaptcha2', {

          'sitekey' : '6LdKuBYTAAAAADLj3h3iK8hhJassh_GLOM6MFVk1', //Replace this with your Site key

          'theme' : 'light'

        });

      };

    </script>







<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript">

var mobile = (/iPhone|iPad|iPod|android|blackberry|bb|mini|Nokia|PlayBook|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

if (mobile) {

    $( ".my-reserva" ).remove(); // delete reservation form

}





$(document).ready(function(){

    $('[data-toggle="popover"]').popover();

     //alert(latter);

    var nowTemp = new Date();

    var h = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0); 

    nowTemp.setSeconds(86400);

    var hoy = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);





    $('.dpd3').on('click', function(date) {

          var desde = $(this).attr('data-min-date');

             //alert(desde);

         var anio = desde.substring(0,4);

         var mes = desde.substring(5,7);

         mes = parseInt(mes) - 1;

         var dia = desde.substring(8,10);



         var now = new Date(anio, mes, dia, 0, 0, 0, 0);

         if(hoy > now){

            now = hoy;

         }



         var hasta = $(this).attr('data-max-date');

         var anio2 = hasta.substring(0,4);

         var mes2 = hasta.substring(5,7);

         mes2 = parseInt(mes2) - 1;

         var dia2 = hasta.substring(8,10);



         var latter = new Date(anio2, mes2, dia2, 0, 0, 0, 0);



         checkin = $(this).datepicker({

            

          onRender: function(date) {    

            return ((date.valueOf() > now.valueOf()) && (date.valueOf() <= latter.valueOf())) ? '' : 'disabled';

          }

        }).datepicker('show').data('datepicker');

        

    });



 $('#fechaNac').on('click', function(date) {



      $(this).datepicker({

            onRender: function(date) {    

                return (date.valueOf() <= h.valueOf()) ? '' : 'disabled';

              }

      }).datepicker('show').data('datepicker');

  });



 //When page loads...

    $(".tab_content").hide(); //Hide all content

    $("ul.tabs li:first").addClass("active").show(); //Activate first tab

    $(".tab_content:first").show(); //Show first tab content



 $("ul.tabs li").click(function() {



        $("ul.tabs li").removeClass("active"); //Remove any "active" class

        $(this).addClass("active"); //Add "active" class to selected tab

        $(".tab_content").hide(); //Hide all tab content



        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content

        $(activeTab).fadeIn(); //Fade in the active ID content

        return false;

    });



});



</script>

<?php

META($charset,$lenguaje,$timezone_set,$region,$go,$data,$hostname,$user,$password,$db_name,$mysqli); ?>

</head>

<!--<body onload="document.getElementById('cargando').style. display='none';">-->

    <body>

<!--<div id="cargando" style="width: 100%; height: 100%; text-align: center;z-index:9999"><img src="https://hoteleshesperia.com.ve/2016h01e29/img/iconos/loading.gif" style="width:60px" alt="Cargando..."></div>-->

<noscript><p>Debe habilitar el uso de <strong>Javascript</strong>, para poder usar muchas de las funciones del sitio</p></noscript>

<?php flush(); ?>

    <nav class="navbar navbar-default" role="navigation">

        <div class="container">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-hesperia" aria-expanded="false">

                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

            </button>

            <a class="navbar-brand hidden-lg hidden-md" href="index.php">

                <img src="https://hoteleshesperia.com.ve/2016h01e29/img/logo/logohesperiahotels-m.png" class="img-responsive logo-main">

            </a>

            <a class="navbar-brand visible-lg visible-md" href="index.php">

                <img src="https://hoteleshesperia.com.ve/2016h01e29/img/logo/logohesperiahotels.png" class="img-responsive logo-main">

            </a>

        </div>

            <div class="collapse navbar-collapse" id="menu-hesperia">

                <ul class="nav navbar-nav">

                    <li><a href="index.php">Inicio</a></li>

                    <li>

                    <?php

//                        if ($_GET['principio'] == 0) { $_GET['sucursal'] = 0;}

//    esta parte muestra solo el link de nosotros general sin importar cual hotel fue seleccionado

echo '<a href="index.php?go=0/nosotros/0/'.FSPATH("Hesperia Hotels & Resorts Venezuela").'" title="Hesperia Hotels & Resorts Venezuela">Nosotros</a>'; ?>

                    </li>

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hoteles <span class="caret"></span></a>

                        <ul class="dropdown-menu" role="menu">

                            <?php

                            $nombres=array("Hesperia-Eden-Club","Hesperia-Isla-Margarita","Hesperia-Playa-el-Agua","Hesperia-WTC-Valencia");

                            $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";

                            $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

                            $i=0;

                            while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC))

                                { echo '

                                    <li>

                                        <a href="index.php?go='.$rows["id"].'/detalle/0/'.FSPATH($rows["nombre_sitio"]).'" target="_self" title=" '.$rows["nombre_sitio"].'">'.$rows["nombre_sitio"].'</a>

                                    </li>';

                                    $i++;

                                }

echo     '</ul>

                    </li>';

echo '

                    <li><a href="index.php?go=0/eventos/0/'.FSPATH("Eventos").'" title="Eventos"> Reuniones y Eventos</a></li>

                    <li><a href="index.php?go=0/experiencias/0/'.FSPATH("Experiencias").'" title="Experiencias">Experiencias</a></li>

                    <li><a href="index.php?go=0/paquetes/0/'.FSPATH("Paquetes ").'" title="Paquetes">Paquetes</a></li>

                    <li><a href="index.php?go=0/partai" title="Partai">Partai</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">';

                    if (!isset($_SESSION["referencia"])){

                echo '

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-users hidden-xs hidden-sm"></i>Mi Cuenta<span class="caret"></span></a>

                        <ul class="dropdown-menu" role="menu">

                            <li>

                                <a id="modal-433854" href="#modal-container-433854" data-toggle="modal" title="Sesión de Usuarios registrados">Iniciar sesión

                                            </a>

                            </li>

                            <li>

                                <a id="modal-433854" href="#modal-container-200917" data-toggle="modal" title="Recuperar Clave">Recuperar Clave

                                            </a>

                            </li>

                        </ul>

                    </li>

                    <li><a id="modal-433854" href="#modal-container-433666" data-toggle="modal" title="Registro de nuevo usuarios"><i class="fa fa-user hidden-xs hidden-sm"></i> Registrarse

                        </a>

                    </li>

                    '; }

                else

        {

                echo '

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                            Usuario: <strong>'.$_SESSION["nombre"].'</strong><span class="caret"></span>

                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>

                                <a href="index.php?go='.$_GET['sucursal'].'/perfil/0/'.FSPATH("Mi Cuenta").'" title="Ver mi Cuenta">

                                    <i class="fa fa-users"></i> Mi Cuenta

                                </a>

                            </li>';

                      if (isset($_SESSION["referencia"])){

                              if ($_SESSION["nivel"] <= 2){

                                echo '<li><a href="panel/home.php" target="_blank" title="Panel de Control"><i class="fa fa-server hidden-xs hidden-sm"></i> Panel de Control </a>

                    </li>';

                                        } }



echo '

                            <li>

                                <a href="index.php?go='.$_GET['sucursal'].'/salir/0/'.FSPATH('Salir del Sistema').'" title="Salir Del Sistema | Cerrar Sesion">

                                    <i class="fa fa-sign-out"></i> Salir

                                </a>

                            </li>

                        </ul>

                    </li>'; }

echo '

                </ul>

            </div>

        </div>

    </nav>';

    if ($go != 'perfil' && $go != 'contacto' && $go != 'prensa' && $go != 'reserva' && $go != 'compra' && $go != 'reservapaquete' && $go != 'eventos_hesperia' && $go != 'paquetes' && $go != 'eventos_wtc' && $go != 'eventos_him' && $go != 'eventos_hpa' && $go != 'eventos_eden' && $go != 'partai' && $go != 'vip'

                    && $go != 'accesoVip' && $go != 'nuevos_paquetes') {

//        if (DETECTAR()) {

            CARROUSEL($mysqli,$data,$hostname,$user,$password,$db_name,$go);

//        }

    }

echo   '<div class="container"><br/>

            <div class="row hidden-lg hidden-md">

                <div class="col-lg-12">';

                if ($go != 'perfil' &&

                    $go != 'contacto' &&

                    $go != 'prensa'

                    && $go != 'reserva'

                    && $go != 'compra'

                    && $go != 'reservapaquete'

                    && $go != 'paquetes'

                    && $go != 'eventos_hesperia' 

                    && $go != 'eventos_wtc' 

                    && $go != 'eventos_him' 

                    && $go != 'eventos_hpa' 

                    && $go != 'eventos_eden' 

                    && $go != 'partai'

                    && $go != 'vip'

                    && $go != 'accesoVip'

                    && $go != 'nuevos_paquetes') {

                           CREAR_OTRA_RESERVA($mysqli,$hostname,$user,$password,$db_name);

                }

    echo'        </div>

            </div>

        </div><!-- /container cor-->';

echo '<div class="container">';

    SELECTOR($go,$hostname,$user,$password,$db_name,$data,$mysqli);

    LOGOS($mysqli,$data,$hostname,$user,$password,$db_name);

    echo '</div><!-- /container S-->';

    FOOTER($mysqli,$data,$hostname,$user,$password,$db_name,$go);

if (empty($_SESSION["referencia"]))

{ INICIO_SESSION($data,$hostname,$user,$password,$db_name); }

if (empty($_SESSION["referencia"]))

{ REGISTRO($data,$hostname,$user,$password,$db_name); }

if (empty($_SESSION["referencia"]))

{ RECUPERAR_CLAVE($data); }

?>

<div class="contcookies" style="display: none;">Este sitio, como la mayoría, usa cookies. Si continua navegando en nuestro sitio entendemos que acepta las <a href="index.php?go=politicas">Política de uso</a>. <a href="#" class="cookiesaceptar">Aceptar</a></div>

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/2016h01e29/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/2016h01e29/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/2016h01e29/js/lightbox.min.js"></script>

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/2016h01e29/js/custom.js"></script>

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/2016h01e29/js/validacionPago.js"></script>

<?php GOOGLE_ANALYTICS("UA-67251481-1"); ?>

</body>

</html>

<?php BUFFER_FIN($mysqli); ?>

