<?php
session_start();

include_once('include/core.php');
include_once('funciones_v2.php');
include_once('include/funcion_paquetes.php');
include_once('blg/blog/funcionesBlog.php');
include_once('geoiploc.php'); 

  if (empty($_POST['checkip']))
  {
        $ip = $_SERVER["REMOTE_ADDR"]; 
  }
  else
  {
        $ip = $_POST['checkip']; 
  }
?>
<!DOCTYPE html>

<html lang="<?php echo $lenguaje; ?>">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
   
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <link href="https://hoteleshesperia.com.ve/css/datepicker.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/bootstrap-timepicker.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/carousel.css" rel="stylesheet" >
<!--    <link href="css/slider-hotel.css" rel="stylesheet">-->
    <link href="https://hoteleshesperia.com.ve/css/lightbox.css" rel="stylesheet" >
    
    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=es" async defer></script>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/js/jquery.price_format.2.0.js"></script>
    <link href="https://hoteleshesperia.com.ve/css/modern-business.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://hoteleshesperia.com.ve/blg/blog.js"></script>

    <link href="https://hoteleshesperia.com.ve/PgwSlideshow-master/pgwslideshow.css" rel="stylesheet">
    
    <link href="https://hoteleshesperia.com.ve/PgwSlideshow-master/pgwslideshow_light.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/PgwSlideshow-master/pgwslideshow_light.min.css" rel="stylesheet">
    
    <link href="https://hoteleshesperia.com.ve/css/principal.css" rel="stylesheet">

    <script type="text/javascript" src="https://hoteleshesperia.com.ve/PgwSlideshow-master/pgwslideshow.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/PgwSlideshow-master/pgwslideshow.min.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/media_center.js"></script>

    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/HesperiaQuality/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
        
    <link href="https://hoteleshesperia.com.ve/css/jquery.dataTables.min.css" rel="stylesheet">
    
    <script>
      var recaptcha1;
      var recaptcha2;
      var myCallBack = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
        recaptcha1 = grecaptcha.render('recaptcha1', {
          'sitekey' : '6LdKuBYTAAAAADLj3h3iK8hhJassh_GLOM6MFVk1', //Replace this with your Site key
          'theme' : 'light'
        });
        
        //Render the recaptcha2 on the element with ID "recaptcha2"
        recaptcha2 = grecaptcha.render('recaptcha2', {
          'sitekey' : '6LdKuBYTAAAAADLj3h3iK8hhJassh_GLOM6MFVk1', //Replace this with your Site key
          'theme' : 'light'
        });
      };
    </script>



<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://hoteleshesperia.com.ve/js/TimeCircles.js"></script>
<link href="https://hoteleshesperia.com.ve/css/TimeCircles.css" rel="stylesheet">   
<script type="text/javascript" src="https://hoteleshesperia.com.ve/js/principal.js"></script>
    
<!--SOCIAL ICONS-->
<link rel="stylesheet" type="text/css" href="https://hoteleshesperia.com.ve/jssocials-1.2.1/dist/jssocials.css" />
<link rel="stylesheet" type="text/css" href="https://hoteleshesperia.com.ve/jssocials-1.2.1/dist/jssocials-theme-flat.css" />
<script src="https://hoteleshesperia.com.ve/jssocials-1.2.1/dist/jssocials.min.js"></script>

<?php
META($charset,$lenguaje,$timezone_set,$region,$go,$data,$hostname,$user,$password,$db_name,$mysqli); 
if(!isset($_SESSION["locationGeo"]))
        $_SESSION["locationGeo"] = getCountryFromIP($ip, "code");
    if(!isset($_SESSION["respondioModal"]))
        $_SESSION["respondioModal"] = 0;
//$_SESSION["locationGeo"] = getCountryFromIP($ip, "code");
/*if($ip == '190.111.122.147'){
  $_SESSION["locationGeo"] = 'US';
}*/
?>
</head>
<!--<body onload="document.getElementById('cargando').style. display='none';">-->
    <body>
        <?php
        echo '<input type="hidden" id="valIP" value="'.$_SESSION["locationGeo"].'" />
        <input type="hidden" id="respondioModalMoneda" value="'.$_SESSION["respondioModal"].'" />';
        ?>
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
<!--<div id="cargando" style="width: 100%; height: 100%; text-align: center;z-index:9999"><img src="https://hoteleshesperia.com.ve/img/iconos/loading.gif" style="width:60px" alt="Cargando..."></div>-->
<noscript><p>Debe habilitar el uso de <strong>Javascript</strong>, para poder usar muchas de las funciones del sitio</p></noscript>
<?php flush(); ?>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
        <div class="navbar-header">
           <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-hesperia" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
            <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-left">
              <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md pull-right" href="https://hoteleshesperia.com.ve/">
                <img src="https://hoteleshesperia.com.ve/img/logo/logohesperiahotels-m.png" class="img-responsive logo-main">
            </a>
            <a class="navbar-brand visible-lg visible-md" href="https://hoteleshesperia.com.ve/">
                <img src="https://hoteleshesperia.com.ve/img/logo/logohesperiahotels.png" class="img-responsive logo-main">
            </a>
        </div>
            <div class="side-collapse in" id="menu-hesperia">
                <ul class="nav navbar-nav">
                    <li><a href="https://hoteleshesperia.com.ve/">Inicio</a></li>
                    <li>
                    <?php
//                        if ($_GET['principio'] == 0) { $_GET['sucursal'] = 0;}
//    esta parte muestra solo el link de nosotros general sin importar cual hotel fue seleccionado
// <a href="https://hoteleshesperia.com.ve/nosotros" title="Hesperia Hotels & Resorts Venezuela">Nosotros</a>
 ?>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hoteles <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                            $nombres=array(
                                "Hesperia-Eden-Club",
                                "Hesperia-Isla-Margarita",
                                "Hesperia-Maracay",
                                "Hesperia-Playa-el-Agua",
                                "Hesperia-WTC-Valencia" 
                                );
                            $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";
                            $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
                            $i=0;
                            while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC))
                                { echo '
                                    <li>
                                        <a href="https://hoteleshesperia.com.ve/'.$nombres[$i].'" target="_self" title=" '.$rows["nombre_sitio"].'">'.$rows["nombre_sitio"].'</a>
                                    </li>';
                                    $i++;
                                }
echo     '</ul>
                    </li>';
echo '
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Paquetes<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!--<li>
                                <a href="https://hoteleshesperia.com.ve/semana-santa" title="Semana Santa">Semana Santa</a>
                            </li>-->
                            <!--<li>
                                <a href="https://hoteleshesperia.com.ve/mes-de-la-madre" title="Mes de la Madre">Mes de las Madres</a>
                            </li>-->

                            <li>
                                <a href="https://hoteleshesperia.com.ve/vuelo-hotel" title="Vuelo + Hotel">Vuelo + Hotel</a>
                            </li>

                            <li>
                                <a href="https://hoteleshesperia.com.ve/promos" title="Promos">Promos</a>
                            </li>

                            <li>
                                <a href="https://hoteleshesperia.com.ve/romanticos" title="Romanticos">Romanticos</a>
                            </li>
                            
                            
                            <li>
                                <a href="https://hoteleshesperia.com.ve/spa" title="Spa">Spa</a>
                            </li>

                            <!--<li>
                                <a href="https://hoteleshesperia.com.ve/partai2017" title="Partai 2017">Partai 2017</a>
                            </li>-->

                            <!--<li>
                                <a href="https://hoteleshesperia.com.ve/pack-gastronomia" title="Gastronomia">Gastronomia</a>
                            </li>
                            <li>
                                <a href="https://hoteleshesperia.com.ve/pack-reuniones" title="Reuniones">Reuniones</a>
                            </li>-->
                            <!-- li>
                                <a href="https://hoteleshesperia.com.ve/paquetes" title="Todos los paquetes">Todos</a>
                            </li -->
                        </ul>
                    </li>
                    <!--<li><a href="https://hoteleshesperia.com.ve/atrapame" title="Atrápame"> Atrápame</a></li>-->
                    <li><a href="https://hoteleshesperia.com.ve/reuniones-eventos" title="Eventos"> Reuniones y Eventos</a></li>
                    <li><a href="https://hoteleshesperia.com.ve/restaurantes">Restaurantes</a></li>
                    <li><a href="https://hoteleshesperia.com.ve/mediacenter">Media Center</a></li>
                    <!--<li><a href="https://hoteleshesperia.com.ve/partai">Partai</a></li>-->
                </ul>
                <ul class="nav navbar-nav navbar-right">';
                    if (!isset($_SESSION["referencia"])){
                echo '
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-users hidden-xs hidden-sm"></i>Mi Cuenta<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a id="modal-433854" href="#modal-container-433854" data-toggle="modal" title="Sesión de Usuarios registrados">Iniciar sesión
                                            </a>
                            </li>
                            <li>
                                <a id="modal-433854" href="#modal-container-200917" data-toggle="modal" title="Recuperar Clave">Recuperar Clave
                                            </a>
                            </li>
                        </ul>
                    </li>
                    <li><a id="modal-433854" href="#modal-container-433666" data-toggle="modal" title="Registro de nuevo usuarios"><i class="fa fa-user hidden-xs hidden-sm"></i> Registrarse
                        </a>
                    </li>
                    '; }
                else
        {
                echo '
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Usuario: <strong>'.$_SESSION["nombre"].'</strong><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="https://hoteleshesperia.com.ve/perfil/mi-cuenta" title="Ver mi Cuenta">
                                    <i class="fa fa-users"></i> Mi Cuenta
                                </a>
                            </li>';
                      if (isset($_SESSION["referencia"])){
                              if ($_SESSION["nivel"] <= 2){
                                echo '<li><a href="panel/home.php" target="_blank" title="Panel de Control"><i class="fa fa-server hidden-xs hidden-sm"></i> Panel de Control </a>
                    </li>';
                                        } }

echo '
                            <li>
                                <a href="https://hoteleshesperia.com.ve/logout" title="Salir Del Sistema | Cerrar Sesion">
                                    <i class="fa fa-sign-out"></i> Salir
                                </a>
                            </li>
                        </ul>
                    </li>'; }
echo '
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <div class="container-fuild">';
    if ($go != 'perfil' && $go != 'contacto' && $go != 'prensa' && $go != 'reserva' && $go != 'compra' && $go != 'reservapaquete' && $go != 'eventos_hesperia' && $go != 'paquetes' && $go != 'eventos_wtc' && $go != 'eventos_him' && $go != 'eventos_hpa' && $go != 'eventos_eden' && $go != 'partai' && $go != 'vip'
                    && $go != 'accesoVip' && $go != 'nuevos_paquetes') {
//        if (DETECTAR()) {
            if ($go=="index" || $go==" ") {
                banner_v2($mysqli, $sql,$hostname,$user,$password,$db_name,"home");
            }else if($go=="blog"){
              mostrarBlogMain($hostname,$user,$password,$db_name, "index");
            }else if(strtolower($go)=="resumenhim" || strtolower($go)=="resumenhpa" ||
            strtolower($go)=="resumenhec" || strtolower($go)=="resumenhwtc"
            || strtolower($go)=="carnavalhpa" || strtolower($go)=="carnavalhec"
            || strtolower($go)=="carnavalhim" || strtolower($go)=="partai2017"
            || strtolower($go)=="sanvalentin2017" || strtolower($go)=="hesperiabluehpa"
            || strtolower($go)=="hesperiabluehim" || strtolower($go)=="hesperiabluehec"
            || strtolower($go)=="cupidoweekend" || strtolower($go)=="h-margarita"
            || strtolower($go)=="vuelo-hotel" || strtolower($go) == "paquetes_vuelos"){
                //NADA
            }
             else if ($go!="restaurantes" && $go!="mediacenter" && $go!="noticia"){
                 
                CARROUSEL($mysqli,$data,$hostname,$user,$password,$db_name,$go);
            }
            
//        }
    }
echo   '</div>
        <div class="container"><br/>
            <div class="row hidden-lg hidden-md">
                <div class="col-lg-12">';
                if ($go != 'perfil' &&
                    $go != 'contacto' &&
                    $go != 'prensa'
                    && $go != 'reserva'
                    && $go != 'compra'
                    && $go != 'reservapaquete'
                    && $go != 'paquetes'
                    && $go != 'eventos_hesperia' 
                    && $go != 'eventos_wtc' 
                    && $go != 'eventos_him' 
                    && $go != 'eventos_hpa' 
                    && $go != 'eventos_eden' 
                    && $go != 'partai'
                    && $go != 'vip'
                    && $go != 'accesoVip'
                    && $go != 'nuevos_paquetes'
                    && $go != 'restaurantes'
                    && $go != 'noticia'
                    && $go != 'mediacenter'
                    && $go != 'resumenhim'
                    && $go != 'resumenhpa'
                    && $go != 'resumenhec'
                    && $go != 'resumenhwtc'
                    && $go != 'carnavalhpa'
                    && $go != 'carnavalhec'
                    && $go != 'carnavalhim'
                    && $go != 'partai2017'
                    && $go != 'sanvalentin2017'
                    && $go != 'hesperiabluehpa'
                    && $go != 'hesperiabluehim'
                    && $go != 'hesperiabluehec'
                    && $go != 'paquetes_vuelos') {
                           CREAR_OTRA_RESERVA($mysqli,$hostname,$user,$password,$db_name);
                }
    echo'        </div>
            </div>
        </div><!-- /container cor-->';
echo '<div class="container">';
    SELECTOR($go,$hostname,$user,$password,$db_name,$data,$mysqli);
    LOGOS($mysqli,$data,$hostname,$user,$password,$db_name);
    echo '</div><!-- /container S-->';
    FOOTER($mysqli,$data,$hostname,$user,$password,$db_name,$go);
if (empty($_SESSION["referencia"]))
{ INICIO_SESSION($data,$hostname,$user,$password,$db_name); }
if (empty($_SESSION["referencia"]))
{ REGISTRO($data,$hostname,$user,$password,$db_name); }
if (empty($_SESSION["referencia"]))
{ RECUPERAR_CLAVE($data); }
?>
<div class="contcookies" style="display: none;">Este sitio, como la mayoría, usa cookies. Si continua navegando en nuestro sitio entendemos que acepta las <a href="index.php?go=politicas">Política de uso</a>. <a href="#" class="cookiesaceptar">Aceptar</a></div>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/lightbox.min.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/custom.js"></script>
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/js/validacionPago.js"></script>
    <script>
        $(".selectdata").on("change", function(){
        $(".item").hide();
        var date = $("#date").val();
        var hotel = $("#shotel").val();
        if(date ==="all"){
            $("."+hotel).show();
        }else if(hotel ==="all"){
            $("."+date).show();
        }else{
            $("."+date+"."+hotel).show();
            console.log("."+date+"."+hotel);
        }
        if (date === 'all' && hotel === 'all') {
            $('.item').show();
        }
        });
    </script>    
<?php GOOGLE_ANALYTICS("UA-67251481-1"); ?>
  <div class="modal fade" id="show-modal-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>              
        <div class="modal-body">
          
        </div>
      </div>
    </div>
  </div>

  <div  class="modal fade" id="modal-container-Hblue2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content mymod">

            <div class="modal-body">

                <p class="text-center">Se ha detectado que usted a accedido con una dirección IP extranjera.
                    Por tanto se mostrarán precios en dólares (USD). <br><br>
                    ¿Desea cambiar a precios en Bolívares (VEF)?</p>           

            </div>

            <div class="modal-footer">
                <a href="#" id="cambiaAbs" class="btn btn-primary">Sí</a>
                <a href="#" id="noCambiaAbs"  class="btn">No</a>

            </div>

        </div>

    </div>

  </div>
</body>
</html>
<?php BUFFER_FIN($mysqli); ?>