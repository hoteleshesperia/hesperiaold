<?php
/*
Script  bajo los términos y Licencia
GNU GENERAL PUBLIC LICENSE
Ver Terminos en:
http://www.gnu.org/copyleft/gpl.html
Traducción al español:
http://gugs.sindominio.net/gnu-gpl/gples.html
Autor: Hector A. Mantellini (Xombra)
*/
$imagen=$_GET["imagen"];
$valor=explode("-",strip_tags(trim($imagen)));

if (count($valor) == 2) {
$imagen = $valor[0];
$opcion = $valor[1]; }
else
{$imagen = $valor[0].'-'.$valor[1];
$opcion = $valor[2];  }


#Carrousel
if ($opcion == 1)
{	$directorio = 'https://hoteleshesperia.com.ve/img/slider';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 1900;
    $maxima_y = 900;
}

if ($opcion == 2)
{	$nombre = $imagen;
    $anchura  = 345;
    $maxima_y = 200;
}

# Escapadas - Paquetes
if ($opcion == 3)
{	$directorio = 'https://hoteleshesperia.com.ve/img/paquetes';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 320;
    $maxima_y = 170;
}

#Habitacion Detalle
if ($opcion == 4)
{	$nombre = $imagen;
    $anchura  = 100;
    $maxima_y = 55;
}

#Habitacion Detalle
if ($opcion == 5)
{	$nombre = $imagen;
    $anchura  = 250;
    $maxima_y = 145;
}

# Eventos
if ($opcion == 6)
{	$directorio = 'https://hoteleshesperia.com.ve/img/eventos';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 350;
    $maxima_y = 300;
}

# Restaurant
if ($opcion == 7)
{	$directorio = 'https://hoteleshesperia.com.ve/img/restaurant';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 350;
    $maxima_y = 330;
}

# galeria
if ($opcion == 8) # Imagen Normal
{	$directorio = 'https://hoteleshesperia.com.ve/img/galeria';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 1024;
    $maxima_y = 500;
}

if ($opcion == 9) # Imagen Thumbs
{	$directorio = 'https://hoteleshesperia.com.ve/img/galeria';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 110;
    $maxima_y =110;
}

# Experiencias
if ($opcion == 10)
{	$directorio = 'https://hoteleshesperia.com.ve/img/experiencias';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 320;
    $maxima_y = 170;
}

# Salones
if ($opcion ==11)
{	$directorio = 'https://hoteleshesperia.com.ve/img/salones';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 380;
    $maxima_y = 350;
}

# Portada
if ($opcion ==12)
{	$directorio = 'https://hoteleshesperia.com.ve/img/portada';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 320;
    $maxima_y = 180;
}

# Sala de prensa inicio
if ($opcion ==13)
{	$directorio = 'https://hoteleshesperia.com.ve/img/prensa';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 160;
    $maxima_y = 160;
}

#Sala de prensa nota
if ($opcion ==14)
{	$directorio = 'https://hoteleshesperia.com.ve/img/prensa';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 600;
    $maxima_y = 550;
}

# Ver Galeria sala de prensa
if ($opcion == 15)
{	$directorio = 'https://hoteleshesperia.com.ve/img/prensa';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 760;
    $maxima_y = 400;
}

# Imagen Youtube de Video
if ($opcion ==16)
{	$directorio = 'https://img.youtube.com/vi';
    $nombre = $directorio.'/'.$imagen.'/0.jpg';
    $anchura  = 200;
    $maxima_y = 170;
}
#Servicios
if ($opcion ==17)
{	$directorio = 'https://hoteleshesperia.com.ve/img/servicios';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 600;
    $maxima_y = 550;
}
#Aperturas
if ($opcion ==17)
{	$directorio = 'https://hoteleshesperia.com.ve/img/aperturas';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 300;
    $maxima_y = 250;
}

#Paquetes V2
if ($opcion == 18)
{   $directorio = 'https://hoteleshesperia.com.ve/img/paquetes';
    $nombre = $directorio.'/'.$imagen;
    $anchura  = 370;
    $maxima_y = 220;
}
if (!isset($imagen) )
    { die("Error no ha declarado una imagen");}

$datos = getimagesize($nombre);
switch ($datos[2]) {
    case 1:
        $img = imagecreatefromgif($nombre);
        break;
    case 2:
        $img = imagecreatefromjpeg($nombre);
        break;
    case 3:
        $img = imagecreatefrompng($nombre);
        break;
  default:
   die("Tipo de Imagen no valida");
}
$ratio  = $datos[0] / $anchura;
$altura = $datos[1] / $ratio;
if($altura >= $maxima_y)
   { $anchura2 = $maxima_y * $anchura / $altura;
     $altura   = $maxima_y;
     $anchura  = $anchura2;
   }
$thumb = imagecreatetruecolor($anchura,$altura);
$negro = imagecolorallocate($thumb, 0, 0, 0);
imagecolortransparent($thumb, $negro);
imagecolorallocatealpha($thumb, 255, 255, 255, 127);
imagecopyresampled($thumb, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);
switch ($datos[2]) {
    case 1:
        header("Content-type: image/gif");
    imagegif($thumb);
        break;
    case 2:
        header("Content-type: image/jpeg");
        imagejpeg($thumb,NULL,72);
        break;
    case 3:
        header("Content-type: image/png");
        imagepng($thumb,NULL,9);
        break;
  default:
   die("Tipo de Imagen no valida");
}
imagedestroy($img);
imagedestroy($thumb);
?>
