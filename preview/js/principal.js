$(document).ready(function(){

  $("#slider-index, #owl-demo3").owlCarousel({

      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true

      // "singleItem:true" is a shortcut for:
      // items : 1,
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
  });

  $("#hoteles-index-carousel").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds

      items : 2,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });

  $("#paquetes-carousel, #eventos-carousel").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });


  $("#restaurant_hotel_slider").owlCarousel({
    navigation : false, // Show next and prev buttons
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem:true

    // "singleItem:true" is a shortcut for:
    // items : 1,
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false
  });

  $('.input-group.date').datepicker({
    format: "dd-mm-yyyy",
    language: "es"
  });

 $("#clock-atrapame").countdown('2017/06/01', function(event) {
    $(this).html(event.strftime('<h4 class="text-center" style="color:white;">'+
    '</h4><h2 class="text-center" style="color:white;">Quedan: %D dias %H:%M:%S</h2>'+
  '<p class="text-center"><a href="#" class="btn btn-primary">Reservar</a></p>'));
  });

});
