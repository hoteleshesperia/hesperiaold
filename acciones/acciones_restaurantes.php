<?php 

$antesdecore = 1;

include '../include/databases.php';
include_once("../include/constantes_mail.php");
include '../include/PHPMailer/PHPMailerAutoload.php';  

$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);


$data = array();

foreach ($_POST as $key => $value) {

  $data["$key"] = $value;

}

//var_dump($data);


switch ($data["accion"]) {
	case 'reservar-restaurant':
		guardar_reservacion($data, $mysqli);
   
		break;
	
	default:
		# code...
		break;
}

function guardar_reservacion($data, $mysqli){
  
  $originalDate = $data["fecha"];
  $fecha = date("Y-m-d", strtotime($originalDate));

  $query = "INSERT into hesperia_v2_reserva_restaurant (id_restaurant, asistentes, mesas, 
  	fecha, hora, solicitante, email, telefono, fecha_solicitud, mensaje) values (".$data["id_restaurant"].",
  	".$data["asistentes"].", ".$data["mesas"].", '".$fecha."', '".$data["hora"]."', '".$data["nombre"]."',
  	'".$data["email"]."', '".$data["telefono"]."', NOW(), '".$data["mensaje"]."')";

  if ($result = $mysqli->query($query)) {
  	 enviar_mail($data);
  }else{
  	echo "mal-->".$mysqli->error;
  }
}

function enviar_mail($data){
include_once("../include/constantes_mail.php");
$head = getCabeceras();
$mensaje ="Sin mensaje";
if ($data["mensaje"]!="") {
  $mensaje = $data["mensaje"];
}
switch ($data["id_restaurant"]) {
  case '8':
   //atmosfera
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/wine.png";
    break;

  case '13':
  //rios
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/wine.png";
    break;

  case '16':
  //penhero
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/wine.png";
    break;

  case '17':
  //valdervira
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/wine.png";
    break;

  case '1':
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/ShoreGrill.jpg";
    break;

  case '2':
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/Logoasian.jpg";
    break;

  case '3':
  //terraza
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/wine.png";
    break;

  case '18':
  //olas
    $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/Logoobc.jpg";
    break;
  
  default:
     $imagen_rest ="https://hoteleshesperia.com.ve/logos_rest/wine.png";
    break;
}
$body = "
        <center>
            <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%'' id='bodyTable'>
                <tr>
                    <td align='center' valign='top' id='bodyCell'>
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
            <table align='center' border='0' cellspacing='0' cellpadding='0' width='600' style='width:600px;'>
            <tr>
            <td align='center' valign='top' width='600' style='width:600px;'>
            <![endif]-->
            <table border='0' cellpadding='0' cellspacing='0' width='100%' class='templateContainer'>
              <tr>
                <td valign='top' id='templatePreheader'></td>
              </tr>
              <tr>
                <td valign='top' id='templateHeader'><table border='0' cellpadding='0' cellspacing='0' width='100%'
                 class='mcnImageCardBlock'>
    <tbody class='mcnImageCardBlockOuter'>
        <tr>
            <td class='mcnImageCardBlockInner' valign='top' style='padding-top:9px; padding-right:18px; padding-bottom:9px;
             padding-left:18px;'>
                
<table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnImageCardBottomContent' width='100%'>
    <tbody><tr>
        <td class='mcnImageCardBottomImageContent' align='left' valign='top' style='padding-top:0px; padding-right:0px;
         padding-bottom:0; padding-left:0px;'>
        
            
            <a href='https://hoteleshesperia.com.ve' title='Ir a nuestra pagina web' class='' target='_blank'>
            

            <img alt='' src='https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png' width='100%'
             style='width: 100%;' class='mcnImage'>
            </a>
        
        </td>
    </tr>
    <tr>
        <td class='mcnTextContent' valign='top' style='padding: 9px 18px; font-family: Helvetica; font-size: 14px;
         font-weight: normal; text-align: center;' width='546'>
            <strong>
              <span style='font-family:verdana,geneva,sans-serif'>Hemos registrado una solicitud de reservación<br>
                a nombre de ".$data["nombre"]."
              </span>
            </strong>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnDividerBlock' style='min-width:100%;'>
    <tbody class='mcnDividerBlockOuter'>
        <tr>
            <td class='mcnDividerBlockInner' style='min-width: 100%; padding: 2px 18px;'>
                <table class='mcnDividerContent' border='0' cellpadding='0' cellspacing='0' width='100%'
                 style='min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;'>
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign='top' id='templateUpperBody'><table border='0' cellpadding='0' cellspacing='0' 
                width='100%' class='mcnTextBlock' style='min-width:100%;'>
    <tbody class='mcnTextBlockOuter'>
        <tr>
            <td valign='top' class='mcnTextBlockInner'>
                
                <table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'
                 style='min-width:100%;' class='mcnTextContentContainer'>
                    <tbody><tr>
                        
                        <td valign='top' class='mcnTextContent' style='padding-top:9px; padding-right: 18px; padding-bottom: 9px;
                         padding-left: 18px;'>
                          <span style='font-family:verdana,geneva,sans-serif'>
                          <span style='font-size:13px'><strong>Restaurant:</strong> ".$data["nombre_rest"]."<br>                        
                          <span style='font-family:verdana,geneva,sans-serif'>
                          <span style='font-size:13px'><strong>Asistentes:</strong> ".$data["asistentes"]." <br>
                          <strong>Mesas:</strong> ".$data["mesas"]."<br>
                          <strong>telefono:</strong> ".$data["telefono"]."<br>
                          <strong>Email:</strong> ".$data["email"]."<br>
                          <strong>Hora:</strong> ".$data["hora"]."<br>
                          <strong>Fecha:</strong> ".$data["fecha"]."<br>
                          <strong>Mensaje:</strong> ".$mensaje."<br>
                          <center><strong>Nota:</strong> Este email no es válido como comprobante de pago</center>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>

            <td align='center' valign='middle' width='24'>
              <img src='".$imagen_rest."'>
            </td>
        </tr>
    </tbody>
</table>

";
  $footer = getPieMail();

  $mensaje = $head.$body.$footer;

  $contacto = 'info@hoteleshesperia.com.ve'; //
  $cliente = $data["email"];
  $restaurant = 'restaurantes@hoteleshesperia.com.ve';
  $mail_object = new PHPMailer();
  $mail_object->IsSMTP(); // Indicamos que use SMTP
              //$mail_object->SMTPDebug = 3;   //activar modo debug                                   
  $mail_object->Host = 'a2plcpnl0042.prod.iad2.secureserver.net';  // Indicamos los servidores SMTP
  $mail_object->SMTPAuth = true;                               // Habilitamos la autenticación SMTP
  $mail_object->Username = 'reserva@hoteleshesperia.com.ve';                 // SMTP username
  $mail_object->Password = 'Nh123456nh';                           // SMTP password
  $mail_object->Port = 465;                                    // TCP port
  $mail_object->SMTPSecure = "ssl"; // Establece el tipo de seguridad SMTP a SSL.
  $mail_object->Timeout =30;
              //$mail_object->SMTPDebug = 1;
                             
              /** Configurar cabeceras del mensaje **/
  $mail_object->From = $contacto;                       // Correo del remitente
  $mail_object->FromName = utf8_decode('Información Hesperia Hotels & Resorts');           // Nombre del remitente
  $mail_object->Subject = utf8_decode('Reservación en restaurant '.$data["nombre_rest"]);                // Asunto
               
              /** Incluir destinatarios. El nombre es opcional **/
 $mail_object->addAddress($cliente); 
 //$mail_object->addAddress($contacto);
 $mail_object->addAddress($restaurant);

              /** Enviarlo en formato HTML **/
  $mail_object->isHTML(true);                                  
               
              /** Configurar cuerpo del mensaje **/
  $mail_object->Body    = utf8_decode($mensaje);
  $mail_object->AltBody = $mensaje;

  if(!$mail_object->send()) {
    $errors = 'Ocurrio un error al registrar el pago. ';
    $errors .= 'Mailer Error: ' . $mail_object->ErrorInfo;
    echo "$errors";
  }else{
    echo "ok";
   
  }
}
 ?>