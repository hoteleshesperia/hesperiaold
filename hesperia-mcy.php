<section style="background: url('https://hoteleshesperia.com.ve/img/banner-hesperia-maracay.jpg') no-repeat center center;
     background-size:cover;" class='bg-1 text-center' >
</section>

 <div class="container">
        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Hesperia Maracay - <small>Nueva Apertura</small>
                </h1>
            </div>
            <p class="lead">
                El Hotel Hesperia Maracay, renovado totalmente en 2017, se encuentra situado en la Av. Las
                Delicias de la Ciudad de Maracay. Está ubicado en la zona más comercial de la ciudad, con
                centros comerciales, restaurantes y servicios a corta distancia. Dispone de parking y WIFI en
                todas las áreas del hotel.</p>

            <p class="text-justify">Hesperia Maracay cuenta con 99 habitaciones perfectamente equipadas para hacer agradable la estancia 
                de clientes de ocio y negocio, pudiendo disfrutar de habitaciones Deluxe, Deluxe Twin y
                Suites con baño privado, algunas de ellas con puertas comunicantes para mayor comodidad y con excelente vista
                a la montaña, contamos con recepción las 24 horas. Las habitaciones son confortables, modernas y disponen de escritorio, tv plana,
                mini nevera, cafetera, maletero y mesa de planchar. No se necesita estancia mínima. 
            </p>

            <div class="well">
                <h4 class="text-center">Para información y reservaciones <strong>info@hoteleshesperia.com.ve</strong></h4>
<h5 class="text-center">Precío de la limpieza incluido en la tarifa <i class="fa fa-money" aria-hidden="true"></i></h5>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Business Center</h4>
                    </div>
                    <div class="panel-body text-justify">
                        <p>Es el espacio propicio para concretar negocios o reuniones de manera rápida y cómoda. Esta
                            oficina virtual ofrece equipos actuales con conexión a internet y agradables espacios.</p>
                            <br>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-briefcase"></i> Reuniones corporativas</h4>
                    </div>
                    <div class="panel-body text-justify">
                        <p>Ponemos a tu disposición salas de reuniones con espacios flexibles dentro de la mejor
                         ubicación del hotel, también contamos con un equipo de expertos que garantizarán el exito
                         de su reunión</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-gift"></i> Eventos sociales</h4>
                    </div>
                    <div class="panel-body text-justify">
                        <p>Tanto si sueñas con un banquete de bodas a lo grande o algún otro evento social,
                         te ofrecemos los mejores espacios para hacer que tu día sea verdaderamente especial y memorable.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Features Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"><a href="https://www.instagram.com/explore/tags/ahoraenmaracay/">#AhoraEnMaracay</a></h2>
            </div>
            <div class="col-md-6">
                <h4><strong>Contamos con 99 Maravillosas Habitaciones</strong></h4>
                <ul>
                    <li>
                        <strong>Deluxe </strong>
                        <p class="text-justify">La habitación cuenta con grandes ventanales y agradables vistas.
                         Posee una cama king con cómodas almohadas, además dispone de un moderno escritorio
                         y silla con kit de cafe. El dormitorio dispone de acceso Wifi-Fi a internet,
                         minibar, caja fuerte, televisor, plancha con mesa, maletero y baño con todas las amenities.</p>
                    </li>
                    <li>
                        <strong>Deluxe Twin </strong>
                        <p class="text-justify">Posee dos camas queen con cómodas almohadas, además dispone de un moderno escritorio
                         y silla con agradable kit de cafe. El dormitorio dispone de acceso Wifi-Fi a internet,
                         minibar, caja fuerte, televisor, plancha con mesa, maletero y baño con todas las amenities.
                        </p>
                    </li>
                    <li>
                        <strong>Suite </strong>
                        <p class="text-justify">La habitación cuenta con agradable espacio, grandes ventanales y
                         encantadoras vistas. Posee una cama king con cómodas almohadas y cobijas extras.
                         Además dispone de un moderno escritorio y silla con agradable cafetera acompañado
                         de un kit de cafe. El dormitorio dispone de acceso Wifi-Fi a internet, minibar, caja fuerte,
                         televisor, plancha con mesa, maletero y baño con todas las amenities.
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="https://hoteleshesperia.com.ve/img/habitacion-h-mcy.jpg" alt="">
            </div>

            
            <div class="col-md-12">
                <hr></div>
            <div class="col-md-6 pull-left">
                <img class="img-responsive" src="https://hoteleshesperia.com.ve/img/h-hespera-mcy-outside.jpg" alt="">
            </div>
            <div class="col-md-6 ">
                <h4><strong>Tambien disponemos de diversos salones para meetings y reuniones</strong></h4>
                <ul>
                    <li>
                        <strong>Gran Salón  </strong>
                        <p class="text-justify">Es el salón ideal para celebrar grandes eventos, conferencias,
                        lanzamientos de productos entre otros. Cuenta con una capacidad minima que va desde 100
                        y hasta 490 personas donde en sus 538m2 y su altura de 5,32m2 se transforman en un
                        encantador espacio.</p>
                    </li>
                    <li>
                        <strong>Salón Orinoco</strong>
                        <p class="text-justify">Con una capacidad minima de 90 personas y hasta 250,
                        este salón se convierte en el espacio adecuado para sus convenciones, reuniones o celebraciones.
                        Cuenta con 276 m2 y una altura de 5,32m2.
                        </p>
                    </li>
                    <li>
                        <strong>Salón Choroní</strong>
                        <p class="text-justify">Al momento de realizar eventos o ceremonias más pequeñas el salón 2
                        se puede adaptar a esas necesidades. Cuenta con 262 m2 en los cuales podrá realizar diferentes
                        tipos de montajes, donde su capacidad minima de personas va desde 54 y hasta 220.
                        </p>
                    </li>

                    <li>
                        <strong>Salón Los Roques</strong>
                        <p class="text-justify">El salón 3 esta especialmente dedicado a reuniones corporativas donde
                         podrá disfrutar de un espacio acogedor y más cercano. El salón cuenta con 154 m2 y su
                         capacidad minima de personas va desde 40 y alcanzando un máximo de 140, de acuerdo al montaje
                         seleccionado.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->
</div>