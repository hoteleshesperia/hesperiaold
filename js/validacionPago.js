$(document).ready(function(){

    $("#pesta4").click(function(){
        $("#bitDiv").toggle();
        $("#tab2").toggle();
    });

    $("#pesta3").click(function(){
        $("#bitDiv").toggle();
        $("#tab2").toggle();
    });


    $("#bitPagoButtonF").click(function(){
        
        if(ValidarFormPreReserva()){
            $("#bitPagoButtonF").toggle();
            /*$(".bp-partner-button").toggle();
            $(".bp-partner-button").css('float','right');*/
            $(".bp-partner-button").click();
        }
    });

    $('#FormPayUsuario .clsBitpagosForm').change(function(){
        if(!ValidarFormPreReserva()){
            $("#bitPagoButtonF").show();
            $(".bp-partner-button").hide();
        }
    });

    $("#bitPagoButtonFPaq").click(function(){
        
        if(ValidarFormPreReservaPaq()){
            $("#bitPagoButtonFPaq").toggle();
            /*$(".bp-partner-button").toggle();
            $(".bp-partner-button").css('float','right');*/
            $(".bp-partner-button").click();
        }
    });

    $('#FormPayUsuarioPaq .clsBitpagosForm').change(function(){
        if(!ValidarFormPreReservaPaq()){
            $("#bitPagoButtonFPaq").show();
            $(".bp-partner-button").hide();
        }
    });

    $("#btnInstapago").click(function(){

        if(ValPago()){
            validarBL();
           // $("#btnInstapago").toggle();

           /* $("#loader").toggle();

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:$("#FormPayUsuario").serialize(),

                    url:'https://hoteleshesperia.com.ve/include/instapago.php',

                    dataType:'text',async:false,

                    complete: function(){

                        $("#btnInstapago").toggle();

                        $("#loader").toggle();

                    }

                }).responseText;
                 var hotel = $("#nombre_hotel").text();

                 var mail = $("#email").val();
                 var total = $("#precio").val();
                 var split = total.split(",");
                 split=split[0].replace(/\./g,'');

                 var desc = hotel+" - "+mail; 


                if(resultado.substr(1, 4) == "html"){

                    ga('send', 'event', 'tangible-tdc', 'reserva-habitacion', desc , total);

                    $("#datosCompra").html(resultado);
                    $('body,html').scrollTop(0);

                }else{

                    document.getElementById("RespuestaIp").innerHTML=resultado;

                } 

            }, 2000);
            
            */
        }

    });



    $("#btnTrans").click(function(){

        if(ValidarFormPreReserva()){

            /*Recuperando datos*/

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var telefono = $("#telefono").val();

            var pais = $("#pais").val();

            var observaciones = $("#observaciones").val();

            var id_hotel = $("#id_hotel").val();

            var precio = $("#precio").val();

            var name_desde = $("#name_desde").val();

            var name_hasta = $("#name_hasta").val();

            var name_noches = $("#name_noches").val();

            var count_h = $("#count_h").val();

            var cantServ = $("#cantServ").val();

            var referencia = $("#referencia").val();

            //alert(referencia);

            var foto = document.getElementById('imagen');

            var temp_foto=foto.files;



            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('telefono', telefono);  

            formData.append('pais', pais);  

            formData.append('observaciones', observaciones); 

            formData.append('id_hotel', id_hotel);  

            formData.append('precio', precio);  

            formData.append('name_desde', name_desde);  

            formData.append('name_hasta', name_hasta);  

            formData.append('name_noches', name_noches);  

            formData.append('count_h', count_h);  

            formData.append('cantServ', cantServ);   

            formData.append('referencia', referencia); 

            formData.append('imagen', temp_foto[0]);      



            for (var i=0; i < count_h; i++) {

                    

                    formData.append('name_adul'+i, $("#name_adul"+i).val());

                    if(id_hotel != 4){

                        formData.append('name_nin'+i, $("#name_nin"+i).val());

                    }

                    

                    formData.append('name_habi'+i, $("#name_habi"+i).val());

                    formData.append('id_habi'+i, $("#id_habi"+i).val());

                    formData.append('name_sub'+i, $("#name_sub"+i).val());

            }  



            for (var i=0; i < cantServ; i++) {

                    formData.append('descPaq'+i, $("#descPaq"+i).val());

                    formData.append('subPaq'+i, $("#subPaq"+i).val());

            }    



            $("#btnTrans").toggle();

            $("#loaderTrans").toggle();

            $("#FormPayUsuario").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/prereserva.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false,                    

                    complete: function(){

                        //$("#btnTrans").toggle();

                        $("#loaderTrans").toggle();



                    }

                }).responseText;

                 $("#RespuestaTrans").html(resultado);

                

            }, 2000);

        }

    });



    $("#btnTransPaq").click(function(){

        if(ValidarFormPreReservaPaq()){

            /*Recuperando datos*/

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var telefono = $("#telefono").val();

            var pais = $("#pais").val();

            var observaciones = $("#observaciones").val();

            var id_hotel = $("#id_hotel").val();

            var precio = $("#precio").val();

            var paquete = $("#paquete").val();

            var desdePaq = $("#desdePaq").val();

            var hastaPaq = $("#hastaPaq").val();

            var dpd3 = $("#dpd3").val();

            var fechaSelectHasta = $("#fechaSelectHasta").val();

            var numPaq = $("#numPaq").val();

            var partai = $("#partai").val();

            var vip = $("#vip").val();

            var tiempo = $("#tiempo").val();

            var precioNino = $("#precioNino").val();

            if(precioNino > 0){

                var numPaqN = $("#numPaqN").val();                

            }

            var porcentaje = $("#porcentaje").val();

            var habitacion = $("#habitacion").val();

            var referencia = $("#referencia").val();

            //alert(referencia);

            var foto = document.getElementById('imagen');

            var temp_foto=foto.files;



            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('telefono', telefono);  

            formData.append('pais', pais);  

            formData.append('observaciones', observaciones); 

            formData.append('id_hotel', id_hotel);  

            formData.append('precio', precio);  

            formData.append('paquete', paquete);  

            formData.append('desdePaq', desdePaq);  

            formData.append('hastaPaq', hastaPaq);  

            formData.append('dpd3', dpd3);  

            formData.append('fechaSelectHasta', fechaSelectHasta); 

            formData.append('numPaq', numPaq);  

            formData.append('partai', partai);  

            formData.append('vip', vip);  

            formData.append('tiempo', tiempo);  

            formData.append('precioNino', precioNino);  

            if(precioNino > 0){                

                formData.append('numPaqN', numPaqN); 

            } 

            formData.append('porcentaje', porcentaje); 

            formData.append('habitacion', habitacion);   

            formData.append('referencia', referencia); 

            formData.append('imagen', temp_foto[0]);      



            $("#btnTransPaq").toggle();

            $("#loaderTransPaq").toggle();

            $("#FormPayUsuarioPaq").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/prereservaPaq.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false,                    

                    complete: function(){

                        //$("#btnTrans").toggle();

                        $("#loaderTransPaq").toggle();



                    }

                }).responseText;

                 $("#RespuestaTrans").html(resultado);

                

            }, 2000);

        }

    });





    $("#btnInstapagoPaq").click(function(){

        if(ValPagoPaq()){
            validarBLPaq();
          /*  $("#btnInstapagoPaq").toggle();

            $("#loaderPaq").toggle();

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:$("#FormPayUsuarioPaq").serialize(),

                    url:'https://hoteleshesperia.com.ve/include/instapagoPaquetes.php',

                    dataType:'text',async:false,

                    complete: function(){

                        $("#btnInstapagoPaq").toggle();

                        $("#loaderPaq").toggle();

                    }

                }).responseText;
                 var hotel = $("#nombre_hotel").text();
                 var mail = $("#email").val();
                 var total = $("#precio").val();
                 var split = total.split(",");
                 split=split[0].replace(/\./g,'');

                 var desc = hotel+" - "+mail; 

                if(resultado.substr(1, 4) == "html"){
                    ga('send', 'event', 'tangible', 'reserva-paquete', desc , split);

                    $("#datosCompraPaq").html(resultado);

                    $('body,html').scrollTop(0);

                

                }else{

                    document.getElementById("RespuestaIp").innerHTML=resultado;

                }

            }, 2000);
        */

        }

    });


    $("#btnPrereserva").click(function(){



        if(ValidarFormPreReserva()){

            /*Recuperando datos*/
            $("#btnPrereserva").toggle();
            $("#datosBancarios").toggle();
            $("#datosComprobante").toggle();

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var telefono = $("#telefono").val();

            var pais = $("#pais").val();

            var observaciones = $("#observaciones").val();

            var id_hotel = $("#id_hotel").val();

            var precio = $("#precio").val();

            var name_desde = $("#name_desde").val();

            var name_hasta = $("#name_hasta").val();

            var name_noches = $("#name_noches").val();

            var count_h = $("#count_h").val();

            var cantServ = $("#cantServ").val();

            var auxId = $("#aux_id").val();

            var cod_promocional = $("#cod_promocional").val();

            var descuento_cod = $("#descuento_cod").val();

            var totalPagar = $("#totalPagar").val();

            /*var referencia = $("#referencia").val();

            //alert(referencia);

            var foto = document.getElementById('imagen');

            var temp_foto=foto.files;*/



            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('telefono', telefono);  

            formData.append('pais', pais);  

            formData.append('observaciones', observaciones); 

            formData.append('id_hotel', id_hotel);  

            formData.append('precio', precio);  

            formData.append('name_desde', name_desde);  

            formData.append('name_hasta', name_hasta);  

            formData.append('name_noches', name_noches);  

            formData.append('count_h', count_h);  

            formData.append('cantServ', cantServ);  

            formData.append('aux_id', auxId); 

            formData.append('cod_promocional', cod_promocional); 

            formData.append('descuento_cod', descuento_cod); 

            formData.append('totalPagar', totalPagar); 

            /*formData.append('referencia', referencia); 

            formData.append('imagen', temp_foto[0]);  */    



            for (var i=0; i < count_h; i++) {

                    

                    formData.append('name_adul'+i, $("#name_adul"+i).val());

                    if(id_hotel != 4){

                        formData.append('name_nin'+i, $("#name_nin"+i).val());

                    }

                    
                    formData.append('name_habi'+i, $("#name_habi"+i).val());

                    formData.append('id_habi'+i, $("#id_habi"+i).val());

                    formData.append('name_sub'+i, $("#name_sub"+i).val());

            }  



            for (var i=0; i < cantServ; i++) {

                    formData.append('descPaq'+i, $("#descPaq"+i).val());

                    formData.append('subPaq'+i, $("#subPaq"+i).val());

            }    

            /*$("#FormPayUsuario").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });*/

            $("#FormPayUsuario").on("submit", function(){
               //Code: Action (like ajax...)
               return false;
             });

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/crearPrereserva.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false

                }).responseText;

                 $("#RespuestaTrans").html(resultado);

                

            }, 2000);

        }

    });

    
    $("#btnSolicitud").click(function(){

        $("#RespuestaTrans").html('');

        if(ValidarEnvioRefereciaFront()){
        $("#btnSolicitud").toggle();
            /*Recuperando datos*/

            $("#loaderTrans").toggle();

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var referencia = $("#referencia").val();

            var auxId = $("#aux_id").val();

            var telefono = $("#telefono").val();

            var ind_descuento = $("#ind_descuento").val();
            var descuento_cod = $("#descuento_cod").val();
            var cod_promocional = $("#cod_promocional").val();
            var permisos = $("#permisos").val();
            var totalPagar = $("#totalPagar").val();

            //alert(referencia);

            var foto = document.getElementById('imagen');

            var temp_foto=foto.files;



            var formData = new FormData();

            var total = $("#precio").val();

            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('referencia', referencia); 

            formData.append('aux_id', auxId); 

            formData.append('telefono', telefono); 

            formData.append('ind_descuento', ind_descuento);  

            formData.append('descuento_cod', descuento_cod);  

            formData.append('cod_promocional', cod_promocional); 

            formData.append('permisos', permisos); 

            formData.append('totalPagar', totalPagar); 

            formData.append('imagen', temp_foto[0]);     



                

            $("#FormPayUsuario").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });


            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/comprobantePrereserva.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false,

                    complete: function(){

                        //$("#btnTrans").toggle();

                        
                        $("#loaderTrans").toggle();



                    }

                }).responseText;

                 
                    if(resultado == '1'){

                        var resultado = '<div class="alert alert-success" role="alert"> <p>Su solicitud de reserva ha sido enviada al departamento de reservas, donde nuestro personal verificaran la efectividad de su pago</p></div>';
                        ga('send', 'event', 'intangible', 'envio-comprobante-hab', nombres+'-'+apellidos+'-'+auxId, total);
                    }else{

                        $("#btnSolicitud").toggle();
                    }

                    $("#RespuestaTrans").html(resultado);
                


                
                

            }, 2000);
        }

        

    });


    $("#btnPrereservaPaq").click(function(){

        if(ValidarFormPreReservaPaq()){

            /*Recuperando datos*/
            $("#btnPrereservaPaq").toggle();
            $("#datosBancarios").toggle();
            $("#datosComprobante").toggle();

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var telefono = $("#telefono").val();

            var pais = $("#pais").val();

            var observaciones = $("#observaciones").val();

            var id_hotel = $("#id_hotel").val();

            var precio = $("#precio").val();

            var paquete = $("#paquete").val();

            var desdePaq = $("#desdePaq").val();

            var hastaPaq = $("#hastaPaq").val();

            var dpd3 = $("#dpd3").val();

            var fechaSelectHasta = $("#fechaSelectHasta").val();

            var numPaq = $("#numPaq").val();

            var partai = $("#partai").val();

            var vuelo = $("#vuelo").val();

            var vip = $("#vip").val();

            var clave = $("#clave").val();

            var tiempo = $("#tiempo").val();

            var auxId = $("#aux_id").val();

            var precioNino = $("#precioNino").val();

            if(precioNino > 0){

                var numPaqN = $("#numPaqN").val();                

            }

            var porcentaje = $("#porcentaje").val();

            var habitacion = $("#habitacion").val();

            var serviciosSubt = $("#serviciosSubt").val();

            if(serviciosSubt > 0){
                var servicios = $("#servicios").val();
                var precioPaquete = $("#precioPaquete").val();
            }

            /*var referencia = $("#referencia").val();

            //alert(referencia);

            var foto = document.getElementById('imagen');

            var temp_foto=foto.files;*/



            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('telefono', telefono);  

            formData.append('pais', pais);  

            formData.append('observaciones', observaciones); 

            formData.append('id_hotel', id_hotel);  

            formData.append('precio', precio);  

            formData.append('paquete', paquete);  

            formData.append('desdePaq', desdePaq);  

            formData.append('hastaPaq', hastaPaq);  

            formData.append('dpd3', dpd3);  

            formData.append('fechaSelectHasta', fechaSelectHasta); 

            formData.append('numPaq', numPaq);  

            formData.append('partai', partai);  

            formData.append('vuelo', vuelo);  

            for (var i = 0; i < vuelo; i++) {
                var j = i;
                j++;
               formData.append('nomPasajero'+j, $("#nomPasajero"+j).val());  
               formData.append('cedPasajero'+j, $("#cedPasajero"+j).val()); 
            };

            formData.append('vip', vip);  

            formData.append('clave', clave);  

            formData.append('tiempo', tiempo);  

            formData.append('precioNino', precioNino);

            formData.append('aux_id', auxId);   

            if(precioNino > 0){                

                formData.append('numPaqN', numPaqN); 

            } 

            formData.append('porcentaje', porcentaje); 

            formData.append('habitacion', habitacion);  

            formData.append('serviciosSubt', serviciosSubt);    

            if(serviciosSubt > 0){

                formData.append('servicios', servicios); 

                formData.append('precioPaquete', precioPaquete); 

            }

            /*formData.append('referencia', referencia); 

            formData.append('imagen', temp_foto[0]);      */



            /*$("#FormPayUsuarioPaq").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });*/

            $("#FormPayUsuarioPaq").on("submit", function(){
               //Code: Action (like ajax...)
               return false;
             });

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/crearPrereservaPaq.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false

                }).responseText;

                

            }, 2000);

        }

    });


    $("#btnSolicitudPaq").click(function(){

        $("#RespuestaTrans").html('');

        if(ValidarEnvioRefereciaFront()){
        $("#btnSolicitudPaq").toggle();
            /*Recuperando datos*/

            $("#loaderTransPaq").toggle();

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var referencia = $("#referencia").val();

            var auxId = $("#aux_id").val();

            var serviciosSubt = $("#serviciosSubt").val();

            if(serviciosSubt > 0){
                var servicios = $("#servicios").val();
                var precioPaquete = $("#precioPaquete").val();
            }

            //alert(referencia);

            var foto = document.getElementById('imagen');

            var temp_foto=foto.files;



            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('referencia', referencia); 

            formData.append('aux_id', auxId); 

            formData.append('serviciosSubt', serviciosSubt); 

            if(serviciosSubt > 0){

                formData.append('servicios', servicios); 

                formData.append('precioPaquete', precioPaquete); 

            }

            formData.append('imagen', temp_foto[0]);     



                

            $("#FormPayUsuarioPaq").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });


            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/comprobantePrereservaPaq.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false,

                    complete: function(){

                        //$("#btnTrans").toggle();

                        
                        $("#loaderTransPaq").toggle();



                    }

                }).responseText;

                
                    if(resultado == '1'){
                        var total = $("#precio").val();
                    
                        var resultado = '<div class="alert alert-success" role="alert"> <p>Su solicitud de reserva ha sido enviada al departamento de reservas, donde nuestro personal verificaran la efectividad de su pago</p></div>';
                        ga('send', 'event', 'intangible', 'envio-comprobante-paq', nombres+'-'+apellidos+'-'+auxId, total);
                    }else{

                        $("#btnSolicitudPaq").toggle();
                    }

                    $("#RespuestaTrans").html(resultado);
                


                
                

            }, 2000);
        }

        

    });

    $("#pagarLuego").click(function(){

        //alert('Pagará luego');
        $("#RespuestaTrans").html('');
        $("#loaderTrans").toggle();
        var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var auxId = $("#aux_id").val();

            var telefono = $("#telefono").val();            

            var ind_descuento = $("#ind_descuento").val();
            var descuento_cod = $("#descuento_cod").val();
            var cod_promocional = $("#cod_promocional").val();
            var permisos = $("#permisos").val();
            var totalPagar = $("#totalPagar").val();



            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('aux_id', auxId); 

            formData.append('telefono', telefono); 

            formData.append('ind_descuento', ind_descuento);  

            formData.append('descuento_cod', descuento_cod);  

            formData.append('cod_promocional', cod_promocional); 

            formData.append('permisos', permisos); 

            formData.append('totalPagar', totalPagar); 


                

            $("#FormPayUsuario").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });


            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/envioMensajePrereserva.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false

                }).responseText;

                 
                    if(resultado == '1'){

                        var resultado = '<div class="alert alert-success" role="alert"> <p>Hemos recibido sus datos para la solicitud de reserva. Se le ha enviado un correo con las indicaciones a seguir</p></div>';

                    }


                    $("#loaderTrans").toggle();
                    $("#RespuestaTrans").html(resultado);
                


                
                

            }, 2000);
    });

    $("#pagarLuegoPaq").click(function(){

            $("#RespuestaTrans").html('');

            /*Recuperando datos*/

            $("#loaderTransPaq").toggle();

            var nombres = $("#nombres").val();

            var apellidos = $("#apellidos").val();

            var email = $("#email").val();

            var auxId = $("#aux_id").val();

            var serviciosSubt = $("#serviciosSubt").val();

            if(serviciosSubt > 0){
                var servicios = $("#servicios").val();
                var precioPaquete = $("#precioPaquete").val();
            }


            var formData = new FormData();



            formData.append('nombres', nombres);

            formData.append('apellidos', apellidos);  

            formData.append('email', email);  

            formData.append('aux_id', auxId); 

            if(serviciosSubt > 0){

                formData.append('servicios', servicios); 

                formData.append('precioPaquete', precioPaquete); 

            }



                

            $("#FormPayUsuarioPaq").submit(function(){

               //aqui podemos llamar alguna funcion por defecto o nada. 

               //El return false va igual

               return false;

              });


            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:formData,

                    url:'https://hoteleshesperia.com.ve/include/envioMensajePrereservaPaq.php',

                    contentType:false,

                    processData:false,

                    cache:false,

                    async:false,

                    complete: function(){
                        $("#loaderTransPaq").toggle();
                    }

                }).responseText;

                
                    if(resultado == '1'){
                        var resultado = '<div class="alert alert-success" role="alert"> <p>Hemos recibido sus datos para la solicitud de reserva. Se le ha enviado un correo con las indicaciones a seguir</p></div>';

                    }

                    $("#RespuestaTrans").html(resultado);
                


                
                

            }, 2000);
        

        

    });

    $("#tiempo").change(function(){
        var dias = parseInt($(this).val()) + parseInt(1);

        if($("#regimen").val() == 1){            
            var precio = $("#precioAD").val();
            var precioPlano = $("#precioPlanoAD").val();
            var tipoRegimen = 'Desayuno incluido';
        }
        if($("#regimen").val() == 2){
            var precio = $("#precioADC").val();  
            var precioPlano = $("#precioPlanoADC").val();
            var tipoRegimen = 'Desayuno y cena incluidos';          
        }
        if($("#regimen").val() == 3){
            if($("#pais").val() == "VE"){
                var precio = $("#precioATI").val(); 
                var precioPlano = $("#precioPlanoATI").val();    
                var tipoRegimen = 'Todo incluido'; 
            }else{
                var precio = $("#priceTI").val(); 
                var precioPlano = $("#pricePlanoTI").val();    
                var tipoRegimen = 'Todo incluido'; 
            }
                  
        }

        if($("#regimen").val() == 4){
            var precio = $("#precioADAC").val();
            var precioPlano = $("#precioPlanoADAC").val();
            var tipoRegimen = 'Desayuno, almuerzo y cena incluido';
        }

        if($("#vblue").val() == 1){
            var precio = $("#precioAHB").val();
            var precioPlano = $("#precioPlanoAHB").val();
            var tipoRegimen = 'Desayuno + Servicios Hesperia Blue';
        }


        var nombrePaq = $("#nombrePaqInicial").val().split("-")[0]+'<br>Estadía con ' + tipoRegimen ;
        
        //var noches = parseInt($("#noches_inicio").val()) ;
                    //precio = parseInt(precio) * parseInt(noches);
        
        var nuevas_noches = parseInt($(this).val()) - parseInt($("#noches_gratis").val());
        precio = precio * nuevas_noches;
        precioAuxiliar = precio;
        $(".aSumar").each(function(){
            if($(this).is(':checked')){
                precioAuxiliar = precioAuxiliar + parseInt($(this).data('precio'));
            }
                
        });
        //$("#noches_inicio").val(nuevas_noches);
        $("#precioTotalPagar").val(parseInt(precioAuxiliar) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioDesc").text(parseInt(precio) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioPlano").text(parseInt(precioPlano) * parseInt($(this).val()) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioPaq").val(parseInt(precio));
        $("#precioSF").val(parseInt(precioAuxiliar));
        $("#nombrePaq").val(nombrePaq);
        
    });

    $("#regimen").change(function(){
        var dias = parseInt($("#tiempo").val()) + parseInt(1);
        if($(this).val() == 1){
            var precio = $("#precioAD").val();
            var precioPlano = $("#precioPlanoAD").val();
            var tipoRegimen = 'Desayuno incluido';
            $("#blue").show();
        }
        if($(this).val() == 2){
            var precio = $("#precioADC").val();
            var precioPlano = $("#precioPlanoADC").val();
            var tipoRegimen = 'Desayuno y cena incluidos';
            $("#HBlue").prop('checked', false);
            $("#blue").hide();
            $("#vblue").val(0);
        }
        if($(this).val() == 3){
            var precio = $("#precioATI").val();
            var precioPlano = $("#precioPlanoATI").val();
            var tipoRegimen = 'Todo incluido';
            $("#HBlue").prop('checked', false);
            $("#blue").hide();
            $("#vblue").val(0);
        }

        if($(this).val() == 4){
            var precio = $("#precioADAC").val();
            var precioPlano = $("#precioPlanoADAC").val();
            var tipoRegimen = 'Desayuno, almuerzo y cena incluido';
            $("#HBlue").prop('checked', false);
            $("#blue").hide();
            $("#vblue").val(0);
        }
        var nombrePaq = $("#nombrePaqInicial").val().split("-")[0]+'<br>Estadía con ' + tipoRegimen ;
        
        //alert(precio.toFixed(2));

        /*var noches = parseInt($("#noches_inicio").val()) ;
        if(noches > 0)
            precio = parseInt(precio) / parseInt(noches);*/
        
        var nuevas_noches = parseInt($("#tiempo").val()) - parseInt($("#noches_gratis").val());
        precio = precio * nuevas_noches;

        precioAuxiliar = precio;
        $(".aSumar").each(function(){
            if($(this).is(':checked')){
                precioAuxiliar = precioAuxiliar + parseInt($(this).data('precio'));
                //alert(precioAuxiliar);
            }
                
        });
        //$("#noches_inicio").val(nuevas_noches);
        $("#precioTotalPagar").val(parseInt(precioAuxiliar)*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioDesc").text(parseInt(precio) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioPlano").text(parseInt(precioPlano) * parseInt($("#tiempo").val()) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioPaq").val(parseInt(precio));
        $("#precioSF").val(parseInt(precioAuxiliar));
        $("#nombrePaq").val(nombrePaq);
    });

    $("#HBlue").change(function(){
        var dias = parseInt($("#tiempo").val()) + parseInt(1);
        if($(this).attr('checked')){
            var precio = $("#precioAHB").val();
            var precioPlano = $("#precioPlanoAHB").val();
            var tipoRegimen = 'Desayuno + Servicios Hesperia Blue';
            $("#vblue").val(1);
        }else{
            var precio = $("#precioAD").val();
            var precioPlano = $("#precioPlanoAD").val();
            var tipoRegimen = 'Desayuno incluido';
            $("#vblue").val(0);
        }
            
        
        var nombrePaq = $("#nombrePaqInicial").val().split("-")[0]+'<br>Estadía con ' + tipoRegimen ;
        
        //alert(precio.toFixed(2));

        /*var noches = parseInt($("#noches_inicio").val()) ;
        if(noches > 0)
            precio = parseInt(precio) / parseInt(noches);*/
        
        var nuevas_noches = parseInt($("#tiempo").val()) - parseInt($("#noches_gratis").val());
        precio = precio * nuevas_noches;

        precioAuxiliar = precio;
        $(".aSumar").each(function(){
            if($(this).is(':checked')){
                precioAuxiliar = precioAuxiliar + parseInt($(this).data('precio'));
                //alert(precioAuxiliar);
            }
                
        });
        //$("#noches_inicio").val(nuevas_noches);
        $("#precioTotalPagar").val(parseInt(precioAuxiliar)*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioDesc").text(parseInt(precio) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioPlano").text(parseInt(precioPlano) * parseInt($("#tiempo").val()) * 100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $("#precioPaq").val(parseInt(precio));
        $("#precioSF").val(parseInt(precioAuxiliar));
        $("#nombrePaq").val(nombrePaq);
    });
    

});



function enviarReferencia(id,total){

    if(ValidarEnvioReferecia(id)){

        var idRecordatorio = '#recordatorio'+id;

        var idLabelId = '#labelId'+id;

        var idLabelArchivo = '#labelArchivo'+id;

        var idButton = '#btnRef'+id;

        var idLoader = '#loaderRef'+id;

        var idRef = '#referencia'+id;

        var form = '#formEnviarReferencia'+id;

        var idImg = "imagen"+id;

        var idImgen = "#imagen"+id;

        var idNombres = '#nombres'+id;

        var idApellidos = '#apellidos'+id;

        var idSitio = '#sitio'+id;

        $(idButton).toggle();

        $(idRef).toggle();

        $(idImgen).toggle();

        $(idLoader).toggle();

        $(idRecordatorio).toggle();

        $(idLabelId).toggle();

        $(idLabelArchivo).toggle();



        var foto = document.getElementById(idImg);

        var temp_foto=foto.files;

        var referencia = $(idRef).val();

        var nombres = $(idNombres).val();

        var apellidos = $(idApellidos).val();

        var sitio = $(idSitio).val();



        var formData = new FormData();

        formData.append('imagen', temp_foto[0]);

        formData.append('referencia', referencia);

        formData.append('id', id);

        formData.append('nombres', nombres);

        formData.append('apellidos', apellidos);

        formData.append('sitio', sitio);

            

            setTimeout(function(){

                     var resultado=$.ajax({

                        type:"POST",

                        data:formData,

                        url:'https://hoteleshesperia.com.ve/include/enviarReferencia.php',

                        contentType:false,

                        processData:false,

                        cache:false,async:false,

                        complete: function(){                        

                            $(idLoader).toggle();

                        }

                    }).responseText;

                     if(resultado == '1'){
                        var total =   
                        ga('send', 'event', 'intangible', 'envio-comprobante-hab', sitio+'-'+nombres+'-'+apellidos+'-'+id, total);
                        var resultado = '<div class="alert alert-success" role="alert"> <p>Su solicitud de reserva ha sido enviada al departamento de reservas, donde nuestro personal verificaran la efectividad de su pago</p></div>';

                     }else{   

                        var resultado = '<div class="alert alert-danger" role="alert"> <p>El comprobante indicado está en un formato no válido. Intente nuevamente</p></div>';                 

                        

                        $(idButton).toggle();

                        $(idRef).toggle();

                        $(idImgen).toggle();

                        $(idRecordatorio).toggle();

                        $(idLabelId).toggle();

                        $(idLabelArchivo).toggle();

                     }

                    $("#RespuestaReserva").html(resultado);



                    

                }, 2000);

    }

}



function enviarReferenciaPaq(id,total){



    if(ValidarEnvioReferecia(id)){



        var idRecordatorio = '#recordatorio'+id;

        var idLabelId = '#labelId'+id;

        var idLabelArchivo = '#labelArchivo'+id;

        var idButton = '#btnRef'+id;

        var idLoader = '#loaderRef'+id;

        var idRef = '#referencia'+id;

        var form = '#formEnviarReferencia'+id;

        var idImg = "imagen"+id;

        var idImgen = "#imagen"+id;

        var idNombres = '#nombres'+id;

        var idApellidos = '#apellidos'+id;

        var idSitio = '#sitio'+id;

        $(idButton).toggle();

        $(idRef).toggle();

        $(idImgen).toggle();

        $(idLoader).toggle();

        $(idRecordatorio).toggle();

        $(idLabelId).toggle();

        $(idLabelArchivo).toggle();



        var foto = document.getElementById(idImg);

        var temp_foto=foto.files;

        var referencia = $(idRef).val();

        var nombres = $(idNombres).val();

        var apellidos = $(idApellidos).val();

        var sitio = $(idSitio).val();



        var formData = new FormData();

        formData.append('imagen', temp_foto[0]);

        formData.append('referencia', referencia);

        formData.append('id', id);

        formData.append('nombres', nombres);

        formData.append('apellidos', apellidos);

        formData.append('sitio', sitio);

            

            setTimeout(function(){

                     var resultado=$.ajax({

                        type:"POST",

                        data:formData,

                        url:'https://hoteleshesperia.com.ve/include/enviarReferenciaPaq.php',

                        contentType:false,

                        processData:false,

                        cache:false,async:false,

                        complete: function(){                        

                            $(idLoader).toggle();

                        }

                    }).responseText;

                     if(resultado == '1'){  
                        ga('send', 'event', 'intangible', 'envio-comprobante-paq', sitio+'-'+nombres+'-'+apellidos+'-'+id, total);
                        var resultado = '<div class="alert alert-success" role="alert"> <p>Su solicitud de reserva ha sido enviada al departamento de reservas, donde nuestro personal verificaran la efectividad de su pago</p></div>';

                     }else{                    

                        var resultado = '<div class="alert alert-danger" role="alert"> <p>El comprobante indicado está en un formato no válido. Intente nuevamente</p></div>';                 

                        

                        $(idButton).toggle();

                        $(idRef).toggle();

                        $(idImgen).toggle();

                        $(idRecordatorio).toggle();

                        $(idLabelId).toggle();

                        $(idLabelArchivo).toggle();

                     }

                    $("#RespuestaReserva").html(resultado);



                    

                }, 2000);

    }

}





function ValPago() {

     var listBin = [414754, 439475, 439476, 522836, 546774, 546803, 627166, 603122, 603256, 541431, 541434, 602695, 406260,
    411047, 421884, 421885, 421886, 422714, 448174, 454134, 454135, 454136, 454188, 455612, 455613, 455614, 455615, 455627,
    455630, 455631, 455724, 455726, 455727, 455729, 459347, 462229, 480433, 493802, 493803, 493804, 512399, 517648, 517657,
    518031, 518152, 518225, 521877, 525739, 540019, 540060, 540075, 540084, 540131, 540142, 540143, 540144, 540145, 541395,
    541407, 541417, 541459, 541463, 542037, 542209, 542624, 546690, 546694, 547290, 547291, 547552, 547588, 549115, 549192,
    552267, 552292, 552590, 554770, 554790, 558491, 558753, 558834, 558841, 589941, 601705, 520173, 522131, 546474, 601759,
    406267, 407440, 411850, 411851, 414764, 422050, 422228, 430906, 476515, 486520, 494170, 499929, 499930, 517707, 518269,
    518310, 524621, 526749, 536570, 604841, 621984, 411032, 411096, 421930, 422099, 452517, 452518, 453230, 453231, 453232,
    453233, 455640, 455641, 456714, 467126, 479320, 479321, 498870, 498871, 501877, 501878, 517758, 527561, 530470, 530471,
    530472, 541247, 541844, 544680, 546088, 547303, 547304, 547793, 548851, 548852, 549190, 550291, 552266, 558870, 588891,
    402765, 411097, 420198, 421177, 421178, 437774, 438095, 439460, 439461, 439462, 446870, 450604, 450630, 450914, 454041,
    454042, 454439, 454440, 473210, 518002, 531295, 539608, 540009, 540628, 542007, 546890, 547326, 547327, 547589, 549197,
    552283, 556605, 589524, 601686, 411019, 422225, 451315, 451325, 454137, 454138, 454139, 457567, 476820, 481290, 489995,
    533416, 540085, 540086, 540132, 549193, 552284, 552299, 603644, 404849, 411018, 414786, 415229, 421882, 433121, 440827,
    454732, 456032, 456033, 456034, 456035, 479344, 518093, 525973, 541154, 543725, 547032, 547469, 549196, 552325, 627534,
    377000, 377010, 377020, 377030, 377031, 377032, 377033, 377034, 377035, 377036, 377037, 377038, 377039, 402686, 407616,
    409709, 414640, 422242, 441132, 441133, 446844, 450961, 450962, 454619, 467478, 474154, 493752, 493753, 517859, 539658,
    540133, 540137, 540932, 543212, 544388, 544395, 552265, 554388, 554395, 554665, 554912, 554937, 558844, 601400, 603071,
    473209, 601653, 500784, 543694, 548505, 554771, 554791, 415270, 415271, 491951, 554566, 554772, 554792, 491508, 491509, 
    601418, 439813, 439814, 455608, 455609, 455610, 455611, 456804, 490735, 407457, 422268, 427170, 434613, 434614, 434615,
    439482, 439483, 518067, 522824, 545174, 554920, 554972, 558762, 603684, 455708, 455709, 455710, 455711, 473276, 473277, 
    545194, 400145, 405761, 409940, 409941, 409942, 415762, 455334, 602693, 013400, 037024, 121099, 123097, 123099, 370243,
    370244, 370245, 371590, 379948, 405932, 411016, 411098, 414530, 421463, 422123, 422169, 450966, 451384, 451385, 451721,
    451722, 451785, 453502, 454500, 454501, 454502, 454504, 454511, 454512, 454513, 454514, 454515, 454516, 454517, 454518,
    454519, 454520, 454530, 454531, 454532, 454533, 454534, 454560, 454561, 454573, 454574, 454575, 454576, 454577, 454578,
    454579, 454580, 454587, 454588, 454589, 454599, 456010, 456011, 456013, 456349, 490788, 490789, 491952, 492457, 496638,
    496676, 496677, 497476, 515827, 523751, 540139, 540140, 540141, 540167, 540470, 540471, 540966, 542094, 542172, 542218,
    542660, 542695, 544194, 544312, 544338, 545049, 546465, 546466, 546492, 546704, 546752, 546753, 547289, 547887, 549189,
    552130, 552311, 554642, 554729, 601288, 601685, 603693, 700013, 824400, 824401, 824402, 824403, 824404, 824490, 824501,
    824601, 999999, 434957, 434958, 434959, 199407, 422039, 453299, 457999, 490112, 490113, 491270, 522963, 548685, 548849,
    548850, 552287, 601618, 434967, 441784, 441785, 441786, 552286, 554798, 554831, 554914, 558886, 621999, 411037, 411038,
    414755, 422230, 514871, 541838, 541839, 550523, 552326, 602904, 517677, 522847, 524369, 553480, 554210, 554211, 603208, 
    434963, 434964, 452490, 452491, 458183, 458184, 524083, 552264, 628001, 627101, 504157, 531241, 531707, 547153, 553635,
    603555, 400606, 405890, 410808, 410809, 411858, 450685, 454000, 467477, 475783, 475784, 476560, 476561, 476562, 518150,
    518854, 522270, 539667, 541614, 541721, 541748, 541782, 547630, 554646, 554735, 554748, 554934, 589950, 602954, 603216,
    405898, 414768, 421037, 422654, 471240, 499889, 522835, 522974, 524892, 525712, 534523, 639589, 422271, 425888, 433485,
    433486, 521359, 541841, 541842, 552462, 601582, 411848, 421039, 512177, 541487, 541494, 550568, 554540, 554541, 554547,
    554548, 602000, 515671, 515856, 522299, 603954, 421895, 458129, 458130, 515591, 550478, 550479, 552285, 514008, 530634,
    545220, 554726, 627998, 457380, 457381, 457415, 457416, 474632, 492008, 492009, 492012, 492014, 516447, 517668, 519707,
    521778, 521872, 536440, 542467, 550280, 601750, 407556, 422260, 422261, 422262, 422263, 512742, 512776, 522782, 524339,
    539660, 553438, 639489, 510808, 514026, 518215, 521304, 528839, 545192, 639781, 522523, 524443, 529890, 606401, 427172,
    474496, 474497, 474498, 474499, 517953, 521041, 530147, 546629, 547554, 606061, 512391, 516668, 523739, 585948, 441058,
    441059, 476812, 476813, 476814, 512460, 515659, 515835, 522206, 527356, 541689, 543863, 548586, 555347, 628155, 900805,
    414747, 421444, 434965, 434966, 441040, 441041, 444806, 473292, 491608, 491609, 499941, 517856, 517857, 521305, 522946,
    522975, 534339, 544807, 544909, 545193, 552293, 553663, 554611, 554633, 515817, 527029, 528521, 528940, 553513, 448741,
    448742, 451449, 451450, 508111, 519723, 546489, 546490, 548686, 552565, 554929, 422044, 422045, 422046, 425881, 446334,
    515673, 515892, 520048, 520148, 520154, 528004, 552327, 627609, 544518, 554518, 602692, 524344, 554799, 554833, 603124,
    550567, 603061, 421034, 421035, 421036, 517981, 518539, 627398, 554500, 554534, 425478, 425479, 504934, 517674];

    var str, ruta=document.FormPayUsuario, error="";

    if(ruta.nombres.value == "")

    { error += "Indicar su Nombre <br>";}

    if(ruta.apellidos.value == "")

    { error += "Indicar su Apellido <br>";}

    if(ruta.email.value == "")

    { error += "Ingresar su eMail <br>";}

    if(ruta.emailR.value == "")

    { error += "Repita su eMail <br>";}

    if(ruta.emailR.value != ruta.email.value)

    { error += "No coinciden los eMail <br>";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida <br>"; }

    if(ruta.ced.value == "")

    { error += "Indicar Cédula del titular de reserva<br>";}

    if(ruta.telefono.value == "")

    { error += "Indicar su telefono <br>";}

    if(ruta.nombre_tarjeta.value == "")

    { error += "Indicar el nombre del Tarjeta Habiente <br>";}

     var valor = parseInt(ruta.NumTarj.value);

    if(ruta.NumTarj.value == "")

    { error += "Indicar el Numero de la Tarjeta <br>";}

    if(ruta.NumTarj.value.length >= 17 || ruta.NumTarj.value.length <= 14)

    { error += "La cantidad de digitos no corresponde a un tarjeta valida <br>";

    }

    if (isNaN(valor)) {

        error += "El Campo Tarjeta Solo Acepta Numeros <br>";

    }

     var identificacion = parseInt(ruta.identificacion.value);

    if(ruta.identificacion.value == "")

    { error += "Indicar el Numero de Cedula <br>";}

    if(ruta.identificacion.value.length >= 9 || ruta.identificacion.value.length <= 5)

    { error += "La cantidad de digitos no corresponde a una Cedula o RIF valido <br>";}

    if (isNaN(identificacion)) {

        error += "El Campo Cedula o RIF Solo Acepta Numeros <br>";

    }

    if(jQuery.inArray(parseInt(ruta.NumTarj.value.substring(0, 6)), listBin)>0){
        //alert(parseInt(ruta.NumTarj.value.substring(0, 6)));
        //alert("tarjeta de credito nacional");
    }else{
        //alert(parseInt(ruta.NumTarj.value.substring(0, 6)));
        //alert("tarjeta inter");
         error += "numero de tarjeta de credito inválido<br>";
    }

     var Expira = parseInt(ruta.Expira.value);

    if(ruta.Expira.value == "")

    { error += "Indicar Fecha de Expiracion <br>";}

    if(ruta.Expira.value.length >= 5 || ruta.Expira.value.length <= 3)

    { error += "La Fecha de Expiracion Es Erronea <br>";}

    if (isNaN(Expira)) {

        error += "La Fecha de Expericion es solo Numeros <br>";

    }

    if(ruta.Expira.value < ruta.years.value)

    { error += "La Fecha de Expiracion No puede ser menor al a#o actual <br>";}



    var MesExpira = parseInt(ruta.MesExpira.value);

    if(ruta.MesExpira.value == "")

    { error += "Indicar El Mes de Expiracion <br>";}

    if(ruta.MesExpira.value.length >= 3 || ruta.MesExpira.value.length <= 1 )

    { error += "El Mes de Expiracion Es Erronea <br>";}

    if (isNaN(MesExpira)) {

        error += "El Mes de Expiracion es solo Numeros <br>";

    }

    if(ruta.MesExpira.value == 0 || ruta.MesExpira.value >= 13 )

    { error += "Debe Colocar un Mes Valido <br>";}



    var cvc = parseInt(ruta.cvc.value);

    if(ruta.cvc.value == "")

    { error += "Indicar CVC <br>";}

    if(ruta.cvc.value.length >= 4 || ruta.cvc.value.length <= 2)

    { error += "El Codigo CVC Es Erroneo Debe Ser Almenos 3 Digitos <br>";}

    if (isNaN(cvc)) {

        error += "EL Campo CVC es solo Numeros <br>";

    }



    if(!ruta.politicas.checked){

        error += "Debe aceptar las políticas de uso <br>";

    }

    if(error!=""){

        //alert("Lista de Errores encontrados:\n\n"+error);        

        $("#RespuestaIp").html('<div class="alert alert-danger">'+error+'</div>');

        return false;

    }else{     



        $("#RespuestaIp").html('');   

        return true;

    }

}

function validarFormBitPagos(){
    return ValidarFormPreReserva();
    
}

function ValidarFormPreReserva() {

    var str, ruta=document.FormPayUsuario, error="";

    if(ruta.nombres.value == "")

    { error += "Indicar su Nombre <br>";}

    if(ruta.apellidos.value == "")

    { error += "Indicar su Apellido <br>";}

    if(ruta.email.value == "")

    { error += "Ingresar su eMail <br>";}

    if(ruta.emailR.value == "")

    { error += "Repita su eMail <br>";}

    if(ruta.emailR.value != ruta.email.value)

    { error += "No coinciden los eMail <br>";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida\n"; }

    if(ruta.telefono.value == "")

    { error += "Indicar su telefono<br>";}

    

    if(error!=""){

        //alert("Lista de Errores encontrados:\n\n"+error);

        $("#RespuestaTrans").html('<div class="alert alert-danger">'+error+'</div>');

        return false;

    }else{    

        $("#RespuestaTrans").html('');    

        return true;

    }

}



function ValidarFormPreReservaPaq() {

    var str, ruta=document.FormPayUsuarioPaq, error="";

    if(ruta.nombres.value == "")

    { error += "Indicar su Nombre <br>";}

    if(ruta.apellidos.value == "")

    { error += "Indicar su Apellido <br>";}

    if(ruta.email.value == "")

    { error += "Ingresar su eMail <br>";}

    if(ruta.emailR.value == "")

    { error += "Repita su eMail <br>";}

    if(ruta.emailR.value != ruta.email.value)

    { error += "No coinciden los eMail <br>";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida<br>"; }

    if(ruta.telefono.value == "")

    { error += "Indicar su telefono<br>";}

    

    if(error!=""){

        //alert("Lista de Errores encontrados:\n\n"+error);

        $("#RespuestaTrans").html('<div class="alert alert-danger">'+error+'</div>');

        return false;

    }else{        

        $("#RespuestaTrans").html(''); 

        return true;

    }

}



function ValPagoPaq() { 

    var listBin = [414754, 439475, 439476, 522836, 546774, 546803, 627166, 603122, 603256, 541431, 541434, 602695, 406260,
    411047, 421884, 421885, 421886, 422714, 448174, 454134, 454135, 454136, 454188, 455612, 455613, 455614, 455615, 455627,
    455630, 455631, 455724, 455726, 455727, 455729, 459347, 462229, 480433, 493802, 493803, 493804, 512399, 517648, 517657,
    518031, 518152, 518225, 521877, 525739, 540019, 540060, 540075, 540084, 540131, 540142, 540143, 540144, 540145, 541395,
    541407, 541417, 541459, 541463, 542037, 542209, 542624, 546690, 546694, 547290, 547291, 547552, 547588, 549115, 549192,
    552267, 552292, 552590, 554770, 554790, 558491, 558753, 558834, 558841, 589941, 601705, 520173, 522131, 546474, 601759,
    406267, 407440, 411850, 411851, 414764, 422050, 422228, 430906, 476515, 486520, 494170, 499929, 499930, 517707, 518269,
    518310, 524621, 526749, 536570, 604841, 621984, 411032, 411096, 421930, 422099, 452517, 452518, 453230, 453231, 453232,
    453233, 455640, 455641, 456714, 467126, 479320, 479321, 498870, 498871, 501877, 501878, 517758, 527561, 530470, 530471,
    530472, 541247, 541844, 544680, 546088, 547303, 547304, 547793, 548851, 548852, 549190, 550291, 552266, 558870, 588891,
    402765, 411097, 420198, 421177, 421178, 437774, 438095, 439460, 439461, 439462, 446870, 450604, 450630, 450914, 454041,
    454042, 454439, 454440, 473210, 518002, 531295, 539608, 540009, 540628, 542007, 546890, 547326, 547327, 547589, 549197,
    552283, 556605, 589524, 601686, 411019, 422225, 451315, 451325, 454137, 454138, 454139, 457567, 476820, 481290, 489995,
    533416, 540085, 540086, 540132, 549193, 552284, 552299, 603644, 404849, 411018, 414786, 415229, 421882, 433121, 440827,
    454732, 456032, 456033, 456034, 456035, 479344, 518093, 525973, 541154, 543725, 547032, 547469, 549196, 552325, 627534,
    377000, 377010, 377020, 377030, 377031, 377032, 377033, 377034, 377035, 377036, 377037, 377038, 377039, 402686, 407616,
    409709, 414640, 422242, 441132, 441133, 446844, 450961, 450962, 454619, 467478, 474154, 493752, 493753, 517859, 539658,
    540133, 540137, 540932, 543212, 544388, 544395, 552265, 554388, 554395, 554665, 554912, 554937, 558844, 601400, 603071,
    473209, 601653, 500784, 543694, 548505, 554771, 554791, 415270, 415271, 491951, 554566, 554772, 554792, 491508, 491509, 
    601418, 439813, 439814, 455608, 455609, 455610, 455611, 456804, 490735, 407457, 422268, 427170, 434613, 434614, 434615,
    439482, 439483, 518067, 522824, 545174, 554920, 554972, 558762, 603684, 455708, 455709, 455710, 455711, 473276, 473277, 
    545194, 400145, 405761, 409940, 409941, 409942, 415762, 455334, 602693, 013400, 037024, 121099, 123097, 123099, 370243,
    370244, 370245, 371590, 379948, 405932, 411016, 411098, 414530, 421463, 422123, 422169, 450966, 451384, 451385, 451721,
    451722, 451785, 453502, 454500, 454501, 454502, 454504, 454511, 454512, 454513, 454514, 454515, 454516, 454517, 454518,
    454519, 454520, 454530, 454531, 454532, 454533, 454534, 454560, 454561, 454573, 454574, 454575, 454576, 454577, 454578,
    454579, 454580, 454587, 454588, 454589, 454599, 456010, 456011, 456013, 456349, 490788, 490789, 491952, 492457, 496638,
    496676, 496677, 497476, 515827, 523751, 540139, 540140, 540141, 540167, 540470, 540471, 540966, 542094, 542172, 542218,
    542660, 542695, 544194, 544312, 544338, 545049, 546465, 546466, 546492, 546704, 546752, 546753, 547289, 547887, 549189,
    552130, 552311, 554642, 554729, 601288, 601685, 603693, 700013, 824400, 824401, 824402, 824403, 824404, 824490, 824501,
    824601, 999999, 434957, 434958, 434959, 199407, 422039, 453299, 457999, 490112, 490113, 491270, 522963, 548685, 548849,
    548850, 552287, 601618, 434967, 441784, 441785, 441786, 552286, 554798, 554831, 554914, 558886, 621999, 411037, 411038,
    414755, 422230, 514871, 541838, 541839, 550523, 552326, 602904, 517677, 522847, 524369, 553480, 554210, 554211, 603208, 
    434963, 434964, 452490, 452491, 458183, 458184, 524083, 552264, 628001, 627101, 504157, 531241, 531707, 547153, 553635,
    603555, 400606, 405890, 410808, 410809, 411858, 450685, 454000, 467477, 475783, 475784, 476560, 476561, 476562, 518150,
    518854, 522270, 539667, 541614, 541721, 541748, 541782, 547630, 554646, 554735, 554748, 554934, 589950, 602954, 603216,
    405898, 414768, 421037, 422654, 471240, 499889, 522835, 522974, 524892, 525712, 534523, 639589, 422271, 425888, 433485,
    433486, 521359, 541841, 541842, 552462, 601582, 411848, 421039, 512177, 541487, 541494, 550568, 554540, 554541, 554547,
    554548, 602000, 515671, 515856, 522299, 603954, 421895, 458129, 458130, 515591, 550478, 550479, 552285, 514008, 530634,
    545220, 554726, 627998, 457380, 457381, 457415, 457416, 474632, 492008, 492009, 492012, 492014, 516447, 517668, 519707,
    521778, 521872, 536440, 542467, 550280, 601750, 407556, 422260, 422261, 422262, 422263, 512742, 512776, 522782, 524339,
    539660, 553438, 639489, 510808, 514026, 518215, 521304, 528839, 545192, 639781, 522523, 524443, 529890, 606401, 427172,
    474496, 474497, 474498, 474499, 517953, 521041, 530147, 546629, 547554, 606061, 512391, 516668, 523739, 585948, 441058,
    441059, 476812, 476813, 476814, 512460, 515659, 515835, 522206, 527356, 541689, 543863, 548586, 555347, 628155, 900805,
    414747, 421444, 434965, 434966, 441040, 441041, 444806, 473292, 491608, 491609, 499941, 517856, 517857, 521305, 522946,
    522975, 534339, 544807, 544909, 545193, 552293, 553663, 554611, 554633, 515817, 527029, 528521, 528940, 553513, 448741,
    448742, 451449, 451450, 508111, 519723, 546489, 546490, 548686, 552565, 554929, 422044, 422045, 422046, 425881, 446334,
    515673, 515892, 520048, 520148, 520154, 528004, 552327, 627609, 544518, 554518, 602692, 524344, 554799, 554833, 603124,
    550567, 603061, 421034, 421035, 421036, 517981, 518539, 627398, 554500, 554534, 425478, 425479, 504934, 517674];
    
    var str, ruta=document.FormPayUsuarioPaq, error="";

    if(ruta.nombres.value == "")

    { error += "Indicar su Nombre <br>";}

    if(ruta.apellidos.value == "")

    { error += "Indicar su Apellido <br>";}

    if(ruta.email.value == "")

    { error += "Ingresar su eMail <br>";}

    if(ruta.emailR.value == "")

    { error += "Repita su eMail <br>";}

    if(ruta.emailR.value != ruta.email.value)

    { error += "No coinciden los eMail <br>";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida<br>"; }

    if(ruta.ced.value == "")

    { error += "Indicar Cédula del titular de reserva<br>";}

    if(ruta.telefono.value == "")

    { error += "Indicar su telefono <br>";}

    if(ruta.nombre_tarjeta.value == "")

    { error += "Indicar el nombre del Tarjeta Habiente <br>";}

     var valor = parseInt(ruta.NumTarj.value);

    if(ruta.NumTarj.value == "")

    { error += "Indicar el Numero de la Tarjeta <br>";}

    if(ruta.NumTarj.value.length >= 17 || ruta.NumTarj.value.length <= 14)

    { error += "La cantidad de digitos no corresponde a un tarjeta valida <br>";}

    if (isNaN(valor)) {

        error += "El Campo Tarjeta Solo Acepta Numeros <br>";

    }

    if(jQuery.inArray(parseInt(ruta.NumTarj.value.substring(0, 6)), listBin)>0){
        //alert(parseInt(ruta.NumTarj.value.substring(0, 6)));
        //alert("tarjeta de credito nacional");
    }else{
        //alert(parseInt(ruta.NumTarj.value.substring(0, 6)));
        //alert("tarjeta inter");
         error += "numero de tarjeta de credito inválido<br>";
    }

     var identificacion = parseInt(ruta.identificacion.value);

    if(ruta.identificacion.value == "")

    { error += "Indicar el Numero de Cedula <br>";}

    if(ruta.identificacion.value.length >= 9 || ruta.identificacion.value.length <= 5)

    { error += "La cantidad de digitos no corresponde a una Cedula o RIF valido <br>";}

    if (isNaN(identificacion)) {

        error += "El Campo Cedula o RIF Solo Acepta Numeros <br>";

    }



     var Expira = parseInt(ruta.Expira.value);

    if(ruta.Expira.value == "")

    { error += "Indicar Fecha de Expiracion <br>";}

    if(ruta.Expira.value.length >= 5 || ruta.Expira.value.length <= 3)

    { error += "La Fecha de Expiracion Es Erronea <br>";}

    if (isNaN(Expira)) {

        error += "La Fecha de Expericion es solo Numeros <br>";

    }

   /* if(ruta.Expira.value < ruta.years.value)

    { error += "La Fecha de Expiracion No puede ser menor al a#o actual\n";}

*/

    var MesExpira = parseInt(ruta.MesExpira.value);

    if(ruta.MesExpira.value == "")

    { error += "Indicar El Mes de Expiracion <br>";}

    if(ruta.MesExpira.value.length >= 3 || ruta.MesExpira.value.length <= 1 )

    { error += "El Mes de Expiracion Es Erronea <br>";}

    if (isNaN(MesExpira)) {

        error += "El Mes de Expiracion es solo Numeros <br>";

    }

    if(ruta.MesExpira.value == 0 || ruta.MesExpira.value >= 13 )

    { error += "Debe Colocar un Mes Valido <br>";}



    var cvc = parseInt(ruta.cvc.value);

    if(ruta.cvc.value == "")

    { error += "Indicar CVC <br>";}

    if(ruta.cvc.value.length >= 4 || ruta.cvc.value.length <= 2)

    { error += "El Codigo CVC Es Erroneo Debe Ser Almenos 3 Digitos <br>";}

    if (isNaN(cvc)) {

        error += "EL Campo CVC es solo Numeros <br>";

    }



    if(!ruta.politicas.checked){

        error += "Debe aceptar las políticas de uso <br>";

    }

    if(error!=""){

        //alert("Lista de Errores encontrados:\n\n"+error);

        $("#RespuestaIp").html('<div class="alert alert-danger">'+error+'</div>');

        return false;

    }else{        

        $("#RespuestaIp").html(''); 

        return true;

    }

        

}



function VerReservaPaq(idReservacion)

{  var resultado = $.ajax({

  type: "POST",

  data: "idReservacion="+idReservacion,

  url: 'https://hoteleshesperia.com.ve/include/ver_reservacion_paq.php',

  dataType: 'text',async:false

 }).responseText;

 document.getElementById("RespuestaReserva").innerHTML = resultado;

}





/*Validar envio referencia*/

function ValidarEnvioReferecia(idReservacion)

{  

    

   var idReferencia = '#referencia'+idReservacion;

   var idImagen = '#imagen'+idReservacion;

   var foto = document.getElementById('imagen'+idReservacion);

            var temp_foto=foto.files;

   var retorno = true;



   if($.trim($(idReferencia).val()) == ""){

        $("#RespuestaReserva").html('<div class="alert alert-danger">Debe Indicar el N° de identificación de la transferencia. </div>');

        //alert('Debe Indicar como mínimo el N° de identificación de la transferencia');

        retorno = false;

   }



   if($.trim($(idImagen).val()) == ""){

        $("#RespuestaReserva").html('<div class="alert alert-danger">Debe adjuntar el comprobante de transferencia.</div>');

        //alert('Debe Indicar como mínimo el N° de identificación de la transferencia');

        retorno = false;

   }else{
        retorno = validar_peso(temp_foto[0]);
        if (!retorno){
            $("#RespuestaReserva").html('<div class="alert alert-danger">El tamaño del archivo excede el límite (2 MB)</div>');
        }
   }



   return retorno;



}


function ValidarEnvioRefereciaFront()

{  

    

   var idReferencia = '#referencia';

   var idImagen = '#imagen';

   var foto = document.getElementById('imagen');
    var temp_foto=foto.files;

   var retorno = true;



   if($.trim($(idReferencia).val()) == ""){

        $("#RespuestaTrans").html('<div class="alert alert-danger">Debe Indicar el N° de identificación de la transferencia. </div>');

        //alert('Debe Indicar como mínimo el N° de identificación de la transferencia');

        retorno = false;

   }



   if($.trim($(idImagen).val()) == ""){

        $("#RespuestaTrans").html('<div class="alert alert-danger">Debe adjuntar el comprobante de transferencia.</div>');

        //alert('Debe Indicar como mínimo el N° de identificación de la transferencia');

        retorno = false;

   }else{
        retorno = validar_peso(temp_foto[0]);
        if (!retorno){
            $("#RespuestaTrans").html('<div class="alert alert-danger">El tamaño del archivo excede el límite (2 MB)</div>');
        }
   }

   return retorno;



}

function validar_peso(obj){
 
 size=obj.size;
 sizemb= size/1024/1024;

 if (sizemb>2){
  return false;
 }else{
  return true;
 }   
}


function aceptarPago(){
      $("#btnInstapago").toggle();

           $("#loader").toggle();

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:$("#FormPayUsuario").serialize(),

                    url:'https://hoteleshesperia.com.ve/include/instapago.php',

                    dataType:'text',async:false,

                    complete: function(){

                        $("#btnInstapago").toggle();

                        $("#loader").toggle();

                    }

                }).responseText;
                 var hotel = $("#nombre_hotel").text();

                 var mail = $("#email").val();
                 var total = $("#precio").val();
                 var split = total.split(",");
                 split=split[0].replace(/\./g,'');

                 var desc = hotel+" - "+mail; 


                if(resultado.substr(1, 4) == "html"){

                    ga('send', 'event', 'tangible-tdc', 'reserva-habitacion', desc , total);

                    $("#datosCompra").html(resultado);
                    $('body,html').scrollTop(0);

                }else{

                    document.getElementById("RespuestaIp").innerHTML=resultado;

                } 

     }, 2000);          
    
}
function aceptarPagoPaq(){
    $("#btnInstapagoPaq").toggle();

            $("#loaderPaq").toggle();

            setTimeout(function(){

                 var resultado=$.ajax({

                    type:"POST",

                    data:$("#FormPayUsuarioPaq").serialize(),

                    url:'https://hoteleshesperia.com.ve/include/instapagoPaquetes.php',

                    dataType:'text',async:false,

                    complete: function(){

                        $("#btnInstapagoPaq").toggle();

                        $("#loaderPaq").toggle();

                    }

                }).responseText;
                 var hotel = $("#nombre_hotel").text();
                 var mail = $("#email").val();
                 var total = $("#precio").val();
                 var split = total.split(",");
                 split=split[0].replace(/\./g,'');

                 var desc = hotel+" - "+mail; 

                if(resultado.substr(1, 4) == "html"){
                    ga('send', 'event', 'tangible', 'reserva-paquete', desc , split);

                    $("#datosCompraPaq").html(resultado);

                    $('body,html').scrollTop(0);

                

                }else{

                    document.getElementById("RespuestaIp").innerHTML=resultado;

                }

            }, 2000);

}

function validarBL(){
    $.ajax({

            type:"POST",

            data:$("#FormPayUsuario").serialize(),

            url:'https://hoteleshesperia.com.ve/include/blacklist.php',

            dataType:'text',async:false,

            success: function(response){
               // alert(response);
                if(response =="0"){
                    aceptarPago();
                }else{
                    alert("Por motivos de seguridad su operacion no ha podido ser procesada");
                    $("#RespuestaIp").html("<p class='bg-warning text-center'>Por motivos de seguridad su reservación no ha podido ser procesada, ante cualquier duda comuniquese con info@hoteleshesperia.com.ve</p>");
                }
            },

            complete: function(){

          /*  $("#btnInstapago").toggle();

            $("#loader").toggle();*/

            }

    });
}


function validarBLPaq(){
    $.ajax({

            type:"POST",

            data:$("#FormPayUsuarioPaq").serialize(),

            url:'https://hoteleshesperia.com.ve/include/blacklist.php',

            dataType:'text',async:false,

            success: function(response){
                //alert(response);
                if(response =="0"){
                    aceptarPagoPaq();
                }else{
                   // alert("Por motivos de seguridad su operacion no ha podido ser procesada");
                    $("#RespuestaIp").html("<p class='bg-warning text-center'>Por motivos de seguridad su reservación no ha podido ser procesada, ante cualquier duda comuniquese con info@hoteleshesperia.com.ve</p>");
                }
            },

            complete: function(){

          /*  $("#btnInstapago").toggle();

            $("#loader").toggle();*/

            }

    });
}