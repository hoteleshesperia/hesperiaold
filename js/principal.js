$(document).ready(function(){

 if($("#valIP").val() != 'VE' ){
    if($("#respondioModalMoneda").val() == '0'){
      $('#modal-container-Hblue2').modal('show');
    }
      
  }
  $("#noCambiaAbs").on('click', function(){
    $.ajax({

        type:"POST",
        url:"https://hoteleshesperia.com.ve/include/noCambioAbs.php",
        
        contentType:false,
        processData:false,
        cache:false,
        success: function(response){
          if(response=="ok"){
             $('#modal-container-Hblue2').modal('hide');
             location.reload();
          }else{
            location.reload();
          }
        },
        error: function(response){
         location.reload();
        }
      }); 
  });
  $("#cambiaAbs").on('click', function(){
    $("#respondioModalMoneda").val('1');
    $.ajax({

        type:"POST",
        url:"https://hoteleshesperia.com.ve/include/cambiaAbs.php",
        
        contentType:false,
        processData:false,
        cache:false,
        success: function(response){
          if(response=="ok"){
             $('#modal-container-Hblue2').modal('hide');
             location.reload();
          }else{
            location.reload();
          }
        },
        error: function(response){
         location.reload();
        }
      }); 
  });
   
var upsAux = 0;
  $(".UpsPers").each(function(){
    if(upsAux == 0){
      //alert('seteando: ' + parseInt($(this).data('precio')));
      $(this).prop('checked', true);
      $("#precioSF").val(parseInt($("#precioPaq").val()) + parseInt($(this).data('precio')));
      
    }else{
      $(this).prop('checked', false);
    }
    $("#precioTotalPagar").val(parseInt($("#precioSF").val())*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    upsAux++;

    });  

var mobile = (/iPhone|iPad|iPod|android|blackberry|bb|mini|Nokia|PlayBook|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
if (mobile) {

    $( ".my-reserva" ).remove(); // delete reservation form

    $("#container-habitaciones").hide();

    $("#widget-facebook").hide();

    $("#widget-instagram").hide();

    $(".imgPortadaV").hide();

    $(".pdf-thumb-box-overlay").addClass("hover-overlay");
}else{

  $(".pdf-thumb-box").hover(function(){
   var id =$(this).attr("id");
    $("#"+id+ " .pdf-thumb-box-overlay").toggleClass("hover-overlay");

  });
}

if (window.history && window.history.pushState && window.location.href == ' https://hoteleshesperia.com.ve/reserva-habitacion/confirmar-pago') {



            $(window).on('popstate', function() {

              var hashLocation = location.hash;

              var hashSplit = hashLocation.split("#!/");

              var hashName = hashSplit[1];



              if (hashName !== '') {

                var hash = window.location.hash;

                if (hash === '') {

                    window.location=' https://hoteleshesperia.com.ve/';

                    return false;

                }

              }

            });



            window.history.pushState('forward', null, './#forward');

          }
    $("#share").jsSocials({
        showLabel: false,
        showCount: false,
        shareIn: "popup",
        shares: ["twitter", "facebook", "googleplus", "whatsapp"]
      });

    $('[data-toggle="popover"]').popover();
     //alert(latter);
    var nowTemp = new Date();
    var h = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0); 
    //nowTemp.setSeconds(86400);
    var hoy = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    $('.dropdown-toggle').dropdown();
    $('.dpd3').on('click', function(date) {
          var desde = $(this).attr('data-min-date');
             //alert(desde);
         var anio = desde.substring(0,4);
         var mes = desde.substring(5,7);
         mes = parseInt(mes) - 1;
         var dia = desde.substring(8,10);

         var now = new Date(anio, mes, dia, 0, 0, 0, 0);
         if(hoy >= now){
            now = hoy;
         }

         var hasta = $(this).attr('data-max-date');
         var anio2 = hasta.substring(0,4);
         var mes2 = hasta.substring(5,7);
         mes2 = parseInt(mes2) - 1;
         var dia2 = hasta.substring(8,10);

         var latter = new Date(anio2, mes2, dia2, 0, 0, 0, 0);

         checkin = $(this).datepicker({
            
          onRender: function(date) {    
            return ((date.valueOf() > now.valueOf()) && (date.valueOf() <= latter.valueOf())) ? '' : 'disabled';
          }
        }).datepicker('show').data('datepicker');
        
    });

 $('#fechaNac').on('click', function(date) {

      $(this).datepicker({
            onRender: function(date) {    
                return (date.valueOf() <= h.valueOf()) ? '' : 'disabled';
              }
      }).datepicker('show').data('datepicker');
  });

 //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

 $("ul.tabs li").click(function() {

        $("ul.tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });

 $(".reset-reserva #member").val("");

  $(".portada-ver-mas").click(function(e){
      e.preventDefault();
      $(this).hide();
      var id = $(this).attr("id");
      $("#"+id+" + .portada-span").show();
  });

  /*Siguiente lineas jquery añadidas el 09/03/2016*/

  $("#numPaqV2").change(function(){
        $("#precioTotalPagar").val(($(this).val()*$("#precioPaq").val()*100)+($("#numPaqNV2").val()*$("#precioNino").val()*100)).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    });

  /*$("#numPaqV2Only").change(function(){
        $("#precioTotalPagar").val(($(this).val()*$("#precioPaq").val()*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    });*/

  $("#numPaqNV2").change(function(){
        $("#precioTotalPagar").val(($(this).val()*$("#precioNino").val()*100)+($("#numPaqV2").val()*$("#precioPaq").val()*100)).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    });

  $(".checkUpselling").click(function(){
    if($(this).is(':checked')){
      $("#precioSF").val(parseInt($("#precioSF").val()) + parseInt($(this).data('precio')));
    }else{
      $("#precioSF").val(parseInt($("#precioSF").val()) - parseInt($(this).data('precio')));
    }
    $("#precioTotalPagar").val(parseInt($("#precioSF").val())*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
    
    
  });

   $("#perso1").click(function(){

    
    if($("#perso2").is(':checked')){
      $("#precioSF").val(parseInt($("#precioSF").val()) - parseInt($("#perso2").data('precio')));
      $("#perso2").prop('checked', false);
      $("#precioSF").val(parseInt($("#precioSF").val()) + parseInt($("#perso1").data('precio')));
    }else{
      
        //alert('Elemento ha sido chequeado');
        $("#perso1").prop('checked', true);
      
    }
      
    
    
    $("#precioTotalPagar").val(parseInt($("#precioSF").val())*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });

  });

  $("#perso2").click(function(){

    
    if($("#perso1").is(':checked')){
      $("#precioSF").val(parseInt($("#precioSF").val()) - parseInt($("#perso1").data('precio')));
      $("#perso1").prop('checked', false);
      $("#precioSF").val(parseInt($("#precioSF").val()) + parseInt($("#perso2").data('precio')));
    }else{
      
        //alert('Elemento ha sido chequeado');
        $("#perso2").prop('checked', true);
      
        

      //alert('No hace nada');
    }
      
    
    
    $("#precioTotalPagar").val(parseInt($("#precioSF").val())*100).priceFormat({          
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });

  });

  $('#timepicker1').timepicker();

   var maxHeight = 350;          
    $(".equalize").each(function(){
      if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });         
    $(".equalize").height(maxHeight);

    var maxHeightAtrapa = 480;          
    $(".equalizeAtrapa").each(function(){
      if ($(this).height() > maxHeightAtrapa) { maxHeightAtrapa = $(this).height(); }
    });         
    $(".equalizeAtrapa").height(maxHeightAtrapa);

    $(".bp-partner-button").hide();
    
   var pgwSlideshow = $('.galeriaSlide').pgwSlideshow({
       // mainClassName: galeriaSlide,
       /// autoSlide: false,
        displayList: true,

  });

 $("#galeriaTab").click(function(){
     pgwSlideshow.nextSlide(); 
 });

  var sideslider = $('[data-toggle=collapse-side]');
  var sel = sideslider.attr('data-target');
  var sel2 = sideslider.attr('data-target-2');
  sideslider.click(function(event){
	   $(sel).toggleClass('in');
	   $(sel2).toggleClass('out');
   });

  var nowTemp = new Date();
  var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
  $('#fecha_restaurant').datepicker({
     onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      },
      'format': 'dd-mm-yyyy'
  });

  $("#reservar-restaurant").click(function(){
    var form_data = new FormData($("#form-reserva-restaurant")[0]);
    form_data.append("accion", "reservar-restaurant");
    form_data.append("nombre_rest", $("#id_restaurant option:selected").text());
    var email = $("#email-rest").val();
    var errores ="";
    if ($("#asistentes").val() <= 0 || $("#asistentes").val() > 20) {
      errores+="Cantidad de asistentes inválida\n";
    };

    if ($("#mesas").val() <=0 || $("#mesas").val() > 10) {
      errores+="Cantidad de mesas inválida\n";
    }; 

    if ($("#nombre").val()=="") {
      errores+="Debe ingresar su nombre\n";
    };

    if (!email.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i)) {
      errores+="Debe ingresar un email válido\n";
    };   

    if (isNaN($("#telefono").val())) {
      errores+="Ingrese un numero telefonico válido\n";
    };
    if (!isDate($("#fecha_restaurant").val())) {
       errores+="Ingrese una fecha válida\n";
    }
    
    if (errores=="") {
      reservar_restaurant(form_data);
    }else{
      alert(errores);
    }
    

  });

if ($(window.location).attr('hash')=="") {
   //alert("sin hash"); NADA
  }else{
    var aux = $(location).attr('hash');
    var rest = aux.substring(1,2) 
    $("#id_restaurant").val(rest);
}

$('.reserva-restaurant').on('click', function(event) {
      event.preventDefault();
      var valor = $(this).data("value");

      $("#id_restaurant").val(valor);

      var target = $(this.getAttribute('href'));
      if( target.length ) {
          
          $('html, body').stop().animate({
              scrollTop: target.offset().top-200
          }, 1000);
      }
    });

  $('[data-date]').each(function() {
      $(this).TimeCircles({

        "count_past_zero": false,
        "animation": "smooth",
    "bg_width": 1,
    "fg_width": 0.04,
    "circle_bg_color": "#90989F",
    "time": {
        "Days": {
            "text": "Días",
            "color": "#40484F",
            "show": true
        },
        "Hours": {
            "text": "Horas",
            "color": "#40484F",
            "show": true
        },
        "Minutes": {
            "text": "Minutos",
            "color": "#40484F",
            "show": true
        },
        "Seconds": {
            "text": "Segundos",
            "color": "#40484F",
            "show": true
        }
    }
      });
     });
  
});


function reservar_restaurant(item_data){
  $.ajax({

        type:"POST",
        url:" https://hoteleshesperia.com.ve/acciones/acciones_restaurantes.php",
        data:item_data,
        contentType:false,
        processData:false,
        cache:false,
        success: function(response){
          if(response=="ok"){
            alert("Su solicitud fue enviada correctamente")
            location.reload();
          }else{
            alert("Ha ocurrido un error, vuelva a intentarlo");
            location.reload();
          }
        },
        error: function(response){
          alert("Ha ocurrido un error feo, vuelva a intentarlo");
         location.reload();
        }
      }); 
}

function isDate(txtDate){
  var currVal = txtDate;
  if(currVal == '')
    return false;
  //Declare Regex 
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
  var dtArray = currVal.match(rxDatePattern); // is format OK?
  if (dtArray == null)
     return false;
  //Checks for mm/dd/yyyy format.
    dtDay = dtArray[1];
    dtMonth= dtArray[3];
    dtYear = dtArray[5]; 

  if (dtMonth < 1 || dtMonth > 12)
      return false;
  else if (dtDay < 1 || dtDay> 31)
      return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2){
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }
  return true;
}
