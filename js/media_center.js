$(document).ready(function(){

	//alert("hola");
	$( ".dinamic-select" ).each(function( index ) {
	  //console.log( index + ": " + $( this ).text() );
	  var id = $(this).attr("id");
	  ocultarItems(id);
	});

	$('#dataTables-noticia').DataTable({
        "ordering": false,
        "info":     false
	});

	$(".dinamic-select").change(function(){
		var id = $(this).attr("id");

		var valor = $(this).val();
		var clase ="";
		if (id=="select-hotel-brochure") {
			clase+=".brochure-item";
		}else if(id=="select-hotel-img"){
			clase+=".foto-item";
		}else if(id=="select-hotel-promos"){
			clase+=".promos-item";
		}
		$(clase).hide();
		$(clase+".hh-"+valor+"").show();
	});

	$(".show-modal").click(function(e){
		e.preventDefault();
		var url = $(this).data("value");
		$("#show-modal-item").modal(function(){
			show:true
		});

		//$("#show-modal-item .modal-body").text("Cargando");
		$("#show-modal-item .modal-body").html("<img src='"+url+"'  class='imagepreview' style='width:100%'> ");
	});

});

function ocultarItems(id){
	var valor = $("#"+id).val();
	var clase="";
	if (id=="select-hotel-brochure") {
			clase+=".brochure-item";
		}else if(id=="select-hotel-img"){
			clase+=".foto-item";
		}else if(id=="select-hotel-promos"){
			clase+=".promos-item";
		}
		$(clase).hide();
		$(clase+".hh-"+valor+"").show();
}