function verifica_url(url){

  var host, path_info

  host='http://'+location.host;

  path_info=(location.pathname.indexOf('?') != -1) ? location.pathname.substring(0, location.pathname.indexOf('?')) :location.pathname;

  query_string=(location.search) ? ((location.search.indexOf('#') != -1) ? location.search.substring(1, location.search.indexOf('#')) :location.search.substring(1)) :'';

  largo=host.length+path_info.length+query_string.length;

  if ( largo > 350) {

       location.href=url;

  }

}



function Recuperar()

{ var str, ruta = document.RecuperaClaveUsuario, error="";

    if(ruta.username.value == "")

   { error += "Indicar su email de registro \n";}

  if(ruta.username.value != "") { str = ruta.username.value;

     if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

        error +="Formato de dirección de e-mail inválida\n"; }

  if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }

  var resultado = $.ajax({

  type: "POST",

  data: $("#RecuperaClaveUsuario").serialize(),

  url: 'https://hoteleshesperia.com.ve/include/recupera.php',

  dataType: 'text',async:false

 }).responseText;

 document.getElementById("recuperaClave").innerHTML = resultado;

}



function Regverificar() {

    var str, ruta=document.Registro, error="";

    if(ruta.email.value == "")

    { error += "Ingresar su eMail \n";}

    if(ruta.fechaNac.value == "")

    { error += "Ingresar su fecha de nacimiento \n";}

    if(ruta.cedula.value == "")

    { error += "Ingresar su cédula \n";}

    if(ruta.pregunta1.value == "" || ruta.pregunta2.value == "" || ruta.pregunta3.value == "")

    { error += "Seleccione las tres preguntas de seguridad \n";}

    if(ruta.respuesta1.value == "" || ruta.respuesta2.value == "" || ruta.respuesta3.value == "")

    { error += "Responda las tres preguntas de seguridad \n";}

    if(ruta.clave.value == "")

    { error += "Ingresar clave de acceso \n";}

    if(ruta.clave.value != ruta.rclave.value)

    { error += "Claves no coinciden \n";}

    if(ruta.rclave.value == "")

    { error += "Debe repetir la clave de acceso \n";}

    if(ruta.apellidos.value == "")

    { error += "Indicar su Apellido \n";}

    if(ruta.nombres.value == "")

    { error += "Indicar su Nombre \n";}

    if(ruta.telefono.value == "")

    { error += "Indicar su telefono\n";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida\n"; }

    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}

    var resultado=$.ajax({

        type:"POST",

        data:$("#Registro").serialize(),

        url:'https://hoteleshesperia.com.ve/include/registro_usuario.php',

        dataType:'text',async:false

    }).responseText;
    if (resultado.length==165) {
        ga('send', 'event', 'intangible', 'registro', ruta.email.value);
    };
    document.getElementById("AccionMensajeRegistro").innerHTML=resultado;

}







function GetUser() {

    var str, ruta=document.form1, error="";

    if(ruta.username.value == "")

    { error += "Debe ingresar su email \n";}

    if(ruta.clave.value == "")

    { error += "Debe ingresar clave de acceso \n";}

    if(ruta.username.value != ""){ str=ruta.username.value;

         if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i)){

         error += "Formato email invalido\n"; }}

    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }

    var respuesta=$.ajax({

        type:"POST", data:$("#form1").serialize(),

        url:'https://hoteleshesperia.com.ve/include/acceso.php',

        dataType:'text',async:false

    }).responseText;

    document.getElementById("myWatch").innerHTML=respuesta;

    if(respuesta=="Correcto!"){
        window.location = "https://hoteleshesperia.com.ve/perfil/mi-cuenta";
        ga('send', 'event', 'intangible', 'inicio-sesion', ruta.username.value);
    }

}

function Reservar(){

    var str,ruta=document.FormReservacion,error="";

    fEntrada = ruta.dpd1.value.split("/");

    fSalida = ruta.dpd2.value.split("/");

    dEntrada = new Date(parseInt(fEntrada[2], 10), parseInt(fEntrada[1]) - 1, parseInt(fEntrada[0]));

    dSalida = new Date(parseInt(fSalida[2], 10), parseInt(fSalida[1]) - 1, parseInt(fSalida[0]));

    if(ruta.hotel.value=="0")

    {error+="Debe Indicar el Hotel \n";}

    if(ruta.dpd1.value=="0" || ruta.dpd1.value=="")

    {error+="Debe Indicar fecha de Entrada \n";}

    if(ruta.dpd2.value=="0" || ruta.dpd2.value=="")

    {error+="Debe Indicar fecha de Salida \n";}

    if(dSalida < dEntrada)

    {error+="La fecha de Salida debe ser mayor que la fecha de Entrada\n";}

    if(ruta.adultos.value == "0")

    {error+="Indique cantidad de adultos\n";}

    if(ruta.ninos.value >= "1" && ruta.adultos.value == "0")

    {error+="Debe indicar por lo menos 1 adulto\n";}

    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}

}

function OtraReservar(){

    var str,ruta=document.FormOtraReservacion,error="";

    fEntrada = ruta.dpd1.value.split("/");

    fSalida = ruta.dpd2.value.split("/");

    dEntrada = new Date(parseInt(fEntrada[2], 10), parseInt(fEntrada[1]) - 1, parseInt(fEntrada[0]));

    dSalida = new Date(parseInt(fSalida[2], 10), parseInt(fSalida[1]) - 1, parseInt(fSalida[0]));

    if(ruta.hotel.value=="0")

    {error+="Debe Indicar el Hotel \n";}

    if(ruta.dpd1.value=="0" || ruta.dpd1.value=="")

    {error+="Debe Indicar fecha de Entrada \n";}

    if(ruta.dpd2.value=="0" || ruta.dpd2.value=="")

    {error+="Debe Indicar fecha de Salida \n";}

    if(dSalida < dEntrada)

    {error+="La fecha de Salida debe ser mayor que la fecha de Entrada\n";}

    if(ruta.adultos.value == "0")

    {error+="Indique cantidad de adultos\n";}

    if(ruta.ninos.value >= "1" && ruta.adultos.value == "0")

    {error+="Debe indicar por lo menos 1 adulto\n";}

    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}

}



function FormContacto() {

    var str, ruta=document.Contacto, error="";

    if(ruta.hotel.value == "" || ruta.hotel.value == 0)

    { error += "Debe indicar que Hotel Contactara \n";}

    if(ruta.nombre.value == "")

    { error += "Debe ingresar su Nombre \n";}

    if(ruta.phone.value == "")

    { error += "Debe ingresar su numero telefonico \n";}

    if(ruta.email.value == "")

    { error += "Debe ingresar su correo electronico \n";}

    if(ruta.email.value != ""){ str=ruta.email.value;

         if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i)){

         error += "Formato email invalido\n"; }}

    if(ruta.pais.value == "")

    { error += "Debe ingresar su pais \n";}

    if(ruta.mensaje.value == "")

    { error += "Debe escribir un mensaje \n";}

    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }

    var resultado=$.ajax({

        type:"POST", data:$("#Contacto").serialize(),

        url:'https://hoteleshesperia.com.ve/include/contacto.php',

        dataType:'text',async:false

    }).responseText;

    document.getElementById("RespuestaContacto").innerHTML=resultado;

}



function Reservando() {

    var str, ruta=document.FormReserva, error="";

    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }

    var respondiendo=$.ajax({

        type:"POST", data:$("#FormReserva").serialize(),

        url:'https://hoteleshesperia.com.ve/include/revisando_reserva.php',

        dataType:'text',async:false

    }).responseText;

    document.getElementById("Respuestareservar").innerHTML=respondiendo;

}



function Formsuscripcion() {

    var str, ruta=document.suscripcion, error="";

    if(ruta.email.value == "")

    { error += "Debe ingresar su correo electronico \n";}

    if(ruta.email.value != ""){ str=ruta.email.value;

         if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i)){

         error += "Formato email invalido\n"; }}

    if(error!=""){ alert("Lista de Errores encontrados:\n\n"+error); return false; }

    var resultado=$.ajax({

        type:"POST", data:$("#suscripcion").serialize(),

        url:'https://hoteleshesperia.com.ve/include/suscripcion.php',

        dataType:'text',async:false

    }).responseText;

    document.getElementById("Respuestasuscripcion").innerHTML=resultado;

}



function UpdateClave() {

    var str, ruta=document.MiClave, error="";

    if(ruta.clave_acceso.value == "")

    { error += "Ingresar clave de acceso \n";}

    if(ruta.clave_acceso.value != ruta.rclave.value)

    { error += "Claves no coinciden \n";}

    if(ruta.rclave.value == "")

    { error += "Debe repetir la clave de acceso \n";}

    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}

    var resultado=$.ajax({

        type:"POST",

        data:$("#MiClave").serialize(),

        url:'https://hoteleshesperia.com.ve/include/update_clave.php',

        dataType:'text',async:false

    }).responseText;

    document.getElementById("MiCLaveperfil").innerHTML=resultado;

}



function Panel() {



   

    var mensajeIncorrecto = '<div class="alert alert-danger" role="alert"><p>Ha ocurrido un error inesperado. no se puedo realizar los cambios o no cambio nada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p></div>';

    var mensajeValidacion = '<div class="alert alert-danger" role="alert"><p>No puede actualizar los datos de perfil debido a que no se respondieron correctamente a las preguntas de seguridad</p></div>'; 

    var str, ruta=document.MiPanel, error="";

    if(ruta.email.value == "")

    { error += "Ingresar su eMail \n";}

    if(ruta.apellidos.value == "")

    { error += "Indicar su Apellido \n";}

    if(ruta.nombres.value == "")

    { error += "Indicar su Nombre \n";}

    if(ruta.telefono.value == "")

    { error += "Indicar su telefono\n";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida\n"; }

    if(error!=""){alert("Lista de Errores encontrados:\n\n"+error);return false;}

    $.ajax({

        type:"POST",

        data:$("#MiPanel").serialize(),

        url:'https://hoteleshesperia.com.ve/include/update_perfil.php',

        dataType:'text',async:false,

        complete: function(){

            //location.reload();

        },

        success: function(response){

            switch($.trim(response)){

                case '0': 

                    location.reload();

                    break;

                case '1':

                    $("#Respperfil").html(mensajeIncorrecto);

                    break;

                case '2':

                    $("#Respperfil").html(mensajeValidacion);

                    break;

            }

            

        }

    });



    return;

}



(function(e){"use strict";var b=function(a,d,c){return 1===arguments.length?b.get(a):b.set(a,d,c)};b._document=document;b._navigator=navigator;b.defaults={path:"/"};b.get=function(a){b._cachedDocumentCookie!==b._document.cookie&&b._renewCache();return b._cache[a]};b.set=function(a,d,c){c=b._getExtendedOptions(c);c.expires=b._getExpiresDate(d===e?-1:c.expires);b._document.cookie=b._generateCookieString(a,d,c);return b};b.expire=function(a,d){return b.set(a,e,d)};b._getExtendedOptions=function(a){return{path:a&&a.path||b.defaults.path,domain:a&&a.domain||b.defaults.domain,expires:a&&a.expires||b.defaults.expires,secure:a&&a.secure!==e?a.secure:b.defaults.secure}};b._isValidDate=function(a){return"[object Date]"===Object.prototype.toString.call(a)&&!isNaN(a.getTime())};b._getExpiresDate=function(a,d){d=d||new Date;switch(typeof a){case"number":a=new Date(d.getTime()+1E3*a);break;case"string":a=new Date(a)}if(a&&!b._isValidDate(a))throw Error("`expires` parameter cannot be converted to a valid Date instance");return a};b._generateCookieString=function(a,b,c){a=a.replace(/[^#$&+\^`|]/g,encodeURIComponent);a=a.replace(/\(/g,"%28").replace(/\)/g,"%29");b=(b+"").replace(/[^!#$&-+\--:<-\[\]-~]/g,encodeURIComponent);c=c||{};a=a+"="+b+(c.path?";path="+c.path:"");a+=c.domain?";domain="+c.domain:"";a+=c.expires?";expires="+c.expires.toUTCString():"";return a+=c.secure?";secure":""};b._getCookieObjectFromString=function(a){var d={};a=a?a.split("; "):[];for(var c=0;c<a.length;c++){var f=b._getKeyValuePairFromCookieString(a[c]);d[f.key]===e&&(d[f.key]=f.value)}return d};b._getKeyValuePairFromCookieString=function(a){var b=a.indexOf("="),b=0>b?a.length:b;return{key:decodeURIComponent(a.substr(0,b)),value:decodeURIComponent(a.substr(b+1))}};b._renewCache=function(){b._cache=b._getCookieObjectFromString(b._document.cookie);b._cachedDocumentCookie=b._document.cookie};b._areEnabled=function(){var a="1"===b.set("js.js",1).get("js.js");b.expire("js.js");return a};b.enabled=b._areEnabled();"function"===typeof define&&define.amd?define(function(){return b}):"undefined"!==typeof exports?("undefined"!==typeof module&&module.exports&&(exports=module.exports=b),exports.Cookies=b):window.Cookies=b})();(function($,w){$.extend({jsMalditasCookies:function(opciones){var configuracion={cookie:"aceptocookies",classContenedorAviso:"contcookies",mensaje:"Este sitio, como la mayoria, usa cookies. Si sigues navegando entendemos que acepta la <a href=\"index.php?go=0/politicas/\">poli­tica de uso</a>.",mensajeAceptar:"Aceptar",esperaScroll:15000,expires:3600*24*365*2}

if(!Cookies.enabled){configuracion.mensaje="Este sitio usa Cookies y en tu navegador estan desactivadas. Acti­valas por favor.";}

jQuery.extend(configuracion,opciones);if(Cookies.get(configuracion.cookie)!="aceptadas"){setTimeout(function(){w.on("scroll",manejadorScroll);},configuracion.esperaScroll);function manejadorScroll(e){console.log("scroll:",w.scrollTop());cerrarAviso();}

function cerrarAviso(){contAviso.slideUp(1000);w.off("scroll",manejadorScroll);}

var contAviso=$("<div>").addClass(configuracion.classContenedorAviso).html(configuracion.mensaje+" <a href=\"#\" class=\"cookiesaceptar\">"+configuracion.mensajeAceptar+"</a>").appendTo("body");var enlace=contAviso.find("a.cookiesaceptar").on("click",function(e){e.preventDefault();cerrarAviso();Cookies.set(configuracion.cookie,'aceptadas',{expires:configuracion.expires});});}

return this;}});})(jQuery,jQuery(window));$(function(){jQuery.jsMalditasCookies();});



function trabajo() {

    var str, ruta=document.FormTrabajo, error="";

    //var $form = $(this);

    var token = 0;

    if(ruta.nombre.value == "")

    { error += "Ingresar su nombre y apellidos \n";}
    
    if(ruta.formacion.value == "-1")

    { error += "Debe seleccionar su formación academica \n";}
    
    if(ruta.hotelpreferencia.value == "-1")

    { error += "Debe seleccionar su formación academica \n";}

    if(ruta.ciudad.value == "-1")

    { error += "Debe seleccionar su ciudad \n";}
    
    if(ruta.telefono.value == "")

    { error += "Indicar su telefono movil \n";}

    if(ruta.direccion.value == "")

    { error += "Indicar su Direccion \n";}

    if(ruta.email.value == "")

    { error += "Ingresar su eMail \n";}

    if(ruta.email.value != ""){

        str=ruta.email.value;

        if(!str.match(/^[\w]{1}[\w\.\-_]*@[\w]{1}[\w\-_\.]*\.[\w]{2,6}$/i))

            error +="Formato de dirección de e-mail inválida\n"; }

    if(ruta.archivo.value == "")

    { error += "Debe enviar su curriculo\n";}

   if(error!=""){ 
       alert("Lista de Errores encontrados:\n\n"+error); 
       return false;
   }

 $('#FormTrabajo').submit(function (e) {

    var data = new FormData(this);

    $.ajax({

        url:'https://hoteleshesperia.com.ve/include/registro_trabajo.php',

        data:data,

        processData:false,

        contentType:false,

        type:'POST',

        success:function (resultado) {

        document.getElementById("Respuestrabajo").innerHTML = resultado;

    e.preventDefault();

        }

    });

});

}



function VerReserva(idReservacion)

{  var resultado = $.ajax({

  type: "POST",

  data: "idReservacion="+idReservacion,

  url: 'https://hoteleshesperia.com.ve/include/ver_reservacion.php',

  dataType: 'text',async:false

 }).responseText;

 document.getElementById("RespuestaReserva").innerHTML = resultado;

}



function numeros(e){

    key = e.keyCode || e.which;

    tecla = String.fromCharCode(key).toLowerCase();

    letras = "0123456789";

    especiales = [8,37,39,46];

 

    tecla_especial = false

    for(var i in especiales){

     if(key == especiales[i]){

         tecla_especial = true;

         break;

            } 

        }

 

    if(letras.indexOf(tecla)==-1 && !tecla_especial)

        return false;

}



function soloLetras(e){

       key = e.keyCode || e.which;

       tecla = String.fromCharCode(key).toLowerCase();

       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";

       especiales = "8-37-39-46";



       tecla_especial = false

       for(var i in especiales){

            if(key == especiales[i]){

                tecla_especial = true;

                break;

            }

        }



        if(letras.indexOf(tecla)==-1 && !tecla_especial){

            return false;

        }

    }



$(function () {

  $('[data-toggle="tooltip"]').tooltip();

})



$(function () {

    $("#dpd1").datepicker({

        minDate: new Date(),

        changeMonth: true,

        dateFormat: 'dd-mm-yyyy',

        changeYear: true

    });

});



$(function () {

    $("#dpd2").datepicker({

        minDate: new Date(),

        dateFormat: 'dd-mm-yyyy',

        changeMonth: true,

        changeYear: true

    });

});



    var nowTemp = new Date();

    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#dpd1').datepicker({

      onRender: function(date) {

        return date.valueOf() < now.valueOf() ? 'disabled' : '';

      }

    }).on('changeDate', function(ev) {

      if (ev.date.valueOf() > checkout.date.valueOf()) {

        var newDate = new Date(ev.date)

        newDate.setDate(newDate.getDate() + 1);

        checkout.setValue(newDate);

      }

      checkin.hide();

      $('#dpd2')[0].focus();

    }).data('datepicker');

    var checkout = $('#dpd2').datepicker({

      onRender: function(date) {

        return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';

      }

    }).on('changeDate', function(ev) {

      checkout.hide();

    }).data('datepicker');



