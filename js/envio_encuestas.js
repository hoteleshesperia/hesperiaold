$(document).ready(function(){

  var formdataIndex = new FormData($("#form-env-indv")[0]);

  formdataIndex.append("accion", "cargar_areas");
  formdataIndex.append("id_hotel", $("#id_hotel").val());
  actualizaLista(formdataIndex);

  if ($("#id_hotel").val()==1) {
    $("#formato").val("hu");
  }else{
    if ($("#id_hotel").val()>1 &&  $("#id_hotel").val()<5) {
      $("#formato").val("hp");
    }else{
      $("#formato").val("mix");
    }
    
  }
  
	$("#env-mail-masivo").click(function(e){
		e.preventDefault();
		var correos = $("#direcciones").val().trim().split(";");
		var formData = new FormData();
		//alert("sirvo"+$("#direcciones").val());
    var cant =0;
		for (var i =0; i< correos.length; i++) {
			
			if (isEmail(correos[i].trim())) {
				//alert("es mail: "+correos[i]);
        cant++;
			   formData.append("email-"+i, correos[i]);	
			}

		};

    if (cant>0) {
      formData.append("formato", $("#formato").val());
      formData.append("id_hotel", $("#id_hotel").val());
      $(this).prop("disabled", "disabled");
      $(this).text("enviando");
      enviarMails(formData);  
    }else{
      alert("no se han encontrado emails validos para enviar")
    }
		

	});
});

$("#id_hotel").change(function(){
  var formdata = new FormData($("#form-env-indv")[0]);

  formdata.append("accion", "cargar_areas");

  actualizaLista(formdata);
});

$("#env-mail-individual").click(function(e){
  e.preventDefault();

  var formdata = new FormData($("#form-env-indv")[0]);
   formdata.append("id_hotel", $("#id_hotel").val());
   $(this).prop("disabled", "disabled");
   $(this).text("enviando");
  enviarMailInd(formdata);
  
});

$("#formato").change(function(){
  var valor = $(this).val();
  if (valor=="mix") {
    $(".hu, .hp").show();
    $("#id_hotel").val(-1);
  }else if(valor=="hp"){

    $(".hp").show();
    $(".hu").hide();
    $("#id_hotel").val(-1);
  }else if(valor=="hu"){
    $(".hu").show();
    $(".hp").hide();
    $("#id_hotel").val(-1);
  }
});

function enviarMails(formdata){
	$.ajax({
        type: "POST",
        url: "https://hoteleshesperia.com.ve/acciones/acciones_encuestas_mail.php",
        data: formdata,
        contentType:false,
        processData:false,
        cache:false,           
        success: function(response){
        if(response=="ok"){
          alert("Procesado Correctamente");
        //  location.reload();
               
        }else{
          alert(response);
          console.log(response);
          location.reload();
          }       
        },
        error: function(res){
          console.log(res);
        }
          
    });
}

function enviarMailInd(formdata){
  $.ajax({
        type: "POST",
        url: "https://hoteleshesperia.com.ve/acciones/enviar_email_individual.php",
        data: formdata,
        contentType:false,
        processData:false,
        cache:false,           
        success: function(response){
        if(response=="ok"){
          alert("Procesado Correctamente");
          location.reload();
               
        }else{
          alert("Ha ocurrido un error, intente nuevamente"+response);
         
          }       
        }
          
    });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function actualizaLista(formdata){
  $.ajax({
        type: "POST",
        url: "https://hoteleshesperia.com.ve/acciones/acciones_form_encuestas.php",
        data: formdata,
        contentType:false,
        processData:false,
        cache:false,           
        success: function(response){
          $("#tipo_encuesta").html(response);
         // alert(response);
        }
          
    });
}
