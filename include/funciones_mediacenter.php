<?php

function menuMediaCenter(){

	$menu = "
	<div class='list-group'>

		<a href='#noticias-content' data-toggle='tab' role='tab' class='list-group-item'>
            <i class='fa fa-newspaper-o'></i> Noticias
        </a>
        
        <a href='#brochures-pdf-content' class='list-group-item' role='tab' data-toggle='tab'>
		    <i class='fa fa-file-pdf-o'></i> Brochures / PDF
		</a>
  
        <a href='#fotos-content' data-toggle='tab' role='tab' class='list-group-item'>
        	<i class='fa fa-file-image-o'></i> Fotos
        </a>

        <a href='#videos-content' data-toggle='tab' role='tab' class='list-group-item'>
        	<i class='fa fa-video-camera'></i> Videos
        </a>

        <a href='#promos-content' data-toggle='tab' role='tab' class='list-group-item'>
        	<i class='fa fa-gift'></i> Promociones
        </a>

        <a href='#marca-content' data-toggle='tab' role='tab' class='list-group-item'>
        	<i class='fa fa-building-o'></i> Manual de Marca
        </a>
          
    </div>";

    return $menu;

} 
	
	function cargar_imagenes_mediacenter(){

		echo "
		<div class='col-md-3'>
	    	<div class='thumbnail'>
	            <img src='http://placehold.it/750x450'>
	            	<div class='caption'>
	                	<a href='#'>Ver</a> - <a href='#'>Descargar</a> 
	                </div>
	        </div>
        </div>
        <div class='col-md-3'>
	    	<div class='thumbnail'>
	            <img src='http://placehold.it/750x450'>
	            	<div class='caption'>
	                	<a href='#'>Ver</a> - <a href='#'>Descargar</a> 
	                </div>
	        </div>
        </div>

        <div class='col-md-3'>
	    	<div class='thumbnail'>
	            <img src='http://placehold.it/750x450'>
	            	<div class='caption'>
	                	<a href='#'>Ver</a> - <a href='#'>Descargar</a> 
	                </div>
	        </div>
        </div>

        <div class='col-md-3'>
	    	<div class='thumbnail'>
	            <img src='http://placehold.it/750x450'>
	            	<div class='caption'>
	                	<a href='#'>Ver</a> - <a href='#'>Descargar</a> 
	                </div>
	        </div>
        </div>

        <div class='col-md-3'>
	    	<div class='thumbnail'>
	            <img src='http://placehold.it/750x450'>
	            	<div class='caption'>
	                	<a href='#'>Ver</a> - <a href='#'>Descargar</a> 
	                </div>
	        </div>
        </div>

        <div class='col-md-3'>
	    	<div class='thumbnail'>
	            <img src='http://placehold.it/750x450'>
	            	<div class='caption'>
	                	<a href='#'>Ver</a> - <a href='#'>Descargar</a> 
	                </div>
	        </div>
        </div>
        ";
	}

	function cargar_brochures_prf(){
		echo "
		<div class='row'>
            <div class='col-md-2'>
                <a href='portfolio-item.html'>
                    <img class='img-responsive' src='https://hoteleshesperia.com.ve/img/pdf.png' alt=''>
                </a>
            </div>
            <div class='col-md-10'>
                <h3>Project Two</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, odit velit cumque vero doloremque repellendus distinctio maiores rem expedita a nam vitae modi quidem similique ducimus! Velit, esse totam tempore.</p>
                <a class='btn btn-primary' href='portfolio-item.html'>Descargar</a>
            </div>
        </div>
        <hr>
      	";
	}

	function cargar_videos(){
		
		echo "
		<div class='col-sm-4'>
   
			<div class='embed-responsive embed-responsive-16by9'>
			    <iframe class='embed-responsive-item' src='//www.youtube.com/embed/ePbKGoIGAXY'></iframe>
			</div>
		</div>
		
		<div class='col-sm-4'>
   
			<div class='embed-responsive embed-responsive-16by9'>
			    <iframe class='embed-responsive-item' src='//www.youtube.com/embed/ePbKGoIGAXY'></iframe>
			</div>
		</div>

		<div class='col-sm-4'>
		
			<div class='embed-responsive embed-responsive-16by9'>
			    <iframe class='embed-responsive-item' src='//www.youtube.com/embed/ePbKGoIGAXY'></iframe>
			</div>
		</div>
		";
		
	}

	function cargar_noticias(){
		echo("
			<div class='row'>  
	            <div class='col-md-5'>
	                <a href='blog-post.html'>
	                    <img class='img-responsive img-hover' src='http://placehold.it/600x300' alt=''>
	                </a>
	            </div>
	            <div class='col-md-6'>
	                <h3>
	                    <a href='blog-post.html'>Blog Post Title</a>
	                </h3>
	                
		            <p>June 17, 2014</p>
		           	<p>subtitulo</p>
	                <a class='btn btn-primary' href='blog-post.html'>Read More <i class='fa fa-angle-right'></i></a>
	            </div>
        	</div>
			");
	}



 ?>