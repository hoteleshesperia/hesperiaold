<?php

function FORM_SELECT_HOTEL_INFOGRAL($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Seleccione el Hotel para editar su información</h3>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="home.php?go=VerInfoGeneral" onsubmit="return InfoGralHotelSelectValida(); return document.MM_returnValue" role="form" method="post" name="FormInfoGralHotelSelectValida" id="FormInfoGralHotelSelectValida">
                            <div class="form-group">
                                <label for="nombre_hotel"></label>
                                <select class="form-control" name="id_hotel" id="id_hotel">
                                    <option value="0" selected>Elija Hotel</option>';
                            $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC");
                            $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
                            while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
                                echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
                            }
                            echo'</select>
                            </div>
                            <div class="box-footer">
                                <div class="col-lg-6 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                        Enviar
                                    </button>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="col-lg-6 pull-left">
                                    <p class="help-block">
                                        Si desea agregar un nuevo hotel, haga clic <a href="home.php?go=AgregaHotelNuevo" data-toggle="tooltip" data-placement="bottom" title="Agregar nuevo hotel">aquí</a>
                                    </p>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

# Informacion de Hotel general (Solo admin principal)
function INFO_GENERAL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
if (isset($_REQUEST["id_hotel"])){
    $id = $_REQUEST["id_hotel"];
}else{ $id = $_SESSION["id_hotel"]; }
    $sql = sprintf("SELECT * FROM hesperia_settings WHERE id = '%s'",
            mysqli_real_escape_string($mysqli,$id));
    $resultSet = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rows = mysqli_fetch_array($resultSet,MYSQLI_ASSOC);
echo '
<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Informacion General</a></li>
            <li><a href="#tab_2" data-toggle="tab">Sobre el Hotel</a></li>
            <li><a href="home.php?go=AgregaHotelNuevo">Agregar un nuevo Hotel</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Informacion General del hotel</h3>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return FormSettings(); return document.MM_returnValue" name="hesperia_settings" id="hesperia_settings">
                            <input type="hidden" name="id" id="id" value="'.$rows["id"].'"/>
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="razon_social">Razon Social:</label>
                                            <input type="text" class="form-control" name="razon_social" id="razon_social" value="'.$rows["razon_social"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_sitio">Nombre Sitio</label>
                                            <input type="text" class="form-control" name="nombre_sitio" id="nombre_sitio" value="'.$rows["nombre_sitio"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="rif">Rif</label>
                                            <input type="text" class="form-control" name="rif" id="rif" value="'.$rows["rif"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="dominio">Dominio <small class="text-danger"> no cambiar a menos que sepa lo que hace</small></label>
                                            <input type="text" class="form-control" name="dominio" id="dominio" value="'.$rows["dominio"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion">Direccion</label>
                                            <textarea  class="form-control" name="direccion" id="direccion">'.utf8_encode($rows["direccion"]).'</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion</label>
<textarea class="form-control" name="descripcion" id="summernote1">'.utf8_encode($rows["descripcion"]).'</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ciudad">Ciudad</label>
                    <input type="text" class="form-control" name="ciudad" id="ciudad" value="'.$rows["ciudad"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="estado">Estado</label>
<input type="text" class="form-control" name="estado" id="estado" value="'.$rows["estado"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="pais">Pais</label>
                                            <input type="text" class="form-control" name="pais" id="pais" value="'.$rows["pais"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="latitud">Latitud <small>Usando <a href="https://www.google.com/maps"  data-toggle="tooltip" data-placement="bottom" title="Buscar informacion en Google Maps" target="_blank">google.com/maps</a></small></label>
                                        <input type="text" class="form-control" name="latitud" id="latitud" value="'.$rows["latitud"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="longitud">Longitud <small>Usando <a href="https://www.google.com/maps"  data-toggle="tooltip" data-placement="bottom" title="Buscar informacion en Google Maps" target="_blank">google.com/maps</a></small></label>
                                        <input type="text" class="form-control" name="longitud" id="longitud" value="'.$rows["longitud"].'">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telefonos">Telefonos</label>
                                            <input type="text" class="form-control" name="telefonos" id="telefonos" value="'.$rows["telefonos"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="telf_reserva">Telefono de reserva</label>
                                            <input type="text" class="form-control" name="telf_reserva" id="telf_reserva" value="'.$rows["telf_reserva"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="check_in">Check in</label>
                                            <input type="text" class="form-control" name="check_in" id="check_in" value="'.$rows["check_in"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="check_out">Check out</label>
                                            <input type="text" class="form-control" name="check_out" id="check_out" value="'.$rows["check_out"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="postal">Zona Postal <small>Usando <a href="http://www.codigopostalde.com.ve/" data-toggle="tooltip" data-placement="bottom" title="Buscar informacion de Codigos Postales Venezuela" target="_blank">codigopostalde.com.ve</a></small> </label>
                                            <input type="text" class="form-control" name="postal" id="postal" value="'.$rows["postal"].'">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Redes Sociales
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="twitter">Twitter <small>sin @</small></label>
                                                <input type="text" class="form-control" name="twitter" id="twitter" placeholder="Twitter" value="'.$rows["twitter"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook">Facebook</label>
<input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook" value="'.$rows["facebook"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="gplus">Gplus</label>
                                            <input type="text" class="form-control" name="gplus" id="gplus" placeholder="Gplus" value="'.$rows["gplus"].'">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="youtube">Youtube</label>
                                            <input type="text" class="form-control" name="youtube" id="youtube" placeholder="Youtube" value="'.$rows["youtube"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="instagram">Instagram</label>
                                            <input type="text" class="form-control" name="instagram" id="instagram" placeholder="Instagram" value="'.$rows["instagram"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="pinterest">Pinterest</label>
<input type="text" class="form-control" name="pinterest" id="pinterest" placeholder="Pinterest" value="'.$rows["pinterest"].'">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="linkedin">Linkedin</label>
                                            <input type="text" class="form-control" name="linkedin" id="linkedin" placeholder="Linkedin" value="'.$rows["linkedin"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="correo_envio">Correo envio <small>por ejemplo: info@SUDOMINIO</small></label>
                                            <input type="text" class="form-control" name="correo_envio" id="correo_envio"  value="'.$rows["correo_envio"].'">
                                    </div>
                                    <div class="form-group">
                                        <label for="correo_contacto">Correo Contacto <small>por ejemplo: info@SUDOMINIO</small></label>
                                        <input type="text" class="form-control" name="correo_contacto" id="correo_contacto"  value="'.$rows["correo_contacto"].'">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Información relacionada con el SEO
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="keywords">Keywords <small>Palabras claves separadas por comas(,) sin espacios</small></label>
                                        <textarea class="form-control" name="keywords" id="keywords" >'.utf8_encode($rows["keywords"]).'</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Descripción vista en buscadores <small>Una breve descripción del hotel, ofertas etc.</small></label>
                                        <textarea class="form-control" name="description" id="description">'.utf8_encode($rows["description"]).'</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Caracteristicas del Hotel. Indique si el hotel ofrece los siguientes servicios
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group fancy-select">
                                        <label for="salones_evento">Salones Evento</label>
                                        <select class="form-control" name="salones_evento" id="salones_evento">';
                                            if ($rows["salones_evento"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="escapadas">Escapadas</label>
                                        <select class="form-control" name="escapadas" id="escapadas">';
                                            if ($rows["escapadas"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="experiencias">Experiencias</label>
                                        <select class="form-control" name="experiencias" id="experiencias">';
                                            if ($rows["experiencias"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="mascotas">Permite Mascotas</label>
                                        <select class="form-control" name="mascotas" id="mascotas">';
                                            if ($rows["mascotas"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="wifi">Conexión Wifi</label>
                                        <select class="form-control" name="wifi" id="wifi">';
                                            if ($rows["wifi"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="piscina">Piscina</label>
                                        <select class="form-control" name="piscina" id="piscina">';
                                            if ($rows["piscina"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="estacionamiento">Estacionamiento</label>
                                        <select class="form-control" name="estacionamiento" id="estacionamiento">';
                                            if ($rows["estacionamiento"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="botones">Botones</label>
                                        <select class="form-control" name="botones" id="botones">';
                                            if ($rows["botones"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="gimnasio">Gimnasio</label>
                                        <select class="form-control" name="gimnasio" id="gimnasio">';
                                            if ($rows["gimnasio"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="spa">Spa</label>
                                        <select class="form-control" name="spa" id="spa">';
                                            if ($rows["spa"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="helipuerto">Helipuerto</label>
                                        <select class="form-control" name="helipuerto" id="helipuerto">';
                                            if ($rows["helipuerto"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="accesibilidad">Accesibilidad</label>
                                        <select class="form-control" name="accesibilidad" id="accesibilidad">';
                                            if ($rows["accesibilidad"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nana">Nana / Niñera</label>
                                        <select class="form-control" name="nana" id="nana">';
                                            if ($rows["nana"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="transporte">Transporte</label>
                                        <select class="form-control" name="transporte">';
                                            if ($rows["transporte"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="vigilancia_privada">Vigilancia privada</label>
                                        <select class="form-control" name="vigilancia_privada" id="vigilancia_privada">';
                                            if ($rows["vigilancia_privada"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="cafetin">Cafetin</label>
                                        <select class="form-control" name="cafetin" id="cafetin">';
                                            if ($rows["cafetin"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="restaurant">Restaurant</label>
                                        <select class="form-control" name="restaurant" id="restaurant">';
                                            if ($rows["restaurant"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="bar">Bar</label>
                                        <select class="form-control" name="bar" id="bar">';
                                            if ($rows["bar"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="discotheca">Discoteca</label>
                                        <select class="form-control" name="discotheca" id="discotheca">';
                                            if ($rows["bar"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="golf">Cancha Golf</label>
                                        <select class="form-control" name="golf" id="golf">';
                                            if ($rows["golf"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="tenis">Cancha Tenis</label>
                                        <select class="form-control" name="tenis" id="tenis">';
                                            if ($rows["tenis"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="basket">Cancha Basket</label>
                                        <select class="form-control" name="basket" id="basket">';
                                            if ($rows["basket"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tienda">Tienda</label>
                                        <select class="form-control" name="tienda" id="tienda">';
                                            if ($rows["tienda"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cant_habitaciones">Cantidad habit.</label>
                                            <input type="text" class="form-control" name="cant_habitaciones" id="cant_habitaciones" value="'.$rows["cant_habitaciones"].'">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="parque">Parque</label>
                                        <select class="form-control" name="parque" id="parque">';
                                            if ($rows["parque"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="terraza">Terraza</label>
                                        <select class="form-control" name="terraza" id="terraza">';
                                            if ($rows["terraza"] == 1)
                                            {  echo '<option value="1" selected>Si</option>
                                                        <option value="0">No</option>';
                                             }
                                            else
                                            {  echo '<option value="0" selected>No</option>
                                                        <option value="1">Si</option>';
                                             }
                                    echo '
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Widgets de TripAdvisor
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tripadvisor">Tripadvisor</label>
<textarea class="form-control" name="tripadvisor" id="tripadvisor" placeholder="Tripadvisor"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tripadvisor_premio">Tripadvisor premio</label>
                                            <textarea class="form-control" name="tripadvisor_premio" id="tripadvisor_premio" placeholder="Tripadvisor_premio"></textarea>
                                    </div>
                                </div>
                            </div>
                                <div class="box-footer">
                                <div id="RespSetting"></div>
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">Cambiar Datos</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab_2">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nosotros</h3>
                        </div>
                        <form role="form">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">
                                        Informacion sobre "Nosotros"
                                    </label>
                                    <textarea id="summernote1"  class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen</label>
                                    <input type="file" id="exampleInputFile">
                                    <p class="help-block">La imagen debe ser de XXX px de alto y XXXpx de ancho</p>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Enviar</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6">
                        <div class="box-header with-border">
                            <h3 class="box-title">Imagen destacada</h3>
                        </div>
                        <form role="form">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen</label>
                                    <input type="file" id="exampleInputFile">
                                    <p class="help-block">La imagen debe ser de XXX px de alto y XXXpx de ancho</p>
                                </div>
                                <div class="box-header with-border">
                                    <h3 class="box-title">Imagen Actual</h3>
                                </div>
                                <img src="http://placehold.it/650x250&text=5" class="img-responsive img-rounded slider-p">
                                <a href="#0" class="boton-del">Eliminar</a>
                                <p class="help-block">Tenga en cuenta que al eliminar la imagen debe subir una para su reemplazo, de no ser asi no se va a mostrar ninguna imagen en este apartado</p>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function AGREGAR_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
echo'
<section class="content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Distancias relativas al hotel</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
<form role="form" enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return FormSettings(); return document.MM_returnValue" name="hesperia_settings" id="hesperia_settings">
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="razon_social">Razon Social:</label>
                                            <input type="text" class="form-control" name="razon_social" id="razon_social">
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre_sitio">Nombre del Hotel</label>
                                            <input type="text" class="form-control" name="nombre_sitio" id="nombre_sitio">
                                    </div>
                                    <div class="form-group">
                                        <label for="rif">Rif</label>
                                            <input type="text" class="form-control" name="rif" id="rif">
                                    </div>
                                    <div class="form-group">
                                        <label for="dominio">Dominio <small class="text-danger"> no cambiar a menos que sepa lo que hace</small></label>
                                            <input type="text" class="form-control" name="dominio" id="dominio">
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion">Direccion</label>
                                            <textarea  class="form-control" name="direccion" id="direccion"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion</label>
<textarea class="form-control" name="descripcion" id="summernote1"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ciudad">Ciudad</label>
                    <input type="text" class="form-control" name="ciudad" id="ciudad">
                                    </div>
                                    <div class="form-group">
                                        <label for="estado">Estado</label>
<input type="text" class="form-control" name="estado" id="estado">
                                    </div>
                                    <div class="form-group">
                                        <label for="pais">Pais</label>
                                            <input type="text" class="form-control" name="pais" id="pais">
                                    </div>
                                    <div class="form-group">
                                        <label for="latitud">Latitud <small>Usando <a href="https://www.google.com/maps"  data-toggle="tooltip" data-placement="bottom" title="Buscar informacion en Google Maps" target="_blank">google.com/maps</a></small></label>
                                        <input type="text" class="form-control" name="latitud" id="latitud">
                                    </div>
                                    <div class="form-group">
                                        <label for="longitud">Longitud <small>Usando <a href="https://www.google.com/maps"  data-toggle="tooltip" data-placement="bottom" title="Buscar informacion en Google Maps" target="_blank">google.com/maps</a></small></label>
                                        <input type="text" class="form-control" name="longitud" id="longitud">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telefonos">Telefonos</label>
                                            <input type="text" class="form-control" name="telefonos" id="telefonos">
                                    </div>
                                    <div class="form-group">
                                        <label for="telf_reserva">Telefono de reserva</label>
                                            <input type="text" class="form-control" name="telf_reserva" id="telf_reserva">
                                    </div>
                                    <div class="form-group">
                                        <label for="check_in">Check in</label>
                                            <input type="text" class="form-control" name="check_in" id="check_in">
                                    </div>
                                    <div class="form-group">
                                        <label for="check_out">Check out</label>
                                            <input type="text" class="form-control" name="check_out" id="check_out">
                                    </div>
                                    <div class="form-group">
                                        <label for="postal">Zona Postal <small>Usando <a href="http://www.codigopostalde.com.ve/" data-toggle="tooltip" data-placement="bottom" title="Buscar informacion de Codigos Postales Venezuela" target="_blank">codigopostalde.com.ve</a></small> </label>
                                            <input type="text" class="form-control" name="postal" id="postal">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Redes Sociales
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="twitter">Twitter <small>sin @</small></label>
                                                <input type="text" class="form-control" name="twitter" id="twitter" placeholder="Twitter">
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook">Facebook</label>
<input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook">
                                    </div>
                                    <div class="form-group">
                                        <label for="gplus">Google plus</label>
                                            <input type="text" class="form-control" name="gplus" id="gplus" placeholder="Google plus"">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="youtube">Youtube</label>
                                            <input type="text" class="form-control" name="youtube" id="youtube" placeholder="Youtube"">
                                    </div>
                                    <div class="form-group">
                                        <label for="instagram">Instagram</label>
                                            <input type="text" class="form-control" name="instagram" id="instagram" placeholder="Instagram">
                                    </div>
                                    <div class="form-group">
                                        <label for="pinterest">Pinterest</label>
<input type="text" class="form-control" name="pinterest" id="pinterest" placeholder="Pinterest">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="linkedin">Linkedin</label>
                                            <input type="text" class="form-control" name="linkedin" id="linkedin" placeholder="Linkedin">
                                    </div>
                                    <div class="form-group">
                                        <label for="correo_envio">Correo envio <small>por ejemplo: info@SUDOMINIO</small></label>
                                            <input type="text" class="form-control" name="correo_envio" id="correo_envio">
                                    </div>
                                    <div class="form-group">
                                        <label for="correo_contacto">Correo Contacto <small>por ejemplo: info@SUDOMINIO</small></label>
                                        <input type="text" class="form-control" name="correo_contacto" id="correo_contacto">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Información relacionada con el SEO
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="keywords">Keywords <small>Palabras claves separadas por comas(,) sin espacios</small></label>
                                        <textarea class="form-control" name="keywords" id="keywords" ></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Descripción vista en buscadores <small>Una breve descripción del hotel, ofertas etc.</small></label>
                                        <textarea class="form-control" name="description" id="description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Caracteristicas del Hotel. Indique si el hotel ofrece los siguientes servicios
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group fancy-select">
                                        <label for="salones_evento">Salones Evento</label>
                                        <select class="form-control" name="salones_evento" id="salones_evento">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="escapadas">Escapadas</label>
                                        <select class="form-control" name="escapadas" id="escapadas">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="experiencias">Experiencias</label>
                                        <select class="form-control" name="experiencias" id="experiencias">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="mascotas">Permite Mascotas</label>
                                        <select class="form-control" name="mascotas" id="mascotas">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="wifi">Conexión Wifi</label>
                                        <select class="form-control" name="wifi" id="wifi">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="piscina">Piscina</label>
                                        <select class="form-control" name="piscina" id="piscina">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="estacionamiento">Estacionamiento</label>
                                        <select class="form-control" name="estacionamiento" id="estacionamiento">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="botones">Botones</label>
                                        <select class="form-control" name="botones" id="botones">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="gimnasio">Gimnasio</label>
                                        <select class="form-control" name="gimnasio" id="gimnasio">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="spa">Spa</label>
                                        <select class="form-control" name="spa" id="spa">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="helipuerto">Helipuerto</label>
                                        <select class="form-control" name="helipuerto" id="helipuerto">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="accesibilidad">Accesibilidad</label>
                                        <select class="form-control" name="accesibilidad" id="accesibilidad">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nana">Nana / Niñera</label>
                                        <select class="form-control" name="nana" id="nana">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="transporte">Transporte</label>
                                        <select class="form-control" name="transporte">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="vigilancia_privada">Vigilancia privada</label>
                                        <select class="form-control" name="vigilancia_privada" id="vigilancia_privada">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="cafetin">Cafetin</label>
                                        <select class="form-control" name="cafetin" id="cafetin">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="restaurant">Restaurant</label>
                                        <select class="form-control" name="restaurant" id="restaurant">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="bar">Bar</label>
                                        <select class="form-control" name="bar" id="bar">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="discotheca">Discoteca</label>
                                        <select class="form-control" name="discotheca" id="discotheca">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="golf">Cancha Golf</label>
                                        <select class="form-control" name="golf" id="golf">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="tenis">Cancha Tenis</label>
                                        <select class="form-control" name="tenis" id="tenis">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="basket">Cancha Basket</label>
                                        <select class="form-control" name="basket" id="basket">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tienda">Tienda</label>
                                        <select class="form-control" name="tienda" id="tienda">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cant_habitaciones">Cantidad habit.</label>
                                            <input type="text" class="form-control" name="cant_habitaciones" id="cant_habitaciones">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="parque">Parque</label>
                                        <select class="form-control" name="parque" id="parque">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="terraza">Terraza</label>
                                        <select class="form-control" name="terraza" id="terraza">
                                            <option value="0" selected>No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Widgets de TripAdvisor
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tripadvisor">Tripadvisor</label>
<textarea class="form-control" name="tripadvisor" id="tripadvisor" placeholder="Tripadvisor"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tripadvisor_premio">Tripadvisor premio</label>
                                            <textarea class="form-control" name="tripadvisor_premio" id="tripadvisor_premio" placeholder="Tripadvisor_premio"></textarea>
                                    </div>
                                </div>
                            </div>
                                <div class="box-footer">
                                <div id="RespSetting"></div>
                                    <div class="col-lg-7">
                                        <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                        </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">Cambiar Datos</button>
                                    </div>
                                </div>
                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>';


    return;
}
