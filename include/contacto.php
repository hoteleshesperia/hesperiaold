<?php
/* -------------------------------------------------------
Script  bajo los t&eacute;rminos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor: ViserProject.com
-------------------------------------------------------- */
session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
    header("location:../error.html");
    die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
$ahora = time();
$email = strip_tags(strtolower(trim($_POST['email'])));
$email = filter_var($email,FILTER_SANITIZE_EMAIL);
$nombre = strip_tags(ucwords(trim($_POST['nombre'])));
$telefono = strip_tags(trim($_POST['phone']));
$contacto = $_POST['correo_contacto'];
$dominio  = $_POST['dominio'];
$razon_social = 'Hoteles Hesperia Venezuela';
$hotel = $_POST['hotel'];
$pais = $_POST['pais'];
$mensaje = strip_tags(trim($_POST['mensaje']));
graba_LOG("Recepcion de mensaje por contacto: $email",$email,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
$sql = sprintf("INSERT INTO hesperia_contacto VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '', '%s')",
                            mysqli_real_escape_string($mysqli,$nombre),
                            mysqli_real_escape_string($mysqli,$email),
                            mysqli_real_escape_string($mysqli,$telefono),
                            mysqli_real_escape_string($mysqli,$mensaje),
                            mysqli_real_escape_string($mysqli,$ahora),
                            mysqli_real_escape_string($mysqli,$ahora),
                            mysqli_real_escape_string($mysqli,0));
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli) >= 1)
   { echo '<br/><div class="alert alert-success" role="alert">
                <p>Hemos recibido su solicitud mensaje, en las próximas horas le estaremos contactando. Gracias por enviarnos sus consultas y/o sugerencias.</p>
                </div>';
                    $asunto  = "Contacto desde formulario de $dominio";
                    $mensaje = "Saludos: Sr(a): $nombre<br>
                    Se ha registrado su solicitud de infomacion desde $dominio.<br>\r\n
                    Para el Hotel $hotel.<br>\r\n
                    Con el siguiente Mensaje:<br>\r\n
                    ----------------------------------------------------------------------------------------<br>\r\n
                    $mensaje <br>\r\n
                    Cualquier duda contactenos al siguiente email: $contacto<br>
                    ----------------------------------------------------------------------------------------<br>\r\n
                    Este mensaje es privado y confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.\r\n<br>
                    ----------------------------------------------------------------------------------------<br>\r\n
                    $dominio<br><br>";
                    $headers = '';
                    $headers .= "MIME-Version:1.0\r\n";
                    $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                    $headers .= "Received:from $dominio\r\n";
                    $headers .= "X-Priority:3\r\n";
                    $headers .= "X-MSMail-Priority:Normal\r\n";
                    $headers .= "From: $razon_social <$contacto>\r\n";
                    $headers .= "X-Mailer:$contacto\r\n";
                    $headers .= "Return-path:$contacto\r\n";
                    $headers .= "Reply-To:$contacto\r\n";
                    $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
                    @mail($email,"RE:$asunto",$mensaje,$headers);
                    $headers = '';
                    $headers .= "MIME-Version:1.0\r\n";
                    $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                    $headers .= "Received:from $dominio\r\n";
                    $headers .= "X-Priority:3\r\n";
                    $headers .= "X-MSMail-Priority:Normal\r\n";
                    $headers .= "From: $nombre <$email>\r\n";
                    $headers .= "X-Mailer:$email\r\n";
                    $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
                    $headers .= "Reply-To:$email\r\n";
                    $headers .= "Return-path:$email\r\n";
                    $mensaje = "Hemos recibido y notificado recepcion de contacto:\r\n\r\n
                    Desde $pais<br>\r\n
                    Telefono $telefono<br>\r\n
                    Mensaje: $mensaje";
                    @mail($contacto,$asunto,$mensaje,$headers);
         } else
         { echo '<div class="alert alert-danger" role="alert">
              <p>Ha ocurrido un error inesperado. Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
            </div>'; }
unset($result,$sql,$email,$headers,$mensaje);
$_POST = array();
?>
