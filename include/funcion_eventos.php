<?php
function AGREGAR_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
?>
<script type="text/javascript">
function enviar_parametro(valor){
location = location.pathname + '?go=AgregarEventos&id_hotel=' + valor;
}
window.onload = function(){
document.getElementById('id_hotel').onchange = function(){
enviar_parametro(this.value);
}
}
</script>
<?php
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                <div class="box-header with-border">
                    <h3 class="box-title">Agrega un nuevo evento</h3>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="include/admin_agregar_evento.php" role="form" method="post" onsubmit="return AgregarEvento(); return document.MM_returnValue" name="FormAgregarEvento" id="FormAgregarEvento">
                      <div class="box-body">
                      <div id="InformacionAgregarEvento">'.$_SESSION["mensaje"].'</div>';
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="nombre_hotel">Seleccione el Hotel al cual le pertenece este evento</label>
            <select class="form-control" name="id_hotel" id="id_hotel" onchange="enviar_parametro(this.value);">';
             if (!isset($_GET["id_hotel"]))
                { echo '<option value="" selected="selected">Seleccione</option>'; }
        $id_hotel =  $_GET["id_hotel"];
        $sql = sprintf("SELECT id,nombre_sitio FROM hesperia_settings WHERE salones_evento = '1'
        ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            if ($_GET["id_hotel"]==$rows["id"]) {
                echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
            } else {
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>'; }
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
        echo '
    <div class="form-group">
        <label for="nombre_Salon">Seleccione el salón donde se va a realizar el evento</label>
            <select class="form-control" name="id_salon" id="id_salon">
              <option value="" selected="selected">Seleccione</option>';
        $sql = sprintf("SELECT id_salon,nombre_Salon
                    FROM hesperia_salones WHERE id_hotel = '%s'
                    ORDER BY nombre_Salon ASC",
                       mysqli_real_escape_string($mysqli,$id_hotel));
        $resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultS,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id_salon"].'">'.$rows["nombre_Salon"].'</option>';
        }
        echo'</select>
        <p class="help-block">De no existir el salón debe agregarlo haciendo clic aqui</p>
    </div>
       <div class="form-group">
                          <label for="nombre_evento">Nombre del evento</label>
                          <input class="form-control" type="text" id="nombre_evento" name="nombre_evento">
                        </div>
                        <div class="form-group">
                          <label for="texto_evento">Descripcion del evento</label>
                          <textarea class="form-control" name="texto_evento" id="summernote1"></textarea>
                          <p class="help-block">Este campo no debe estar vacio</p>
                        </div>
                          <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_evento">Fecha del evento</label>
                        <input type="text" name="fecha_evento" class="form-control" data-date-format="dd/mm/yyyy" id="dp1" placeholder="">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora del Evento</label>
                                <input type="text" name="hora_evento" id="timepicker1" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="precio_evento">Precio del evento</label>
                                <input type="number" name="precio_evento" id="precio_evento" class="form-control">
                            </div>
                          </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen del Evento</label>
                                <input type="file" id="imagen"  name="imagen">
                            </div>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Submit</button>
                                <input type="hidden" value="1" name="opcion" id="opcion">
                            </div>
                        </div>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
$_SESSION["mensaje"] = '';
    return;
}
function FORM_HOTEL_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Lista de Eventos</h3>
                        </div>
                        <div class="box-body">';
    if ($_SESSION["id_hotel"] == 0){
        echo '
                        <form action="home.php?go=VerTodosEventos" role="form" method="post">
                            <div class="form-group">
                                <label for="nombre_hotel">Seleccione el hotel</label>
                                <select class="form-control" name="id_hotel" id="id_hotel">';
        $sql = sprintf("SELECT id,nombre_sitio
        FROM hesperia_settings
        WHERE salones_evento = '1'
        ORDER BY razon_social ASC");
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
        }
        echo'
                                </select>
                            </div>
                            <div class="box-footer">
                                <div class="col-lg-6 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                        Enviar
                                    </button>
                                    <input type="hidden" value="1" name="opcion" id="opcion">
                                </div>
                            </div>
                        </form>';
    }else{
        $cant = 0;
        $id_hotel = $_SESSION["id_hotel"];
        $sql = "SELECT id,nombre_evento,imagen_evento FROM hesperia_eventos WHERE id_hotel = '$id_hotel'";
        $resultEventos = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultEventos,MYSQLI_ASSOC)){
            $cant = 1;
            echo'
                            <div class="list-group">
                                <span class="list-group-item clearfix">
                                    '.$rows["nombre_evento"].'
                                    <span class="pull-right">
                                        <a class="btn btn-success btn-xs" href="home.php?go=EditarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Editar
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="home.php?go=EliminarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-trash-o"></i>
                                            Eliminar
                                        </a>
                                    </span>
                                </span>
                            </div>';
        }
        if ($cant == 0){
            echo '
                        <div class="callout callout-danger text-center">
                            <h4>
                                Disculpe
                            </h4>
                            <p>
                                Actualmente no hay eventos en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
                            </p>
                        </div>
                        ';
        }
    }
    echo'</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}

function LISTAR_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    if (isset($_REQUEST["id_hotel"])){
        $id = $_REQUEST["id_hotel"];
    }else{ $id = $_SESSION["id_hotel"]; }
    $sql = sprintf("SELECT * FROM hesperia_eventos WHERE id_hotel = '%s'",
                   mysqli_real_escape_string($mysqli,$id));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $cant = 0;
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eventos actuales en el hotel</h3>
                        </div>
                        <div class="box-body">';
    while ($rows=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $cant = 1;
        echo '
                            <div class="list-group">
                                <span class="list-group-item clearfix">
                                    '.$rows["nombre_evento"].'
                                    <span class="pull-right">
                                        <a class="btn btn-success btn-xs" href="home.php?go=EditarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-pencil-square-o"></i>
                                            Editar
                                        </a>
                                        <a class="btn btn-danger btn-xs" href="home.php?go=EliminarEvento&id='.$rows["id"].'" role="button">
                                            <i class="fa fa-trash-o"></i>
                                            Eliminar
                                        </a>
                                    </span>
                                </span>
                            </div>';
    }
    if ($cant == 0){
        echo '
                        <div class="callout callout-danger text-center">
                            <h4>
                                Disculpe
                            </h4>
                            <p>
                                Actualmente no hay eventos en este hotel. <a href="javascript:history.back()" data-toggle="tooltip" data-placement="bottom" title="Regresar al menu anterior">Regresar al menu anterior</a>
                            </p>
                        </div>
                        ';
    }
    echo '
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>';
    return;
}
function FORM_ACTUALIZA_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Actualizar evento</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return AgregarEvento(); return document.MM_returnValue" name="FormAgregarEvento" id="FormAgregarEvento">
                      <div class="box-body">
                      <div id="InformacionAgregarEvento"></div>';
    $id_evento = $_GET["id"];
    $sql = "SELECT * FROM hesperia_eventos WHERE id = '$id_evento'";
    $resultEventos = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $rowE = mysqli_fetch_array($resultEventos,MYSQLI_ASSOC);
    if ($_SESSION["id_hotel"] == 0){
        echo '
    <div class="form-group">
        <label for="nombre_hotel">Seleccione el Hotel al cual le pertenece este evento</label>
            <select class="form-control" name="id_hotel" id="id_hotel">
              <option>Seleccione</option>';
        $sql = "SELECT id,nombre_sitio FROM hesperia_settings  WHERE salones_evento = '1' ORDER BY razon_social ASC";
        $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
            if($rows["id"] == $rowE["id_hotel"]){
                echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';
            }else{
                echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';
            }
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '<input type="hidden" value="'.$id_hotel.'" name="id_hotel" id="id_hotel">';
    }
    if ($_SESSION["id_hotel"] == 0){
        echo'
    <div class="form-group">
        <label for="nombre_Salon">Seleccione el salón donde se va a realizar el evento</label>
        <select class="form-control" name="id_salon" id="id_salon">
        <option>Seleccione</option>';
        $sql = "SELECT id_salon,nombre_Salon FROM hesperia_salones ORDER BY nombre_Salon ASC";
        $resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultS,MYSQLI_ASSOC)){
            if($rowE["id_salon"] == $rows["id_salon"]){
                echo '<option value="'.$rows["id_salon"].'" selected>'.$rows["nombre_Salon"].'</option>';
            }else{
                echo '<option value="'.$rows["id_salon"].'">'.$rows["nombre_Salon"].'</option>';
            }
        }
        echo'</select>
    </div>';
    }else{
        $id_hotel = $_SESSION["id_hotel"];
        echo '
    <div class="form-group">
        <label for="nombre_Salon">Seleccione el salón donde se va a realizar el evento</label>
            <select class="form-control" name="id_salon" id="id_salon">
            <option>Seleccionar</option>';
        $sql = "SELECT id_salon,nombre_Salon FROM hesperia_salones WHERE id_hotel = '$id_hotel' ORDER BY nombre_Salon ASC";
        $resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
        while ($rows = mysqli_fetch_array($resultS,MYSQLI_ASSOC)){
            if($rows["id_salon"] == 1){
                echo '<option value="'.$rows["id_salon"].'" selected>'.$rows["nombre_Salon"].'</option>';
            }else{
                echo '<option value="'.$rows["id_salon"].'">'.$rows["nombre_Salon"].'</option>';
            }
        }
        echo'</select>
    </div>';
    }
    echo '                  <div class="form-group">
                          <label for="nombre_evento">Nombre del evento</label>
                          <input class="form-control" type="text" id="nombre_evento" name="nombre_evento" value="'.$rowE["nombre_evento"].'">
                        </div>
                        <div class="form-group">
                          <label for="texto_evento">Descripcion del evento</label>
                          <textarea class="form-control" name="texto_evento" id="texto_evento">'.$rowE["texto_evento"].'</textarea>
                          <p class="help-block">Este campo no debe estar vacio</p>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_evento">Fecha del evento</label>
<input type="text" name="fecha_evento" class="form-control" value="'.date("d-m-Y",$rowE["fecha_evento"]).'" data-date-format="dd-mm-yyyy" id="dp1" placeholder="'.date("d-m-Y",$rowE["fecha_evento"]).'">
                            </div>
                        </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="hora_evento">Hora del Evento</label>
                                <input type="text" name="hora_evento" id="timepicker1" class="form-control" value="'.$rowE["hora_evento"].'">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="precio_evento">Precio del evento</label>
                                <input type="number" name="precio_evento" id="precio_evento" class="form-control" value="'.$rowE["precio_evento"].'">
                            </div>
                          </div>
                        <div class="form-group">
                          <label for="exampleInputFile">Imagen del Evento</label>
                          <input type="file" id="exampleInputFile">
                        </div>
                        <div class="form-group">
                          <p>
                            Haga clic
                            <a href="#0" data-toggle="modal" data-target="#myModal">aquí</a> para ver la imagen actual
                          </p>
                        </div>
                      </div>
                        <div class="box-footer">
                            <div class="col-lg-7">
                                <p>
                                    Si ya realizo todo los cambios que deseaba hacer por favor, pulse el siguiente boton:
                                </p>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Enviar</button>
                                <input type="hidden" value="2" name="opcion" id="opcion">
                                <input type="hidden" value="'.$id_evento.'" name="id" id="id">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Imagen actual</h4>
      </div>
      <div class="modal-body text-center">
        <img src="../img/eventos/'.$rows["imagen_evento"].'" data-toggle="tooltip" data-placement="bottom" title="'.$rows["nombre_evento"].'">
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>';
    return;
}
?>
