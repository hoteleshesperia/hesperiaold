<?php 
  function PAQUETES_GENERAL($mysqli,$data,$hostname,$user,$password,$db_name){
    if($data["temporada"] == 100){
       date_default_timezone_set('America/Caracas');
      $test = date('Y-m-d H:i:s', time());

      echo '
      <div class="page-header">
            <img src="https://hoteleshesperia.com.ve/img/contenido/Banner atrapalo-01.jpg" class="img-responsive"/> 
            <h2 class="text-center">¡Todos los días atrapa las diferentes oportunidades que tenemos para tí!</h2> 
            <h3 class="text-center">Accede cada día a esta sección y atrapa las novedades que iremos publicando.</h3>   
            <br>
                    
      </div>';

      /*$sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["temporada"]));

      
      $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

      if ($hay < 1){ 
        echo '<br>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos ofertas para atrapar!</p>
              </div>';
        return; 
      }else{
        while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
          $data["id_paquete_promo"] = $rowPaquetes["id"];
          PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name);
          
        }
      }*/

    } else{

      echo '
      <div class="page-header">
            <h1 style="margin-top:10px;">Nuestros Paquetes</h1>
          </div>';
    }
    //if($_SESSION["locationGeo"] == 'VE'){
          $hoy = date("Y-m-d",time());          

        if($data["id_paquete_promo"] == 0){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada <> 100 ORDER BY id_hotel, temporada ASC");
            else
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 0 and vip = 0 AND temporada <> 100 ORDER BY id_hotel, temporada ASC");

        }else{
          if($_SESSION["locationGeo"] == 'VE')
            $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND id = '%s' AND temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));
          else
            $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 0 and vip = 0 AND id = '%s' AND temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

        }

        if($data["temporada"] > 0){

          if($data["temporada"] == 100){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' AND hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' AND hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
          }else{
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
          }
          
        }

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

        if ($hay < 1){ 
          echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
            return; 
       }else{
          echo '
          <div class="container-fluid">
          <div class="row">';
          $id_hotel_aux = 0;
          while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
            $fecha = date("d/m/Y",time()+86400); 
            $nombre_paquete = $rowPaquetes["nombre_paquete"];
            $capacidad = $rowPaquetes["capacidad"];
            $asunto = str_replace(' ','+',$nombre_paquete);

            $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                           mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

            $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

            $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
            $hotel = $rowSetting["razon_social"];
            /*if($id_hotel_aux != $rowPaquetes["id_hotel"]){
              if($id_hotel_aux != 0){
                echo '</div><div class="row">';
              }
              echo '
                <h3 class="text-primary text-center">'.$hotel.'</h3>';
              $id_hotel_aux = $rowPaquetes["id_hotel"];
            }*/

            $desdePaq = '';
            $hastaPaq = '';
            if($_SESSION["locationGeo"] == 'VE' ){
              $precio = $rowPaquetes["precio"];
            }else{
              $precio = $rowPaquetes["price"];
            }
            
            $habitacion = '';

            if($rowPaquetes["porcentaje"] > 0){
              $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                        mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));
              $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
              $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
              $habitacion = $rowHab["nombre_habitacion"];
              echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

            }

            $pos = strpos($rowPaquetes["nombre_paquete"], '. ');
            if($pos > 0){
              $pos++;
            }

            $nombre = substr($rowPaquetes["nombre_paquete"], $pos);
            echo '
            <div class="col-sm-6 col-md-4">';
            if($rowPaquetes["temporada"] == 100)
              echo'
              <div class="thumbnail equalizeAtrapa">';
            else
              echo'
              <div class="thumbnail equalize">';
              if($rowPaquetes["temporada"] == 100)
              echo '
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="200" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">';
              else
              echo '
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="300" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">';
                
              echo '
                <div class="caption">
                  <h4 style="height: 50px;" class="text-justify">'.$nombre.'</h4>
                  <p class="text-muted text-center"><strong>'.$hotel.'</strong></p>';
                  if($rowPaquetes["temporada"] == 100)
                  echo'
                  <div id="getting-started'.$rowPaquetes["id"].'" data-date="'.$rowPaquetes["hasta_atrapalo"].'"></div>
                  ';  
                  
                  echo'
                  <p><a href="https://hoteleshesperia.com.ve/paquetes/'.$rowPaquetes["id"].'/'.toAscii($nombre).'" class="btn btn-primary" role="button">Reservar</a></p>
                </div>
              </div>
            </div>
            ';
          }
          echo '</div>
          </div>';
       } 

    
      
    /*}else{
       echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
    }*/
    return;
    
  }

  function PAQUETES_GENERAL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
    <div class="page-header">
            <h1 style="margin-top:10px;">Nuestros Paquetes</h1>
          </div>';
    //if($_SESSION["locationGeo"] == 'VE'){
          $hoy = date("Y-m-d",time());          

        
        if($_SESSION["locationGeo"] == 'VE' )
          $sql = sprintf("SELECT * FROM hesperia_paquetes where temporada <> 100 and alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND id_hotel = '%s' ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id"]));
        else
          $sql = sprintf("SELECT * FROM hesperia_paquetes where temporada <> 100 and alojamiento = 0 and status_dolar <> 'N' and partai = 0 and vip = 0 AND id_hotel = '%s' ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id"]));
        

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

        if ($hay < 1){ 
          echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
            return; 
       }else{
          echo '
          <div class="container-fluid">
          <div class="row">';
          $id_hotel_aux = 0;
          while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
            $fecha = date("d/m/Y",time()+86400); 
            $nombre_paquete = $rowPaquetes["nombre_paquete"];
            $capacidad = $rowPaquetes["capacidad"];
            $asunto = str_replace(' ','+',$nombre_paquete);

            $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                           mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

            $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

            $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
            $hotel = $rowSetting["razon_social"];
            /*if($id_hotel_aux != $rowPaquetes["id_hotel"]){
              if($id_hotel_aux != 0){
                echo '</div><div class="row">';
              }
              echo '
                <h3 class="text-primary text-center">'.$hotel.'</h3>';
              $id_hotel_aux = $rowPaquetes["id_hotel"];
            }*/

            $desdePaq = '';
            $hastaPaq = '';
            if($_SESSION["locationGeo"] == 'VE' ){
              $precio = $rowPaquetes["precio"];
            }else{
              $precio = $rowPaquetes["price"];
            }
            
            $habitacion = '';

            if($rowPaquetes["porcentaje"] > 0){
              $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                        mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));
              $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
              $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
              $habitacion = $rowHab["nombre_habitacion"];
              echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

            }

            $pos = strpos($rowPaquetes["nombre_paquete"], '. ');
            if($pos > 0){
              $pos++;
            }

            $nombre = substr($rowPaquetes["nombre_paquete"], $pos);
            echo '
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail equalize">
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="300" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">
                <div class="caption">
                  <h4 style="height: 50px;" class="text-justify">'.$nombre.'</h4>
                  <p class="text-muted text-center"><strong>'.$hotel.'</strong></p>
                  
                  <p><a href="https://hoteleshesperia.com.ve/paquetes/'.$rowPaquetes["id"].'/'.toAscii($nombre).'" class="btn btn-primary" role="button">Reservar</a></p>
                </div>
              </div>
            </div>
            ';
          }
          echo '</div>
          </div>';
       } 
    /* }else{
       echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
    }*/
    return;
    
  }

  function PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name){
    $pais = $_SESSION["locationGeo"];
    $temporada = -1;
    
      $hoy = date("Y-m-d",time());   

      if($pais == 'VE')
        $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' AND id = '%s' ORDER BY orden ASC",
                        mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));
      else
        $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' AND id = '%s' ORDER BY orden ASC",
                        mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

        echo '<input type="hidden" name="pais" id="pais" value="'.$pais.'"/>';

        if ($hay < 1){ 

          echo '<br/><div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';

          return; 
        }else{
           while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $temporada = $rowPaquetes["temporada"];
             $id_hotel=$rowPaquetes["id_hotel"];
            if($pais == 'VE' ){
              $precio = $rowPaquetes["precio"];
              $tipoRegimen = 'Desayuno incluido';
              if($precio == 0){
                $precio = $rowPaquetes["precio_pension"];
                $tipoRegimen = 'Desayuno y cena incluidos';
                if($precio == 0){
                  $precio = $rowPaquetes["precio_todo_inc"];
                  $tipoRegimen = 'Todo incluido';
                  if($precio == 0){
                    $precio = $rowPaquetes["pension_completa"];
                    $tipoRegimen = 'Pension Completa';
                    if($precio == 0){
                      $precio = $rowPaquetes["pension_blue"];
                      $tipoRegimen = 'Hesperia Blue';
                    }
                  }
                }
              }
              $precio_nino = $rowPaquetes["precio_nino"];
              $moneda = 'Bs. ';
            }else{
              $precio = $rowPaquetes["price"];
              $precio_nino = $rowPaquetes["price_child"];
              $moneda = 'USD. ';
              if( $id_hotel == 1)
                $tipoRegimen = 'Desayuno incluido';
              else
                $tipoRegimen = 'Todo incluido';
            }

              $nombre_paquete = $rowPaquetes["nombre_paquete"];
              $capacidad = $rowPaquetes["capacidad"];
              /*Se buscan los upselling disponibles por ese paquete*/
              $sqlUpselling = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 1 and status <> 'N' and partai = 0 and vip = 0 AND id_hotel = '%s'
              and (solo_incluir_en = 0 or solo_incluir_en = '".$data["id_paquete_promo"]."') ORDER BY orden ASC",
                mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));
              $resultUpselling = QUERYBD($sqlUpselling,$hostname,$user,$password,$db_name);
              /*Se busca información del hotel en donde se ofrece el paquete*/
              $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                       mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));
              $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

              $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
              $hotel = $rowSetting["razon_social"];

              $desdePaq = '';
              $hastaPaq = '';
              $habitacion = '';
              //$fecha = date("d/m/Y",time()+86400); 
              $fecha = date("d/m/Y",time());
              
              if($rowPaquetes["porcentaje"] > 0){

                $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                               mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));

                 $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
                 $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
                 $habitacion = $rowHab["nombre_habitacion"];

              }
              
              if($rowPaquetes["tiempo"] > 0){
                if($rowPaquetes["noches_gratis"] > 0){
                  $precio_ant = $precio * $rowPaquetes["tiempo"];
                  $precio = $precio * ($rowPaquetes["tiempo"] - $rowPaquetes["noches_gratis"]);
                }else{
                  $precio = $precio * $rowPaquetes["tiempo"];
                  $precio_ant = $precio;
                  $precio = $precio - ($precio * $rowPaquetes["porcentaje"]);
                 
                }
              }
              if($hoy < $rowPaquetes["desde"])
                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));


            echo '<input type="hidden" name="precioAD" id="precioAD" value="'.($rowPaquetes["precio"]- ($rowPaquetes["precio"] * $rowPaquetes["porcentaje"])).'"/>';
            echo '<input type="hidden" name="precioADC" id="precioADC" value="'.($rowPaquetes["precio_pension"]- ($rowPaquetes["precio_pension"] * $rowPaquetes["porcentaje"])).'"/>';
            echo '<input type="hidden" name="precioADAC" id="precioADAC" value="'.($rowPaquetes["pension_completa"]- ($rowPaquetes["pension_completa"] * $rowPaquetes["porcentaje"])).'"/>';
            echo '<input type="hidden" name="precioATI" id="precioATI" value="'.($rowPaquetes["precio_todo_inc"]- ($rowPaquetes["precio_todo_inc"] * $rowPaquetes["porcentaje"])).'"/>';
            echo '<input type="hidden" name="precioAHB" id="precioAHB" value="'.($rowPaquetes["pension_blue"]- ($rowPaquetes["pension_blue"] * $rowPaquetes["porcentaje"])).'"/>';

            echo '<input type="hidden" name="precioPlanoAD" id="precioPlanoAD" value="'.($rowPaquetes["precio"]).'"/>';
            echo '<input type="hidden" name="precioPlanoADC" id="precioPlanoADC" value="'.($rowPaquetes["precio_pension"]).'"/>';
            echo '<input type="hidden" name="precioPlanoADAC" id="precioPlanoADAC" value="'.($rowPaquetes["pension_completa"]).'"/>';
            echo '<input type="hidden" name="precioPlanoATI" id="precioPlanoATI" value="'.($rowPaquetes["precio_todo_inc"]).'"/>';
            echo '<input type="hidden" name="precioPlanoAHB" id="precioPlanoAHB" value="'.($rowPaquetes["pension_blue"]).'"/>';

            echo '<input type="hidden" name="priceTI" id="priceTI" value="'.($rowPaquetes["price"]- ($rowPaquetes["price"] * $rowPaquetes["porcentaje"])).'"/>
                  <input type="hidden" name="pricePlanoTI" id="pricePlanoTI" value="'.$rowPaquetes["price"].'"/>';

              echo '
              <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3>'.$nombre_paquete.'</h3>';
                    
                  echo '
                  </div>
                  <div class="panel-body">
                    <div class="col-md-12 text-justify">
                      <div class="col-md-4">
                        <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'"  alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive" />
                        ';
                    if($rowPaquetes["temporada"] == 100)
                  echo'
                  <div class="col-md-11">
                  <div id="getting-started'.$rowPaquetes["id"].'" data-date="'.$rowPaquetes["hasta_atrapalo"].'"></div>
                  </div>';  

                  echo '
                      <div class="text-center" id="share"></div>
                      </div>
                      <div class="col-md-8">
                      '.$rowPaquetes["descripcion_paquete"].' <br>';


                      
                      

                      if($precio > 0){

                        echo'
                      <hr>

                          <strong>Selecciona fecha de llegada<br> </strong><input type="text" class="dpd3 input-sm" value="'.$fecha.'" id="dpd3" name="dpd3" placeholder="Entrada" data-date-format="dd/mm/yyyy" data-min-date="'.$rowPaquetes["desde"].'" data-max-date="'.$rowPaquetes["hasta"].'" language="es" readonly="readonly" ><br>';

                        if($rowPaquetes["alojamiento"] == 0 and $rowPaquetes["full_day"] == 0){
                          if($pais == 'VE'){
                            echo '
                            <strong>Seleccione régimen <br></strong>
                            <select name="regimen" id="regimen" >';
                              if($rowPaquetes["precio"] > 0){
                                echo '<option value="1">Desayuno</option>';
                              }
                              if($rowPaquetes["precio_pension"] > 0){
                                echo '<option value="2">Desayuno y Cena</option>';
                              }
                              if($rowPaquetes["precio_todo_inc"] > 0){
                                echo '<option value="3">Todo incluido</option>';
                              }
                              if($rowPaquetes["pension_completa"] > 0){
                                echo '<option value="4">Desayuno, almuerzo y cena</option>';
                              }
                            echo '</select>
                            <br>';
                             if($rowPaquetes["pension_blue"] > 0){
                                echo '<p id="blue">
                            <input type="checkbox" id="HBlue" name="HBlue"/>Añadir Servicios Hesperia Blue<strong> <a id="modal-433854" href="#modal-container-Hblue" data-toggle="modal">¿Que es?</a></strong>
                            </p>
                            <input type="hidden" id="vblue" name="vblue" value="0"/>';
                             }
                          }else{
                            if($id_hotel == 1 ){
                              echo '
                              <br>
                              <select>
                                <option>Desayuno incluido</option>
                              </select>
                              <input type="hidden" name="regimen" id="regimen" value="1"/>
                              <br>';
                            }else{
                              echo '
                              <br>
                              <select>
                                <option>Todo incluido</option>
                              </select>
                              <input type="hidden" name="regimen" id="regimen" value="3"/>
                              <br>'; 
                            }
                              
                          }
                          
                            if($rowPaquetes["noches_gratis"] > 0){

                              $diasP = $rowPaquetes["tiempo"] + $rowPaquetes["noches_gratis"] + 1;
                              $nochesP = $rowPaquetes["tiempo"] + $rowPaquetes["noches_gratis"];

                              $nombre_paquete = $nombre_paquete.'<br>Detalles: Estadía con '.$tipoRegimen;
                              if($rowPaquetes["configurable"] == 0){
                              echo '
                                <strong>Seleccione cantidad de noches <br></strong>
                                <select name="tiempo" id="tiempo" >';
                                
                                  for($i = $rowPaquetes["tiempo"]; $i < ($rowPaquetes["tiempo"]+3); $i++){
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                  }
                                
                              
                              echo '
                                </select>
                                <br>
                                <br>';
                              }else{
                                echo '<input type="hidden" name="tiempo" id="tiempo" value="'.$rowPaquetes["tiempo"].'"/>';
                              }
                            }else{
                              $diasP = $rowPaquetes["tiempo"]  + 1;
                              $nochesP = $rowPaquetes["tiempo"] ;
                              $nombre_paquete = $nombre_paquete.'<br>Detalles: Estadía con '.$tipoRegimen;
                              if($rowPaquetes["configurable"] == 0){
                                echo '
                                  <strong>Seleccione cantidad de noches <br></strong>
                                  <select name="tiempo" id="tiempo" >';
                                $noches_hasta = $rowPaquetes["noches_hasta"];
                                if($noches_hasta < $rowPaquetes["tiempo"]){
                                  $noches_hasta = $rowPaquetes["tiempo"]+3;
                                }
                                for($i = $rowPaquetes["tiempo"]; $i < $noches_hasta; $i++){
                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                echo '
                                  </select>
                                  <br>
                                  <br>';
                              }else{
                                echo '<input type="hidden" name="tiempo" id="tiempo" value="'.$rowPaquetes["tiempo"].'"/>';
                              }
                            }
                            
                        }else{
                          echo '<input type="hidden" name="tiempo" id="tiempo" value="0"/>';
                        }


                          if($precio_nino > 0){

                              echo '<strong>Precio por adulto: '.number_format(round($precio), 2, ',', '.').'</strong><br/>

                              <strong>Precio por niño: '.number_format(round($precio_nino), 2, ',', '.').'</strong><br/>';

                              if($hoy < $rowPaquetes["desde"]){

                                      $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                              }

                          }else{

                              if($rowPaquetes["porcentaje"] > 0){

                                 

                                  if($rowPaquetes["porcentaje"] >  0 && $rowPaquetes["ninos_gratis"] == 0){

                                      echo 'Precio antes: <label><strike id="precioPlano">'.number_format(round($precio_ant), 2, ',', '.').'</strike></label><br/>

                                      <strong>Precio ahora: <label id="precioDesc">'.number_format(round($precio), 2, ',', '.').'</label></strong>'; 
if($rowPaquetes["id"] != 251)
echo ' (Con un '.substr($rowPaquetes["porcentaje"], 2, 2).'% de descuento aplicado)'; 

echo '<br/>

                                      <input type="hidden" name="numPaq" id="numPaqV2" value="1"/>';

                                  }else{

                                      echo 'Precio antes: <label><strike id="precioPlano">'.number_format(round($precio_ant), 2, ',', '.').'</strike></label><br/>

                                      <strong>Precio ahora: <label id="precioDesc">'.number_format(round($precio), 2, ',', '.').'</label></strong> (Incluidos '.$rowPaquetes["ninos_gratis"].' Niños Gratis)<br/>

                                      <input type="hidden" name="numPaq" id="numPaqV2" value="1"/>';

                                  }

                                  

                              }else{

                                  

                                  /*echo '<strong>Precio: '.number_format(round($precio), 2, ',', '.').'</strong><br/>
                                  <input type="hidden" name="numPaq" id="numPaqV2" value="1"/>'; */
                                  if($rowPaquetes["noches_gratis"] >  0){


                                      echo 'Precio antes: <label><strike id="precioPlano">'.number_format(round($precio_ant), 2, ',', '.').'</strike></label><br/>

                                      <strong>Precio ahora: <label id="precioDesc">'.number_format(round($precio), 2, ',', '.').'</label></strong> (Con '.$rowPaquetes["noches_gratis"].' Noche(s) gratis)<br/>

                                      <input type="hidden" name="numPaq" id="numPaqV2" value="1"/>';

                                  }else{
                                    echo '<input type="hidden" name="numPaq" id="numPaqV2" value="1"/>';
                                  }

                              }

                              

                          }
                      echo '<input type="hidden" name="id_paquete_promo" value="'.$data["id_paquete_promo"].'"/>';
                      echo '
                      <input type="hidden" name="nombrePaqInicial" value="'.$rowPaquetes["nombre_paquete"].'" id="nombrePaqInicial"/>
                      <input type="hidden" name="nombrePaq" value="'.$nombre_paquete.'" id="nombrePaq"/>

                      <input type="hidden" name="precioPaq" value="'.$precio.'" id="precioPaq"/>

                      <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                      <input type="hidden" name="id_hotel" value="'.$rowPaquetes["id_hotel"].'"/>

                      <input type="hidden" name="noches_gratis" value="'.$rowPaquetes["noches_gratis"].'" id="noches_gratis"/>              

                      <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                      <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                      <input type="hidden" name="precioNino" value="'.$precio_nino.'" id="precioNino"/>

                      <input type="hidden" name="porcentaje" value="'.$rowPaquetes["porcentaje"].'"/>

                      <input type="hidden" name="partai" value="'.$rowPaquetes["partai"].'"/>

                      <input type="hidden" name="habitacion" value="'.$habitacion.'"/>

                      <input type="hidden" name="vip" value="0"/>

                      <input type="hidden" name="vuelo" value="'.$rowPaquetes["vuelo"].'"/>

                      <input type="hidden" name="noches_inicio" value="'.$rowPaquetes["tiempo"].'" id="noches_inicio"/>';

                      if($precio_nino > 0){

                         echo'<div class="row">

                                  <div class="col-md-2"><strong>Adultos<br> 

                                      <select name="numPaq" id="numPaqV2">

                                          <option value="1">1</option>

                                          <option value="2">2</option>

                                          <option value="3">3</option>

                                          <option value="4">4</option>

                                      </select></strong>

                                  </div>

                                  <div class="col-md-2"><strong>Niños<br> 

                                      <select name="numPaqN" id="numPaqNV2">

                                          <option value="0">0</option>

                                          <option value="1">1</option>

                                          <option value="2">2</option>

                                          <option value="3">3</option>

                                          <option value="4">4</option>

                                      </select></strong>

                                  </div>

                              </div>';

                      }else{

                          if($rowPaquetes["porcentaje"] <= 0 && $capacidad > 1){

                              if($capacidad == 5){

                                  echo'

                              <div class="row">

                                      <div class="col-sm-6"><strong>Cantidad<br> 

                                          <select name="numPaq" id="numPaqV2">                                    

                                              <option value="5">5</option>

                                              <option value="6">6</option>

                                              <option value="7">7</option>

                                              <option value="8">8</option>

                                              <option value="9">9</option>

                                              <option value="10">10</option>

                                              <option value="11">11</option>

                                              <option value="12">12</option>

                                              <option value="13">13</option>

                                              <option value="14">14</option>

                                              <option value="15">15</option>

                                              <option value="16">16</option>

                                              <option value="17">17</option>

                                              <option value="18">18</option>

                                              <option value="19">19</option>

                                              <option value="20">20</option>

                                          </select></strong>

                                      </div>

                              </div>';

                              }else{

                              echo'

                              <div class="row">

                                      <div class="col-sm-6"><strong>Cantidad<br> 

                                          <select name="numPaq" id="numPaqV2">

                                              <option value="1">1</option>

                                              <option value="2">2</option>

                                              <option value="3">3</option>

                                              <option value="4">4</option>

                                              <option value="5">5</option>

                                              <option value="6">6</option>

                                              <option value="7">7</option>

                                              <option value="8">8</option>

                                              <option value="9">9</option>

                                              <option value="10">10</option>

                                              <option value="11">11</option>

                                              <option value="12">12</option>

                                              <option value="13">13</option>

                                              <option value="14">14</option>

                                              <option value="15">15</option>

                                              <option value="16">16</option>

                                              <option value="17">17</option>

                                              <option value="18">18</option>

                                              <option value="19">19</option>

                                              <option value="20">20</option>

                                          </select></strong>

                                      </div>

                              </div>';

                              }

                          }

                      }

                      
                      }
                    echo '
                      </div>  
                    </div>
                  </div>
                  <div  class="modal fade" id="modal-container-Hblue" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                            <div class="modal-dialog">

                                <div class="modal-content mymod">

                                    <div class="modal-header">
                                    <!--
                                        <a data-dismiss="modal" class="close">×</a>

                                        <h3>Servicios Hesperia Blue</h3>-->

                                     </div>

                                     <div class="modal-body">
                                     <!--
                                        <div class="text-justify">
                                            <p class="MsoNormal">
                                                <span style="line-height: 1.42857; font-weight: bold;">
                                                Los servicios de Hesperia blue incluyen:
                                                </span>
                                            </p>
                                            <p class="MsoNormal">
                                                <ul>
                                                    <li>
                                                        <span style="line-height: 1.42857;">Almuerzo</span>
                                                    </li>
                                                    <li>
                                                        <span style="line-height: 1.42857;">Cena</span>
                                                    </li>
                                                    <li>
                                                        <span style="line-height: 1.42857;">Snacks</span>
                                                    </li>
                                                    <li>
                                                        <span style="line-height: 1.42857;">Selección de bebidas alcohólicas y no alcohólicas nacionales</span>
                                                    </li>
                                                </ul>
                                                
                                            </p>
                                        </div> -->
                                        
                                        <img src="https://hoteleshesperia.com.ve/img/hblue.png" alt="Hesperia Blue" class="img-responsive">   

                                    </div>

                                    <div class="modal-footer">

                                        <a href="#" data-dismiss="modal" class="btn">Cerrar</a>

                                    </div>

                                </div>

                            </div>

                        </div>
                  <div class="panel-footer">';
                  if($rowPaquetes["full_day"] == 0 && $rowPaquetes["temporada"] != 100){
                    $up = 0;
                    $ups_especial = 0;
                  echo '
                    <h5><strong>Personalice su estancia</strong></h5>
                    <div class="checkbox">';
                      while($rowUpselling = mysqli_fetch_array($resultUpselling,MYSQLI_ASSOC)){
                        if($pais == 'VE'){
                          $precio_up = $rowUpselling["precio"];
                        }else{
                          $precio_up = $rowUpselling["price"];
                        }

                        if($rowUpselling["solo_incluir_en"] == $data["id_paquete_promo"]){
                          if($ups_especial == 0)
                            echo '<label style="font-size:14px;"><input type="checkbox" name="upselling-'.$up.'" class="UpsPers aSumar" id="perso1" data-precio="'.$precio_up.'" value="'.$rowUpselling["id"].'" /><a id="modal-433854" href="#modal-container-paq'.$rowUpselling["id"].'" data-toggle="modal" title="Detalles del paquete">'.$rowUpselling["nombre_paquete"].'</a>.&nbsp;Precio:&nbsp;'.$moneda.'&nbsp;<strong>'.number_format($precio_up, 2, ',', '.').'</strong></label><br>';
                          else
                            echo '<label style="font-size:14px;"><input type="checkbox" name="upselling-'.$up.'" class="UpsPers aSumar" id="perso2" data-precio="'.$precio_up.'" value="'.$rowUpselling["id"].'" /><a id="modal-433854" href="#modal-container-paq'.$rowUpselling["id"].'" data-toggle="modal" title="Detalles del paquete">'.$rowUpselling["nombre_paquete"].'</a>.&nbsp;Precio:&nbsp;'.$moneda.'&nbsp;<strong>'.number_format($precio_up, 2, ',', '.').'</strong></label><br>';
                          $ups_especial++;  
                        }else{
             if($rowPaquetes["personalizado"] == 'N')
                          echo '<label style="font-size:14px;"><input type="checkbox" name="upselling-'.$up.'" class="checkUpselling aSumar" data-precio="'.$precio_up.'" value="'.$rowUpselling["id"].'" /><a id="modal-433854" href="#modal-container-paq'.$rowUpselling["id"].'" data-toggle="modal" title="Detalles del paquete">'.$rowUpselling["nombre_paquete"].'</a>.&nbsp;Precio:&nbsp;'.$moneda.'&nbsp;<strong>'.number_format($precio_up, 2, ',', '.').'</strong></label><br>';
                        }
                        

                        echo'
                        
                        <div  class="modal fade" id="modal-container-paq'.$rowUpselling["id"].'" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                <div class="modal-dialog">

                                    <div class="modal-content mymod">

                                        <div class="modal-header">

                                            <a data-dismiss="modal" class="close">×</a>

                                            <h5>'.$rowUpselling["nombre_paquete"].'</h5>

                                         </div>

                                         <div class="modal-body">

                                            <p style="font-size:13px;"><strong>Precio: '.$moneda.' '.number_format(round($precio_up), 2, ',', '.').'</strong></p>

                                             <br/>

                                            <div class="text-justify" style="font-size:13px;"> '.$rowUpselling["descripcion_paquete"].'</div> 

                                                         

                                        </div>

                                        <div class="modal-footer">

                                            <a href="#" data-dismiss="modal" class="btn">Cerrar</a>

                                        </div>

                                    </div>

                                </div>

                            </div>
                        ';
                        $up ++;
                      }

                    echo '
                    </div>
                    <hr>';
                    }

                    if($precio > 0){
                      echo'
                      <div class="text-center">
                        <p><strong>Precio total: '.$moneda.' <input type="text" disabled="disabled" value="'.number_format(round($precio), 2, ',', '.').'" name="precioTotalPagar" id="precioTotalPagar"></strong></p>
                        <input type="hidden" id="precioSF" value='.$precio.'/>
                        <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>
                      </div>';

                    }
                echo '
                  </div>
                </div>
              </form>
              ';
           } 
        }
      if($temporada != 100){

         
        
        $sqlDestacados = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 and destacado = 1 ORDER BY orden ASC");
         $resultDestacados = QUERYBD($sqlDestacados,$hostname,$user,$password,$db_name);
         echo '
         <h3>Los más destacados</h3>
          <div class="row">';
         while ($rowDestacados=mysqli_fetch_array($resultDestacados,MYSQLI_ASSOC)){
            echo '
              <div class="col-md-3 col-xs-6 pdf-thumb-box no-space" id="paquete-'.$rowDestacados["id"].'"> 
                          
                  <a href="https://hoteleshesperia.com.ve/paquetes/'.$rowDestacados["id"].'/'.toAscii($rowDestacados["nombre_paquete"]).'" >
                    <div class="pdf-thumb-box-overlay">
                      <h6 class="text-center">'.$rowDestacados["nombre_paquete"].'</h6>
                      <div class="btn-primary btn btn-sm">
                        Reservar
                      </div>
                    </div>
                    <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowDestacados["img_paquete"].'" alt="'.$rowDestacados["nombre_paquete"].'" class="img-responsive">
                  </a>
              </div>';
         }
         echo '</div>'; 
      }

        echo '

            <div id="RespuestaIp"></div>

               </div>';

        return;
    /*}else{
      echo '<br/><div class="text-center">
            <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';

      return;
    }*/
    
  }
?>

    