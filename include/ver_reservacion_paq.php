<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/

session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header("location:../error.html");
	die();}

$antesdecore = 1;
include 'databases.php';
$idReservacion = $_POST['idReservacion']; # id reservacion
$sql = sprintf("SELECT * FROM hesperia_v2_reservas_paq WHERE id = '%s'" ,
						mysqli_real_escape_string($mysqli,$idReservacion));

$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
	
	$sqlH = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s'" ,
					mysqli_real_escape_string($mysqli,$row["id_hotel"]));
	$resultH = QUERYBD($sqlH,$hostname,$user,$password,$db_name);
	$rowH=mysqli_fetch_array($resultH,MYSQLI_ASSOC);

#
echo '
	<div class="row"><h4>&nbsp&nbsp;Revisión de Datos de Reservación</h4>
		<div class="col-md-12">
			<div class="form-group">
				Hotel: <strong>'.$rowH["nombre_sitio"].'</strong><br/>';
				echo 'Descripción: <strong>'.$row["nombre_paq"].'</strong><br/>';
				echo 'Fecha: <strong>'.$row["fecha"].'</strong> </strong><br/>'; 
				echo 'Pago Bs: '.number_format($row["total"], 2, ',', '.');


echo '
			</div
<hr/>
		</div>
	</div>';
}
else
{  echo '<div class="alert alert-danger" role="alert">
	<p>La reservación solicitada, pudo haber sido eliminada; o no está registrada.
	</p></div>';

}

# Fin Revision de la solicitudes por parte del usuario
unset($result,$sql,$ahora);
$_POST = array();
?>
