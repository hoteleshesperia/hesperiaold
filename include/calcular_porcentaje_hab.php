<?php 
session_start();
$antesdecore = 1;
include 'databases.php';

$id_hotel = $_POST["idhotel"];
$id_tipo_hab = $_POST["idtipohab"];
$combinacion = $_POST["combinacion"];
$monto = $_POST["monto"];
$fechaDesde = $_POST["fechaDesde"];
$fechaHasta = $_POST["fechaHasta"];
$regimen = $_POST["regimen"];
$pais=$_SESSION["locationGeo"];


if($pais == 'VE' ){
    $sql = sprintf("SELECT porcentaje FROM hesperia_v2_porcentaje_hab 
                WHERE id_hotel = '%s' 
                AND id_tipo_hab = '%s' 
                AND combinacion = '%s'",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$id_tipo_hab),
                mysqli_real_escape_string($mysqli,$combinacion));
}else{
    $sql = sprintf("SELECT percentage as porcentaje FROM hesperia_v2_porcentaje_hab 
                WHERE id_hotel = '%s' 
                AND id_tipo_hab = '%s' 
                AND combinacion = '%s'",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$id_tipo_hab),
                mysqli_real_escape_string($mysqli,$combinacion));
}
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    

if($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
    $porcHab = $rows["porcentaje"];
    if($pais == 'VE' ){
        $sql = sprintf("SELECT id_habitacion, nombre_habitacion, sum(b.suplementos) as suplementos, (sum(round(b.precio + (b.precio * '$porcHab')))) as precio, (min(disponible)) as disponible, capacidad_personas, (sum(round(b.precio_pension + (b.precio_pension * '$porcHab')))) as precio_pension, (sum(round(b.pension_completa + (b.pension_completa * '$porcHab')))) as pension_completa, (sum(round(b.pension_blue + (b.pension_blue * '$porcHab')))) as pension_blue FROM hesperia_habitaciones a LEFT JOIN hesperia_v2_precio_calendario b on a.id_hotel = b.id_hotel AND a.id_habitacion = b.id_tipo_hab AND b.fecha BETWEEN '$fechaDesde' and '$fechaHasta' WHERE a.id_hotel = '$id_hotel' and id_tipo_hab = '$id_tipo_hab' group by id_habitacion, nombre_habitacion, capacidad_personas ORDER BY a.nombre_habitacion ASC");
                    
    }else{
        $sql = sprintf("SELECT id_habitacion, nombre_habitacion, sum(b.suplements) as suplementos, (sum(round(b.price_sd + (b.price_sd * '$porcHab')))) as price_sd, (sum(round(b.price + (b.price * '$porcHab')))) as precio, (min(enable)) as disponible, capacidad_personas, (sum(round(b.precio_pension + (b.precio_pension * '$porcHab')))) as precio_pension FROM hesperia_habitaciones a LEFT JOIN hesperia_v2_precio_calendario b on a.id_hotel = b.id_hotel AND a.id_habitacion = b.id_tipo_hab AND b.fecha BETWEEN '$fechaDesde' and '$fechaHasta' WHERE a.id_hotel = '$id_hotel' and id_tipo_hab = '$id_tipo_hab' and id_tipo_hab not in (4,5,6,17) group by id_habitacion, nombre_habitacion, capacidad_personas ORDER BY a.nombre_habitacion ASC");
   
    }
    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $numFilas = $resultH->num_rows;
    if($rowsH = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){
        if($pais == 'VE' ){
            if($regimen == 1)
                $monto = $rowsH["precio"];
            if($regimen == 2)
                $monto = $rowsH["precio_pension"];
            if($regimen == 3)
                $monto = $rowsH["pension_completa"];
            if($regimen == 4)
                $monto = $rowsH["pension_blue"];
        }else{
            if($regimen == -1)
                $monto = $rowsH["price_sd"];
            else
              $monto = $rowsH["precio"];
        }

        if($rowsH["suplementos"] > 0){
            $ad = substr($combinacion, 0, 1);
            $ni = substr($combinacion, 1);

            if($ni > 0)
                $monto = $monto + (($rowsH["suplementos"] * $ad) + (($rowsH["suplementos"] / 2) * $ni));
            else
                $monto = $monto + ($rowsH["suplementos"] * $ad);
        }
        
    }
    
}

 echo number_format(round($monto), 2, ',', '.');
//echo $regimen;
?>