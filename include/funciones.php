<?php

/* ----------------------------------------------------

Script  bajo los t&eacute;rminos y Licencia

Apache License

Version 2.0, January 2004

https://www.apache.org/licenses/LICENSE-2.0

-------------------------------------------------------- */



function APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name){

    $sql = sprintf("SELECT * FROM hesperia_aperturas ORDER BY id DESC");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    echo'

          <div class="row">

            <div class="page-header">

                <h1 class="text-uppercase" style="margin-top:10px;">PRÓXIMAS APERTURAS</h1>

            </div>';

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no hay información en este apartado.</p></div>';

     return; }

    while ($aperturas=mysqli_fetch_array($result,MYSQLI_ASSOC)){

        echo '

        <div class="col-md-12 cuadro text-justify">

            <h2 class="page-header">'.$aperturas["titulo_apertura"].'</h2>

            <div class="col-md-4">';

        if (!file_exists("img/aperturas/$aperturas[img_apertura]")){

            echo'<img src="https://hoteleshesperia.com.ve/img/aperturas/sin-imagen.png" alt="'.$aperturas["titulo_apertura"].'" class="img-responsive center-block"/>';

        }else{

            echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$aperturas["img_apertura"].'-17"  alt="'.$aperturas["titulo_apertura"].'" class="img-responsive center-block" />';

        }echo'

            </div>

            <div class="col-md-8">

                '.$aperturas["texto_apertura"].'

            </div>

            <hr>

          </div>';

    }

    echo '

           </div>';

    return;

}

function BOLETINES($mysqli,$data,$hostname,$user,$password,$db_name)

{ $cantidad=100;

 if (isset($_GET["primera"]))

 { $primera=htmlentities($_GET["primera"]);

  $pg=htmlentities($_GET["pg"]);

  $temp=$pg;

  settype($primera,integer); }

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"]; }

 if (!isset($_GET["pg"])){ $pg = 1;}

 else {$pg = $_GET["pg"]; }

 if ($temp>=$pg && $primera !=1)

 { $inicial=$pg * $cantidad + 1;  }

 else {  $inicial=$pg * $cantidad - $cantidad; }

 $sql = sprintf("SELECT id FROM hesperia_descarga WHERE tipo = '2'");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $total_boletines=mysqli_num_rows($result);

 $pages=@intval($total_boletines / $cantidad) + 1;

 echo '

<div class="page-header">

    <h3 class="g-prensa text-uppercase">

        Listado de Boletines

    </h3>

</div>';

 $sql = sprintf("SELECT * FROM hesperia_descarga WHERE tipo = '2'

                  ORDER BY fecha DESC LIMIT %s,%s",

                mysqli_real_escape_string($mysqli,$inicial),

                mysqli_real_escape_string($mysqli,$cantidad));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1){

     echo '<br/><div class="text-center">

    <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

    <h2>Disculpe</h2> <p>Actualmente no poseemos boletines disponibles</p></div>';

     return;

 }



 echo "<h4>Descarga nuestros boletines informativos</h4>

            <p>Total de Boletines: <strong>$total_boletines</strong> | P&aacute;gina(s): <strong>$pg / $pages</strong></p>

     <dl>";

 while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 {  $titulo = $rows["titulo"];

  $descripcion = $rows["descripcion"];

  $archivo = $rows["archivo"];

  echo  '

           <dt>'.$titulo.' </dt>

              <dd>'.$descripcion.' <a href="pdf-descarga.php?archivo='.$archivo.'" title="Descargar: '.$titulo.' - '.$descripcion.'" target="_parent">Descargar</a> <i class="fa fa-download"></i></dd>';

 }

 echo'</dl>

<div class="row">

    <div class="col-md-12"> ';

 PAGINACION_BOLETINES($mysqli,$data,$hostname,$user,$password,$db_name,$total_boletines,$pages,$cantidad,$pg);

 echo '

    </div>

</div> ';

 return;

}



function CONDICIONES_RESERVAS($mysqli,$data,$hostname,$user,$password,$db_name){

    echo'

<!-- Modal -->

<div class="modal fade" id="myCondiciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="myModalLabel">TERMINOS Y CONDICIONES DE LA RESERVACIÓN</h4>

      </div>

      <div class="modal-body text-justify">

        <p>

Al realizar la reservación de su estadía en nuestro sitio web, el costo total de la misma indicado por el sistema será debitado de la tarjeta de crédito cuyos datos fueron facilitados por usted en el formulario de reserva, el cual incluye el número de habitaciones, personas a ocupar, la  tipología de habitación y las noches de la estadía.

En caso de que requiera hacer algún cambio en su reservación o cancelar la misma, podrá realizarlo llamando a los teléfonos de reservas o emails de reservas que se indican en el apartado de contactos en la web dónde ha realizado la reserva, como máximo con una anticipación de 48 horas previas a la fecha de estadía.

A la hora de realizar el check in debe contar con los siguientes documentos: Cédula de identidad laminada o pasaporte, localizador de la reserva y la tarjeta de crédito con la que realizó el pago de la reservación a través del sitio web. </p>

Si por algún motivo usted requiere cancelar la reservación, debe hacerlo con un mínimo de 48 horas de anticipación, para reembolso del dinero. En caso de no presentarse el día de la reservación el hotel no se compromete en reembolzar el dinero por concepto de la reservación realizada por nuestra página web.</p><p>

La reservación realizada no es transferible a otro hotel de la cadena Hesperia Venezuela.

Usted debe presentar una tarjeta de crédito al momento de realizar el check in a fin de garantizar los gastos en los que pueda incurrir durante su estadía.</p><p>

En los hoteles de Isla Margarita, existe una política de precios especial para niños. Para la aplicación de la misma, se consideran niños aquellos cuya edad esté comprendida entre 6 y 12 años. Los niños cuya edad esté comprendida entre los 0 y 5 años no incurren en precio alguno, por lo que a la hora de hacer la reserva no deberá indicarlo ni como adulto ni como niño.</p><p>

Al realizar la reservación en nuestro sitio web, usted acepta los términos y condiciones aquí establecidos. Si requiere de mayor información puede escribirnos al correo electrónico reservas@hoteleshesperia.com.ve

        </p>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary btn-sm btn-lg btn-block" data-dismiss="modal">

            Cerrar

        </button>

      </div>

    </div>

  </div>

</div>';

    return;

}



function CAMBIO_CLAVE($mysqli,$data,$hostname,$user,$password,$db_name)

{ 

    $id = $_SESSION["referencia"];

    $sql = sprintf("SELECT * FROM hesperia_usuario WHERE id = '%s'" ,

                   mysqli_real_escape_string($mysqli,$id));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $row=mysqli_fetch_array($result,MYSQLI_ASSOC);

    echo '

<h3>Cambio de clave</h3>

<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" name="MiClave" id="MiClave" onsubmit="return UpdateClave(); return document.MM_returnValue">

  <div class="form-group">

    <label class="sr-only" for="clave_acceso">Clave</label>

    <input type="password" class="form-control" value="" placeholder="Clave" name="clave_acceso" id="clave_acceso">

  </div>

  <div class="form-group">

    <label class="sr-only" for="clave_acceso">Repetir Clave</label>

    <input type="password" class="form-control" value="" placeholder="Repetir Clave" name="rclave" id="rclave">

  </div>';

      if($row["pregunta_1"] == ''){

        $sqlPreg = sprintf("SELECT * FROM hesperia_v2_preguntas_seg WHERE  status = 'S'");

        $resultPreg = QUERYBD($sqlPreg,$hostname,$user,$password,$db_name);

        $i = 0;

        while ($rowsPreg=mysqli_fetch_array($resultPreg,MYSQLI_ASSOC)){

            $pregunta[$i] = $rowsPreg['pregunta'];

            $i++;

        }

        echo '

                <div class="form-group">

                    <center><label>Seleccione y responda a las siguiente preguntas de seguridad</label></center><br>

                        <select name="pregunta1" id="pregunta1"  class="form-control input-sm">

                            <option value="" selected>Seleccione:</option>';

                            for ($i=0; $i < 3; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                </div>

                <div class="form-group">

                        <input type="text" class="form-control" value="" id="respuesta1" name="respuesta1" >

                    </div>

                <div class="form-group">        

                        <select name="pregunta2" id="pregunta2"  class="form-control input-sm" >

                            <option value="" selected>Seleccione:</option>';

                            for ($i=3; $i < 6; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                </div>

                <div class="form-group">

                        <input type="text" class="form-control" value="" id="respuesta2" name="respuesta2" >

                    </div>

                <div class="form-group"> 

                        <select name="pregunta3" id="pregunta3"  class="form-control input-sm">

                            <option value="" selected>Seleccione:</option>';

                            for ($i=6; $i < 9; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                </div>

                    <div class="form-group">

                        <input type="text" class="form-control" value="" id="respuesta3" name="respuesta3" >

                    </div>

                <br>



                <input type="hidden" name="preguntas" value="0"/>

                    

        ';

      }else{

        echo '

            <div class="form-group">

                <label for="respuesta1">'.$row["pregunta_1"].'</label>

                <input type="text" class="form-control" value="" id="respuesta1" name="respuesta1" >

            </div>

            <div class="form-group">

                <label for="respuesta2">'.$row["pregunta_2"].'</label>

                <input type="text" class="form-control" value="" id="respuesta2" name="respuesta2" >

            </div>

            <div class="form-group">

                <label for="respuesta3">'.$row["pregunta_3"].'</label>

                <input type="text" class="form-control" value="" id="respuesta3" name="respuesta3" >

            </div>

            <input type="hidden" name="preguntas" value="1"/>

        ';

      }

      echo '

      <div id="recaptcha2"></div>

  <div class="form-group">

    <div id="MiCLaveperfil"></div>

  </div>

  <button type="submit" class="btn btn-primary btn-large btn-block">Cambiar Clave</button>

</form>';

 return;

}

function CARROUSEL($mysqli,$data,$hostname,$user,$password,$db_name,$go){

    if ($_GET['principio']) { $_POST["hotel"] = $_GET["sucursal"]; }

    if (!isset($_POST["hotel"])) {

        $sql = "SELECT promo_id,nombre,imagen, texto,idhotel FROM hesperia_headers

                 WHERE estatus = '1' && ubicacion = '1' ORDER BY RAND() LIMIT 0,5"; }

    else { $hotel = $_POST["hotel"];

          $_GET["sucursal"] = $_POST["hotel"];

          //        echo $_GET["sucursal"];

          $sql = "SELECT promo_id,nombre,imagen, texto,idhotel FROM hesperia_headers

                 WHERE estatus = '1' && idhotel = '$hotel'

                 ORDER BY RAND() LIMIT 0,5"; }

    if($go == 'eventos' || $go == 'experiencias' || $go == 'paquetes'  || $go == 'aperturas' || (isset($_GET["id"]) && $go = $_GET["id"])){

        switch ($go){

            case 'eventos':

                $ubicacion = 4;

                break;

            case 'experiencias':

                $ubicacion = 3;

                break;

            case 'paquetes':

                $ubicacion = 2;

                break;

            case 'aperturas':

                $ubicacion = 8;

                break;

            case '1':

                $ubicacion = 5;

                break;

            case '3':

                $ubicacion = 6;

                break;

            case '4':

                $ubicacion = 7;

                break;

        }

        $sql = "SELECT promo_id,nombre,imagen,texto,idhotel

                FROM hesperia_headers

                WHERE estatus = '1'  && ubicacion = '$ubicacion'

                ORDER BY RAND() LIMIT 0,5";

//                print_r($go);

//        die($sql);

    }

//    die();

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if($hay == 0){

        $sql = "SELECT promo_id,nombre,imagen, texto,idhotel FROM hesperia_headers

                 WHERE estatus = '1' && ubicacion = '1' ORDER BY RAND() LIMIT 0,5";

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

    }

    $empieza = 0;

    echo '

    <header>

    <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">';

    if ($hay > 0) {

        echo '<ol class="carousel-indicators hidden-xs hidden-sm">';

        for ($empieza = 0; $empieza < $hay; $empieza++) {

            if ($empieza== 0) {

                echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>'; }

            else { echo '<li data-target="#myCarousel" data-slide-to="'.$empieza.'"></li>'; }

        } echo '</ol>';

    }

    echo '<div class="carousel-inner">';

    $empieza = 0;

    if ($hay > 0) {

        while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

        {  if ($empieza== 0) {

            echo '<div class="item active">'; }

         else { echo '<div class="item">'; }

         $empieza++;

         $idhoteltem =

             $sqlH = sprintf("SELECT id,latitud,longitud FROM hesperia_settings WHERE id = '$rows[idhotel]'");

         $resultH = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

         if ($rowsH = mysqli_fetch_array($resultH,MYSQLI_ASSOC))

         { $temperatura = @number_format(CLIMA($rowsH["id"],$mysqli,$hostname,$user,$password,$db_name), 2, '.', ''); }

         else { $temperatura = @number_format(CLIMA($data["id"],$mysqli,$hostname,$user,$password,$db_name), 2, '.', ''); }

         echo '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rows['imagen'].'-1" style="width:100%" alt="'.$rows["nombre"].'" />

            <div class="carousel-caption hidden-lg hidden-md">

                <h3 class="main-text">'.$rows["texto"].'</h3>

                <p class=" visible-lg visible-md"><strong>Temperatura '.$temperatura.'°</strong></p>

            </div>

            <div class="carousel-caption visible-lg visible-md move-left">

                <h3 class="main-text">'.$rows["texto"].'</h3>';



         if($go == 'eventos' || $go == 'experiencias' || $go == 'paquetes'  || $go == 'aperturas' || $go = isset($_GET["id"])){

             switch ($go){

                 case 'eventos':

                     echo '';

                     break;

                 case 'experiencias':

                     echo '';

                     break;

                 case 'paquetes':

                     echo '';

                 case 'aperturas':

                     echo '';

                     break;

                 case '1':

                     echo '';

                     break;

                 case '3':

                     echo '';

                     break;

                 case '4':

                     echo '';

                     break;

             }

         }else{

             echo'<p class=""><strong>Temperatura '.$temperatura.'°</strong></p>';

         }

         echo'

             </div>

             </div>';

        }

    }

    echo '</div>

             <div id="my-reserva" class="panel reset-reserva panel-reserva over-slider ancho-reserva pull-right visible-lg visible-md">';

        CREAR_RESERVA($mysqli,$hostname,$user,$password,$db_name);

    echo'

             </div>

             </div>

             </header>';

    return;

}



function CREAR_RESERVA($mysqli,$hostname,$user,$password,$db_name){

?>

<script type="text/javascript">

var mobile = (/iPhone|iPad|iPod|android|blackberry|bb|mini|Nokia|PlayBook|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

if (mobile) {

    $( "#my-reserva" ).remove(); // delete reservation form

}

function pagoOnChange(sel) {

    if (sel.value!=4 || sel.value=='' || sel.value==0){

       if(document.getElementById('member').value > 0){

            $("#tolNin").show();

        }

        $(".nCuenta").show();

    }else{

        $("#tolNin").hide();

        $(".nCuenta").hide();

    }

}

//

// add new select form

//

function addFields() {

    var number = document.getElementById("member").value,

        cont = document.getElementById("dem"),

        i;

    var hotel = document.getElementById("hotel").value;



    while (cont.hasChildNodes()) {

        cont.removeChild(cont.lastChild);

    }



    if(number == ''){

        $("#tolNin").hide();

    }else{

        if (hotel!=4 || hotel=='' || hotel==0){

            $("#tolNin").show();

        }

    }



    for (i = 0; i < number; i++) {

        var ni = document.getElementById('dem'),

            num = i,

            newdiv = document.createElement('div');

        if (hotel!=4 || hotel=='' || hotel==0){

          newdiv.innerHTML = '<div class="row"><div class="col-md-6"><label>Adultos</label><select class="form-control input-sm" id="adultos" name="adultos_' + num + '"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div id="nCuenta" class="nCuenta"><div class="col-md-6 nin"><label>Niños</label><select class="form-control input-sm" id="ninos" name="ninos_' + num + '"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option></select></div></div></div>';

        }else{

          newdiv.innerHTML = '<div class="row"><div class="col-md-6"><label>Adultos</label><select class="form-control input-sm" id="adultos" name="adultos_' + num + '"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div id="nCuenta" class="nCuenta" style="display: none;"><div class="col-md-6 nin"><label>Niños</label><select class="form-control input-sm" id="ninos" name="ninos_' + num + '"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option></select></div></div></div>';

        }

        ni.appendChild(newdiv);

    }

}



function validarPrereserva(){

   var hotel = document.getElementById('hotel').value;

    if(hotel == '0'){

        alert('Error: Indique hotel');

        return false;

    }



    dpd1 = document.getElementById('dpd1').value;

    dpd2 = document.getElementById('dpd2').value;



    if(dpd1 == '' || dpd1 == '0'){

        alert('Error: Debe indicar fecha de check in');

        return false;

    }



    if(dpd2 == '' || dpd2 == '0'){

        alert('Error: Debe indicar fecha de check out');

        return false;

    }



    dpd1 = dpd1.split('/');

    dpd2 = dpd2.split('/');

    var dateStart=new Date(dpd1[2],(dpd1[1]-1),dpd1[0]);

    var dateEnd=new Date(dpd2[2],(dpd2[1]-1),dpd2[0]);



    if(dateStart > dateEnd){

        alert('Error: Fecha de check in debe ser menor que fecha de check out');

        return false;

    }



    var number = document.getElementById("member").value;



    if(number > 0){

        if ( $("#adultos").length > 0 ) {

            return true;

        }else{

            alert('Debe indicar al menos un adulto');

            return false;

        }

    }else{

        alert('Debe indicar número de habitaciones');



        return false;

    }

}

</script>

<?php

    echo '

    <div class="panel-heading text-center">

        <h3 class="panel-title">Reservaciones</h3>

        <small>Mejor precio garantizado</small>

    </div>

    <div class="panel-body">

        <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-habitacion/proceso-reservacion" role="form" method="post" name="FormReservacion" id="FormReservacion" class="reserva" onsubmit="return validarPrereserva(); return document.MM_returnValue">';

    if (isset($_POST["hotel"])) {

        echo '<input type="hidden" name="hotel" value="'.$_POST["hotel"].'"/>'; }

    echo '<div class="form-group form-group-sm">

    <label>Hotel</label>

                    <select class="form-control input-sm" name="hotel" id="hotel" onChange="pagoOnChange(this)">';

    if (!isset($_POST["hotel"])) {

        echo'<option value="0">Seleccionar Hotel</option>'; }

    $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    if (isset($_POST["hotel"])) { $_GET['sucursal'] = $_POST["hotel"];  }

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        if ($rows["id"] == $_GET['sucursal']) {

            echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';

        }else{

            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';

        }

    }

    echo '</select>';

    if (!isset($_POST["dpd1"])) {

        $fecha_inicio = date("d/m/Y",time()); }

    else { $fecha_inicio = $_POST["dpd1"]; }

    if (!isset($_POST["dpd2"])) {

        $fecha_fin = date("d/m/Y",time()+86400); }

    else { $fecha_fin = $_POST["dpd2"]; }

    echo '

                <label>Check in </label>

                <input type="text" class="form-control input-sm" value="'.$fecha_inicio.'" id="dpd1" name="dpd1" placeholder="Entrada" data-date-format="dd/mm/yyyy" language="es">

                <label>Check out </label>

                <input type="text" class="form-control input-sm" value="'.$fecha_fin.'" id="dpd2" name="dpd2" placeholder="Salida" data-date-format="dd/mm/yyyy" language="es">

                <label>Habitaciones</label>

                <select class="form-control input-sm" id="member" name="member" onchange="addFields()">

                    <option value="">A reservar:</option>

                    <option value="1">1</option>

                    <option value="2">2</option>

                    <option value="3">3</option>

                    <option value="4">4</option>

                </select>

                <div class="row"><div class="col-md-6">&nbsp;</div><div class="col-md-6" ><i style="display: none;" id="tolNin" class="fa fa-question-circle" tabindex="0" role="button" data-toggle="popover" data-trigger="hover focus" data-placement="bottom" data-trigger="focus" data-content="Son considerados niños de 6 a 12 años"></i></div></div>

                <div id="dem"></div>

            </div>

            <button type="submit" id="botonConsulta" class="btn btn-primary btn-sm btn-lg btn-block" >Reserva ahora</button>

        </form>

    </div>';

    return;

}

//

// Se elimina contenido de la funcion CREAR_RESERVA_PAGO

// Esta funcion era invocada desde la opcion REINICIAR RESERVACIÓN

// En la seccion de seleccion de habitaciones

// El contenido se encuentra en un archivo llamado ELIMINADO

//

function CREAR_RESERVA_PAGO($mysqli,$hostname,$user,$password,$db_name){

    echo '';

    return;

}

function CREAR_OTRA_RESERVA($mysqli,$hostname,$user,$password,$db_name){

//

//

//

// hay que agregar un div de clase panel panel-default panel-reservados

// ese div con esas clases es para darle la forma de cajon tal cual como el slider

// luego hay que agregar esta clase panel-heading panel-heading-reservados text-center

// al div que corresponde al titulo debe quedar de la siguiente forma

//<div class="panel-heading panel-heading-reservados text-center">

//    <h3 class="panel-title">Reservaciones</h3>

//    <small>Mejor precio garantizado</small>

//</div>

// esto es para mantener la estructura actual

//

?>

<script type="text/javascript">



function pagoOnChange(sel) {

    if (sel.value!=4 || sel.value=='' || sel.value==0){

        if(document.getElementById('member').value > 0){

            $("#tolNin").show();

        }

        $(".nCuenta").show();

    }else{

        $("#tolNin").hide();

        $(".nCuenta").hide();

    }

}

//

// add new select form

//

function addFields() {

    var number = document.getElementById("member").value,

        cont = document.getElementById("dem"),

        i;

    var hotel = document.getElementById("hotel").value;



    while (cont.hasChildNodes()) {

        cont.removeChild(cont.lastChild);

    }



    if(number == ''){

        $("#tolNin").hide();

    }else{

        if (hotel!=4 || hotel=='' || hotel==0){

            $("#tolNin").show();

        }

    }



    for (i = 0; i < number; i++) {

        var ni = document.getElementById('dem'),

            num = i,

            newdiv = document.createElement('div');

        if (hotel!=4 || hotel=='' || hotel==0){

          newdiv.innerHTML = '<div class="row"><div class="col-md-6"><label>Adultos</label><select class="form-control input-sm" id="adultos" name="adultos_' + num + '"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div id="nCuenta" class="nCuenta"><div class="col-md-6 nin"><label>Niños</label><select class="form-control input-sm" id="ninos" name="ninos_' + num + '"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option></select></div></div></div>';

        }else{

          newdiv.innerHTML = '<div class="row"><div class="col-md-6"><label>Adultos</label><select class="form-control input-sm" id="adultos" name="adultos_' + num + '"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div id="nCuenta" class="nCuenta" style="display: none;"><div class="col-md-6 nin"><label>Niños</label><select class="form-control input-sm" id="ninos" name="ninos_' + num + '"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option></select></div></div></div>';

        }

        ni.appendChild(newdiv);

    }

}

function validarPrereserva(){

    

    var hotel = document.getElementById('hotel').value;

    if(hotel == '0'){

        alert('Error: Indique hotel');

        return false;

    }



    dpd1 = document.getElementById('dpd1').value;

    dpd2 = document.getElementById('dpd2').value;



    if(dpd1 == '' || dpd1 == '0'){

        alert('Error: Debe indicar fecha de check in');

        return false;

    }



    if(dpd2 == '' || dpd2 == '0'){

        alert('Error: Debe indicar fecha de check out');

        return false;

    }

    dpd1 = dpd1.split('/');

    dpd2 = dpd2.split('/');

    var dateStart=new Date(dpd1[2],(dpd1[1]-1),dpd1[0]);

    var dateEnd=new Date(dpd2[2],(dpd2[1]-1),dpd2[0]);



    if(dateStart > dateEnd){

        alert('Error: Fecha de check in debe ser menor que fecha de check out');

        return false;

    }



    var number = document.getElementById("member").value;



    if(number > 0){

        if ( $("#adultos").length > 0 ) {

            return true;

        }else{

            alert('Debe indicar al menos un adulto');

            return false;

        }

    }else{

        alert('Debe indicar número de habitaciones');



        return false;

    }

}

</script>

<?php



    echo '

    <div class="panel reset-reserva panel-reservados">

    <div class="panel-heading panel-heading-reservados text-center">

        <h3 class="panel-title">Reservaciones</h3>

        <small>Mejor precio garantizado</small>

    </div>

    <div class="panel-body">

        <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-habitacion/proceso-reservacion" role="form" method="post" name="FormReservacion" id="FormReservacion" class="reserva" onsubmit="return validarPrereserva(); return document.MM_returnValue">';

    if (isset($_POST["hotel"])) {

        echo '<input type="hidden" name="hotel" value="'.$_POST["hotel"].'"/>'; }

    echo '<div class="form-group form-group-sm">

    <label>Hotel</label>

                    <select class="form-control input-sm" name="hotel" id="hotel" onChange="pagoOnChange(this)">';

    if (!isset($_POST["hotel"])) {

        echo'<option value="0">Seleccionar Hotel</option>'; }

    $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    if (isset($_POST["hotel"])) { $_GET['sucursal'] = $_POST["hotel"];  }

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        if ($rows["id"] == $_GET['sucursal']) {

            echo '<option value="'.$rows["id"].'" selected>'.$rows["nombre_sitio"].'</option>';

        }else{

            echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';

        }

    }

    echo '</select>';

    if (!isset($_POST["dpd1"])) {

        $fecha_inicio = date("d/m/Y",time()); }

    else { $fecha_inicio = $_POST["dpd1"]; }

    if (!isset($_POST["dpd2"])) {

        $fecha_fin = date("d/m/Y",time()+86400); }

    else { $fecha_fin = $_POST["dpd2"]; }

    echo '

                <label>Check in </label>

                <input type="text" class="form-control input-sm" value="'.$fecha_inicio.'" id="dpd1" name="dpd1" placeholder="Entrada" data-date-format="dd/mm/yyyy" language="es">

                <label>Check out </label>

                <input type="text" class="form-control input-sm" value="'.$fecha_fin.'" id="dpd2" name="dpd2" placeholder="Salida" data-date-format="dd/mm/yyyy" language="es">

                <label>Habitaciones</label>

                <select class="form-control input-sm" id="member" name="member" onchange="addFields()">

                    <option value="">A reservar:</option>

                    <option value="1">1</option>

                    <option value="2">2</option>

                    <option value="3">3</option>

                    <option value="4">4</option>

                </select>

                <div class="row"><div class="col-md-6">&nbsp;</div><div class="col-md-6" ><i style="display: none;" id="tolNin" class="fa fa-question-circle" tabindex="0" role="button" data-toggle="popover" data-trigger="hover focus" data-placement="bottom" data-trigger="focus" data-content="Son considerados niños de 6 a 12 años"></i></div></div>

                <div id="dem"></div>

            </div>

            <button type="submit" id="botonConsulta" class="btn btn-primary btn-sm btn-lg btn-block" onsubmit="return validarPrereserva();">Reserva ahora</button>

        </form>

    </div>

    </div>';

}



function CREAR_CLIMA($mysqli,$hostname,$user,$password,$db_name)

{	$ahora = time();

 $temperatura = 1;

 $archivo = 'cache-clima/clima.php';

 $sql = sprintf("SELECT id,latitud,longitud FROM hesperia_settings");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $fp = fopen($archivo, 'a+');

 while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 {	$url = "http://api.openweathermap.org/data/2.5/weather?lat=$rows[latitud]&lon=$rows[longitud]&units=metric&APPID=7676708d5f7b9472f81a070d265c4502";

  $clima = @file_get_contents("$url");

  $json = json_decode($clima);

  $temperatura = $json->main->temp;

  $contenido = $rows["id"].'='.$temperatura;

  fwrite($fp, $contenido);

  fwrite($fp,"\n");

 }

 fclose($fp);

 return;

}



function CLIMA($id,$mysqli,$hostname,$user,$password,$db_name)

{ 	$ahora = time();

 $archivo = 'cache-clima/clima.php';

 if (!file_exists ($archivo)) {

     $gestor = fopen($archivo, "w");

     fclose($gestor);

     CREAR_CLIMA($mysqli,$hostname,$user,$password,$db_name);

 } else

 {  $tiempoarchivo = filectime ($archivo) + 43200 ;

  if ( $tiempoarchivo <= $ahora)

  { 	unlink($archivo);

   $gestor = fopen($archivo, "w");

   fclose($gestor);

   CREAR_CLIMA($mysqli,$hostname,$user,$password,$db_name); }

 }

 $temperatura = LEER_CLIMA($id);

 return $temperatura;

}



function DETALLE_HOTEL($mysqli,$hostname,$user,$password,$db_name,$data){

    $sql = sprintf("SELECT * FROM hesperia_servicios WHERE  id_hotel = '%s' limit 1",

                   mysqli_real_escape_string($mysqli,$data["id"]));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rowServicio=mysqli_fetch_array($result,MYSQLI_ASSOC);

    $sql = sprintf("SELECT video_promocion FROM hesperia_video WHERE activo = '1'

                && id_hotel = '%s' ORDER BY RAND()",

                   mysqli_real_escape_string($mysqli,$data["id"]));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $video = '';

    if ($row=mysqli_fetch_array($result,MYSQLI_ASSOC))

    { $video = $row["video_promocion"]; }

    echo '

<div class="row">

    <div class="col-lg-12">

        <div role="tabpanel" class="page-panel">

            <!-- Nav tabs -->

            <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">

                    <a href="#informacion" aria-controls="informacion" role="tab" data-toggle="tab">Informaci&oacute;n</a>

                </li>

                <li role="presentation">

                    <a href="#habitaciones" aria-controls="habitaciones" role="tab" data-toggle="tab">Habitaciones</a>

                </li>';

   /* if ($data["eventos"] == 1) {

        echo '

                <li role="presentation">

                    <a href="#eventos" aria-controls="eventos" role="tab" data-toggle="tab">Eventos</a>

                </li>';

    }*/

    echo '<li role="presentation">

                    <a href="#paquetes" aria-controls="paquetes" role="tab" data-toggle="tab">Paquetes</a>

                </li>';

    if ($data["restaurant"] == 1) {

        echo '

                <li role="presentation">

                    <a href="#gastronomia" aria-controls="gastronomia" role="tab" data-toggle="tab">Gastronomía</a>

                </li>';

    }

   /* echo '

                <li role="presentation">

                    <a href="#experiencias" aria-controls="experiencias" role="tab" data-toggle="tab">Experiencias</a></li>';
                    */

     echo'           <li role="presentation">

                    <a href="#galeria"  id="galeriaTab" aria-controls="galeria" role="tab" data-toggle="tab">Galer&iacute;a</a></li>

                



                ';

    if ($data["salones"] == 1) {

        echo '

                <li role="presentation">

                    <a href="#salones" aria-controls="salones" role="tab" data-toggle="tab">Salones</a></li>';

    }

    echo '

  



            </ul>';

    echo '

            <!-- Tab panes -->

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade in active" id="informacion">';

    TAB_INFORMACION($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio,$video);

    echo '      </div>';

    echo '

                <div role="tabpanel" class="tab-pane fade" id="habitaciones">';

    TAB_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio);

    echo '

                </div>';


    echo '<div role="tabpanel" class="tab-pane fade" id="gastronomia">';

    LISTANDO_RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio);

    echo '		    </div>';

    /*echo'            <div role="tabpanel" class="tab-pane fade" id="experiencias">';

    OTROS_SERVICIOS($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio);

    echo '		    </div>*/

      echo'       <div role="tabpanel" class="tab-pane fade" id="galeria">';

    //GALERIA($mysqli,$data,$hostname,$user,$password,$db_name);
    GALERIA2($mysqli,$data,$hostname,$user,$password,$db_name);

    echo '

                </div>

                

                <div role="tabpanel" class="tab-pane fade" id="salones">';

    SALONES_LISTADO($mysqli,$data,$hostname,$user,$password,$db_name);

    echo '      </div>

                <div role="tabpanel" class="tab-pane fade" id="paquetes">';

            PAQUETES_GENERAL_HOTEL($mysqli,$data,$hostname,$user,$password,$db_name);

            echo '      

            </div>



            </div>

        </div>

    </div>

</div>';

    return;

}



function DETALLES_SERVICIOS($mysqli,$hostname,$user,$password,$db_name,$data)

{ $id = $_GET["id"];

 $id_hotel = $_GET["sucursal"];

 $valor = explode('_',$id);

 $id = $valor[0];

 $campo = $valor[1];

 $titulo = strtoupper($campo);

 $campoL = strtolower('texto_'.$campo);

 $imagen = strtolower('imagen_'.$campo);

 $sql = sprintf("SELECT $campoL,$imagen FROM hesperia_servicios

                    WHERE id_servicios = '%s' && id_hotel = '%s'",

                mysqli_real_escape_string($mysqli,$id),

                mysqli_real_escape_string($mysqli,$id_hotel));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 if ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $imagen = $rows[$imagen];

  echo'

 <div class="row">

           <h2>Servicio: '.$titulo.'</h2>

           <div class="col-md-4">';

  if (!file_exists("img/servicios/$imagen")){



      echo'<img src="https://hoteleshesperia.com.ve/img/servicios/sin-imagen.png" alt="'.utf8_encode($titulo).'" class="img-responsive" title= "Servicio '.utf8_encode($titulo).'"/>';



  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-17" style="width:100%" alt= "Servicio  '.utf8_encode($titulo).'" title= "Servicio  '.utf8_encode($titulo).'"/>';

  }echo'

            </div>

            <div class="col-md-8">

               <span style="text-align:justify">'.utf8_encode($rows[$campoL]).'</span>

               <p><a href="javascript:history.back()">Regresar a página anterior</a></p>

              </div>

            </div><br/>';

 }

 return;

}

function DETALLES_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name){

    $sql = sprintf("SELECT * FROM hesperia_detalles_eventos ORDER BY id DESC");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-3 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no hay contenido para esta sección</p></div>';

     return; }

    while ($rowsDetalle = mysqli_fetch_array($result,MYSQLI_ASSOC)){

        echo'

        <div class="col-md-12 cuadro text-justify">

            <h2 class="page-header">'.$rowsDetalle["titulo_detalle"].'</h2>

            <div class="col-md-4">';

        if (!file_exists("img/eventos/$rowsDetalle[img_detalle]")){

            echo'<img src="https://hoteleshesperia.com.ve/img/eventos/sin-imagen.png"  alt="'.$rowsDetalle["titulo_detalle"].'" class="img-responsive"/>';

        }else{

            echo'<img src="https://hoteleshesperia.com.ve/img/eventos/'.$rowsDetalle["img_detalle"].'" alt=" '.$rowsDetalle["titulo_detalle"].'" class="img-responsive"/>';

        } echo'

            </div>

            <div class="col-md-8">

                '.$rowsDetalle["descripcion_detalle"].'

                </br>

            </div>

            <hr>

        </div>';

    }

    return;

}   

function EVENTOS_HOTELES_GENERAL($mysqli,$data,$hostname,$user,$password,$db_name)

{	 $sql = sprintf("SELECT * FROM hesperia_eventos_extra");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $rowPortada=mysqli_fetch_array($result,MYSQLI_ASSOC);

 echo '<style>.thumbnail {border: 0;}</style>

        <div class="row">

            <div class="col-lg-12">

                <h1 class="page-header text-center page-title page-title-responsive">

                    '.utf8_decode(strtoupper($rowPortada["titulo_principal"])).'

                </h1>

            </div>

            <div class="col-sm-6 col-md-4">

                <div class="thumbnail caja-portada">';

 if (!file_exists("img/eventos/$rowPortada[img_uno]")){

     echo'<img src="https://hoteleshesperia.com.ve/img/eventos/sin-imagen.png"  alt="'.$rowPortada["titulo_uno"].'" class="img-responsive"/>';

 }else{

     echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPortada["img_uno"].'-6"  alt="Portada:'.$rowPortada["titulo_uno"].'" class="img-responsive"/>';

 }

 echo'<div class="caption caja-texto-portada">

                    <h3 class="page-title">'.strtoupper($rowPortada["titulo_uno"]).'</h3>

                    '.utf8_decode($rowPortada["texto_uno"]).'

                  </div>

                </div>

            </div>

            <div class="col-sm-6 col-md-4">

                <div class="thumbnail caja-portada">';

 if (!file_exists("img/eventos/$rowPortada[img_dos]")){

     echo'<img src="https://hoteleshesperia.com.ve/img/eventos/sin-imagen.png"  alt="'.$rowPortada["titulo_dos"].'" class="img-responsive"/>';

 }else{

     echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPortada["img_dos"].'-6"  alt="Portada:'.$rowPortada["titulo_dos"].'" class="img-responsive"/>';

 }

 echo'<div class="caption caja-texto-portada">

                    <h3 class="page-title">'.strtoupper($rowPortada["titulo_dos"]).'</h3>

                    '.utf8_decode($rowPortada["texto_dos"]).'

                  </div>

                </div>

            </div>

            <div class="col-sm-6 col-md-4">

                <div class="thumbnail caja-portada">';

 if (!file_exists("img/eventos/$rowPortada[img_tres]")){

     echo'<img src="https://hoteleshesperia.com.ve/img/eventos/sin-imagen.png"  alt="'.$rowPortada["titulo_tres"].'" class="img-responsive"/>';

 }else{

     echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPortada["img_tres"].'-6"  alt="Portada:'.$rowPortada["titulo_tres"].'" class="img-responsive"/>';

 }

 echo'<div class="caption caja-texto-portada">

                    <h3 class="page-title">'.strtoupper($rowPortada["titulo_tres"]).'</h3>

                    '.utf8_decode($rowPortada["texto_tres"]).'

                  </div>

                </div>

            </div>

            <div class="col-md-12">';

 EVENTOS_CONTACTO($mysqli,$data,$hostname,$user,$password,$db_name);

 echo        '</div>';

 DETALLES_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);

 echo'

        </div><!-- /row -->';

 return;

}

function EVENTOS_CONTACTO($mysqli,$data,$hostname,$user,$password,$db_name){

    echo '

<h2 class="page-header text-uppercase">planifica tu evento</h2>

<form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" name="EventoContacto" id="EventoContacto" onsubmit="return ContactoEvent(); return document.MM_returnValue">

<div class="col-md-3">

  <div class="form-group">

    <label for="hoteles">¿Dónde quieres celebrar tu evento?</label>';

    $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    echo'

    <select class="form-control" name="id_hotel" id="id_hotel" required>

    <option value="" selected>Seleccione</option>';

    if (isset($_POST["hotel"])) { $_GET['sucursal'] = $_POST["hotel"]; 	}

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        echo '<option value="'.$rows["id"].'">'.$rows["nombre_sitio"].'</option>';

    }

    echo'       </select>

  </div>

</div>

<div class="col-md-5">

  <div class="form-group">

    <label for="necesitas">¿Qué necesitas y para cuántos?</label></br>

    <div class="col-md-4">

    <input type="number" class="form-control" name="asistentes" id="asistentes" placeholder="Asistentes" required>

    </div>

    <div class="col-md-4">

    <input type="number" class="form-control" name="salones" id="salones" placeholder="Salones" required>

    </div>

    <div class="col-md-4">

    <input type="number" class="form-control" name="habitaciones" id="habitaciones" placeholder="Habitaciones" required>

    </div>

  </div>

</div>

<div class="col-md-2">

  <div class="form-group">

    <label for="hoteles">¿Qué tipo de evento?</label>

    <select class="form-control" name="tipo_evento" id="tipo_evento" required>

      <option value="" selected>Seleccione</option>

      <option value="Reunión de negocios">Reunión de negocios</option>

      <option value="Congresos, convenciones, exposiciones">Congresos, convenciones, exposiciones</option>

      <option value="Incentivos">Incentivos</option>

      <option value="Formación">Formación</option>

      <option value="Bodas / Eventos Sociales">Bodas / Eventos Sociales</option>

      <option value="Otros">Otros</option>

    </select>

  </div>

</div>

<div class="col-md-2"><br/>

  <button type="button" class="btn btn-primary btn-sm btn-lg btn-block m-button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Agregar Datos de Contacto</button><br>

</div>

    <div class="row">

        <div class="col-md-12">

            <div class="collapse" id="collapseExample">

                <div class="col-md-4">

                    <div class="form-group">

                        <label for="nombres">Nombres y Apellidos</label>

                <input type="text" class="form-control" name="names" id="names" placeholder="Nombres y Apellidos" required>

                    </div>

                </div>

                <div class="col-md-3">

                    <div class="form-group">

                        <label for="Email">Dirección de Correo</label>

    <input type="email" class="form-control" name="correo_contacto" id="correo_contacto" placeholder="Dirección de Correo" required>

                    </div>

                </div>

                <div class="col-md-3">

                    <div class="form-group">

                        <label for="telefono">Teléfono de Contacto</label>

    <input type="text" class="form-control" name="telefono_contacto" id="telefono_contacto" placeholder="Teléfono de Contacto" required>

                    </div>

                </div>

                <div class="col-md-2"><br>

                    <button type="submit" class="btn btn-success btn-sm btn-lg btn-block m-button">

                        <i class="fa fa-paper-plane"></i>

                        Enviar

                    </button><br>

                </div>

                <div id="RespuestaContactoEvento"></div>

            </div>

        </div>

    </div>

</form>

';

    return;

}

function EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name){

    echo '

          <div class="row">

          <div class="page-header">

            <h1 class="text-uppercase" style="margin-top:10px;">Disfruta de Nuevas Experiencias</h1>

        </div>';

    $sql = sprintf("SELECT * FROM hesperia_experiencias ORDER BY fecha DESC");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no poseemos experiencias disponibles</p></div>';

     return; }

    while ($rowExp=mysqli_fetch_array($result,MYSQLI_ASSOC))

    { $nombre_expe = $rowExp["nombre_expe"];

     $asunto = str_replace(' ','+',$nombre_expe);

     echo '

        <div class="col-md-12 cuadro text-justify">

            <h2 class="text-uppercase page-header"> '.$rowExp["nombre_expe"].'</h2>

            <div class="col-md-4">';

     if (!file_exists("img/experiencias/$rowExp[img_expe]")){

         echo'<img src="https://hoteleshesperia.com.ve/img/experiencias/sin-imagen.png" alt="'.$nombre_expe.'" class="img-responsive"/>';

     }else{

         echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowExp["img_expe"].'-10"  alt="'.$nombre_expe.'" class="img-responsive"/>';

     }echo'

            </div>

            <div class="col-md-8">

            '.$rowExp["descripcion_expe"].'<br/>

               <small class="pull-right">

                <a href="https://hoteleshesperia.com.ve/contacto/'.toAscii($nombre_expe).'" data-toggle="tooltip" data-placement="bottom" title="Solicitar más Información de esta Experiencia" class="btn btn-primary" target="_self"> Información y reservas.</a></small>

              </div>

            <hr>

        </div>';

    }

    echo '

           </div>';

    return;

}



function FOOTER($mysqli,$data,$hostname,$user,$password,$db_name,$go){

    $sql = "SELECT id,nombre_categoria FROM hesperia_categoria ORDER BY id ASC";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    echo '

        <footer class="separador">

            <div class="container">

<div class="row">

                <div class="col-lg-12">

                    <div class="row">

                      <div class="col-md-3 col-sm-6">

                        <h3>Hesperia Venezuela</h3>

                        <ul>
                         ';

    while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

    {$rows["nombre_categoria"] = utf8_encode($rows["nombre_categoria"]);

     echo '<li>';

     echo '<a href="https://hoteleshesperia.com.ve/textos/'.$rows["id"].'-'.toAscii($rows["nombre_categoria"]).'" title="'.$rows["nombre_categoria"].'">'.$rows["nombre_categoria"].'</a></li>';

    }

    echo '

                            <li>

                                <a href="https://hoteleshesperia.com.ve/nosotros">

                                    Nosotros

                                </a>

                            </li>

                            <li>

                                <a href="https://hoteleshesperia.com.ve/aperturas">

                                    Próximas Aperturas

                                </a>

                            </li>

                            

                            <li>

                                <a href="https://hoteleshesperia.com.ve/trabajo">

                                    Trabaja con nosotros

                                </a>

                            </li>

                            <li>

                                <a href="https://hoteleshesperia.com.ve/contacto/'.null.'">

                                    Contacto

                                </a>

                            </li>

                            <li style="visibility: collapse;">
                               <a href="https://hoteleshesperia.com.ve/partai" title="Partai Margarita">Partai - Margarita Weekend</a>
                            </li>

                        </ul>

                      </div>

                      <div class="col-md-3 col-sm-6">

                        <h3>Area de Clientes</h3>

                        <ul>';

    if (!isset($_SESSION["referencia"])){

        echo '

                    <li><a id="modal-433854" href="#modal-container-433854" data-toggle="modal" title="Seci&oacute;n de Usuarios registrados">Mi Cuenta</a>
                    </li>
                    <li><a href="https://hoteleshesperia.com.ve/mediacenter">Media Center</a></li>
                    
                    ';

    }

    else

    { echo '

                            <li>
                              <a href="https://hoteleshesperia.com.ve/perfil/mi-cuenta" title="Ver mi Cuenta">Mi Cuenta</a>
                            </li>
                           
                            ';

    }

    echo '                  

                        </ul>

                      </div>

                      <div class="col-md-3 col-sm-6">

                        <h6 class="stuff">Suscribete a nuestro Newsletter</h6>

                            <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" name="suscripcion" id="suscripcion" onsubmit="return Formsuscripcion(); return document.MM_returnValue">

                          <div class="form-group">

                            <label class="sr-only" for="exampleInputEmail1">Introduzca su email</label>

                            <input type="email" class="form-control" id="email" name="email" placeholder="Introduzca su email">

                            <input type="hidden" name="dominio" id="dominio" value="'.$data["dominio"].'">

                            <input type="hidden" name="contacto" id="contacto" value="'.$data["correo_contacto"].'">

                          </div>

                          <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block">Enviar</button>

                            <div id="Respuestasuscripcion"></div>

                        </form>

                      </div>

                      <div class="col-md-3 col-sm-6">';

    if($go == 'index' ||

       $go == 'nosotros' ||

       $go == 'eventos' ||

       $go == 'experiencias' ||

       $go == 'paquetes' ||

       $go == 'textos' ||

       $go == 'aperturas' ||

       $go == 'boletines' ||

       $go == 'prensa' ||

       $go == 'trabajo' ||

       $go == 'contacto'){

        FOOTER_TRIPADVISOR_DEFAULT($mysqli,$data,$hostname,$user,$password,$db_name);

    }

    if($go == 'index' ||

       $go == 'nosotros' ||

       $go == 'eventos' ||

       $go == 'experiencias' ||

       $go == 'paquetes' ||

       $go == 'textos' ||

       $go == 'aperturas' ||

       $go == 'boletines' ||

       $go == 'prensa' ||

       $go == 'trabajo' ||

       $go == 'contacto'){

        echo'';

    }else{

        if(!empty($data["tripadvisor"])){

            echo'

                        <div class="row">

                            <div class="col-md-12 col-sm-12"><!-- ta-->

                                '.$data["tripadvisor"].'

                               <script src="https://www.tripadvisor.com/WidgetEmbed-certificateOfExcellence?amp;locationId=2065193&amp;uniq=44&amp;year=2015&amp;lang=es_VE&amp;display_version=2"></script>

                               <script src="https://www.tripadvisor.com/WidgetEmbed-rated?amp;locationId=7730156&amp;lang=es_VE&amp;display_version=2&amp;uniq=931"></script>

                               <script src="https://www.tripadvisor.com/WidgetEmbed-excellent?amp;locationId=316888&amp;lang=es_VE&amp;display_version=2&amp;uniq=970"></script>

                               <script src="https://www.tripadvisor.com/WidgetEmbed-excellent?amp;locationId=316990&amp;lang=es_VE&amp;display_version=2&amp;uniq=660"></script>

                            </div>';

            echo'

                        </div>';

        }

    }

    if($go == 'index' ||

       $go == 'nosotros' ||

       $go == 'eventos' ||

       $go == 'experiencias' ||

       $go == 'paquetes' ||

       $go == 'textos' ||

       $go == 'aperturas' ||

       $go == 'boletines' ||

       $go == 'prensa' ||

       $go == 'trabajo' ||

       $go == 'contacto'){

        echo'';

    }else{

        if(!empty($data["tripadvisor_premio"])){

            echo'

                        <div class="row">

                            <div class="col-md-12 col-sm-12"><!-- ta-winner -->

                                '.$data["tripadvisor_premio"].'

                               <script src="https://www.tripadvisor.com/WidgetEmbed-certificateOfExcellence?amp;locationId=2065193&amp;uniq=44&amp;year=2015&amp;lang=es_VE&amp;display_version=2"></script>

                               <script src="https://www.tripadvisor.com/WidgetEmbed-rated?amp;locationId=7730156&amp;lang=es_VE&amp;display_version=2&amp;uniq=931"></script>

                               <script src="https://www.tripadvisor.com/WidgetEmbed-excellent?amp;locationId=316888&amp;lang=es_VE&amp;display_version=2&amp;uniq=970"></script>

                               <script src="https://www.tripadvisor.com/WidgetEmbed-excellent?amp;locationId=316990&amp;lang=es_VE&amp;display_version=2&amp;uniq=660"></script>

                            </div>';

            echo'

                        </div>';

        }

    }

    FOOTER_SOCIAL($mysqli,$data,$hostname,$user,$password,$db_name);

    echo'

                        </div>

                    </div>

                </div>

            </div>

            </div>

        </footer>';

    return;

}

function FOOTER_TRIPADVISOR_DEFAULT($mysqli,$data,$hostname,$user,$password,$db_name){

    echo'

<div class="row"><!-- /default -->

    <div class="col-md-12 col-sm-12"><br>

        <div class="center-block">

            <a target="_blank" href="https://static.tacdn.com/Hotel_Review-g316069-d2065193-Reviews-Hesperia_WTC_Valencia-Valencia_Central_Region.html">

                <img src="https://www.tripadvisor.com.ve/img/cdsi/img2/awards/tchotel_2015_LL_TM-11655-2.jpg" class="img-responsive" style="margin:0 auto;">

            </a>

        </div>

    </div>

</div>';

    return;

}

function FOOTER_SOCIAL($mysqli,$data,$hostname,$user,$password,$db_name){

    echo '          <div class="widget-social">

                        <p class="social-text">Siguenos en nuestras redes sociales</p>

                        <ul class="list-inline text-center social-icon">';

    if (!empty($data["url_cuenta_facebook"])) {

        echo '<li><a href="https://www.facebook.com/'.$data["url_cuenta_facebook"].'" target="_blank" title="Facebook"><img src="https://hoteleshesperia.com.ve/img/social/32-facebook.png" class="img-responsive img-hover" alt="facebook"></a></li>';}

    if (!empty($data["url_cuenta_instagram"])) {

        echo '

                                        <li> <a href="https://instagram.com/'.$data["url_cuenta_instagram"].'" target="_blank" title="Instagram"><img src="https://hoteleshesperia.com.ve/img/social/32-instagram.png" class="img-responsive img-hover" alt="instagram"></a></li>';}

    if (!empty($data["url_cuenta_twitter"])) {

        echo '<li><a href="https://twitter.com/'.$data["url_cuenta_twitter"].'" target="_blank" title="Twitter"><img src="https://hoteleshesperia.com.ve/img/social/32-twitter.png" class="img-responsive img-hover" alt="twitter"></a>

</li> ';}

    if (!empty($data["url_cuenta_google"])) {

        echo '<li><a href="https://plus.google.com/u/0/'.$data["url_cuenta_google"].'/posts" target="_blank" title="g+"><img src="https://hoteleshesperia.com.ve/img/social/32-googleplus.png" class="img-responsive img-hover" alt="gPlus"></a></li>'; }

    if (!empty($data["url_cuenta_linkedin"])) {

        echo '<li><a href="https://www.linkedin.com/company/'.$data["url_cuenta_linkedin"].'" target="_blank" title="linkedin"><img src="https://hoteleshesperia.com.ve/img/social/32-linkedin.png" class="img-responsive img-hover" alt="linkedin"></a></li>'; }

    if (!empty($data["url_cuenta_youtube"])) {

        echo '<li><a href="https://www.youtube.com/channel/'.$data["url_cuenta_youtube"].'" target="_blank" title="Youtube"><img src="https://hoteleshesperia.com.ve/img/social/32-youtube-2.png" class="img-responsive img-hover" alt="youtube"></a></li>'; }

    echo '

                        </ul>

                    </div>';

    return;

}

function FORM_CLIENTE($mysqli,$data,$hostname,$user,$password,$db_name)

{ # Formulario de contacto

    $sql = sprintf("SELECT * FROM hesperia_settings");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    echo'

<div class="row">

    <div class="col-lg-12 text-center">

        <h3 class="page-title">FORMULARIO DE CONTACTO</h3>

        <p class="page-header page-title page-title-responsive">

            Nuestros representantes están disponibles para brindarle información sobre nosotros y nuestras sucursales, así como para responder a sus preguntas, realizar reservas y atender tus solicitudes

        </p>

    </div>

</div>

<div class="row">

    <div class="col-lg-12">

        <div class="col-lg-8">

            <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" name="Contacto" id="Contacto" onsubmit="return FormContacto(); return document.MM_returnValue">

            <div id="RespuestaContacto"></div>

            <div class="row">

                <div class="col-md-12">

                    <div class="form-group">

                        <select class="form-control" name="hotel" id="hotel">

                                <option value="0"  selected>Seleccionar Hotel</option>';

    $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";

    $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

    while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC)){

        echo '<option value="'.$rows["nombre_sitio"].'">'.$rows["nombre_sitio"].'</option>';

    }

    echo '</select>

                    </div>

                    <div class="form-group">

                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre y Apellidos" value="'.$_SESSION["nombres"].''.$_SESSION["apellidos"].'">

                    </div>

                    <div class="form-group">

                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Tel&eacute;fono" value="'.$_SESSION["telefono"].'">

                    </div>

                    <div class="form-group">

            <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="'.$_SESSION["email"].'">

                    </div>

                    <div class="form-group">

                        <input type="text" class="form-control" name="pais" autocomplete="off" id="pais" placeholder="Pa&iacute;s">

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <div class="form-group">

                        <textarea class="form-control textarea" rows="3" name="mensaje" id="mensaje" placeholder="Mensaje">';

    

    if (isset($_GET["asunto"])) {

       $asunto = str_replace("-", " ", toAscii($_GET["asunto"]));

    }else{

        $asunto="";

    }

    





    if (!empty($asunto) || !isset($asunto)) { echo $asunto."\n"; }

    echo '</textarea>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <input type="hidden" name="correo_contacto" value="info@hoteleshesperia.com.ve"/>

                    <input type="hidden" name="dominio" value="www.hoteleshesperia.com.ve"/>

                    <button type="submit" class="btn btn-primary pull-right">Enviar Mensajes</button>

                </div>

            </div>

        </form>

        </div><br/>

        <div class="col-lg-4">';

    $hotelesID = $hoteles = array();

    while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

    { echo '<address>

              <abbr title="Name"><strong>'.$rows["razon_social"].'</strong></abbr><br/>

             <abbr title="Address"> '.$rows["direccion"].'<br/>

              '.$rows["ciudad"].', '.$rows["estado"].', '.$rows["pais"].'</abbr><br/>

              <abbr title="Phone">'.$rows["telefonos"].'</abbr><br/>

              <abbr title="Phone">Reservas</abbr>'.$rows["telf_reserva"].'<br/>

              <abbr title="Mail">'.$rows["correo_contacto"].'</a>

            </address>';

     $hotelesID[$rows["id"]] = $rows["id"];

     $hoteles[$rows["id"]] = $rows["razon_social"];

    }

    echo("<p>Para más información: info@hoteleshesperia.com</p>");

    $hay = count($hoteles);

    if (isset($_GET["asunto"]))

    {  $asunto = $_GET["asunto"];

     $asunto = 'Necesito Información sobre: '.str_replace('+',' ',$asunto);

    }

    echo '

        </div>

    </div>

</div>';

    return;

}



function GALERIA($mysqli,$data,$hostname,$user,$password,$db_name)

{     $sql = sprintf("SELECT * FROM hesperia_galeria WHERE  id_hotel = '%s'",

                     mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay imagenes en galería para este Hotel.</p></div>';

  return; }

 echo'<br/><div class="row">';

 $sql = sprintf("SELECT * FROM hesperia_galeria WHERE  id_hotel = '%s'",

                mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $query = QUERYBD($sql,$hostname,$user,$password,$db_name);

 while ($rows = mysqli_fetch_array($query,MYSQLI_ASSOC)) {

     echo'

    <div class="col-xs-6 col-md-3">

        <a href="img/galeria/'.$rows["nombre_imagen"].'" class="thumbnail" data-lightbox="galeria-hotel" data-title="Galeria del hotel '.utf8_decode($data["nombre_sitio"]).'">

            <img src="https://hoteleshesperia.com.ve/img/galeria/'.$rows["nombre_imagen"].'" class="img-responsive" alt="Galeria del hotel '.$data["nombre_sitio"].'">

        </a>

    </div>';

 }

 echo'</div>';

 return;

}

function GALERIA2($mysqli,$data,$hostname,$user,$password,$db_name)

{    

  $sql = sprintf("SELECT * FROM hesperia_galeria WHERE  id_hotel = '%s'
   and ind_mediacenter = 'N' && ind_promo = 'N' ",

                     mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay imagenes en galería para este Hotel.</p></div>';

  return; }else{
    echo "
      <ul class='galeriaSlide'>
    
    ";
    while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
     // var_dump($rows);
   echo " 
      <li><img src='https://hoteleshesperia.com.ve/img/galeria/$rows[nombre_imagen]' alt='".utf8_decode($data["nombre_sitio"])."'></li>
    ";
    }

    echo "</ul>";
  }
}

function GRACIAS_RESERVA(){

    echo '

<div class="row">

    <div class="col-lg-4">

        <div class="panel panel-default">

          <div class="panel-heading">

            <h3 class="panel-title text-center">Hesperia WTC Valencia</h3>

          </div>

          <div class="panel-body">

            <p><strong>16/03/2015</strong> hasta <strong>18/03/2015</strong> </p>

            <p><strong>Personas</strong>: 1 Adulto 2 Niños</p>

            <p><strong>Tipo Habitación:</strong></p>

            <p>Junior Suite</p>

            <p class="bg-primary text-capitalize">TOTAL: 14.850 Bs.F</p>

            <small>Impuestos incluidos</small>

          </div>

        </div>

    </div>

    <div class="col-lg-8">

        <p><img src="https://hoteleshesperia.com.ve/img/gracias_multilingue.jpg" class="img-responsive"></p>

        <p>Confirmación de la reserva nº .... Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>

    </div>

</div>';

    return;

}

function IMAGENES_HABITACION($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio,$rowHab,$carpeta)

{ echo '

<ul class="list-inline">';

 for ($item=1;$item<=3;$item++) {

     $no =0;

     echo '<li>';

     switch($item){

         case '1':

             if (!empty($rowHab["imagen1"])) {

                 if (!file_exists("img/habitaciones/$rowHab[imagen1]")){

                     $no = 1;

                 }else{

                     $imagen = 'img/habitaciones/'.$rowHab["imagen1"];



                     $imagen2 =  '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen1"].'" />';}

             }

             break;

         case '2':

             if (!empty($rowHab["imagen2"])) {

                 if (!file_exists("img/habitaciones/$rowHab[imagen2]")){

                     $no = 1;

                 }else{

                     $imagen = 'img/habitaciones/'.$rowHab["imagen2"];

                     $imagen2 = '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen2"].'" />'; }

             }

             break;

         case '3':

             if (!empty($rowHab["imagen3"])) {

                 if (!file_exists("img/habitaciones/$rowHab[imagen3]")){

                     $no = 1;

                 }else{

                     $imagen = 'img/habitaciones/'.$rowHab["imagen3"];

                     $imagen2 =  '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen3"].'" />'; }

             }

             break;

         case '4':

             if (!empty($rowHab["imagen4"])) {

                 $imagen = 'img/habitaciones/'.$rowHab["imagen4"];

                 $imagen2 =  '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen4"].'" />'; }

             break;

         case '5':

             if (!empty($rowHab["imagen5"])) {

                 if (!file_exists("img/habitaciones/$rowHab[imagen5]")){

                     $no = 1;

                 }else{

                     $imagen = 'img/habitaciones/'.$rowHab["imagen5"];

                     $imagen2 =  '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen5"].'" />';}

             }

             break;

         case '6':

             if (!empty($rowHab["imagen6"])) {

                 if (!file_exists("img/habitaciones/$rowHab[imagen6]")){

                     $no = 1;

                 }else{

                     $imagen = 'img/habitaciones/'.$rowHab["imagen6"];

                     $imagen2 =  '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen6"].'" class="img-responsive" />'; }

             }

             break;

     }



     if ($no == 0){

         if (!file_exists("img/habitaciones/$rowHab[imagen1]")){

             $no = 1;

         }else{

             $imagen2 =  '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-4" class="img-responsive img-thumbnail" alt="'.$carpeta.'-'.$rowHab["imagen6"].'" class="img-responsive" />';

             echo '		<a href="'.$imagen.'" data-lightbox="example-set" data-title="Habitacion: '.$rowHab["nombre_habitacion"].'">'.

                 $imagen2.'

                </a>';

         }

     }

     else {

         //echo '<!-- <a href="img/habitaciones/sin-imagen.png" data-lightbox="example-set" data-title="Habitacion: sin imagen disponible"><img src="https://hoteleshesperia.com.ve/imagen.php?imagen=img/habitaciones/sin-imagen.png-4" class="img-responsive img-thumbnail" alt="Imagen no disponible" /></a> -->';

     }

     echo '</li>';

 }

 echo '

</ul>

';

 return;

}



function INDEX($mysqli,$data,$hostname,$user,$password,$db_name)

{	 $sql = sprintf("SELECT * FROM hesperia_portada limit 1");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $rowPortada=mysqli_fetch_array($result,MYSQLI_ASSOC);

 echo '

        <div class="row">

<div class="col-lg-12">

    <h1 class="page-header text-center page-title page-title-responsive">

        '.$rowPortada["titulo_princial"].'

    </h1>

</div>

<div class="col-sm-6 col-md-4">

    <div class="thumbnail caja-portada">';

 if (!file_exists("img/portada/$rowPortada[imagen_UNO]")){

     echo'<img src="https://hoteleshesperia.com.ve/img/portada/sin-imagen.png"  alt="'.$rowPortada["titulo_UNO"].'" class="img-responsive"/>';

 }else{

     echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPortada["imagen_UNO"].'-12"  alt="Portada:'.$rowPortada["imagen_UNO"].'" class="img-responsive"/>';

 }

 echo'

        <div class="caption caja-texto-portada">

            <h3 class="page-title">'.$rowPortada["titulo_UNO"].'</h3>

            '.utf8_decode($rowPortada["texto_UNO"]).'

            <a href="https://hoteleshesperia.com.ve/experiencias" data-toggle="tooltip" data-placement="bottom" title="'.$rowPortada["titulo_UNO"].'"> Leer más</a>

        </div>

    </div>

</div>

  <div class="col-sm-6 col-md-4">

    <div class="thumbnail caja-portada">';

 if (!file_exists("img/portada/$rowPortada[imagen_DOS]")){

     echo'<img src="https://hoteleshesperia.com.ve/img/portada/sin-imagen.png"  alt="'.$rowPortada["titulo_DOS"].'" class="img-responsive"/>';

 }else{

     echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPortada["imagen_DOS"].'-12"  alt="Portada:'.$rowPortada["imagen_DOS"].'" class="img-responsive"/>';

 }

 echo'<div class="caption caja-texto-portada">

        <h3 class="page-title">'.$rowPortada["titulo_DOS"].'</h3>

        '.utf8_decode($rowPortada["texto_DOS"]).'

        <a href="https://hoteleshesperia.com.ve/paquetes" data-toggle="tooltip" data-placement="bottom" title="'.$rowPortada["titulo_DOS"].'"> Leer más</a>

      </div>

    </div>

  </div>

  <div class="col-sm-6 col-md-4">

    <div class="thumbnail caja-portada">';

 if (!file_exists("img/portada/$rowPortada[imagen_TRES]")){

     echo'<img src="https://hoteleshesperia.com.ve/img/portada/sin-imagen.png"  alt="'.$rowPortada["titulo_TRES"].'" class="img-responsive"/>';

 }else{

     echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPortada["imagen_TRES"].'-12"  alt="Portada:'.$rowPortada["imagen_TRES"].'" class="img-responsive"/>';

 }

 echo'<div class="caption caja-texto-portada">

        <h3 class="page-title">'.$rowPortada["titulo_TRES"].'</h3>

        '.utf8_decode($rowPortada["texto_TRES"]).'

        <a href="https://hoteleshesperia.com.ve/reuniones-eventos" data-toggle="tooltip" data-placement="bottom" title="'.$rowPortada["titulo_TRES"].'"> Leer más</a>

      </div>

    </div>

  </div>

  </div>';

 return;

}



function INICIO_SESSION($data,$hostname,$user,$password,$db_name)

{ echo '

<div class="modal fade" id="modal-container-433854" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content mymod">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h2 class="modal-title" id="myModalLabel"> Acceso al Sistema</h2>

            </div>

            <div class="modal-body">

                <div style="width:98%;margin-left:5px;margin-right:5px">

                    <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" name="form1" id="form1" onsubmit="return GetUser(); return document.MM_returnValue">

                        <div class="form-group">

                            <input type="email" name="username" value= "" id="username" class="form-control" placeholder="Email"/>

                        </div>

                        <div class="form-group">

                            <input type="password" name="clave" value= "" id="clave" class="form-control" placeholder="Password"/>

                        </div>

                        <button type="submit" class="btn btn-primary btn-sm btn-block" id="check" value="Check">Ingresar</button>

                        <input type="hidden" name="auth_token" value="'.CREAR_TOKEN('HOTEL').'"/>

                    <div id="myWatch"></div>

                    </form>

                </div>                

            </div>

        </div>

    </div>

</div>';

 return;

}



function LISTANDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name)

{ 

    $hoy = time();

    $sql = sprintf("SELECT * FROM hesperia_eventos WHERE  id_hotel = '%s' AND fecha_evento >= '%s'",

                 mysqli_real_escape_string($mysqli,$data["id"]),

                 mysqli_real_escape_string($mysqli,$hoy));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);



 $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",

                   mysqli_real_escape_string($mysqli,$data["id"]));

     $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

     $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);

     $hotel = $rowSetting["razon_social"];

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay eventos en este Hotel!.</p></div>';

  return; }

 // echo '

 //    <div class="page-header">

 //        <h3 class="g-prensa text-uppercase">

 //            Próximos eventos en nuestras Instalaciones

 //        </h3>

 //    </div>';

 while ($rowEvento=mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $nombre_paquete = $rowEvento["nombre_evento"];

  $asunto = str_replace(' ','+',$nombre_paquete);

  $desdePaq = date("d/m/Y",$rowEvento["fecha_evento"]);

  $hastaPaq = date("d/m/Y",$rowEvento["fecha_evento"]);

  echo '

  <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >

<div class="col-md-12 cuadro text-justify">

    <h2 class="page-header text-uppercase">'.$rowEvento["nombre_evento"].'</h2>

    <div class="col-md-4">';

  if (!file_exists("img/eventos/$rowEvento[imagen_evento]")){

      echo'<img src="https://hoteleshesperia.com.ve/img/eventos/sin-imagen.png" alt="'.$rowEvento["nombre_evento"].'" class="img-responsive"/>';

  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowEvento["imagen_evento"].'-6"  alt="'.$rowEvento["nombre_evento"].'" class="img-responsive" />';

  }echo'

            </div>

    <div class="col-md-8">

        <ul class="list-unstyled">

            <li>

                <i class="fa fa-info-circle"></i> <strong>Hora:</strong>'.$rowEvento["hora_evento"].'

            </li>

            <li>

                <i class="fa fa-info-circle"></i> <strong>Fecha:</strong> '.date("d/m/Y",$rowEvento["fecha_evento"]).'

            </li>

        </ul>

    '.$rowEvento["texto_evento"].'<br/>';



            if($rowEvento["precio_evento"] > 0){

                if($rowEvento["precio_nino"] > 0){

                        echo '<ul class="list-unstyled">

                                    <li>

                                        <i class="fa fa-info-circle"></i> <strong>Precio por adulto:</strong> '.number_format(round($rowEvento["precio_evento"]), 2, ',', '.').'

                                    </li>

                                    <li>

                                        <i class="fa fa-info-circle"></i> <strong>Precio por niño:</strong> '.number_format(round($rowEvento["precio_nino"]), 2, ',', '.').'

                                    </li>

                             </ul>';

                    }else{

                        echo '<ul class="list-unstyled">

                                    <li>

                                        <i class="fa fa-info-circle"></i> <strong>Precio:</strong> '.number_format(round($rowEvento["precio_evento"]), 2, ',', '.').'

                                    </li>

                             </ul>';

                    }

            echo '

              <input type="hidden" name="nombrePaq" value="'.$rowEvento["nombre_evento"].'"/>

                <input type="hidden" name="precioPaq" value="'.$rowEvento["precio_evento"].'"/>

                <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                <input type="hidden" name="id_hotel" value="'.$rowEvento["id_hotel"].'"/>

                <input type="hidden" name="tiempo" value="0"/>

                <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                <input type="hidden" name="precioNino" value="'.$rowEvento["precio_nino"].'"/>

                <input type="hidden" name="dpd3" value=""/>

                <input type="hidden" name="porcentaje" value="0"/>

                <input type="hidden" name="habitacion" value=""/>';

                if($rowEvento["precio_nino"] > 0){

                   echo'

                   <div class="row">

                            <div class="col-sm-1"><strong>Adultos<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                            <div class="col-sm-1"><strong>Niños<br> 

                                <select name="numPaqN">

                                    <option value="0">0</option>

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                        </div>';

                }else{

                    echo'

                    <div class="row">

                            <div class="col-sm-6"><strong>Cantidad<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                    </div>';

                }



                echo'

                <br>

              <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>';

            }

    echo '

        

    </div>

    <hr>

</div>

</form>';

 }

 return;

}



function LISTANDO_EVENTOS_HOTELES($mysqli,$data,$hostname,$user,$password,$db_name)

{ 

    $hoy = time();

    $sql = sprintf("SELECT * FROM hesperia_eventos WHERE fecha_evento >= '%s'",

                 mysqli_real_escape_string($mysqli,$hoy));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);



 $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",

                   mysqli_real_escape_string($mysqli,$data["id"]));

     $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

     $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);

     $hotel = $rowSetting["razon_social"];

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay eventos en este Hotel.</p></div>';

  return; }

 // echo '

 //    <div class="page-header">

 //        <h3 class="g-prensa text-uppercase">

 //            Próximos eventos en nuestras Instalaciones

 //        </h3>

 //    </div>';

 while ($rowEvento=mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $nombre_paquete = $rowEvento["nombre_evento"];

  $asunto = str_replace(' ','+',$nombre_paquete);

  $desdePaq = date("d/m/Y",$rowEvento["fecha_evento"]);

  $hastaPaq = date("d/m/Y",$rowEvento["fecha_evento"]);

  echo '

  <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >

<div class="col-md-12 cuadro text-justify">

    <h2 class="page-header text-uppercase">'.$rowEvento["nombre_evento"].'</h2>

    <div class="col-md-4">';

  if (!file_exists("img/eventos/$rowEvento[imagen_evento]")){

      echo'<img src="https://hoteleshesperia.com.ve/img/eventos/sin-imagen.png" alt="'.$rowEvento["nombre_evento"].'" class="img-responsive"/>';

  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowEvento["imagen_evento"].'-6"  alt="'.$rowEvento["nombre_evento"].'" class="img-responsive" />';

  }echo'

            </div>

    <div class="col-md-8">

        <ul class="list-unstyled">

            <li>

                <i class="fa fa-info-circle"></i> <strong>Hora:</strong>'.$rowEvento["hora_evento"].'

            </li>

            <li>

                <i class="fa fa-info-circle"></i> <strong>Fecha:</strong> '.date("d/m/Y",$rowEvento["fecha_evento"]).'

            </li>

        </ul>

    '.$rowEvento["texto_evento"].'<br/>';



            if($rowEvento["precio_evento"] > 0){

                if($rowEvento["precio_nino"] > 0){

                        echo '<ul class="list-unstyled">

                                    <li>

                                        <i class="fa fa-info-circle"></i> <strong>Precio por adulto:</strong> '.number_format(round($rowEvento["precio_evento"]), 2, ',', '.').'

                                    </li>

                                    <li>

                                        <i class="fa fa-info-circle"></i> <strong>Precio por niño:</strong> '.number_format(round($rowEvento["precio_nino"]), 2, ',', '.').'

                                    </li>

                             </ul>';

                    }else{

                        echo '<ul class="list-unstyled">

                                    <li>

                                        <i class="fa fa-info-circle"></i> <strong>Precio:</strong> '.number_format(round($rowEvento["precio_evento"]), 2, ',', '.').'

                                    </li>

                             </ul>';

                    }

            echo '

              <input type="hidden" name="nombrePaq" value="'.$rowEvento["nombre_evento"].'"/>

                <input type="hidden" name="precioPaq" value="'.$rowEvento["precio_evento"].'"/>

                <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                <input type="hidden" name="id_hotel" value="'.$rowEvento["id_hotel"].'"/>

                <input type="hidden" name="tiempo" value="0"/>

                <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                <input type="hidden" name="precioNino" value="'.$rowEvento["precio_nino"].'"/>

                <input type="hidden" name="dpd3" value=""/>

                <input type="hidden" name="porcentaje" value="0"/>

                <input type="hidden" name="habitacion" value=""/>';

                if($rowEvento["precio_nino"] > 0){

                   echo'

                   <div class="row">

                            <div class="col-sm-1"><strong>Adultos<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                            <div class="col-sm-1"><strong>Niños<br> 

                                <select name="numPaqN">

                                    <option value="0">0</option>

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                        </div>';

                }else{

                    echo'

                    <div class="row">

                            <div class="col-sm-6"><strong>Cantidad<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                    </div>';

                }



                echo'

                <br>

              <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>';

            }

    echo '

        

    </div>

    <hr>

</div>

</form>';

 }

 return;

}



function LISTA_NOTA($mysqli,$data,$hostname,$user,$password,$db_name)

{  $cantidad=100;



 if (isset($_GET["primera"]))

 { $primera=htmlentities($_GET["primera"]);

  $pg=htmlentities($_GET["pg"]);

  $temp=$pg;

  settype($primera,integer); }



 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"]; }



 if (!isset($_GET["pg"])){ $pg = 1;}

 else {$pg = $_GET["pg"]; }



 if ($temp>=$pg && $primera !=1)

 { $inicial=$pg * $cantidad + 1;  }

 else {  $inicial=$pg * $cantidad - $cantidad; }



 $sql = sprintf("SELECT noticia_id,titulo,intro FROM hesperia_noticias");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $total_noticias=mysqli_num_rows($result);

 $pages=@intval($total_noticias / $cantidad) + 1;

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay histórico de noticias.</p></div>';

  return; }

 echo '<h3 style="margin-top:-5px">Listado de Publicaciones</h3>';

 $sql = sprintf("SELECT noticia_id,titulo,intro FROM hesperia_noticias

                  ORDER BY noticia_id DESC LIMIT %s,%s",

                mysqli_real_escape_string($mysqli,$inicial),

                mysqli_real_escape_string($mysqli,$cantidad));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 echo "<p>Total de publicaciones: <strong>$total_noticias</strong> | P&aacute;gina(s): <strong>$pg / $pages</strong></p>

     <dl>";

 while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $id = ($rows["noticia_id"]);

  $titulo = $rows["titulo"];

  $intro = $rows["intro"];

  echo '

              <dt>'.$titulo.'</dt>

              <dd>'.$intro.' <br/><a href="index.php?go=0/prensa/'.$id.'/'.FSPATH($titulo).'" title="Leer mas de: '.$titulo .'" class="pull-right">Leer mas</a></dd>';

 }

 echo'</dl>';

 PAGINACION($mysqli,$data,$hostname,$user,$password,$db_name,$total_noticias,$pages,$cantidad,$pg);

 return;

}



function LISTA_PDF($mysqli,$data,$hostname,$user,$password,$db_name)

{  $cantidad=120;

 if (isset($_GET["primera"]))

 { $primera=htmlentities($_GET["primera"]);

  $pg=htmlentities($_GET["pg"]);

  $temp=$pg;

  settype($primera,integer); }

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"]; }

 if (!isset($_GET["pg"])){ $pg = 1;}

 else {$pg = $_GET["pg"]; }

 if ($temp>=$pg && $primera !=1)

 { $inicial=$pg * $cantidad + 1;  }

 else {  $inicial=$pg * $cantidad - $cantidad; }

 $sql = sprintf("SELECT id FROM hesperia_descarga WHERE tipo = '1'");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $total_pdf=mysqli_num_rows($result);

 $pages=@intval($total_pdf / $cantidad) + 1;

 $sql = sprintf("SELECT * FROM hesperia_descarga WHERE tipo = '1'

                  ORDER BY fecha DESC LIMIT %s,%s",

                mysqli_real_escape_string($mysqli,$inicial),

                mysqli_real_escape_string($mysqli,$cantidad));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay PDF para descarga.</p></div>';

  return; }

 echo '

<div class="page-header">

    <h3 class="g-prensa text-uppercase">

        Listado de PDF Descarga

    </h3>

</div>';

 echo "<p>Total de archivos .PDF: <strong>$total_pdf</strong> | P&aacute;gina(s): <strong>$pg / $pages</strong></p>

     <dl>";

 while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 {  $titulo = $rows["titulo"];

  $descripcion = $rows["descripcion"];

  $archivo = $rows["archivo"];

  echo  '

            <dt>'.$titulo.'</dt>

                <dd>'.$descripcion.'</dd>

                <dd>

                    <a href="pdf-descarga.php?archivo='.$archivo.'" title="Descargar: '.$titulo.'" target="_blank">

                    Descargar

                    </a>

                    <i class="fa fa-download"></i>

                </dd>';

 }

 echo'</dl>

<div class="row">

    <div class="col-md-12"> ';

 PAGINACION_PDF($mysqli,$data,$hostname,$user,$password,$db_name,$total_pdf,$pages,$cantidad,$pg);

 echo '

    </div>

</div> ';

 return;

}



function LISTA_VIDEO($mysqli,$data,$hostname,$user,$password,$db_name)

{  $cantidad=40;

 if (isset($_GET["primera"]))

 { $primera=htmlentities($_GET["primera"]);

  $pg=htmlentities($_GET["pg"]);

  $temp=$pg;

  settype($primera,integer); }

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"]; }

 if (!isset($_GET["pg"])){ $pg = 1;}

 else {$pg = $_GET["pg"]; }

 if ($temp>=$pg && $primera !=1)

 { $inicial=$pg * $cantidad + 1;  }

 else {  $inicial=$pg * $cantidad - $cantidad; }

 $sql = sprintf("SELECT id FROM hesperia_descarga WHERE tipo = '0'");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $total_video=mysqli_num_rows($result);

 $pages=@intval($total_video/ $cantidad) + 1;

 $sql = sprintf("SELECT * FROM hesperia_descarga WHERE tipo = '0'

                  ORDER BY fecha DESC LIMIT %s,%s",

                mysqli_real_escape_string($mysqli,$inicial),

                mysqli_real_escape_string($mysqli,$cantidad));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay videos disponibles para descarga.</p></div>';

  return; }

 echo '

<div class="page-header">

    <h3 class="g-prensa text-uppercase">Listado de Videos

    </h3>

</div>';

 echo "<p>Total de Videos: <strong>$total_video</strong> | P&aacute;gina(s): <strong>$pg / $pages</strong></p>";

 echo'<div class="row">';

 $i=0;

 while($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 { if ($i%2) { echo'<div class="row">'; }

  $titulo = $rows["titulo"];

  $descripcion = $rows["descripcion"];

  $archivo = $rows["archivo"];

  echo '

    <div class="col-sm-6 col-md-6">

        <div class="thumbnail">

          <img class="img-hover" src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$archivo.'-16"  alt="'.$titulo.'">

          <div class="caption">

            <h3>'.$titulo.'</h3>

            '.$descripcion.'...

            <p><a href="https://youtu.be/'.$archivo.'" title="Ver videoLeer mas de: '.$titulo .'" target="_blank">Ver Video</a> »</p>

          </div>

        </div>

    </div>';

  if ($i%2) { echo'</div>'; }

  $i++;

 }

 PAGINACION_VIDEO($mysqli,$data,$hostname,$user,$password,$db_name,$total_video,$pages,$cantidad,$pg);

 echo '</div></div>';

 return;

}



function LISTANDO_RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio)

{ $sql = sprintf("SELECT * FROM hesperia_restaurant WHERE  id_hotel = '%s'  ORDER BY RAND()",

                 mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay restaurantes en este Hotel.</p></div>';

  return; }

 $imagen = $rowServicio["imagen_restaurant"];

 while ($rowRestaurant=mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $nombre_paquete = $rowRestaurant["nombre_restaurant"];

  $asunto = str_replace(' ','+',$nombre_paquete);

  echo '

    <div class="col-md-12 cuadro text-justify">

    <div class="page-header">

        <h2 class="g-prensa">'.$rowRestaurant["nombre_restaurant"].'</h2>

    </div>

        <div class="col-md-4">';

  if (!file_exists("img/restaurant/$rowRestaurant[imagen]")){

      echo'<img src="https://hoteleshesperia.com.ve/img/restaurant/sin-imagen.png" alt="'.utf8_encode($rowRestaurant["nombre_restaurant"]).'" class="img-responsive" title= "'.utf8_encode($rowRestaurant["nombre_restaurant"]).'"/>';

  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowRestaurant["imagen"].'-7" style="width:100%" alt= "Restaurant '.$rowRestaurant["nombre_restaurant"].'" title= "Restaurant '.$rowRestaurant["nombre_restaurant"].'"/>';

  }echo'

    </div>

    <div class="col-md-8">

        <ul class="list-unstyled">

            <li>

                <i class="fa fa-info-circle"></i>

                <strong>Horario: </strong>'.$rowRestaurant["hora_apertura"].' a '.$rowRestaurant["hora_cierre"].'

            </li>

            <li>

                <i class="fa fa-info-circle"></i>

                <strong>Tipo de Cocina:</strong> '.$rowRestaurant["tipo_comida"].'

            </li>

        </ul>

        '.$rowRestaurant["caracteristicas"].'<br/>

        <small class="pull-right">

                <a href="https://hoteleshesperia.com.ve/contacto/'.toAscii($nombre_paquete).'" title="Solicitar más Información de este Salón" class="btn btn-primary" target="_self"> Información y reservas.</a></small>

    </div>

</div>



    ';

 }

 return;

}



function LEER_CLIMA($id)

{ $temperatura = 1;

 $archivo = 'cache-clima/clima.php';

 $gestor = fopen($archivo, "r");

 if ($gestor) {

     while (($buffer = fgets($gestor, 4096)) !== false)

     {	$valor = explode ('=' ,$buffer);

      if ($valor[0] == $id)

      {  $temperatura = trim($valor[1]);

       return $temperatura;

      }

     }

     fclose($gestor);

 }

 return $temperatura;

}



function LOGOS($mysqli,$data,$hostname,$user,$password,$db_name)

{ $sql = "SELECT id,razon_social,logo,nombre_sitio FROM hesperia_settings ORDER BY RAND()";

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $nombres=array("","Hesperia-WTC-Valencia","Hesperia-Isla-Margarita","Hesperia-Playa-el-Agua","Hesperia-Eden-Club", "Hesperia-Maracay");

 echo '

   <div class="row">

        <div class="col-md-12">

            <div class="row text-center logo-hoteles">

                <div class="col-lg-12">

                <hr/>

                    <ul class="list-inline text-center">';

 while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){

     echo '<li>

            <a href="https://hoteleshesperia.com.ve/'.$nombres[$rows["id"]].'"  data-toggle="tooltip" data-placement="bottom" title="'.$rows["razon_social"].'">

            <img src="https://hoteleshesperia.com.ve/img/logo/'.$rows["logo"].'" class="img-responsive img-hover" alt="'.$rows["razon_social"].'">



            </a>

        </li>';

 }

 echo '

                    </ul>

                </div>

            </div>

        </div>

    </div>';

 return;

}



function MEDIACENTER($mysqli,$data,$hostname,$user,$password,$db_name)

{ echo '
    <div class="row">

        <div class="page-header">

            <h1 class="pull-right">SALA DE PRENSA</h1>

        </div>

        

        <div class="col-md-12">';

 if ($_GET["id"] == '0') {

     $sql = "SELECT * FROM hesperia_noticias ORDER BY noticia_id DESC limit 0,2";

     $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

     $hay = mysqli_num_rows($result);

     if ($hay < 1)

     { echo '<br/><div class="text-center">

        <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

        <h2>Disculpe</h2> <p>Actualmente no hay notas de prensa.</p></div></div>';

      return; }

     echo'

            <div class="row">';

     while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){

         $imagen = $rows["imagen"];

         $titulo = $rows["titulo"];

         $intro = $rows["intro"];

         $contenido = $rows["contenido"];

         $id = $rows["noticia_id"];

         echo '

                    <div class="col-sm-6 col-md-6">

                        <div class="thumbnail">

                            <img class="img-hover" src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-13" alt="'.$titulo.'" title="'.$titulo.'">

                            <div class="caption">

                                <h3>'.$titulo.'</h3>

                                    '.$intro.'

                                <p>

                                    <a href="index.php?go=0/prensa/'.$id.'/'.FSPATH($titulo).'" title="Leer mas de: '.$titulo .'" target="_self">

                                Ver contenido

                                    </a>»

                                </p>

                            </div>

                        </div>

                    </div>

                  ';

     }

     echo'

</div>

            

</div>

';



 } else {  $ok = 0;

         if ($_GET["id"] == 'lista')

         { $ok = 1;

          LISTA_NOTA($mysqli,$data,$hostname,$user,$password,$db_name); }

         if ($_GET["id"] == 'galeria')

         { $ok = 1;

          VER_GALERIA($mysqli,$data,$hostname,$user,$password,$db_name); }

         if ($_GET["id"] == 'pdf')

         { $ok = 1;

          LISTA_PDF($mysqli,$data,$hostname,$user,$password,$db_name); }

         if ($_GET["id"] == 'videosY')

         { $ok = 1;

          LISTA_VIDEO($mysqli,$data,$hostname,$user,$password,$db_name); }

         if ( $ok == 0) {

             VER_NOTA($mysqli,$data,$hostname,$user,$password,$db_name); }

        }

 echo '

        </div>

    </div>';

 return;

}



function NOSOTROS($data,$hostname,$user,$password,$db_name){

    echo '<div class="row">

    <div class="col-md-12">';

    if (!file_exists("img/logo/$data[logo]")){

        echo'<img src="https://hoteleshesperia.com.ve/img/logo/sin-imagen.png" alt="'.$data["razon_social"].'" class="img-responsive center-block img-us"/>';

    }else{

        echo'<img src="https://hoteleshesperia.com.ve/img/logo/'.$data["logo"].'" class="img-responsive center-block img-us" alt="Logo '.$data["razon_social"].'">';

    } echo'<div class="page-header">

            <h1 class="about_us">'.$data["razon_social"].'</h1>

        </div>

       '.$data["quienes"].'<br/>

        <address>

            <div id="Coordenadas">

                <p>Coordenadas GPS:</p>

                <p><br/><abbr title="latitud">Latitud:</abbr> '.$data["latitud"].'</p>

                <p><abbr title="latitud">Longitud:</abbr> '.$data["longitud"].'</p>

            </div>

            <div id="direccion">

             <br/><abbr title="direccion">Dirección:<br/></abbr> '.html_entity_decode($data['direccion']).'

            <br/>'.$data['ciudad'].' - '.$data['estado'].'</abbr>

            <br/>'.$data['pais'].' '. $data['codigo_postal'].'

            </div>

            <div id="telefono">

                <br/><abbr title="Telefono">Telef&oacute;no(s): '.$data['telefono'].'</abbr>

            </div>

            <div class="ubicacion"><br/>

                <abbr title="ubicacion"><iframe src="include/mapa.php" width="100%" height="400" frameborder="0" style="border:0"></iframe></abbr><br/>

            </div>

        </address>

    </div>

</div>';

    return;

}



function NOSOTROS_PRINCIPAL($mysqli,$hostname,$user,$password,$db_name){

    $sql = "SELECT * FROM hesperia_nosotros limit 1";

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);

    echo '<div class="row">

    <div class="col-md-12">';

    if(!empty($rows["imagen"])){

        if (!file_exists("img/logo/$rows[imagen]")){

            echo'<img src="https://hoteleshesperia.com.ve/img/logo/sin-imagen.png" alt="'.$rows["titulo"].'xxx" class="img-responsive center-block img-us"/>';

        }else{

            echo'<img src="https://hoteleshesperia.com.ve/img/logo/'.$rows["imagen"].'" class="img-responsive center-block img-us" alt="Logo '.$rows["titulo"].'">';

        }

    }



    echo'

        <div class="page-header">

            <h1 class="about_us">'.$rows["titulo"].'</h1>

        </div>

       '.$rows["texto_nosotros"].'<br/>

    </div>

</div>';

    return;

}



function OTROS_SERVICIOS($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio)

{

    $sql = sprintf("SELECT * FROM hesperia_experiencias WHERE  id_hotel = '%s' ORDER BY fecha DESC",

                   mysqli_real_escape_string($mysqli,$data["id"]));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no poseemos Experiencias disponibles</p></div>';

     return; }

    //    echo'

    //<div class="page-header">

    //        <h3 class="g-prensa text-uppercase">

    //            Listado de experiencias

    //        </h3>

    //    </div>';

    while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC))

    { $nombre_paquete = $rowPaquetes["nombre_expe"];

     $asunto = str_replace(' ','+',$nombre_paquete);

     echo '

        <div class="col-md-12 cuadro">

            <h2>'.$nombre_paquete.'</h2>

            <div class="col-md-4">';

     if (!file_exists("img/experiencias/$rowPaquetes[img_expe]")){

         echo'<img src="https://hoteleshesperia.com.ve/img/experiencias/sin-imagen.png" alt="'.utf8_encode($rowPaquetes["nombre_expe"]).'" class="img-responsive"/>';

     }else{

         echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPaquetes["img_expe"].'-10"  alt="'.utf8_encode($rowPaquetes["nombre_expe"]).'" class="img-responsive" />';

     }echo'

            </div>

            <div class="col-md-8">

                '.$rowPaquetes["descripcion_expe"].'

              <small class="pull-right">

                <a href="https://hoteleshesperia.com.ve/contacto/'.toAscii($nombre_paquete).'" title="Solicitar más Información de esta Experiencia" class="btn btn-primary" target="_self"> Información y reservas.</a></small>

            </div>

            <hr>

          </div>';

    }

    return;



}



function PAGINACION($mysqli,$data,$hostname,$user,$password,$db_name,$total_noticias,$pages,$cantidad,$pg)

{	if (isset($_GET["primera"]))

{	$primera=htmlentities($_GET["primera"]);

 $pg=htmlentities($_GET["pg"]);

 $temp=$pg;

 settype($primera,integer);

}

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"];

  $primera=htmlentities($_GET["primera"]); }

 echo '

<div id="paginacion">

    <ul class="pagination">';

 for ($i=1; $i<($pages + 1); $i++) {

     if ($i ==$pg) {

         echo '<li class="active"><a href="#">'.$i.'</a></li>';

     }

     else {

         if ($paginacion ==40)

         { $paginacion=0; }

         $paginacion++;

         echo '<li><a href="index.php?go=0/prensa/galeria/&pg='.$i.'&primera=1&" title="Ir a p&aacute;gina:'.$i.'">'.$i.'</a></li>';

     }

 }

 echo '

    </ul>

</div>';

 return;

}



function PAGINACION_BOLETINES($mysqli,$data,$hostname,$user,$password,$db_name,$total_boletines,$pages,$cantidad,$pg)

{	if (isset($_GET["primera"]))

{	$primera=htmlentities($_GET["primera"]);

 $pg=htmlentities($_GET["pg"]);

 $temp=$pg;

 settype($primera,integer);

}

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"];

  $primera=htmlentities($_GET["primera"]); }

 echo '

<div id="paginacion">

    <ul class="pagination">';

 for ($i=1; $i<($pages + 1); $i++) {

     if ($i ==$pg) {

         echo '<li class="active"><a href="#">'.$i.'</a></li>';

     }

     else {

         if ($paginacion ==30)

         { $paginacion=0; }

         $paginacion++;

         echo '<li><a href="index.php?go=0/boletines/pdf/&pg='.$i.'&primera=1&" title="Ir a p&aacute;gina:'.$i.'">'.$i.'</a></li>';

     }

 }

 echo '

    </ul>

</div>';

 return;

}



function PAGINACION_GALERIA($mysqli,$data,$hostname,$user,$password,$db_name,$total_imagenes,$pages,$cantidad,$pg)

{	if (isset($_GET["primera"]))

{	$primera=htmlentities($_GET["primera"]);

 $pg=htmlentities($_GET["pg"]);

 $temp=$pg;

 settype($primera,integer);

}

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"];

  $primera=htmlentities($_GET["primera"]); }

 echo '



    <ul class="pagination">';

 for ($i=1; $i<($pages + 1); $i++) {

     if ($i ==$pg) {

         echo '<li class="active"><a href="#">'.$i.'</a></li>';

     }

     else {

         if ($paginacion ==30)

         { $paginacion=0; }

         $paginacion++;

         echo '<li><a href="index.php?go=0/prensa/galeria/&pg='.$i.'&primera=1&" title="Ir a p&aacute;gina:'.$i.'">'.$i.'</a></li>';

     }

 }

 echo '

    </ul>';

 return;

}



function PAGINACION_PDF($mysqli,$data,$hostname,$user,$password,$db_name,$total_pdf,$pages,$cantidad,$pg)

{	if (isset($_GET["primera"]))

{	$primera=htmlentities($_GET["primera"]);

 $pg=htmlentities($_GET["pg"]);

 $temp=$pg;

 settype($primera,integer);

}

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"];

  $primera=htmlentities($_GET["primera"]); }

 echo '

<div id="paginacion">

    <ul class="pagination">';

 for ($i=1; $i<($pages + 1); $i++) {

     if ($i ==$pg) {

         echo '<li class="active"><a href="#">'.$i.'</a></li>';

     }

     else {

         if ($paginacion ==30)

         { $paginacion=0; }

         $paginacion++;

         echo '<li><a href="index.php?go=0/prensa/pdf/&pg='.$i.'&primera=1&" title="Ir a p&aacute;gina:'.$i.'">'.$i.'</a></li>';

     }

 }

 echo '

    </ul>

</div>';

 return;

}



function PAGINACION_VIDEO($mysqli,$data,$hostname,$user,$password,$db_name,$total_video,$pages,$cantidad,$pg)

{	if (isset($_GET["primera"]))

{	$primera=htmlentities($_GET["primera"]);

 $pg=htmlentities($_GET["pg"]);

 $temp=$pg;

 settype($primera,integer);

}

 if (!isset($primera))

 {	$primera=$pg=$temp =1;

  $inicial=$paginacion=0; }

 else

 {	$pg=$_GET["pg"];

  $primera=htmlentities($_GET["primera"]); }

 echo '

    <ul class="pagination">';

 for ($i=1; $i<($pages + 1); $i++) {

     if ($i ==$pg) {

         echo '<li class="active"><a href="#">'.$i.'</a></li>';

     }

     else {

         if ($paginacion ==30)

         { $paginacion=0; }

         $paginacion++;

         echo '<li><a href="index.php?go=0/prensa/videosY/&pg='.$i.'&primera=1&" title="Ir a p&aacute;gina:'.$i.'">'.$i.'</a></li>';

     }

 }

 echo '

    </ul>';

 return;

}



function PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name){

    

    $hoy = date("Y-m-d",time());

    

   

    if($data["id_paquete_promo"] == 0){
       
       echo'<div class="row"><div class="page-header">

            <h1 style="margin-top:10px;">Nuestros Paquetes</h1>

        </div>';

        $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 ORDER BY orden ASC");

    }else{

       //echo'<div style="margin-top:28px;"></div>';

        $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND id = '%s' ORDER BY orden ASC",

            mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

    }



    if($data["temporada"] > 0){

       /*echo'<div class="row"><div class="page-header">

            <h1 style="margin-top:10px;">Paquetes de Temporada</h1>

        </div>';*/

      $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' ORDER BY orden ASC",

            mysqli_real_escape_string($mysqli,$data["temporada"]));

    }



    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';

     return; }
     
    while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC))

    { 

        $fecha = date("d/m/Y",time()+86400); 

        $nombre_paquete = $rowPaquetes["nombre_paquete"];

    $capacidad = $rowPaquetes["capacidad"];

     $asunto = str_replace(' ','+',$nombre_paquete);

      $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",

                   mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

     $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

     $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);

     $hotel = $rowSetting["razon_social"];

     $desdePaq = '';

     $hastaPaq = '';

     $precio = $rowPaquetes["precio"];

     $habitacion = '';

     if($rowPaquetes["porcentaje"] > 0){

    $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",

                   mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));

     $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);

     $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);

     $habitacion = $rowHab["nombre_habitacion"];

     echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

  }



     echo '

     <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >

        <div class="col-md-12 cuadro text-justify">

            <h2 class="page-header">'.$nombre_paquete.'</h2>

            <div class="col-md-4">';

     if (!file_exists("img/paquetes/$rowPaquetes[img_paquete]")){

         echo'<img src="https://hoteleshesperia.com.ve/img/paquetes/sin-imagen.png" alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive"/>';

     }else{

         echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPaquetes["img_paquete"].'-3"  alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive" />';

     }echo'

            </div>

            <div class="col-md-8">

                '.$rowPaquetes["descripcion_paquete"].' <br>';

                if($precio > 0){



                    if($rowPaquetes["precio_nino"] > 0){

                        echo '<strong>Precio por adulto: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>

                        <strong>Precio por niño: '.number_format(round($rowPaquetes["precio_nino"]), 2, ',', '.').'</strong><br/>';

                        if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                        }

                    }else{

                        if($rowPaquetes["porcentaje"] > 0){

                            $precio = $rowPaquetes["precio"] - ($rowPaquetes["precio"] * $rowPaquetes["porcentaje"]);

                            if($rowPaquetes["ninos_gratis"] ==  0){

                                echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Con un '.substr($rowPaquetes["porcentaje"], 2, 2).'% de descuento aplicado)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }else{

                                echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Incluidos '.$rowPaquetes["ninos_gratis"].' Niños Gratis)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }

                            if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                            }

                        }else{

                            if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                            }

                            echo '<strong>Precio: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>';

                        }

                        

                    }

                echo '

                <hr>

                <strong>Selecciona fecha de llegada<br> </strong><input type="text" class="dpd3 input-sm" value="'.$fecha.'" id="dpd3" name="dpd3" placeholder="Entrada" data-date-format="dd/mm/yyyy" data-min-date="'.$rowPaquetes["desde"].'" data-max-date="'.$rowPaquetes["hasta"].'" language="es" readonly="readonly" ><br>

                <input type="hidden" name="nombrePaq" value="'.$nombre_paquete.'"/>

                <input type="hidden" name="precioPaq" value="'.$precio.'"/>

                <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                <input type="hidden" name="id_hotel" value="'.$rowPaquetes["id_hotel"].'"/>

                <input type="hidden" name="tiempo" value="'.$rowPaquetes["tiempo"].'"/>                

                <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                <input type="hidden" name="precioNino" value="'.$rowPaquetes["precio_nino"].'"/>

                <input type="hidden" name="porcentaje" value="'.$rowPaquetes["porcentaje"].'"/>

                <input type="hidden" name="partai" value="'.$rowPaquetes["partai"].'"/>

                <input type="hidden" name="habitacion" value="'.$habitacion.'"/>

                <input type="hidden" name="vip" value="0"/>';

                if($rowPaquetes["precio_nino"] > 0){

                   echo'<div class="row">

                            <div class="col-sm-1"><strong>Adultos<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                            <div class="col-sm-1"><strong>Niños<br> 

                                <select name="numPaqN">

                                    <option value="0">0</option>

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                        </div>';

                }else{

                    if($rowPaquetes["porcentaje"] <= 0){

                        if($capacidad == 5){

                            echo'

                        <div class="row">

                                <div class="col-sm-6"><strong>Cantidad<br> 

                                    <select name="numPaq">                                    

                                        <option value="5">5</option>

                                        <option value="6">6</option>

                                        <option value="7">7</option>

                                        <option value="8">8</option>

                                        <option value="9">9</option>

                                        <option value="10">10</option>

                                        <option value="11">11</option>

                                        <option value="12">12</option>

                                        <option value="13">13</option>

                                        <option value="14">14</option>

                                        <option value="15">15</option>

                                        <option value="16">16</option>

                                        <option value="17">17</option>

                                        <option value="18">18</option>

                                        <option value="19">19</option>

                                        <option value="20">20</option>

                                    </select></strong>

                                </div>

                        </div>';

                        }else{

                        echo'

                        <div class="row">

                                <div class="col-sm-6"><strong>Cantidad<br> 

                                    <select name="numPaq">

                                        <option value="1">1</option>

                                        <option value="2">2</option>

                                        <option value="3">3</option>

                                        <option value="4">4</option>

                                        <option value="5">5</option>

                                        <option value="6">6</option>

                                        <option value="7">7</option>

                                        <option value="8">8</option>

                                        <option value="9">9</option>

                                        <option value="10">10</option>

                                        <option value="11">11</option>

                                        <option value="12">12</option>

                                        <option value="13">13</option>

                                        <option value="14">14</option>

                                        <option value="15">15</option>

                                        <option value="16">16</option>

                                        <option value="17">17</option>

                                        <option value="18">18</option>

                                        <option value="19">19</option>

                                        <option value="20">20</option>

                                    </select></strong>

                                </div>

                        </div>';

                        }

                    }

                }

                echo'

                <br>

                <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>';

                }

                echo'

            </div>

            <hr>

          </div>



    </form>';

    }
    $sqlDestacados = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 and destacado = 1 ORDER BY orden ASC");
     $resultDestacados = QUERYBD($sqlDestacados,$hostname,$user,$password,$db_name);
     echo '
     <h3>Los más destacados</h3>
      <div class="row">';
     while ($rowDestacados=mysqli_fetch_array($resultDestacados,MYSQLI_ASSOC)){
        echo '
          <div class="col-md-3 col-xs-6 pdf-thumb-box no-space" id="paquete-'.$rowDestacados["id"].'"> 
                      
              <a href="https://hoteleshesperia.com.ve/paquetes/'.$rowDestacados["id"].'/" >
                <div class="pdf-thumb-box-overlay">
                  <h6 class="text-center">'.$rowDestacados["nombre_paquete"].'</h6>
                  <div class="btn-primary btn btn-sm">
                    Reservar
                  </div>
                </div>
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowDestacados["img_paquete"].'" alt="'.$rowDestacados["nombre_paquete"].'" class="img-responsive">
              </a>
          </div>';
     }
     echo '</div>'; 

    echo '

        <div id="RespuestaIp"></div>

           </div>';

    return;

}



function PARTAI($mysqli,$data,$hostname,$user,$password,$db_name){

    
  //if($_SESSION["locationGeo"] == 'VE'){
    $hoy = date("Y-m-d",time());

    

    echo'

    

          <div class="row"><div class="page-header">

            <h1 class="text-uppercase" style="margin-top:10px;">PartaÏ Margarita Weekend</h1>

        </div>';

        
    if($_SESSION["locationGeo"] == 'VE')
      $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 1 and vip = 0 ORDER BY orden ASC");
    else
      $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 1 and vip = 0 ORDER BY orden ASC");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';

     return; }

    while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC))

    { 

        $fecha = date("d/m/Y",time()+86400); 

        $nombre_paquete = $rowPaquetes["nombre_paquete"];

    $capacidad = $rowPaquetes["capacidad"];

     $asunto = str_replace(' ','+',$nombre_paquete);

      $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",

                   mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

     $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

     $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);

     $hotel = $rowSetting["razon_social"];

     $desdePaq = '';

     $hastaPaq = '';

     if($_SESSION["locationGeo"] == 'VE'){
      $precio = $rowPaquetes["precio"];
      $precio_nino = $rowPaquetes["precio_nino"];
     }else{
      $precio = $rowPaquetes["price"];
      $precio_nino = $rowPaquetes["price_child"];
     }
      
     $habitacion = '';

     if($rowPaquetes["porcentaje"] > 0){

    $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",

                   mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));

     $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);

     $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);

     $habitacion = $rowHab["nombre_habitacion"];

     echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

  }



     echo '

     <form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >

        <div class="col-md-12 cuadro text-justify">

            <h2 class="page-header">'.$nombre_paquete.'</h2>

            <div class="col-md-4">';

     if (!file_exists("img/paquetes/$rowPaquetes[img_paquete]")){

         echo'<img src="https://hoteleshesperia.com.ve/img/paquetes/sin-imagen.png" alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive"/>';

     }else{

         echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPaquetes["img_paquete"].'-3"  alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive" />';

     }echo'

            </div>

            <div class="col-md-8">

                '.$rowPaquetes["descripcion_paquete"].' <br>';

                if($precio > 0){



                    if($precio_nino > 0){

                        echo '<strong>Precio por adulto: '.number_format(round($precio), 2, ',', '.').'</strong><br/>

                        <strong>Precio por niño: '.number_format(round($precio_nino), 2, ',', '.').'</strong><br/>';

                        if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                        }

                    }else{

                        if($rowPaquetes["porcentaje"] > 0){
                          $precio_ant = $precio;
                            $precio = $precio - ($precio * $rowPaquetes["porcentaje"]);

                            if($rowPaquetes["ninos_gratis"] ==  0){

                                echo 'Precio antes: <strike>'.number_format(round($precio_ant), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Con un '.substr($rowPaquetes["porcentaje"], 2, 2).'% de descuento aplicado)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }else{

                                echo 'Precio antes: <strike>'.number_format(round($precio_ant), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Incluidos '.$rowPaquetes["ninos_gratis"].' Niños Gratis)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }

                            if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                            }

                        }else{

                            echo '<strong>Precio: '.number_format(round($precio), 2, ',', '.').'</strong><br/>';

                            if($rowPaquetes["partai"] == 1){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                            }

                        }

                        

                    }

                echo '

                <hr>

                <strong>Selecciona fecha de llegada<br> </strong><input type="text" class="dpd3 input-sm" value="'.$fecha.'" id="dpd3" name="dpd3" placeholder="Entrada" data-date-format="dd/mm/yyyy" data-min-date="'.$rowPaquetes["desde"].'" data-max-date="'.$rowPaquetes["hasta"].'" language="es" readonly="readonly" ><br>

                <input type="hidden" name="nombrePaq" value="'.$nombre_paquete.'"/>

                <input type="hidden" name="precioPaq" value="'.$precio.'"/>

                <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                <input type="hidden" name="id_hotel" value="'.$rowPaquetes["id_hotel"].'"/>

                <input type="hidden" name="tiempo" value="'.$rowPaquetes["tiempo"].'"/>                

                <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                <input type="hidden" name="precioNino" value="'.$precio_nino.'"/>

                <input type="hidden" name="porcentaje" value="'.$rowPaquetes["porcentaje"].'"/>

                <input type="hidden" name="partai" value="'.$rowPaquetes["partai"].'"/>

                <input type="hidden" name="vuelo" value="'.$rowPaquetes["vuelo"].'"/>

                <input type="hidden" name="habitacion" value="'.$habitacion.'"/>

                <input type="hidden" name="vip" value="0"/>';

                if($precio_nino > 0){

                   echo'<div class="row">

                            <div class="col-sm-1"><strong>Adultos<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                            <div class="col-sm-1"><strong>Niños<br> 

                                <select name="numPaqN">

                                    <option value="0">0</option>

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                        </div>';

                }else{

                    if($rowPaquetes["porcentaje"] <= 0){

                        if($capacidad == 5){

                            echo'

                        <div class="row">

                                <div class="col-sm-6"><strong>Cantidad<br> 

                                    <select name="numPaq">                                    

                                        <option value="5">5</option>

                                        <option value="6">6</option>

                                        <option value="7">7</option>

                                        <option value="8">8</option>

                                        <option value="9">9</option>

                                        <option value="10">10</option>

                                        <option value="11">11</option>

                                        <option value="12">12</option>

                                        <option value="13">13</option>

                                        <option value="14">14</option>

                                        <option value="15">15</option>

                                        <option value="16">16</option>

                                        <option value="17">17</option>

                                        <option value="18">18</option>

                                        <option value="19">19</option>

                                        <option value="20">20</option>

                                    </select></strong>

                                </div>

                        </div>';

                        }else{

                        echo'

                        <div class="row">

                                <div class="col-sm-6"><strong>Cantidad<br> 

                                    <select name="numPaq">

                                        <option value="1">1</option>

                                        <option value="2">2</option>

                                        <option value="3">3</option>

                                        <option value="4">4</option>

                                        <option value="5">5</option>

                                        <option value="6">6</option>

                                        <option value="7">7</option>

                                        <option value="8">8</option>

                                        <option value="9">9</option>

                                        <option value="10">10</option>

                                        <option value="11">11</option>

                                        <option value="12">12</option>

                                        <option value="13">13</option>

                                        <option value="14">14</option>

                                        <option value="15">15</option>

                                        <option value="16">16</option>

                                        <option value="17">17</option>

                                        <option value="18">18</option>

                                        <option value="19">19</option>

                                        <option value="20">20</option>

                                    </select></strong>

                                </div>

                        </div>';

                        }

                    }

                }

                echo'

                <br>

                <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>';

                }

                echo'

            </div>

            <hr>

          </div>



    </form>';

    }

    echo '

        <div id="RespuestaIp"></div>

           </div>';
  /*}else{
    echo '<br/><div class="text-center">

    <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

    <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';
  }*/
    

    return;

}



function PARTAI_VIP($mysqli,$data,$hostname,$user,$password,$db_name, $clave){

  if($data["temporada"] == 100){
       date_default_timezone_set('America/Caracas');
      $test = date('Y-m-d H:i:s', time());

      echo '
      <div class="page-header">
            <img src="https://hoteleshesperia.com.ve/img/contenido/Banner atrapalo-01.jpg" class="img-responsive"/> 
            <h2 class="text-center">¡Todos los días atrapa las diferentes oportunidades que tenemos para tí!</h2> 
            <h3 class="text-center">Accede cada día a esta sección y atrapa las novedades que iremos publicando.</h3>   
            <br>
                    
      </div>';

      /*$sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["temporada"]));

      
      $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

      if ($hay < 1){ 
        echo '<br>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos ofertas para atrapar!</p>
              </div>';
        return; 
      }else{
        while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
          $data["id_paquete_promo"] = $rowPaquetes["id"];
          PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name);
          
        }
      }*/

    } else{

      echo '
      <div class="page-header">
            <h1 style="margin-top:10px;">Nuestros Paquetes</h1>
          </div>';
    }
    //if($_SESSION["locationGeo"] == 'VE'){
          $hoy = date("Y-m-d",time());          

        if($data["id_paquete_promo"] == 0){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 1 and vip = 1 AND temporada <> 100 ORDER BY id_hotel, temporada ASC");
            else
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 1 and vip = 1 AND temporada <> 100 ORDER BY id_hotel, temporada ASC");

        }else{
          if($_SESSION["locationGeo"] == 'VE')
            $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 1 and vip = 1 AND id = '%s' AND temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));
          else
            $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 1 and vip = 1 AND id = '%s' AND temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

        }

        if($data["temporada"] > 0){

          if($data["temporada"] == 100){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 1 and vip = 1 AND temporada = '%s' AND hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 1 and vip = 1 AND temporada = '%s' AND hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
          }else{
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 1 and vip = 1 AND temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status_dolar <> 'N' and partai = 1 and vip = 1 AND temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
          }
          
        }

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

        if ($hay < 1){ 
          echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
            return; 
       }else{
          echo '
          <div class="container-fluid">
          <div class="row">';
          $id_hotel_aux = 0;
          while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
            $fecha = date("d/m/Y",time()+86400); 
            $nombre_paquete = $rowPaquetes["nombre_paquete"];
            $capacidad = $rowPaquetes["capacidad"];
            $asunto = str_replace(' ','+',$nombre_paquete);

            $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                           mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

            $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

            $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
            $hotel = $rowSetting["razon_social"];
            /*if($id_hotel_aux != $rowPaquetes["id_hotel"]){
              if($id_hotel_aux != 0){
                echo '</div><div class="row">';
              }
              echo '
                <h3 class="text-primary text-center">'.$hotel.'</h3>';
              $id_hotel_aux = $rowPaquetes["id_hotel"];
            }*/

            $desdePaq = '';
            $hastaPaq = '';
            if($_SESSION["locationGeo"] == 'VE' ){
              $precio = $rowPaquetes["precio"];
            }else{
              $precio = $rowPaquetes["price"];
            }
            
            $habitacion = '';

            if($rowPaquetes["porcentaje"] > 0){
              $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                        mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));
              $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
              $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
              $habitacion = $rowHab["nombre_habitacion"];
              echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

            }

            $pos = strpos($rowPaquetes["nombre_paquete"], '. ');
            if($pos > 0){
              $pos++;
            }

            $nombre = substr($rowPaquetes["nombre_paquete"], $pos);
            echo '
            <div class="col-sm-6 col-md-4">';
            if($rowPaquetes["temporada"] == 100)
              echo'
              <div class="thumbnail equalizeAtrapa">';
            else
              echo'
              <div class="thumbnail equalize">';
              if($rowPaquetes["temporada"] == 100)
              echo '
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="200" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">';
              else
              echo '
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="300" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">';
                
              echo '
                <div class="caption">
                  <h4 style="height: 50px;" class="text-justify">'.$nombre.'</h4>
                  <p class="text-muted text-center"><strong>'.$hotel.'</strong></p>';
                  if($rowPaquetes["temporada"] == 100)
                  echo'
                  <div id="getting-started'.$rowPaquetes["id"].'" data-date="'.$rowPaquetes["hasta_atrapalo"].'"></div>
                  ';  
                  
                  echo'
                  <p><a href="https://hoteleshesperia.com.ve/paquetes/'.$rowPaquetes["id"].'/'.toAscii($nombre).'" class="btn btn-primary" role="button">Reservar</a></p>
                </div>
              </div>
            </div>
            ';
          }
          echo '</div>
          </div>';
       } 

    
      
    /*}else{
       echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
    }*/
    return;
    

}



function PERFIL($mysqli,$data,$hostname,$user,$password,$db_name)

{# PERFIL DE USUARIO





    $id = $_SESSION["referencia"];



    $sql = sprintf("SELECT * FROM hesperia_usuario WHERE id = '%s'" ,

                   mysqli_real_escape_string($mysqli,$id));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    if ($row=mysqli_fetch_array($result,MYSQLI_ASSOC))

    {   



      if($row["email"] == 'admin@hoteleshesperia.com.ve'){

        $sqlR = sprintf("SELECT * FROM hesperia_v2_reservaciones

                                WHERE status <> 'N' ORDER BY id DESC" ,

                        mysqli_real_escape_string($mysqli,$row["email"]));

      }else{



        $sqlR = sprintf("SELECT * FROM hesperia_v2_reservaciones

                                WHERE email = '%s' and status <> 'N' ORDER BY id DESC" ,

                        mysqli_real_escape_string($mysqli,$row["email"]));

      }

      

     $resultR = QUERYBD($sqlR,$hostname,$user,$password,$db_name);

     if($row["email"] == 'admin@hoteleshesperia.com.ve'){

      $sqlRP = sprintf("SELECT * FROM hesperia_v2_reservas_paq

                                WHERE status <> 'N' ORDER BY id DESC" );

     }else{

      $sqlRP = sprintf("SELECT * FROM hesperia_v2_reservas_paq

                                WHERE email = '%s' and status <> 'N' ORDER BY id DESC" ,

                        mysqli_real_escape_string($mysqli,$row["email"]));

     }

     

     $resultRP = QUERYBD($sqlRP,$hostname,$user,$password,$db_name);

     echo '<br/>

<div class="row">

<div class="col-md-12">

    <div class="page-header">

      <h1 class="media-center text-right">Bienvenido/a '.$row["nombres"].'</h1>

    </div>

    <p>En este apartado podra ver la lista de todas las reservaciones que ha realizado en nuestros hoteles, tambien podra cambiar sus datos personales y la clave de acceso al sistema</p>

</div>

  <div class="col-md-8">

    <div class="panel panel-info" >

      <div class="panel-heading">

        <h3 class="panel-title">Mis reservaciones</h3>

      </div>

      <div id="RespuestaReserva"></div>

      <div class="panel-body" style="max-height: 500px; overflow-y: scroll;">

        

        <ul class="list-group">';

     $si = 0;

     $i = 0;

     while ($rowR=mysqli_fetch_array($resultR,MYSQLI_ASSOC)) {

         $si =1;

         $i++;

         $sqlH = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s' ORDER BY id DESC" ,

                         mysqli_real_escape_string($mysqli,$rowR["id_hotel"]));

         $resultH = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

         $rowH=mysqli_fetch_array($resultH,MYSQLI_ASSOC); 

         if($rowR["tipo_pago"] == 2 && $rowR["status_transac"] == 2){

            echo '

            <form id="formEnviarReferencia'.$rowR["id"].'" method="post" enctype="multipart/form-data" action="javascript:void(0)" role="form">

              <li class="list-group-item"><i class="fa fa-check"></i> <a href="javascript:void(0)"  onclick="javascript:VerReserva('.$rowR["id"].');" title="ver reservacion">'.$rowH["nombre_sitio"].' - '.$rowR["desde"].'</a><br>

              <p style="color: red;" id="recordatorio'.$rowR["id"].'">¡Esta reserva está pendiente del envío de información de la transferencia bancaria!</p>

              <label id="labelId'.$rowR["id"].'">Indique aquí N° Identificación de la transferencia: </label><input type="text" id="referencia'.$rowR["id"].'" name="referencia" placeholder="N° Identificador"/><br>

              <label id="labelArchivo'.$rowR["id"].'">Adjunte aquí el comprobante de transferencia: </label><input id="imagen'.$rowR["id"].'" name="imagen'.$rowR["id"].'" type="file" ><br>

              <button id="btnRef'.$rowR["id"].'" class="btnInstapagoA btn btn-primary" type="submit" name="botonConfirmaRef" onclick="enviarReferencia('.$rowR["id"].','.$rowR["total"].')" >Confirmar</button><p id="loaderRef'.$rowR["id"].'" class="bg-info text-center" style="display: none;">Procesando... Por favor espere.</p></li>

              <input type="hidden" name="id'.$rowR["id"].'" value="'.$rowR["id"].'"/>

              <input type="hidden" id="nombres'.$rowR["id"].'" value="'.$row["nombres"].'"/>

              <input type="hidden" id="apellidos'.$rowR["id"].'" value="'.$row["apellidos"].'"/>              

              <input type="hidden" id="sitio'.$rowR["id"].'" value="'.$rowH["nombre_sitio"].'"/>

              <input type="hidden" id="total'.$rowR["id"].'" value="'.$rowR["total"].'"/>

            </form>';

         }else{

            echo '

            <li class="list-group-item"><i class="fa fa-check"></i> <a href="javascript:void(0)"  onclick="javascript:VerReserva('.$rowR["id"].');" title="ver reservacion">'.$rowH["nombre_sitio"].' - '.$rowR["desde"].'</a></li>';

         }

     }

     while ($rowRP=mysqli_fetch_array($resultRP,MYSQLI_ASSOC)) {

         $si =1;

         $i++;

         $sqlH = sprintf("SELECT nombre_sitio FROM hesperia_settings WHERE id = '%s' ORDER BY id DESC" ,

                         mysqli_real_escape_string($mysqli,$rowRP["id_hotel"]));

         $resultH = QUERYBD($sqlH,$hostname,$user,$password,$db_name);

         $rowH=mysqli_fetch_array($resultH,MYSQLI_ASSOC); 

         if($rowRP["tipo_pago"] == 2 && $rowRP["status_transac"] == 2){

            echo '

            <form id="formEnviarReferencia'.$rowRP["id"].'" method="post" enctype="multipart/form-data" action="javascript:void(0)" role="form">

              <li class="list-group-item"><i class="fa fa-check"></i> <a href="javascript:void(0)"  onclick="javascript:VerReservaPaq('.$rowRP["id"].');" title="ver reservacion">'.$rowH["nombre_sitio"].' - '.$rowRP["nombre_paq"].'</a><br>

              <p style="color: red;" id="recordatorio'.$rowRP["id"].'">¡Esta reserva está pendiente del envío de información de la transferencia bancaria!</p>

              <label id="labelId'.$rowRP["id"].'">Indique aquí N° Identificación de la transferencia: </label><input type="text" id="referencia'.$rowRP["id"].'" name="referencia" placeholder="N° Identificador"/><br>

              <label id="labelArchivo'.$rowRP["id"].'">Adjunte aquí el comprobante de transferencia: </label><input id="imagen'.$rowRP["id"].'" name="imagen'.$rowRP["id"].'" type="file" ><br>

              <button id="btnRef'.$rowRP["id"].'" class="btnInstapagoA btn btn-primary" type="submit" name="botonConfirmaRef" onclick="enviarReferenciaPaq('.$rowRP["id"].','.$rowRP["total"].')" >Confirmar</button><p id="loaderRef'.$rowRP["id"].'" class="bg-info text-center" style="display: none;">Procesando... Por favor espere.</p></li>

              <input type="hidden" name="id'.$rowRP["id"].'" value="'.$rowRP["id"].'"/>

              <input type="hidden" id="nombres'.$rowRP["id"].'" value="'.$row["nombres"].'"/>

              <input type="hidden" id="apellidos'.$rowRP["id"].'" value="'.$row["apellidos"].'"/>              

              <input type="hidden" id="sitio'.$rowRP["id"].'" value="'.$rowH["nombre_sitio"].'"/>
              
              <input type="hidden" id="total'.$rowRP["id"].'" value="'.$rowRP["total"].'"/>

            </form>';

         }else{

            echo '

            <li class="list-group-item"><i class="fa fa-check"></i> <a href="javascript:void(0)"  onclick="javascript:VerReservaPaq('.$rowRP["id"].');" title="ver reservacion">'.$rowH["nombre_sitio"].' - '.$rowRP["nombre_paq"].'</a></li>';

         }

     }

     if ($si == 0)

     {   echo '<div class="alert alert-danger" role="alert">

                        <p>No existen reservaciones almacenadas!</p>

                    </div>'; }

     echo '

        </ul>

      </div>

    </div>

    </div>

  <div class="col-md-4">

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-success">

    <div class="panel-heading" role="tab" id="headingOne">

      <h4 class="panel-title">

        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">

          Datos Personales

        </a>

      </h4>

    </div>

    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">

      <div class="panel-body">

    <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" name="MiPanel" id="MiPanel" onsubmit="return Panel(); return document.MM_returnValue">

      <div class="form-group">

        <label class="sr-only" for="user-nombre">Nombres</label>

        <input type="text" class="form-control" id="user-nombre" name="nombres" value="'.$row["nombres"].'" placeholder="Nombres">

      </div>

      <div class="form-group">

        <label class="sr-only" for="user-apellidos">Apellidos</label>

        <input type="text" class="form-control" id="user-apellidos" name="apellidos" value="'.$row["apellidos"].'" placeholder="Apellidos">

      </div>

      <div class="form-group">

        <label class="sr-only" for="user-email">Correo Electrónico</label>

        <input type="email" class="form-control" id="email" name="email" value="'.$row["email"].'" placeholder="Correo Electrónico">

      </div>

      <div class="form-group">

        <label class="sr-only" for="user-telefono">Telefono de Contacto</label>

        <input type="text" class="form-control" id="telefono" name="telefono" value="'.$row["telefono"].'" placeholder="Telefono de Contacto">

      </div>

      <div class="form-group">

        <label class="sr-only" for="nombre-empresa">Nombre de Empresa</label>

        <input type="text" class="form-control" id="empresa" name="empresa" value="'.$row["empresa"].'" placeholder="Nombre de Empresa">

      </div>

      <div class="form-group">

        <label class="sr-only" for="user-telefono">Telefono de Empresa</label>

        <input type="text" class="form-control" id="numero_empresa" name="numero_empresa" value="'.$row["numero_empresa"].'" placeholder="Telefono de Empresa">

      </div>

      <div class="form-group">

        <label class="sr-only" for="user-email">Correo Electrónico Empresa</label>

        <input type="email" class="form-control" id="email_empresa" name="email_empresa" value="'.$row["email_empresa"].'" placeholder="Correo Electrónico Empresa">

      </div>

      <div class="form-group">

        <label for="fecha-registro">Fecha registro:</label>

        <input type="text" class="form-control" id="fecha_registro" name="fecha_registro" value="'.date("d-m-Y",$row["fecha_registro"]).'" readonly>

      </div>

      <div class="form-group">

        <label for="ultimo-login">Ultimo ingreso al sistema:</label>

        <input type="text" class="form-control" id="ultimo_login" name="ultimo_login" value="'.date("d-m-Y",$row["ultimo_login"]).'" readonly>

      </div>';

      if($row["pregunta_1"] == ''){

        $sqlPreg = sprintf("SELECT * FROM hesperia_v2_preguntas_seg WHERE  status = 'S'");

        $resultPreg = QUERYBD($sqlPreg,$hostname,$user,$password,$db_name);

        $i = 0;

        while ($rowsPreg=mysqli_fetch_array($resultPreg,MYSQLI_ASSOC)){

            $pregunta[$i] = $rowsPreg['pregunta'];

            $i++;

        }

        echo '

                <div class="form-group">

                    <center><label>Seleccione y responda a las siguiente preguntas de seguridad</label></center><br>

                        <select name="pregunta1" id="pregunta1"  class="form-control input-sm">

                            <option value="" selected>Seleccione:</option>';

                            for ($i=0; $i < 3; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                </div>

                <div class="form-group">

                        <input type="text" class="form-control" value="" id="respuesta1" name="respuesta1" >

                    </div>

                <div class="form-group">        

                        <select name="pregunta2" id="pregunta2"  class="form-control input-sm" >

                            <option value="" selected>Seleccione:</option>';

                            for ($i=3; $i < 6; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                </div>

                <div class="form-group">

                        <input type="text" class="form-control" value="" id="respuesta2" name="respuesta2" >

                    </div>

                <div class="form-group"> 

                        <select name="pregunta3" id="pregunta3"  class="form-control input-sm">

                            <option value="" selected>Seleccione:</option>';

                            for ($i=6; $i < 9; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                </div>

                    <div class="form-group">

                        <input type="text" class="form-control" value="" id="respuesta3" name="respuesta3" >

                    </div>

                <br>



                <input type="hidden" name="preguntas" value="0"/>

                    

        ';

      }else{

        echo '

            <div class="form-group">

                <label for="respuesta1">'.$row["pregunta_1"].'</label>

                <input type="text" class="form-control" value="" id="respuesta1" name="respuesta1" >

            </div>

            <div class="form-group">

                <label for="respuesta2">'.$row["pregunta_2"].'</label>

                <input type="text" class="form-control" value="" id="respuesta2" name="respuesta2" >

            </div>

            <div class="form-group">

                <label for="respuesta3">'.$row["pregunta_3"].'</label>

                <input type="text" class="form-control" value="" id="respuesta3" name="respuesta3" >

            </div>

            <input type="hidden" name="preguntas" value="1"/>

        ';

      }

      echo '

      <div id="recaptcha1"></div>

      <div class="form-group">

        <div id="Respperfil"></div>

      </div>

      <button type="submit" class="btn btn-primary btn-large btn-block" id="check" value="Check">Cambiar Datos</button>

    </form>

      </div>

    </div>

  </div>

  <div class="panel panel-warning">

    <div class="panel-heading" role="tab" id="headingTwo">

      <h4 class="panel-title">

        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

          Cambiar Clave de Acceso

        </a>

      </h4>

    </div>

    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">

      <div class="panel-body">';

     CAMBIO_CLAVE($mysqli,$data,$hostname,$user,$password,$db_name);

     echo '

      </div>

    </div>

  </div>

</div>

  </div>

</div>';

    } else { echo '<div class="alert alert-danger" role="alert">

        <p>No fue conseguido su usuario o su sesión expiro, intente nuevamente.</p>

</div>';}

    return;

}

//

// NO USAR ir al archivo funcion_reservacion.php

//

function PROCESO_RESERVA_OLD($mysqli,$data,$hostname,$user,$password,$db_name)

{	if (!isset($_POST["hotel"])) { $hotel = 1;}

 else { $hotel = $_POST["hotel"]; }

 $sql = sprintf("SELECT razon_social FROM hesperia_settings WHERE id = '$hotel'");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);

 if (isset($_POST["dpd1"])) { $desde = $_POST["dpd1"];  }

 else { $desde = date("d/m/Y",time()); }

 if (isset($_POST["dpd2"])) { $hasta = $_POST["dpd2"]; }

 else { $hasta = date("d/m/Y",time()); }

 if (isset($_POST["adultos"])) { $adultos = $_POST["adultos"]; }

 else { $adultos = 0 ; }

 if (isset($_POST["ninos"])) { $ninos = $_POST["ninos"]; }

 else { $ninos = 0 ; }

 $desdeReserva = NORMAL_MYSQL($desde);

 $hastaReserva = NORMAL_MYSQL($hasta);

 $cantPersonasReserva = $adultos + $ninos;

 $dias_reservados = ($hastaReserva - $desdeReserva) / 86400;



 echo'

<!-- Comienza formulario de reserva -->

<div class="row">

    <div class="col-lg-12">

        <h4 class="text-center"><strong>'.strtoupper($rows["razon_social"]).'</strong></h4>

        <p class="text-center page-title page-title-responsive">

          Solicitud de reservación: desde <strong>'.$desde.'</strong> hasta <strong>'.$hasta.'</strong><br/>

           adulto(s): <strong>'.$adultos.'</strong> ni&ntilde;o(s): <strong>'.$ninos.'</strong>. <br/> Cantidad de personas solicitadas en reservación: <strong>'.$cantPersonasReserva.'</strong><br/>Días solicitados: <strong>'.$dias_reservados.'</strong><br/>





 <strong><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> Reiniciar Reservación</a></strong></p> ';





 echo '

            <div class="collapse" id="collapseExample">

              <div class="center-block">';



 CREAR_RESERVA_PAGO($mysqli,$hostname,$user,$password,$db_name);



 echo'         </div>

            </div>

    </div>

</div>

            <form enctype="application/x-www-form-urlencoded" class="reserva" action="javascript:void(0)" role="form" method="post" name="FormReserva" id="FormReserva" onsubmit="return Reservando(); return document.MM_returnValue">

            <div class="row">

                <div class="col-lg-12">

                    <div id="Respuestareservar"></div>

                </div>

            </div>';



 echo '

            <!-- mejor precio -->

            <div class="row">

            <hr/>';



 $sql = sprintf("SELECT * FROM hesperia_habitaciones

                        WHERE precio_promocion > '%s' && id_hotel = '%s'

                        && disponibles > '%s' ORDER BY RAND() limit 1",

                mysqli_real_escape_string($mysqli,0),

                mysqli_real_escape_string($mysqli,$hotel),

                mysqli_real_escape_string($mysqli,0));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 if ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $nHAB = $rows["id_habitacion"];

  $carpeta = strtolower(str_replace (' ','_',$rows["nombre_habitacion"]));

  $imagen = array("$rows[imagen1]","$rows[imagen2]","$rows[imagen3]");

  $imagen =array_filter($imagen);

  $item = array_rand($imagen);

  $precio_promocion = $rows["precio_promocion"];

  $imagen = 'img/habitaciones/'.$imagen[$item];

  $hab = $rows["id_habitacion"];

  $cantHab = $rows["cantidad_hab"];

  $DispHab = $rows["disponibles"];



  $sqlR = sprintf("SELECT disponible_dia

                         FROM hesperia_post_precio

                                WHERE fecha_precio >= '%s' && fecha_precio <= '%s'

                                && id_hotel = '%s' && id_habitacion = '%s'",

                  mysqli_real_escape_string($mysqli,$desdeReserva),

                  mysqli_real_escape_string($mysqli,$hastaReserva),

                  mysqli_real_escape_string($mysqli,1),

                  mysqli_real_escape_string($mysqli,1),

                  mysqli_real_escape_string($mysqli,$hotel),

                  mysqli_real_escape_string($mysqli,$hab));

  $resulR = QUERYBD($sqlR,$hostname,$user,$password,$db_name);

  $minimo = $DispHab;

  while ($rowsDisponible = mysqli_fetch_array($resulR,MYSQLI_ASSOC))

  { if ($minimo >= $rowsR["disponible_dia"])

  { $DispHab = $minimo = $rowsR["disponible_dia"]; }

  }



  $sqlR = sprintf("SELECT id_habitacion FROM hesperia_reservaciones

                                WHERE desde >= '%s' && hasta <= '%s'

                                 && aprobada = '%s' && pago = '%s'

                                && id_hotel = '%s' && id_habitacion = '%s'",

                  mysqli_real_escape_string($mysqli,$desdeReserva),

                  mysqli_real_escape_string($mysqli,$hastaReserva),

                  mysqli_real_escape_string($mysqli,1),

                  mysqli_real_escape_string($mysqli,1),

                  mysqli_real_escape_string($mysqli,$hotel),

                  mysqli_real_escape_string($mysqli,$hab));

  $resulR = QUERYBD($sqlR,$hostname,$user,$password,$db_name);

  $cantReserva = 0;

  while ($rowsReserva = mysqli_fetch_array($resulR,MYSQLI_ASSOC))

  { if ($cantReserva <= $rowsDisponible["disponible_dia"])

  { $cantReserva++; }

  }



  if ( $minimo > 0 )

  {  $DispHab = $minimo - $cantReserva; }



  $cantidadP = $adultos + $ninos;



  if ($ninos > 0) {

      echo '<br/><div class="col-sm-6 pull-right">

                <label><u><strong>NOTA</strong></u>: Califican como niños solo Menores de <strong>'.$data["minima_edad_nino"].' </strong>  años, mayores aplican como <u>Adultos</u>.</label>

                </div><br/>';

  }



  # Busca Mejor Precio

  $sqlMP = sprintf("SELECT MIN(precio) as Mprecio

                    FROM hesperia_post_precio

                    WHERE fecha_precio >= '%s' && fecha_precio <= '%s'

                    && id_habitacion = '%s' && precio > '0'",

                   mysqli_real_escape_string($mysqli,$desdeReserva),

                   mysqli_real_escape_string($mysqli,$hastaReserva),

                   mysqli_real_escape_string($mysqli,$hab));

  $resulMP = QUERYBD($sqlMP,$hostname,$user,$password,$db_name);

  if ($rowsMP = mysqli_fetch_array( $resulMP,MYSQLI_ASSOC))

  {  if ($rowsMP["Mprecio"] < $precio_promocion && !empty($rowsMP["Mprecio"]))

  { $precio_promocion = $rowsMP["Mprecio"]; }



  }

  # Fin Busca Mejor Precio



  #  echo '<div class="row cuadro">';

  /*

echo '

        <div class="col-lg-10">

            <div class="col-lg-4">

               <h3>Mejor precio disponible</h3>

                <img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-2" style="width:100%" alt="'.$carpeta.'" />

            </div>

            <div class="col-lg-8">

                <p class="text-uppercase">Habitación: '.$rows["nombre_habitacion"].'</p>

<p>M&aacute;xima capacidad: '.$rows["capacidad_personas"].' personas</p>

                '.$rows["descripcion"].'

            </div>

        </div>';

*/

  echo '



                <input type="hidden" name="cantPersonasReserva" id="cantPersonasReserva" value="'.$cantPersonasReserva.'"/>

                <input type="hidden" name="adultos" id="adultos" value="'.$adultos.'"/>

                <input type="hidden" name="ninos" id="ninos" value="'.$ninos.'"/>

                <input type="hidden" name="desde" id="desde" value="'.$desdeReserva.'"/>

                <input type="hidden" name="hasta" id="hasta" value="'.$hastaReserva.'"/>

                <input type="hidden" name="hotel" id="hotel" value="'.$hotel.'">

                <input type="hidden" name="test" id="test" value="1">

                <input type="hidden" name="capacidad['.$hab.']" id="capacidad['.$hab.']" value="'.$rows["capacidad_personas"].'"/>

                <input type="hidden" name="disponible['.$hab.']" id="disponible['.$hab.']" value="'.$DispHab.'"/>

                <input type="hidden" name="habitacion['.$hab.']" id="habitacion['.$hab.']" value="'.$hab.'">

                <input type="hidden" name="mejor['.$hab.']" id="mejor['.$hab.']" value="'.$hab.'">

                  ';

  /*

  if ($DispHab >0) {

      echo '

                                <label for="num">Reservar habitaciones</label>

                                <select class="form-control" id="NumHab['.$hab.']" name="NumHab['.$hab.']">

                                    <option value="" selected>Seleccione</option>';

      for ($i=1;$i<=($cantidadP);$i++)

      { echo '<option value="'.$i.'">'.$i.'</option>'; }

      echo '

                                </select>

                    </div>

                            <div class="form-group">

                            <label>Adultos:</label>

                           <select class="form-control" id="CantAdultosHab['.$hab.']" name="CantAdultosHab['.$hab.']">

                            <option value="" selected>Seleccione</option>';

      for ($a=1;$a<=$cantidadP;$a++)

      { echo '<option value="'.$a.'">'.$a.'</option>'; }

      echo '</select>

                            </div>';

      if ($ninos > 0) {

          echo '

                            <div class="form-group">

                                <label>Niños:</label>

                                <select class="form-control" id="CantninosHab['.$hab.']" name="CantninosHab['.$hab.']">

                                <option value="" selected>Seleccione</option>';

          for ($a=1;$a<=$ninos;$a++){

              echo '<option value="'.$a.'">'.$a.'</option>';

          }

          echo '</select>

                            </div>';

      }

      echo '

                    <button type="submit" class="btn main-btn btn-sm btn-lg btn-block pull-right">

                        Procesar

                    </button>';

  } else { echo '<div class="alert alert-danger" role="alert">

                        <p>No hay habitaciones de este tipo disponible para la(s) fecha(s) indicada(s)</p>

                        </div>'; }

*/



  # echo '

  #           </div></div>';

  # fin mejor precio

 }



 echo '	</div>

<!-- fin mejor precio -->';

 echo '

    <div class="row">

        <div class="col-lg-12">

            <h1 class="page-header">Tipos de Habitaci&oacute;n</h1>

        </div>

    </div>';



 microtime(true);

 $sql = sprintf("SELECT * FROM hesperia_habitaciones

                        WHERE  id_hotel = '$hotel' ORDER BY nombre_habitacion ASC");

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 $inicio = 0;

 while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)) {

     $inicio++;

     $carpeta = strtolower(str_replace (' ','_',$rows["nombre_habitacion"]));

     $imagen = array("$rows[imagen1]","$rows[imagen2]","$rows[imagen3]");

     $imagen =array_filter($imagen);

     $item = array_rand($imagen);

     $imagen = 'img/habitaciones/'.$imagen[$item];

     # Busca Mejor Precio

     $sqlMP = sprintf("SELECT MIN(precio) as Mprecio, MIN(disponible_dia) as Dispo

                    FROM hesperia_post_precio

                    WHERE fecha_precio >= '%s' && fecha_precio <= '%s'

                    && id_habitacion = '%s' && precio > '0'",

                      mysqli_real_escape_string($mysqli,$desdeReserva),

                      mysqli_real_escape_string($mysqli,$hastaReserva),

                      mysqli_real_escape_string($mysqli,$hab));

     $resulMP = QUERYBD($sqlMP,$hostname,$user,$password,$db_name);

     if ($rowsMP = mysqli_fetch_array( $resulMP,MYSQLI_ASSOC))

     {    $minimo = $DispHab = $rowsMP["Dispo"];

      $rows["precio"] = $rowsMP["Mprecio"];

     }

     # Fin busca Mejor precio

     echo '

    <div class="row cuadro">

        <div class="col-lg-12">

            <div class="col-lg-4">

                <img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-2" style="width:100%" alt="'.$carpeta.'" />

            </div>

            <div class="col-lg-6">

                <p class="text-uppercase">Habitación: '.$rows["nombre_habitacion"].'</p>

                <p>M&aacute;xima capacidad: '.$rows["capacidad_personas"].' personas</p>

                <p>Tipo de cama: '.$rows["tipo_cama"].'</p>

                <p>Tamaño: '.$rows["tamano_hab"].' Mts²</p>

                <!-- <p>Tipo: '.$rows["tipo_habitacion"].'</p> -->

                <p>Caracter&iacute;sticas:</p>

                <ul class="list-inline">';

     if ($rows["mini_bar"]) {

         echo '<li>Mini Bar | </li>'; }

     if ($rows["albornoz"]) {

         echo '<li>Albornoz (<i>Bata de Baño</i>) | </li>'; }

     if ($rows["telefono"]) {

         echo '<li>Teléfono |  </li>'; }

     if ($rows["tv"]) {

         echo '<li>TV | </li>'; }

     if ($rows["tv_cable"]) {

         echo '<li>TV Cable | </li>'; }

     if ($rows["control_remoto_tv"]) {

         echo '<li>Control Remoto TV | </li>'; }

     if ($rows["aire_acondicionado"]) {

         echo '<li>Aire Acondicionado | </li>'; }

     if ($rows["servicio_habitacion"]) {

         echo '<li>Servicio Habitación | </li>'; }

     if ($rows["desayuno"]) {

         echo '<li>Desayuno Incluído | </li>'; }

     if ($rows["ambiente_musical"]) {

         echo '<li>Ambiente Músical | </li>'; }

     if ($rows["lavanderia"]) {

         echo '<li>Serv. Lavandería | </li>'; }

     if ($rows["secador_pelo"]) {

         echo '<li>Secador de Cabello | </li>'; }

     if ($rows["caja_fuerte"]) {

         echo '<li>Caja Fuerte |</li>'; }

     if ($rows["jacuzzi"]) {

         echo '<li>Jacuzzi | </li>'; }

     if ($rows["area_estar"]) {

         echo '<li>Area de Estar | </li>'; }

     if ($rows["jarra_agua_vasos"]) {

         echo '<li>Jarra de agua/vasos | </li>'; }

     if ($rows["cafetera"]) {

         echo '<li>Cafetera | </li>'; }

     if ($rows["balcon"]) {

         echo '<li>Balcon | </li>'; }

     if ($rows["banera"]) {

         echo '<li>Bañera | </li>'; }

     if ($rows["servicio_medico"]) {

         echo '<li>Servicio Médico </li>'; }

     $hab = $rows["id_habitacion"];

     $cantHab = $rows["cantidad_hab"];

     $DispHab = $rows["disponibles"];

     $sqlR = sprintf("SELECT disponible_dia,precio

                         FROM hesperia_post_precio

                                WHERE fecha_precio >= '%s' && fecha_precio <= '%s'

                                && id_hotel = '%s' && id_habitacion = '%s'",

                     mysqli_real_escape_string($mysqli,$desdeReserva),

                     mysqli_real_escape_string($mysqli,$hastaReserva),

                     mysqli_real_escape_string($mysqli,$hotel),

                     mysqli_real_escape_string($mysqli,$hab));

     $resulR = QUERYBD($sqlR,$hostname,$user,$password,$db_name);

     if ($rowsR = mysqli_fetch_array($resulR,MYSQLI_ASSOC))

     { $minimo = $rowsR["disponible_dia"];  }



     $sqlR = sprintf("SELECT id_habitacion FROM hesperia_reservaciones

                                WHERE desde >= '%s' && hasta <= '%s'

                                 && aprobada = '%s' && pago = '%s'

                                && id_hotel = '%s' && id_habitacion = '%s'",

                     mysqli_real_escape_string($mysqli,$desdeReserva),

                     mysqli_real_escape_string($mysqli,$hastaReserva),

                     mysqli_real_escape_string($mysqli,1),

                     mysqli_real_escape_string($mysqli,1),

                     mysqli_real_escape_string($mysqli,$hotel),

                     mysqli_real_escape_string($mysqli,$hab));

     $resulR = QUERYBD($sqlR,$hostname,$user,$password,$db_name);

     $cantReserva = 0;

     while ($rowsReserva = mysqli_fetch_array($resulR,MYSQLI_ASSOC))

     { if ($cantReserva <= $rowsDisponible["disponible_dia"])

     { $cantReserva++; }

     }



     if ( $minimo > 0 )

     {  $DispHab = $minimo - $cantReserva; }

     echo '

                </ul><br/>

               <div> '.$rows["descripcion"].'</div>

            </div>

            <div class="col-lg-2">

                    <div class="form-group">

                        <label for="num">Reservar habitaciones</label>

                        <input type="hidden" name="capacidad['.$hab.']" id="capacidad['.$hab.']" value="'.$rows["capacidad_personas"].'"/>

                        <input type="hidden" name="disponible['.$hab.']" id="disponible['.$hab.']" value="'.$DispHab.'"/>

                        <input type="hidden" name="habitacion['.$hab.']" id="habitacion['.$hab.']" value="'.$hab.'">';





     if ($DispHab >0) {



         #   if ($nHAB != $hab) {

         echo '<select class="form-control" id="NumHab['.$hab.']" name="NumHab['.$hab.']">

                                            <option value="" selected>Seleccione</option>';

         for ($i=1;$i<=($cantidadP);$i++)

         { echo '<option value="'.$i.'">'.$i.'</option>'; }

         echo '

                                        </select>';

         #	}



         /*  else { echo '<span>Habitación aparece en <strong>Promoción</strong></span> <i>Mejor precio disponible</i>'; } */



         echo '

                    </div>';

         #   if ($nHAB != $hab) {

         echo '<div class="form-group"><label>Adultos:</label>

                           <select class="form-control" id="CantAdultosHab['.$hab.']" name="CantAdultosHab['.$hab.']">

                            <option value="" selected>Seleccione</option>';

         for ($a=1;$a<=$cantidadP;$a++)

         { echo '<option value="'.$a.'">'.$a.'</option>'; }

         echo '</select>

                            </div>';

         if ($ninos > 0) {

             echo '

                            <div class="form-group"><label>Niños:</label>

                           <select class="form-control" id="CantninosHab['.$hab.']" name="CantninosHab['.$hab.']">

                            <option value="" selected>Seleccione</option>';

             for ($a=1;$a<=$ninos;$a++)

             { echo '<option value="'.$a.'">'.$a.'</option>'; }

             echo '</select>

                            </div>';

         }



         echo '<button type="submit" class="btn main-btn btn-sm btn-lg btn-block pull-right">

                                            Procesar

                                    </button>';

         #    }

     }

     else { echo '<div class="alert alert-danger" role="alert">

                        <p>No hay habitaciones de este tipo disponible para la(s) fecha(s) indicada(s)</p>

                        </div></div>'; }



     echo '

            </div>

        </div>

    </div>';

     if ($inicio < $hay)

         echo '<br>';

 }

 $mejorP = array();

 echo    '

<input name="mejorP" value="'.$mejorP.'">

</form>';

 return;

}





function PROMOCIONES_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio)

{   

    $hoy = date("Y-m-d",time());

    $sql = sprintf("SELECT * FROM hesperia_paquetes WHERE  id_hotel = '%s' and alojamiento = 0 and status <> 'N' and partai = 0 ORDER BY orden ASC",

                   mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",

                   mysqli_real_escape_string($mysqli,$data["id"]));

     $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

     $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);

     $hotel = $rowSetting["razon_social"];

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';

  return; }

 while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC))

 { 



    $fecha = date("d/m/Y",time()+86400); 

    $nombre_paquete = $rowPaquetes["nombre_paquete"];

    $capacidad = $rowPaquetes["capacidad"];

  $asunto = str_replace(' ','+',$nombre_paquete);

  $desdePaq = '';

  $hastaPaq = '';

  $precio = $rowPaquetes["precio"];

  $habitacion = '';



  if($rowPaquetes["porcentaje"] > 0){

    $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_hotel = '%s' and id_habitacion = '%s'",

                   mysqli_real_escape_string($mysqli,$data["id"]),

                   mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));

     $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);

     $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);

     $habitacion = $rowHab["nombre_habitacion"];

     echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

  }



  echo '<form enctype="application/x-www-form-urlencoded" action="https://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >

        <div class="col-md-12 cuadro">

            <h2>'.$rowPaquetes["nombre_paquete"].'</h2>

            <div class="col-md-4">';

  if (!file_exists("img/paquetes/$rowPaquetes[img_paquete]")){

      echo'<img src="https://hoteleshesperia.com.ve/img/paquetes/sin-imagen.png" alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive"/>';

  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPaquetes["img_paquete"].'-3"  alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive" />';

  }echo'

            </div>

            <div class="col-md-8">

                '.$rowPaquetes["descripcion_paquete"].' <br>';

                

                if($rowPaquetes["precio"] > 0){

                    

                    /*if($rowPaquetes["precio_nino"] > 0){

                        echo '<strong>Precio: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>';

                        if($rowPaquetes["ninos_gratis"] ==  0){

                                echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Con un '.substr($rowPaquetes["porcentaje"], 2).'% de descuento aplicado)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }else{

                                echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').' (Incluidos '.$rowPaquetes["ninos_gratis"].' Niños Gratis)</strong><br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }

                    }else{

                        if($rowPaquetes["porcentaje"] > 0){

                            $precio = $rowPaquetes["precio"] - ($rowPaquetes["precio"] * $rowPaquetes["porcentaje"]);

                            echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                            <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').' con un '.substr($rowPaquetes["porcentaje"], 2, 2).'% de descuento aplicado</strong><br/>

                            <input type="hidden" name="numPaq" value="1"/>';

                            if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                            }

                        }else{

                            echo '<strong>Precio: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>';

                        }

                    }*/

                    if($rowPaquetes["precio_nino"] > 0){

                        echo '<strong>Precio por adulto: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>

                        <strong>Precio por niño: '.number_format(round($rowPaquetes["precio_nino"]), 2, ',', '.').'</strong><br/>';

                        if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                        }

                    }else{

                        if($rowPaquetes["porcentaje"] > 0){

                            $precio = $rowPaquetes["precio"] - ($rowPaquetes["precio"] * $rowPaquetes["porcentaje"]);

                            if($rowPaquetes["ninos_gratis"] ==  0){

                                echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Con un '.substr($rowPaquetes["porcentaje"], 2, 2).'% de descuento aplicado)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }else{

                                echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Incluidos '.$rowPaquetes["ninos_gratis"].' Niños Gratis)<br/>

                                <input type="hidden" name="numPaq" value="1"/>';

                            }

                            if($hoy < $rowPaquetes["desde"]){

                                $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                            }

                        }else{

                            echo '<strong>Precio: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>';

                        }

                        

                    }

            echo '

                <hr>

                <strong>Selecciona fecha de llegada<br> </strong><input type="text" class="dpd3 input-sm" value="'.$fecha.'" id="dpd3" name="dpd3" placeholder="Entrada" data-date-format="dd/mm/yyyy" data-min-date="'.$rowPaquetes["desde"].'" data-max-date="'.$rowPaquetes["hasta"].'" language="es" readonly="readonly">

                <input type="hidden" name="nombrePaq" value="'.$nombre_paquete.'"/>

                <input type="hidden" name="precioPaq" value="'.$precio.'"/>

                <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                <input type="hidden" name="id_hotel" value="'.$rowPaquetes["id_hotel"].'"/>

                <input type="hidden" name="tiempo" value="'.$rowPaquetes["tiempo"].'"/>

                <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                <input type="hidden" name="precioNino" value="'.$rowPaquetes["precio_nino"].'"/>

                <input type="hidden" name="porcentaje" value="'.$rowPaquetes["porcentaje"].'"/>

                <input type="hidden" name="partai" value="'.$rowPaquetes["partai"].'"/>

                <input type="hidden" name="habitacion" value="'.$habitacion.'"/>';

                if($rowPaquetes["precio_nino"] > 0){

                   echo'<div class="row">

                            <div class="col-sm-1"><strong>Adultos<br> 

                                <select name="numPaq">

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                            <div class="col-sm-1"><strong>Niños<br> 

                                <select name="numPaqN">

                                    <option value="0">0</option>

                                    <option value="1">1</option>

                                    <option value="2">2</option>

                                    <option value="3">3</option>

                                    <option value="4">4</option>

                                </select></strong>

                            </div>

                        </div>';

                }else{



                    if($rowPaquetes["porcentaje"] <= 0){

                        if($capacidad == 5){

                            echo'

                        <div class="row">

                                <div class="col-sm-6"><strong>Cantidad<br> 

                                    <select name="numPaq">                                    

                                        <option value="5">5</option>

                                        <option value="6">6</option>

                                        <option value="7">7</option>

                                        <option value="8">8</option>

                                        <option value="9">9</option>

                                        <option value="10">10</option>

                                        <option value="11">11</option>

                                        <option value="12">12</option>

                                        <option value="13">13</option>

                                        <option value="14">14</option>

                                        <option value="15">15</option>

                                        <option value="16">16</option>

                                        <option value="17">17</option>

                                        <option value="18">18</option>

                                        <option value="19">19</option>

                                        <option value="20">20</option>

                                    </select></strong>

                                </div>

                        </div>';

                        }else{

                        echo'

                        <div class="row">

                                <div class="col-sm-6"><strong>Cantidad<br> 

                                    <select name="numPaq">

                                        <option value="1">1</option>

                                        <option value="2">2</option>

                                        <option value="3">3</option>

                                        <option value="4">4</option>

                                        <option value="5">5</option>

                                        <option value="6">6</option>

                                        <option value="7">7</option>

                                        <option value="8">8</option>

                                        <option value="9">9</option>

                                        <option value="10">10</option>

                                        <option value="11">11</option>

                                        <option value="12">12</option>

                                        <option value="13">13</option>

                                        <option value="14">14</option>

                                        <option value="15">15</option>

                                        <option value="16">16</option>

                                        <option value="17">17</option>

                                        <option value="18">18</option>

                                        <option value="19">19</option>

                                        <option value="20">20</option>

                                    </select></strong>

                                </div>

                        </div>';

                        }

                    }

                }

                echo'

                <br>

                <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>';

            }

            echo '

            </div>

          </div>

        </form>';

 }

 return;

}



function RECUPERAR($hostname,$user,$password,$db_name,$data)

{echo '<div class="app-title">

          <h1>Recuperar Clave de Acceso</h1>

    </div>

    <p style="text-align:center;padding-bottom:5px;">Ingrese su correo electrónico para solicitar una nueva clave de acceso</p>

    <div class="main-content">

        <form enctype="application/x-www-form-urlencoded" class="form-horizontal" action="javascript:void(0)" role="form" method="post" name="RecuperaClaveUsuario" id="RecuperaClaveUsuario" onsubmit="return RecuperarClaveLogin(); return document.MM_returnValue">

            <div id="recuperaClave"></div>

              <div class="control-group">

                    <input type="email" class="login-field" placeholder="e-Mail" name="username" id="username">

                    <input type="hidden" name="contacto" value="'.$data["correo_contacto"].'"/>

                    <input type="hidden" name="dominio" value="'.$data["dominio"].'"/>

                </div>

                <button type="submit" class="btn btn-primary btn-large btn-block" id="check" value="Check">Recuperar Clave</button>

        </form>

    </div>';

 return;

}



function REGISTRO($data,$hostname,$user,$password,$db_name)

{ # Formulario de registro

    $sql = sprintf("SELECT * FROM hesperia_v2_preguntas_seg WHERE  status = 'S'");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $i = 0;

    while ($rows=mysqli_fetch_array($result,MYSQLI_ASSOC)){

        $pregunta[$i] = $rows['pregunta'];

        $i++;

    }

    echo '

<div class="modal fade" id="modal-container-433666" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-dialog">

    <div class="modal-content mymod">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h2 class="modal-title" id="myModalLabel"> Registro en el Sistema</h2>

      </div>

      <div class="modal-body">

            <div id="AccionMensajeRegistro"></div>

            <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return Regverificar(); return document.MM_returnValue" name="Registro" id="Registro" >

                <div class="row">

                    <div class="col-sm-6">

                        <input type="text"  class="form-control input-sm"  value="" placeholder="Nombres" name="nombres" id="nombres" onkeypress="return soloLetras(event)">

                    </div>

                    <div class="col-sm-6">

                        <input type="text" class="form-control input-sm"  value="" placeholder="Apellidos" name="apellidos" id="apellidos" onkeypress="return soloLetras(event)">

                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-sm-6">

                        <input type="email"  class="form-control input-sm"  value="" placeholder="e-Mail" name="email" id="email">

                    </div>

                    <div class="col-sm-6">

                        <input type="tel"  class="form-control input-sm"  value="" placeholder="Telefono" name="telefono" onkeypress="return numeros(event)">

                        <input type="hidden" name="contacto" value="'.$data["correo_contacto"].'"/>

                        <input type="hidden" name="dominio" value="'.$data["dominio"].'"/>

                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-sm-6">

                        <input type="text"  class="form-control input-sm"  value="" placeholder="Cédula" name="cedula" id="cedula" onkeypress="return numeros(event)">

                    </div>

                    <div class="col-sm-6">

                        <input type="text" class="form-control input-sm" value="" id="fechaNac" name="fechaNac" placeholder="Fecha de Nacimiento" data-date-format="yyyy-mm-dd" language="es" readonly="readonly">

                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-sm-6">

                        <input type="password"  class="form-control input-sm"  value="" placeholder="Clave" name="clave" id="clave">

                    </div>

                    <div class="col-sm-6">

                        <input type="password"  class="form-control input-sm"  value="" placeholder="Repetir Clave" name="rclave" id="rclave">

                    </div>

                </div>

                <br>

                <div class="row">

                    <center><label>Seleccione y responda a las siguiente preguntas de seguridad</label></center><br>

                    <div class="col-sm-4">

                        <select name="pregunta1" id="pregunta1"  class="form-control input-sm">

                            <option value="" selected>Seleccione:</option>';

                            for ($i=0; $i < 3; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                    </div>

                    <div class="col-sm-4">

                        <select name="pregunta2" id="pregunta2"  class="form-control input-sm" >

                            <option value="" selected>Seleccione:</option>';

                            for ($i=3; $i < 6; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                    </div>

                    <div class="col-sm-4">

                        <select name="pregunta3" id="pregunta3"  class="form-control input-sm">

                            <option value="" selected>Seleccione:</option>';

                            for ($i=6; $i < 9; $i++) { 

                                echo '<option value="'.$pregunta[$i].'">'.$pregunta[$i].'</option>';

                            }

                        echo '

                        </select>

                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-sm-4">

                        <input type="text" class="input-sm" value="" id="respuesta1" name="respuesta1" >

                    </div>

                    <div class="col-sm-4">

                        <input type="text" class="input-sm" value="" id="respuesta2" name="respuesta2" >

                    </div>

                    <div class="col-sm-4">

                        <input type="text" class="input-sm" value="" id="respuesta3" name="respuesta3" >

                    </div>

                </div>                

                <br>

            <button type="submit" class="btn btn-primary btn-sm btn-block" id="check" value="Check">Registrarse</button>

            </form>

     </div>

    </div>

  </div>

</div>';

    return;

}



function SALONES_LISTADO($mysqli,$data,$hostname,$user,$password,$db_name)

{   $sql = sprintf("SELECT * FROM hesperia_salones

                                    WHERE  id_hotel = '%s'

                                     ORDER BY nombre_Salon ASC",

                   mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay salones en este Hotel.</p></div>';

  return; }

 while ($rowSalon=mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $nombre_paquete = $rowSalon["nombre_Salon"];

  $asunto = str_replace(' ','+',$nombre_paquete);

  echo '

<div class="col-md-12 cuadro text-justify">

    <h2 class="page-header text-uppercase">'.$rowSalon["nombre_Salon"].'</h2>

    <div class="col-md-4">';

  if (!file_exists("img/salones/$rowSalon[imagen]")){

      echo'<img src="https://hoteleshesperia.com.ve/img/salones/sin-imagen.png" alt="'.$rowSalon["nombre_Salon"].'" title="'.$rowSalon["nombre_Salon"].'" class="img-responsive"/>';

  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$rowSalon["imagen"].'-11"  alt="'.$rowSalon["nombre_Salon"].'" class="img-responsive" />';

  }echo'

    </div>

    <div class="col-md-8">

    '.$rowSalon["caracteristicas"].'<br/>

        <small class="pull-right">

                <a href="https://hoteleshesperia.com.ve/contacto/'.toAscii($nombre_paquete).'" title="Solicitar más Información de este Salón" class="btn btn-primary" target="_self"> Información y reservas.</a></small>

    </div>

    <hr>

</div>';

 }

 return;

}



function TAB_HABITACIONES($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio)

{ $sql = sprintf("SELECT * FROM hesperia_habitaciones WHERE  id_hotel = '%s'

                        ORDER BY nombre_habitacion ASC",

                 mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $hay = mysqli_num_rows($result);

 if ($hay < 1)

 { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay habitaciones en este Hotel.</p></div>';

  return; }

 while ($rowHab=mysqli_fetch_array($result,MYSQLI_ASSOC)) {

     $carpeta = strtolower($rowHab["nombre_habitacion"]);

     $carpeta = str_replace(' ','_',$carpeta);

     echo '

    <div class="col-md-12 cuadro text-justify">

        <h2>Habitaci&oacute;n: '.$rowHab["nombre_habitacion"].'</h2>

        

        Ocupación: máximo <strong>'.$rowHab["capacidad_personas"].'</strong> personas</p>

        <div class="col-md-8">

            <div class="table-responsive">';

     if ($rowHab["mini_bar"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/minibar.jpg" alt="Minibar" title="Minibar"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["albornoz"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/bata.jpg" alt="Bata de Baño / Albornoz" title="Bata de Baño / Albornoz"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["telefono"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/telefonoenhabitacion.jpg" alt="Telefono Habitacion" title="Telefono Habitacion"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["tv"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/tv.jpg" alt="TV" title="TV"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["aire_acondicionado"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/aireacondicionado.jpg" alt="Aire Acondicionado"  title="Aire Acondicionado"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["servicio_habitacion"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/servicioalahabitacion.jpg" alt="Servicio Habitacion" title="Servicio Habitacion"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["desayuno"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/desayuno.jpg" alt="Desayuno" title="Desayuno" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["secador_pelo"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/secadordepelo.jpg" alt="Secador de Pelo"title="Secador de Pelo" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["lavanderia"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/lavanderia.jpg" alt="Lavanderia" title="Lavanderia"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["caja_fuerte"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/cajafuerte.jpg" alt="Caja Fuerte" title="Caja Fuerte" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["jacuzzi"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/jacuzzi.jpg" alt="Jacuzzi" title="Jacuzzi" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["area_estar"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/area_estar.jpg" alt="Area de Estar" title="Area de Estar" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["jarra_agua_vasos"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/jarra_agua_vasos.jpg" alt="Jarra de Agua" title="Jarra de Agua" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["cafetera"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/cafetera.jpg" alt="Cafetera" title="Cafetera" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["servicio_medico"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/serviciomedico.jpg" alt="Servicio Medico" title="Servicio Medico" style="float:left; margin-right:8px"/>'; }

     if ($rowHab["balcon"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/balcon.jpg" alt="Balcon"  title="Balcon"  style="float:left; margin-right:8px"/>'; }

     if ($rowHab["banera"]== 1)

     { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/banera.jpg" alt="Bañera / Tina de Baño" title="Bañera / Tina de Baño" style="float:left; margin-right:8px"/>'; }

     echo '

            </div>

            <br/>

            <div class="text-justify">

                '.$rowHab["descripcion"].'

            </div>';

     echo '

        </div>

        <div class="col-md-4">';

     IMAGENES_HABITACION($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio,$rowHab,$carpeta);

     echo '

         </div>

        </div>

';

 }

 return;

}



function TAB_INFORMACION($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio,$video)

{echo '

<div class="row">

  <div class="col-md-4"><br/>';

 if ($data["wifi"] == 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/wifi.jpg" alt="Wifi" title="Wifi"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["terraza"] == 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/terraza.jpg" alt="Terraza" title="Terraza"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["estacionamiento"] == 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/estacionamiento.jpg" alt="Estacionamiento Privado" title="Estacionamiento Privado" style="float:left; margin-right:8px;margin-bottom:5px" />'; }

 if ($data["piscina"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/piscina.jpg" alt="Piscina" title="Piscina" style="float:left; margin-right:8px;margin-bottom:5px;margin-bottom:5px"/>'; }

 if ($data["botones"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/botones.jpg" alt="Servicio de Botones"  title="Servicio de Botones"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["mascotas"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/mascotas.jpg" alt="Se permiten Mascotas" title="Se permiten Mascotas"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["gimnasio"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/gimnasio.jpg" alt="Gimnasio"  title="Gimnasio" style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["spa"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/spa.jpg" alt="Spa" title="Spa"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["helipuerto"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/helipuerto.jpg" alt="Helipuerto" title="Helipuerto"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["accesibilidad"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/accesibilidad.jpg" alt="Accesibilidad" title="Accesibilidad"   style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["nana"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/nana.jpg" alt="Servicio de Niñera" title="Servicio de Niñera"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["transporte"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/taxi.jpg" alt="Servicio de Taxi" title="Servicio de Taxi"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["vigilancia_privada"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/vigilancia_privada.jpg" alt="Vigilancia Privada" title="Vigilancia Privada"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["cafetin"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/cafetin.jpg" alt="Cafetin" title="Cafetin"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["restaurant"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/restaurant.jpg" alt="Restaurant" title="Restaurant"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["bar"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/bar.jpg" alt="Bar" title="Bar"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["discotheca"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/discoteca.jpg" alt="Discotheca" title="Discotheca"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["golf"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/golf.jpg" alt="Cancha de Golf" title="Cancha de Golf" style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["tenis"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/tenis.jpg" alt="Cancha de Tenis"  title="Cancha de Tenis" style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["basket"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/basket.jpg" alt="Cancha de Basket" title="Cancha de Basket"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["parque"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/parque.jpg" alt="Parque de Niños" title="Parque de Niños"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 if ($data["tienda"]== 1)

 { echo '<img src="https://hoteleshesperia.com.ve/img/icon_hotel/tiendas.jpg" alt="Tienda" title="Tienda"  style="float:left; margin-right:8px;margin-bottom:5px"/>'; }

 echo'<br/>

<div id="address" style="clear:both;margin-top:20px">

        <address>

            <abbr title="Name">

                <strong>'.$data["razon_social"].'</strong>

            </abbr><br />

            <abbr title="Address">

                Dirección: '.$data["direccion"].'<br />

                '.$data["ciudad"].' - '.$data["estado"].' - '.$data["pais"].'<br/>Código Postal:'.$data["codigo_postal"].'

            </abbr><br />

            <abbr title="Phone">

                <img src="https://hoteleshesperia.com.ve/img/icon_hotel/phone-icon.png" alt="Telf"/> '.$data["telefono"].'

            </abbr><br />

            <abbr title="Email Reservacion">

                <img src="https://hoteleshesperia.com.ve/img/icon_hotel/icon_mini_mail.png" alt="Reservacion"/> '.$data["correo_envio"].'

            </abbr><br/>

            <abbr title="Información">

                <img src="https://hoteleshesperia.com.ve/img/icon_hotel/icon_mini_mail.png" alt="Información"/> '.$data["correo_contacto"].'

            </abbr><br/>

        </address>

         <p>

            La hora de entrada (check in) al hotel es: '.$data["check_in"].'<br/>

            La hora de salida (check out) del hotel es: '.$data["check_out"].'

        </p>

        <div class="col-md-12">

            <iframe src="include/mapa.php" width="100%" height="250" frameborder="0" style="border:0"></iframe>

        </div>
    </div>

  </div>

  <div class="col-md-8">

        <h2>

            Informaci&oacute;n

        </h2>

        '.utf8_decode($data["descripcion"]).'

        <hr/>
  </div>

</div>

<div class="row">

<!--    <div class="col-md-4">

        <h2>Distancias</h2>

        <ul>';

 /*$sql = sprintf("SELECT * FROM hesperia_distancia WHERE  id_hotel = '%s' limit 1",

                mysqli_real_escape_string($mysqli,$data["id"]));

 $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

 $rowDistancia=mysqli_fetch_array($result,MYSQLI_ASSOC);

 if (!empty($rowDistancia["nombre_aeropuerto"])){

     echo '<li>'.utf8_encode($rowDistancia["nombre_aeropuerto"]).' - '.number_format($rowDistancia["aeropuerto"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["nombra_parque"])){

     echo '<li>Parques: '.utf8_encode($rowDistancia["nombra_parque"]).' - '.number_format($rowDistancia["parques_nacionales"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["centro_ciudad"])){

     echo '<li>Centro de la Ciudad: '.number_format($rowDistancia["centro_ciudad"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["nombre_museo"])){

     echo '<li>'.utf8_encode($rowDistancia["nombre_museo"]).' - '.number_format($rowDistancia["museo"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["nombre_estacion"])){

     echo '<li>Estación del metro: '.utf8_encode($rowDistancia["nombre_estacion"]).' - '.number_format($rowDistancia["metro"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["playas"])){

     echo '<li>Playas: '.number_format($rowDistancia["playas"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["centro_comercial"])){

     echo '<li>Centro Comercial: '.number_format($rowDistancia["centro_comercial"], 2, ',', '.').' Km(s)</li>';

 }

 if (!empty($rowDistancia["banco"])){

     echo '<li>Banco: '.number_format($rowDistancia["banco"], 2, ',', '.').' Km(s)</li>';

 }*/

 echo '

        </ul>

    </div> este es el div col-md-4 -->

</div><br/>';

 if (isset($video) && !empty($video)) {

     echo '

<div class="row">

      <div class="col-md-12">

        <iframe width="100%" height="420" src="https://www.youtube.com/embed/'.$video.'" frameborder="0" allowfullscreen=""></iframe>

      </div>

</div>';
 //var_dump($data["id"]);
   if ($data["id"] != 1 && $data["id"]!=5) {

     echo '

    <div class="row">

          <div class="col-md-12">

            <iframe width="100%" height="420" src="https://www.youtube.com/embed/mX3f0dZ7bRM" frameborder="0" allowfullscreen=""></iframe>

          </div>

    </div>';
   }

 }

 if ($data["escapadas"] == 1 && !empty($rowServicio["texto_escapada"])) {

     echo '

    <div class="row">

        <div class="col-md-4">

            <h2>Paquetes</h2>';

     if (!empty($rowServicio["imagen_escapada"])) {

         $imagen = $data["carpeta"].'/'.$rowServicio["imagen_escapada"];

         echo '<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-3" alt="Escapadas '.$data["razon_social"].'" class="img-responsive"/>';

     }

     echo'

        </div>

        <div class="col-md-8">

            <p class="text-justify">'.utf8_encode($rowServicio["texto_escapada"]).'</p>

                <a href="#0" type="button" class="btn btn-primary btn-lg btn-block btn-sm">

                    Solicitar Ahora

                </a>

        </div>

    </div>';

 }

 return;

}



function TEXTOS($data,$hostname,$user,$password,$db_name,$mysqli) {



    $sql = sprintf("SELECT * FROM hesperia_contenido

                    WHERE id_categoria ='%s' limit 1",

                   mysqli_real_escape_string($mysqli,$_GET["id"]));

    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

    $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);

    $sql2 = sprintf("SELECT nombre_categoria FROM hesperia_categoria

                    WHERE id ='%s'  limit 1",

                    mysqli_real_escape_string($mysqli,$_GET["id"]));

    $result2=QUERYBD($sql2,$hostname,$user,$password,$db_name);

    $rows2 = mysqli_fetch_array($result2,MYSQLI_ASSOC);

    echo '<div class="row">

    <div class="col-md-12">';

    if (!file_exists("img/contenido/$rows[imagen]")){

        echo'<img src="https://hoteleshesperia.com.ve/img/contenido/sin-imagen.png" alt="'.strtoupper(str_replace('-',' ',$rows2["nombre_categoria"])).'" class="img-responsive"/>';

    }else{

        $imagen = 'img/contenido/'.$rows["imagen"];

        echo'<img src="https://hoteleshesperia.com.ve/'.$imagen.'" class="img-responsive center-block img-us" alt="'.strtoupper(str_replace('-',' ',$rows2["nombre_categoria"])).'" title="'.strtoupper(str_replace('-',' ',$rows2["nombre_categoria"])).'">';

    }



    echo'

        <div class="page-header">

            <h1 class="about_us">'.utf8_encode($rows2["nombre_categoria"]).'</h1>

        </div>

       '.$rows["texto"].'<br/>

    </div>

</div>';



    return;

}



function SALIDA($hostname,$user,$password,$db_name)

{ # Salida del sistem

    $sql=sprintf("DELETE FROM hesperia_sesion WHERE referencia='$_SESSION[referencia]'");

    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

    session_unset();

    session_destroy();

    unset($_SESSION["referencia"]);

    $_POST=$_GET=$_SESSION=array();

    $tok=md5(time());

    echo '<meta http-equiv="refresh" content="0; url=https://hoteleshesperia.com.ve/logout"/>';

    die();

    return;

}



function TRABAJA($mysqli,$hostname,$user,$password,$db_name,$data)

{ # Formulario de solicitud de trabajo

    $sqlHoteles = sprintf("SELECT * FROM hesperia_settings");

    $resultHoteles = QUERYBD($sqlHoteles,$hostname,$user,$password,$db_name);

    echo '

<div class="row">

    <div class="col-lg-12">

        <h3 class="page-title text-center">FORMULARIO SOLICITUD DE EMPLEO</h3>

        <p>En <strong>Hesperia Hotels & Resorts</strong> estamos en la búsqueda de nuevos talentos que deseen formar parte de nuestro excelente equipo de trabajo en constante crecimiento. Si eres una persona pro-activa, creativa y emprendedora, te gustan los retos y tienes deseos de aprender y compartir lo que conoces, no dudes en llenar el siguiente formulario de solicitud de empleo. Una vez disponible una posición en nuestra organización te estaremos contactando para la entrevista.

        </p>

    <hr/>

    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="col-lg-12">
            <form enctype="multipart/form-data" action="javascript:void(0)" role="form" method="post" name="FormTrabajo" id="FormTrabajo" onsubmit="return trabajo(); return document.MM_returnValue">
            <div class="row">
                <div class="col-md-12">
                    <div id="Respuestrabajo">';
    if (isset($_SESSION["id_C"])) {
        $id = $_SESSION["id_C"];
        $sql = sprintf("DELETE FROM hesperia_trabajo WHERE id_curriculum = '%s' limit 1",
                       mysqli_real_escape_string($mysqli,$id));
        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    }
     echo ' 		</div>
                    <div class="form-group">

                        <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off" placeholder="Nombre y Apellidos">

                        <input type="hidden" name="contacto" id="contacto" value="'.$data["correo_contacto"].'"/>

                        <input type="hidden" name="dominio" id="dominio" value="'.$data["dominio"].'"/>

                        <input type="hidden" name="razon_social" id="razon_social" value="'.$data["razon_social"].'"/>

                        <input type="hidden" name="ahora" id="ahora" value="'.time().'"/>

                    </div>
                    <div class="form-group">
                        <select class="form-control" name="ciudad">
                        <option value="-1">Ciudad de Residencia</option>';
                $sqlCiudades = sprintf("SELECT * FROM ciudades ORDER BY ciudad");

                $resultCiudades = QUERYBD($sqlCiudades,$hostname,$user,$password,$db_name);                        
                        
                while ($rowsCiudades = mysqli_fetch_array($resultCiudades,MYSQLI_ASSOC)) { 
                echo '<option value="'. utf8_encode($rowsCiudades["ciudad"]) .'">'. utf8_encode($rowsCiudades["ciudad"]) .'</option>';
                }
                echo '
                        </select>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputName2">Seleccione Su Especialidad</label><hr>';
                $especialidad = [
                    [
                        'nombre' => 'Cocina'
                    ],
                    [
                        'nombre' => 'Restaurantes y Bares'
                    ],
                    [
                        'nombre' => 'Recepción'
                    ],
                    [
                        'nombre' => 'Ama de Llaves'
                    ],
                    [
                        'nombre' => 'Gerencia General'
                    ],
                    [
                        'nombre' => 'Administración y Finanzas'
                    ],
                    [
                        'nombre' => 'Comercial y marketing'
                    ],
                    [
                        'nombre' => 'Limpieza'
                    ],
                    [
                        'nombre' => 'Seguridad'
                    ],
                    [
                        'nombre' => 'Reservas'
                    ],
                    [
                        'nombre' => 'Animación'
                    ],
                    [
                        'nombre' => 'Mantenimiento'
                    ],
                ];
                foreach ($especialidad as $value) { 
                echo '
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox1" name="check_list[]" value="'. $value["nombre"]  .'"> '. $value["nombre"] .'
                    </label><br>';
                }
                $formacion = [
                    [
                        'nombre' => ' Sin estudios'
                    ],  
                    [
                        'nombre' => 'Bachiller'
                    ], 
                    [
                        'nombre' => 'TSU'
                    ], 
                    [
                        'nombre' => 'Diplomado'
                    ], 
                    [
                        'nombre' => 'Licenciado'
                    ], 
                    [
                        'nombre' => 'Doctorado'
                    ],   
                ];
                echo '
                    </div>
                    <div class"form-group">
                        <select class="form-control" name="formacion">
                          <option value="-1">Formación Academica</option>';

                foreach ($formacion as $value) { 
                echo '
                    <option value="'. $value["nombre"] .'">'. $value["nombre"] .'</option>';
                }  
                echo '
                        </select>
                    </div>
                    <br>
                    <div class"form-group">
                        <select class="form-control" name="hotelpreferencia">
                          <option value="-1">Hotel de preferencia para trabajar</option>';
                          while ($rows = mysqli_fetch_array($resultHoteles,MYSQLI_ASSOC)) {
                           echo '<option value="'.$rows["razon_social"].'">'.$rows["razon_social"].'</option>';
                          }
                    echo '<option value="Cualquier Hotel">Cualquier Hotel</option>
                        </select>
                    </div>  <br>
                    <div class="form-group">

                        <input type="email" class="form-control" name="email" id="email" autocomplete="off" placeholder="E-mail">

                    </div>

                    <div class="form-group">

                        <input type="text" class="form-control" name="telefono" id="telefono" autocomplete="off" placeholder="Tel&eacute;fono Movil">

                    </div>

                    <div class="form-group">

                        <input type="text" class="form-control" name="telefono_habi" id="telefono_habi" autocomplete="off" placeholder="Tel&eacute;fono Habitaci&oacute;n">

                    </div>

                    <div class="form-group">

                        <textarea class="form-control" name="direccion" id="direccion" autocomplete="off" placeholder="Direccion" rows="6"></textarea>

                    </div>

                    <div class="form-group">

                        <input type="file" class="form-control" name="archivo" id="archivo" autocomplete="off" accept="application/pdf">

                        <p class="help-block"><strong>Nota:</strong> Solo archivos .pdf </p>

                    </div>

                </div>

                <div class="form-group">';

    if (!isset($_SESSION["clickN"])) {

        echo '<button type="submit" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="bottom" title="Haga Clic dos veces">

                            Enviar Solicitud

                          </button>'; }

    else {

        echo '<button type="submit" class="btn btn-primary pull-right" disabled>Ya envio su solicitud</button>'; }

    echo '     </div>

        </form>

        </div>

    </div>
</div>';

    return;

}



function UBICACION($hostname,$user,$password,$db_name,$data)

{ # Ubicacion del hotel

    echo '

<div class="app-title">

    <img src="https://hoteleshesperia.com.ve/img/logo/logo_colegio.jpg" class="img-login" alt="Logo">

    <h1>Nuestra Ubicaci&oacute;n</h1>

</div>

<div class="main-content">

        <div style="float:right;margin-bottom:10px"><a href="#" onclick="window.history.back();return false;" title="Regresar a la pagina anterior">Regresar</a></div>';

    if (!empty($data['latitud']) && !empty($data['longitud'])) {

        echo'<div class="ubicacion">

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15705.601307791372!2d-68.012095!3d10.229261!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x86f3057374cc191b!2sColegio+de+Contadores+P%C3%BAblicos+del+Estado+Carabobo!5e0!3m2!1ses-419!2sve!4v1429589528571" width="99%" height="300" frameborder="0" style="border:0"></iframe>

<br/></div>'; }

    FORM_CLIENTE($data,$hostname,$user,$password,$db_name);

    echo '<br/>

    <address>

        <strong>'.$data['razon_social'].'</strong>

        <br/><abbr title="Ubicacion">'.$data['direccion'].'

        <br/>'.$data['ciudad'].' - '.$data['estado'].'</abbr>

        <br/>'.$data['pais'].' '. $data['codigo_postal'].'

        <br/><abbr title="Telefono">Telef&oacute;no(s):'.$data['telefono'].'</abbr>

    </address>

</div>';

    return;

}



function VER_GALERIA($mysqli,$data,$hostname,$user,$password,$db_name){

    $cantidad=60;

    if (isset($_GET["primera"]))

    { $primera=htmlentities($_GET["primera"]);

     $pg=htmlentities($_GET["pg"]);

     $temp=$pg;

     settype($primera,integer); }

    if (!isset($primera))

    {	$primera=$pg=$temp =1;

     $inicial=$paginacion=0; }

    else

    {	$pg=$_GET["pg"]; }

    if (!isset($_GET["pg"])){ $pg = 1;}

    else {$pg = $_GET["pg"]; }

    if ($temp>=$pg && $primera !=1)

    { $inicial=$pg * $cantidad + 1;  }

    else {  $inicial=$pg * $cantidad - $cantidad; }

    $sql = sprintf("SELECT * FROM hesperia_imagenes_prensa");

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $total_imagenes=mysqli_num_rows($result);

    $pages=@intval($total_imagenes / $cantidad) + 1;

    $titulo = array();

    $sql = sprintf("SELECT * FROM hesperia_imagenes_prensa

                  ORDER BY  fecha  DESC LIMIT %s,%s",

                   mysqli_real_escape_string($mysqli,$inicial),

                   mysqli_real_escape_string($mysqli,$cantidad));

    $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1)

    { echo '<br/><div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Disculpe</h2> <p>No hay imagenes disponibles en esta galeria.</p></div>';

     return; }

    echo '

<div class="page-header">

  <h3 class="g-prensa text-uppercase">Galeria de Imagenes Prensa</h3>

</div>';

    while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

    { $imagen = $rows["imagen"];

     $id = $rows["id"];

     $texto =  $rows["texto"];

     echo'



    <div class="col-sm-6 col-md-4">

        <div class="thumbnail">

           <img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-15" alt="'.$rows["titulo"].'" title="'.$rows["imagen"].'" class="panel-image-preview" />

            <div class="caption">

                <h3>'.$rows["titulo"].'</h3>

                <p>

                    <!--a href="i-descarga.php?imagen='.$imagen.'"  data-toggle="tooltip" data-placement="bottom" title="Descargar esta imágen" -->

                    <a href="img/prensa/'.$imagen.'" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Descargar esta imágen">

                    <i class="fa fa-download"></i> Descargar imágen

                    </a>

                </p>

            </div>

        </div>

    </div>';

    }

    echo '

    <div class="row">

        <div class="col-md-12"> ';

    PAGINACION_GALERIA($mysqli,$data,$hostname,$user,$password,$db_name,$total_imagenes,$pages,$cantidad,$pg);

    echo '

        </div>

    </div>';

    return;

}



function VER_NOTA($mysqli,$data,$hostname,$user,$password,$db_name)

{ 	$id = trim($_GET["id"]);

 $sql=sprintf("SELECT * FROM hesperia_noticias WHERE noticia_id = '%s'",

              mysqli_real_escape_string($mysqli,$id));

 $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

 if ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))

 { $imagen = $rows["imagen"];

  $titulo = $rows["titulo"];

  $intro = $rows["intro"];

  $contenido = $rows["contenido"];

  $fecha = date("d-m-Y",$rows["fecha"]);

  $id = $rows["noticia_id"];

  $lecturas = $rows["lecturas"]+1;

  $sql=sprintf("UPDATE hesperia_noticias SET lecturas = '%s'

                                    WHERE noticia_id = '%s'",

               mysqli_real_escape_string($mysqli,$lecturas),

               mysqli_real_escape_string($mysqli,$id));

  $query=QUERYBD($sql,$hostname,$user,$password,$db_name);

  echo '

            <div class="col-md-12">

            <div class="page-header">

              <h3 class="g-prensa text-uppercase">'.$titulo.'</h3>

            </div>';

  if (!file_exists("img/prensa/$imagen")){

      echo'<img src="https://hoteleshesperia.com.ve/img/prensa/sin-imagen.png" alt="'.utf8_encode($titulo).'" title="'.utf8_encode($titulo).'" class="img-responsive img-hover center-block"/>';

  }else{

      echo'<img src="https://hoteleshesperia.com.ve/imagen.php?imagen='.$imagen.'-14" alt="'.$titulo.'" title="'.$titulo.'" class="img-responsive img-hover center-block"/>';

  }

  echo '<hr/>

                        '.$intro.'

                        <div class="text-justify">

                        '.$contenido.'</div><br/><hr>

                        <small class="pull-right">';

  SHARE();

  echo '

                            <i class="fa fa-calendar"></i>

                            Publicado: '.$fecha.'

                            <i class="fa fa-eye"></i>

                            Lecturas: '.$lecturas.'

                        </small>

                        

            </div>';

 }

 else {  NADA($hostname,$user,$password,$db_name); }

 return;

}





function toAscii($str, $replace=array(), $delimiter='-') {

  setlocale(LC_ALL, 'en_US.UTF8');

 if( !empty($replace) ) {

  $str = str_replace((array)$replace, ' ', $str);

 }



 $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);

 $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);

 $clean = strtolower(trim($clean, '-'));

 $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);



 return $clean;

}



function ACCESO_VIP($data,$hostname,$user,$password,$db_name){

  echo'<header>

    <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">';

    

        echo '<ol class="carousel-indicators hidden-xs hidden-sm">';

        

                echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>'; 

        echo '</ol>';

    

    echo '<div class="carousel-inner">';

   

            echo '<div class="item active">'; 

        

         echo '<img src="https://hoteleshesperia.com.ve/img/contenido/Banner_partai.jpg"  alt="Festival Partaï Margarita 2017" class="img-responsive center-block" />

            <div class="carousel-caption hidden-lg hidden-md">

                <h3 class="main-text">Usted es un cliente ESPECIAL.</h3>

                
            </div>

            <div class="carousel-caption visible-lg visible-md move-left" style="margin-bottom: 10%">

                <h3 class="main-text">Usted es un cliente ESPECIAL.</h3>';



         
         echo'

             </div>

             </div>';

    echo '</div>

             <div id="my-reserva" class="panel reset-reserva panel-reserva over-slider ancho-reserva pull-right visible-lg visible-md" >

                    

                        <div class="panel-heading panel-heading-reservados text-center">

                             <h3 class="panel-title">Acceso Único</h3>

                        </div>

                        <div class="panel-body">

                             <form enctype="application/x-www-form-urlencoded" action="?go='.$_GET["sucursal"].'/accesoVip/0/" role="form" method="post" name="FormAccesoVip" id="FormAccesoVip" class="acceso" >

                             <label>Clave única</label>

                             <input type="password" class="form-control input-sm" value="" id="clave" name="clave" placeholder="Clave" language="es">                

                             <br>

                             <button type="submit" id="botonConsulta" class="btn btn-primary btn-sm btn-lg btn-block">Acceder</button>

                             </form>

                        </div>';

    echo'

             </div>

             </div>

             </header>

             <div class="container"><br/>

              <div class="row hidden-lg hidden-md">

                <div class="col-lg-12">

                  <div class="panel panel-default panel-reservados">

                        <div class="panel-heading panel-heading-reservados text-center">

                             <h3 class="panel-title">Acceso Único</h3>

                        </div>

                        <div class="panel-body">

                             <form enctype="application/x-www-form-urlencoded" action="index.php?go='.$_GET["sucursal"].'/accesoVip/0/" role="form" method="post" name="FormAccesoVip" id="FormAccesoVip" class="acceso" >

                             <label>Clave única</label>

                             <input type="password" class="form-control input-sm" value="" id="clave" name="clave" placeholder="Clave" language="es">                

                             <br>

                             <button type="submit" id="botonConsulta" class="btn btn-primary btn-sm btn-lg btn-block">Acceder</button>

                             </form>

                        </div>
                  </div>
                </div>
              </div>
            </div>
             ';


  

}



function RECUPERAR_CLAVE($data){

    echo '

<div class="modal fade" id="modal-container-200917" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content mymod">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h2 style="margin-left:10px;margin-right:5px">Recuperar Clave de Acceso</h2>

            </div>

            <div class="modal-body">                

                <div style="width:98%;margin-left:5px;margin-right:5px">                    

                    <div id="recuperaClave"></div>

                    <form enctype="application/x-www-form-urlencoded" action="javascript:void(0)" role="form" method="post" onsubmit="return Recuperar(); return document.MM_returnValue" name="RecuperaClaveUsuario" id="RecuperaClaveUsuario">

                        <div class="form-group">

                            <input type="email" value= "" name="username" id="username" class="form-control" placeholder="Email"/>

                            <input type="hidden" name="contacto" value="'.$data["correo_contacto"].'"/>

                            <input type="hidden" name="dominio" value="'.$data["dominio"].'"/>

                        </div>

                        <button type="submit" class="btn btn-primary btn-sm btn-block" id="check" value="Check">Recuperar</button>

                    </form>

                    <br/>

                </div>

            </div>

        </div>

    </div>

</div>';

 return;

}

function titulos_descripciones($texto, $data, $direccion, $mysqli){

    $descripcion ="Reserva ahora tu hotel en Venezuela al mejor precio. Valencia, Isla Margarita y Maracaibo (próxima apertura) Vacaciones y paquetes en hoteles de Isla Margarita. Hoteles de negocios y eventos en Valencia y Maracaibo";
    
    $titulo ="Hesperia Venezuela";
    $imagen="https://hoteleshesperia.com.ve/img/logo/logohesperiahotels.png";

    $twitter = $data["url_cuenta_twitter"];

    switch (strtolower(trim($texto))) {

            case '':
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial
             <script type=\"application/ld+json\">
                {
                    \"@context\": \"http://schema.org\",
                    \"@type\": \"Organization\",
                    \"url\": \"https://hoteleshesperia.com.ve\",
                    \"logo\": \"https://hoteleshesperia.com.ve/img/logo/logohesperiahotels.png\",
                    \"name\": \"Hoteles Hesperia\",
                    \"sameAs\" : [
                \"http://www.facebook.com/HesperiaVe\",
                \"http://www.twitter.com/hesperiaVe\",
                \"http://instagram.com/hesperiaVe\",
                \"https://www.youtube.com/channel/UCW6Y9Fk_Pw2gSFIY456WmJw\"
              ]
                }             
            </script>
             ";
            break;

            case 'inicio':
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

             <script type=\"application/ld+json\">
                {
                    \"@context\": \"http://schema.org\",
                    \"@type\": \"Organization\",
                    \"url\": \"https://hoteleshesperia.com.ve\",
                    \"logo\": \"https://hoteleshesperia.com.ve/img/logo/logohesperiahotels.png\",
                  \"sameAs\" : [
                \"http://www.facebook.com/HesperiaVe\",
                \"http://www.twitter.com/hesperiaVe\",
                \"http://instagram.com/hesperiaVe\",
                \"https://www.youtube.com/channel/UCW6Y9Fk_Pw2gSFIY456WmJw\"
              ]
                }             
            </script>

             ";
            break;

            case 'nosotros':

              $descripcion = "Hesperia Hotels & Resorts Venezuela pertenece a la prestigiosa cadena NH Hotel Group, que es una de las 25 hoteleras más grandes del mundo y una de las principales de Europa.";

              $titulo="Hesperia Venezuela - Nosotros";
              $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
              
             echo "  
            <title>
               $titulo
            </title>
            $metaStandardSocial
            <meta name=\"description\" content=\"".$descripcion."\"/>
             
             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/nosotros\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
                break;
            
            
            case 'reuniones-eventos':
            $titulo ="Hesperia Venezuela - Reuniones y Eventos";

            $descripcion="Organiza tu gran evento con Hesperia.  Te garantizamos la máxima flexibilidad, adaptabilidad y servicio especializado, en las mejores ubicaciones.";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
            echo "  
            <title>
               $titulo
            </title>
            
            $metaStandardSocial

            <meta name=\"description\" content=\"$descripcion\"/>
            <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/reuniones-eventos\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
                break;
            
            case 'experiencias':
            $titulo="Hesperia Venezuela - Experiencias";
            $descripcion="Reservar online los mejores hoteles en Isla Margarita y en Valencia nunca fue tan fácil. En Hesperia convertimos tu estancia en una experiencia única.";
             $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  
            <title>
               $titulo
            </title>
            
            $metaStandardSocial

            <meta name=\"description\" content=\"$descripcion\"/>
             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/experiencias\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
           
                break;
            
            case 'paquetes':
            $titulo ="Hesperia Venezuela - Paquetes";
            $descripcion="Encuentra y reserva online los mejores paquetes en Valencia, Margarita y proximamente Maracaibo. Todos diseñados para garantizarte la mejor experiencia posible";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  
            <title>
               $titulo
            </title>
            
            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/paquetes\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";

                break;
            ///////////////////////////////////TEXTOS///////////////////////////////////////77
            case '1-historia':

            $titulo="Hesperia Venezuela - Historia";
            $descripcion="Hesperia Hotels & Resorts es una marca gestionada por la cadena NH Hotel Group, una de las 25 cadenas hoteleras más grandes del mundo. La Compañía opera 375 hoteles y alrededor de 60.000 habitaciones en 28 países de Europa, América y África.";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial
    
             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/1-historia\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
             
            break;

            case '3-gastronomia':
            $titulo="Hesperia Venezuela - Gastronomía";
            $descripcion="Disfruta del mejor desayuno incluido de la ciudad sin salir del hotel y en Margarita visita nuestros exclusivos restaurantes temáticos.";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/3-gastronomia\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
             
            break;

            case '4-responsabilidad-social':
            $titulo="Hesperia Venezuela - Responsabilidad Social";
            $descripcion="Hoteles Hesperia Venezuela como parte del NH Hotel Group quiere seguir creciendo de forma responsable y comprometida con todos sus grupos de interés contribuyendo al desarrollo sostenible del Grupo de la Sociedad.";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  

            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/4-responsabilidad-social\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
             
            break;
            
            /////////////////////////////////// FIN TEXTOS///////////////////////////////////////77
            case 'boletines':
            $titulo="Hesperia Venezuela - Boletines/Newsletter";
            $descripcion="Descarga nuestros boletines informativos y entérate de lo nuevo de Hesperia";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/boletines\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
            break;

            case 'trabajo':
            $titulo="Hesperia Venezuela - Trabaja con Nosotros";
            $descripcion="En Hesperia Hotels & Resorts estamos en la búsqueda de nuevos talentos que deseen formar parte de nuestro excelente equipo de trabajo en constante crecimiento.";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
             echo "  
            <title>
               $titulo
            </title>
            
            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/trabajo\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
            break;
            
            case 'contacto':
            $titulo="Hesperia Venezuela - Contacto";
            $descripcion="Nuestros Representantes estan disponibles para brindarle información sobre nosotros y nuestras sucursales, así como para responder a sus preguntas, realizar reservaciones y atender tus solicitudes";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>
            
            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/contacto\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>
             ";
            break;

            case 'mi-cuenta':
            $titulo="Hesperia Venezuela - Mi Cuenta";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
            echo "  
            <title>
               
            </title>
            
            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial


             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/mi-cuenta\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;
            
            case 'prensa':
            $titulo="Hesperia Venezuela- Prensa/Media-Center";
            $descripcion="Encuentra  imagenes, videos, archivos y noticias en nuestro Media center.";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\" />

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/prensa\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'partai':
            $titulo="Hesperia Venezuela- Partai";
            $descripcion="Del  10 al 13 de Marzo vuelve el festival Partai 2017 a las instalaciones de los Hoteles Hesperia de Isla Margarita. Desde ahora ya puedes adquirir aquí tu paquete de alojamiento y entradas para vivir el evento del año a tope";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/partai\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'partai2017':
            $titulo="Hesperia Venezuela- Paquetes Partai 2017";
            $descripcion="Del  10 al 13 de Marzo vuelve el festival Partai 2017 a las instalaciones de los Hoteles Hesperia de Isla Margarita. Desde ahora ya puedes adquirir aquí tu paquete de alojamiento y entradas para vivir el evento del año a tope";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/partai\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'mediacenter':
            $titulo="Hesperia Venezuela- Media Center";
            $descripcion="Encuentra todos nuestros contenidos y mantente informado de nuestras noticias y novedades";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/mediacenter\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;


            case 'hesperia-isla-margarita':
            $titulo="Hesperia Venezuela - Hesperia Isla Margarita";
            $descripcion="En el cálido pueblo de Pedro González se encuentra el Hesperia Isla Margarita. Recorrer los hermosos paisajes neoespartanos camino al hotel y divisar la imponente estructura resulta una agradable sorpresa a la vista del visitante";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>

            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/hesperia-isla-margarita\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'hesperia-eden-club':
            $titulo="Hesperia Eden Club";
            $descripcion="Hesperia Edén Club es un novedoso concepto sólo para adultos o para familias con hijos mayores de edad.  Encanto, romance, exclusividad, y relax son los principales atributos de este maravilloso lugar";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>
            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/hesperia-eden-club\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'aperturas':
            $titulo="Hesperia Venezuela- Aperturas";
            $descripcion="El Hotel Hesperia WTC Maracaibo de categoría 5 es un nuevo proyecto que está ejecutando la cadena Hesperia en Venezuela. Cuenta con 240 habitaciones incluyendo 108 suites y piso ejecutivo. 
            ";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>
            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/aperturas\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'hesperia-playa-el-agua':
            $titulo="Hesperia Venezuela- Hesperia Playa El Agua";
            $descripcion="Excelente destino familiar Ubicado en el corazón de playa el agua, que con sus 4000 metros de longitud es la más famosa de la isla de Margarita. 
            ";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>
            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/Hesperia-Playa-el-Agua\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;

            case 'hesperia-wtc-valencia':
            $titulo="Hesperia WTC Valencia";
            $descripcion="Hesperia WTC Valencia - Hotel and Convention Center, de categoría superior, pertenece a la reconocida cadena Hotelera Hesperia Hotels & Recorreresorts, con presencia en 27 países; es una de las más grandes en España y la quinta de Europa en la categorí­a de negocios.
            ";
            $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
            
              echo "  
            <title>
               $titulo
            </title>

            <meta name=\"description\" content=\"$descripcion\"/>
            $metaStandardSocial

             <script type=\"application/ld+json\">
              {
                    \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https://hoteleshesperia.com.ve/Hesperia-WTC-Valencia\",
                    \"name\" : \"$titulo\",
                    \"description\" : \"$descripcion\"
                    
                }
            </script>

             ";
            break;
            /*
            //////////////////////////////////////////PENDIENTES///////////////////////////////////////////////
            case 'compra':
              //  <meta name="description" content="'.$data['description'].' '.$data["serie"].'"/>
                break;
            


            case 'reserva':
              //  <meta name="description" content="'.$data['description'].' '.$data["serie"].'"/>
                break;
            //////////////////////////////////////////FIN PENDIENTES///////////////////////////////////////////////
            */
            default:

            $array_dir = explode("/", $direccion);
           // echo "----------------------";
           // var_dump($array_dir);
            switch (strtolower($array_dir[1])) {

              case 'paquetes':
                cargar_info_paquete($array_dir[2], $mysqli, $texto);
                break;
              
              case 'noticia':
               //  $array_dir = explode("/", $direccion);
                // echo "<br><br><br><br><br>----------------------";
                // var_dump($array_dir);
                 cargar_info_noticia($array_dir[2], $mysqli, $texto);
              break;

              default:
               $metaStandardSocial=metasocial($titulo,$descripcion,$twitter,$imagen);
                echo "  
                <title>
                   $titulo
                </title>

                <meta name=\"description\" content=\"$descripcion\"/>

                $metaStandardSocial

                 <script type=\"application/ld+json\">
                    {
                        \"@context\": \"http://schema.org\",
                        \"@type\": \"Organization\",
                        \"url\": \"https://hoteleshesperia.com.ve/\",
                        \"logo\": \"https://hoteleshesperia.com.ve/img/logo/logohesperiahotels.png\",
                        \"name\": \"Hoteles Hesperia\", 
                         \"sameAs\" : [
                    \"http://www.facebook.com/HesperiaVe\",
                    \"http://www.twitter.com/hesperiaVe\",
                    \"http://instagram.com/hesperiaVe\",
                    \"https://www.youtube.com/channel/UCW6Y9Fk_Pw2gSFIY456WmJw\"
                  ]
                    }             
                </script>
                 ";
                break;
              }

            
          break; //BREAK DEL PRIMER DEFAULT
        }    
}

function metasocial($titulo,$descripcion,$twitter,$imagen){

    return "
            <!-- TITULOS Y DESCRIPCIONES!! -->
            <meta property=\"og:type\" content=\"website\"/>
            <meta property=\"og:locale\" content=\"es_VE\"/>
            <meta property=\"og:site_name\" content=\"$titulo\"/>
            <meta name=\"descripcion\" property=\"og:description\" content=\"$descripcion\">
            <meta property=\"og:image:width\" content=\"350\" />
            <meta property=\"og:image:height\" content=\"350\" />

            <meta name=\"twitter:card\" content=\"summary\"/>
            <meta name=\"twitter:site\" content=\"@$twitter\" />
            <meta name=\"twitter:creator\" content=\"@$twitter\" />
            <meta name=\"twitter:title\" content=\"$titulo\" />
            <meta name=\"twitter:description\" content=\"$descripcion\" />
            
            <meta property=\"og:image\" content=\"$imagen\"/>
            <meta name=\"twitter:imagen\" content=\"$imagen\"/>

            <meta name=\"dc.title\" content=\"$titulo\"/>
    
            <meta name=\"application-name\" content=\"$titulo\"/>
                
            <meta name=\"msapplication-tooltip\" content=\"$titulo\"/>
    
        ";

}

function partaiAfter(){
  echo "
  <div class='row'>
    <div class='page-header'>
      <h1 class='text-uppercase' style='margin-top:10px;'>PartaÏ Margarita Weekend</h1>
    </div>
    <div class='col-md-12'>
      <h3 class='text-center'>¿Estas listo para la experiencia PartaÏ 2017?</h3>
      <h3 class='lead text-center'>
         <a href='https://hoteleshesperia.com.ve/partai2017'>Visita aquí nuestros paquetes disponibles</a>
      </h3>
      <p class='text-center'><a class='btn btn-primary' href='https://hoteleshesperia.com.ve/partai2017'>Paquetes PartaÏ 2017</a></p>
      <br>
      <div class='well text-center'>
        <p>Hesperia Playa el Agua y Hesperia Edén Club <span class='label label-danger'>SOLD OUT</span></p>
        <p>Hesperia Isla Margarita disponible <span class='label label-primary'>CON TRANSPORTE ENTRE HOTELES INCLUIDO</span> </p>
        <p><strong>Si lo prefiere también puede reservar llamando al <span>0295-400.71.00 <i class='fa fa-phone-square' aria-hidden='true'></i></span></strong></p>
      </div>
    </div>

    <div class='col-md-6 col-md-offset-3'>
     <iframe width='560' height='315' src='https://www.youtube.com/embed/7JUVw1TdlQQ' frameborder='0' allowfullscreen></iframe>
    </div>
    
  </div>


  ";
}

function cargar_info_paquete($codigo, $mysqli, $slug){

    $query = "SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0
      and vip = 0 AND id = '".$codigo."' ORDER BY orden ASC";
    
     $result=$mysqli->query($query);

     while ($reg=mysqli_fetch_assoc($result)) {
      $resena = strip_tags(substr($reg["descripcion_paquete"], 0, strpos($reg["descripcion_paquete"], "</p>")));
     // $resena = strip_tags($reg["descripcion_paquete"], "<br>");
      //var_dump(strip_tags($resena));

       echo(" 
        <title>
          $reg[nombre_paquete]
        </title>

        <meta name=\"description\" content=\"$resena\"/>


        <script type=\"application/ld+json\">
           \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https:hoteleshesperia.com.ve/$reg[id]/$slug\",
                    \"name\" : \"$reg[nombre_paquete]\",
                    \"description\" : \"$resena\"    
        </script>

        <meta property=\"og:type\" content=\"website\"/>
            <meta property=\"og:locale\" content=\"es_VE\"/>
            <meta property=\"og:site_name\" content=\"$reg[nombre_paquete]\"/>
            <meta name=\"descripcion\" property=\"og:description\" content=\"$resena\">
            <meta property=\"og:image:width\" content=\"350\" />
            <meta property=\"og:image:height\" content=\"350\" />

            <meta name=\"twitter:card\" content=\"summary\"/>
            <meta name=\"twitter:site\" content=\"@hesperiave\" />
            <meta name=\"twitter:creator\" content=\"@hesperiave\" />
            <meta name=\"twitter:title\" content=\"$reg[nombre_paquete]\" />
            <meta name=\"twitter:description\" content=\"$resena\" />
            
            <meta property=\"og:image\" content=\"https://hoteleshesperia.com.ve/imagen.php?imagen=$reg[img_paquete]-3\"/>
            <meta name=\"twitter:imagen\" content=\"https://hoteleshesperia.com.ve/imagen.php?imagen=$reg[img_paquete]-3\"/>

            <meta name=\"dc.title\" content=\"$reg[nombre_paquete]\"/>
    
            <meta name=\"application-name\" content=\"$reg[nombre_paquete]\"/>
                
            <meta name=\"msapplication-tooltip\" content=\"$reg[nombre_paquete]\"/>
      ");
     }

     if ($mysqli->error) {
       echo($mysqli->error);
     }

     $mysqli->close();
}

function cargar_info_noticia($codigo, $mysqli, $slug){

    $query = "SELECT * FROM hesperia_v2_noticias where ind_activo = 'S' AND id_noticia = '".$codigo."'";
    
     $result=$mysqli->query($query);

     while ($reg=mysqli_fetch_assoc($result)) {
      $resena = strip_tags(substr($reg["contenido"], 0, strpos($reg["contenido"], "</p>")));
      //$resena = strip_tags($reg["descripcion_paquete"], "<br>");
      //var_dump(strip_tags($resena));

       echo(" 
        <title>
          $reg[titulo]
        </title>

        <meta name=\"description\" content=\"$resena\"/>


        <script type=\"application/ld+json\">
           \"@context\": \"http://schema.org\",
                    \"@type\" : \"WebSite\",
                    \"url\": \"https:hoteleshesperia.com.ve/$reg[id_noticia]/$slug\",
                    \"name\" : \"$reg[titulo]\",
                    \"description\" : \"$resena\"    
        </script>

        <meta property=\"og:type\" content=\"website\"/>
            <meta property=\"og:locale\" content=\"es_VE\"/>
            <meta property=\"og:site_name\" content=\"$reg[titulo]\"/>
            <meta name=\"descripcion\" property=\"og:description\" content=\"$resena\">
            <meta property=\"og:image:width\" content=\"350\" />
            <meta property=\"og:image:height\" content=\"350\" />

            <meta name=\"twitter:card\" content=\"summary\"/>
            <meta name=\"twitter:site\" content=\"@hesperiave\" />
            <meta name=\"twitter:creator\" content=\"@hesperiave\" />
            <meta name=\"twitter:title\" content=\"$reg[titulo]\" />
            <meta name=\"twitter:description\" content=\"$resena\" />
            
            <meta property=\"og:image\" content=\"https://hoteleshesperia.com.ve/img/media_center/$reg[imagen]\"/>
            <meta name=\"twitter:imagen\" content=\"https://hoteleshesperia.com.ve/img/media_center/$reg[imagen]\"/>

            <meta name=\"dc.title\" content=\"$reg[titulo]\"/>
    
            <meta name=\"application-name\" content=\"$reg[titulo]\"/>
                
            <meta name=\"msapplication-tooltip\" content=\"$reg[titulo]\"/>
      ");
     }

     if ($mysqli->error) {
       echo($mysqli->error);
     }

     $mysqli->close();

   }

?>