<?php
session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header("location:../error.html");
	die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
$email = filter_var($_POST["username"],FILTER_SANITIZE_EMAIL);
$sql = sprintf("SELECT email  FROM hesperia_usuario WHERE email = '%s'",
				mysqli_real_escape_string($mysqli,$email));
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))
{ $ahora     = time();
  $localidad = $_SERVER['REMOTE_ADDR'];
  $dominio   = $_POST['dominio'];
  $contacto  = $_POST['contacto'];
  $nueva_clave = substr(time(),-4).'-'.rand ( 0 , 9 );
  $clave     = md5($nueva_clave);
  $sql = sprintf("UPDATE hesperia_usuario SET
				clave_acceso = '%s'
				WHERE email  = '%s'",
			mysqli_real_escape_string($mysqli,$clave),
			mysqli_real_escape_string($mysqli,$email));
  $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);
  graba_LOG("Recuperacion de clave de: $email",$email,$localidad,$ahora,$hostname,$user,$password,$db_name);
  echo '<div class="alert alert-success" role="alert">>Se ha enviado su nueva clave a su email de registro.
		</div>';
  $asunto  = "Recuperar clave en $dominio";
  $mensaje ="Saludos: <br>
             Usted o alguien a solicitado cambio de clave en su cuenta de $dominio<br>
			Cuenta email: $email<br>
             Su nueva clave es: $nueva_clave<br><br>
             Ingrese a http://$dominio y haga click en Mi Cuenta<br><br>
             Localidad de la solictud: $localidad<br>
             Hora: ".date("d-m-Y",$ahora)."<br><br>
             $dominio<br>";
			 $headers = '';
			 $headers .= "MIME-Version:1.0\r\n"; 
			 $headers .= "Content-type:text/html; charset=UTF-8\r\n";
			 $headers .= "Received:from www.$dominio\r\n";
			 $headers .= "X-Priority:3\r\n";
			 $headers .= "X-MSMail-Priority:Normal\r\n";
 			 $headers .= "From: Hotel Hesperia <$contacto>\r\n";
			 $headers .= "X-Mailer:$contacto\r\n"; 
			 $headers .= "Return-path:$contacto\r\n";
			 $headers .= "Reply-To:$contacto\r\n";
			 $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
			 @mail($email,$asunto,$mensaje,$headers);
} else
{ echo '<div class="alert alert-danger" role="alert">La cuenta email que introdujo no est&aacute; registrada en nuestro sistema. Intente de nuevo o contacte al Administrador</p>
	</div>'; }
unset($result,$sql,$email,$headers);
$_POST = array(); 
?>
