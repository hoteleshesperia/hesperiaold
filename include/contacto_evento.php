<?php

//echo'<pre>';
//print_r($_POST);
//echo'</pre>';
//die();

session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
    header("location:../error.html");
    die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
$ahora = time();

$id_hotel           = $_POST['id_hotel'];
$asistentes         = $_POST['asistentes'];
$salones            = $_POST['salones'];
$habitaciones       = $_POST['habitaciones'];
$tipo_evento        = $_POST['tipo_evento'];
$names              = strip_tags(ucwords(trim($_POST['names'])));
$correo_contacto    = strip_tags(strtolower(trim($_POST['correo_contacto'])));
$correo_contacto    = filter_var($correo_contacto,FILTER_SANITIZE_EMAIL);
$telefono_contacto  = trim($_POST['telefono_contacto']);


// traer nombre del hotel


$sql = sprintf("SELECT nombre_sitio,razon_social,correo_contacto,
                dominio,direccion,telf_reserva,telefonos
                FROM hesperia_settings WHERE id = '%s'",
               mysqli_real_escape_string($mysqli,$id_hotel));
$resultSet = QUERYBD($sql,$hostname,$user,$password,$db_name);
$rows = mysqli_fetch_array($resultSet,MYSQLI_ASSOC);
$razonsocial = $rows["razon_social"];
$contacto = 'eventos@hoteleshesperia.com.ve';
$dominio = $rows["dominio"];
$nombre_sitio = $rows["nombre_sitio"];
$direccion = $rows["direccion"];
$treserva = $rows["telf_reserva"];
$thotel = $rows["telefonos"];


$solicitud = 'Solicitud de planificación de evento para el hotel: '.$nombre_sitio.' La cantiad de: '.$asistentes.' asistentes. Con '.$salones.' salones para relizar el siguiente evento: '.$tipo_evento;

graba_LOG("Recepcion de mensaje por contacto: $correo_contacto",$correo_contacto,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
$sql = sprintf("INSERT INTO hesperia_contacto VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '', '%s')",
               mysqli_real_escape_string($mysqli,$names),
               mysqli_real_escape_string($mysqli,$correo_contacto),
               mysqli_real_escape_string($mysqli,$telefono_contacto),
               mysqli_real_escape_string($mysqli,$solicitud),
               mysqli_real_escape_string($mysqli,$ahora),
               mysqli_real_escape_string($mysqli,$ahora),
               mysqli_real_escape_string($mysqli,0));
$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
if (mysqli_affected_rows($mysqli) >= 1)
{ echo '
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            <p>
                Hemos recibido su solicitud mensaje, en las próximas horas le estaremos contactando. Gracias por enviarnos sus consultas y/o sugerencias.
            </p>
        </div>
    </div>
</div>';
 $asunto  = "Contacto desde formulario de $dominio";
 $mensaje = '<!DOCTYPE html><html style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><head><meta name="viewport" content="width=device-width"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>'.$dominio.'</title></head><body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;"><style type="text/css">@media only screen and (max-width:600px){div[class=column]{width: auto !important;}table.social div[class=column] {width: auto !important;}a[class=btn]{display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;}div[class=column]{float: none !important;}}</style><table class="head-wrap" bgcolor="#009ddc" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td class="header container" style="clear: both !important; font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;"><div class="content" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;"><table bgcolor="#009ddc" class="" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><img src="https://'.$dominio.'/hh/img/iconos/email.png" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 100%; margin: 0; padding: 0;"></td><td align="right" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><h6 class="collapse" style="font-family: HelveticaNeue-Light,\'Helvetica Neue Light\',\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-weight: 900; line-height: 1.1; color: #1F1F21; font-size: 14px; text-transform: uppercase; margin: 0; padding: 0;">'.$dominio.'</h6></td></tr></table></div></td></tr></table><table class="body-wrap" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td class="container" bgcolor="#FFFFFF" style="clear: both !important; font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;"><div class="content" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;"><table style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><h4 style="font-family: HelveticaNeue-Light,\'Helvetica Neue Light\',\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-weight: 500; line-height: 1.1; color: #1F1F21; font-size: 23px; margin: 0 0 15px; padding: 0;">Sr(a), nombre aqui</h4></td></tr></table><table class="footer-wrap" style="clear: both !important; font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td class="container" style="clear: both !important; font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;"><div class="content" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;"><table style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><p class="" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.6; margin: 0 0 10px; padding: 0;">'.$solicitud.'</p></td></tr></table></div></td></tr></table></div><div class="column-wrap" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 600px !important; margin: 0 auto; padding: 0;"><div class="column" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 300px; float: left; margin: 0; padding: 0;"></div><div class="column" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 300px; float: left; margin: 0; padding: 0;"></td></tr></table></td></tr></table></div><div class="content" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;"><table style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><p class="callout" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.6; background: #ECF8FF; margin: 0 0 15px; padding: 15px;"><strong style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;">Información de Contacto:</strong> Teléf. de Reservas: '.$treserva.' - Teléf. Hotel: '.$thotel.'</p></td></tr></table></div></div></td></tr></table><table class="footer-wrap" style="clear: both !important; font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td class="container" style="clear: both !important; font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;"><div class="content" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;"><table style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; margin: 0; padding: 0;"><p class="" style="font-family: \'Helvetica Neue\',Helvetica,Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.6; margin: 0 0 10px; padding: 0;">Este mensaje es privado y confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.</p></td></tr></table></div></td></tr></table></body></html>';
$headers = '';
$headers .= "MIME-Version:1.0\r\n";
$headers .= "Content-type:text/html; charset=UTF-8\r\n";
$headers .= "Received:from $dominio\r\n";
$headers .= "X-Priority:3\r\n";
$headers .= "X-MSMail-Priority:Normal\r\n";
$headers .= "From: $razonsocial <$contacto>\r\n";
$headers .= "X-Mailer:$contacto\r\n";
$headers .= "Return-path:$contacto\r\n";
$headers .= "Reply-To:$contacto\r\n";
$headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
@mail($email,"RE:$asunto",$mensaje,$headers);
$headers = '';
$headers .= "MIME-Version:1.0\r\n";
$headers .= "Content-type:text/html; charset=UTF-8\r\n";
$headers .= "Received:from $dominio\r\n";
$headers .= "X-Priority:3\r\n";
$headers .= "X-MSMail-Priority:Normal\r\n";
$headers .= "From: $names <$correo_contacto>\r\n";
$headers .= "X-Mailer:$correo_contacto\r\n";
$headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
$headers .= "Reply-To:$correo_contacto\r\n";
$headers .= "Return-path:$correo_contacto\r\n";
$mensaje = "Hemos recibido y notificado recepcion del siguiente mensaje:\r\n\r\n $solicitud";
 @mail($contacto,$asunto,$mensaje,$headers);
} else
{ echo '<div class="alert alert-danger" role="alert">
              <p>Ha ocurrido un error inesperado. Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
            </div>'; }
unset($result,$sql,$email,$headers,$mensaje);
$_POST = array();
?>

