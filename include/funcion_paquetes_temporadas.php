<?php
/*
* Funcion para los paquetes por temporadas
*/

function PAQUETES_TEMPORADA($mysqli,$data,$hostname,$user,$password,$db_name){
echo '
          <div class="page-header">
          <h1 style="margin-top:10px;">Nuestros Paquetes</h1>
          </div>';
          
    echo '<div class="row"><div id="form-ui">';
    BOTONES($mysqli,$data,$hostname,$user,$password,$db_name);
    echo '</div></div><br><br><br>';
    //if($_SESSION["locationGeo"] == 'VE'){
          $hoy = date("Y-m-d",time());          

        if($data["id_paquete_promo"] == 0){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes 
                              WHERE alojamiento = 0 && 
                              status <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada <> 100 ORDER BY id_hotel, temporada ASC");
            else
              $sql = sprintf("SELECT * FROM hesperia_paquetes
                              WHERE alojamiento = 0 && 
                              status_dolar <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada <> 100 ORDER BY id_hotel, temporada ASC");

        }else{
          if($_SESSION["locationGeo"] == 'VE')
            $sql = sprintf("SELECT * FROM hesperia_paquetes 
                            WHERE alojamiento = 0 && 
                            status <> 'N' && 
                            partai = 0 && 
                            vip = 0 && 
                            id = '%s' && 
                            temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));
          else
            $sql = sprintf("SELECT * FROM hesperia_paquetes 
                            WHERE alojamiento = 0 && 
                            status_dolar <> 'N' && 
                            partai = 0 && 
                            vip = 0 && 
                            id = '%s' && temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

        }

        if($data["temporada"] > 0){

          if($data["temporada"] == 100){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes 
                              WHERE alojamiento = 0 && 
                              status <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada = '%s' && 
                              hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes 
                                WHERE alojamiento = 0 && 
                                status_dolar <> 'N' && 
                                partai = 0 && 
                                vip = 0 && 
                                temporada = '%s' && 
                                hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
          }else{
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes 
                              WHERE alojamiento = 0 && 
                              status <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes 
                                WHERE alojamiento = 0 && status_dolar <> 'N' && partai = 0 && vip = 0 && temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
          }
          
        }

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

        $hay = mysqli_num_rows($result);

        if ($hay < 1){ 
          echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
            return; 
       }else{
          echo '
          <div class="container-fluid">
          <div id="posts" class="row">';
          $id_hotel_aux = 0;
          while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
            $fecha = date("d/m/Y",time()+86400); 
            $nombre_paquete = $rowPaquetes["nombre_paquete"];
            $capacidad = $rowPaquetes["capacidad"];
            $asunto = str_replace(' ','+',$nombre_paquete);

            $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                           mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

            $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

            $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
            $hotel = $rowSetting["razon_social"];
            /*if($id_hotel_aux != $rowPaquetes["id_hotel"]){
              if($id_hotel_aux != 0){
                echo '</div><div class="row">';
              }
              echo '
                <h3 class="text-primary text-center">'.$hotel.'</h3>';
              $id_hotel_aux = $rowPaquetes["id_hotel"];
            }*/

            $desdePaq = '';
            $hastaPaq = '';
            if($_SESSION["locationGeo"] == 'VE' ){
              $precio = $rowPaquetes["precio"];
            }else{
              $precio = $rowPaquetes["price"];
            }
            
            $habitacion = '';

            if($rowPaquetes["porcentaje"] > 0){
              $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                        mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));
              $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
              $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
              $habitacion = $rowHab["nombre_habitacion"];
              echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

            }

            $pos = strpos($rowPaquetes["nombre_paquete"], '. ');
            if($pos > 0){
              $pos++;
            }

            $nombre = substr($rowPaquetes["nombre_paquete"], $pos);
            
            echo '
            <div class="col-sm-6 col-md-4 isotope item '.str_replace(' ', '', $hotel).' '.str_replace('-', '', $rowPaquetes["desde"]).'" data-category="'.str_replace('-', '', $rowPaquetes["desde"]).'" id="'.str_replace('-', '', $rowPaquetes["desde"]).'">';
            if($rowPaquetes["temporada"] == 100)
              echo'
              <div class="thumbnail equalizeAtrapa myitem">';
            else
              echo'
              <div class="thumbnail equalize">';
              if($rowPaquetes["temporada"] == 100)
              echo '
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="200" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">';
              else
              echo '
                <img src="https://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="300" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">';
                
              echo '
                <div class="caption">
                  <h4 style="height: 50px;" class="text-justify">'.$nombre.'</h4>
                  <p class="text-muted text-center"><strong>'.$hotel.'</strong></p>';
                  if($rowPaquetes["temporada"] == 100)
                  echo'
                  <div id="getting-started'.$rowPaquetes["id"].'" data-date="'.$rowPaquetes["hasta_atrapalo"].'"></div>
                  ';  
                  
                  echo'
                  <p><a href="https://hoteleshesperia.com.ve/paquetes/'.$rowPaquetes["id"].'/'.toAscii($nombre).'" class="btn btn-primary btn-block" role="button">Reservar</a></p>
                </div>
              </div>
            </div>
            ';
          }
          echo '</div>
          </div>
          
          ';
       } 

    echo '<div class="col-sm-6 col-md-4 shuffle_sizer"></div>';
      
    /*}else{
       echo '<br/>
              <div class="text-center">
                <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
                <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
              </div>';
    }*/
    return;
    
  }
  
/*
* Esto es una solucion temporal
* Obviamente esta mala pero es
* para resolver momentaneamente
*/

function BOTONES($mysqli,$data,$hostname,$user,$password,$db_name) {
        
        $hoy = date("Y-m-d",time());
        
        if($data["id_paquete_promo"] == 0){
          
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes 
                              WHERE alojamiento = 0 && 
                              status <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada <> 100 ORDER BY id_hotel, temporada ASC");
            else
              $sql = sprintf("SELECT * FROM hesperia_paquetes
                              WHERE alojamiento = 0 && 
                              status_dolar <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada <> 100 ORDER BY id_hotel, temporada ASC");

        }else{
          if($_SESSION["locationGeo"] == 'VE')
            $sql = sprintf("SELECT * FROM hesperia_paquetes 
                            WHERE alojamiento = 0 && 
                            status <> 'N' && 
                            partai = 0 && 
                            vip = 0 && 
                            id = '%s' && 
                            temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));
          else
            $sql = sprintf("SELECT * FROM hesperia_paquetes 
                            WHERE alojamiento = 0 && 
                            status_dolar <> 'N' && 
                            partai = 0 && 
                            vip = 0 && 
                            id = '%s' && temporada <> 100 ORDER BY orden ASC",
                          mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

        }

        if($data["temporada"] > 0){

          if($data["temporada"] == 100){
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes 
                              WHERE alojamiento = 0 && 
                              status <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada = '%s' && 
                              hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes 
                                WHERE alojamiento = 0 && 
                                status_dolar <> 'N' && 
                                partai = 0 && 
                                vip = 0 && 
                                temporada = '%s' && 
                                hasta_atrapalo > '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]),
                              mysqli_real_escape_string($mysqli,$test));
          }else{
            if($_SESSION["locationGeo"] == 'VE')
              $sql = sprintf("SELECT * FROM hesperia_paquetes 
                              WHERE alojamiento = 0 && 
                              status <> 'N' && 
                              partai = 0 && 
                              vip = 0 && 
                              temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
              else
                $sql = sprintf("SELECT * FROM hesperia_paquetes 
                                WHERE alojamiento = 0 && status_dolar <> 'N' && partai = 0 && vip = 0 && temporada = '%s' ORDER BY orden ASC",
                              mysqli_real_escape_string($mysqli,$data["temporada"]));
          }
          
        }

        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
          
          echo '<div class="col-md-6">
                  <label>Seleccione el Hotel de su preferencia</label>
                  <select class="filters-select form-control selectdata" id="shotel">
                  <option value="all">Mostrar Todos</option>';
          
          $nombresHoteles = array();
          
          while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
            
            
            $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings 
                                   WHERE  id = '%s' LIMIT 1",
                          mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

            $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

            $rowSetting    = mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
            
            $hotel         = $rowSetting["razon_social"];
            
            if (!in_array($hotel, $nombresHoteles)) {
                
                array_push($nombresHoteles, $hotel);
              
            }
  
          }
          
          foreach ($nombresHoteles as $value) {
            echo '<option value="'.str_replace(' ', '', $value).'">'.$value.'</option>';
          }
          
          
          
  echo '          </select></div>
                  <div class="col-md-6">
                  
                  <label>Seleccione la fecha de salida</label>
                  <select class="filters-select-date form-control selectdata" id="date">
                  <option value="all">Mostrar Todos</option>';
                  $resultPaquete = QUERYBD($sql,$hostname,$user,$password,$db_name);
                  
                  
          $fechasPaquetes = array();
                  
          while ($rowPaquete=mysqli_fetch_array($resultPaquete,MYSQLI_ASSOC)){ 
            
            $fecha = $rowPaquete["desde"];
            
            if (!in_array($fecha, $fechasPaquetes)) {
                
                array_push($fechasPaquetes, $fecha);
              
            }
        
          }
          
          foreach ($fechasPaquetes as $value) {
            echo '<option data-sort-by="'.str_replace('-', '', $value).'" value="'.str_replace('-', '', $value).'">Fecha de Salida '.$value.'</option>';
          }
                    
          echo '
          </select></div>';
  return;
  
}