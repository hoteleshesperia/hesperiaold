<?php

session_start();

include 'PHPMailer/PHPMailerAutoload.php';

include 'funciones.php';



$antesdecore = 1;



include 'databases.php';


$sqlR = sprintf("SELECT * FROM hesperia_v2_reservaciones

                                WHERE aux_id = '%s' AND email = '%s' ORDER BY id DESC" ,

                        mysqli_real_escape_string($mysqli,$_POST["aux_id"]),

                        mysqli_real_escape_string($mysqli,$_POST["email"]));

     $resultR = QUERYBD($sqlR,$hostname,$user,$password,$db_name);

     $rowR=mysqli_fetch_array($resultR,MYSQLI_ASSOC);




$id = $_POST["aux_id"];

$nombres = $_POST["nombres"];

$apellidos = $_POST["apellidos"];

$ind_descuento = $_POST["ind_descuento"];

$descuento_cod = $_POST["descuento_cod"];

$cod_promocional = $_POST["cod_promocional"];

$permisos = $_POST["permisos"];

$totalPagar = $_POST["totalPagar"];


$desde = $rowR["desde"];

$hasta = $rowR["hasta"];

$count_h = $rowR["cant_hab"];

$arrayHab = split('_', $rowR["habitaciones"]);

$arraySub = split('_', $rowR["subtotal"]);

$arrayAdultos = split('_', $rowR["adultos"]);

$arrayNinos = split('_', $rowR["ninos"]);

$arrayDescServ = split('_', $rowR["desc_serv"]);

$arraySubtServ = split('_', $rowR["sub_serv"]);

$cantServ = count($arrayDescServ);

$formatPrecio = number_format($rowR["total"], 2, ',', '.');

$contacto = 'info@hoteleshesperia.com.ve';

$id_hotel = $rowR["id_hotel"];

if($id_hotel == 3 || $id_hotel == 4){
  $idImgBanco = 3;
}else{
  $idImgBanco = $id_hotel;
}

if($_SESSION["locationGeo"] == 'VE' ){
  $moneda = 'Bs. ';
}else{
  $moneda = 'USD. ';
  $idImgBanco = 'usd';
} 

$email = $rowR["email"];

$observaciones = $rowR["observaciones"];





$sqlS = sprintf("SELECT * FROM hesperia_settings

                                WHERE id = '%s'" ,

                mysqli_real_escape_string($mysqli,$rowR["id_hotel"]));

     $resultS = QUERYBD($sqlS,$hostname,$user,$password,$db_name);

     $rowS=mysqli_fetch_array($resultS,MYSQLI_ASSOC);



$contProg = $rowS["email_prog"];

$contExtra = $rowS["email_extra"];

$razonsocial = $rowS["razon_social"];


$direccion = $rowS["direccion"];

$telefHotel = $rowS["telefonos"];

$telefReserva = $rowS["telf_reserva"];

$dominio = $rowS["dominio"];

$telefono = $_POST["telefono"];

            /*Envio mensaje a usuario*/
            $mensaje = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

  <head>

    <!-- NAME: 1:2:1 COLUMN -->

    <!--[if gte mso 15]>

        <xml>

            <o:OfficeDocumentSettings>

            <o:AllowPNG/>

            <o:PixelsPerInch>96</o:PixelsPerInch>

            </o:OfficeDocumentSettings>

        </xml>

        <![endif]-->

    <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>*|MC:SUBJECT|*</title>

        

    <style type="text/css">

    p{

      margin:10px 0;

      padding:0;

    }

    table{

      border-collapse:collapse;

    }

    h1,h2,h3,h4,h5,h6{

      display:block;

      margin:0;

      padding:0;

    }

    img,a img{

      border:0;

      height:auto;

      outline:none;

      text-decoration:none;

    }

    body,#bodyTable,#bodyCell{

      height:100%;

      margin:0;

      padding:0;

      width:100%;

    }

    #outlook a{

      padding:0;

    }

    img{

      -ms-interpolation-mode:bicubic;

    }

    table{

      mso-table-lspace:0pt;

      mso-table-rspace:0pt;

    }

    .ReadMsgBody{

      width:100%;

    }

    .ExternalClass{

      width:100%;

    }

    p,a,li,td,blockquote{

      mso-line-height-rule:exactly;

    }

    a[href^=tel],a[href^=sms]{

      color:inherit;

      cursor:default;

      text-decoration:none;

    }

    p,a,li,td,body,table,blockquote{

      -ms-text-size-adjust:100%;

      -webkit-text-size-adjust:100%;

    }

    .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{

      line-height:100%;

    }

    a[x-apple-data-detectors]{

      color:inherit !important;

      text-decoration:none !important;

      font-size:inherit !important;

      font-family:inherit !important;

      font-weight:inherit !important;

      line-height:inherit !important;

    }

    #bodyCell{

      padding:10px;

    }

    .templateContainer{

      max-width:600px !important;

    }

    a.mcnButton{

      display:block;

    }

    .mcnImage{

      vertical-align:bottom;

    }

    .mcnTextContent{

      word-break:break-word;

    }

    .mcnTextContent img{

      height:auto !important;

    }

    .mcnDividerBlock{

      table-layout:fixed !important;

    }

    body,#bodyTable{

      /*@editable*/background-color:#FAFAFA;

    }

    #bodyCell{

      /*@editable*/border-top:0;

    }

  /*

  @tab Page

  @section Email Border

  @tip Set the border for your email.

  */

    .templateContainer{

      /*@editable*/border:0;

    }

  /*

  @tab Page

  @section Heading 1

  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.

  @style heading 1

  */

    h1{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:26px;

      /*@editable*/font-style:normal;

      /*@editable*/font-weight:bold;

      /*@editable*/line-height:125%;

      /*@editable*/letter-spacing:normal;

      /*@editable*/text-align:left;

    }

  /*

  @tab Page

  @section Heading 2

  @tip Set the styling for all second-level headings in your emails.

  @style heading 2

  */

    h2{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:22px;

      /*@editable*/font-style:normal;

      /*@editable*/font-weight:bold;

      /*@editable*/line-height:125%;

      /*@editable*/letter-spacing:normal;

      /*@editable*/text-align:left;

    }

  /*

  @tab Page

  @section Heading 3

  @tip Set the styling for all third-level headings in your emails.

  @style heading 3

  */

    h3{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:20px;

      /*@editable*/font-style:normal;

      /*@editable*/font-weight:bold;

      /*@editable*/line-height:125%;

      /*@editable*/letter-spacing:normal;

      /*@editable*/text-align:left;

    }

  /*

  @tab Page

  @section Heading 4

  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.

  @style heading 4

  */

    h4{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:18px;

      /*@editable*/font-style:normal;

      /*@editable*/font-weight:bold;

      /*@editable*/line-height:125%;

      /*@editable*/letter-spacing:normal;

      /*@editable*/text-align:left;

    }

    #templatePreheader{

      /*@editable*/background-color:#FAFAFA;

      /*@editable*/border-top:0;

      /*@editable*/border-bottom:0;

      /*@editable*/padding-top:9px;

      /*@editable*/padding-bottom:9px;

    }

    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{

      /*@editable*/color:#656565;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:12px;

      /*@editable*/line-height:150%;

      /*@editable*/text-align:left;

    }

    #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{

      /*@editable*/color:#656565;

      /*@editable*/font-weight:normal;

      /*@editable*/text-decoration:underline;

    }

    #templateHeader{

      /*@editable*/background-color:#FFFFFF;

      /*@editable*/border-top:0;

      /*@editable*/border-bottom:0;

      /*@editable*/padding-top:0px;

      /*@editable*/padding-bottom:0;

    }

    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:16px;

      /*@editable*/line-height:150%;

      /*@editable*/text-align:left;

    }

    #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{

      /*@editable*/color:#2BAADF;

      /*@editable*/font-weight:normal;

      /*@editable*/text-decoration:underline;

    }

    #templateUpperBody{

      /*@editable*/background-color:#FFFFFF;

      /*@editable*/border-top:0;

      /*@editable*/border-bottom:0;

      /*@editable*/padding-top:0;

      /*@editable*/padding-bottom:0;

    }

  

    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:16px;

      /*@editable*/line-height:150%;

      /*@editable*/text-align:left;

    }

  

    #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{

      /*@editable*/color:#2BAADF;

      /*@editable*/font-weight:normal;

      /*@editable*/text-decoration:underline;

    }

    #templateColumns{

      /*@editable*/background-color:#FFFFFF;

      /*@editable*/border-top:0;

      /*@editable*/border-bottom:0;

      /*@editable*/padding-top:0;

      /*@editable*/padding-bottom:0;

    }

    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:16px;

      /*@editable*/line-height:150%;

      /*@editable*/text-align:left;

    }

    #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{

      /*@editable*/color:#2BAADF;

      /*@editable*/font-weight:normal;

      /*@editable*/text-decoration:underline;

    }

    #templateLowerBody{

      /*@editable*/background-color:#FFFFFF;

      /*@editable*/border-top:0;

      /*@editable*/border-bottom:2px solid #EAEAEA;

      /*@editable*/padding-top:0;

      /*@editable*/padding-bottom:9px;

    }

    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{

      /*@editable*/color:#202020;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:16px;

      /*@editable*/line-height:150%;

      /*@editable*/text-align:left;

    }

    #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{

      /*@editable*/color:#2BAADF;

      /*@editable*/font-weight:normal;

      /*@editable*/text-decoration:underline;

    }

    #templateFooter{

      /*@editable*/background-color:#FAFAFA;

      /*@editable*/border-top:0;

      /*@editable*/border-bottom:0;

      /*@editable*/padding-top:9px;

      /*@editable*/padding-bottom:9px;

    }

    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{

      /*@editable*/color:#656565;

      /*@editable*/font-family:Helvetica;

      /*@editable*/font-size:12px;

      /*@editable*/line-height:150%;

      /*@editable*/text-align:center;

    }

    #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{

      /*@editable*/color:#656565;

      /*@editable*/font-weight:normal;

      /*@editable*/text-decoration:underline;

    }

  @media only screen and (min-width:768px){

    .templateContainer{

      width:600px !important;

    }



} @media only screen and (max-width: 480px){

    body,table,td,p,a,li,blockquote{

      -webkit-text-size-adjust:none !important;

    }



} @media only screen and (max-width: 480px){

    body{

      width:100% !important;

      min-width:100% !important;

    }



} @media only screen and (max-width: 480px){

    #bodyCell{

      padding-top:10px !important;

    }



} @media only screen and (max-width: 480px){

    .columnWrapper{

      max-width:100% !important;

      width:100% !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImage{

      width:100% !important;

    }



} @media only screen and (max-width: 480px){

    .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{

      max-width:100% !important;

      width:100% !important;

    }



} @media only screen and (max-width: 480px){

    .mcnBoxedTextContentContainer{

      min-width:100% !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImageGroupContent{

      padding:9px !important;

    }



} @media only screen and (max-width: 480px){

    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{

      padding-top:9px !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{

      padding-top:18px !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImageCardBottomImageContent{

      padding-bottom:9px !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImageGroupBlockInner{

      padding-top:0 !important;

      padding-bottom:0 !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImageGroupBlockOuter{

      padding-top:9px !important;

      padding-bottom:9px !important;

    }



} @media only screen and (max-width: 480px){

    .mcnTextContent,.mcnBoxedTextContentColumn{

      padding-right:18px !important;

      padding-left:18px !important;

    }



} @media only screen and (max-width: 480px){

    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{

      padding-right:18px !important;

      padding-bottom:0 !important;

      padding-left:18px !important;

    }



} @media only screen and (max-width: 480px){

    .mcpreview-image-uploader{

      display:none !important;

      width:100% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Heading 1

  @tip Make the first-level headings larger in size for better readability on small screens.

  */

    h1{

      /*@editable*/font-size:22px !important;

      /*@editable*/line-height:125% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Heading 2

  @tip Make the second-level headings larger in size for better readability on small screens.

  */

    h2{

      /*@editable*/font-size:20px !important;

      /*@editable*/line-height:125% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Heading 3

  @tip Make the third-level headings larger in size for better readability on small screens.

  */

    h3{

      /*@editable*/font-size:18px !important;

      /*@editable*/line-height:125% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Heading 4

  @tip Make the fourth-level headings larger in size for better readability on small screens.

  */

    h4{

      /*@editable*/font-size:16px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Boxed Text

  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.

  */

    .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{

      /*@editable*/font-size:14px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

    #templatePreheader{

      /*@editable*/display:block !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Preheader Text

  @tip Make the preheader text larger in size for better readability on small screens.

  */

    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{

      /*@editable*/font-size:14px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Header Text

  @tip Make the header text larger in size for better readability on small screens.

  */

    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{

      /*@editable*/font-size:16px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Upper Body Text

  @tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.

  */

    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{

      /*@editable*/font-size:16px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Column Text

  @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.

  */

    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{

      /*@editable*/font-size:16px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Lower Body Text

  @tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.

  */

    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{

      /*@editable*/font-size:16px !important;

      /*@editable*/line-height:150% !important;

    }



} @media only screen and (max-width: 480px){

  /*

  @tab Mobile Styles

  @section Footer Text

  @tip Make the footer content text larger in size for better readability on small screens.

  */

    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{

      /*@editable*/font-size:14px !important;

      /*@editable*/line-height:150% !important;

    }



}</style></head>

    <body>

        <center>

            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">

                <tr>

                    <td align="center" valign="top" id="bodyCell">

            <!-- BEGIN TEMPLATE // -->

            <!--[if gte mso 9]>

            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">

            <tr>

            <td align="center" valign="top" width="600" style="width:600px;">

            <![endif]-->

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">

              <tr>

                <td valign="top" id="templatePreheader"></td>

              </tr>

              <tr>

                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">

    <tbody class="mcnImageCardBlockOuter">

        <tr>

            <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                

<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">

    <tbody><tr>

        <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">

        

            

            <a href="https://hoteleshesperia.com.ve" title="Ir a nuestra pagina web" class="" target="_blank">

            



            <img alt="" src="https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png" width="100%" style="width: 100%;" class="mcnImage">

            </a>

        

        </td>

    </tr>

    <tr>

        <td class="mcnTextContent" valign="top" style="padding: 9px 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;" width="546">

            <strong><span style="font-family:verdana,geneva,sans-serif">Sr(a) '.$nombres.' '.$apellidos.'<br>

¡Hemos recibido su solicitud de reserva!</span></strong>

        </td>

    </tr>

</tbody></table>

            </td>

        </tr>

    </tbody>

</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
              <div style="text-align: justify;"><strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Condiciones de reservas</span></span></strong><br>
                  <span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify; color: red;">

                      Estimado cliente esta solicitud de reserva aun no esta confirmada hasta que se verifique la efectividad de su pago por transferencia. Una vez verificado su pago, usted recibirá una confirmación de su reserva y localizador.

                  </span><br><br>
              </div>
            </td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="text-align: center;">
            <img style="max-width:100%;" src="https://hoteleshesperia.com.ve/img/banco/'.$idImgBanco.'.png" />
            </td>
        </tr>
    </tbody>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">

    <tbody class="mcnTextBlockOuter">

        <tr>

            <td valign="top" class="mcnTextBlockInner">

                

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">

                    <tbody><tr>

                        

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                        

                            <div style="text-align: justify;">

                <br>

                ';

                $mensaje = $mensaje.'<br>

              </div>



                        </td>

                    </tr>

                </tbody></table>

                

            </td>

        </tr>

    </tbody>

</table>



<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">

    <tbody class="mcnDividerBlockOuter">

        <tr>

            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">

                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">

                    <tbody><tr>

                        <td>

                            <span></span>

                        </td>

                    </tr>

                </tbody></table>

<!--            

                <td class="mcnDividerBlockInner" style="padding: 18px;">

                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />

-->

            </td>

        </tr>

    </tbody>

</table></td>

              </tr>

              <tr>

                <td valign="top" id="templateUpperBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">

    <tbody class="mcnTextBlockOuter">

        <tr>

            <td valign="top" class="mcnTextBlockInner">

                

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">

                    <tbody><tr>

                        

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

<strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Información de su solicitud de reserva</span></span></strong><br>                       

<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Hotel:</strong> '.$razonsocial.'<br>

<strong>Check In:</strong> '.$desde.'<br>

<strong>Check Out:</strong> '.$hasta.'<br>

<strong>Dirección:</strong> '.$direccion.'</span></span>

                        </td>

                    </tr>

                </tbody></table>

                

            </td>

        </tr>

    </tbody>

</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">

    <tbody class="mcnDividerBlockOuter">

        <tr>

            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">

                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">

                    <tbody><tr>

                        <td>

                            <span></span>

                        </td>

                    </tr>

                </tbody></table>

<!--            

                <td class="mcnDividerBlockInner" style="padding: 18px;">

                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />

-->

            </td>

        </tr>

    </tbody>

</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">

    <tbody class="mcnTextBlockOuter">

        <tr>

            <td valign="top" class="mcnTextBlockInner">

                

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">

                    <tbody><tr>

                        

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                        

                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Habitaciones reservadas</span></span></h1>

                            <span style="font-family:verdana,geneva,sans-serif">

  <span style="font-size:13px">';



for ($k=0; $k < $count_h; $k++) { 

              $tipoH = $arrayHab[$k];

              $subt = $arraySub[$k];

              $h = $k+1;

              $mensaje = $mensaje.'<strong>Habitación '.$h.':</strong>&nbsp;'.$tipoH.'<br>';

              $hAdul = $arrayAdultos[$k];

              if($id_hotel != 4){

                $hNin = $arrayNinos[$k];

                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.' , <strong>Niños</strong> '.$hNin.' </br>';

              }else{

                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.'</br>';

              } 

              $mensaje = $mensaje.'<strong>Subtotal: </strong>'.$moneda.$subt.' </br>';

              $mensaje = $mensaje.'<br>';

            }

            if($cantServ > 0){

              $mensaje = $mensaje.'

              <br><strong>Servicios Adicionales</strong><br>';

                      for ($i=0; $i < $cantServ ; $i++) { 

                          

                                  $mensaje = $mensaje.$arrayDescServ[$i].'</br>

                                  <strong>Sub-Total '.$moneda.': '.$arraySubtServ[$i].'</strong></br>';

                      }

            }
            if($ind_descuento > 0 && $descuento_cod > 0){
              /*$mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total antes: '.$moneda.'<strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
              <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
              Con un '.substr($descuento_cod, 2, 2).'% de descuento aplicado en habitaciones<br><br>';*/
              if($permisos > 0){
                $mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total antes: '.$moneda.'<strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
                <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
                Con un descuento aplicado en habitaciones<br><br>';
              }else{
                $mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total antes: '.$moneda.'<strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
                <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
                Con un '.substr($descuento_cod, 2, 2).'% de descuento aplicado en habitaciones<br><br>';
              }
              
            }

$mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total de la reservación: '.$formatPrecio.'</span></strong></span></span>

                        </td>

                    </tr>

                </tbody></table>

                

            </td>

        </tr>

    </tbody>

</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">

    <tbody class="mcnDividerBlockOuter">

        <tr>

            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">

                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">

                    <tbody><tr>

                        <td>

                            <span></span>

                        </td>

                    </tr>

                </tbody></table>

<!--            

                <td class="mcnDividerBlockInner" style="padding: 18px;">

                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />

-->

            </td>

        </tr>

    </tbody>

</table></td>

              </tr>

              <tr>

                <td valign="top" id="templateColumns">

                  <!--[if gte mso 9]>

                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">

                  <tr>

                  <td align="center" valign="top" width="300" style="width:300px;">

                  <![endif]-->

                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">

                    <tr>

                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">

    <tbody class="mcnTextBlockOuter">

        <tr>

            <td valign="top" class="mcnTextBlockInner">

                

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">

                    <tbody><tr>

                        

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                        

                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comentarios del Cliente</span></span></h1>

<span style="font-family:verdana,geneva,sans-serif"><em><span style="font-size:13px">'.$observaciones.'</span></em></span>

                        </td>

                    </tr>

                </tbody></table>

                

            </td>

        </tr>

    </tbody>

</table></td>

                    </tr>

                  </table>

                  <!--[if gte mso 9]>

                  </td>

                  

                  </tr>

                  </table>

                  <![endif]-->

                </td>

              </tr>

              <tr>

                <td valign="top" id="templateLowerBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">

    <tbody class="mcnDividerBlockOuter">

        <tr>

            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">

                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">

                    <tbody><tr>

                        <td>

                            <span></span>

                        </td>

                    </tr>

                </tbody></table>

<!--            

                <td class="mcnDividerBlockInner" style="padding: 18px;">

                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />

-->

            </td>

        </tr>

    </tbody>

</table>



<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">

    <tbody class="mcnTextBlockOuter">

        <tr>

            <td valign="top" class="mcnTextBlockInner">

                

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">

                    <tbody><tr>

                        

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                        

                            <div style="text-align: justify;"><strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Información de contacto: </span></span></strong><br>

<span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Reservas</strong> '.$telefReserva.' - <strong>Central&nbsp;Hotel</strong> '.$telefHotel.'.</span></span></span><br>

<br>

<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">Para ver esta información más detallada puede ingresar a: https://'.$dominio.' e ingresar como usuario con su cuenta email ['.$email.'].&nbsp;</span><span style="text-align:justify"><font face="verdana, geneva, sans-serif"><span style="font-size:12px; line-height:1.6em">Cualquier duda&nbsp;</span><span style="font-size:12px; line-height:19.2px">contáctenos</span><span style="font-size:12px; line-height:1.6em">&nbsp;al siguiente&nbsp;email: '.$contacto.'.</span></font></span><br>

<br>

&nbsp;</div>



                        </td>

                    </tr>

                </tbody></table>

                

            </td>

        </tr>

    </tbody>

</table>



</td>

              </tr>

              <tr>

                <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">

    <tbody class="mcnFollowBlockOuter">

        <tr>

            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">

    <tbody><tr>

        <td align="center" style="padding-left:9px;padding-right:9px;">

            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">

                <tbody><tr>

                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">

                        <table align="center" border="0" cellpadding="0" cellspacing="0">

                            <tbody><tr>

                                <td align="center" valign="top">

                                    <!--[if mso]>

                                    <table align="center" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                    <![endif]-->

                                    

                                        <!--[if mso]>

                                        <td align="center" valign="top">

                                        <![endif]-->

                                        

                                        

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">

                                                <tbody><tr>

                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">

                                                            <tbody><tr>

                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">

                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">

                                                                        <tbody><tr>

                                                                            

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">

                                                                                    <a href="https://www.facebook.com/HesperiaVenezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>

                                                                                </td>

                                                                            

                                                                            

                                                                        </tr>

                                                                    </tbody></table>

                                                                </td>

                                                            </tr>

                                                        </tbody></table>

                                                    </td>

                                                </tr>

                                            </tbody></table>

                                        

                                        <!--[if mso]>

                                        </td>

                                        <![endif]-->

                                    

                                        <!--[if mso]>

                                        <td align="center" valign="top">

                                        <![endif]-->

                                        

                                        

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">

                                                <tbody><tr>

                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">

                                                            <tbody><tr>

                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">

                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">

                                                                        <tbody><tr>

                                                                            

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">

                                                                                    <a href="https://instagram.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a>

                                                                                </td>

                                                                            

                                                                            

                                                                        </tr>

                                                                    </tbody></table>

                                                                </td>

                                                            </tr>

                                                        </tbody></table>

                                                    </td>

                                                </tr>

                                            </tbody></table>

                                        

                                        <!--[if mso]>

                                        </td>

                                        <![endif]-->

                                    

                                        <!--[if mso]>

                                        <td align="center" valign="top">

                                        <![endif]-->

                                        

                                        

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">

                                                <tbody><tr>

                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">

                                                            <tbody><tr>

                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">

                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">

                                                                        <tbody><tr>

                                                                            

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">

                                                                                    <a href="https://twitter.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a>

                                                                                </td>

                                                                            

                                                                            

                                                                        </tr>

                                                                    </tbody></table>

                                                                </td>

                                                            </tr>

                                                        </tbody></table>

                                                    </td>

                                                </tr>

                                            </tbody></table>

                                        

                                        <!--[if mso]>

                                        </td>

                                        <![endif]-->

                                    

                                        <!--[if mso]>

                                        <td align="center" valign="top">

                                        <![endif]-->

                                        

                                        

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">

                                                <tbody><tr>

                                                    <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">

                                                            <tbody><tr>

                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">

                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">

                                                                        <tbody><tr>

                                                                            

                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">

                                                                                    <a href="https://www.linkedin.com/company/hoteles-hesperia-venezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display:block;" height="24" width="24" class=""></a>

                                                                                </td>

                                                                            

                                                                            

                                                                        </tr>

                                                                    </tbody></table>

                                                                </td>

                                                            </tr>

                                                        </tbody></table>

                                                    </td>

                                                </tr>

                                            </tbody></table>

                                        

                                        <!--[if mso]>

                                        </td>

                                        <![endif]-->

                                    

                                    <!--[if mso]>

                                    </tr>

                                    </table>

                                    <![endif]-->

                                </td>

                            </tr>

                        </tbody></table>

                    </td>

                </tr>

            </tbody></table>

        </td>

    </tr>

</tbody></table>



            </td>

        </tr>

    </tbody>

</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">

    <tbody class="mcnDividerBlockOuter">

        <tr>

            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">

                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">

                    <tbody><tr>

                        <td>

                            <span></span>

                        </td>

                    </tr>

                </tbody></table>

<!--            

                <td class="mcnDividerBlockInner" style="padding: 18px;">

                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />

-->

            </td>

        </tr>

    </tbody>

</table></td>

              </tr>

            </table>

            <!--[if gte mso 9]>

            </td>

            </tr>

            </table>

            <![endif]-->

            <!-- // END TEMPLATE -->

                    </td>

                </tr>

            </table>

        </center>

    </body>

</html>';



$mail_object = new PHPMailer();

            $mail_object->IsSMTP(); // Indicamos que use SMTP

            //$mail_object->SMTPDebug = 3;   //activar modo debug                                   

            $mail_object->Host = 'a2plcpnl0042.prod.iad2.secureserver.net';  // Indicamos los servidores SMTP

            $mail_object->SMTPAuth = true;                               // Habilitamos la autenticación SMTP

            $mail_object->Username = 'reserva@hoteleshesperia.com.ve';                 // SMTP username

            $mail_object->Password = 'Nh123456nh';                           // SMTP password

            $mail_object->Port = 465;                                    // TCP port

            $mail_object->SMTPSecure = "ssl"; // Establece el tipo de seguridad SMTP a SSL.

            $mail_object->Timeout =30;

            //$mail_object->SMTPDebug = 1;

                           

            /** Configurar cabeceras del mensaje **/

            $mail_object->From = $contacto;                       // Correo del remitente

            $mail_object->FromName = utf8_decode('Información Hesperia Hotels & Resorts');           // Nombre del remitente

            $mail_object->Subject = 'Solicitud de Reserva en '.utf8_decode($razonsocial);                // Asunto

             

            /** Incluir destinatarios. El nombre es opcional **/

            $mail_object->addAddress($email);          

            /** Enviarlo en formato HTML **/

            $mail_object->isHTML(true);                                  

             

            /** Configurar cuerpo del mensaje **/

            $mail_object->Body    = utf8_decode($mensaje);

            $mail_object->AltBody = $mensaje;           

            

            

            /** Enviar mensaje... **/           

            if(!$mail_object->send()) {

                $errors = 'Ocurrio un error al registrar el pago. ';

                $errors .= 'Mailer Error: ' . $mail_object->ErrorInfo;

                

            }

            if(!isset($errors)){

              $errors = '1';

            }

            echo $errors;



?>