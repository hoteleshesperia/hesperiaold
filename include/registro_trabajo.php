<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/
session_start();

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
    header("location:../error.html");
    die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
unset($sql);
$ahora = time();

if ($_FILES["archivo"]["error"] > 0) {
 echo '<div class="alert alert-danger" role="alert">';
     echo FileUploadErrorMsg($_FILES["archivo"]["error"]);
 echo '</div>';
} else {

    if ( $_FILES['archivo']['type'] != 'application/pdf') {  
        echo '<div class="alert alert-danger" role="alert">
                  <p>El archivo que intentó subir no es un archivo .PDF valido. Intente de nuevo, si el problema persiste contacte al Administrador Principal del sitio</p>
              </div>';
    } else {  	
        
        $email          = strip_tags(strtolower(trim($_POST['email'])));
        $email          = filter_var($email,FILTER_SANITIZE_EMAIL);
        $nombre         = strip_tags(ucwords(trim($_POST['nombre'])));
        $telefono       = strip_tags(trim($_POST['telefono']));
        $telefono_habi  = strip_tags(trim($_POST['telefono_habi']));
        $direccion      = strip_tags(trim($_POST['direccion']));
        $archivo        = strtolower($_FILES['archivo']['name']);
        $contacto       = $_POST['contacto'];
        $dominio        = $_POST['dominio'];
        $razon_social   = $_POST['razon_social'];
        $newname        = 'curriculum_'.substr($ahora,-5).'_'.$_FILES['archivo']['name'];
        $ahora          = $_POST['ahora'];
        $especialidad   = implode(',', $_POST['check_list']);
        $hotelpreferencia = $_POST["hotelpreferencia"];
        $formacion      = $_POST["formacion"];
        $ciudad         = $_POST["ciudad"];
        
    if (!isset($_SESSION["agregadosXY"]) || $_SESSION["agregadosXY"] = 'Agregado') {
        $_SESSION["agregadosXY"] = 'Agregado';
        if(move_uploaded_file($_FILES['archivo']['tmp_name'], '../empleos/'.$newname)) { 
        graba_LOG("Envio de solicitud de trabajo: $email",$email,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
        
        $sql = sprintf("INSERT INTO hesperia_trabajo VALUES (NULL,'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                            mysqli_real_escape_string($mysqli,$nombre),
                            mysqli_real_escape_string($mysqli,$telefono),
                            mysqli_real_escape_string($mysqli,$telefono_habi),
                            mysqli_real_escape_string($mysqli,$direccion),
                            mysqli_real_escape_string($mysqli,$email),
                            mysqli_real_escape_string($mysqli,$newname),
                            mysqli_real_escape_string($mysqli,$especialidad),
                            mysqli_real_escape_string($mysqli,$ciudad),
                            mysqli_real_escape_string($mysqli,$formacion),
                            mysqli_real_escape_string($mysqli,$hotelpreferencia),
                            mysqli_real_escape_string($mysqli,$ahora));
        //die($sql);
        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
        if (mysqli_affected_rows($mysqli) >= 1)
            {  	$id=mysqli_insert_id($mysqli);
                echo '<br/><div class="alert alert-success" role="alert">
                <p>Hemos recibido su solicitud de empleo, en las próximas horas le estaremos contactando. Gracias por pensar en trabajar con nosotros.</p>
                </div>';
                    $asunto  = "Solicitud de Empleo desde $dominio";
                    $mensaje = "Saludos: Sr(a): $nombre<br>
                    Se ha registrado su solicitud de empleo desde $dominio.<br>\r\n
                    Cualquier duda contactenos al siguiente email: $contacto<br>
                    ----------------------------------------------------<br>\r\n
                    Este mensaje es privado y confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.\r\n<br>
                    ----------------------------------------------------<br>\r\n
                    $dominio<br><br>";
                    $headers = '';
                    $headers .= "MIME-Version:1.0\r\n";
                    $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                    $headers .= "Received:from www.$dominio\r\n";
                    $headers .= "X-Priority:3\r\n";
                    $headers .= "X-MSMail-Priority:Normal\r\n";
                    $headers .= "From: Hotel Hesperia <$contacto>\r\n";
                    $headers .= "X-Mailer:$contacto\r\n";
                    $headers .= "Return-path:$contacto\r\n";
                    $headers .= "Reply-To:$contacto\r\n";
                    $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
                    @mail($email,$asunto,$mensaje,$headers);
                    $headers = '';
                    $headers .= "MIME-Version:1.0\r\n";
                    $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                    $headers .= "Received:from www.$dominio\r\n";
                    $headers .= "X-Priority:3\r\n";
                    $headers .= "X-MSMail-Priority:Normal\r\n";
                    $headers .= "From: Hotel Hesperia <$contacto>\r\n";
                    $headers .= "X-Mailer:$contacto\r\n";
                    $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
                    $headers .= "Reply-To:$email\r\n";
                    $headers .= "Return-path:$email\r\n";
                    $mensaje = "Hemos recibido una solicitud de empleo de  $email [$nombre ] numero de telefono: [ $telefono ],<br>>";
                    @mail($contacto,$asunto,$mensaje,$headers);
                    $_SESSION["clickN"] = 1;
         } else
         { echo '<div class="alert alert-danger" role="alert">
              <p>Ha ocurrido un error inesperado. <br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
            </div>'; }
        }

    } else
         { echo '<div class="alert alert-danger" role="alert">
              <p>Ha ocurrido un error inesperado. <br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
            </div>'; }

    }
}
$id= $_SESSION["id_C"] = mysqli_insert_id($mysqli);
unset($result,$sql,$email,$headers);
$_POST = array();
echo '<meta http-equiv="refresh" content="3; url=https://hoteleshesperia.com.ve/trabajo"/>';
?>
