<?php

/* -------------------------------------------------------

Script  bajo los términos y Licencia

Apache License

Version 2.0, January 2004

https://www.apache.org/licenses/LICENSE-2.0

Autor:Hector A. Mantellini (Xombra)

--------------------------------------------------------*/

session_start();

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {

	header("location:../error.html");

	die();}

$antesdecore = 1;

include 'databases.php';

$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);

unset($sql);

$ahora = time();

$nivel = 3; # Usuario

$email = strip_tags(strtolower(trim($_POST['email'])));

$email = filter_var($email,FILTER_SANITIZE_EMAIL);

$apellidos = strip_tags(ucwords(trim($_POST['apellidos'])));

$nombres = strip_tags(ucwords(trim($_POST['nombres'])));

$telefono = strip_tags(trim($_POST['telefono']));

$numero_empresa  = '0';

$email_empresa = $empresa = '';

$nueva_clave = $_POST['clave'];

$clave_acceso = md5(trim($_POST['clave']));

$contacto = $_POST['contacto'];

$dominio  = $_POST['dominio'];

$fecha_nacimiento = $_POST['fechaNac'];

$cedula = $_POST['cedula'];



$pregunta_1 = $_POST['pregunta1'];

$pregunta_2 = $_POST['pregunta2'];

$pregunta_3 = $_POST['pregunta3'];



$respuesta_1 = $_POST['respuesta1'];

$respuesta_2 = $_POST['respuesta2'];

$respuesta_3 = $_POST['respuesta3'];

$sql = sprintf("SELECT email FROM hesperia_usuario WHERE email='%s'",

					mysqli_real_escape_string($mysqli,$email));

$result = QUERYBD($sql,$hostname,$user,$password,$db_name);

if (mysqli_num_rows($result) > 0){  

	echo '<div class="alert alert-danger" role="alert">

	  <p>Ya existe una cuenta registrada bajo la dirección de correo indicada<br/>Intente iniciar sesión o recupere su clave</p>

	</div>';

}else { 

	graba_LOG("Registro de Nuevo usuario: $email",$email,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);

	$solicitud = "$apellidos $nombres";

	$sql = sprintf("INSERT INTO hesperia_usuario (id,email,clave_acceso,nombres,apellidos,telefono,

				empresa,numero_empresa,email_empresa,

				fecha_registro,ultimo_login,nivel,valido,id_hotel, fecha_nacimiento, cedula, pregunta_1, pregunta_2, pregunta_3, respuesta_1, respuesta_2, respuesta_3)

				VALUES ( NULL,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",

					mysqli_real_escape_string($mysqli,$email),

					mysqli_real_escape_string($mysqli,$clave_acceso),

					mysqli_real_escape_string($mysqli,$nombres),

					mysqli_real_escape_string($mysqli,$apellidos),

					mysqli_real_escape_string($mysqli,$telefono),

					mysqli_real_escape_string($mysqli,$empresa),

					mysqli_real_escape_string($mysqli,$numero_empresa),

					mysqli_real_escape_string($mysqli,$email_empresa),

					mysqli_real_escape_string($mysqli,$ahora),

					mysqli_real_escape_string($mysqli,$ahora),

					mysqli_real_escape_string($mysqli,$nivel),

					mysqli_real_escape_string($mysqli,0),

					mysqli_real_escape_string($mysqli,0),

					mysqli_real_escape_string($mysqli,$fecha_nacimiento),

					mysqli_real_escape_string($mysqli,$cedula),

					mysqli_real_escape_string($mysqli,$pregunta_1),

					mysqli_real_escape_string($mysqli,$pregunta_2),

					mysqli_real_escape_string($mysqli,$pregunta_3),

					mysqli_real_escape_string($mysqli,$respuesta_1),

					mysqli_real_escape_string($mysqli,$respuesta_2),

					mysqli_real_escape_string($mysqli,$respuesta_3));

	$result = QUERYBD($sql,$hostname,$user,$password,$db_name);

	if (mysqli_affected_rows($mysqli)){ 



		echo '<br/><div class="alert alert-success" role="alert">

		<p>Su Registro se ha completado,se ha enviado un email de notificación!.<br/> Bienvenido(a)!</p>

		</div>';

			$asunto  = "Registro sitio web $dominio";

			$mensaje = "Saludos: Sr(a): $nombres<br>

			Se ha registrado como usuario  en el sitio web $dominio.<br>\r\n

			Su usuario es: $email<br>\r\n

			Su clave de acceso provisional es: $nueva_clave<br>\r\n<br>

			Una vez ingrese al sistema estara validado como usuario\r\n<br>

			Cualquier duda contactenos al siguiente email: $contacto<br>

			----------------------------------------------------<br>\r\n

			Este mensaje es privado y confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.\r\n<br>

			----------------------------------------------------<br>\r\n

			$dominio<br><br>";

			$headers = '';

			$headers .= "MIME-Version:1.0\r\n";

			$headers .= "Content-type:text/html; charset=UTF-8\r\n";

			$headers .= "Received:from www.$dominio\r\n";

			$headers .= "X-Priority:3\r\n";

			$headers .= "X-MSMail-Priority:Normal\r\n";

			$headers .= "From: Hotel Hesperia <$contacto>\r\n";

			$headers .= "X-Mailer:$contacto\r\n";

			$headers .= "Return-path:$contacto\r\n";

			$headers .= "Reply-To:$contacto\r\n";

			$headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";

			@mail($email,$asunto,$mensaje,$headers);

			/*$headers = '';

			$headers .= "MIME-Version:1.0\r\n";

			$headers .= "Content-type:text/html; charset=UTF-8\r\n";

			$headers .= "Received:from www.$dominio\r\n";

			$headers .= "X-Priority:3\r\n";

			$headers .= "X-MSMail-Priority:Normal\r\n";

			$headers .= "From: Hotel Hesperia <$contacto>\r\n";

			$headers .= "X-Mailer:$contacto\r\n";

			$headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";

			$headers .= "Reply-To:$email\r\n";

			$headers .= "Return-path:$email\r\n";

			$mensaje = "El usuario $email [$apellidos $nombre ] numero de telefono: [ $telefono ],<br>

						se ha registrado en el sitio $dominio.<br>

						Una vez ingrese al sistema sera validado.<br/>";

			@mail($contacto,$asunto,$mensaje,$headers); */

		echo '<meta http-equiv="refresh" content="3; url=https://hoteleshesperia.com.ve"/>';

 	} else{ echo '<div class="alert alert-danger" role="alert">

	  <p>Ha ocurrido un error inesperado. Probablemente ya la cuenta email esta registrada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>

	</div>'; }

}

unset($result,$sql,$email,$headers);

$_POST = array();

?>

