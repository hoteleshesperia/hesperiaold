<?php 
$tiempo=time();
$antesdecore = 1;
include_once('include/databases.php');


function mostrarPostMain($hostname,$user,$password,$db_name, $lugar){

	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
	$query ="SELECT a.*, b.nombre, b.apellido FROM hesperia_v2_post a inner join hesperia_v2_blog_autores b on(a.id_autor = b.id_autor)
	ORDER BY a.fecha DESC";

	$result=$mysqli->query($query);


	switch ($lugar) {
		case 'index':
			while ($reg=mysqli_fetch_assoc($result)) {

			  	$phpdate = strtotime( $reg["fecha"] );
				$mysqldate = date( 'Y-m-d H:i:s', $phpdate);

			  	echo " 
			  	<h2>
			  		<a href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>$reg[titulo]</a>
		        </h2>
		        <p class='lead'>
		            Autor: $reg[nombre] $reg[apellido]
		        </p>
		        <p><i class='fa fa-clock-o'></i> Publicado: $mysqldate</p>
		        
		        <hr>
		        <a href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>
		        	<img class='img-responsive img-hover' src='http://hoteleshesperia.com.ve/blg/img/$reg[imagen]' alt=''>
		        </a>
		        <hr>
		       	<p>$reg[subtitulo]</p>


		        <hr>
		        ";
			}
			/*echo "
				<ul class='pager'>
                    <li class='previous'>
                        <a href='#'>← Older</a>
                    </li>
                    <li class='next'>
                        <a href='#'>Newer →</a>
                    </li>
                </ul>
                ";*/
			break;
		
		case 'panel':
			echo "

			<div class='col-md-9'>
			<h3>Posts</h3>

			<p>
			   <a href='http://hoteleshesperia.com.ve/panelBlog/formularioPost'>Nuevo</a>
			</p>
			<table class='table table-bordered'>
				<thead>
					<tr>
					    <td>Autor</td>
					    <td>Titulo</td>
					    <td>Fecha</td>
					    <td>Acciones</td>
					</tr>
				</thead>
				<tbody>
				";
				while ($reg=mysqli_fetch_assoc($result)) {

			  	$phpdate = strtotime( $reg["fecha"] );
				$mysqldate = date( 'Y-m-d H:i:s', $phpdate );
				echo "

					<tr>
						<td>$reg[nombre] $reg[apellido]</td>
					    <td>$reg[titulo]</td>
					    <td>$mysqldate</td>
					    <td>
					    	<a href='http://hoteleshesperia.com.ve/panelBlog/asignarCategoria/$reg[id_post]''>Asignar Categorias</a> - 
					        <a href='http://hoteleshesperia.com.ve/panelBlog/formularioPost/$reg[id_post]'>Modificar</a> - 
					        <a href='#' class='eliminar-item' data-item='post' data-value='".json_encode($reg)."'
					        data-toggle='modal' data-target='#eliminar-item-modal'>Eliminar</a>
					    </td>	
					</tr>
				";
				}
			echo "
				</tbody>	
			</table>
			</div>
			";
			
			break;
	}

	while ($reg=mysqli_fetch_assoc($result)) {

	  	$phpdate = strtotime( $reg["fecha"] );
		$mysqldate = date( 'Y-m-d H:i:s', $phpdate );

	  	echo " 
	  	<h2>
	  		<a href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>$reg[titulo]</a>
        </h2>
        <p class='lead'>
           $reg[nombre] $reg[apellido]
        </p>
        <p><i class='fa fa-clock-o'></i> Publicado: $mysqldate</p>
        
        <hr>
      		<a href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>
		        <img class='img-responsive img-hover' src='http://localhost/blg/img/$reg[imagen]' alt=''>
		    </a>
        <hr>
       	<p>$reg[subtitulo]</p>


        <hr>
        ";
	}  
}

function mostrarCategoriasMain($hostname,$user,$password,$db_name, $lugar){

	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
	$query ="SELECT a.* FROM hesperia_v2_categoria a";

	$result=$mysqli->query($query);

	switch ($lugar) {
		case 'panel':
			echo "
			<div class='col-md-9'>
			<h3>Categorias</h3>
			<p>
			   <a class='btn btn-primary' data-toggle='modal' href='#' data-target='#formularioCategoria' id='crearCategoria'>Nuevo</a>
			</p>
			<table class='table table-bordered'>
				<thead>
					<tr>
					    <td>ID</td>
					    <td>Categoria</td>
					    <td>Acciones</td>
					</tr>
				</thead>
				<tbody>
				";
				while ($reg=mysqli_fetch_assoc($result)) {

			  	echo "
					<tr>
						<td>$reg[id_categoria]</td>
						<td>$reg[nombre_categoria]</td>
					    <td>
						    <a class='modificar-categoria' data-toggle='modal' href='#'
						     data-target='#formularioCategoria' data-value='".json_encode($reg)."'>
						       Modificar
						    </a>
						     - 
						     <a class='eliminar-item' data-toggle='modal' href='#'
						     data-target='#eliminar-item-modal' data-item='categoria' data-value='".json_encode($reg)."'>
						       Eliminar
						    </a>
					    </td>	
					</tr>
				";
				}
			echo "
				</tbody>	
			</table>
			</div>
			";
			
			break;
			

		case 'index':
			echo "
			<div class='col-md-3'>
              <br>
                <div class='well'>
                    <h4>Categorias</h4>
                    <div class='row'>
                        <div class='col-lg-12'>
                            <ul class='list-unstyled'>";
			while ($reg=mysqli_fetch_assoc($result)) {                      
            	echo "
            	                <li>

                                	 <a href='http://hoteleshesperia.com.ve/blog/categoria/$reg[id_categoria]-".toAscii($reg['nombre_categoria'])."'>$reg[nombre_categoria]</a>
                                </li>
                 ";
            }
            echo "
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
			";
			break;
	}
}

function mostrarBlogMain($hostname,$user,$password,$db_name, $lugar){

	$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);

	$query ="SELECT * FROM hesperia_v2_landingpages where acceso = 'blog'";
	$result=$mysqli->query($query);

	$reg=mysqli_fetch_assoc($result);
	/*echo '<section class="bg-1 text-center">
      <div class="blog-header">
        <h1 class="blog-title">The Bootstrap Blog</h1>
        <p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
      </div>
    </section>';*/
	echo "
     <section style=\"background: url('http://hoteleshesperia.com.ve/blg/$reg[banner]') no-repeat center center;
     background-size:cover; \" class='bg-1 text-center' >
      <div class='blog-header'>
       
      </div>
    </section>";

    echo"

    <div class='container'>
      <div class='page-header'>
		  <h1>$reg[titulo]<br> <small>By $reg[subtitulo]</small></h1>
	   </div>
      <div class='row'>
      	<div class='col-md-9'>
      ";
      	if(isset($_GET["verPost"])){
            mostrarPostIndividual($mysqli, $_GET["verPost"]);
        }else if(isset($_GET["categoria"])){
        	cargarCategoriaIndividual($mysqli, $_GET["categoria"]);
        }else{
        	mostrarPostMain($hostname,$user,$password,$db_name, $lugar);
        }
            
            
    echo "  
    	</div>";
    	mostrarCategoriasMain($hostname,$user,$password,$db_name, $lugar);
echo "
        </div>

    </div>

    </div>
    ";	
}

function getObjetoItem($mysqli, $item){

	//$mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
	switch ($item) {
		case 'infoGral':
			$query ="SELECT a.* FROM hesperia_v2_landingpages a";
			break;
		
		default:
			$query="";
			break;
	}
	$result=$mysqli->query($query);

	return ($result);
}

function mostrarPostIndividual($mysqli, $id){

	$query ="SELECT a.*, b.nombre, b.apellido, b.resena FROM hesperia_v2_post a
	inner join hesperia_v2_blog_autores b on(a.id_autor = b.id_autor)
	where a.id_post =$id";

	$result=$mysqli->query($query);

	if($result->num_rows>0){

		$reg=mysqli_fetch_assoc($result);
		
		$phpdate = strtotime( $reg["fecha"] );
		$mysqldate = date( 'Y-m-d H:i:s', $phpdate);
		echo "
		
                <!-- Blog Post -->
                <h2>$reg[titulo]</h2>
                <hr>

                <!-- Date/Time -->
                <p><i class='fa fa-clock-o'></i> Fecha: $mysqldate</p>

                <hr>

                <!-- Preview Image -->
                <img class='img-responsive' src='http://hoteleshesperia.com.ve/blg/img/$reg[imagen]'' alt='$reg[titulo]'>

                <hr>

                <!-- Post Content -->
                <p class='lead'>$reg[subtitulo]</p>
                $reg[contenido]
                <h3 class='text-primary'>Sobre el autor</h3>
                
                <blockquote>
                  <h4>$reg[nombre] $reg[apellido]</h4>
				  <p>$reg[resena]</p>
				</blockquote>
                <hr>
                <h3 class='text-primary'>Compartir</h3>
                <ul class='list-unstyled list-inline list-social-icons'>
                    <li>
                        <a href='javascript: void(0);'
                         onclick='window.open(\"http://www.facebook.com/sharer.php?u=http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-$reg[titulo]/\",
                         	\"ventanacompartir\", \"toolbar=0, status=0, width=650, height=450\");'>
                         <i class='fa fa-facebook-square fa-2x'></i></a>
                    </li>
               
                    <li>
                        <a onclick=\"javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\"
                         href=\"https://plus.google.com/share?url=http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."\"><i class='fa fa-google-plus-square fa-2x'></i></a>
               
                    </li>

                    <li>
                       <a href='#' id='share-twitter-button'
						 data-url='http://hoteleshesperia.com/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>
						 <i class='fa fa-twitter-square fa-2x'></i>
						</a>

                    </li>
                </ul>
                ".//<h4 class='text-primary'>Social</h4><div class='fb-share-button' data-href='http://localhost/blg/blog.php?verPost=$reg[id_post]' data-layout='icon_link' data-mobile-iframe='true'></div>
	            //<br><hr>
	            "
                ";
                if (isset($_SESSION["referencia"])){

                	echo " 	
		                <!-- Blog Comments -->

		                <!-- Comments Form -->
		                <div class='well'>
		                    <h4>Deja un comentario:</h4>
		                    
		                        <div class='form-group'>
		                            <textarea id='contenido-comentario' class='form-control' rows='3'></textarea>
		                        </div>
		                        <button type='submit' id='comentar-post' class='btn btn-primary' data-idpost='$reg[id_post]'>Comentar</button>
		                   
		                </div>

		                <hr>
                	";
                }else{
                	echo "<h3 class='text-primary text-center'>Regístrese para comentar</h3>";
                }
               
	}

	cargarComentarios($mysqli, $id);
	$mysqli->close();
}

function cargarComentarios($mysqli, $id){

	$query ="SELECT a.*, b.nombres, b.apellidos, b.email FROM hesperia_v2_blog_comentarios a 
	inner join hesperia_usuario b on(a.id_usuario = b.id)
	where a.id_post =$id";

	$result=$mysqli->query($query);
	
	if($result->num_rows>0){
		echo "<h3 class='text-primary'>Comentarios</h3>";
		while($reg=mysqli_fetch_assoc($result)){
			$phpdate = strtotime( $reg["fecha"] );
			$mysqldate = date( 'Y-m-d H:i:s', $phpdate);
			
			echo "
			<div class='media'>
		    	<div class='media-body'>
		        	<h4 class='media-heading'>$reg[nombres] $reg[apellidos]
		            	<small>Fecha: $mysqldate</small>
		            </h4>
		            $reg[contenido]
		        </div>
		    </div>
	    	";
		}
	}else{
		echo "<h3>Sin Comentarios</h3>";
	}
	
}

function cargarCategoriaIndividual($mysqli, $id){

	$query ="SELECT a.*, b.nombre, b.apellido FROM hesperia_v2_post a inner join
	 hesperia_v2_blog_autores b on(a.id_autor = b.id_autor) inner join hesperia_v2_categoria_post c 
	 on(a.id_post = c.id_post) 
	where c.id_categoria = $id";

	$result=$mysqli->query($query);
	
	if($result->num_rows>0){

		while ($reg=mysqli_fetch_assoc($result)) {

	  	$phpdate = strtotime( $reg["fecha"] );
		$mysqldate = date( 'Y-m-d H:i:s', $phpdate );

	  	echo " 
	  	<h2>
	  		<a href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>$reg[titulo]</a>
        </h2>
        <p class='lead'>
            Autor: <small>$reg[nombre] $reg[apellido]</small>
        </p>
        <p><i class='fa fa-clock-o'></i> Publicado: $mysqldate</p>
        
        <hr>
        <a href='http://hoteleshesperia.com.ve/blog/post/$reg[id_post]-".toAscii($reg['titulo'])."'>
        	<img class='img-responsive img-hover' src='http://hoteleshesperia.com.ve/blg/img/$reg[imagen]' alt=''>
        </a>
        <hr>
       	<p>$reg[subtitulo]</p>
        <hr>
        ";
	   }  
	}else{
		echo "<h3 class='text-center'>Sin Publicaciones recientes</h3>";
	}
	
}

function asignacionCategorias($mysqli, $id_post){
	if ($id_post!="") {
		$array_cat_post =  Array();
		$array_solo_cat =  Array();

		$query ="SELECT a.*, b.id_post from hesperia_v2_categoria_post a right join
		 hesperia_v2_post b on(a.id_post = b.id_post) where b.id_post =$id_post";

		$query2 = "SELECT * from hesperia_v2_categoria";

		$result=$mysqli->query($query);
		$i=0;
		$j=0;
		while($reg=mysqli_fetch_assoc($result)){
			$array_cat_post[$i]=$reg;
			//echo("este post tiene:".$array_cat_post[$i]["id_categoria"]."<br>");
			$i++;
		}

		if (count($array_cat_post)<1) {
			echo "<h3>El post al que intenta acceder no existe</h3>";
			$mysqli->close();
			exit();
		}

		$result2=$mysqli->query($query2);

		while($reg2=mysqli_fetch_assoc($result2)){
			$array_solo_cat[$j]=$reg2;
			//echo($array_solo_cat[$j]["id_categoria"]."-"); 
			$j++;
		}

		for ($i=0; $i <count($array_solo_cat) ; $i++) { 
			for ($j=0; $j <count($array_cat_post) ; $j++) { 
				if($array_solo_cat[$i]["id_categoria"]==$array_cat_post[$j]["id_categoria"]){
					$array_solo_cat[$i]["asignada"]="S";
				}
			}
		}

		echo " 
			<div class='col-md-9'>
			<h3>Asignar Categorias</h3>
			<input type='hidden' id='id-post' value='".$array_cat_post[0]["id_post"]."'>
			<table class='table table-bordered'>
				<thead>
					<tr>
					    <td>Categoria</td>
					    <td>Accion</td>
					</tr>
				</thead>
				<tbody>
				    <tr>
		";

		for ($i=0; $i <count($array_solo_cat) ; $i++) {
			if (isset($array_solo_cat[$i]['asignada'])) {
				echo "
				    <td>".$array_solo_cat[$i]["nombre_categoria"]."</td>
				    <td><a href='#' data-idcat='".$array_solo_cat[$i]["id_categoria"]."' class='asignar-categoria' data-accion='eliminar-cat'>Eliminar</a></td>
				";
			 	//echo "<br>".$array_solo_cat[$i]['asignada']." - ".$array_solo_cat[$i]['id_categoria'];
			 }else{
			 	echo "
				    <td>".$array_solo_cat[$i]["nombre_categoria"]."</td>
				    <td><a href='#' data-idcat='".$array_solo_cat[$i]["id_categoria"]."' class='asignar-categoria' data-accion='asignar-cat'>Asignar</a></td>
				";
			 	//echo "<br>No Asignada - ".$array_solo_cat[$i]['id_categoria'];
			 } 
			echo "</tr>";
		}		

		echo "	  
				</tbody>	
			</table>
		</div>
		";
	}
}

 ?>