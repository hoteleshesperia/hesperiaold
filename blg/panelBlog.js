$(document).ready(function(){
	$("#contactForm, #form-general, #form-login").submit(function(){
		return false;
	});

	$(".summernote").summernote({
		lang: 'es-ES',
        toolbar: [
	        ['style', ['bold', 'italic', 'underline', 'clear', 'link', 'color']],
	        ['para', ['ul', 'ol', 'paragraph']],
	        
        ]
	});

	$("#guardarPost").click(function(){

	//alert($("#contactForm").serialize()+" - "+$(".summernote").code());	
		var titulo = $("#titulo");
		var subtitulo = $("#subtitulo");
		var portada = document.getElementById("portada");
		var temp_portada=portada.files;

		var errores="";

		alert(titulo.val()+"-"+subtitulo.val());
		if(titulo.val()==null || titulo.val()==""){
			errores+="Titulo es requerido\n ";
		}else if(titulo.val().length>100){
			errores="titulo debe tener un maximo de 100 caracteres\n";
		}


		if(subtitulo.val()==null || subtitulo.val()==""){
			errores+="Subtitulo es requerido\n ";
		}else if(subtitulo.val().length>200){
			errores="subtitulo debe tener un maximo de 200 caracteres\n";
		}
		if($(this).data("accion")=="nuevo"){
			if(temp_portada[0]==null || temp_portada[0]=="undefined"){
           		errores+="Imagen de portada es requerida\n ";
			}
		}
		

		if($(".summernote").code()==""){
			errores+="El contenido del post no debe estar vacio\n";
		}

		if (errores!="") {
			alert(errores);
		}else{
			var item_data= new FormData();

			item_data.append("titulo", titulo.val());
			item_data.append("subtitulo", subtitulo.val());
			item_data.append("imagen", temp_portada[0]);
			item_data.append("contenido", $(".summernote").code());

			if ($(this).data("accion")=="nuevo") {
			  item_data.append("accion", "insert");
			  item_data.append("anterior", "");
			}else if($(this).data("accion")=="modificar"){
			  item_data.append("id", $('#id_post').val());
			  item_data.append("accion", "update");
			  item_data.append("anterior", $("#anterior").val());
			}

			$.ajax({

				type:"POST",
				url:"https://hoteleshesperia.com.ve/blg/blog/funcionesPost.php",
				data:item_data,
				contentType:false,
			 	processData:false,
				cache:false,
				success: function(response){
					if(response=="ok"){
						alert("Solicitud procesada correctamente")
						location.reload();
					}else{
						alert("Ha ocurrido un error, vuelva a intentarlo");
						location.reload();
					}
				},
				error: function(response){
					alert("Ha ocurrido un error, vuelva a intentarlo");
					location.reload();
				}
			});	
		}

		
	});

	$(".modificar-categoria").click(function(){
		var obj = $(this).data("value");
		$("#formularioCategoria #lbl-form-categoria").text("Modificar Categoria");
		$("#boton-form-categoria").attr("data-accion","mod");
		$("#id-cat").val(obj.id_categoria);
		$("#nombre-categoria").val(obj.nombre_categoria);	
	});


	$("#crearCategoria").click(function(){

		$("#boton-form-categoria").attr("data-accion","add");
		$("#nombre-categoria").val("");
		$("#id-cat").val(-1);
		
	});
	
	$("#boton-form-categoria").click(function(){
		var nombre = $("#nombre-categoria").val();
		var id = $("#id-cat").val();
		var accion = $(this).data("accion");
		if(nombre!=""){
		  var item_data= new FormData();

			if(accion=="add"){	
				item_data.append("nombre", nombre);
				item_data.append("accion", "insert");
				
	
			}else if(accion=="mod"){
				item_data.append("id", id);
				item_data.append("nombre", nombre);
				item_data.append("accion", "update");
				
			}
			funcionAjax(item_data, "funcionesCategorias");
		}else{
			alert("campo requerido")
		}

	});

	$(".eliminar-item").click(function(){
		var valor = $(this).data("value");
		var item = $(this).data("item");

		if(item=="categoria"){
			$("#aceptar-btn-eliminar").attr("data-value", valor.id_categoria);
			
		}else if(item=="post"){
			
			$("#aceptar-btn-eliminar").attr("data-value", valor.id_post);
		}

		$("#aceptar-btn-eliminar").attr("data-item", item);	$("#aceptar-btn-eliminar").attr("data-item", item);	
	});
		
	$("#aceptar-btn-eliminar").click(function(){
		var valor = $(this).data("value");
		var item = $(this).data("item");

		var item_data= new FormData();
		item_data.append("id",valor);
		item_data.append("accion", "delete");

		if (item=="categorias") {
			funcionAjax(item_data, "funcionesCategorias");
		}else if("post"){
			funcionAjax(item_data, "funcionesPost");
		}

		
	});

	$("#guardarInfoGral").click(function(){

		var titulo = $("#titulo");
		var subtitulo = $("#subtitulo");
		var portada = document.getElementById("portada");
		var anterior = $("#anterior").val();
		var temp_portada=portada.files;

		var item_data= new FormData();

		var errores="";

		if(titulo.val()==null || titulo.val()==""){
			errores+="Titulo es requerido\n ";
		}else if(titulo.val().length>100){
			errores="titulo debe tener un maximo de 100 caracteres\n";
		}


		if(subtitulo.val()==null || subtitulo.val()==""){
			errores+="Subtitulo es requerido\n ";
		}else if(subtitulo.val().length>200){
			errores="subtitulo debe tener un maximo de 200 caracteres\n";
		}

		if (errores!="") {
			alert (errores);
		}else{
			if (temp_portada[0]!=null && temp_portada[0]!="" && temp_portada[0]!="undefined") {
			item_data.append("imagen", temp_portada[0]);	
			}else{
				item_data.append("imagen", "");
			}

			item_data.append("titulo", titulo.val());
			item_data.append("subtitulo", subtitulo.val());
			item_data.append("accion", "update");
			item_data.append("anterior", "");

			funcionAjax(item_data, "modificarBlog");
		}
	});

	$(".asignar-categoria").click(function(){

		var accion = $(this).data("accion");
		var id_categoria = $(this).data("idcat");
		var id_post = $("#id-post").val();

		var item_data= new FormData();
		item_data.append("id",id_categoria);
		item_data.append("id_post",id_post);
		item_data.append("accion", accion);

		funcionAjax(item_data,"funcionesCategorias");
	});

	$("#boton-login").click(function(){
	 	var email = $("#inputEmail").val();
	 	var pass = $("#inputPassword").val();

		if(email==null || email==""){
			alert("email es requerido");
		}else if(pass==null || pass==""){
			alert("contraseña es requerida");
		}else{
			var item_data= new FormData();
			item_data.append("email",email);
			item_data.append("password",pass);

			$.ajax({

				type:"POST",
				url:"https://hoteleshesperia.com.ve/blg/blog/login.php",
				data:item_data,
				contentType:false,
				processData:false,
				cache:false,
				success: function(response){
					if(response=="ok"){
						alert("Procesado Correctamente "+response);
						location.reload();	
					}else{
						alert("Ha ocurrido un error, vuelva a intentarlo "+response);
					}
					location.reload();
				},
				error: function(response){
					alert("Ha ocurrido un error, vuelva a intentarlo");
					location.reload();
				}
			})
		}

	});

	
		

	function funcionAjax(formData,ruta){
		$.ajax({
			type:"POST",
			url:"https://hoteleshesperia.com.ve/blg/blog/"+ruta+".php",
			data:formData,
			contentType:false,
			processData:false,
			cache:false,
			success: function(response){
				if(response=="ok"){
					alert("Procesado Correctamente "+response);
					location.reload();	
				}else{
					alert("Ha ocurrido un error, vuelva a intentarlo "+response);
				}
				location.reload();
			},
			error: function(response){
				alert("Ha ocurrido un error, vuelva a intentarlo");
				location.reload();
			}

		});
	}	
});

	function validar_formato(obj){

	value= obj.val();

		if(!value.match(/.(jpg)|(jpeg)|(png)$/)){
			return true;
		}else{
			return false;
		}
	}

	function validar_longitud(min,max, obj){
		alert(obj.value);
		if(obj.val().length<min || obj.val().length>max){

			return false;

		}else{

			return true;	
		}
	}

	function validar_dimension(maxH,maxW, obj){
		value = obj;
		alert("H:"+value.height+"-"+"w"+value.width);
		if (value.height > maxH || value.width > maxW) {
			return false;
		}
	}