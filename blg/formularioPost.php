<?php 

$tiempo=time();
$antesdecore = 1;
include_once('include/databases.php');

$titulo_pagina ="";

$titulo_post ="";
$subtitulo ="";
$bloque ="";
$img_anterior = "";

if (isset($_GET["idpost"])) {
    $id=$_GET["idpost"];
    $mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
    $query = "SELECT * from hesperia_v2_post where id_post = $id";
    $result=$mysqli->query($query);
    //$result=getObjetoItem($query, "post");
    $rows = $result->num_rows;

    if ($rows>0) {
        $reg= mysqli_fetch_assoc($result);
        $titulo_post = $reg["titulo"];
        $subtitulo = $reg["subtitulo"];
        $bloque = $reg["contenido"];
        $img_anterior = $reg["imagen"];
        $accion = "data-accion='modificar'";
    }
  
    $mysqli->close();

    $titulo_pagina ="Modificar Post";

}else{
    $titulo_pagina ="Nuevo Post";
    $accion = "data-accion='nuevo'";    
}

?>
<div class="col-md-9">
    <h3><?php echo"$titulo_pagina"; ?></h3>
    <form name="sentMessage" id="contactForm" novalidate>
        <div class="control-group form-group">
            <div class="controls">
                <label>Titulo</label>
                <input type="text" class="form-control" name="titulo" value=<?php echo "'$titulo_post'"; ?> id="titulo">
                <p class="help-block"></p>
            </div>
        </div>
        <div class="control-group form-group">
            <div class="controls">
                <label>Subtitulo</label>
                <input type="text" class="form-control" name="subtitulo" id="subtitulo" value =<?php echo "'$subtitulo'";?> >
            </div>
        </div>
        <div class="control-group form-group">
            <div class="controls">
                <label>Portada</label>
                <input type="file" class="form-control" name="portada" id="portada">
                <p class="help-block">Solo se permiten imagenes en formato JPG y PNG con resolucion(1400x480)</p>
            </div>
        </div>

        <div class="control-group form-group">
            <div class="controls">
                <label>Contenido</label>
                <div class="col-md-12" style="background:#eee;" class="menu-parrafo">
                    <!--<label>Tamaño</label>
                    <select>
                        <option>1 Columna</option>
                        <option>2 Columna</option>
                        <option>3 Columna</option>
                    </select>-->
                </div>                                
                <textarea class="summernote" id="summernote1" name="contenido" ><?php echo($bloque);?></textarea>
                <input type="hidden" value=<?php echo "'$img_anterior'"; ?> id="anterior">
                <?php if (isset($reg["id_post"])) {
                   echo "
                       <input type='hidden' value='$reg[id_post]' id='id_post'>
                   ";
                } 

                 ?>
            </div>
        </div>
        <div id="success"></div>
                        <!-- For success/fail messages -->
        <button type="submit" id="guardarPost" class="btn btn-primary" <?php echo($accion) ?>>Guardar</button>
    </form>
</div>