<?php 
include_once('blog/funcionesBlog.php');
 ?>
<html>
<head>
	<title>Hoteles Testing</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <link href="https://hoteleshesperia.com.ve/css/datepicker.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/carousel.css" rel="stylesheet" >
<!--    <link href="css/slider-hotel.css" rel="stylesheet">-->
    <link href="https://hoteleshesperia.com.ve/css/lightbox.css" rel="stylesheet" >
    
    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=es" async defer></script>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://hoteleshesperia.com.ve/js/jquery.price_format.2.0.js"></script>
    <link href="https://hoteleshesperia.com.ve/css/modern-business.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/bootstrap.min.css" rel="stylesheet">
    <!--<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>   -->
    <script src="https://hoteleshesperia.com.ve/noticias/wp-includes/js/jquery-migrate.min.js"></script>

    <script src="http://localhost/hoteles/blog.js"></script>
</head>

<style type="text/css">

.portada-span{
    display: none;
}

#myCarousel .carousel-caption{
    left:0;
    text-align: left;
    margin-left: 25px;
}

#myCarousel, #myCarousel .item{
    height: auto;
}

.no-space{
    padding: 0;
}
.pdf-thumb-box {
  display: inline-block !important;
  position: relative !important;
  overflow: hidden;
}
.pdf-thumb-box-overlay {
  display: none;
}

.hover-overlay{
  display: inline;
  text-align: center;
  position: absolute;
  transition: background 0.2s ease, padding 0.8s linear;
  background-color: rgba(0,0,0,0.4);
  color: #fff;
  width: 100%;
  height: 100%;
  text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
}

.pdf-thumb-box-overlay span {
  position: relative;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.pdf-thumb-box-overlay h2{
    margin-top:25%;

}
#my-reserva{
  position: absolute;
  z-index: 1 !important;
  
}

#myCarousel{
  //z-index: -1;
}

.navbar{
  margin-bottom: 0px;
}

.carousel-hab .featurette-heading{
  margin-top: 40px;
}
#carousel-example-generic{
  height: auto;
}
.carousel-hab, .carousel-hab .carousel-inner, .carousel-hab .item{
    height: 400px;
  }

@media(max-width:767px){
   .carousel-hab, .carousel-hab .carousel-inner, .carousel-hab .item{
    height: auto;
    //min-height: 400px;
  }
}

.index-icon{
    margin-top: 50%;
    font-size: 50px;
  }

  .rounded-img{
    width: 150px;
    height: 150px;
    background: #eee;
    border-radius: 100%;
    margin: 5px 15px 5px 5px;
  }

  .img-example{
    background: #eee;
   // width: 300px;
    height: 200px;
    margin-bottom: 20px;
  }

  .img-example1{
     background: #eee;
   // width: 300px;
    height: 300px;
    margin-bottom: 20px; 
  }
  .tabs{
    height: 80px;
    text-align: center;

  }
  .grey-section{
   // padding: 8em 0;
    background: #f3f3f3;
  }
  #paquetes, #nosotros, #welcome, #galeria{
    padding: 7em 0;
  }

  .bg {
  background: url('/hoteles/banner.jpg') no-repeat center center;
  position: fixed;
  width: 100%;
  height: 350px; /*same height as jumbotron */
  top:0;
  left:0;
  z-index: -1;
}

.jumbotron {
  height: 350px;
  color: white;
  text-shadow: #444 0 1px 1px;
  background:transparent;
}

.divider {
    height:50px;
}

section {
    height:500px;
    padding-top:50px;
    padding-bottom:50px;
    overflow:auto;
}

.bg-1 {
    color:#fff;
}
</style>
<body>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

	    <nav class="navbar navbar-default" role="navigation">
       <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-hesperia" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md" href="https://hoteleshesperia.com.ve">
                <img src="https://hoteleshesperia.com.ve/img/logo/logohesperiahotels-m.png" class="img-responsive logo-main">
            </a>
            <a class="navbar-brand visible-lg visible-md" href="https://hoteleshesperia.com.ve">
                <img src="https://hoteleshesperia.com.ve/img/logo/logohesperiahotels.png" class="img-responsive logo-main">
            </a>
        </div>
            <div class="collapse navbar-collapse" id="menu-hesperia">
                <ul class="nav navbar-nav">
                    <li><a href="https://hoteleshesperia.com.ve">Inicio</a></li>
                    <li>
                    <?php
//                        if ($_GET['principio'] == 0) { $_GET['sucursal'] = 0;}
//    esta parte muestra solo el link de nosotros general sin importar cual hotel fue seleccionado
echo '<a href="https://hoteleshesperia.com.ve/nosotros" title="Hesperia Hotels & Resorts Venezuela">Nosotros</a>'; ?>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hoteles <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                            $nombres=array("Hesperia-Eden-Club","Hesperia-Isla-Margarita","Hesperia-Playa-el-Agua","Hesperia-WTC-Valencia");
                            $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";
                            $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);
                            $i=0;
                            while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC))
                                { echo '
                                    <li>
                                        <a href="https://hoteleshesperia.com.ve/'.$nombres[$i].'" target="_self" title=" '.$rows["nombre_sitio"].'">'.$rows["nombre_sitio"].'</a>
                                    </li>';
                                    $i++;
                                }
echo     '</ul>
                    </li>';
echo '
                    <li><a href="https://hoteleshesperia.com.ve/reuniones-eventos" title="Eventos"> Reuniones y Eventos</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Paquetes<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="https://hoteleshesperia.com.ve/semana-santa" title="Semana Santa en Hesperia">Semana Santa</a>
                            </li>
                            <li>
                                <a href="https://hoteleshesperia.com.ve/paquetes" title="Todos los paquetes">Todos</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="https://hoteleshesperia.com.ve/partai" title="Partai">Partai</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">';
                    if (!isset($_SESSION["referencia"])){
                echo '
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-users hidden-xs hidden-sm"></i>Mi Cuenta<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a id="modal-433854" href="#modal-container-433854" data-toggle="modal" title="Sesión de Usuarios registrados">Iniciar sesión
                                            </a>
                            </li>
                            <li>
                                <a id="modal-433854" href="#modal-container-200917" data-toggle="modal" title="Recuperar Clave">Recuperar Clave
                                            </a>
                            </li>
                        </ul>
                    </li>
                    <li><a id="modal-433854" href="#modal-container-433666" data-toggle="modal" title="Registro de nuevo usuarios"><i class="fa fa-user hidden-xs hidden-sm"></i> Registrarse
                        </a>
                    </li>
                    '; }
                else
        {
                echo '
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Usuario: <strong>'.$_SESSION["nombre"].'</strong><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="https://hoteleshesperia.com.ve/perfil/mi-cuenta" title="Ver mi Cuenta">
                                    <i class="fa fa-users"></i> Mi Cuenta
                                </a>
                            </li>';
                      if (isset($_SESSION["referencia"])){
                              if ($_SESSION["nivel"] <= 2){
                                echo '<li><a href="panel/home.php" target="_blank" title="Panel de Control"><i class="fa fa-server hidden-xs hidden-sm"></i> Panel de Control </a>
                    </li>';
                                        } }   

echo '
                            <li>
                                <a href="https://hoteleshesperia.com.ve/logout" title="Salir Del Sistema | Cerrar Sesion">
                                    <i class="fa fa-sign-out"></i> Salir
                                </a>
                            </li>
                        </ul>
                    </li>'; }
echo '
                </ul>
            </div>
        </div>
    </nav>';
    ?>

    

</body>
</html>