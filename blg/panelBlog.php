<?php 

include_once('blog/funcionesBlog.php');
session_start();
 ?>

<html>
<head>
    <meta charset="UTF-8">
	<title>Hoteles Testing</title>
    
    <link href="https://hoteleshesperia.com.ve/css/modern-business.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <link href="https://hoteleshesperia.com.ve/css/datepicker.css" rel="stylesheet">
    <link href="https://hoteleshesperia.com.ve/css/carousel.css" rel="stylesheet" >
<!--    <link href="css/slider-hotel.css" rel="stylesheet">-->
    <link href="https://hoteleshesperia.com.ve/css/lightbox.css" rel="stylesheet" >
    
  <!--  <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=es" async defer></script>-->
  <!--  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->
  <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
    <script src="https://hoteleshesperia.com.ve/js/jquery.js"></script>
    <script src="https://hoteleshesperia.com.ve/js/jquery.price_format.2.0.js"></script>
   <!-- <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
    <script type="text/javascript" src="https://hoteleshesperia.com.ve/blg/panelBlog.js"></script>


 <link href="https://hoteleshesperia.com.ve/panel/css/summernote.css" rel="stylesheet" >
   
<script src="https://hoteleshesperia.com.ve/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://hoteleshesperia.com.ve/panel/js/summernote.min.js" type="text/javascript"></script>
<script src="https://hoteleshesperia.com.ve/panel/js/summernote-es-ES.js" type="text/javascript"></script>
    
</head>

<body>


    <div class="container">

        <?php 

            if(!isset($_SESSION["username"])){

            ?>
          <form id="form-login" class="form-signin col-md-6 col-md-offset-3">
            <h2 class="form-signin-heading text-center">Bienvenido</h2>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email" required="" autofocus="">
            <label for="inputPassword" class="sr-only">Contraseña</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">

            <button class="btn btn-lg btn-primary btn-block" id="boton-login"  type="submit">Iniciar Sesion</button>
          </form>

   
          <?php 

            }else{

           ?>
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hoteles Hesperia
                    <small>Blog</small>
                </h1>
            </div>

        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <div class="list-group">
                    <a href="https://hoteleshesperia.com.ve/panelBlog/posts" class="list-group-item">Posts</a>
                    <a href="https://hoteleshesperia.com.ve/panelBlog/categorias" class="list-group-item">Categorias</a>
                    <a href="https://hoteleshesperia.com.ve/panelBlog/general" class="list-group-item">General</a>
                    <a href="https://hoteleshesperia.com.ve/panelBlog/logout" class="list-group-item">Salir</a>
                    
                </div>
            </div>
            <!-- Content Column -->
            <?php 
                if (isset($_GET["q"])) {

                    switch (strtolower($_GET["q"])){

                    case 'formulariopost':
                        include_once("formularioPost.php");
                    break;

                    case 'general':
                        include_once("formularioBlog.php");
                    break;

                    case 'posts':
                        mostrarPostMain($hostname,$user,$password,$db_name, "panel");
                    break;

                    case 'categorias':
                        mostrarCategoriasMain($hostname,$user,$password,$db_name, "panel");
                    break;

                    case 'asignarcategoria':
                        
                        if ($_GET["idpost"]!=null) {
                            $id_post=$_GET["idpost"];
                        }else{
                            $id_post="";
                        }
                        asignacionCategorias($mysqli, $id_post);
                    break;

                    case 'logout':
                        //session_start();
                        session_unset($_SESSION["id_autor"]);
                        session_unset($_SESSION["username"]);
                        session_destroy();
                        //session_unset($_SESSION["username"]);

                      // echo "Hola majo";
                        header('Location: /hh/panelBlog');
                    break;

                    default:
                        # code...
                        break;
                    }
                }else{

                    mostrarPostMain($hostname,$user,$password,$db_name, "panel");

                }
                
             ?> 
            
        </div>
        <!-- /.row -->

        <hr>
        <?php 
            include_once("modales.php");
        ?>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright © Your Website 2014</p>
                </div>
            </div>
        </footer>

        <?php 

        }
        
        ?>
     </div>
</body>
</html>
	