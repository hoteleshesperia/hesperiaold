<div class="modal fade" id="formularioCategoria" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lbl-form-categoria">Nueva Categoria</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="nombre-categoria" class="control-label">Nombre</label>
            <input type="text" class="form-control" id="nombre-categoria">
            <input type="hidden" name="id-cat" id="id-cat" value="-1">
          </div>
        </form>
      </div>
      <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" id="boton-form-categoria" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" id="eliminar-item-modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Atención</h4>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar este elemento permanentemente?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" id="aceptar-btn-eliminar" class="btn btn-primary">Aceptar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->