<?php 
$tiempo=time();
$antesdecore = 1;
include_once('include/databases.php');
 $mysqli=CONECTAR_BD($hostname,$user,$password,$db_name);
 $result=getObjetoItem($mysqli, "infoGral");

 $reg= mysqli_fetch_assoc($result);
 
 $mysqli->close();

 ?>

<div class="col-md-9">
    <h3>General</h3>
    <form name="general" id="form-general">
        <div class="control-group form-group">
            <div class="controls">
                <label>Titulo</label>
                <input type="text" class="form-control" value=<?php echo "'$reg[titulo]'"; ?> name="titulo" id="titulo" required>
                <p class="help-block"></p>
            </div>
        </div>
        <div class="control-group form-group">
            <div class="controls">
                <label>Subtitulo</label>
                <input type="text" class="form-control" value=<?php echo "'$reg[subtitulo]'"; ?> name="subtitulo" id="subtitulo" required>
            </div>
        </div>
        <div class="control-group form-group">
            <div class="controls">
                <label>Portada</label> 
                <input type="file" class="form-control" name="portada" id="portada">
                <p class="help-block">Solo se permiten imagenes en formato JPG y PNG con resolucion(1400x480)</p>
            </div>
        </div>
        <input type="hidden" name="anterior" value=<?php echo "'$reg[banner]'"; ?>>
        <div id="success"></div>
                        <!-- For success/fail messages -->
        <button type="submit" id="guardarInfoGral" class="btn btn-primary">Guardar</button>
    </form>
</div>