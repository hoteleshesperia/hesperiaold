$(document).ready(function(){


	$("#comentar-post").click(function(){

		var contenido = $("#contenido-comentario").val();
		var id_post = $(this).data("idpost");
		
		if (contenido==null || contenido=="") {
			return false;
		}else{
			var item_data= new FormData();
			item_data.append("contenido", contenido);
			item_data.append("id_post", id_post);
			item_data.append("accion", "insert");
			$.ajax({
				type:"POST",
				url:"https://hoteleshesperia.com.ve/blg/blog/funcionesComentarios.php",
				data:item_data,
				contentType:false,
				processData:false,
				cache:false,
				success: function(response){
					if(response=="ok"){
						alert("Procesado Correctamente "+response);
						location.reload();	
					}else{
						alert("Ha ocurrido un error, vuelva a intentarlo "+response);
					}
					location.reload();
				},
				error: function(response){
					alert("Ha ocurrido un error, vuelva a intentarlo");
					location.reload();
				}

			});			
		}

	});

});
