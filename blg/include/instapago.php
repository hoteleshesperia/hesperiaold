<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
--------------------------------------------------------*/
include 'PHPMailer/PHPMailerAutoload.php';
/*include("class.phpmailer.php");
include("class.smtp.php");*/
session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
    header("location:../error.html");
    exit();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
//instapago
$url = 'https://api.instapago.com/payment';
$nombres =  strtoupper(trim($_POST["nombres"]));
$apellidos = strtoupper(trim($_POST["apellidos"]));
$email = strip_tags(strtolower(trim($_POST['email'])));
$email = filter_var($email,FILTER_SANITIZE_EMAIL);
$telefono = strip_tags(trim($_POST["telefono"]));
$pais = strip_tags(trim($_POST["pais"]));
$nombre_tarjeta = strip_tags(strtoupper(trim($_POST["nombre_tarjeta"])));
$NumTarj = strip_tags(trim($_POST["NumTarj"]));
$identificacion = strip_tags(trim($_POST["identificacion"]));
$Expira = strip_tags(trim($_POST["Expira"]));
$MesExpira = strip_tags(trim($_POST["MesExpira"]));
$cvc = strip_tags(trim($_POST["cvc"]));
$ExpirationDate = $MesExpira.'/'.$Expira;
$ip = $_SERVER["REMOTE_ADDR"];
//fin instapago
$ahora = time();
$id_hotel = $_POST["id_hotel"];
$Tprecio = strip_tags(trim($_POST["precio"]));

$fecha_reserva = $ahora;
$tem_id_hab = 1;
$precioparcial = 0;
$adultos = 0;
$ninos  = 0;
$temp_id_hab = explode('|',$tem_id_hab);
$precio_parcial = explode('|',$precioparcial);
$observaciones = strip_tags(trim($_POST["observaciones"]));
$desde = $_POST["name_desde"];
$hasta = $_POST["name_hasta"];
$noches = $_POST["name_noches"];
$count_h = $_POST["count_h"];
$ind_descuento = $_POST["ind_descuento"];
$descuento_cod = $_POST["descuento_cod"];
$cod_promocional = $_POST["cod_promocional"];
$permisos = $_POST["permisos"];
$totalPagar = $_POST["totalPagar"];

$fDesdeSql = str_replace('/', '-', $desde);
$fDesdeSql = date('Y-m-d', strtotime($fDesdeSql));
$fHastaSql = str_replace('/', '-', $hasta);
$fHastaSql = date('Y-m-d', strtotime($fHastaSql));
$arrayAdultos = array();
$arrayNinos = array();
$arrayHab = array();
$arrayIdHab = array();
$arraySub = array();
$formatPrecio = number_format($Tprecio, 2, ',', '.');
$habis = '';
$aduls = '';
$nins = '';
$subs = '';
$voucher = '';
$cantServ = $_POST["cantServ"];
$arrayDescServ = array();
$arraySubtServ = array();

for ($i=0; $i < $count_h; $i++) {
        $arrayAdultos[$i] = $_POST["name_adul".$i];
        $adultos = $adultos + $arrayAdultos[$i];
        if($id_hotel != 4){
            $arrayNinos[$i] = $_POST["name_nin".$i];
            $ninos = $ninos + $arrayNinos[$i];
        }
        
        $arrayHab[$i] = $_POST["name_habi".$i];
        $arrayIdHab[$i] = $_POST["id_habi".$i];
        $arraySub[$i] = $_POST["name_sub".$i];
        $habis = $habis.$arrayHab[$i].'_';
        $aduls = $aduls.$arrayAdultos[$i].'_';
        $nins = $nins.$arrayNinos[$i].'_';
        $subs = $subs.$arraySub[$i].'_';
}

for ($i=0; $i < $cantServ; $i++) {
        $arrayDescServ[$i] = $_POST["descPaq".$i];
        $arraySubtServ[$i] = $_POST["subPaq".$i];
}

$sql = sprintf("SELECT *
                FROM hesperia_settings WHERE id = '%s'",
               mysqli_real_escape_string($mysqli,$id_hotel));
$resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
$rows = mysqli_fetch_array($resultS,MYSQLI_ASSOC);
$razonsocial = $rows["razon_social"];
$contacto = "info@hoteleshesperia.com.ve";
$contRes = $rows["email_reserva"];
$contAdmin = $rows["email_admin"];
$contRec = $rows["email_rec"];
$contPag = $rows["email_pag"];
$contProg = $rows["email_prog"];
$contExtra = $rows["email_extra"];
$contExtra2 = $rows["email_extra2"];
$dominio = $rows["dominio"];
$direccion = $rows["direccion"];
$telefHotel = $rows["telefonos"];
$telefReserva = $rows["telf_reserva"];
$politicas = $rows["politicas"];
$sql = sprintf("SELECT * FROM hesperia_instapago WHERE id_hotel = '%s'",
               mysqli_real_escape_string($mysqli,$id_hotel));
$resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
if ($rows = mysqli_fetch_array($resultS,MYSQLI_ASSOC)){
   $fields = array(
        "KeyID"             => $rows["KeyID"], //requerido
        "PublicKeyId"       => $rows["PublicKeyId"], //requerido
        "Amount"            => $Tprecio, //requerido
        "Description"       => "Reservacion en Hotel $razonsocial", //requerido
        "CardHolder"        => $nombre_tarjeta, //requerido
        "CardHolderId"      => $identificacion, //requerido
        "CardNumber"        => $NumTarj, //requerido
        "CVC"               => $cvc, //requerido
        "ExpirationDate"    => $ExpirationDate, //requerido
        "StatusId"          => "2", //requerido
        "IP"                => $ip //requerido
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url );
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($fields));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);
    $obj = json_decode($server_output);
    $code = $obj->code;
    $msgerror = '';
    switch($code){
        case 400:
            $msgerror= 'Error al validar los datos enviados.';
            break;
        case 401:
            #Error de autenticación, ha ocurrido un error con las llaves utilizadas.
            $msgerror= 'Error no pudimos conectar con la entidad Bancaria.';
            break;
        case 403:
            $msgerror= 'Error La Entidad Bancaria Ha Rechazado su Pago.';
            break;
        case 500:
            $msgerror= 'Error Imposible conectar con la entidad Bancaria.';
            break;
        case 503:
            #Ha Ocurrido un error al procesar los parámetros de entrada. Revise los datos
            #enviados y vuelva a intentarlo.
            $msgerror= 'Error Imposible Procesar Sus Datos, Intente Nuevamente.';
            break;
        case 201:
            $msg_banco  = $obj->message; // Pago procesado
            $voucher  = $obj->voucher;
            $voucher = html_entity_decode($voucher);// HTML que contiene el recibo del pago
            $id_pago  = $obj->id; //Código del pago
            $reference  = $obj->reference; //Código de referencia bancaria
            break;
        default:
            $msgerror='Error inesperado, Imposible determinar. Contacte al Administrador';
            break;
    }
    if(!empty($msgerror)){
        echo'
        <div class="alert alert-danger" role="alert"><p>'.$msgerror.'</p></div>';
        exit();
    } 

    $sqlSec = sprintf("SELECT LPAD(id_secuencia, 9, '0') as secuencia, id_secuencia, next_value FROM hesperia_v2_secuencia_reserva WHERE 1");
    $resultSec = QUERYBD($sqlSec,$hostname,$user,$password,$db_name);
    $rowsSec = mysqli_fetch_array($resultSec,MYSQLI_ASSOC);
    $idsecu = $rowsSec["id_secuencia"];
    $nextval = $rowsSec["next_value"];
    $nuevaSec = $nextval;
    $nex_sec = $nextval;
    $nex_sec++;
    $sec = $rowsSec["secuencia"];
    $codigoR = $id_hotel.'-'.$sec;

    $sql = sprintf("SELECT id FROM hesperia_usuario WHERE email = '%s'",
                   mysqli_real_escape_string($mysqli,$email));
    $resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
    $x = 0;

    for ($indS=0; $indS < $count_h; $indS++) { 
          $dispo = 0;
           
           $inicio = NORMAL_MYSQL2($desde);
            $finalizado = NORMAL_MYSQL2($hasta);
            $id_tipoH = $arrayIdHab[$indS];

            while ($inicio < $finalizado){
              $fecha = date("Y-m-d",$inicio);
              $sqlSel = sprintf("SELECT disponible 
                FROM hesperia_v2_precio_calendario 
                WHERE id_hotel = '%s'
                AND id_tipo_hab = '%s'
                AND fecha = '%s'",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$id_tipoH),
                mysqli_real_escape_string($mysqli,$fecha));
              $resultSel = QUERYBD($sqlSel,$hostname,$user,$password,$db_name);
              
              if($rowsel = mysqli_fetch_array($resultSel,MYSQLI_ASSOC)){
                $dispo = $rowsel["disponible"];
                $dispo--;
                $sqlUp = sprintf("UPDATE hesperia_v2_precio_calendario SET disponible='%s' 
                                  WHERE id_hotel = '%s' 
                                  and id_tipo_hab = '%s' 
                                  and fecha = '%s'",
                       mysqli_real_escape_string($mysqli,$dispo),
                       mysqli_real_escape_string($mysqli,$id_hotel),
                       mysqli_real_escape_string($mysqli,$id_tipoH),
                       mysqli_real_escape_string($mysqli,$fecha));
                $resultUp = QUERYBD($sqlUp,$hostname,$user,$password,$db_name);

              }
              $inicio = $inicio + 86400;
            }
            
        }

        /*Actualizando secuencia*/
        $sqlUp = sprintf("UPDATE hesperia_v2_secuencia_reserva SET id_secuencia='%s',next_value='%s' WHERE 1",
                       mysqli_real_escape_string($mysqli,$nuevaSec),
                       mysqli_real_escape_string($mysqli,$nex_sec));
                $resultUp = QUERYBD($sqlUp,$hostname,$user,$password,$db_name);

        /*Actualizando promocional*/
        if($ind_descuento > 0){
          $permisos--;
          if($permisos == 0){
            $sqlUp = sprintf("UPDATE hesperia_v2_codigos_promocionales SET status='N', permisos=0 WHERE codigo = '%s'",
                       mysqli_real_escape_string($mysqli,$cod_promocional));
          }else{
            $sqlUp = sprintf("UPDATE hesperia_v2_codigos_promocionales SET permisos='%s' WHERE codigo = '%s'",
                       mysqli_real_escape_string($mysqli,$permisos),
                       mysqli_real_escape_string($mysqli,$cod_promocional));
          }
          
          $resultUp = QUERYBD($sqlUp,$hostname,$user,$password,$db_name);
        }
        /**************************/
    
    if($rows = mysqli_fetch_array($resultS,MYSQLI_ASSOC))
    {  // El usuario esta registrado, solo se agrega la reservacion
        $id_usuario = $rows["id"];
        foreach ($temp_id_hab as $key => $valor){
            if(!empty($valor)){
                $id_habitacion = $valor;
                $monto = $precio_parcial[$key];
                $x++;
                 $sql = sprintf("INSERT INTO hesperia_v2_reservaciones(id_hotel, cant_hab, habitaciones, email, adultos, ninos, 
                  total, subtotal, recibido, desde, hasta, codigo_reserva, id, voucher, observaciones) 
                  VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', NULL, '%s', '%s')",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$count_h),
                mysqli_real_escape_string($mysqli,$habis),
                mysqli_real_escape_string($mysqli,$email),
                mysqli_real_escape_string($mysqli,$aduls),
                mysqli_real_escape_string($mysqli,$nins),
                mysqli_real_escape_string($mysqli,$Tprecio),
                mysqli_real_escape_string($mysqli,$subs),
                mysqli_real_escape_string($mysqli,1),
                mysqli_real_escape_string($mysqli,$fDesdeSql),
                mysqli_real_escape_string($mysqli,$fHastaSql),
                mysqli_real_escape_string($mysqli,$codigoR),
                mysqli_real_escape_string($mysqli,$voucher),
                mysqli_real_escape_string($mysqli,$observaciones));
                $resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);

            }
        } //foreach
        /*$desde = date("d/m/Y",$desde);
        $hasta = date("d/m/Y",$hasta);*/
        /*$fDesdeSq = str_replace('/', '-', $desde);
        $fDesdeSq = date('Y-m-d', strtotime($fDesdeSq));
        $fHastaSq = str_replace('/', '-', $hasta);
        $fHastaSq = date('Y-m-d', strtotime($fHastaSq)-86400);*/


$mensaje = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!-- NAME: 1:2:1 COLUMN -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>
        
    <style type="text/css">
    p{
      margin:10px 0;
      padding:0;
    }
    table{
      border-collapse:collapse;
    }
    h1,h2,h3,h4,h5,h6{
      display:block;
      margin:0;
      padding:0;
    }
    img,a img{
      border:0;
      height:auto;
      outline:none;
      text-decoration:none;
    }
    body,#bodyTable,#bodyCell{
      height:100%;
      margin:0;
      padding:0;
      width:100%;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    table{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    p,a,li,td,blockquote{
      mso-line-height-rule:exactly;
    }
    a[href^=tel],a[href^=sms]{
      color:inherit;
      cursor:default;
      text-decoration:none;
    }
    p,a,li,td,body,table,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
      line-height:100%;
    }
    a[x-apple-data-detectors]{
      color:inherit !important;
      text-decoration:none !important;
      font-size:inherit !important;
      font-family:inherit !important;
      font-weight:inherit !important;
      line-height:inherit !important;
    }
    #bodyCell{
      padding:10px;
    }
    .templateContainer{
      max-width:600px !important;
    }
    a.mcnButton{
      display:block;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent{
      word-break:break-word;
    }
    .mcnTextContent img{
      height:auto !important;
    }
    .mcnDividerBlock{
      table-layout:fixed !important;
    }
    body,#bodyTable{
      /*@editable*/background-color:#FAFAFA;
    }
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*
  @tab Page
  @section Email Border
  @tip Set the border for your email.
  */
    .templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section Heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:22px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:20px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
    #templatePreheader{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0px;
      /*@editable*/padding-bottom:0;
    }
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateUpperBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
  
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateColumns{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateLowerBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:2px solid #EAEAEA;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:9px;
    }
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateFooter{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:center;
    }
    #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (min-width:768px){
    .templateContainer{
      width:600px !important;
    }

} @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    #bodyCell{
      padding-top:10px !important;
    }

} @media only screen and (max-width: 480px){
    .columnWrapper{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImage{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnBoxedTextContentContainer{
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupContent{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardBottomImageContent{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockInner{
      padding-top:0 !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockOuter{
      padding-top:9px !important;
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnTextContent,.mcnBoxedTextContentColumn{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
      padding-right:18px !important;
      padding-bottom:0 !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcpreview-image-uploader{
      display:none !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:22px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
    #templatePreheader{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Upper Body Text
  @tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Column Text
  @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Lower Body Text
  @tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Footer Text
  @tip Make the footer content text larger in size for better readability on small screens.
  */
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

}</style></head>
    <body>
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
            <tr>
            <td align="center" valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
              <tr>
                <td valign="top" id="templatePreheader"></td>
              </tr>
              <tr>
                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                
<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">
        
            
            <a href="https://hoteleshesperia.com.ve" title="Ir a nuestra pagina web" class="" target="_blank">
            

            <img alt="" src="https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png" width="100%" style="width: 100%;" class="mcnImage">
            </a>
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding: 9px 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;" width="546">
            <strong><span style="font-family:verdana,geneva,sans-serif">Sr(a) '.$nombres.' '.$apellidos.'<br>
¡Su reserva está confirmada!</span></strong>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateUpperBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Número de reservación '.$codigoR.'</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Hotel:</strong> '.$razonsocial.'<br>
<strong>Check In:</strong> '.$desde.'<br>
<strong>Check Out:</strong> '.$hasta.'<br>
<strong>Dirección:</strong> '.$direccion.'</span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Habitaciones reservadas</span></span></h1>
                            <span style="font-family:verdana,geneva,sans-serif">
  <span style="font-size:13px">';

for ($k=0; $k < $count_h; $k++) { 
              $tipoH = $arrayHab[$k];
              $subt = $arraySub[$k];
              $h = $k+1;
              $mensaje = $mensaje.'<strong>Habitación '.$h.':</strong>&nbsp;'.$tipoH.'<br>';
              $hAdul = $arrayAdultos[$k];
              if($id_hotel != 4){
                $hNin = $arrayNinos[$k];
                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.' , <strong>Niños</strong> '.$hNin.' </br>';
              }else{
                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.'</br>';
              } 
              $mensaje = $mensaje.'<strong>Subtotal: </strong>Bs. '.$subt.' </br>';
              $mensaje = $mensaje.'<br>';
            }
            if($cantServ > 0){
              $mensaje = $mensaje.'
              <br><strong>Servicios Adicionales</strong><br>';
                      for ($i=0; $i < $cantServ ; $i++) { 
                          
                                  $mensaje = $mensaje.$arrayDescServ[$i].'</br>
                                  <strong>Sub-Total Bs: '.$arraySubtServ[$i].'</strong></br>';
                      }
            }
            if($ind_descuento > 0 && $descuento_cod > 0){
              $mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total antes: Bs <strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
              <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
              Con un '.substr($descuento_cod, 2, 2).'% de descuento aplicado en habitaciones<br><br>';
            }
$mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total de la reservación: '.$formatPrecio.'</span></strong></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateColumns">
                  <!--[if gte mso 9]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                  <tr>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comentarios del Cliente</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><em><span style="font-size:13px">'.$observaciones.'</span></em></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comprobante de pago</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><em>'.$voucher.'</em></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateLowerBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <div style="text-align: justify;"><strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Información de contacto: </span></span></strong><br>
<span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Reservas</strong> '.$telefReserva.' - <strong>Central&nbsp;Hotel</strong> '.$telefHotel.'.</span></span></span><br>
<br>
<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">Para ver esta información más detallada puede ingresar a: https://'.$dominio.' e ingresar como usuario con su cuenta email ['.$email.'].&nbsp;</span><span style="text-align:justify"><font face="verdana, geneva, sans-serif"><span style="font-size:12px; line-height:1.6em">Cualquier duda&nbsp;</span><span style="font-size:12px; line-height:19.2px">contáctenos</span><span style="font-size:12px; line-height:1.6em">&nbsp;al siguiente&nbsp;email: '.$contacto.'.</span></font></span><br>
<br>
<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">'.$politicas.'</span><br>
&nbsp;</div>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.facebook.com/HesperiaVenezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://instagram.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://twitter.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.linkedin.com/company/hoteles-hesperia-venezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
            </table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
            <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
       
        

    }
    else
    {
        //    no existe, agregar usuario y reservacion
        $sql = sprintf("INSERT INTO hesperia_usuario (id,email,clave_acceso,nombres,apellidos,telefono,
                empresa,numero_empresa,email_empresa,
                fecha_registro,ultimo_login,nivel,valido,id_hotel)
                VALUES ( NULL,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                       mysqli_real_escape_string($mysqli,$email),
                       mysqli_real_escape_string($mysqli,md5($ahora)),
                       mysqli_real_escape_string($mysqli,$nombres),
                       mysqli_real_escape_string($mysqli,$apellidos),
                       mysqli_real_escape_string($mysqli,$telefono),
                       mysqli_real_escape_string($mysqli,""),
                       mysqli_real_escape_string($mysqli,""),
                       mysqli_real_escape_string($mysqli,""),
                       mysqli_real_escape_string($mysqli,$ahora),
                       mysqli_real_escape_string($mysqli,$ahora),
                       mysqli_real_escape_string($mysqli,3),
                       mysqli_real_escape_string($mysqli,1),
                       mysqli_real_escape_string($mysqli,$id_hotel));
        $result = QUERYBD($sql,$hostname,$user,$password,$db_name);


        $mensajeRegistro = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!-- NAME: 1:2:1 COLUMN -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>
        
    <style type="text/css">
    p{
      margin:10px 0;
      padding:0;
    }
    table{
      border-collapse:collapse;
    }
    h1,h2,h3,h4,h5,h6{
      display:block;
      margin:0;
      padding:0;
    }
    img,a img{
      border:0;
      height:auto;
      outline:none;
      text-decoration:none;
    }
    body,#bodyTable,#bodyCell{
      height:100%;
      margin:0;
      padding:0;
      width:100%;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    table{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    p,a,li,td,blockquote{
      mso-line-height-rule:exactly;
    }
    a[href^=tel],a[href^=sms]{
      color:inherit;
      cursor:default;
      text-decoration:none;
    }
    p,a,li,td,body,table,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
      line-height:100%;
    }
    a[x-apple-data-detectors]{
      color:inherit !important;
      text-decoration:none !important;
      font-size:inherit !important;
      font-family:inherit !important;
      font-weight:inherit !important;
      line-height:inherit !important;
    }
    #bodyCell{
      padding:10px;
    }
    .templateContainer{
      max-width:600px !important;
    }
    a.mcnButton{
      display:block;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent{
      word-break:break-word;
    }
    .mcnTextContent img{
      height:auto !important;
    }
    .mcnDividerBlock{
      table-layout:fixed !important;
    }
  
    body,#bodyTable{
      /*@editable*/background-color:#FAFAFA;
    }
  
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*
  @tab Page
  @section Email Border
  @tip Set the border for your email.
  */
    .templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section Heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:22px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:20px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  
    #templatePreheader{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
  
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
 
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0px;
      /*@editable*/padding-bottom:0;
    }
  
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  
    #templateUpperBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }

    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  
    #templateColumns{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
  
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  
    #templateLowerBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:2px solid #EAEAEA;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:9px;
    }
  
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  
    #templateFooter{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
  
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:center;
    }
  
    #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (min-width:768px){
    .templateContainer{
      width:600px !important;
    }

} @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    #bodyCell{
      padding-top:10px !important;
    }

} @media only screen and (max-width: 480px){
    .columnWrapper{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImage{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnBoxedTextContentContainer{
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupContent{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardBottomImageContent{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockInner{
      padding-top:0 !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockOuter{
      padding-top:9px !important;
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnTextContent,.mcnBoxedTextContentColumn{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
      padding-right:18px !important;
      padding-bottom:0 !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcpreview-image-uploader{
      display:none !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:22px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  
    #templatePreheader{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Upper Body Text
  @tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Column Text
  @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Lower Body Text
  @tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Footer Text
  @tip Make the footer content text larger in size for better readability on small screens.
  */
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

}</style></head>
    <body>
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
            <tr>
            <td align="center" valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
              <tr>
                <td valign="top" id="templatePreheader"></td>
              </tr>
              <tr>
                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                
<table align="right" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">
        
            
            <a href="https://www.hoteleshesperia.com.ve" title="Ir a nuestra pagina web" class="" target="_blank">
            

            <img alt="" src="https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png" width="564" style="max-width:1169px;" class="mcnImage">
            </a>
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding: 9px 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;" width="546">
            <strong><span style="font-family:verdana,geneva,sans-serif">Sr(a) '.$nombres.' '.$apellidos.'<br>
¡Usted ha sido registrado!</span></strong>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateUpperBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><font face="verdana, geneva, sans-serif"><span style="font-size:14px; line-height:16.8px">Datos de accesso</span></font></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Usuario:</strong> '.$email.'<br>
<strong>Contraseña provicional:</strong>&nbsp;'.$ahora.'</span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1 style="text-align: justify;"><strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Validar cuenta&nbsp;</span></span></strong><br>
<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">Para validar su cuenta usted debe&nbsp;ingresar a: https://www.hoteleshesperia.com.ve y en el menu superior, en la sección "Mi Cuenta" iniciar sesión con el usuario y contraseña indicados. Una vez que pueda acceder le recomendamos cambiar su contraseña</span><span style="text-align:justify"><font face="verdana, geneva, sans-serif"><span style="font-size:12px; line-height:1.6em">.&nbsp;</span></font></span><font face="verdana, geneva, sans-serif"><span style="font-size:12px; line-height:19.2px">Ante cualquier duda, escríbanos a info@hoteleshesperia.com.ve</span></font></h1>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1 style="text-align: justify;"><span style="font-size:12px"><span style="font-family:verdana,geneva,sans-serif; line-height:1.6em; text-align:justify">Este mensaje es privado, confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.</span></span></h1>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateColumns">
                  <!--[if gte mso 9]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                  <tr>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateLowerBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.facebook.com/HesperiaVenezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://instagram.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://twitter.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.linkedin.com/company/hoteles-hesperia-venezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
            </table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
            <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';

$mail_object = new PHPMailer();
            $mail_object->IsSMTP(); // Indicamos que use SMTP
            //$mail_object->SMTPDebug = 3;   //activar modo debug                                   
            $mail_object->Host = 'a2plcpnl0042.prod.iad2.secureserver.net';  // Indicamos los servidores SMTP
            $mail_object->SMTPAuth = true;                               // Habilitamos la autenticación SMTP
            $mail_object->Username = 'reserva@hoteleshesperia.com.ve';                 // SMTP username
            $mail_object->Password = 'Nh123456nh';                           // SMTP password
            $mail_object->Port = 465;                                    // TCP port
            $mail_object->SMTPSecure = "ssl"; // Establece el tipo de seguridad SMTP a SSL.
            $mail_object->Timeout =30;
            //$mail_object->SMTPDebug = 1;
                           
            /** Configurar cabeceras del mensaje **/
            $mail_object->From = $contacto;                      // Correo del remitente
            $mail_object->FromName = utf8_decode('Información Hesperia Hotels & Resorts');           // Nombre del remitente
            $mail_object->Subject = 'Registro de Usuario';                // Asunto
             
            /** Incluir destinatarios. El nombre es opcional **/
            $mail_object->addAddress($email);           
            /** Enviarlo en formato HTML **/
            $mail_object->isHTML(true);                                  
             
            /** Configurar cuerpo del mensaje **/
            $mail_object->Body    = utf8_decode($mensajeRegistro);
            $mail_object->AltBody = $mensajeRegistro;                   
            
            
            /** Enviar mensaje... **/           
            $mail_object->send();

        $id_usuario =mysqli_insert_id($mysqli);
        // agregar reservacion
        $x = 0;
        foreach ($temp_id_hab as $key => $valor){
            $id_habitacion = $valor;
            $monto = $precio_parcial[$key];
            $x++;
            
            $sql = sprintf("INSERT INTO hesperia_v2_reservaciones(id_hotel, cant_hab, habitaciones, email, adultos, ninos, 
                  total, subtotal, recibido, desde, hasta, codigo_reserva, id, voucher, observaciones) 
                  VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', NULL, '%s', '%s')",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$count_h),
                mysqli_real_escape_string($mysqli,$habis),
                mysqli_real_escape_string($mysqli,$email),
                mysqli_real_escape_string($mysqli,$aduls),
                mysqli_real_escape_string($mysqli,$nins),
                mysqli_real_escape_string($mysqli,$Tprecio),
                mysqli_real_escape_string($mysqli,$subs),
                mysqli_real_escape_string($mysqli,1),
                mysqli_real_escape_string($mysqli,$fDesdeSql),
                mysqli_real_escape_string($mysqli,$fHastaSql),
                mysqli_real_escape_string($mysqli,$codigoR),
                mysqli_real_escape_string($mysqli,$voucher),
                mysqli_real_escape_string($mysqli,$observaciones));
                $resultS = QUERYBD($sql,$hostname,$user,$password,$db_name);
        } //foreach
        
$mensaje = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!-- NAME: 1:2:1 COLUMN -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>
        
    <style type="text/css">
    p{
      margin:10px 0;
      padding:0;
    }
    table{
      border-collapse:collapse;
    }
    h1,h2,h3,h4,h5,h6{
      display:block;
      margin:0;
      padding:0;
    }
    img,a img{
      border:0;
      height:auto;
      outline:none;
      text-decoration:none;
    }
    body,#bodyTable,#bodyCell{
      height:100%;
      margin:0;
      padding:0;
      width:100%;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    table{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    p,a,li,td,blockquote{
      mso-line-height-rule:exactly;
    }
    a[href^=tel],a[href^=sms]{
      color:inherit;
      cursor:default;
      text-decoration:none;
    }
    p,a,li,td,body,table,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
      line-height:100%;
    }
    a[x-apple-data-detectors]{
      color:inherit !important;
      text-decoration:none !important;
      font-size:inherit !important;
      font-family:inherit !important;
      font-weight:inherit !important;
      line-height:inherit !important;
    }
    #bodyCell{
      padding:10px;
    }
    .templateContainer{
      max-width:600px !important;
    }
    a.mcnButton{
      display:block;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent{
      word-break:break-word;
    }
    .mcnTextContent img{
      height:auto !important;
    }
    .mcnDividerBlock{
      table-layout:fixed !important;
    }
    body,#bodyTable{
      /*@editable*/background-color:#FAFAFA;
    }
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*men
  @tab Page
  @section Email Border
  @tip Set the border for your email.
  */
    .templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section Heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:22px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:20px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
    #templatePreheader{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0px;
      /*@editable*/padding-bottom:0;
    }
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateUpperBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
  
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateColumns{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateLowerBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:2px solid #EAEAEA;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:9px;
    }
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateFooter{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:center;
    }
    #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (min-width:768px){
    .templateContainer{
      width:600px !important;
    }

} @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    #bodyCell{
      padding-top:10px !important;
    }

} @media only screen and (max-width: 480px){
    .columnWrapper{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImage{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnBoxedTextContentContainer{
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupContent{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardBottomImageContent{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockInner{
      padding-top:0 !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockOuter{
      padding-top:9px !important;
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnTextContent,.mcnBoxedTextContentColumn{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
      padding-right:18px !important;
      padding-bottom:0 !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcpreview-image-uploader{
      display:none !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:22px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
    #templatePreheader{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Upper Body Text
  @tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Column Text
  @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Lower Body Text
  @tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Footer Text
  @tip Make the footer content text larger in size for better readability on small screens.
  */
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

}</style></head>
    <body>
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
            <tr>
            <td align="center" valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
              <tr>
                <td valign="top" id="templatePreheader"></td>
              </tr>
              <tr>
                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                
<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">
        
            
            <a href="https://hoteleshesperia.com.ve" title="Ir a nuestra pagina web" class="" target="_blank">
            

            <img alt="" src="https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png" width="100%" style="width: 100%;" class="mcnImage">
            </a>
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding: 9px 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;" width="546">
            <strong><span style="font-family:verdana,geneva,sans-serif">Sr(a) '.$nombres.' '.$apellidos.'<br>
¡Su reserva está confirmada!</span></strong>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateUpperBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Número de reservación '.$codigoR.'</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Hotel:</strong> '.$razonsocial.'<br>
<strong>Check In:</strong> '.$desde.'<br>
<strong>Check Out:</strong> '.$hasta.'<br>
<strong>Dirección:</strong> '.$direccion.'</span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Habitaciones reservadas</span></span></h1>
                            <span style="font-family:verdana,geneva,sans-serif">
  <span style="font-size:13px">';

for ($k=0; $k < $count_h; $k++) { 
              $tipoH = $arrayHab[$k];
              $subt = $arraySub[$k];
              $h = $k+1;
              $mensaje = $mensaje.'<strong>Habitación '.$h.':</strong>&nbsp;'.$tipoH.'<br>';
              $hAdul = $arrayAdultos[$k];
              if($id_hotel != 4){
                $hNin = $arrayNinos[$k];
                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.' , <strong>Niños</strong> '.$hNin.' </br>';
              }else{
                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.' </br>';
              } 
              $mensaje = $mensaje.'<strong>Subtotal: </strong>Bs. '.$subt.' </br>';
              $mensaje = $mensaje.'<br>';
            }
            if($cantServ > 0){
                $mensaje = $mensaje.'
                <br><strong>Servicios Adicionales</strong><br>';
                    for ($i=0; $i < $cantServ ; $i++) { 
                        
                                $mensaje = $mensaje.$arrayDescServ[$i].'</br>
                                <strong>Sub-Total Bs: '.$arraySubtServ[$i].'</strong></br>';
                    }
            }
            if($ind_descuento > 0 && $descuento_cod > 0){
              $mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total antes: Bs <strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
              <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
              Con un '.substr($descuento_cod, 2, 2).'% de descuento aplicado en habitaciones<br><br>';
            }
        $mensaje = $mensaje.'
<strong><span style="font-size:14px">Precio total de la reservación: '.$formatPrecio.'</span></strong></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateColumns">
                  <!--[if gte mso 9]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                  <tr>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comentarios del Cliente</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><em><span style="font-size:13px">'.$observaciones.'</span></em></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comprobante de pago</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><em>'.$voucher.'</em></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateLowerBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <div style="text-align: justify;"><strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Información de contacto: </span></span></strong><br>
<span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Reservas</strong> '.$telefReserva.' - <strong>Central&nbsp;Hotel</strong> '.$telefHotel.'.</span></span></span><br>
<br>
<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">Para ver esta información más detallada puede ingresar a: https://'.$dominio.' e ingresar como usuario con su cuenta email ['.$email.'].&nbsp;</span><span style="text-align:justify"><font face="verdana, geneva, sans-serif"><span style="font-size:12px; line-height:1.6em">Cualquier duda&nbsp;</span><span style="font-size:12px; line-height:19.2px">contáctenos</span><span style="font-size:12px; line-height:1.6em">&nbsp;al siguiente&nbsp;email: '.$contacto.'.</span></font></span><br>
<br>
<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">'.$politicas.'</span><br>
&nbsp;</div>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.facebook.com/HesperiaVenezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://instagram.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://twitter.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.linkedin.com/company/hoteles-hesperia-venezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
            </table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
            <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
       
    }
    if (mysqli_affected_rows($mysqli)){
        $asunto = "Reservación en $razonsocial";
        // se envia al usuario
        $headers = '';
        $headers .= "MIME-Version:1.0\r\n";
        $headers .= "Content-type:text/html; charset=UTF-8\r\n";
        $headers .= "Received:from www.$dominio\r\n";
        $headers .= "X-Priority:3\r\n";
        $headers .= "X-MSMail-Priority:Normal\r\n";
        $headers .= "From: Reservas Hesperia Hotels & Resorts <$contacto>\r\n";
        $headers .= "X-Mailer:$contacto\r\n";
        $headers .= "Return-path:$contacto\r\n";
        $headers .= "Reply-To:$contacto\r\n";
        $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";

        $mensajeHuesped = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!-- NAME: 1:2:1 COLUMN -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>
        
    <style type="text/css">
    p{
      margin:10px 0;
      padding:0;
    }
    table{
      border-collapse:collapse;
    }
    h1,h2,h3,h4,h5,h6{
      display:block;
      margin:0;
      padding:0;
    }
    img,a img{
      border:0;
      height:auto;
      outline:none;
      text-decoration:none;
    }
    body,#bodyTable,#bodyCell{
      height:100%;
      margin:0;
      padding:0;
      width:100%;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    table{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    p,a,li,td,blockquote{
      mso-line-height-rule:exactly;
    }
    a[href^=tel],a[href^=sms]{
      color:inherit;
      cursor:default;
      text-decoration:none;
    }
    p,a,li,td,body,table,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
      line-height:100%;
    }
    a[x-apple-data-detectors]{
      color:inherit !important;
      text-decoration:none !important;
      font-size:inherit !important;
      font-family:inherit !important;
      font-weight:inherit !important;
      line-height:inherit !important;
    }
    #bodyCell{
      padding:10px;
    }
    .templateContainer{
      max-width:600px !important;
    }
    a.mcnButton{
      display:block;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent{
      word-break:break-word;
    }
    .mcnTextContent img{
      height:auto !important;
    }
    .mcnDividerBlock{
      table-layout:fixed !important;
    }
    body,#bodyTable{
      /*@editable*/background-color:white;
    }
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*
  @tab Page
  @section Email Border
  @tip Set the border for your email.
  */
    .templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section Heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:22px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:20px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
    #templatePreheader{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0px;
      /*@editable*/padding-bottom:0;
    }
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateUpperBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
  
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateColumns{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateLowerBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:2px solid #EAEAEA;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:9px;
    }
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateFooter{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:center;
    }
    #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (min-width:768px){
    .templateContainer{
      width:600px !important;
    }

} @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    #bodyCell{
      padding-top:10px !important;
    }

} @media only screen and (max-width: 480px){
    .columnWrapper{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImage{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnBoxedTextContentContainer{
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupContent{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardBottomImageContent{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockInner{
      padding-top:0 !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockOuter{
      padding-top:9px !important;
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnTextContent,.mcnBoxedTextContentColumn{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
      padding-right:18px !important;
      padding-bottom:0 !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcpreview-image-uploader{
      display:none !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:22px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
    #templatePreheader{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Upper Body Text
  @tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Column Text
  @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Lower Body Text
  @tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Footer Text
  @tip Make the footer content text larger in size for better readability on small screens.
  */
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

}</style></head>
    <body style="background-color: white;">
      <div align="right"> <button class="btn btn-primary" onclick="imprimir();">Imprimir</button></div>
      <div id="imprimeme">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
            <tr>
            <td align="center" valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
              <tr>
                <td valign="top" id="templatePreheader"></td>
              </tr>
              <tr>
                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                
<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">
        
            
            <a href="https://hoteleshesperia.com.ve" title="Ir a nuestra pagina web" class="" target="_blank">
            

            <img alt="" src="https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png" width="100%" style="width: 100%;" class="mcnImage">
            </a>
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding: 9px 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;" width="546">
            <strong><span style="font-family:verdana,geneva,sans-serif">Sr(a) '.$nombres.' '.$apellidos.'<br>
¡Su reserva está confirmada!</span></strong>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateColumns">
                  <!--[if gte mso 9]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                  <tr>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Número de reservación<br> '.$codigoR.'</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Hotel:</strong> '.$razonsocial.'<br>
<strong>Check In:</strong> '.$desde.'<br>
<strong>Check Out:</strong> '.$hasta.'<br>
<strong>Dirección:</strong> '.$direccion.'</span></span>
                        </td>
                    </tr>

                    <tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comentarios del Cliente</span></span></h1>
                            <span style="font-family:verdana,geneva,sans-serif"><em><span style="font-size:13px">'.$observaciones.'</span></em></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comprobante de pago</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><em><div id="voucher">'.$voucher.'</div></em></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Habitaciones reservadas</span></span></h1>
                            <span style="font-family:verdana,geneva,sans-serif">
  <span style="font-size:13px">';

for ($k=0; $k < $count_h; $k++) { 
              $tipoH = $arrayHab[$k];
              $subt = $arraySub[$k];
              $h = $k+1;
              $mensajeHuesped = $mensajeHuesped.'<strong>Habitación '.$h.':</strong>&nbsp;'.$tipoH.'<br>';
              $hAdul = $arrayAdultos[$k];
              if($id_hotel != 4){
                $hNin = $arrayNinos[$k];
                $mensajeHuesped = $mensajeHuesped.'<strong>Adultos</strong> '.$hAdul.' , <strong>Niños</strong> '.$hNin.' </br>';
              }else{
                $mensajeHuesped = $mensajeHuesped.'<strong>Adultos</strong> '.$hAdul.' </br>';
              } 
              $mensajeHuesped = $mensajeHuesped.'<strong>Subtotal: </strong>Bs. '.$subt.' </br>';
              $mensajeHuesped = $mensajeHuesped.'<br>';
            }
            if($cantServ > 0){
            $mensajeHuesped = $mensajeHuesped.'
            <br><strong>Servicios Adicionales</strong><br>';
                    for ($i=0; $i < $cantServ ; $i++) { 
                        
                                $mensajeHuesped = $mensajeHuesped.$arrayDescServ[$i].'</br>
                                <strong>Sub-Total Bs: '.$arraySubtServ[$i].'</strong></br>';
                    }
            }
            if($ind_descuento > 0 && $descuento_cod > 0){
              $mensajeHuesped = $mensajeHuesped.'<strong><span style="font-size:14px">Precio total antes: Bs <strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
              <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
              Con un '.substr($descuento_cod, 2, 2).'% de descuento aplicado en habitaciones<br><br>';
            }

        $mensajeHuesped = $mensajeHuesped.'
<strong><span style="font-size:14px">Precio total de la reservación: '.$formatPrecio.'</span></strong></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
            </table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
            <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
        </div>
    </body>
</html>';


 $mail_object = new PHPMailer();
            $mail_object->IsSMTP(); // Indicamos que use SMTP
            //$mail_object->SMTPDebug = 3;   //activar modo debug                                   
            $mail_object->Host = 'a2plcpnl0042.prod.iad2.secureserver.net';  // Indicamos los servidores SMTP
            $mail_object->SMTPAuth = true;                               // Habilitamos la autenticación SMTP
            $mail_object->Username = 'reserva@hoteleshesperia.com.ve';                 // SMTP username
            $mail_object->Password = 'Nh123456nh';                           // SMTP password
            $mail_object->Port = 465;                                    // TCP port
            $mail_object->SMTPSecure = "ssl"; // Establece el tipo de seguridad SMTP a SSL.
            $mail_object->Timeout =30;
            //$mail_object->SMTPDebug = 1;
                           
            /** Configurar cabeceras del mensaje **/
            $mail_object->From = $contacto;                       // Correo del remitente
            $mail_object->FromName = 'Reservas Hesperia Hotels & Resorts';           // Nombre del remitente
            $mail_object->Subject = 'Reserva registrada en '.$razonsocial;                // Asunto
             
            /** Incluir destinatarios. El nombre es opcional **/
            $mail_object->addAddress($email);
            /** Enviarlo en formato HTML **/
            $mail_object->isHTML(true);                                  
             
            /** Configurar cuerpo del mensaje **/
            $mail_object->Body    = utf8_decode($mensaje);
            $mail_object->AltBody = $mensaje;           
            
            
            /** Enviar mensaje... **/           
            if(!$mail_object->send()) {
                $errors = 'Ocurrio un error al registrar el pago. ';
                $errors .= 'Mailer Error: ' . $mail_object->ErrorInfo;
                echo $errors;
            }
        /** Configurar SMTP **/
            

        // Se envia al hotel
        $headers = '';
        $headers .= "MIME-Version:1.0\r\n";
        $headers .= "Content-type:text/html; charset=UTF-8\r\n";
        $headers .= "Received:from www.$dominio\r\n";
        $headers .= "X-Priority:3\r\n";
        $headers .= "X-MSMail-Priority:Normal\r\n";
        $headers .= "From: Reservas Hesperia Hotels & Resorts<$contacto>\r\n";
        $headers .= "X-Mailer:$contacto\r\n";
        $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
        $headers .= "Reply-To:$email\r\n";
        $headers .= "Return-path:$email\r\n";

        
$mensaje = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <!-- NAME: 1:2:1 COLUMN -->
    <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
    <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>
        
    <style type="text/css">
    p{
      margin:10px 0;
      padding:0;
    }
    table{
      border-collapse:collapse;
    }
    h1,h2,h3,h4,h5,h6{
      display:block;
      margin:0;
      padding:0;
    }
    img,a img{
      border:0;
      height:auto;
      outline:none;
      text-decoration:none;
    }
    body,#bodyTable,#bodyCell{
      height:100%;
      margin:0;
      padding:0;
      width:100%;
    }
    #outlook a{
      padding:0;
    }
    img{
      -ms-interpolation-mode:bicubic;
    }
    table{
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    .ReadMsgBody{
      width:100%;
    }
    .ExternalClass{
      width:100%;
    }
    p,a,li,td,blockquote{
      mso-line-height-rule:exactly;
    }
    a[href^=tel],a[href^=sms]{
      color:inherit;
      cursor:default;
      text-decoration:none;
    }
    p,a,li,td,body,table,blockquote{
      -ms-text-size-adjust:100%;
      -webkit-text-size-adjust:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
      line-height:100%;
    }
    a[x-apple-data-detectors]{
      color:inherit !important;
      text-decoration:none !important;
      font-size:inherit !important;
      font-family:inherit !important;
      font-weight:inherit !important;
      line-height:inherit !important;
    }
    #bodyCell{
      padding:10px;
    }
    .templateContainer{
      max-width:600px !important;
    }
    a.mcnButton{
      display:block;
    }
    .mcnImage{
      vertical-align:bottom;
    }
    .mcnTextContent{
      word-break:break-word;
    }
    .mcnTextContent img{
      height:auto !important;
    }
    .mcnDividerBlock{
      table-layout:fixed !important;
    }
    body,#bodyTable{
      /*@editable*/background-color:#FAFAFA;
    }
    #bodyCell{
      /*@editable*/border-top:0;
    }
  /*
  @tab Page
  @section Email Border
  @tip Set the border for your email.
  */
    .templateContainer{
      /*@editable*/border:0;
    }
  /*
  @tab Page
  @section Heading 1
  @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
  @style heading 1
  */
    h1{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:26px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 2
  @tip Set the styling for all second-level headings in your emails.
  @style heading 2
  */
    h2{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:22px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 3
  @tip Set the styling for all third-level headings in your emails.
  @style heading 3
  */
    h3{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:20px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
  /*
  @tab Page
  @section Heading 4
  @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
  @style heading 4
  */
    h4{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:18px;
      /*@editable*/font-style:normal;
      /*@editable*/font-weight:bold;
      /*@editable*/line-height:125%;
      /*@editable*/letter-spacing:normal;
      /*@editable*/text-align:left;
    }
    #templatePreheader{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateHeader{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0px;
      /*@editable*/padding-bottom:0;
    }
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateUpperBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
  
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
  
    #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateColumns{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:0;
    }
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateLowerBody{
      /*@editable*/background-color:#FFFFFF;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:2px solid #EAEAEA;
      /*@editable*/padding-top:0;
      /*@editable*/padding-bottom:9px;
    }
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/color:#202020;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:16px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:left;
    }
    #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
      /*@editable*/color:#2BAADF;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
    #templateFooter{
      /*@editable*/background-color:#FAFAFA;
      /*@editable*/border-top:0;
      /*@editable*/border-bottom:0;
      /*@editable*/padding-top:9px;
      /*@editable*/padding-bottom:9px;
    }
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/color:#656565;
      /*@editable*/font-family:Helvetica;
      /*@editable*/font-size:12px;
      /*@editable*/line-height:150%;
      /*@editable*/text-align:center;
    }
    #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
      /*@editable*/color:#656565;
      /*@editable*/font-weight:normal;
      /*@editable*/text-decoration:underline;
    }
  @media only screen and (min-width:768px){
    .templateContainer{
      width:600px !important;
    }

} @media only screen and (max-width: 480px){
    body,table,td,p,a,li,blockquote{
      -webkit-text-size-adjust:none !important;
    }

} @media only screen and (max-width: 480px){
    body{
      width:100% !important;
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    #bodyCell{
      padding-top:10px !important;
    }

} @media only screen and (max-width: 480px){
    .columnWrapper{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImage{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
      max-width:100% !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnBoxedTextContentContainer{
      min-width:100% !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupContent{
      padding:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
      padding-top:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
      padding-top:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardBottomImageContent{
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockInner{
      padding-top:0 !important;
      padding-bottom:0 !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageGroupBlockOuter{
      padding-top:9px !important;
      padding-bottom:9px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnTextContent,.mcnBoxedTextContentColumn{
      padding-right:18px !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
      padding-right:18px !important;
      padding-bottom:0 !important;
      padding-left:18px !important;
    }

} @media only screen and (max-width: 480px){
    .mcpreview-image-uploader{
      display:none !important;
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 1
  @tip Make the first-level headings larger in size for better readability on small screens.
  */
    h1{
      /*@editable*/font-size:22px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 2
  @tip Make the second-level headings larger in size for better readability on small screens.
  */
    h2{
      /*@editable*/font-size:20px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 3
  @tip Make the third-level headings larger in size for better readability on small screens.
  */
    h3{
      /*@editable*/font-size:18px !important;
      /*@editable*/line-height:125% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Heading 4
  @tip Make the fourth-level headings larger in size for better readability on small screens.
  */
    h4{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Boxed Text
  @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
    #templatePreheader{
      /*@editable*/display:block !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Preheader Text
  @tip Make the preheader text larger in size for better readability on small screens.
  */
    #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Header Text
  @tip Make the header text larger in size for better readability on small screens.
  */
    #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Upper Body Text
  @tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Column Text
  @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Lower Body Text
  @tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
  */
    #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
      /*@editable*/font-size:16px !important;
      /*@editable*/line-height:150% !important;
    }

} @media only screen and (max-width: 480px){
  /*
  @tab Mobile Styles
  @section Footer Text
  @tip Make the footer content text larger in size for better readability on small screens.
  */
    #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
      /*@editable*/font-size:14px !important;
      /*@editable*/line-height:150% !important;
    }

}</style></head>
    <body>
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
            <tr>
            <td align="center" valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
              <tr>
                <td valign="top" id="templatePreheader"></td>
              </tr>
              <tr>
                <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageCardBlock">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                
<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnImageCardBottomContent" width="100%">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" align="left" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">
        
            
            <a href="https://hoteleshesperia.com.ve" title="Ir a nuestra pagina web" class="" target="_blank">
            

            <img alt="" src="https://gallery.mailchimp.com/0d2d917a9b763d261dffacbcd/images/5038414b-4eed-4704-bee8-0137da1e2e83.png" width="100%" style="width: 100%;" class="mcnImage">
            </a>
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding: 9px 18px; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center;" width="546">
            <strong><span style="font-family:verdana,geneva,sans-serif">¡Se ha registrado una reserva!<br>
A nombre de Sr(a) '.$nombres.' '.$apellidos.'</span></strong>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 2px 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #918F8F;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateUpperBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Número de reservación '.$codigoR.'</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><strong>Hotel:</strong> '.$razonsocial.'<br>
<strong>Check In:</strong> '.$desde.'<br>
<strong>Check Out:</strong> '.$hasta.'<br>
<strong>Dirección:</strong> '.$direccion.'</span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Habitaciones reservadas</span></span></h1>
                            <span style="font-family:verdana,geneva,sans-serif">
  <span style="font-size:13px">';

for ($k=0; $k < $count_h; $k++) { 
              $tipoH = $arrayHab[$k];
              $subt = $arraySub[$k];
              $h = $k+1;
              $mensaje = $mensaje.'<strong>Habitación '.$h.':</strong>&nbsp;'.$tipoH.'<br>';
              $hAdul = $arrayAdultos[$k];
              if($id_hotel != 4){
                $hNin = $arrayNinos[$k];
                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.' , <strong>Niños</strong> '.$hNin.' </br>';
              }else{
                $mensaje = $mensaje.'<strong>Adultos</strong> '.$hAdul.' </br>';
              } 
              $mensaje = $mensaje.'<strong>Subtotal: </strong>Bs. '.$subt.' </br>';
              $mensaje = $mensaje.'<br>';
            }
            if($cantServ > 0){
                $mensaje = $mensaje.'
              <br><strong>Servicios Adicionales</strong><br>';
                    for ($i=0; $i < $cantServ ; $i++) { 
                        
                                $mensaje = $mensaje.$arrayDescServ[$i].'</br>
                                <strong>Sub-Total Bs: '.$arraySubtServ[$i].'</strong></br>';
                    }
            }
            if($ind_descuento > 0 && $descuento_cod > 0){
              $mensaje = $mensaje.'<strong><span style="font-size:14px">Precio total antes: Bs <strike style="color:red;">'.$totalPagar.'</strike></span></strong><br>
              <strong><span style="font-size:14px">Precio total ahora: '.$formatPrecio.'</span></strong><br>
              Con un '.substr($descuento_cod, 2, 2).'% de descuento aplicado en habitaciones<br>';
            }
        $mensaje = $mensaje.'
<strong><span style="font-size:14px">Precio total de la reservación: '.$formatPrecio.'</span></strong></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateColumns">
                  <!--[if gte mso 9]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                  <tr>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comentarios del Cliente</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><em><span style="font-size:13px">'.$observaciones.'</span></em></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  <td align="center" valign="top" width="300" style="width:300px;">
                  <![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <h1><span style="font-family:verdana,geneva,sans-serif"><span style="font-size:14px">Comprobante de pago</span></span></h1>
<span style="font-family:verdana,geneva,sans-serif"><span style="font-size:13px"><em>'.$voucher.'</em></span></span>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateLowerBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                        
                            <div style="text-align: justify;"><strong><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Información de contacto: Tlf. '.$telefono.', email '.$email.'</span></span></strong><br>
<br>
<span style="font-family:verdana,geneva,sans-serif; font-size:12px; line-height:1.6em; text-align:justify">Este mensaje es privado, confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.</span><br>
&nbsp;</div>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
              <tr>
                <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.facebook.com/HesperiaVenezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://instagram.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://twitter.com/hesperiave" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                        <tbody><tr>
                                                                            
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                                    <a href="https://www.linkedin.com/company/hoteles-hesperia-venezuela" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display:block;" height="24" width="24" class=""></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
              </tr>
            </table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
            <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
$mail_object = new PHPMailer();
            $mail_object->IsSMTP(); // Indicamos que use SMTP
            //$mail_object->SMTPDebug = 3;   //activar modo debug                                   
            $mail_object->Host = 'a2plcpnl0042.prod.iad2.secureserver.net';  // Indicamos los servidores SMTP
            $mail_object->SMTPAuth = true;                               // Habilitamos la autenticación SMTP
            $mail_object->Username = 'reserva@hoteleshesperia.com.ve';                 // SMTP username
            $mail_object->Password = 'Nh123456nh';                           // SMTP password
            $mail_object->Port = 465;                                    // TCP port
            $mail_object->SMTPSecure = "ssl"; // Establece el tipo de seguridad SMTP a SSL.
            $mail_object->Timeout =30;
            //$mail_object->SMTPDebug = 1;
                           
            /** Configurar cabeceras del mensaje **/
            $mail_object->From = $contacto;                       // Correo del remitente
            $mail_object->FromName = 'Reservas Hesperia Hotels & Resorts';           // Nombre del remitente
            $mail_object->Subject = 'Reserva registrada en '.$razonsocial;                // Asunto
             
            /** Incluir destinatarios. El nombre es opcional **/
            $mail_object->addAddress($contRes);
            $mail_object->addAddress($contAdmin);
            $mail_object->addAddress($contRec);
            $mail_object->addAddress($contPag);
            $mail_object->addAddress($contProg);
            $mail_object->addAddress($contExtra);
            $mail_object->addAddress($contExtra2);            
            /** Enviarlo en formato HTML **/
            $mail_object->isHTML(true);                                  
             
            /** Configurar cuerpo del mensaje **/
            $mail_object->Body    = utf8_decode($mensaje);
            $mail_object->AltBody = $mensaje;           
            
            
            /** Enviar mensaje... **/           
            if(!$mail_object->send()) {
                $errors = 'Ocurrio un error al registrar el pago. ';
                $errors .= 'Mailer Error: ' . $mail_object->ErrorInfo;
                echo $errors;
            }
        
        /** Configurar SMTP **/
            
            
        echo $mensajeHuesped;
        graba_LOG("Registro de Nueva Reservación: $nombres $apellidos -$codigoR",$email,$ip,$ahora,$hostname,$user,$password,$db_name);
    }else{
        echo '<div class="alert alert-danger" role="alert">
      <p>Ha ocurrido un error inesperado. Su pago fue procesado pero no pudo ser registrado en nuestra plataforma por favor contactenos via email a la siguiente dirección: '.$contacto.' e indiquenos este error.</p>
    </div>';
    }
} else
{  echo'
        <div class="alert alert-danger" role="alert"><p>Error inesperado, no se pudo procesar su pago por error en nuestra plataforma. Por favor contacte al administrador principal del sitio.</p></div>';
 exit();
}
?>
