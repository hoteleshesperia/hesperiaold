<?php 
$antesdecore = 1;
include 'databases.php';

$id_hotel = $_POST["idhotel"];
$id_tipo_hab = $_POST["idtipohab"];
$combinacion = $_POST["combinacion"];
$monto = $_POST["monto"];

$sql = sprintf("SELECT porcentaje FROM hesperia_v2_porcentaje_hab 
				WHERE id_hotel = '%s' 
				AND id_tipo_hab = '%s' 
				AND combinacion = '%s'",
                mysqli_real_escape_string($mysqli,$id_hotel),
                mysqli_real_escape_string($mysqli,$id_tipo_hab),
                mysqli_real_escape_string($mysqli,$combinacion));
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    

if($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){
	$monto = $monto + ($monto * $rows["porcentaje"]);
}

echo number_format(round($monto), 2, ',', '.');
?>