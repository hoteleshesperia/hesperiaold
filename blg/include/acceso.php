<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/
session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header("location:../error.html");
	die();}
$antesdecore = 1;
include 'databases.php';
$_POST["username"]=strip_tags(strtolower(trim($_POST["username"])));
$_POST["clave"]=strip_tags(trim($_POST["clave"]));
$email=filter_var(trim($_POST["username"]),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$clave=md5(filter_var(trim($_POST["clave"]),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH));
$sql=sprintf("SELECT * FROM hesperia_usuario
				WHERE hesperia_usuario.email='%s' && hesperia_usuario.clave_acceso='%s' limit 1",
			mysqli_real_escape_string($mysqli,$email),mysqli_real_escape_string($mysqli,$clave));
$result=QUERYBD($sql,$hostname,$user,$password,$db_name);
if ($row=mysqli_fetch_array($result,MYSQLI_ASSOC))
 { session_cache_expire(28800);
   $tiempo=time();
   $hora=$tiempo-28800;
   $sql=sprintf("DELETE FROM hesperia_sesion WHERE hesperia_sesion.fecha <='$hora'");
   $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
   $_SESSION["referencia"]=$row["id"];
   $_SESSION["email"]=$row["email"];
   if ($row["ultimo_login"] == 0 || empty($row["ultimo_login"]))
	{ $row["ultimo_login"]=$tiempo; }
   $_SESSION["ultimo_login"]=$row["ultimo_login"];
   $_SESSION["nivel"]=$row["nivel"];
 if ($_SESSION["nivel"]<=2)
    {	$_SESSION["id_hotel"]=$row["id_hotel"];
        $_SESSION["admin"]= $row["nombres"]; }
   $_SESSION["email"]=$row["email"];
   $_SESSION['telefono']=$row["telefono"];
	$_SESSION["nombres"] = $row["nombres"];
	$_SESSION["apellidos"] = $row["apellidos"];
   $_SESSION["nombre"]=$row["nombres"].' '.$row["apellidos"];
   $sql  =sprintf("DELETE FROM hesperia_sesion WHERE hesperia_sesion.referencia='$_SESSION[referencia]'");
   $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
  $sql=sprintf("UPDATE hesperia_usuario SET ultimo_login='$tiempo', valido='1' WHERE hesperia_usuario.id='$_SESSION[referencia]'");
   $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
   $sesion=session_id();
   $sql=sprintf("INSERT INTO hesperia_sesion VALUES (NULL, '$sesion', '$tiempo', '$_SESSION[referencia]')");
   $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
   graba_LOG("Inicio de sesion",$_SESSION["nombre"],$_SERVER['REMOTE_ADDR'],$tiempo,$hostname,$user,$password,$db_name);
   echo 'Correcto!';
   die(); }
else { echo '<div class="alert alert-danger" role="alert">
	  <p>El eMail o la clave de acceso que proporcion&oacute; no es correcta. Intente de Nuevo.</p>
	</div>';
	graba_LOG("Inicio de sesion fallido",$_POST["username"],$_SERVER['REMOTE_ADDR'],$tiempo,$hostname,$user,$password,$db_name); }
?>
