<?php

/* -------------------------------------------------------

Script  bajo los t&eacute;rminos y Licencia

Apache License

Version 2.0, January 2004

https://www.apache.org/licenses/LICENSE-2.0

Autor: Hector A. Mantellini (@Xombra)

Diseño: Angel Cruz (@abr4xas)

ViserProject - @viserproject

-------------------------------------------------------- */

session_start();

//require( 'php_error.php' );

//    \php_error\reportErrors();

include 'include/core.php';

?>

<!DOCTYPE html>

<html lang="<?php echo $lenguaje; ?>">

<head>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link href="css/modern-business.css" rel="stylesheet">

    <link href="css/datepicker.css" rel="stylesheet">

<!--    <link href="css/slider-hotel.css" rel="stylesheet">-->

    <link href="css/lightbox.css" rel="stylesheet" >

    <script type="text/javascript" src="http://hoteleshesperia.com.ve/2016h01e29/js/jquery.js"></script>



<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript">

var mobile = (/iPhone|iPad|iPod|android|blackberry|bb|mini|Nokia|PlayBook|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

if (mobile) {

    $( ".my-reserva" ).remove(); // delete reservation form

}





$(document).ready(function(){

    $('[data-toggle="popover"]').popover();

});

</script>

<?php

META($charset,$lenguaje,$timezone_set,$region,$go,$data,$hostname,$user,$password,$db_name,$mysqli); ?>

</head>

<!--<body onload="document.getElementById('cargando').style. display='none';">-->

    <body>

<!--<div id="cargando" style="width: 100%; height: 100%; text-align: center;z-index:9999"><img src="http://hoteleshesperia.com.ve/2016h01e29/img/iconos/loading.gif" style="width:60px" alt="Cargando..."></div>-->

<noscript><p>Debe habilitar el uso de <strong>Javascript</strong>, para poder usar muchas de las funciones del sitio</p></noscript>

<?php flush(); ?>

    <nav class="navbar navbar-default" role="navigation">

        <div class="container">

        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-hesperia" aria-expanded="false">

                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

            </button>

            <a class="navbar-brand hidden-lg hidden-md" href="index.php">

                <img src="http://hoteleshesperia.com.ve/2016h01e29/img/logo/logohesperiahotels-m.png" class="img-responsive logo-main">

            </a>

            <a class="navbar-brand visible-lg visible-md" href="index.php">

                <img src="http://hoteleshesperia.com.ve/2016h01e29/img/logo/logohesperiahotels.png" class="img-responsive logo-main">

            </a>

        </div>

            <div class="collapse navbar-collapse" id="menu-hesperia">

                <ul class="nav navbar-nav">

                    <li><a href="index.php">Inicio</a></li>

                    <li>

                    <?php

//                        if ($_GET['principio'] == 0) { $_GET['sucursal'] = 0;}

//    esta parte muestra solo el link de nosotros general sin importar cual hotel fue seleccionado

echo '<a href="index.php?go=0/nosotros/0/'.FSPATH("Hesperia Hotels & Resorts Venezuela").'" title="Hesperia Hotels & Resorts Venezuela">Nosotros</a>'; ?>

                    </li>

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hoteles <span class="caret"></span></a>

                        <ul class="dropdown-menu" role="menu">

                            <?php

                            $sql = "SELECT id,nombre_sitio FROM hesperia_settings ORDER BY razon_social ASC";

                            $resultH = QUERYBD($sql,$hostname,$user,$password,$db_name);

                            while ($rows = mysqli_fetch_array($resultH,MYSQLI_ASSOC))

                                { echo '

                                    <li>

                                        <a href="index.php?go='.$rows["id"].'/detalle/0/'.FSPATH($rows["nombre_sitio"]).'" target="_self" title=" '.$rows["nombre_sitio"].'">'.$rows["nombre_sitio"].'</a>

                                    </li>';

                                }

echo     '</ul>

                    </li>';

echo '

                    <li><a href="index.php?go=0/eventos/0/'.FSPATH("Eventos").'" title="Eventos">Reuniones y Eventos</a></li>

                    <li><a href="index.php?go=0/experiencias/0/'.FSPATH("Experiencias").'" title="Experiencias">Experiencias</a></li>

                    

                </ul>

                <ul class="nav navbar-nav navbar-right">';

                    if (!isset($_SESSION["referencia"])){

                echo '

                    <li><a id="modal-433854" href="#modal-container-433854" data-toggle="modal" title="Seci&oacute;n de Usuarios registrados"><i class="fa fa-users hidden-xs hidden-sm"></i> Mi Cuenta

                        </a>

                    </li>

                    <li><a id="modal-433854" href="#modal-container-433666" data-toggle="modal" title="Registro de nuevo usuarios"><i class="fa fa-user hidden-xs hidden-sm"></i> Registrarse

                        </a>

                    </li>'; }

                else

        {

                echo '

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                            Usuario: <strong>'.$_SESSION["nombre"].'</strong><span class="caret"></span>

                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>

                                <a href="index.php?go='.$_GET['sucursal'].'/perfil/0/'.FSPATH("Mi Cuenta").'" title="Ver mi Cuenta">

                                    <i class="fa fa-users"></i> Mi Cuenta

                                </a>

                            </li>';

                      if (isset($_SESSION["referencia"])){

                              if ($_SESSION["nivel"] <= 2){

                                echo '<li><a href="panel/home.php" target="_blank" title="Panel de Control"><i class="fa fa-server hidden-xs hidden-sm"></i> Panel de Control </a>

                    </li>';

                                        } }



echo '

                            <li>

                                <a href="index.php?go='.$_GET['sucursal'].'/salir/0/'.FSPATH('Salir del Sistema').'" title="Salir Del Sistema | Cerrar Sesion">

                                    <i class="fa fa-sign-out"></i> Salir

                                </a>

                            </li>

                        </ul>

                    </li>'; }

echo '

                </ul>

            </div>

        </div>

    </nav>';

    if ($go != 'perfil' && $go != 'contacto' && $go != 'prensa' && $go != 'reserva' && $go != 'compra' && $go != 'reservapaquete') {

//        if (DETECTAR()) {

            CARROUSEL($mysqli,$data,$hostname,$user,$password,$db_name,$go);

//        }

    }

echo   '<div class="container"><br/>

            <div class="row hidden-lg hidden-md">

                <div class="col-lg-12">';

                if ($go != 'perfil' &&

                    $go != 'contacto' &&

                    $go != 'prensa'

                    && $go != 'reserva'

                    && $go != 'compra'

                    && $go != 'reservapaquete' ) {

                           CREAR_OTRA_RESERVA($mysqli,$hostname,$user,$password,$db_name);

                }

    echo'        </div>

            </div>

        </div><!-- /container cor-->';

echo '<div class="container">';

    SELECTOR($go,$hostname,$user,$password,$db_name,$data,$mysqli);

    LOGOS($mysqli,$data,$hostname,$user,$password,$db_name);

    echo '</div><!-- /container S-->';

    FOOTER($mysqli,$data,$hostname,$user,$password,$db_name,$go);

if (empty($_SESSION["referencia"]))

{ INICIO_SESSION($data,$hostname,$user,$password,$db_name); }

if (empty($_SESSION["referencia"]))

{ REGISTRO($data,$hostname,$user,$password,$db_name); }

?>

<div class="contcookies" style="display: none;">Este sitio, como la mayoría, usa cookies. Si continua navegando en nuestro sitio entendemos que acepta las <a href="index.php?go=politicas">Política de uso</a>. <a href="#" class="cookiesaceptar">Aceptar</a></div>

    <script type="text/javascript" src="http://hoteleshesperia.com.ve/2016h01e29/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="http://hoteleshesperia.com.ve/2016h01e29/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="http://hoteleshesperia.com.ve/2016h01e29/js/lightbox.min.js"></script>

    <script type="text/javascript" src="http://hoteleshesperia.com.ve/2016h01e29/js/custom.js"></script>

<?php GOOGLE_ANALYTICS("UA-67251481-1"); ?>

</body>

</html>

<?php BUFFER_FIN($mysqli); ?>

