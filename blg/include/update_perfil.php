<?php

    require_once('recaptchalib.php');
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/
session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header("location:../error.html");
	die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
unset($sql);
$ahora = time();
$email = strip_tags(strtolower(trim($_POST['email'])));
$email = filter_var($email,FILTER_SANITIZE_EMAIL);
$apellidos = strip_tags(ucwords(trim($_POST['apellidos'])));
$nombres = strip_tags(ucwords(trim($_POST['nombres'])));
$telefono = strip_tags(trim($_POST['telefono']));
$numero_empresa  = strip_tags(trim($_POST['numero_empresa']));
$empresa  = strip_tags(trim($_POST['empresa']));
$email_empresa = strip_tags(strtolower(trim($_POST['email_empresa'])));
$email_empresal = filter_var($email_empresa,FILTER_SANITIZE_EMAIL);
$preguntas = $_POST['preguntas'];

// tu clave secreta
    $secret = "6LdKuBYTAAAAAAzaD1NhaYVfM3wyGET4VTIpIjUt";
     
    // respuesta vacía
    $response = null;
     
    // comprueba la clave secreta
    $reCaptcha = new ReCaptcha($secret);

if ($_POST["g-recaptcha-response"]) {
	$response = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
    );
}

if ($response != null && $response->success) {

	if($preguntas == 0){
		$pregunta_1 = $_POST['pregunta1'];
		$pregunta_2 = $_POST['pregunta2'];
		$pregunta_3 = $_POST['pregunta3'];
	}


	$respuesta_1 = $_POST['respuesta1'];
	$respuesta_2 = $_POST['respuesta2'];
	$respuesta_3 = $_POST['respuesta3'];
	$sql = sprintf("SELECT * FROM hesperia_usuario WHERE email='%s'",
						mysqli_real_escape_string($mysqli,$email));
	$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
	if (mysqli_num_rows($result) == 0){  
		echo '1';
		/*echo '<div class="alert alert-danger" role="alert">
		  <p>Ha ocurrido un error inesperado. Probablemente no esta registrada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
		</div>'; */
	}else { 
		graba_LOG("Cambio de Perfil usuario: $email",$email,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
		if($preguntas == 1){
			 $row=mysqli_fetch_array($result,MYSQLI_ASSOC);
				 if($row["respuesta_1"] == $respuesta_1 && $row["respuesta_2"] == $respuesta_2 && $row["respuesta_3"] == $respuesta_3){
				 	$sql = sprintf("UPDATE hesperia_usuario SET
								email = '%s',
								nombres = '%s',
								apellidos = '%s',
								telefono = '%s',
								empresa = '%s',
								numero_empresa = '%s',
								email_empresa = '%s',
								valido = '%s'
								WHERE id = '%s'",
								mysqli_real_escape_string($mysqli,$email),
								mysqli_real_escape_string($mysqli,$nombres),
								mysqli_real_escape_string($mysqli,$apellidos),
								mysqli_real_escape_string($mysqli,$telefono),
								mysqli_real_escape_string($mysqli,$empresa),
								mysqli_real_escape_string($mysqli,$numero_empresa),
								mysqli_real_escape_string($mysqli,$email_empresa),
								mysqli_real_escape_string($mysqli,1),
								mysqli_real_escape_string($mysqli,$_SESSION["referencia"]));
				 	$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
					if (mysqli_affected_rows($mysqli)){ 
						echo '0';
						/*echo '<br/><div class="alert alert-success" role="alert">
							<p>Cambios en perfil realizados satisfactoriamente!</p>
							</div>';*/
					 } else{ 
					 	echo '1';
					 	/*echo '<div class="alert alert-danger" role="alert">
						  <p>Ha ocurrido un error inesperado. no se puedo realizar los cambios o no cambio nada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
						</div>'; */
					}
				 }else{
				 	echo '2';
				 	/*echo '<div class="alert alert-danger" role="alert">
						  <p>No puede actualizar los datos de perfil debido a que no se respondieron correctamente a las preguntas de seguridad</p>
						</div>'; */
				 }
			
			
		}else{
			$sql = sprintf("UPDATE hesperia_usuario SET
							email = '%s',
							nombres = '%s',
							apellidos = '%s',
							telefono = '%s',
							empresa = '%s',
							numero_empresa = '%s',
							email_empresa = '%s',
							valido = '%s',
							pregunta_1 = '%s',
							pregunta_2 = '%s',
							pregunta_3 = '%s',
							respuesta_1 = '%s',
							respuesta_2 = '%s',
							respuesta_3 = '%s'
							WHERE id = '%s'",
							mysqli_real_escape_string($mysqli,$email),
							mysqli_real_escape_string($mysqli,$nombres),
							mysqli_real_escape_string($mysqli,$apellidos),
							mysqli_real_escape_string($mysqli,$telefono),
							mysqli_real_escape_string($mysqli,$empresa),
							mysqli_real_escape_string($mysqli,$numero_empresa),
							mysqli_real_escape_string($mysqli,$email_empresa),
							mysqli_real_escape_string($mysqli,1),
							mysqli_real_escape_string($mysqli,$pregunta_1),
							mysqli_real_escape_string($mysqli,$pregunta_2),
							mysqli_real_escape_string($mysqli,$pregunta_3),
							mysqli_real_escape_string($mysqli,$respuesta_1),
							mysqli_real_escape_string($mysqli,$respuesta_2),
							mysqli_real_escape_string($mysqli,$respuesta_3),
							mysqli_real_escape_string($mysqli,$_SESSION["referencia"]));
			
			$result = QUERYBD($sql,$hostname,$user,$password,$db_name);
			if (mysqli_affected_rows($mysqli)){ 
				echo '0';
				/*echo '<br/><div class="alert alert-success" role="alert">
					<p>Cambios en perfil realizados satisfactoriamente!</p>
					</div>';*/
			 } else{ 
			 	echo '1';
			 	/*echo '<div class="alert alert-danger" role="alert">
				  <p>Ha ocurrido un error inesperado. no se puedo realizar los cambios o no cambio nada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
				</div>'; */
			}
		}
	}
	unset($result,$sql,$email,$headers);
	$_POST = array();
}else{
	echo '1';
}
?>
