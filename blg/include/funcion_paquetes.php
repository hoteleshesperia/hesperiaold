<?php 
  function PAQUETES_GENERAL($mysqli,$data,$hostname,$user,$password,$db_name){
    echo '
    <div class="page-header">
            <h1 style="margin-top:10px;">Nuestros Paquetes</h1>
          </div>';

    $hoy = date("Y-m-d",time());          

    if($data["id_paquete_promo"] == 0){

      $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 ORDER BY id_hotel, temporada ASC");

    }else{

      $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND id = '%s' ORDER BY orden ASC",
                      mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

    }

    if($data["temporada"] > 0){
      $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND temporada = '%s' ORDER BY orden ASC",
                      mysqli_real_escape_string($mysqli,$data["temporada"]));
    }

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1){ 
      echo '<br/>
          <div class="text-center">
            <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p>
          </div>';
        return; 
   }else{
      echo '
      <div class="container-fluid">
      <div class="row">';
      $id_hotel_aux = 0;
      while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
        $fecha = date("d/m/Y",time()+86400); 
        $nombre_paquete = $rowPaquetes["nombre_paquete"];
        $capacidad = $rowPaquetes["capacidad"];
        $asunto = str_replace(' ','+',$nombre_paquete);

        $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                       mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));

        $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

        $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
        $hotel = $rowSetting["razon_social"];
        /*if($id_hotel_aux != $rowPaquetes["id_hotel"]){
          if($id_hotel_aux != 0){
            echo '</div><div class="row">';
          }
          echo '
            <h3 class="text-primary text-center">'.$hotel.'</h3>';
          $id_hotel_aux = $rowPaquetes["id_hotel"];
        }*/

        $desdePaq = '';
        $hastaPaq = '';
        $precio = $rowPaquetes["precio"];
        $habitacion = '';

        if($rowPaquetes["porcentaje"] > 0){
          $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                    mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));
          $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
          $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
          $habitacion = $rowHab["nombre_habitacion"];
          echo '<input type="hidden" name="habitacion" value="'.$habitacion.'"/> ';

        }

        $pos = strpos($rowPaquetes["nombre_paquete"], '. ');
        if($pos > 0){
          $pos++;
        }

        $nombre = substr($rowPaquetes["nombre_paquete"], $pos);
        echo '
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="http://hoteleshesperia.com.ve/img/paquetes/'.$rowPaquetes["img_paquete"].'" width="300" height="120" alt="'.$rowPaquetes["nombre_paquete"].'">
            <div class="caption">
              <h4 style="height: 70px;" class="text-justify">'.$nombre.'</h4>
              <p class="text-muted text-center"><strong>'.$hotel.'</strong></p>
              <p><a href="http://hoteleshesperia.com.ve/paquetes/'.$rowPaquetes["id"].'/" class="btn btn-primary" role="button">Ver más</a></p>
            </div>
          </div>
        </div>
        ';
      }
      echo '</div>
      </div>';
      return;
   } 
  }

  function PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name){
    $hoy = date("Y-m-d",time());   

    $sql = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 AND id = '%s' ORDER BY orden ASC",
                    mysqli_real_escape_string($mysqli,$data["id_paquete_promo"]));

    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

    $hay = mysqli_num_rows($result);

    if ($hay < 1){ 

      echo '<br/><div class="text-center">
            <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>
            <h2>Disculpe</h2> <p>Actualmente no poseemos paquetes disponibles</p></div>';

      return; 
    }else{
       while ($rowPaquetes=mysqli_fetch_array($result,MYSQLI_ASSOC)){

          $precio = $rowPaquetes["precio"];
          $nombre_paquete = $rowPaquetes["nombre_paquete"];
          $capacidad = $rowPaquetes["capacidad"];
          /*Se buscan los upselling disponibles por ese paquete*/
          $sqlUpselling = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 1 and status <> 'N' and partai = 0 and vip = 0 AND id_hotel = '%s' ORDER BY orden ASC",
            mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));
          $resultUpselling = QUERYBD($sqlUpselling,$hostname,$user,$password,$db_name);
          /*Se busca información del hotel en donde se ofrece el paquete*/
          $sqlSetting = sprintf("SELECT razon_social FROM hesperia_settings WHERE  id = '%s'",
                   mysqli_real_escape_string($mysqli,$rowPaquetes["id_hotel"]));
          $resultSetting = QUERYBD($sqlSetting,$hostname,$user,$password,$db_name);

          $rowSetting=mysqli_fetch_array($resultSetting,MYSQLI_ASSOC);
          $hotel = $rowSetting["razon_social"];

          $desdePaq = '';
          $hastaPaq = '';
          $habitacion = '';
          $fecha = date("d/m/Y",time()+86400); 
          
          if($rowPaquetes["porcentaje"] > 0){

            $sqlHab = sprintf("SELECT nombre_habitacion FROM hesperia_habitaciones WHERE id_habitacion = '%s'",
                           mysqli_real_escape_string($mysqli,$rowPaquetes["id_tipo_hab"]));

             $resultHab = QUERYBD($sqlHab,$hostname,$user,$password,$db_name);
             $rowHab=mysqli_fetch_array($resultHab,MYSQLI_ASSOC);
             $habitacion = $rowHab["nombre_habitacion"];

          }

          echo '
          <form enctype="application/x-www-form-urlencoded" action="http://hoteleshesperia.com.ve/reserva-paquete/'.toAscii($nombre_paquete).'" role="form" method="post" name="FormReservacionPaq" id="FormReservacionPaq" class="reserva" >
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3>'.$nombre_paquete.'</h3>
              </div>
              <div class="panel-body">
                <div class="col-md-12 text-justify">
                  <div class="col-md-4">
                    <img src="http://hoteleshesperia.com.ve/imagen.php?imagen='.$rowPaquetes["img_paquete"].'-3"  alt="'.$rowPaquetes["nombre_paquete"].'" class="img-responsive" />
                  </div>
                  <div class="col-md-8">
                  '.$rowPaquetes["descripcion_paquete"].' <br>';

                  if($precio > 0){



                      if($rowPaquetes["precio_nino"] > 0){

                          echo '<strong>Precio por adulto: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>

                          <strong>Precio por niño: '.number_format(round($rowPaquetes["precio_nino"]), 2, ',', '.').'</strong><br/>';

                          if($hoy < $rowPaquetes["desde"]){

                                  $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                          }

                      }else{

                          if($rowPaquetes["porcentaje"] > 0){

                              $precio = $rowPaquetes["precio"] - ($rowPaquetes["precio"] * $rowPaquetes["porcentaje"]);

                              if($rowPaquetes["ninos_gratis"] ==  0){

                                  echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                  <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Con un '.substr($rowPaquetes["porcentaje"], 2, 2).'% de descuento aplicado)<br/>

                                  <input type="hidden" name="numPaq" id="numPaqV2" value="1"/>';

                              }else{

                                  echo 'Precio antes: <strike>'.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strike><br/>

                                  <strong>Precio ahora: '.number_format(round($precio), 2, ',', '.').'</strong> (Incluidos '.$rowPaquetes["ninos_gratis"].' Niños Gratis)<br/>

                                  <input type="hidden" name="numPaq" id="numPaqV2" value="1"/>';

                              }

                              if($hoy < $rowPaquetes["desde"]){

                                  $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                              }

                          }else{

                              if($hoy < $rowPaquetes["desde"]){

                                  $fecha = date("d/m/Y", strtotime($rowPaquetes["desde"]));

                              }

                              echo '<strong>Precio: '.number_format(round($rowPaquetes["precio"]), 2, ',', '.').'</strong><br/>';

                          }

                          

                      }

                  echo '

                  <hr>

                  <strong>Selecciona fecha de llegada<br> </strong><input type="text" class="dpd3 input-sm" value="'.$fecha.'" id="dpd3" name="dpd3" placeholder="Entrada" data-date-format="dd/mm/yyyy" data-min-date="'.$rowPaquetes["desde"].'" data-max-date="'.$rowPaquetes["hasta"].'" language="es" readonly="readonly" ><br>

                  <input type="hidden" name="nombrePaq" value="'.$nombre_paquete.'"/>

                  <input type="hidden" name="precioPaq" value="'.$precio.'" id="precioPaq"/>

                  <input type="hidden" name="hotelPaq" value="'.$hotel.'"/>

                  <input type="hidden" name="id_hotel" value="'.$rowPaquetes["id_hotel"].'"/>

                  <input type="hidden" name="tiempo" value="'.$rowPaquetes["tiempo"].'"/>                

                  <input type="hidden" name="desdePaq" value="'.$desdePaq.'"/>

                  <input type="hidden" name="hastaPaq" value="'.$hastaPaq.'"/>

                  <input type="hidden" name="precioNino" value="'.$rowPaquetes["precio_nino"].'" id="precioNino"/>

                  <input type="hidden" name="porcentaje" value="'.$rowPaquetes["porcentaje"].'"/>

                  <input type="hidden" name="partai" value="'.$rowPaquetes["partai"].'"/>

                  <input type="hidden" name="habitacion" value="'.$habitacion.'"/>

                  <input type="hidden" name="vip" value="0"/>';

                  if($rowPaquetes["precio_nino"] > 0){

                     echo'<div class="row">

                              <div class="col-md-2"><strong>Adultos<br> 

                                  <select name="numPaq" id="numPaqV2">

                                      <option value="1">1</option>

                                      <option value="2">2</option>

                                      <option value="3">3</option>

                                      <option value="4">4</option>

                                  </select></strong>

                              </div>

                              <div class="col-md-2"><strong>Niños<br> 

                                  <select name="numPaqN" id="numPaqNV2">

                                      <option value="0">0</option>

                                      <option value="1">1</option>

                                      <option value="2">2</option>

                                      <option value="3">3</option>

                                      <option value="4">4</option>

                                  </select></strong>

                              </div>

                          </div>';

                  }else{

                      if($rowPaquetes["porcentaje"] <= 0){

                          if($capacidad == 5){

                              echo'

                          <div class="row">

                                  <div class="col-sm-6"><strong>Cantidad<br> 

                                      <select name="numPaq" id="numPaqV2">                                    

                                          <option value="5">5</option>

                                          <option value="6">6</option>

                                          <option value="7">7</option>

                                          <option value="8">8</option>

                                          <option value="9">9</option>

                                          <option value="10">10</option>

                                          <option value="11">11</option>

                                          <option value="12">12</option>

                                          <option value="13">13</option>

                                          <option value="14">14</option>

                                          <option value="15">15</option>

                                          <option value="16">16</option>

                                          <option value="17">17</option>

                                          <option value="18">18</option>

                                          <option value="19">19</option>

                                          <option value="20">20</option>

                                      </select></strong>

                                  </div>

                          </div>';

                          }else{

                          echo'

                          <div class="row">

                                  <div class="col-sm-6"><strong>Cantidad<br> 

                                      <select name="numPaq" id="numPaqV2">

                                          <option value="1">1</option>

                                          <option value="2">2</option>

                                          <option value="3">3</option>

                                          <option value="4">4</option>

                                          <option value="5">5</option>

                                          <option value="6">6</option>

                                          <option value="7">7</option>

                                          <option value="8">8</option>

                                          <option value="9">9</option>

                                          <option value="10">10</option>

                                          <option value="11">11</option>

                                          <option value="12">12</option>

                                          <option value="13">13</option>

                                          <option value="14">14</option>

                                          <option value="15">15</option>

                                          <option value="16">16</option>

                                          <option value="17">17</option>

                                          <option value="18">18</option>

                                          <option value="19">19</option>

                                          <option value="20">20</option>

                                      </select></strong>

                                  </div>

                          </div>';

                          }

                      }

                  }

                  
                  }
                echo '
                  </div>  
                </div>
              </div>
              <div class="panel-footer">';
              if($rowPaquetes["full_day"] == 0){
                $up = 0;
              echo '
                <h5><strong>Personalice su estancia</strong></h5>
                <div class="checkbox">';
                  while($rowUpselling = mysqli_fetch_array($resultUpselling,MYSQLI_ASSOC)){
                    echo '<label style="font-size:14px;"><input type="checkbox" name="upselling-'.$up.'" class="checkUpselling" data-precio="'.$rowUpselling["precio"].'" value="'.$rowUpselling["id"].'" /><a id="modal-433854" href="#modal-container-paq'.$rowUpselling["id"].'" data-toggle="modal" title="Detalles del paquete">'.$rowUpselling["nombre_paquete"].'</a>.&nbsp;Precio:&nbsp;Bs.&nbsp;<strong>'.number_format(round($rowUpselling["precio"]), 2, ',', '.').'</strong></label><br>
                    
                    <div  class="modal fade" id="modal-container-paq'.$rowUpselling["id"].'" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                            <div class="modal-dialog">

                                <div class="modal-content mymod">

                                    <div class="modal-header">

                                        <a data-dismiss="modal" class="close">×</a>

                                        <h5>'.$rowUpselling["nombre_paquete"].'</h5>

                                     </div>

                                     <div class="modal-body">

                                        <p style="font-size:13px;"><strong>Precio: Bs. '.number_format(round($rowUpselling["precio"]), 2, ',', '.').'</strong></p>

                                         <br/>

                                        <div class="text-justify" style="font-size:13px;"> '.$rowUpselling["descripcion_paquete"].'</div> 

                                                     

                                    </div>

                                    <div class="modal-footer">

                                        <a href="#" data-dismiss="modal" class="btn">Cerrar</a>

                                    </div>

                                </div>

                            </div>

                        </div>
                    ';
                    $up ++;
                  }

                echo '
                </div>
                <hr>';
                }

                if($precio > 0){
                  echo'
                  <div class="text-center">
                    <p><strong>Precio total: Bs. <input type="text" disabled="disabled" value="'.number_format(round($precio), 2, ',', '.').'" name="precioTotalPagar" id="precioTotalPagar"></strong></p>
                    <input type="hidden" id="precioSF" value='.$precio.'/>
                    <button type="submit" title="Reservar paquete" class="btn btn-primary" target="_self">¡Reserva Ahora!</button>
                  </div>';

                }
            echo '
              </div>
            </div>
          </form>
          ';
       } 
    }
     
    
    $sqlDestacados = sprintf("SELECT * FROM hesperia_paquetes where alojamiento = 0 and status <> 'N' and partai = 0 and vip = 0 and destacado = 1 ORDER BY orden ASC");
     $resultDestacados = QUERYBD($sqlDestacados,$hostname,$user,$password,$db_name);
     echo '
     <h3>Los más destacados</h3>
      <div class="row">';
     while ($rowDestacados=mysqli_fetch_array($resultDestacados,MYSQLI_ASSOC)){
        echo '
          <div class="col-md-3 col-xs-6 pdf-thumb-box no-space" id="paquete-'.$rowDestacados["id"].'"> 
                      
              <a href="http://hoteleshesperia.com.ve/paquetes/'.$rowDestacados["id"].'/" >
                <div class="pdf-thumb-box-overlay">
                  <h6 class="text-center">'.$rowDestacados["nombre_paquete"].'</h6>
                  <div class="btn-primary btn btn-sm">
                    Reservar
                  </div>
                </div>
                <img src="http://hoteleshesperia.com.ve/img/paquetes/'.$rowDestacados["img_paquete"].'" alt="'.$rowDestacados["nombre_paquete"].'" class="img-responsive">
              </a>
          </div>';
     }
     echo '</div>'; 

    echo '

        <div id="RespuestaIp"></div>

           </div>';

    return;
  }
?>

    