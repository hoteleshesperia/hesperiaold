<?php

/* -------------------------------------------------------

Script  bajo los términos y Licencia

Apache License

Version 2.0, January 2004

https://www.apache.org/licenses/LICENSE-2.0

Autor:Hector A. Mantellini (Xombra)

--------------------------------------------------------*/

$ahora = md5(time());

global $mysqli;



if(!isset($antesdecore)){

    include 'include/databases.php';

    include 'include/funciones.php';

}

else{

    include 'databases.php';

    include 'funciones.php';

}



include 'include/funcion_reservacion.php';



ENCABEZADO($lenguaje,$timezone_set,$charset);

LIMPIAR_VALORES();

#BUFFER_INICIO("COMPRESS_PAGE");



# Opciones de enlace



#Explicacion

# index.php?go=sucursal/opcion_selector/opcion_seleccion_otra_ID/titulo_corto

# Ejemplo:

# index.php?go='.$rows["id"].'/index/0/'.FSPATH($rows["nombre_sitio"]).'

# index.php?go='.$_GET['sucursal'].'/index/0/'.FSPATH($rows["nombre_sitio"]).'



if (empty($_GET['go'])){

    $_GET['go']=$go='index';

    $_GET['principio'] = 0;

    $data['valor'] = 'INDEX';

    $sucursal = $_GET['sucursal']= 1; }

else

{  $value=explode("/",strip_tags(trim($_GET['go'])));

 if ($value[0] == 0) {

     $sucursal = $_GET['sucursal'] = $value[0] = 1;

     $_GET['principio'] = 0; }

 else { $sucursal=$_GET['sucursal']=$value[0];

       $_GET['principio'] = 1; } # Cual Sucursal

 $_GET['go']=$go=$value[1]; 	# Que hace

 if (isset($value[3])) {

     $data['valor']=$value[3];   } # en enlace corto

 if(empty($_GET["id"])){

     if (isset($value[2])) {

         $_GET["id"]=$value[2]; }

     else { $_GET['sucursal'] = 1; }  # opcion de seleccion

     if (isset($_POST["hotel"]))

     { $_GET['sucursal'] = $_POST["hotel"]; }

 }

}

if (isset($_POST["hotel"])) {

    $_GET["sucursal"] = $_POST["hotel"];



}



# Verificacion de Precios Postdatado

$hora = time();

$dia = date("j",$hora);

$mes = date("n",$hora);

$year = date("Y",$hora);

$momento1 = mktime ( 0, 0, 1, $mes, $dia , $year);

$momento2 = mktime ( 11, 59,59, $mes, $dia , $year);



$sqlPost = sprintf("SELECT * FROM hesperia_post_precio

                WHERE fecha_precio >= '$momento1'

                && fecha_precio <= '$momento2' && procesado = '0'");

$query= QUERYBD($sqlPost,$hostname,$user,$password,$db_name);

while ($rows   = mysqli_fetch_array($query,MYSQLI_ASSOC))

{	$id_hab = $rows["id_habitacion"];

 $sql = sprintf("UPDATE hesperia_post_precio

                SET procesado = '1'

                WHERE id_habitacion = '%s'",

                mysqli_real_escape_string($mysqli,$id_hab));

 $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);



 $precio = $rows["precio"];

 $precio_promocion = $rows["precio_promocion"];

 $habitacion_disponible_fecha = $rows["disponible_dia"];



 $NumHabRes = 0;

 $sqlRes = sprintf("SELECT id_habitacion FROM hesperia_reservaciones

                    WHERE desde = '$hora' && id_habitacion= '$id_hab'");

 $queryR= QUERYBD($sqlRes,$hostname,$user,$password,$db_name);

 while ($rows = mysqli_fetch_array($queryR,MYSQLI_ASSOC))

 { $NumHabRes++; }



 $habitacion_disponible_fecha = $rows["disponible_dia"] - $NumHabRes;



 $sql = sprintf("UPDATE hesperia_habitaciones SET

        precio = '%s',

        precio_promocion = '%s',

        disponibles = '%s'

        WHERE id_habitacion = '%s'",

                mysqli_real_escape_string($mysqli,$precio),

                mysqli_real_escape_string($mysqli,$precio_promocion),

                mysqli_real_escape_string($mysqli,$habitacion_disponible_fecha),

                mysqli_real_escape_string($mysqli,$id_hab));

 $result =  QUERYBD($sql,$hostname,$user,$password,$db_name);



}



$sql = "SELECT * FROM hesperia_settings WHERE id = '$sucursal' limit 1";

$result = QUERYBD($sql,$hostname,$user,$password,$db_name);

$rows   = mysqli_fetch_array($result,MYSQLI_ASSOC);

$data["id"] = $_SESSION["empresa"] = $rows["id"];

$data["nombre_sitio"] = $_SESSION["nombre_sitio"] = utf8_encode($rows["nombre_sitio"]);

$data["descripcion"] = utf8_encode($rows["descripcion"]);

$data["dominio"] = $_SESSION["dominio"] = $rows["dominio"];

$data["correo_contacto"] = $_SESSION["correo_contacto"] = $rows["correo_contacto"];

$data["correo_envio"] = $rows["correo_envio"];

$data["razon_social"] = $rows["razon_social"];

$data["keywords"] = utf8_encode($rows["keywords"]);

$data["description"] = utf8_encode($rows["description"]);

$data["pais"] = $rows["pais"];

$data["ciudad"] = $rows["ciudad"];

$data["estado"] = $estados = $rows["estado"];

$data["telefono"]  = $rows["telefonos"];

$data["rif"] = $rows["rif"];

$data["latitud"] = $_SESSION["latitud"] =  $rows["latitud"];

$data["longitud"] = $_SESSION["longitud"] = $rows["longitud"];

$data["direccion"] = utf8_encode($rows["direccion"]);

$data["codigo_postal"] = $rows["postal"];

$data["quienes"] = utf8_encode($rows["quienes"]);

$data["url_cuenta_twitter"] = $rows["twitter"];

$data["url_cuenta_facebook"] = $rows["facebook"];

$data["url_cuenta_instagram"] = $rows["instagram"];

$data["url_cuenta_google"] = $rows["gplus"];

$data["url_cuenta_youtube"] = $rows["youtube"];

$data["url_cuenta_pinterest"] = $rows["pinterest"];

$data["url_cuenta_linkedin"] = $rows["linkedin"];

$data["telf_reserva"] = $rows["telf_reserva"];

$data["salones"] = $rows["salones"];

$data["eventos"] = $rows["eventos"];

$data["escapadas"] = $rows["escapadas"];

$data["experiencias"] = $rows["experiencias"];

$data["mascotas"] = $rows["mascotas"];

$data["wifi"] = $rows["wifi"];

$data["piscina"] = $rows["piscina"];

$data["botones"] = $rows["botones"];

$data["estacionamiento"] = $rows["estacionamiento"];

$data["gimnasio"] = $rows["gimnasio"];

$data["spa"] = $rows["spa"];

$data["accesibilidad"] = $rows["accesibilidad"];

$data["helipuerto"] = $rows["helipuerto"];

$data["transporte"] = $rows["transporte"];

$data["vigilancia_privada"] = $rows["vigilancia_privada"];

$data["cafetin"] = $rows["cafetin"];

$data["restaurant"] = $rows["restaurant"];

$data["bar"] = $rows["bar"];

$data["discotheca"] = $rows["discotheca"];

$data["serie"] = 'HESPERIA-2015';

$data["cant_habitaciones"] = $rows["cant_habitaciones"];

$data["ninera"] = $data["nana"] = $rows["nana"];

$data["tienda"] = $rows["tienda"];

$data["logo"] = $rows["logo"];

$data["golf"] = $rows["golf"];

$data["tenis"] = $rows["tenis"];

$data["basket"] = $rows["basket"];

$data["parque"] = $rows["parque"];

$data["terraza"] = $rows["terraza"];

$data["tripadvisor"] = $rows["tripadvisor"];

$data["tripadvisor_premio"] = $rows["tripadvisor_premio"];

$data["check_in"] = $rows["check_in"];

$data["check_out"] = $rows["check_out"];



$sql = sprintf("SELECT minima_edad_nino FROM hesperia_porcentage_hab

                        WHERE id_hotel = '$data[id]'");

$result= QUERYBD($sql,$db["hostname"],$db["user"],$db["password"],$db["db_name"]);

@$rows = mysqli_fetch_array($result,MYSQLI_ASSOC);

$data["minima_edad_nino"] = $rows["minima_edad_nino"];



# Eliminacion de Sessiones previas

$ahora = time();

$tok = md5($ahora);

$hora = $ahora - 28800;

$sql = sprintf("DELETE FROM hesperia_sesion WHERE fecha <='$hora'");

$result= QUERYBD($sql,$db["hostname"],$db["user"],$db["password"],$db["db_name"]);



global $palabras;

$GLOBALS['palabras']=array(

    ' a ',' ah ',' al ',' alla ',' alo ',' ano ',' ante ',' anti ',' am ',' aquel ',' aquellos ',' aquellas ',

    ' atras ',' ay ',' ahora ',' are ',' anos ',' ano ',

    ' b ',' bajo',' bien',' bueno ',

    ' cada',' casi ',' con ',' contra ',' coma ',' comer ',' como ',' cómo ',' cambiar ',' cuyo ',' .com ',' com ',

    ' da ',' dando ',' de ',' del',' dejar ',' desde ',' di ',' dia ',' dice ',' donde ',' dijo ',' día ',

    ' e ',' el ',' ella ',' ellas ',' ello ',' ellos ',' en ',' entonces ',' entre ',' era ',' eran ',' es ',

    ' esa ',' ese ',' esas ',' eso ',' esos ',' esta ',' estan ',' estas ',' esto ',' estos ',' está ',' eramos ',

    ' fue ',' fueron ',' fuese ',' fuesen ',' fui ',' fuimos ','full ',' for ',

    ' gran ',' grande ','gracias ',

    ' ha ',' halla ',' hallar ',' hasta ',' haya ',' hayan ',' hayamos ',' hayaron ',' hubo ',

    ' hablemos ',' hablar ',

    ' i ',' iban ',' idem ',' ido ',' in ',' ir ',' irian ',' item ',' is ',

    ' ja ',' jamas ',' je ',' ji ',' jo ',' juntos ',

    ' kill ',

    ' la ',' las ',' lanza ',' le ',' les ',' lo ',' los ',

    ' mas ', ' matar ',' me ',' mes ',' mejor ',' mi ',' mias ',' mios ',' mis ',' mucho ',

    ' nada ',' nadie ',' ni ',' ninguno ',' no ',' nos ',' nosotros ',' numero ',' numeros ',

    ' o ',' ok ',' on ',' otras ',' otros ',

    ' para ',' paran ',' parte ',' peor ',' pm ',' por ',' poco ',' podran ',' pondra ',' pondran ',' porque ',

    ' puede ',' pero ',' puedo ',' pueden ',' puedes ',' pudo ',' punto ',

    ' que ',' quien ',' quienes ',' quiere ',' quieren ',' quiero ',' quisiera ',' quisieran ',' quisieras ',' quiso ',

    ' se ',' sera ',' si ',' sin ',' sobre ',' solo ',' sólo ',' su ',' sus ',' ser ',

    ' t ',' tambien ',' te ',' tenia ',' the ',' tiene ',' tienen ',' to ',' toda ',' todas ',' tras ',' tu ',

    ' tus ',' tuyas ',' tuyos ',' tuvo ',

    ' u ', ' un ',' una ',' unas ',' uno ',' unos ',

    ' v ',' vamos ',' van ',' viene ',' vienen ',' vosotros ',' vive ',' voy ',' vuelve ',' va ',

    ' y ',' ya ',' yo ',

    '/','#','&','(',')','.',',',';',':','-','*','{','}','[',']','<','>','$','%','=','@','?','!','"','+','¿',

    '|','“','...','¡',' - ','–','”',

    '1','2','3','4','5','6','7','8','9','0');



function BORRAR_VARIABLES()

{ $_GET=$_POST=array();

 unset($_POST,$_GET);

 return;

}



function BUFFER_INICIO($buffer)

{ if (!empty($buffer)) { $buffer=ob_start($buffer); }

 else { $buffer=ob_start(); }

 return $buffer;

}



function BUFFER_FIN($mysqli)

{ ob_flush();

 BORRAR_VARIABLES();

 mysqli_close($mysqli);

 return;

}



function COMPRESS_PAGE($buffer)

{ $search =$replace=array();

 $search =array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');

 $replace=array(">","<",'\\1');

 return trim(preg_replace($search,$replace,$buffer));

}



function CREAR_TOKEN($TokenForm)

{ $token=md5(uniqid(microtime(),true));

 $token_time=time();

 $_SESSION['csrf'][$TokenForm.'_token']=array('token'=>$token,'time'=>$token_time);

 return $token;

}



function DECODE($origen) {

    #$origen=html_entity_decode($origen,ENT_QUOTES,"UTF-8");

    #$origen=htmlspecialchars($origen,ENT_QUOTES,"UTF-8");

    return $origen;

}



function DETECTAR(){

    $List_User_Agent = array(

        'android' => 'android',

        'AvantGo' => 'AvantGo',

        'BlackBerry' => 'BlackBerry',

        'blazer' => 'blazer',

        'hiptop' => 'hiptop',

        'Cellphone' => 'Cellphone',

        'EudoraWeb' => 'EudoraWeb',

        'IEMobile' => 'IEMobile',

        'iPhone' => 'iPhone',

        'iPod'   => 'iPod',

        'Googlebot-Mobile' => 'Googlebot-Mobile',

        'GoogleMobilizer'=>'Google Wireless Transcoder',

        'KYOCERA' => 'KYOCERA',

        'LGE' => 'LGE',

        'midp' => 'midp',

        'Minimo' => 'Minimo',

        'NetFront' => 'NetFront',

        'Nintendo' => 'Nintendo',

        'Nitro' => 'Nitro',

        'Nokia' => 'Nokia',

        'Motorola' => 'mot-',

        'OperaMini' => 'Opera Mini',

        'Palm' => 'Palm',

        'PDA' => 'PDA',

        'PIE4' => 'compatible; MSIE 4.01; Windows CE; PPC; 240x320',

        'PIE4_Smartphone' => 'compatible; MSIE 4.01; Windows CE; Smartphone;',

        'PIE6' => 'compatible; MSIE 6.0; Windows CE;',

        'Plucker' => 'Plucker',

        'PPC' => 'PPC',

        'PlayStation' => 'PlayStation',

        'proxinet' => 'proxinet',

        'SHARP-TQ-GX10' => 'SHARP-TQ-GX10',

        'SHG-i900' => 'SHG-i900',

        'Skweezer'=>'.NET CLR 1.1.4322',

        'Smartphone' => 'Smartphone',

        'SonyEricsson' => 'SonyEricsson',

        'symbian' => 'symbian',

        'vodafone' => 'vodafone',

        'webOS' => 'webOS',

        'WindowsMobile' => 'Windows CE',

        'WinWAP' => 'WinWAP',

        'WX310K' => 'WX310K'

    );

    function browser($lista_user_agents, $user_agents_capturado){

        foreach($lista_user_agents as $navegador => $user_agent_rev) {

            if(stristr($user_agents_capturado, strtolower($user_agent_rev))!==false){

                return 1;

            }

        }

        return 0;

    }

}



function ENCABEZADO($lenguaje,$timezone_set,$charset){

    global $ExpStr;

    setlocale(LC_TIME,$lenguaje);

    header('Accept-Ranges:bytes');

    $tiempo= $_SERVER['REQUEST_TIME'] + 28800;

    $ExpStr='Expires:'.gmdate("D,d M Y H:i:s",$tiempo)." GMT";

    session_cache_limiter('private_no_expire');

    session_cache_expire(28800);

    header($ExpStr);

    header("Cache-Control:maxage=$tiempo");

    header("Cache-Control:public,must-revalidate");

    header("Cache-Control:public");

    header("pragma:public");

    header("Content-Transfer-Encoding:gzip;q=1.0,identity;q=0.5,*;q=0");

    header("Cache-Control:cache");

    header("Pragma:cache");

    header("Content-Type:text/html; charset=$charset");

    $etag=md5($_SERVER['REQUEST_URI'].$ExpStr);

    header("Etag:$etag");

    return $ExpStr;

}



function ERROR($hostname,$user,$password,$db_name)

{ echo '<div class="text-center err">

    <i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

    <h2>Ha solicitado una acci&oacute;n no valida o su sesi&oacute;n expir&oacute;, por favor haga click <a href="https://hoteleshesperia.com.ve">aquí</a> para regresar a la página principal</h2>

</div>';

 return;

}



function FSPATH($str) {

    $search=array('&lt;','&gt;','&quot;','&amp;');

    $str=str_replace($search,'',$str);

    $search=array('&aacute;','&Aacute;','&eacute;','&Eacute;','&iacute;','&Iacute;',

                  '&oacute;','&Oacute;','&uacute;','&Uacute;','&ntilde;','&Ntilde;');

    $replace=array('a','a','e','e','i','i','o','o','u','u','n','n');

    $search=array('','','','','','','','','','','','','','','_','-');

    $replace=array('a','e','i','o','u','a','e','i','o','u','u','u','n','n',' ',' ');

    $str=str_replace($search,$replace,$str);

    $str=preg_replace('/&(?!#[0-9]+;)/s','',$str);

    $search=$GLOBALS['palabras'];

    $str=str_replace($search,' ',strtolower($str));

    $str=str_replace($search,$replace,strtolower(trim($str)));

    $str=preg_replace("/[^a-zA-Z0-9\s]/",'',$str);

    $str=preg_replace('/\s\s+/',' ',$str);

    $str=str_replace(' ','-',$str);

    return substr($str,0,40);

}



function GENERAR_CODIGO()

{ $letra=array('A','B','C','D','E','F','G','H','I','J');

 $micro= microtime(true);

 $codigo='CC-'.rand (0,9).'-'.$letra[rand (0,9)].'-'.substr(str_replace('.','X',$micro),-4);

 return $codigo;

}



function GOOGLE_ANALYTICS($ganalitycs)

{ ?>

<script type="text/javascript">

$(document).ready(function() {

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

                            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '<?php echo $ganalitycs; ?>', 'auto');

    ga('send', 'pageview');

});

</script>

<?php

 return; }



function getCleanParselyPageValue($val) {

    $val=str_replace("\n","",$val);

    $val=str_replace("\r","",$val);

    return $val;

}



function LIMPIAR_VALORES()

{ $_SERVER['QUERY_STRING']=trim(strip_tags($_SERVER['QUERY_STRING']));

 URL();

 if (!empty($_GET)){

     foreach($_GET as $variable=>$valor){

         $_GET[$variable]=$_GET[$variable];

         $_GET[$variable]=str_replace("'","\'",$_GET[$variable]);

         $_GET[$variable]=DECODE($_GET[$variable]);

         $_GET[$variable]=filter_var(trim($_GET[$variable]),FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);

     }}

 if (!empty($_POST) ){

     foreach($_POST as $variable=>$valor){

         $_POST[$variable]=$_POST[$variable];

         $_POST[$variable]=str_replace("'","\'",$_POST[$variable]);

         $_POST[$variable]=DECODE($_POST[$variable]);

     }}

 return;

}



function META($charset,$lenguaje,$timezone_set,$region,$go,$data,$hostname,$user,$password,$db_name,$mysqli)
{ $enlace= 'https://'.$_SESSION["dominio"];
 $tags=array();
 echo '
    <link rel="alternate" type="application/rss+xml" title="RSS" href="backend.php"/>
    <link rel="copyright" href="index.php?go=politicas"/>
    <link rel="apple-touch-icon" sizes="152x152" href="img/iconos-apple/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/iconos-apple/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/iconos-apple/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/iconos-apple/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/iconos-apple/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/iconos-apple/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/iconos-apple/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="img/iconos-apple/apple-touch-icon-57x57.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/iconos-apple/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/iconos-apple/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/iconos-apple/favicon-16x16.png">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="dns-prefetch" href="'.$enlace.'"/>
    <link rel="dns-prefetch" href="https://www.gstatic.com"/>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com"/>
    <link rel="dns-prefetch" href="https://fonts.googleapis.com"/>
    <link rel="dns-prefetch" href="https://accounts.google.com"/>
    <link rel="dns-prefetch" href="https://ssl.google-analytics.com"/>
    <link rel="dns-prefetch" href="https://google-analytics.com"/>
    <link rel="dns-prefetch" href="https://googleads.g.doubleclick.net"/>
    <link rel="dns-prefetch" href="https://pagead2.googlesyndication.com"/>
    <link type="text/plain" rel="socialmedia" href="'.$enlace.'/socialmedia.txt"/>
    <meta http-equiv="content-type" content="text/html;">

    <meta charset="UTF-8">
    <meta name="keywords" content="hoteles en playa el agua,wtc hesperia,isla margarita todo incluido,hospedaje margarita,paquetes a isla margarita,mejores hoteles de valencia,hospedaje en margarita economicos,hesperia de valencia,hesperia playa,hotel todo incluido margarita,hotel margarita resort,hesperia eden club,hotel margarita todo incluido,valencia hoteles,hotel playa el agua isla margarita,playa el agua en margarita,hoteles de margarita playa el agua,paquete a isla margarita,mejor hotel de venezuela,paquetes a margarita todo incluido,hotel en margarita playa el agua,vacaciones isla margarita,hoteles todo incluido en margarita,todo incluido margarita,hoteles en margarita todo incluido 2015,paquetes de hoteles en margarita,los mejores hoteles de venezuela,hesperia el agua,margarita todo incluido,hotel lagunama,paquetes en margarita,turismo en venezuela,mejor hotel de valencia,resort en margarita todo incluido,paquete margarita,el mejor hotel de margarita,hoteles playa el agua,playa agua,hoteles todo incluido margarita,hoteles en isla margarita todo incluido,hesperia edén club,hesperia venezuela,hotel todo incluido en margarita,hoteles isla margarita 5 estrellas,playa el agua,hoteles valencia,hesperia isla,hoteles en margaritas,hotel lido margarita,hoteles en margarita todo incluido playa el agua,hesperia playa el agua,los mejores hoteles,isla margarita resort,playa el agua isla de margarita,posadas en margarita,hotell valencia,hotel en playa el agua,hotel margarita international resort,alojamientos en valencia,hoteles valencia venezuela,el mejor hotel de valencia,playa el agua isla margarita,hoteles en playa el agua todo incluido,hoteles isla margarita todo incluido,vuelos a isla margarita,hoteles en margarita playa el agua,reservaciones en margarita,vacaciones venezuela,pasajes a isla margarita,hotel en valencia,resorts en margarita,hotels valencia,paquetes todo incluido isla margarita,margarita island hotels,hotel bella vista,hesperia isla de margarita todo incluido,hesperia valencia reservaciones,vacaciones margarita,el agua venezuela,eventos en el hesperia valencia,hotel wtc valencia,hoteles en valencia venezuela,hoteles en margarita paquetes,mejor hotel en margarita,playa el agua venezuela,vacaciones en isla margarita,resort isla margarita,hoteles nuevos en margarita,paquetes en margarita todo incluido,pasajes isla margarita,mejor hotel margarita,venezuela playa el agua,agua de playa,hotel portofino,hoteles de valencia,hoteles en margaritas todo incluido,hoteles en margarita,hoteles margarita venezuela todo incluido,paquetes a margarita,hoteles en margarita playa el agua todo incluido,isla margarita playa el agua,hotel em valencia,vacaciones en margarita todo incluido 2015,hoteles en margarita todo incluido,mejores hoteles en valencia,island margarita,hotel hisperia margarita,isla margarita paquetes 2016,hesperia wtc valencia,hotel margarita,mejores hoteles de margarita,viajar a isla margarita,hoteles cerca de valencia,hoteles margarita todo incluido 2015,wtc valencia,hotel laguna mar,hesperia valencia venezuela,hotel em margarita,mejores hoteles de venezuela,hotel playa el agua margarita paquetes,hospedaje en valencia,paquetes margarita todo incluido,todo incluido isla margarita,mejores hoteles margarita,margarita international resort,playa el agua margarita,lagunamar isla margarita,esperia margarita,hotel lagunamar margarita,hesperia marcaibo,hotel playa el agua,isla margarita resorts,hotel playa el agua margarita,alojamiento en valencia,hoteles de margarita todo incluido,hesperia wtc maracaibo,el mejor hotel de venezuela,hesperia en valencia,hesperia wtc,alojamiento en margarita,hoteles en margarita cerca de playa el agua,paquetes para margarita todo incluido,hospedaje en margarita,paquetes margarita,lido hotel margarita,viajes a margarita todo incluido,sitios turisticos en margarita,hotel ikin margarita,mejores hoteles en venezuela,hesperia isla margarita,hoteles en margarita con todo incluido,playa el agua hoteles,margarita playa el agua,hotel de valencia,hotel en margarita todo incluido,margarita hoteles,paquetes a la isla margarita,reservar hotel en margarita,hesperia isla bonita,todo incluido en margarita,margarita club,hoteles todo incluido isla margarita,hoteles playa el agua todo incluido,playa agua margarita,paquetes de viajes a margarita,resort en margarita,hoteles en margarita 5 estrellas,islas margaritas todo incluido,hospedajes en margarita,eventos hesperia valencia,venezuela turismo,hospedaje en margarita todo incluido,hoteles isla de margarita todo incluido,club margarita,hotel cerca de valencia,hoteles margarita todo incluido,margarita todo incluido 2015,vacaciones en margarita todo incluido,isla margarita 2016 paquetes,hotels margarita island,paquetes todo incluido margarita,valencia margarita,venezuela vacaciones,margarita hoteles todo incluido,hoteles cerca de playa el agua,reservaciones margarita,hesperiawtc,viajes isla margarita todo incluido,wtc hesperia valencia,paquetes isla margarita,mapa isla margarita,hotels in margarita island,hoteles 5 estrellas isla margarita,hoteles 5 estrellas en margarita,hotel marina bay margarita,isla de margarita playa el agua,isla margarita paquete,hotel en margarita,hesperia margarita todo incluido,hotel playa el agua margarita todo incluido,hoteles playa el agua margarita,islas margaritas paquetes,mejor hotel valencia,hoteles 5 estrellas margarita,hoteles en valencia,hesperia valencia eventos,isla de margarita todo incluido,hoteles y posadas en margarita,hoteles margarita playa el agua,vacaciones en margarita,hotel esperia margarita,posadas en playa el agua,hotel bella vista margarita,isla margarita paquetes 2015,hôtel valencia,margarita paquetes,mejores hoteles valencia,hotel margarita playa el agua,islas margarita venezuela todo incluido,hoteles de margarita 5 estrellas,hesperia playa agua,promociones isla margarita,hoteles de valencia venezuela,viaje isla margarita todo incluido,paquetes turisticos margarita todo incluido,hotel playa del agua isla margarita,los mejores hoteles en margarita,eventos margarita,mejores hoteles en margarita,hotel valencia venezuela,posadas en margarita todo incluido,hotel 5 estrellas en margarita,los mejores hoteles de margarita,playa del agua,hotel en valencia venezuela,hoteles margarita,margarita island hotel,paquete isla margarita,mejor hotel de margarita,isla margarita hoteles 5 estrellas,playa de agu,hotels in valencia venezuela,valencia venezuela hoteles,playa el agua isla margarita venezuela,viajes a margarita,hotel dunes,isla margarita paquetes,los mejores hoteles de valencia,mejores hoteles en margarita todo incluido,hoteles de playa el agua,hotel costa caribe margarita,margarita islands,margarita hotels,mejores hoteleshesperia valencia,keyword,keyword,WOT_keyword'.$data["serie"].'"/>

    <meta http-equiv="X-UA-Compatible" content="chrome=1,IE=edge"/>
    <meta http-equiv="pragma" content="cache"/>
    <meta http-equiv="cache-control" content="cache"/>
    <meta http-equiv="vary" content="content-language"/>
    <meta name="owner" content="'.$enlace.'"/>
    <meta name="resource-type" content="document"/>
    <meta name="robots" content="index,follow"/>
    <meta name="author" content="Hoteles Hesperia Venezuela"/>
    <meta name="copyright" content="CopyLeft (c) '.date("Y",time()).' HOTELES HESPERIA VENEZUELA"/>
    <meta name="revisit-after" content="7 days"/>
    <meta name="revisit" content="7"/>
    <meta name="distribution" content="Global"/>
    <meta name="generator" content="Aptana"/>
    <meta name="rating" content="General"/>
    <meta name="country" content="'.$data['pais'].'"/>
    <meta name="language" content="es_VE"/>
    <meta name="adblock" content="disable"/>
    <meta name="dc.date" content="'.date("Y-m-d",$_SERVER['REQUEST_TIME']).'"/>
    <meta name="dc.format" content="text/html"/>
    <meta name="dc.language" content="es_VE"/>
    <meta name="geo.region" content="'.$region.'"/>
    <meta name="geo.placename" content="Venezuela"/>
    <meta name="geo.position"  content="8.0000000;-66.0000000"/>
    <meta name="icbm" content="8.0000000;-66.0000000"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta name="no-email-collection" content="http://www.unspam.com/noemailcollection/"/>
    <meta name="medium" content="mult"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="msapplication-starturl" content="'.$enlace.'"/>
    <!-- <meta name="msapplication-window" content="width=1024;height=768"/> -->
    <meta name="msapplication-task" content="name=HOTELES HESPERIA VENEZUELA – RESERVA ONLINE;action-uri='.$enlace.'/;icon-uri=images/favicon.ico"/>
    <meta name="msapplication-TileImage" content="img/iconos-apple/apple-touch-icon-144x144.png"/>
    <meta name="msapplication-TileColor" content="#1F1F21"/>
    <meta name="theme-color" content="#ffffff/">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320"/>

    ';
 if ($_GET['go'] != 'prensa')
 { 
    /*
    echo '
        <meta name="twitter:title" content="HOTELES HESPERIA VENEZUELA – RESERVA ONLINE "/>
        <meta property="og:description" content="'.$data['description'].'"/>
        <meta name="twitter:description" content="'.$data['description'].'"/>
        <meta name="twitter:url" content="'.$enlace.'/"/>
        <link rel="canonical" href="'.$enlace.'"/>';
        */
         }
 else {
/*     $id = trim($_GET["id"]);
     if ($id >=1) {
         $sql=sprintf("SELECT noticia_id,titulo,imagen FROM hesperia_noticias WHERE noticia_id = '%s'",
                      mysqli_real_escape_string($mysqli,$id));
         $result=QUERYBD($sql,$hostname,$user,$password,$db_name);
         $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
         $titulo = utf8_encode($rows["titulo"]);
         $imagen = $rows["imagen"];
         $enlace=$enlace.'/index.php?go=0/prensa/'.$id.'/'.FSPATH($titulo);
         $titulo = $titulo.' - '.$data['valor'];
         $data['description'] =  $titulo.' - '.$data['description'];
         echo '
    <meta name="twitter:title" content="'.$data['nombre_sitio'].' - '.$titulo.'"/>
    <meta property="og:description" content="'.$data['description'].'"/>
    <meta name="twitter:description" content="'.$data['description'].'"/>
    <meta name="twitter:url" content="'.$enlace.'"/>
    <meta property="og:image" content="'.$enlace.'/img/prensa/'.$imagen.'"/>
    <meta name="twitter:imagen" content="'.$enlace.'/img/prensa/'.$imagen.'"/>
    <link rel="canonical" href="'.$enlace.'"/>';    }
    */

 }
// include_once("funciones.php");
$slug =basename($_SERVER['REQUEST_URI']);
$dir = dirname($_SERVER['REQUEST_URI']);
//echo "go tiene-->$go";
 titulos_descripciones($slug, $data, $dir);
?>
<!--[if lt IE 7]>
<script  async type="text/javascript" src="js/ie6_script_other.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script  async  src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js" type="text/javascript"></script>
<![endif]-->
<!-- [if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<?php
 return $tags; 
}


function NADA($texto)

{ echo '<div class="text-center">

<i class="fa fa-exclamation-triangle fa-5 exclamation"></i>

<h2>Alerta</h2> No hay contenido para mostrar... Por favor haga click, <a href="https://hoteleshesperia.com.ve" title="Regresar a la pagina principal">para regresar</a>.</div>';

 return;

}



function NORMAL_MYSQL($fecha)

{ $dia  = substr($fecha,0,2);

 $mes  = substr($fecha,3,2);

 $year = substr($fecha,6);

 return mktime ( 10, 1,1, $mes, $dia, $year);

}



function SHARE()

{ ?> <div style="text-align:left; margin-top:5px; margin-bottom:10px; ">

<div class="addthis_toolbox addthis_default_style addthis_32x32_style">

    <a class="addthis_button_preferred_1"></a>

    <a class="addthis_button_preferred_2"></a> <a class="addthis_button_preferred_3"></a>

    <a class="addthis_button_preferred_4"></a> <a class="addthis_button_compact"></a>

    <a class="addthis_counter addthis_bubble_style"></a>

</div>

<script type="text/javascript" defer="defer">var addthis_config = {"data_track_addressbar":true};</script>

<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5095f30841b2c8f8" defer="defer"></script>

</div>

<?php

    return;

}





function SELECTOR($go,$hostname,$user,$password,$db_name,$data,$mysqli){

    switch($go){

        /*case 'index':

            INDEX($mysqli,$data,$hostname,$user,$password,$db_name);

            break;*/
        case 'index':
            include_once 'funciones_v2.php';
            //INDEX($mysqli,$data,$hostname,$user,$password,$db_name);
            titulos_index($hostname,$user,$password,$db_name);
            $array = cargar_items_index($hostname,$user,$password,$db_name);
            mostrar_paquetes($array);
            habitaciones_index($array);
            redes_sociales();
            break;

        case 'aperturas':

            APERTURAS($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'boletines':

            BOLETINES($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'contacto':

            FORM_CLIENTE($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'compra':

            DATOS_COMPRA($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'detalle':

            DETALLE_HOTEL($mysqli,$hostname,$user,$password,$db_name,$data);

            break;

            //        case 'Detalles':

            //        DETALLES_SERVICIOS($mysqli,$hostname,$user,$password,$db_name,$data);

            //        break;

        case 'experiencias':

            EXPERIENCIAS($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'eventos':

            EVENTOS_HOTELES_GENERAL($mysqli,$data,$hostname,$user,$password,$db_name);

        case 'eventos_wtc':

            $data["id"] = 1;

            LISTANDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'eventos_him':

            $data["id"] = 2;

            LISTANDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'eventos_hpa':

            $data["id"] = 3;

            LISTANDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'eventos_eden':

            $data["id"] = 4;

            LISTANDO_EVENTOS($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'eventos_hesperia':

            LISTANDO_EVENTOS_HOTELES($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'ingresar':

            INICIO_SESSION($data,$hostname,$user,$password,$db_name);

            break;

        case 'gastronomia':

            $sql = sprintf("SELECT * FROM hesperia_servicios WHERE  id_hotel = '%s' limit 1",

                           mysqli_real_escape_string($mysqli,$data["id"]));

            $result = QUERYBD($sql,$hostname,$user,$password,$db_name);

            $rowServicio=mysqli_fetch_array($result,MYSQLI_ASSOC);

            LISTANDO_RESTAURANT($mysqli,$data,$hostname,$user,$password,$db_name,$rowServicio);

            break;

        case 'nosotros':

            if ($_GET['principio'] == 0) {

                NOSOTROS_PRINCIPAL($mysqli,$hostname,$user,$password,$db_name); }

            else { NOSOTROS($data,$hostname,$user,$password,$db_name); }

            break;

        case 'paquetes':

            $data["id_paquete_promo"] = $_GET["id"];

            $data["temporada"] = 0;

            if(isset($_GET["id"]) && $_GET["id"] > 0){
                PAQUETE($mysqli,$data,$hostname,$user,$password,$db_name);
            }else{
                PAQUETES_GENERAL($mysqli,$data,$hostname,$user,$password,$db_name);
            }            

            break;

        case 'nuevos_paquetes':

            $data["id_paquete_promo"] = 0;

            $data["temporada"] = $_GET["id"];

            PAQUETES_GENERAL($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'partai':

            PARTAI($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'perfil':

            if (!empty($_SESSION["referencia"])) {

                PERFIL($mysqli,$data,$hostname,$user,$password,$db_name); }

            else { ERROR($hostname,$user,$password,$db_name);}

            break;

        case 'politicas':

            TERMINOS($data['nombre_sitio'],$data['ciudad'],$data['estado'],$data['pais']);

            break;

        case 'prensa':

            MEDIACENTER($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'registro':

            if (empty($_SESSION["referencia"]))

            { REGISTRO($hostname,$user,$password,$db_name,$data); }

            break;

        case 'reserva':

            PROCESO_RESERVA($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'textos':

            TEXTOS($data,$hostname,$user,$password,$db_name,$mysqli);

            break;

        case 'trabajo':

            TRABAJA($mysqli,$hostname,$user,$password,$db_name,$data);

            break;

        case 'salir':

            if (!empty($_SESSION["referencia"]))

            { SALIDA($hostname,$user,$password,$db_name); }

            break;

        case 'ubicacion':

            UBICACION($hostname,$user,$password,$db_name,$data);

            break;

        case 'reservapaquete':

            DATOS_COMPRA_PAQUETES($mysqli,$data,$hostname,$user,$password,$db_name);

            break;

        case 'vip':

            ACCESO_VIP($data,$hostname,$user,$password,$db_name);

            break;

        case 'accesoVip': 

            $clave = $_POST["clave"];

            $sql=sprintf("SELECT * FROM hesperia_v2_acceso_vip WHERE codigo = '%s' AND status = 'S'",

                       mysqli_real_escape_string($mysqli,$clave));

            $result=QUERYBD($sql,$hostname,$user,$password,$db_name);

            if($rows = mysqli_fetch_array($result,MYSQLI_ASSOC)){

                PARTAI_VIP($mysqli,$data,$hostname,$user,$password,$db_name, $clave);

            }else{

                ERROR($hostname,$user,$password,$db_name);

            }

            break;

        default:

            ERROR($hostname,$user,$password,$db_name);

            break;

    }

    return;

}



function TERMINOS($nombre_sitio,$ciudad,$estado,$pais)

{ include 'include/politicas.php';

 return;

}



function TITULOS($data,$title){

    if  ($title == 'INDEX') {$row = $data;}

    else { $row = $data.' - '.$title; }

    echo $row;

    return $row;

}



function unhtmlentities($cadena)

{   $search = array("&lt;","&gt;");

 $cambio= array('<','>');

 $trans_tbl = get_html_translation_table(HTML_ENTITIES);

 $trans_tbl = array_flip($trans_tbl);

 $cadena = strtr($cadena, $trans_tbl);

 $cadena = str_replace($search,$cambio,$cadena);

 return $cadena;

}



function URL() {

    if (empty($_SERVER["HTTP_REFERER"])) { $_SERVER["HTTP_REFERER"]='';}

    $valor=strip_tags($_SERVER["HTTP_REFERER"]);

    $replace="%20";

    $search=array(">","<","|",";","-","'",'"');

    $_SERVER["HTTP_REFERER"]=str_replace($search,$replace,$valor);

    return;

}



function VERIFICA_EMAIL($email){

    $email= strtolower(trim($email));

    $mail_correcto=0;

    if ((strlen($email)>=6) && (substr_count($email,"@")==1) &&

        (substr($email,0,1) !="@") && (substr($email,strlen($email)-1,1) !="@")){

        if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) &&

            (!strstr($email,"\$")) && (!strstr($email," "))) {

            if (substr_count($email,".")>=1){

                $term_dom=substr(strrchr ($email,'.'),1);

                if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){

                    $antes_dom=substr($email,0,strlen($email) - strlen($term_dom) - 1);

                    $caracter_ult=substr($antes_dom,strlen($antes_dom)-1,1);

                    if ($caracter_ult !="@" && $caracter_ult !="."){

                        return 1;

                    }

                }

            }

        }

    }

    return 0;

}



function VERIFICA_TOKEN($TokenForm,$token)

{ if(!isset($_SESSION['csrf'][$TokenForm.'_token'])) {

    return false; }

 if ($_SESSION['csrf'][$TokenForm.'_token']['token'] !==$token) {

     return false; }

 return true;

}

?>

