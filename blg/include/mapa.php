<?php
session_start(); 
header("Cache-Control:public,must-revalidate");
header("Content-Transfer-Encoding:gzip;q=1.0,identity;q=0.5,*;q=0");
$ubicacion = $_SESSION["latitud"].','.$_SESSION["longitud"]; 
?>
<!doctype html>
<html><head>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<meta charset="utf-8">
	<style>html, body, #map-canvas { height:100%; margin: 0px;
	padding: 0px} </style>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script>
	var map;
	function initialize() {
		var mapOptions = {
		zoom: 17,
		center: new google.maps.LatLng(<?php echo $ubicacion; ?>) };
		map = new google.maps.Map(document.getElementById('map-canvas'),
		mapOptions); }
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
 </head>
<body>
<?php flush(); ?>
	<div id="map-canvas"></div>
</body></html>
