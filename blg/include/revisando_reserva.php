<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/

session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
	header("location:../error.html");
	die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);

# se usaran $_SESSION para la parte de confirmacion de reserva y pago

# arreglo
$id_habitacion = $_POST['habitacion']; # id de habitacion
$Cant_Hab_reservadas = $_POST['NumHab']; # cantidad de habitaciones
$capacidad = $_POST['capacidad']; # capacidad de habitacion
$disponible = $_POST['disponible']; # cant disponible habitacion
$mejor = $_POST['mejor']; # mejor precio

# normal
$id_hotel = $_POST['hotel']; # hotel reservacion
$ninos = $_POST['ninos']; # cantidad de niños en reserva
$adultos = $_POST['adultos']; # cantidad de adultos en reserva
$desdeReserva = $_POST['desde']; # fecha de comienzo de reserva
$hastaReserva = $_POST['hasta']; # fecha de fin de reserva
$cantPersonasReserva = $_POST['cantPersonasReserva'];
$dias_reservados = ($hastaReserva - $desdeReserva) / 86400;
# Incialiazacion de valores
$CapMaxHab = $diferencia = $PerHabReser = $ok = $cantHab = 0;

# Revision de habitaciones solicitadas contra la cantidad de personas
 foreach($Cant_Hab_reservadas as $key=>$value)
{	if ($value >= 1)
        {	$CapMaxHab = ($capacidad[$key] + $CapMaxHab) * $value;
            $PerHabReser = $value + $PerHabReser;
            $cantHab = $cantHab + $Cant_Hab_reservadas[$key];
            $ok = 1;
        }
}

# Fin Revision de habitaciones solicitadas contra la cantidad de personas

# No se eligio una cantidad de habitacion

if ($ok == 0)
{ echo '<div class="alert alert-danger" role="alert">
    <p>No ha indicado la cantidad de habitaciones que desea reservar.<br/>
    Intente de nuevo, en caso contrario contacte al Administrador del sitio
    </p></div>';
die();
}

# Fin No se eligio una cantidad de habitacion

# Se determina que la cantidad de personas en reserva no excede la
# cantidad permitida por habtaciones indicadas

if ($cantPersonasReserva > $CapMaxHab)
{ $diferencia = $cantPersonasReserva - $CapMaxHab;
  echo '<div class="alert alert-danger" role="alert">
			<p>Ha excedido la cantidad máxima de persona según la(s) habitación(es) solicitadas en reservación: 	Usted indicó  <strong>'.$cantPersonasReserva.'</strong>  personas y la(s) habitació(nes) que seleccionó la capacidad máxima es de <strong>'.$CapMaxHab.'</strong> personas.<br/>Intente de nuevo, en caso contrario contacte al Administrador del sitio</p>
			</div>';
die();
}

$promocion = $reservacion = '';

foreach($id_habitacion as $key=>$value)
{ $sql = sprintf("SELECT disponible_dia FROM hesperia_post_precio 
			WHERE id = '%s' && fecha_precio = '%s'",
             mysqli_real_escape_string($mysqli,$id_habitacion[$key]),
             mysqli_real_escape_string($mysqli,$desdeReserva));
  $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
  $rows = mysqli_fetch_array($result,MYSQLI_ASSOC);
  $nohay = 0;
  $Cant_Hab_reservadas = array_filter($Cant_Hab_reservadas, "strlen");
	if (isset($Cant_Hab_reservadas) && !empty($Cant_Hab_reservadas[$key])) {
	if ($Cant_Hab_reservadas[$key] > 0) 
        {	if ($rows["disponible_dia"] > $Cant_Hab_reservadas[$key])
				{ $nohay = 1;}
		#	if (!empty($mejor[$key])) {
		  #      $promocion .= $id_habitacion[$key].'-'.$Cant_Hab_reservadas[$key].'|';
		  #   }
			#else { 
				$reservacion .= $id_habitacion[$key].'-'.$Cant_Hab_reservadas[$key].'|';
				#}
			}
		}
}

if ($nohay == 1)
{   echo '<div class="alert alert-danger" role="alert">
			<p>No hay disponible habitaciones para la fecha(s) seleccionada(s).<br/>Intente de nuevo con otro rango de fechas, en caso contrario contactenos por favor!</p>
			</div>';
	die();
}

$chr = '';

# valores a pasar a la reservacion
#foreach ($Cant_Hab_reservadas as $value) {
#    $chr .= $value.'|';
#}

if (!isset( $_POST["CantninosHab"]))
{  $_POST["CantninosHab"] =array(); }

if (!isset( $_POST["CantninosHab"]))
{  $_POST["CantninosHab"] =array(); }

# cantidad de adultos por hab
$CantAdultosHab = $_POST["CantAdultosHab"];
# cantidad de ninos por hab
$CantninosHab = $_POST["CantninosHab"];

$hayAdultos = 0;
 foreach($CantAdultosHab as $key=>$value)
{ if ($value > 0) 
	{ $hayAdultos = $hayAdultos + $value; }
 }

if ($hayAdultos > $adultos)
{   echo '<div class="alert alert-danger" role="alert">
			<p>Error!. Usted indicó [ <strong>'.$adultos.' </strong>] Adultos, pero está indicando  en la elección [ <strong>'.$hayAdultos.'</strong> ] Adultos por favor corrija con los valores correctos!</p>
			</div>';
	die(); 
}

foreach($Cant_Hab_reservadas as $key=>$value)
{ if ($value > 0) 
	{  if ($CantAdultosHab[$key] == 0)
		{   echo '<div class="alert alert-danger" role="alert">
			<p>Error!. Usted ha indicado una Habitacion pero no selecciono la cantidad de adultos, por favor corrija con los valores correctos!</p>
			</div>';
	die();}
	}
}

# $_SESSION["Cant_Hab_reservadas"] = $chr;
#$_SESSION["mejorP"] = $promocion;

$desde = $_SESSION["desdeReserva"] = $desdeReserva;
$hasta = $_SESSION["hastaReserva"] = $hastaReserva;
$_SESSION["Cant_Hab_reservadas"] = $Cant_Hab_reservadas;
$_SESSION["id_hotel"] = $id_hotel;
$_SESSION["cantPersonasReserva"] = $cantPersonasReserva;
$_SESSION["adultos"] = $adultos;
$_SESSION["ninos"] = $ninos;
$_SESSION["id_habitacion"] = $reservacion;
$_SESSION["dias_reservados"] = $dias_reservados;
$_SESSION["cantHab"] = $cantHab;
$_SESSION["CantAdultosHab"] = $CantAdultosHab;
$_SESSION["CantninosHab"] = $CantninosHab;
$_SESSION["reservacion"] = $reservacion;

# limpiamos los item vacios 
$_SESSION["Cant_Hab_reservadas"] = array_filter($_SESSION["Cant_Hab_reservadas"], "strlen");
$_SESSION["CantAdultosHab"] = array_filter($_SESSION["CantAdultosHab"], "strlen");
$_SESSION["CantninosHab"] = array_filter($_SESSION["CantninosHab"], "strlen");

unset($result,$sql,$ahora,$chr);
$_POST = array();
$ahora = md5(time());
$enlace = 'index.php?go='.$id_hotel.'/compra/0/Proceso-Reserva-Compra-Pago&tok='.$ahora;
echo '<meta http-equiv="refresh" content="0; url='.$enlace.'"/>';
?>
