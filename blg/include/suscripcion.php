<?php
/* -------------------------------------------------------
Script  bajo los términos y Licencia
Apache License
Version 2.0, January 2004
https://www.apache.org/licenses/LICENSE-2.0
Autor:Hector A. Mantellini (Xombra)
--------------------------------------------------------*/
session_start();
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
    header("location:../error.html");
    die();}
$antesdecore = 1;
include 'databases.php';
$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);
unset($sql);
$ahora = time();
$email = strip_tags(strtolower(trim($_POST['email'])));
$email = filter_var($email,FILTER_SANITIZE_EMAIL);
$contacto = $_POST['contacto'];
$dominio  = $_POST['dominio'];
graba_LOG("Nuevo suscriptor en Boletines: $email",$email,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);
//$solicitud = "$apellidos $nombres";

    $revisa = sprintf("SELECT email
                       FROM hesperia_suscripciones
                       WHERE email = '%s'",
                       mysqli_real_escape_string($mysqli,$email));
    $resultRevisa = QUERYBD($revisa,$hostname,$user,$password,$db_name);
    $cantidad = mysqli_num_rows($resultRevisa);
if($cantidad == 1){
    echo '<br/> <div class="alert alert-success" role="alert">
                    <p>
                        Ya se encuentra registrado en el NewsLetter
                    </p>
                </div>';
}else{
    $sql = sprintf("INSERT INTO hesperia_suscripciones (id,email,fecha)
                            VALUES ( NULL,'%s','%s')",
                        mysqli_real_escape_string($mysqli,$email),
                        mysqli_real_escape_string($mysqli,$ahora));
//    die($sql);
    $result = QUERYBD($sql,$hostname,$user,$password,$db_name);
    if (mysqli_affected_rows($mysqli))
        { echo '<br/><div class="alert alert-success" role="alert">
            <p>Ha sido suscrito a nuestros Boletin. Estaremos informandolo vía email de todas nuestras ofertas!!!</p>
            </div>';
                $asunto  = "Suscripcion en $dominio";
                $mensaje = "Saludos: Sr(a):
                Ha sido suscrito a nuestros Boletin de $dominio.<br>\r\n
                Suemail es: $email<br>\r\n
                Cualquier duda contactenos al siguiente email: $contacto<br>
                ----------------------------------------------------<br>\r\n
                Este mensaje es privado y confidencial y solamente para la persona a la que va dirigido. Si usted ha recibido este mensaje por error, no debe revelar, copiar, distribuir o usarlo en ningún sentido. Le rogamos lo comunique al remitente y borre dicho mensaje y cualquier documento adjunto que pudiera contener. Los correos electrónicos no son seguros, no garantizan la confidencialidad ni la correcta recepción de los mismos, dado que pueden ser interceptados, manipulados, destruidos, llegar con demora, incompletos, o con virus. El emisor no se hace responsable de las alteraciones que pudieran hacerse al mensaje una vez enviado. En el caso de que el destinatario de este mensaje no consintiera la utilización del correo electrónico vía Internet, rogamos nos los haga saber.\r\n<br>
                ----------------------------------------------------<br>\r\n
                $dominio<br><br>";
                $headers = '';
                $headers .= "MIME-Version:1.0\r\n";
                $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                $headers .= "Received:from www.$dominio\r\n";
                $headers .= "X-Priority:3\r\n";
                $headers .= "X-MSMail-Priority:Normal\r\n";
                $headers .= "From: Hotel Hesperia <$contacto>\r\n";
                $headers .= "X-Mailer:$contacto\r\n";
                $headers .= "Return-path:$contacto\r\n";
                $headers .= "Reply-To:$contacto\r\n";
                $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
                @mail($email,$asunto,$mensaje,$headers);
                $headers = '';
                $headers .= "MIME-Version:1.0\r\n";
                $headers .= "Content-type:text/html; charset=UTF-8\r\n";
                $headers .= "Received:from www.$dominio\r\n";
                $headers .= "X-Priority:3\r\n";
                $headers .= "X-MSMail-Priority:Normal\r\n";
                $headers .= "From: Hotel Hesperia <$contacto>\r\n";
                $headers .= "X-Mailer:$contacto\r\n";
                $headers .= "X-Antiabuse:Enviar notificacion a $contacto\r\n";
                $headers .= "Reply-To:$email\r\n";
                $headers .= "Return-path:$email\r\n";
                $mensaje = "El usuario $email nuevo supscriptor a boletines,<br>
                            se ha registrado en el sitio $dominio.<br>";
                @mail($contacto,$asunto,$mensaje,$headers);
     } else
     { echo '<div class="alert alert-danger" role="alert">
          <p>Ha ocurrido un error inesperado. Probablemente ya la cuenta email esta registrada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>
        </div>'; }
    unset($result,$sql,$email,$headers);
    $_POST = array();
}



?>
