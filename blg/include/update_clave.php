<?php

/* -------------------------------------------------------

Script  bajo los términos y Licencia

Apache License

Version 2.0, January 2004

https://www.apache.org/licenses/LICENSE-2.0

Autor:Hector A. Mantellini (Xombra)

--------------------------------------------------------*/

require_once('recaptchalib.php');

session_start();

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {

	header("location:../error.html");

	die();}

$antesdecore = 1;

include 'databases.php';

$mysqli = CONECTAR_BD($hostname,$user,$password,$db_name);

unset($sql);

$ahora = time();

$clave_acceso = md5(trim($_POST['clave_acceso']));



// tu clave secreta

    $secret = "6LdKuBYTAAAAAAzaD1NhaYVfM3wyGET4VTIpIjUt";

     

    // respuesta vacía

    $response = null;

     

    // comprueba la clave secreta

    $reCaptcha = new ReCaptcha($secret);



if ($_POST["g-recaptcha-response"]) {

	$response = $reCaptcha->verifyResponse(

        $_SERVER["REMOTE_ADDR"],

        $_POST["g-recaptcha-response"]

    );

}



if ($response != null && $response->success) {



	$sql = sprintf("UPDATE hesperia_usuario SET

						clave_acceso= '%s'

						WHERE id = '%s'",

						mysqli_real_escape_string($mysqli,$clave_acceso),

						mysqli_real_escape_string($mysqli,$_SESSION["referencia"]));

	$result = QUERYBD($sql,$hostname,$user,$password,$db_name);

	graba_LOG("Cambio de Clave de Usuario:  $_SESSION[email]",$email,$_SERVER['REMOTE_ADDR'],$ahora,$hostname,$user,$password,$db_name);

	if (mysqli_affected_rows($mysqli))

		{ echo '<br/><div class="alert alert-success" role="alert">

			<p>Clave cambiada satisfactoriamente!</p></div>';

		echo '<meta http-equiv="refresh" content="3; url=https://hoteleshesperia.com.ve"/>';

	 } else { echo '<div class="alert alert-danger" role="alert">

		  <p>Ha ocurrido un error inesperado. Tal vez no modificó nada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>

		</div>'; }

	unset($result,$sql);

	$_POST = array();

}else{

	echo '<div class="alert alert-danger" role="alert">

		  <p>Ha ocurrido un error inesperado. Tal vez no modificó nada.<br/>Intente de nuevo, en caso contrario contacte al Administrador Principal del sitio</p>

		</div>';

}

?>

